/********************************************************************************

   Fotoxx      edit photos and manage collections

   Copyright 2007-2019 Michael Cornelison
   source code URL: https://kornelix.net
   contact: kornelix@posteo.de

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version. See https://www.gnu.org/licenses

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
   See the GNU General Public License for more details.

*********************************************************************************

   Fotoxx image edit - Tools menu functions

   m_index                 dialog to create/update image index file
   index_rebuild           create/update image index file
   index_rebuild_old       use old image index file without updates
   m_move_fotoxx_home      move fotoxx home folder from default ~/.fotoxx
   m_settings              user settings dialog
   m_KBshortcuts           edit keyboard shortcuts
   KBshortcuts_load        load KB shortcuts file into memory
   m_brightgraph           show brightness distribution graph
   m_magnify               magnify the image within a radius of the mouse
   m_duplicates            find all duplicated images
   m_show_RGB              show RGB values for clicked image positions
   m_color_profile         convert from one color profile to another
   m_calibrate_printer     printer color calibration
   print_calibrated        print current image file with calibrated colors
   m_gridlines             setup for grid lines
   m_line_color            choose color for foreground lines
   m_darkbrite             highlight darkest and brightest pixels
   m_remove_dust           remove dust specs from an image
   m_stuck_pixels          find and fix stuck pixels from camera sensor defects
   m_monitor_color         monitor color and contrast check
   m_monitor_gamma         monitor gamma check and adjust
   m_change_lang           choose GUI language
   m_untranslated          report misting translations
   m_resources             print memory allocated and CPU time used
   m_zappcrash             zappcrash test

*********************************************************************************/

#define EX extern                                                                //  enable extern declarations
#include "fotoxx.h"                                                              //  (variables in fotoxx.h are refs)

/********************************************************************************/

//  Index Image Files menu function
//  Dialog to get top image folders, thumbnails folder, indexed metadata items.
//  Update the index_config file and generate new image index.

namespace index_names
{
   zdialog  *zd_indexlog = 0;
   xxrec_t  *xxrec = 0, **xxrec_old = 0, **xxrec_new = 0;
   int      *Fupdate = 0, *Tupdate = 0;
   int      Ffullindex = 0;
   int      Nold, Nnew;
   int      indexupdates, thumbupdates, thumbdeletes;
   int      indexthread1, indexthread2, thumbthread1, thumbthread2;              //  18.07
   int      Fuserkill;
}


//  menu function

void m_index(GtkWidget *, cchar *)
{
   using namespace index_names;

   void index_callbackfunc(GtkWidget *widget, int line, int pos, int kbkey);
   int index_dialog_event(zdialog *zd, cchar *event);

   zdialog        *zd;
   FILE           *fid;
   char           filespec[200], buff[200], sthumbfolder[200];
   char           *pp;
   GtkWidget      *widget;
   int            line, ii, cc, zstat;
   cchar          *greet1 = E2X("Folders for image files "
                                "(subfolders included automatically).");
   cchar          *greet2 = E2X("Select to add, click on X to delete.");
   cchar          *greet3 = E2X("folder for thumbnails");
   cchar          *greet4 = E2X("extra metadata items to include in index");
   cchar          *greet5 = E2X("force a full re-index of all image files");
   cchar          *termmess = E2X("Index function terminated. \n" 
                                  "Indexing is required for search and map functions \n"
                                  "and to make the gallery pages acceptably fast.");

   F1_help_topic = "index_files";
   m_viewmode(0,"0");                                                            //  no view mode                       19.0

   if (checkpend("all")) return;                                                 //  check nothing pending
   Fblock = 1;
// Findexvalid = 0;                                                              //  not before dialog                  19.13
   Ffullindex = 0;

/***
       ______________________________________________________________
      |                      Index Image Files                       |
      |                                                              |
      | Folders for image files (subdirs included automatically).    |
      | [Select] Select to add, click on X to delete.                |
      |  __________________________________________________________  |
      | | X  /home/<user>/Pictures                                 | |
      | | X  /home/<user>/...                                      | |
      | |                                                          | |
      | |                                                          | |
      | |                                                          | |
      | |__________________________________________________________| |
      |                                                              |
      | [Select] folder for thumbnails_____________________________  |
      | [__________________________________________________________] |
      |                                                              |
      | [Select] extra metadata items to include in index            |           18.01
      |                                                              |
      | [x] force a full re-index of all image files                 |           18.07
      |                                                              |
      |                                    [Help] [Proceed] [Cancel] |
      |______________________________________________________________|

***/

   zd = zdialog_new(E2X("Index Image Files"),Mwin,Bhelp,Bproceed,Bcancel,null);

   zdialog_add_widget(zd,"hsep","space","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbgreet1","dialog");
   zdialog_add_widget(zd,"label","labgreet1","hbgreet1",greet1,"space=3");
   zdialog_add_widget(zd,"hbox","hbtop","dialog");
   zdialog_add_widget(zd,"button","browsetop","hbtop",Bselect,"space=3");        //  browse top button
   zdialog_add_widget(zd,"label","labgreet2","hbtop",greet2,"space=5");

   zdialog_add_widget(zd,"hbox","hbtop2","dialog",0,"expand");
   zdialog_add_widget(zd,"label","space","hbtop2",0,"space=3");
   zdialog_add_widget(zd,"vbox","vbtop2","hbtop2",0,"expand");
   zdialog_add_widget(zd,"label","space","hbtop2",0,"space=3");
   zdialog_add_widget(zd,"frame","frtop","vbtop2",0,"expand");
   zdialog_add_widget(zd,"scrwin","scrtop","frtop",0,"expand");
   zdialog_add_widget(zd,"text","topfolders","scrtop");                          //  topfolders text

   zdialog_add_widget(zd,"hsep","space","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbthumb1","dialog");
   zdialog_add_widget(zd,"button","browsethumb","hbthumb1",Bselect,"space=3");   //  browse thumb button
   zdialog_add_widget(zd,"label","labgreet3","hbthumb1",greet3,"space=5");
   zdialog_add_widget(zd,"hbox","hbthumb2","dialog");
   zdialog_add_widget(zd,"frame","frthumb","hbthumb2",0,"space=5|expand");
   zdialog_add_widget(zd,"zentry","sthumbfolder","frthumb");                     //  thumbnail folder

   zdialog_add_widget(zd,"hsep","space","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbxmeta","dialog");
   zdialog_add_widget(zd,"button","browsxmeta","hbxmeta",Bselect,"space=3");     //  browse xmeta metadata              18.01
   zdialog_add_widget(zd,"label","labgreet4","hbxmeta",greet4,"space=5");

   zdialog_add_widget(zd,"hsep","space","dialog",0,"space=5");                   //  force full re-index                18.07
   zdialog_add_widget(zd,"hbox","hbforce","dialog");
   zdialog_add_widget(zd,"check","forcex","hbforce",greet5,"space=3");
   
   widget = zdialog_widget(zd,"topfolders");                                     //  set click function for
   textwidget_set_callbackfunc(widget,index_callbackfunc);                       //    top folders text window

   textwidget_clear(widget);                                                     //  default top folder
   textwidget_append(widget,0," X  %s\n",getenv("HOME"));                        //    /home/<user>

   snprintf(sthumbfolder,200,"%s/thumbnails",get_zhomedir());                    //  default thumbnails folder
   zdialog_stuff(zd,"sthumbfolder",sthumbfolder);                                //    /home/<user>/.fotoxx/thumbnails
   
   xmeta_keys[0] = 0;                                                            //  default no indexed metadata

   snprintf(filespec,200,"%s/index_config",index_folder);                        //  read index_config file,
                                                                                 //    stuff data into dialog widgets
   fid = fopen(filespec,"r");
   if (fid) {
      textwidget_clear(widget);
      while (true) {
         pp = fgets_trim(buff,200,fid,1);
         if (! pp) break;
         if (strmatchN(buff,"thumbnails:",11)) {                                 //  if "thumbnails: /..."
            if (buff[12] == '/')                                                 //    stuff thumbnails folder
               zdialog_stuff(zd,"sthumbfolder",buff+12);
         }
         else if (strmatchN(buff,"metadata:",9)) {                               //  if "metadata:"                     18.01
            for (ii = 0; ii < Mxmeta; ii++) {                                    //    build indexed metadata list
               pp = (char *) strField(buff+9,"^",ii+1);
               if (! pp) break;
               xmeta_keys[ii] = zstrdup(pp);
            }
            xmeta_keys[ii] = 0;                                                  //  mark EOL
         }
         else textwidget_append(widget,0," X  %s\n",buff);                       //  stuff " X  /dir1/dir2..."
      }
      fclose(fid);
   }

   zdialog_resize(zd,500,500);                                                   //  run dialog
   zdialog_run(zd,index_dialog_event,"save");
   zstat = zdialog_wait(zd);                                                     //  wait for completion

   if (zstat != 2)                                                               //  canceled
   {
/***
      STATB    statb;
      int err = stat(sthumbfolder,&statb);                                       //  keep thumbnails folder    removed  19.13
      if (err || ! S_ISDIR(statb.st_mode))                                       //    if possible
         err = shell_ack("mkdir -p -m 0750 \"%s\"",sthumbfolder);
      if (err) zmessageACK(Mwin,"%s \n %s",sthumbfolder,strerror(errno));
      else thumbfolder = zstrdup(sthumbfolder);
***/
      zdialog_free(zd);
      zmessageACK(Mwin,termmess);                                                //  index not finished
      Fblock = 0;
      return;
   }

   snprintf(filespec,200,"%s/index_config",index_folder);                        //  open/write index config file
   fid = fopen(filespec,"w");
   if (! fid) {                                                                  //  fatal error
      zmessageACK(Mwin,"index_config file: \n %s",strerror(errno));
      Fblock = 0;
      m_quit(0,0);
   }

   widget = zdialog_widget(zd,"topfolders");                                     //  get top folders from dialog widget

   for (line = 0; ; line++) {
      pp = textwidget_line(widget,line,1);                                       //  loop widget text lines
      if (! pp || ! *pp) break;
      pp += 4;                                                                   //  skip " X  "
      if (*pp != '/') continue;
      strncpy0(buff,pp,200);                                                     //  /dir1/dir2/...
      cc = strlen(buff);
      if (cc < 5) continue;                                                      //  ignore blanks or rubbish
      if (buff[cc-1] == '/') buff[cc-1] = 0;                                     //  remove trailing '/'
      fprintf(fid,"%s\n",buff);                                                  //  write top folder to config file
   }

   zdialog_fetch(zd,"sthumbfolder",buff,200);                                    //  get thumbnails folder from dialog
   strTrim2(buff);                                                               //  remove surrounding blanks
   cc = strlen(buff);
   if (cc && buff[cc-1] == '/') buff[cc-1] = 0;                                  //  remove trailing '/'
   fprintf(fid,"thumbnails: %s\n",buff);                                         //  thumbnails folder >> config file

   *buff = 0;   
   for (ii = 0; ii < Mxmeta; ii++) {                                             //  indexed metadata >> config file    18.01
      if (! xmeta_keys[ii]) break;
      strncatv(buff,200,xmeta_keys[ii],"^ ",0);
   }
   fprintf(fid,"metadata: %s\n",buff);

   fclose(fid);
   zdialog_free(zd);                                                             //  close dialog

   index_rebuild(2,1);                                                           //  build image index and thumbnail files
   if (! Findexvalid) m_index(0,0);                                              //  failed, try again
   Fblock = 0;                                                                   //  OK
   return;
}


// ------------------------------------------------------------------------------

//  mouse click function for top folders text window
//  remove folder from list where "X" is clicked

void index_callbackfunc(GtkWidget *widget, int line, int pos, int kbkey)
{
   GdkWindow   *gdkwin;
   char        *pp;
   char        *dirlist[maxtopfolders];
   int         ii, jj;
   
   if (kbkey == GDK_KEY_F1) {                                                    //  key F1 pressed, show help          18.01
      showz_userguide(F1_help_topic);
      return;
   }

   gdkwin = gtk_widget_get_window(widget);                                       //  stop updates between clear and refresh
   gdk_window_freeze_updates(gdkwin);
   
   for (ii = jj = 0; ii < maxtopfolders; ii++)                                   //  loop text lines in widget
   {                                                                             //    " X  /dir1/dir2/... "
      pp = textwidget_line(widget,ii,1);
      if (! pp || strlen(pp) < 4) break;
      if (ii == line && pos < 3) continue;                                       //  if "X" clicked, skip deleted line
      dirlist[jj] = zstrdup(pp);
      jj++;
   }
   
   textwidget_clear(widget);

   for (ii = 0; ii < jj; ii++)                                                   //  stuff remaining lines back into widget
   {
      textwidget_append(widget,0,"%s\n",dirlist[ii]);
      zfree(dirlist[ii]); 
   }

   gdk_window_thaw_updates(gdkwin);
   return;
}


// ------------------------------------------------------------------------------

//  index dialog event and completion function

int index_dialog_event(zdialog *zd, cchar *event)
{
   using namespace index_names;

   int         ii;
   GtkWidget   *widget;
   char        **flist, *pp, *sthumbfolder;
   cchar       *topmess = E2X("Choose top image folders");
   cchar       *thumbmess = E2X("Choose thumbnail folder");
   cchar       *xmetamess = E2X("All image files will be re-indexed. \n"
                                "  Continue?");

   if (strmatch(event,"browsetop")) {                                            //  [browse] top folders
      flist = zgetfiles(topmess,MWIN,"folders",getenv("HOME"));                  //  get top folders from user
      if (! flist) return 1;
      widget = zdialog_widget(zd,"topfolders");                                  //  add to dialog list
      for (ii = 0; flist[ii]; ii++) {
         textwidget_append2(widget,0," X  %s\n",flist[ii]);                      //  " X  /dir1/dir2/..."
         zfree(flist[ii]);
      }
      zfree(flist);
   }

   if (strmatch(event,"browsethumb")) {                                          //  [browse] thumbnail folder
      pp = zgetfile(thumbmess,MWIN,"folder",getenv("HOME"));
      if (! pp) return 1;
      sthumbfolder = zstrdup(pp,12);
      if (! strstr(sthumbfolder,"/thumbnails"))                                  //  if not containing /thumbnails,
         strcat(sthumbfolder,"/thumbnails");                                     //    append /thumbnails
      zdialog_stuff(zd,"sthumbfolder",sthumbfolder);
      zfree(sthumbfolder);
      zfree(pp);
   }
   
   if (strmatch(event,"browsxmeta")) {                                           //  [select]                           18.01
      ii = zmessageYN(Mwin,xmetamess);                                           //  add optional indexed metadata
      if (! ii) return 1;
      ii = select_meta_keys(xmeta_keys,1);
      if (ii) Ffullindex = 1;                                                    //  changes made, force full re-index
   }
   
   if (strmatch(event,"forcex")) {                                               //  force full re-index                18.07
      zdialog_fetch(zd,"forcex",ii);
      if (ii) Ffullindex = 1;
   }

   if (! zd->zstat) return 1;                                                    //  wait for completion

   if (zd->zstat == 1) {                                                         //  [help]
      zd->zstat = 0;
      showz_userguide("index_files");
      return 1;
   }

   return 1;                                                                     //  [proceed] or cancel status
}


// ------------------------------------------------------------------------------

//  Rebuild the image index from the index config data. 
//  index level = 0/1/2  =  no index / old files only / old + new files
//  Called from main() when Fotoxx is started (index level from user setting)
//  Called from menu function m_index() (index level = 2)

void index_rebuild(int indexlev, int menu)
{
   using namespace index_names;

   void   index_rebuild_old();
   int    indexlog_dialog_event(zdialog *zd, cchar *event);
   int    index_compare(cchar *rec1, cchar *rec2);
   void   *index_thread(void *);
   void   *thumb_thread(void *);

   GtkWidget   *wlog;
   FILE        *fid;
   int         ii, jj, nn;
   int         Ntop, Nthumb, err, updatesreq;
   int         ftf, NF, orec, orec2, nrec, comp;
   int         cc, cc1, cc2;
   char        *pp, *pp1, *pp2;
   char        filespec[200], buff[XFCC+500];
   char        **flist, *file, *thumbfile;
   STATB       statb;
   double      startime, time0;

   cchar *indexmess = E2X("No image file index was found.\n"
                          "An image file index will be created.\n"
                          "Your image files will not be changed.\n" 
                          "This may need considerable time if you \n"
                          "have many thousands of image files.");

   cchar *indexerr2 = E2X("Thumbnails folder: %s \n"
                          "Please remove.");
   
   cchar *thumberr = E2X("Thumbnails folder: \n  %s \n"
                         "must be named .../thumbnails");
   
   cchar *duperr = E2X("Duplicate or nested folders: \n %s \n %s \n"
                       "Please remove.");

   startime = get_seconds();                                                     //  index function start time
   Findexvalid = 0;
   Fblock = 1;
   Fuserkill = 0;

   if (navi::galleryname) {                                                      //  clear gallery                      19.13
      zfree(navi::galleryname);
      navi::galleryname = 0;
   }

   //  get current top image folders and thumbnails folder
   //  from /home/<user>/.fotoxx/image_index/index_config
   
   for (ii = 0; ii < Ntopfolders; ii++)                                          //  free prior                         19.0
      zfree(topfolders[ii]);

   Ntop = Nthumb = 0;
   snprintf(filespec,200,"%s/index_config",index_folder);                        //  read index config file

   fid = fopen(filespec,"r");
   if (fid) {
      while (true) {                                                             //  get top image folders
         pp = fgets_trim(buff,200,fid,1);
         if (! pp) break;
         if (strmatchN(buff,"thumbnails: /",13)) {                               //  get thumbnails folder
            if (thumbfolder) zfree(thumbfolder);
            thumbfolder = zstrdup(buff+12);
            Nthumb++;
         }
         else if (strmatchN(buff,"metadata:",9)) {                               //  get indexed metadata keys          18.01
            for (ii = 0; ii < Mxmeta; ii++) {
               pp = (char *) strField(buff+9,"^",ii+1);
               if (! pp) {
                  xmeta_keys[ii] = 0;
                  break;
               }
               xmeta_keys[ii] = zstrdup(pp);
            }
         }
         else {
            topfolders[Ntop] = zstrdup(buff);
            if (++Ntop == maxtopfolders) break;
         }
      }
      fclose(fid);
   }
   
   Ntopfolders = Ntop;

   if (! Ntopfolders) {                                                          //  if nothing found, must ask user
      zmessageACK(Mwin,indexmess);
      goto cleanup;
   }

   if (Nthumb != 1) {                                                            //  0 or >1 thumbnail folders
      zmessageACK(Mwin,E2X("specify 1 thumbnail folder"));
      goto cleanup;
   }

   cc = strlen(thumbfolder) - 11 ;                                               //  check /thumbnails name 
   if (! strmatch(thumbfolder+cc,"/thumbnails")) {
      zmessageACK(Mwin,thumberr,thumbfolder);
      goto cleanup;
   }

   err = stat(thumbfolder,&statb);                                               //  create thumbnails folder if needed
   if (err || ! S_ISDIR(statb.st_mode))
      err = shell_ack("mkdir -p -m 0750 \"%s\" ",thumbfolder);                   //  use shell mkdir
   if (err) {
      zmessageACK(Mwin,"%s \n %s",thumbfolder,strerror(errno));
      goto cleanup;
   }
   
   for (ii = 0; ii < Ntopfolders; ii++) {                                        //  disallow top dir = thumbnail dir 
      if (strmatch(topfolders[ii],thumbfolder)) {
         zmessageACK(Mwin,indexerr2,topfolders[ii]);
         goto cleanup;
      }
   }

   for (ii = 0; ii < Ntopfolders; ii++)                                          //  check for duplicate folders
   for (jj = ii+1; jj < Ntopfolders; jj++)                                       //   or nested folders                 19.0
   {
      cc1 = strlen(topfolders[ii]);
      cc2 = strlen(topfolders[jj]);
      if (cc1 <= cc2) {
         pp1 = zstrdup(topfolders[ii],2);
         pp2 = zstrdup(topfolders[jj],2);
      }
      else {
         pp1 = zstrdup(topfolders[jj],2);
         pp2 = zstrdup(topfolders[ii],2);
         cc = cc1;
         cc1 = cc2;
         cc2 = cc;
      }
      strcpy(pp1+cc1,"/");
      strcpy(pp2+cc2,"/");
      nn = strmatchN(pp1,pp2,cc1+1);
      zfree(pp1);
      zfree(pp2);
      if (nn) {
         zmessageACK(Mwin,duperr,topfolders[ii],topfolders[jj]);
         goto cleanup;
      }
   }

   //  process image index according to indexlev:
   //    0 / 1 / 2  =  no index / old files only / old + new files
   
   if (indexlev == 0) {
      printz("no image index: reports disabled \n");                             //  no image index
      Findexvalid = 0;
      Fblock = 0;
      return;
   }

   if (indexlev == 1) {
      printz("old image index: reports will omit new files \n");                 //  image index has old files only
      index_rebuild_old();
      Fblock = 0;
      return;
   }

   if (indexlev == 2)                                                            //  update image index for all image files
      printz("full image index: reports will be complete \n");

   //  create log window for reporting status and statistics
   
   if (zd_indexlog) zdialog_free(zd_indexlog);                                   //  make dialog for output log
   zd_indexlog = zdialog_new(E2X("build index"),Mwin,Bdone,Bcancel,null);
   zdialog_add_widget(zd_indexlog,"frame","frame","dialog",0,"expand");
   zdialog_add_widget(zd_indexlog,"scrwin","scrwin","frame");
   zdialog_add_widget(zd_indexlog,"text","text","scrwin");
   wlog = zdialog_widget(zd_indexlog,"text");

   zdialog_resize(zd_indexlog,700,500);
   zdialog_run(zd_indexlog,indexlog_dialog_event);         

   textwidget_append(wlog,0,"top image folders:\n");                             //  log top image folders
   printz("top image folders: \n"); 

   for (ii = jj = 0; ii < Ntopfolders; ii++) 
   {
      textwidget_append(wlog,0," %s\n",topfolders[ii]);
      printz(" %s\n",topfolders[ii]);
      err = stat(topfolders[ii],&statb);
      if (err || ! S_ISDIR(statb.st_mode)) {                                     //  flag and remove invalid folders    19.0
         textwidget_append(wlog,0,"  *** invalid or not mounted *** \n");
         printz("  *** invalid or not mounted *** \n");
         zfree(topfolders[ii]);
         continue;
      }
      topfolders[jj] = topfolders[ii];
      jj++;
   }
   
   Ntopfolders = jj;                                                             //  revised count

   textwidget_append(wlog,0,"thumbnails folder: \n");                            //  and thumbnails folder
   textwidget_append2(wlog,0," %s \n",thumbfolder);
   printz("thumbnails folder: \n");
   printz(" %s \n",thumbfolder);
   
   if (Ffullindex) {                                                             //  force full re-index                18.01
      Nold = 0;
      goto get_new;
   }

   //  read image index file and build "old list" of index recs

   textwidget_append2(wlog,0,"reading image index file ...\n");
   
   cc = maximages * sizeof(xxrec_t *);
   xxrec_old = (xxrec_t **) zmalloc(cc);                                         //  "old" image index recs
   Nold = 0;
   ftf = 1;
   
   while (true)
   {
      xxrec = read_xxrec_seq(ftf);                                               //  read curr. index recs
      if (! xxrec) break;
      xxrec_old[Nold] = xxrec;
      Nold++;
      if (Nold == maximages) {
         zmessageACK(Mwin,"exceeded max. images: %d \n",maximages);
         Fblock = 0;
         m_quit(0,0);
      }
      zmainloop(100);
   }
   
   //  sort old index recs in order of file name and file mod date

   if (Nold)
      HeapSort((char **) xxrec_old,Nold,index_compare);

   //  replace older recs with newer recs that were appended at the end before sorting

   if (Nold)
   {
      for (orec = 0, orec2 = 1; orec2 < Nold; orec2++)
      {
         if (strmatch(xxrec_old[orec]->file,xxrec_old[orec2]->file)) 
            xxrec_old[orec] = xxrec_old[orec2];
         else {
            orec++;
            xxrec_old[orec] = xxrec_old[orec2];
         }
      }

      Nold = orec + 1;                                                           //  new count
   }

   get_new:

   //  find all image files and create "new list" of index recs

   textwidget_append2(wlog,0,"find all image files ...\n");

   cc = maximages * sizeof(xxrec_t *);
   xxrec_new = (xxrec_t **) zmalloc(cc);                                         //  "new" image index recs
   Fupdate = (int *) zmalloc(maximages * sizeof(int));                           //  flags, index update needed
   Tupdate = (int *) zmalloc(maximages * sizeof(int));                           //  flags, thumb update needed         18.01
   Nnew = 0;
   
   for (ii = 0; ii < Ntopfolders; ii++)
   {
      err = find_imagefiles(topfolders[ii],1+16,flist,NF);                       //  image files, recurse dirs          18.01
      if (err) {
         zmessageACK(Mwin,"find_imagefiles() failure \n");
         Fblock = 0;
         m_quit(0,0);
      }
      
      if (Nnew + NF > maximages) {
         zmessageACK(Mwin,"exceeded max. images: %d \n",maximages);
         Fblock = 0;
         m_quit(0,0);
      }

      for (jj = 0; jj < NF; jj++)
      {
         file = flist[jj];
         nrec = Nnew++;
         xxrec_new[nrec] = (xxrec_t *) zmalloc(sizeof(xxrec_t));                 //  allocate xxrec
         xxrec_new[nrec]->file = file;                                           //  filespec
         stat(file,&statb);
         compact_time(statb.st_mtime,xxrec_new[nrec]->fdate);                    //  file mod date
         xxrec_new[nrec]->pdate[0] = 0;                                          //  image date = empty
         strcpy(xxrec_new[nrec]->rating,"0");                                    //  stars = "0"
         xxrec_new[nrec]->ww = xxrec_new[nrec]->hh = 0;                          //  ww = hh = 0                        19.0
         xxrec_new[nrec]->fsize = 0;                                             //  fsize = 0                          19.0
         xxrec_new[nrec]->tags = 0;                                              //  tags = empty
         xxrec_new[nrec]->capt = 0;                                              //  caption = empty
         xxrec_new[nrec]->comms = 0;                                             //  comments = empty
         xxrec_new[nrec]->location = 0;                                          //  location = empty 
         xxrec_new[nrec]->country = 0;
         xxrec_new[nrec]->flati = 0;                                             //  earth coordinates = 0 
         xxrec_new[nrec]->flongi = 0;
         xxrec_new[nrec]->xmeta = 0;                                             //  indexed metadata = none            18.01
      }

      if (flist) zfree(flist);
   }

   textwidget_append2(wlog,0,"image files found: %d \n",Nnew);
   printz("image files found: %d \n",Nnew);
   
   if (Nnew == 0) {                                                              //  no images found 
      zmessageACK(Mwin,E2X("Top folders have no images"));
      goto cleanup;
   }

   //  sort new index recs in order of file name and file mod date

   if (Nnew)
      HeapSort((char **) xxrec_new,Nnew,index_compare);

   //  merge and compare lists
   //  if filespecs match and have the same date, then "old" record is OK
   //  otherwise flag the file for index record update

   updatesreq = Nnew;
   
   for (orec = nrec = 0; nrec < Nnew; nrec++)                                    //  loop all image files
   {
      Fupdate[nrec] = 1;                                                         //  assume index update is needed
      Tupdate[nrec] = 1;                                                         //  and thumb update                   18.01
      
      if (thumbfile_OK(xxrec_new[nrec]->file)) Tupdate[nrec] = 0;                //  no thumbnail update needed         18.01

      if (orec == Nold) continue;                                                //  no more old recs

      while (true)
      {
         comp = strcmp(xxrec_old[orec]->file,xxrec_new[nrec]->file);             //  compare orec file to nrec file
         if (comp >= 0) break;                                                   //  orec >= nrec
         orec++;                                                                 //  orec < nrec, next orec
         if (orec == Nold) break;
      }
      
      if (comp == 0)                                                             //  orec = nrec (same image file)
      {
         if (strmatch(xxrec_new[nrec]->fdate,xxrec_old[orec]->fdate))            //  file dates match
         {
            Fupdate[nrec] = 0;                                                   //  index update not needed
            updatesreq--;
            strncpy0(xxrec_new[nrec]->pdate,xxrec_old[orec]->pdate,15);          //  copy data from old to new
            strncpy0(xxrec_new[nrec]->rating,xxrec_old[orec]->rating,2);
            xxrec_new[nrec]->ww = xxrec_old[orec]->ww;                           //  19.0
            xxrec_new[nrec]->hh = xxrec_old[orec]->hh;
            xxrec_new[nrec]->fsize = xxrec_old[orec]->fsize;                     //  19.0
            xxrec_new[nrec]->tags = xxrec_old[orec]->tags;
            xxrec_old[orec]->tags = 0;
            xxrec_new[nrec]->capt = xxrec_old[orec]->capt;
            xxrec_old[orec]->capt = 0;
            xxrec_new[nrec]->comms = xxrec_old[orec]->comms;
            xxrec_old[orec]->comms = 0;
            xxrec_new[nrec]->location = xxrec_old[orec]->location; 
            xxrec_old[orec]->location = 0;
            xxrec_new[nrec]->country = xxrec_old[orec]->country;
            xxrec_old[orec]->country = 0;
            xxrec_new[nrec]->flati = xxrec_old[orec]->flati; 
            xxrec_new[nrec]->flongi = xxrec_old[orec]->flongi;
            xxrec_new[nrec]->xmeta = xxrec_old[orec]->xmeta;                     //  18.01
            xxrec_old[orec]->xmeta = 0;
         }
         orec++;                                                                 //  next old rec
      }
   }

   textwidget_append(wlog,0,"index updates needed: %d \n",updatesreq);
   printz("index updates needed: %d \n",updatesreq);

   //  Process entries needing update in the new index list
   //  (new files or files dated later than image index date).
   //  Get updated metadata from image file EXIF/IPTC data.
   //  Check if thumbnail is missing or stale and update if needed.

   textwidget_append(wlog,0,"updating image index and thumbnails ... \n");
   textwidget_append2(wlog,0,"\n");

   indexupdates = thumbupdates = 0;
   indexthread1 = indexthread2 = 0;
   thumbthread1 = thumbthread2 = 0;

   for (nrec = 0; nrec < Nnew; nrec++)                                           //  loop all index recs
   {
      if (Fuserkill || Fshutdown) break;                                         //  killed by user                     18.07
      
      if (Fupdate[nrec]) {                                                       //  update metadata
         indexupdates++;
         while (indexthread1 - indexthread2 == 2) zsleep(0.001);                 //  exif_server count (2)              18.07
         indexthread1++;
         Fupdate[nrec] = nrec;                                                   //  separate arg per thread
         start_detached_thread(index_thread,&Fupdate[nrec]);
      }
      
      if (Tupdate[nrec]) {                                                       //  update thumbnail                   18.01
         thumbupdates++;
         while (thumbthread1 - thumbthread2 == 8) zsleep(0.001);                 //  8 parallel threads
         thumbthread1++;
         Tupdate[nrec] = nrec;
         start_detached_thread(thumb_thread,&Tupdate[nrec]);
      }

      if (Fupdate[nrec] || Tupdate[nrec]) {                                      //  update counters                    18.01
         file = xxrec_new[nrec]->file;
         textwidget_replace(wlog,0,-1,"%d %d %s \n",
                              indexupdates,thumbupdates,file);
      }

      if (indexupdates && indexupdates == 2000 * (indexupdates / 2000))          //  update image index file            18.07
      {                                                                          //    every 2000 updated recs.
         ftf = 1;
         for (ii = 0; ii < nrec; ii++)
            write_xxrec_seq(xxrec_new[ii],ftf);
         write_xxrec_seq(null,ftf);                                              //  close output
         printz("index file updates: %d \n",indexupdates);
      }
   }

   time0 = get_seconds();

   while (true)                                                                  //  wait for last threads to exit
   {
      zsleep(0.01);
      if (get_seconds() - time0 > 10) {                                          //  limit the wait time                18.07
         ii = indexthread1 - indexthread2;
         if (ii) printz("*** %d index threads pending \n",ii);                   //  note the failure
         ii = thumbthread1 - thumbthread2;
         if (ii) printz("*** %d thumbnail threads pending \n",ii);
         break;                                                                  //  continue, finish index file
      }
      if (indexthread2 != indexthread1) continue;
      if (thumbthread2 != thumbthread1) continue;
      break;
   }
   
   if (Fuserkill || Fshutdown) goto cleanup;                                     //  18.07

   textwidget_replace(wlog,0,-1,"index updates: %d   thumbnails: %d \n",         //  final statistics
                           indexupdates, thumbupdates);
   printz("index updates: %d  thumbnail updates: %d, deletes: %d \n",
                       indexupdates, thumbupdates, thumbdeletes);

   //  write updated index records to image index file
   
   printz("writing updated image index file \n");  
   
   if (Nnew)
   {
      err = 0;
      ftf = 1;

      for (nrec = 0; nrec < Nnew; nrec++)
         write_xxrec_seq(xxrec_new[nrec],ftf);                                   //  19.7

      write_xxrec_seq(null,ftf);                                                 //  close output
   }

   //  create image index table in memory

   if (xxrec_tab)
   {
      for (ii = 0; ii < Nxxrec; ii++)                                            //  free memory for old xxrec_tab
      {
         if (xxrec_tab[ii]->file) zfree(xxrec_tab[ii]->file);
         if (xxrec_tab[ii]->tags) zfree(xxrec_tab[ii]->tags);
         if (xxrec_tab[ii]->capt) zfree(xxrec_tab[ii]->capt);
         if (xxrec_tab[ii]->comms) zfree(xxrec_tab[ii]->comms);
         if (xxrec_tab[ii]->location) zfree(xxrec_tab[ii]->location);
         if (xxrec_tab[ii]->country) zfree(xxrec_tab[ii]->country);
         if (xxrec_tab[ii]->xmeta) zfree(xxrec_tab[ii]->xmeta);                  //  18.01
         zfree(xxrec_tab[ii]);
      }
      
      zfree(xxrec_tab);
      xxrec_tab = 0;
      Nxxrec = 0;
   }

   if (Nnew)
   {
      cc = maximages * sizeof(xxrec_t *);                                        //  make new table with max. capacity
      xxrec_tab = (xxrec_t **) zmalloc(cc);
      
      for (nrec = 0; nrec < Nnew; nrec++)
      {
         if (! xxrec_new[nrec]->tags) xxrec_new[nrec]->tags = zstrdup("null");
         if (! xxrec_new[nrec]->capt) xxrec_new[nrec]->capt = zstrdup("null");
         if (! xxrec_new[nrec]->comms) xxrec_new[nrec]->comms = zstrdup("null");
         if (! xxrec_new[nrec]->location) xxrec_new[nrec]->location = zstrdup("null");
         if (! xxrec_new[nrec]->country) xxrec_new[nrec]->country = zstrdup("null");
         if (! xxrec_new[nrec]->xmeta) xxrec_new[nrec]->xmeta = zstrdup("null");                                    //  18.01
         xxrec_tab[nrec] = xxrec_new[nrec];
      }
      
      Nxxrec = Nnew;
   }

   //  find orphan thumbnails and delete them
   
   textwidget_append2(wlog,0,"deleting orphan thumbnails ... \n");
   thumbdeletes = 0;
   
   err = find_imagefiles(thumbfolder,2+16,flist,NF);                             //  thumbnails, recurse dirs           18.01
   if (err) {
      zmessageACK(Mwin,strerror(errno));
      NF = 0;
   }

   for (ii = 0; ii < NF; ii++)
   {
      zmainloop(100);
      thumbfile = flist[ii];
      file = thumb2imagefile(thumbfile);
      if (file) { 
         zfree(file); 
         zfree(thumbfile); 
         continue; 
      }
      
      pp = thumbfile + strlen(thumbfolder);                                      //  do not remove thumbnails           19.0
      for (jj = 0; jj < Ntopfolders; jj++)                                       //    for missing top image folder
         if (strmatchN(topfolders[jj],pp,strlen(topfolders[jj]))) break;         //  (maybe unmounted disk)
      if (jj == Ntopfolders) continue;
      err = stat(topfolders[jj],&statb);
      if (err) continue;

      remove(thumbfile);
      zfree(thumbfile);
      thumbdeletes++;
   }
   
   if (flist) zfree(flist);
   
   textwidget_append2(wlog,0,"thumbnails deleted: %d \n",thumbdeletes);

   textwidget_append2(wlog,0,"%s\n",Bcompleted);                                 //  index complete and OK
   printz("index time: %.1f seconds \n",get_seconds() - startime);               //  log elapsed time 

   if (Nxxrec) Findexvalid = 2;                                                  //  image index is complete
   
cleanup:                                                                         //  free allocated memory

   if (Fshutdown) {                                                              //  18.07
      Fblock = 0;
      return;
   }

   if (xxrec_old)
   {
      for (orec = 0; orec < Nold; orec++)
      {
         zfree(xxrec_old[orec]->file);                                           //  free xxrec-> char. strings
         if (xxrec_old[orec]->tags) zfree(xxrec_old[orec]->tags);
         if (xxrec_old[orec]->capt) zfree(xxrec_old[orec]->capt);
         if (xxrec_old[orec]->comms) zfree(xxrec_old[orec]->comms);
         if (xxrec_old[orec]->location) zfree(xxrec_old[orec]->location);
         if (xxrec_old[orec]->country) zfree(xxrec_old[orec]->country);
         if (xxrec_old[orec]->xmeta) zfree(xxrec_old[orec]->xmeta);              //  18.01
         zfree(xxrec_old[orec]);                                                 //  free xxrec record
      }

      zfree(xxrec_old);
      xxrec_old = 0;
   }

   if (xxrec_new)                                                                //  xxrec_new[*] xxrec records
   {                                                                             //    now belong to xxrec_tab[*] 
      zfree(xxrec_new);                                                          //  free pointers only
      xxrec_new = 0;
   }

   if (Fupdate)
   {
      zfree(Fupdate);                                                            //  free memory
      Fupdate = 0;
   }

   if (zd_indexlog && ! menu)                                                    //  if not manual run, kill log window
      zdialog_send_event(zd_indexlog,"exitlog");
   
   Fblock = 0;                                                                   //  unblock
   Ffullindex = 0;
   
   if (! Findexvalid) {                                                          //  18.01
      if (menu) return;                                                          //  called manually
      else m_index(0,0);                                                         //  called from fotoxx startup
   }

   return;
}


// ------------------------------------------------------------------------------

//  thread process - create thumbnail file for given image file

void * thumb_thread(void *arg)                                                   //  18.01
{
   using namespace index_names;

   int      nrec;
   char     *file;
   
   nrec = *((int *) arg);
   file = xxrec_new[nrec]->file;                                                 //  image file

   file = xxrec_new[nrec]->file;                                                 //  image file to check
   update_thumbfile(file);                                                       //  do thumbnail update if needed
   zadd_locked(thumbthread2,+1);
   pthread_exit(0);
   return 0;
}


// ------------------------------------------------------------------------------

//  thread process - get image file metadata and build image index rec.
//  thread has no GTK calls

void * index_thread(void *arg)
{
   using namespace index_names;

   int      nrec, err, ii, nkey, xcc;
   int      ww, hh;
   char     *file, *pp;
   float    flati, flongi;
   char     xmetarec[1000];
   STATB    statb;

   cchar    *exifkeys[100] = { "FileName", exif_date_key, iptc_keywords_key,     //  first 12 keys are fixed
            iptc_rating_key, exif_ww_key, exif_hh_key,                           //  (replace exif_wwhh_key)            19.15
            exif_comment_key, iptc_caption_key,
            exif_city_key, exif_country_key, exif_lati_key, exif_longi_key };

   char     *ppv[100], *exiffile = 0;
   char     *exifdate = 0, *iptctags = 0, *iptcrating = 0;
   char     *exifww = 0, *exifhh = 0, *iptccapt = 0, *exifcomms = 0;
   char     *exifcity = 0, *exifcountry = 0, *exiflat = 0, *exiflong = 0;
   char     city2[100], country2[100], lat2[20], long2[20];
   
   nrec = *((int *) arg);
   file = xxrec_new[nrec]->file;                                                 //  image file
   
   nkey = 12;                                                                    //  add keys for indexed metadata      18.01
   for (ii = 0; ii < Mxmeta; ii++) {                                             //    from exifkeys[11]
      if (! xmeta_keys[ii]) break;
      exifkeys[nkey] = xmeta_keys[ii];
      nkey++;
   }
   
   err = exif_get(file,exifkeys,ppv,nkey);                                       //  get exif/iptc metadata
   if (err) {
      printz("exif_get() failure: %s \n",file);                                  //  metadata unreadable
      goto exit_thread;
   }

   exiffile = ppv[0];
   pp = strrchr(file,'/');
   if (! exiffile || ! strmatch(exiffile,pp+1)) {                                //  image file has no metadata
      printz("exif_get() no data: %s \n",file);
      goto exit_thread;
   }
   
   exifdate = ppv[1];                                                            //  exif/iptc metadata returned
   iptctags = ppv[2];                                                            //    11 fixed keys
   iptcrating = ppv[3];
   exifww = ppv[4];
   exifhh = ppv[5];
   exifcomms = ppv[6];
   iptccapt = ppv[7];
   exifcity = ppv[8];
   exifcountry = ppv[9];
   exiflat = ppv[10];
   exiflong = ppv[11];

   if (exifdate && strlen(exifdate) > 3)                                         //  exif date (photo date)
      exif_tagdate(exifdate,xxrec_new[nrec]->pdate);
   else strcpy(xxrec_new[nrec]->pdate,"null");                                   //  not present

   if (iptcrating && strlen(iptcrating)) {                                       //  iptc rating
      xxrec_new[nrec]->rating[0] = *iptcrating;
      xxrec_new[nrec]->rating[1] = 0;
   }
   else strcpy(xxrec_new[nrec]->rating,"0");                                     //  not present

   if (exifww && exifhh) {                                                       //  19.15
      convSI(exifww,ww);
      convSI(exifhh,hh);
      if (ww > 0 && hh > 0) {
         xxrec_new[nrec]->ww = ww;
         xxrec_new[nrec]->hh = hh;
      }
   }

   err = stat(file,&statb);
   xxrec_new[nrec]->fsize = statb.st_size;                                       //  file size                          19.0

   if (iptctags && strlen(iptctags)) {                                           //  iptc tags
      xxrec_new[nrec]->tags = iptctags;
      iptctags = 0;
   }
   else xxrec_new[nrec]->tags = zstrdup("null");                                 //  19.13

   if (iptccapt && strlen(iptccapt)) {                                           //  iptc caption
      xxrec_new[nrec]->capt = iptccapt;
      iptccapt = 0;
   }
   else xxrec_new[nrec]->capt = zstrdup("null");                                 //  19.13

   if (exifcomms && strlen(exifcomms)) {                                         //  exif comments
      xxrec_new[nrec]->comms = exifcomms;
      exifcomms = 0;
   }
   else xxrec_new[nrec]->comms = zstrdup("null");                                //  19.13

   strcpy(city2,"null");                                                         //  geotags = empty
   strcpy(country2,"null");
   strcpy(lat2,"null");
   strcpy(long2,"null");

   if (exifcity) strncpy0(city2,exifcity,99);                                    //  get from exif if any
   if (exifcountry) strncpy0(country2,exifcountry,99);
   if (exiflat) strncpy0(lat2,exiflat,10);
   if (exiflong) strncpy0(long2,exiflong,10);
   
   xxrec_new[nrec]->location = zstrdup(city2); 
   xxrec_new[nrec]->country = zstrdup(country2);
   
   if (strmatch(lat2,"null") || strmatch(long2,"null")) 
      xxrec_new[nrec]->flati = xxrec_new[nrec]->flongi = 0;
   else {
      flati = atof(lat2);
      flongi = atof(long2);
      if (flati < -90.0 || flati > 90.0) flati = flongi = 0;
      if (flongi < -180.0 || flongi > 180.0) flati = flongi = 0;
      xxrec_new[nrec]->flati = flati;
      xxrec_new[nrec]->flongi = flongi;
   }

   if (exiffile) zfree(exiffile);
   if (exifdate) zfree(exifdate);                                                //  free EXIF data
   if (iptcrating) zfree(iptcrating);
   if (exifww) zfree(exifww);
   if (exifhh) zfree(exifhh);
   if (iptctags) zfree(iptctags);
   if (iptccapt) zfree(iptccapt);
   if (exifcomms) zfree(exifcomms);
   if (exifcity) zfree(exifcity);
   if (exifcountry) zfree(exifcountry);
   if (exiflat) zfree(exiflat);
   if (exiflong) zfree(exiflong);

   xcc = 0;

   for (ii = 12; ii < nkey; ii++) {                                              //  add indexed metadata if any        18.01
      if (! ppv[ii]) continue;
      if (strlen(exifkeys[ii]) + strlen(ppv[ii]) > 100) continue;                //  impractical for image search
      strcpy(xmetarec+xcc,exifkeys[ii]);                                         //  construct series 
      xcc += strlen(exifkeys[ii]);                                               //    "keyname=keydata^ "
      xmetarec[xcc++] = '=';
      strcpy(xmetarec+xcc,ppv[ii]);
      xcc += strlen(ppv[ii]); 
      strcpy(xmetarec+xcc,"^ ");
      xcc += 2;
      zfree(ppv[ii]);
      if (xcc > 895) {
         printz("file metadata exceeds record size: %s \n",file);
         break;
      }
   }

   if (xcc > 0) xxrec_new[nrec]->xmeta = zstrdup(xmetarec);
   else xxrec_new[nrec]->xmeta = zstrdup("null");

exit_thread:
   zadd_locked(indexthread2,+1);                                                 //  bugfix                             18.01
   pthread_exit(0);
   return 0;
}


// ------------------------------------------------------------------------------

//  index log window dialog response function

int indexlog_dialog_event(zdialog *zd, cchar *event)
{
   using namespace index_names;

   int      nn;
   cchar    *canmess = E2X("Cancel image index function?");
   
   if (strmatch(event,"exitlog")) {                                              //  auto-kill from index_rebuild()
      zmainloop();
      zsleep(0.5);
      zdialog_free(zd);
      zd_indexlog = 0;
      return 1;
   }

   if (! zd->zstat) return 1;                                                    //  continue

   if (zd->zstat == 1 || zd->zstat == 2) {                                       //  [Done] or [Cancel] button
      if (Findexvalid) {     
         zdialog_free(zd);                                                       //  index completed, kill dialog
         zd_indexlog = 0;
         return 1;
      }
      nn = zdialog_choose(Mwin,"mouse",canmess,Bcontinue,Bcancel,null);          //  incomplete, ask for confirmation 
      if (nn == 1) {
         zd->zstat = 0;                                                          //  continue
         return 1;
      }
   }
   
   zdialog_free(zd);                                                             //  cancel, kill index job             18.01
   zd_indexlog = 0;
   Fuserkill = 1;
   return 1;
}


// ------------------------------------------------------------------------------

//  sort compare function - compare index records and return
//    <0   0   >0   for   rec1 < rec2   rec1 == rec2   rec1 > rec2

int index_compare(cchar *rec1, cchar *rec2)
{
   xxrec_t *xxrec1 = (xxrec_t *) rec1;
   xxrec_t *xxrec2 = (xxrec_t *) rec2;

   int nn = strcmp(xxrec1->file,xxrec2->file);
   if (nn) return nn;
   nn = strcmp(xxrec1->fdate,xxrec2->fdate);
   return nn;
}


/********************************************************************************/

//  Rebuild image index table from existing image index file 
//    without searching for new and modified files.

void index_rebuild_old()
{
   using namespace index_names;

   int   ftf, cc, rec, rec2, Ntab;
   
   Fblock = 1;
   Findexvalid = 0;

   //  read image index file and build table of index records

   cc = maximages * sizeof(xxrec_t *);
   xxrec_tab = (xxrec_t **) zmalloc(cc);                                         //  image index recs
   Ntab = 0;
   ftf = 1;

   while (true)
   {
      xxrec = read_xxrec_seq(ftf);                                               //  read curr. index recs
      if (! xxrec) break;
      xxrec_tab[Ntab] = xxrec;
      Ntab++;
      if (Ntab == maximages) {
         zmessageACK(Mwin,"exceeded max. images: %d \n",maximages);
         Fblock = 0;
         m_quit(0,0);
      }
      zmainloop(100);
   }
   
   //  sort index recs in order of file name and file mod date

   if (Ntab)
      HeapSort((char **) xxrec_tab,Ntab,index_compare);

   //  replace older recs with newer (appended) recs now sorted together

   if (Ntab)
   {   
      for (rec = 0, rec2 = 1; rec2 < Ntab; rec2++)
      {
         if (strmatch(xxrec_tab[rec]->file,xxrec_tab[rec2]->file)) 
            xxrec_tab[rec] = xxrec_tab[rec2];
         else {
            rec++;
            xxrec_tab[rec] = xxrec_tab[rec2];
         }
      }

      Ntab = rec + 1;                                                            //  new count
   }

   Nxxrec = Ntab;
   if (Nxxrec) Findexvalid = 1;                                                  //  index OK but missing new files
   Fblock = 0;                                                                   //  unblock
   return;
}


/********************************************************************************/

//  move fotoxx home folder with all contained user files
//  does not return. starts new session if OK.

void m_move_fotoxx_home(GtkWidget *, cchar *)                                    //  18.07
{
   int      yn, nn, err;
   FILE     *fid;
   STATB    statb;
   char     oldhome[200], newhome[200];
   char     oldstring[200], newstring[200];
   char     *pp, configfile[200], buff[200], fotoxxhome[100];

   cchar    *mess1 = E2X("Do you want to move Fotoxx home? \n"
                         "  from: %s \n  to: %s");
   cchar    *mess3 = E2X("moving files ...");

   F1_help_topic = "move_fotoxx_home";

   if (checkpend("all")) return;                                                 //  check nothing pending

   strncpy0(oldhome,get_zhomedir(),200);                                         //  get present home folder

   pp = oldhome + strlen(oldhome);                                               //  remove trailing '/' if present
   if (pp[-1] == '/') pp[-1] = 0;

   err = stat(oldhome,&statb);                                                   //  check old home exists
   if (err) {
      zmessageACK(Mwin,"old fotoxx home: %s \n",strerror(errno));
      exit(1);
   }

   if (! S_ISDIR(statb.st_mode)) {                                               //  check if a folder
      zmessageACK(Mwin,"old location is not a folder");
      exit(1);
   }

retry:

   pp = zgetfolder(E2X("new Fotoxx home folder"),MWIN,oldhome);                  //  get new home folder
   if (! pp) return;
   strncpy0(newhome,pp,200);
   zfree(pp);

   pp = newhome + strlen(newhome);                                               //  remove trailing '/' if present
   if (pp[-1] == '/') pp[-1] = 0;
   
   err = stat(newhome,&statb);                                                   //  check new home folder exists
   if (err) {
      zmessageACK(Mwin,strerror(errno));
      goto retry;
   }
   
   if (! S_ISDIR(statb.st_mode)) {                                               //  check if a folder
      zmessageACK(Mwin,E2X("new location is not a folder"));
      goto retry;
   }
   
   if (strchr(newhome,' ')) {                                                    //  disallow blanks in path name
      zmessageACK(Mwin,E2X("new location name contains a space"));
      goto retry;
   }
   
   if (! strcasestr(newhome,"fotoxx"))                                           //  add /fotoxx if not already         19.0
      strncatv(newhome,200,"/fotoxx",null);
   
   if (strmatch(oldhome,newhome)) goto retry;
   
   yn = zmessageYN(Mwin,mess1,oldhome,newhome);                                  //  ask user to proceed
   if (! yn) goto retry;
   
   printz("%s \n",mess3);                                                        //  "moving files ..."
   poptext_mouse(mess3,0,0,0,0);

   err = shell_ack("cp -R %s/* %s",oldhome,newhome);                             //  copy all files from old home to new
   if (err) {
      zmessageACK(Mwin,"copy failed: %s \n",strerror(errno));
      exit(1);
   }
   
   zmainsleep(2);
   poptext_mouse(0,0,0,0,0);

   snprintf(configfile,200,"%s/image_index/index_config",newhome);               //  read (new) index config file
   fid = fopen(configfile,"r");
   if (! fid) {
      zmessageACK(Mwin,"index_config file: %s",strerror(errno));
      m_quit(0,0);
   }
   
   while (true) {
      pp = fgets_trim(buff,200,fid,1);                                           //  look for record "thumbnails: /..."
      if (! pp) break;
      if (strmatchN(pp,"thumbnails: /",13)) break;
   }
   
   fclose(fid);

   if (! pp) {
      zmessageACK(Mwin,E2X("index_config file has no thumbnails folder"));
      m_quit(0,0);
   }
   
   snprintf(oldstring,200,"thumbnails: %s/thumbnails",oldhome);                  //  if thumbnails folder inside,
   snprintf(newstring,200,"thumbnails: %s/thumbnails",newhome);                  //    oldhome, change to newhome
   nn = zsed(configfile,oldstring,newstring,null);
   if (nn < 0) {
      zmessageACK(Mwin,E2X("update thumbnail folder failed"));
      m_quit(0,0);
   }
   if (nn == 0) zmessageACK(Mwin,E2X("thumbnails folder not changed"));
   else zmessageACK(Mwin,E2X("new thumbnails folder: %s/thumbnails"),newhome);
   
   snprintf(fotoxxhome,100,"%s/.fotoxx-home",getenv("HOME"));                    //  write new ~/.fotoxx-home file
   fid = fopen(fotoxxhome,"w");
   if (! fid) {
      zmessageACK(Mwin,"cannot write .fotoxx-home file: %s",strerror(errno));
      m_quit(0,0);
   }
   fprintf(fid,"%s",newhome);
   fclose(fid);
   
   zmessageACK(Mwin,E2X("Fotoxx will restart"));
   new_session(0);                                                               //  start fotoxx again
   zsleep(1);                                                                    //  delay before SIGTERM in quitxx()   19.0
   m_quit(0,0);                                                                  //  quit this session

   return;
}


/********************************************************************************/

//  user settings dialog

namespace usersettings
{
   cchar    *startopt[8][2] = {  
               {"recent", E2X("Recent Files Gallery")},                            //  fotoxx startup display options
               {"newest", E2X("Newest Files Gallery")},
               {"specG",  E2X("Specific Gallery")},
               {"album",  E2X("Album Gallery")},
               {"prevG",  E2X("Previous Gallery")},
               {"prevF",  E2X("Previous File")},
               {"specF",  E2X("Specific File")},
               {"blank",  E2X("Blank Window")} };
}


//  menu function

void m_settings(GtkWidget *, cchar *)
{
   using namespace usersettings;
   
   int   settings_dialog_event(zdialog *zd, cchar *event);

   int      ii, zstat;
   zdialog  *zd;
   char     txrgb[20];

/***
       ______________________________________________________________
      |                       User Settings                          |
      |                                                              |
      | Startup Display [ previous file |v]                          |
      | [_________________________________________________] [Browse] |
      |                                                              |
      | Background color: F-View [###]  G-View [###]                 |
      | Menu Text [###]  Background [###]                            |                                                  18.01
      | Menu Style  (o) Icons  (o) Text  (o) Both   Icon Size [___]  |
      | Dialog Font [ sans 10 _______] [choose]                      |
      | Zoom: (o) drag  (o) scroll  [x] fast  zooms/2x [__]          |
      | JPEG save quality [____]                                     |
      | Curve node capture distance [____]                           |
      | Map marker size [___]                                        |
      | [x] prev/next shows last file version only                   |
      | [x] shift image right when editing                           |
      | image index level [___] Fotoxx started directly              |           //  0/1/2 = none/old/old+search new
      | image index level [___] Fotoxx started by file manager       |
      | RAW file types  [__________________________________________] |
      | VIDEO file types  [________________________________________] |
      |                                                              |
      |                                                       [done] |
      |______________________________________________________________|

***/

   F1_help_topic = "user_settings";

   m_viewmode(0,"0");                                                            //  file view mode                     19.0
   if (checkpend("all")) return;                                                 //  check nothing pending
   Fblock = 1;

   zd = zdialog_new(E2X("User Settings"),Mwin,Bdone,null);

   zdialog_add_widget(zd,"hbox","hbstart","dialog");
   zdialog_add_widget(zd,"label","labstart","hbstart",E2X("Startup Display"),"space=5");
   zdialog_add_widget(zd,"combo","startopt","hbstart",0);
   
   for (ii = 0; ii < 8; ii++)
      zdialog_cb_app(zd,"startopt",startopt[ii][1]);                             //  startup display option

   zdialog_add_widget(zd,"hbox","hbbrowse","dialog","space=3");
   zdialog_add_widget(zd,"zentry","browsefile","hbbrowse",0,"expand");
   zdialog_add_widget(zd,"button","browse","hbbrowse",Bbrowse,"space=5");

   zdialog_add_widget(zd,"hbox","hbbg","dialog",0,"space=3");                    //  F-view and G-view background color
   zdialog_add_widget(zd,"label","labbg","hbbg",E2X("Background color:"),"space=5");
   zdialog_add_widget(zd,"label","labfbg","hbbg",E2X("F-View"),"space=5");
   zdialog_add_widget(zd,"colorbutt","FBrgb","hbbg");
   zdialog_add_widget(zd,"label","space","hbbg",0,"space=5");
   zdialog_add_widget(zd,"label","labgbg","hbbg",E2X("G-View"),"space=5");
   zdialog_add_widget(zd,"colorbutt","GBrgb","hbbg");

   zdialog_add_widget(zd,"hbox","hbmenu","dialog");                              //  menu text and background color     18.01
   zdialog_add_widget(zd,"label","labmt","hbmenu",E2X("Menu Text"),"space=5");
   zdialog_add_widget(zd,"colorbutt","MFrgb","hbmenu");
   zdialog_add_widget(zd,"label","space","hbmenu",0,"space=5");
   zdialog_add_widget(zd,"label","labmb","hbmenu",E2X("Background"),"space=5");
   zdialog_add_widget(zd,"colorbutt","MBrgb","hbmenu");

   zdialog_add_widget(zd,"hbox","hbmenu","dialog");                              //  menu style
   zdialog_add_widget(zd,"label","labms","hbmenu",E2X("Menu Style"),"space=5");
   zdialog_add_widget(zd,"radio","icons","hbmenu",E2X("Icons"),"space=3");
   zdialog_add_widget(zd,"radio","text","hbmenu",E2X("Text"),"space=3");         //  18.07
   zdialog_add_widget(zd,"radio","both","hbmenu",E2X("Both"),"space=3");
   zdialog_add_widget(zd,"label","space","hbmenu",0,"space=10");
   zdialog_add_widget(zd,"label","labis","hbmenu",E2X("Icon size"));
   zdialog_add_widget(zd,"zspin","iconsize","hbmenu","26|48|1|32","space=3");    //  18.07
   
   zdialog_add_widget(zd,"hbox","hbfont","dialog");                              //  menu and dialog font
   zdialog_add_widget(zd,"label","labfont","hbfont",E2X("Dialog font"),"space=5");
   zdialog_add_widget(zd,"zentry","font","hbfont","Sans 10","size=20");
   zdialog_add_widget(zd,"button","choosefont","hbfont",Bchoose,"space=5");
   
   zdialog_add_widget(zd,"hbox","hbz","dialog");                                 //  pan/zoom options
   zdialog_add_widget(zd,"label","labips","hbz","Zoom:","space=5");
   zdialog_add_widget(zd,"radio","drag","hbz","drag","space=3");
   zdialog_add_widget(zd,"radio","scroll","hbz","scroll","space=3");
   zdialog_add_widget(zd,"check","fast","hbz","fast","space=3");
   zdialog_add_widget(zd,"label","labz","hbz","zooms/2x","space=5");
   zdialog_add_widget(zd,"zspin","zoomcount","hbz","1|8|1|2","size=2");

   zdialog_add_widget(zd,"hbox","hbjpeg","dialog");
   zdialog_add_widget(zd,"label","labqual","hbjpeg",E2X("JPEG save quality"),"space=5");
   zdialog_add_widget(zd,"zspin","quality","hbjpeg","1|100|1|90");
   
   zdialog_add_widget(zd,"hbox","hbcap","dialog");
   zdialog_add_widget(zd,"label","labcap","hbcap",E2X("Curve node capture distance"),"space=5");
   zdialog_add_widget(zd,"zspin","nodecap","hbcap","3|20|1|5");

   zdialog_add_widget(zd,"hbox","hbnet","dialog");
   zdialog_add_widget(zd,"label","labnet","hbnet",E2X("Map marker size"),"space=5");
   zdialog_add_widget(zd,"zspin","map_dotsize","hbnet","5|20|1|8");

   zdialog_add_widget(zd,"hbox","hblastver","dialog");
   zdialog_add_widget(zd,"check","lastver","hblastver",E2X("show last file version only"),"space=5");

   zdialog_add_widget(zd,"hbox","hbshiftright","dialog");
   zdialog_add_widget(zd,"check","shiftright","hbshiftright",E2X("shift image to right margin"),"space=5");

   zdialog_add_widget(zd,"hbox","hbxlev","dialog");
   zdialog_add_widget(zd,"label","labxlev","hbxlev",E2X("image index level"),"space=5");
   zdialog_add_widget(zd,"zspin","indexlev","hbxlev","0|2|1|2");
   zdialog_add_widget(zd,"label","labxlev2","hbxlev",E2X("Fotoxx started directly"),"space=5");

   zdialog_add_widget(zd,"hbox","hbfmxlev","dialog");
   zdialog_add_widget(zd,"label","labfmxlev","hbfmxlev",E2X("image index level"),"space=5");
   zdialog_add_widget(zd,"zspin","fmindexlev","hbfmxlev","0|2|1|2");
   zdialog_add_widget(zd,"label","labfmxlev2","hbfmxlev",E2X("Fotoxx started by file manager"),"space=5");

   zdialog_add_widget(zd,"hbox","hbrawfile","dialog");
   zdialog_add_widget(zd,"label","rawlab","hbrawfile",E2X("RAW file types"),"space=5");
   zdialog_add_widget(zd,"zentry","rawtypes","hbrawfile",".raw .rw2","expand");

   zdialog_add_widget(zd,"hbox","hbvideos","dialog");
   zdialog_add_widget(zd,"label","videolab","hbvideos",E2X("video file types"),"space=5");
   zdialog_add_widget(zd,"zentry","videotypes","hbvideos",".mp4 .mov","expand");

   for (ii = 0; ii < 8; ii++) {
      if (strmatch(startdisplay,startopt[ii][0]))                                //  set startup display option list
         zdialog_stuff(zd,"startopt",startopt[ii][1]);
   }

   if (strmatch(startdisplay,"specG"))                                           //  restore current startup option
      zdialog_stuff(zd,"browsefile",startfolder);
   else if (strmatch(startdisplay,"specF")) 
      zdialog_stuff(zd,"browsefile",startfile);
   else if (strmatch(startdisplay,"album")) 
      zdialog_stuff(zd,"browsefile",startalbum);
   else zdialog_stuff(zd,"browsefile","");
   
   zdialog_stuff(zd,"icons",0);                                                  //  menu style
   zdialog_stuff(zd,"text",0);
   zdialog_stuff(zd,"both",0);
   if (strmatch(menu_style,"icons"))
      zdialog_stuff(zd,"icons",1);
   else if (strmatch(menu_style,"text"))
      zdialog_stuff(zd,"text",1);
   else zdialog_stuff(zd,"both",1);
   
   snprintf(txrgb,20,"%d|%d|%d",FBrgb[0],FBrgb[1],FBrgb[2]);                     //  F-view background color
   zdialog_stuff(zd,"FBrgb",txrgb);
   snprintf(txrgb,20,"%d|%d|%d",GBrgb[0],GBrgb[1],GBrgb[2]);                     //  G-view background color
   zdialog_stuff(zd,"GBrgb",txrgb);

   snprintf(txrgb,20,"%d|%d|%d",MFrgb[0],MFrgb[1],MFrgb[2]);                     //  menus font color                   18.01
   zdialog_stuff(zd,"MFrgb",txrgb);
   snprintf(txrgb,20,"%d|%d|%d",MBrgb[0],MBrgb[1],MBrgb[2]);                     //  menus background color             18.01
   zdialog_stuff(zd,"MBrgb",txrgb);
   
   zdialog_stuff(zd,"iconsize",iconsize);                                        //  icon size
   
   zdialog_stuff(zd,"font",dialog_font);                                         //  curr. dialog font
   
   zdialog_stuff(zd,"drag",1);                                                   //  drag/scroll defaults
   zdialog_stuff(zd,"scroll",1);
   zdialog_stuff(zd,"fast",0);
   
   if (Fdragopt == 1) zdialog_stuff(zd,"drag",1);                                //  set drag/scroll option
   if (Fdragopt == 2) zdialog_stuff(zd,"scroll",1);
   if (Fdragopt == 3) zdialog_stuff(zd,"drag",1);
   if (Fdragopt == 4) zdialog_stuff(zd,"scroll",1);
   if (Fdragopt >= 3) zdialog_stuff(zd,"fast",1);

   if (zoomcount >= 1 && zoomcount <= 8)                                         //  zooms for 2x increase
      zdialog_stuff(zd,"zoomcount",zoomcount);

   zdialog_stuff(zd,"quality",jpeg_def_quality);                                 //  default jpeg file save quality

   zdialog_stuff(zd,"nodecap",splcurve_minx);                                    //  edit curve min. node distance 
   zdialog_stuff(zd,"map_dotsize",map_dotsize);                                  //  map dot size

   zdialog_stuff(zd,"lastver",Flastversion);                                     //  prev/next shows last version only
   zdialog_stuff(zd,"shiftright",Fshiftright);                                   //  shift image to right margin
   zdialog_stuff(zd,"indexlev",Findexlev);                                       //  index level, always
   zdialog_stuff(zd,"fmindexlev",FMindexlev);                                    //  index level, file manager call

   zdialog_stuff(zd,"rawtypes",RAWfiletypes);                                    //  RAW file types
   zdialog_stuff(zd,"videotypes",VIDEOfiletypes);                                //  VIDEO file types

   zdialog_run(zd,settings_dialog_event,"parent");                               //  run dialog and wait for completion
   zstat = zdialog_wait(zd);
   zdialog_free(zd);

   Fblock = 0;

   if (zstat == 1) {
      new_session(0);                                                            //  start new session if changes made  18.01
      zsleep(1);                                                                 //  delay before SIGTERM in quitxx()   19.0
      quitxx();
   }

   return;
}


//  settings dialog event function

int settings_dialog_event(zdialog *zd, cchar *event)
{
   using namespace usersettings;

   int            ii, jj, nn;
   char           *pp, temp[200];
   cchar          *ppc;
   static char    browsefile[500] = "";
   char           txrgb[20];
   GtkWidget      *font_dialog;
   
   if (zd->zstat && zd->zstat != 1) return 1;                                    //  cancel

   if (strmatch(event,"browse"))                                                 //  browse for startup folder or file
   {
      zdialog_fetch(zd,"startopt",temp,200);                                     //  get startup option
      for (ii = 0; ii < 8; ii++) {
         if (strmatch(temp,startopt[ii][1])) {
            zfree(startdisplay);
            startdisplay = zstrdup(startopt[ii][0]);
         }
      }
      
      zdialog_fetch(zd,"browsefile",browsefile,500);                             //  initial browse location
      if (! *browsefile && topfolders[0]) 
         strncpy0(browsefile,topfolders[0],500);

      if (strmatch(startdisplay,"specG")) {
         pp = zgetfile(E2X("Select startup folder"),MWIN,"folder",browsefile);
         if (! pp) return 1;
         zdialog_stuff(zd,"browsefile",pp);
         zfree(pp);
      }

      if (strmatch(startdisplay,"specF")) {
         pp = zgetfile(E2X("Select startup image file"),MWIN,"file",browsefile);
         if (! pp) return 1;
         zdialog_stuff(zd,"browsefile",pp);
         zfree(pp);
      }
      
      if (strmatch(startdisplay,"album")) {                                      //  choose album
         pp = zgetfile(E2X("Select startup album"),MWIN,"file",albums_folder);
         if (! pp) return 1;
         zdialog_stuff(zd,"browsefile",pp);
         zfree(pp);
      }
   }
   
   if (strmatch(event,"choosefont"))                                             //  choose menu/dialog font
   {
      zdialog_fetch(zd,"font",temp,200);
      font_dialog = gtk_font_chooser_dialog_new(E2X("select font"),MWIN);
      gtk_font_chooser_set_font(GTK_FONT_CHOOSER(font_dialog),temp);
      gtk_dialog_run(GTK_DIALOG(font_dialog));
      pp = gtk_font_chooser_get_font(GTK_FONT_CHOOSER(font_dialog));
      gtk_widget_destroy(font_dialog);
      if (! pp) return 1;
      zdialog_stuff(zd,"font",pp);
      zsetfont(pp);
      dialog_font = zstrdup(pp);
      g_free(pp);
   }
   
   if (zd->zstat != 1) return 1;                                                 //  wait for dialog completion

   zdialog_fetch(zd,"startopt",temp,200);                                        //  get startup option
   for (ii = 0; ii < 8; ii++) {
      if (strmatch(temp,startopt[ii][1])) {
         zfree(startdisplay);
         startdisplay = zstrdup(startopt[ii][0]);
      }
   }
   
   if (strmatch(startdisplay,"album")) {                                         //  startup option = album 
      zdialog_fetch(zd,"browsefile",browsefile,500);                             //  get startup album
      if (startalbum) zfree(startalbum);
      startalbum = zstrdup(browsefile);
   }

   if (strmatch(startdisplay,"specG")) {                                         //  startup option = folder gallery
      zdialog_fetch(zd,"browsefile",browsefile,500);                             //  get startup folder
      if (startfolder) zfree(startfolder);
      startfolder = zstrdup(browsefile);
      if (image_file_type(startfolder) != FDIR) {
         zmessageACK(Mwin,E2X("startup folder is invalid"));
         zd->zstat = 0;
         return 1;
      }
   }

   if (strmatch(startdisplay,"specF")) {                                         //  startup option = image file
      zdialog_fetch(zd,"browsefile",browsefile,500);                             //  get startup file
      if (startfile) zfree(startfile);
      startfile = zstrdup(browsefile);
      if (image_file_type(startfile) != IMAGE) {
         zmessageACK(Mwin,E2X("startup file is invalid"));
         zd->zstat = 0;
         return 1;
      }
   }

   zdialog_fetch(zd,"icons",nn);                                                 //  menu type = icons
   if (nn) {
      if (! strmatch(menu_style,"icons")) {
         zfree(menu_style);
         menu_style = zstrdup("icons");
      }
   }

   zdialog_fetch(zd,"text",nn);                                                  //  menu type = text                   18.07
   if (nn) {
      if (! strmatch(menu_style,"text")) {
         zfree(menu_style);
         menu_style = zstrdup("text");
      }
   }

   zdialog_fetch(zd,"both",nn);                                                  //  menu type = icons + text
   if (nn) {
      if (! strmatch(menu_style,"both")) {
         zfree(menu_style);
         menu_style = zstrdup("both");
      }
   }
   
   zdialog_fetch(zd,"iconsize",nn);                                              //  icon size
   if (nn != iconsize) {
      iconsize = nn;
   }
   
   zdialog_fetch(zd,"FBrgb",txrgb,20);                                           //  F-view background color
   ppc = strField(txrgb,"|",1);
   if (ppc) FBrgb[0] = atoi(ppc);
   ppc = strField(txrgb,"|",2);
   if (ppc) FBrgb[1] = atoi(ppc);
   ppc = strField(txrgb,"|",3);
   if (ppc) FBrgb[2] = atoi(ppc);

   zdialog_fetch(zd,"GBrgb",txrgb,20);                                           //  G-view background color
   ppc = strField(txrgb,"|",1);
   if (ppc) GBrgb[0] = atoi(ppc);
   ppc = strField(txrgb,"|",2);
   if (ppc) GBrgb[1] = atoi(ppc);
   ppc = strField(txrgb,"|",3);
   if (ppc) GBrgb[2] = atoi(ppc);
   
   zdialog_fetch(zd,"MFrgb",txrgb,20);                                           //  menu font color                    18.01
   ppc = strField(txrgb,"|",1);
   if (ppc) MFrgb[0] = atoi(ppc);
   ppc = strField(txrgb,"|",2);
   if (ppc) MFrgb[1] = atoi(ppc);
   ppc = strField(txrgb,"|",3);
   if (ppc) MFrgb[2] = atoi(ppc);

   zdialog_fetch(zd,"MBrgb",txrgb,20);                                           //  menu background color              18.01
   ppc = strField(txrgb,"|",1);
   if (ppc) MBrgb[0] = atoi(ppc);
   ppc = strField(txrgb,"|",2);
   if (ppc) MBrgb[1] = atoi(ppc);
   ppc = strField(txrgb,"|",3);
   if (ppc) MBrgb[2] = atoi(ppc);
   
   zdialog_fetch(zd,"drag",nn);                                                  //  drag/scroll/zoom options
   if (nn) Fdragopt = 1;                                                         //  1/2 = drag/scroll
   else Fdragopt = 2;
   zdialog_fetch(zd,"fast",nn);                                                  //  3/4 = drag/scroll fast
   if (nn) Fdragopt += 2;

   zdialog_fetch(zd,"zoomcount",zoomcount);                                      //  zooms for 2x image size
   zoomratio = pow( 2.0, 1.0 / zoomcount);                                       //  2.0, 1.4142, 1.2599, 1.1892 ...
   
   zdialog_fetch(zd,"quality",jpeg_def_quality);                                 //  default jpeg file save quality
   zdialog_fetch(zd,"nodecap",splcurve_minx);                                    //  edit curve min. node distance
   zdialog_fetch(zd,"map_dotsize",map_dotsize);                                  //  map dot size
   zdialog_fetch(zd,"lastver",Flastversion);                                     //  prev/next shows last version only
   zdialog_fetch(zd,"shiftright",Fshiftright);                                   //  shift image to right margin
   zdialog_fetch(zd,"indexlev",Findexlev);                                       //  index level, always
   zdialog_fetch(zd,"fmindexlev",FMindexlev);                                    //  index level, started from FM

   zdialog_fetch(zd,"rawtypes",temp,200);                                        //  RAW file types, .raw .rw2 ...
   pp = zstrdup(temp,100);

   for (ii = jj = 0; temp[ii]; ii++) {                                           //  insure blanks between types
      if (temp[ii] == '.' && temp[ii-1] != ' ') pp[jj++] = ' ';
      pp[jj++] = temp[ii];
   }
   if (pp[jj-1] != ' ') pp[jj++] = ' ';                                          //  insure 1 final blank
   pp[jj] = 0;

   if (RAWfiletypes) zfree(RAWfiletypes);
   RAWfiletypes = pp;

   zdialog_fetch(zd,"videotypes",temp,200);                                      //  VIDEO file types, .mp4 .mov ...
   pp = zstrdup(temp,100);

   for (ii = jj = 0; temp[ii]; ii++) {                                           //  insure blanks between types
      if (temp[ii] == '.' && temp[ii-1] != ' ') pp[jj++] = ' ';
      pp[jj++] = temp[ii];
   }
   if (pp[jj-1] != ' ') pp[jj++] = ' ';                                          //  insure 1 final blank
   pp[jj] = 0;

   if (VIDEOfiletypes) zfree(VIDEOfiletypes);
   VIDEOfiletypes = pp;

   save_params();                                                                //  done, save modified parameters
   Fpaint2();
   return 1;
}


/********************************************************************************/

//  keyboard shortcuts

namespace KBshortcutnames
{
   zdialog     *zd;

   int         Nreserved = 13;                                                   //  reserved shortcuts (hard coded)    18.01
   cchar       *reserved[13] = { 
      "F1", "F10", "F11", "Escape", "Delete", "Left", "Right", 
      "Up", "Down", "First", "Last", "Page_Up", "Page_Down" 
   };

   kbsutab_t   kbsutab2[maxkbsu];                                                //  KB shortcuts list during editing
   int         Nkbsu2;
}


//  KB shortcuts menu function

void m_KBshortcuts(GtkWidget *, cchar *)
{
   using namespace KBshortcutnames;
   
   int  KBshorts_dialog_event(zdialog *zd, cchar *event);
   void KBshortcuts_edit();

   int         zstat, ii;
   GtkWidget   *widget;

   F1_help_topic = "KB_shortcuts";

   zd = zdialog_new(E2X("Keyboard Shortcuts"),Mwin,Bedit,Bcancel,null);
   zdialog_add_widget(zd,"scrwin","scrlist","dialog",0,"space=5|expand");
   zdialog_add_widget(zd,"text","shortlist","scrlist",0,"expand");

   widget = zdialog_widget(zd,"shortlist");                                      //  list fixed shortcuts

   textwidget_append(widget,1,E2X("Reserved Shortcuts \n"));
   textwidget_append(widget,0,E2X(" F1       User Guide for current function \n"));
   textwidget_append(widget,0,E2X(" F10      Full Screen with menus \n"));
   textwidget_append(widget,0,E2X(" F11      Full Screen without menus \n"));
   textwidget_append(widget,0,E2X(" Escape   Quit dialog; Quit Fotoxx \n"));
   textwidget_append(widget,0,E2X(" Delete   Delete/Trash dialog \n"));
   textwidget_append(widget,0,E2X(" Arrows   Previous/Next Image \n"));

   textwidget_append(widget,0,"\n");
   textwidget_append(widget,1,"Custom Shortcuts \n");

   for (ii = 0; ii < Nkbsu; ii++)                                                //  list custom shortcuts              18.07
      textwidget_append(widget,0," %-14s %s \n",
                     kbsutab[ii].key, E2X(kbsutab[ii].menu));

   zdialog_resize(zd,400,600);
   zdialog_run(zd,KBshorts_dialog_event,"save");
   zstat = zdialog_wait(zd);
   zdialog_free(zd);
   if (zstat == 1) KBshortcuts_edit();
   return;
}


//  dialog event and completion function

int KBshorts_dialog_event(zdialog *zd, cchar *event)
{
   if (zd->zstat) zdialog_destroy(zd);
   return 1;
}


//  KB shortcuts edit function 

void KBshortcuts_edit()
{
   using namespace KBshortcutnames;

   void KBshorts_callbackfunc1(GtkWidget *widget, int line, int pos, int kbkey);
   void KBshorts_callbackfunc2(GtkWidget *widget, int line, int pos, int kbkey);
   int  KBshorts_keyfunc(GtkWidget *dialog, GdkEventKey *event);
   int  KBshorts_edit_dialog_event(zdialog *zd, cchar *event);

   int         ii;
   GtkWidget   *widget;
   char        *sortlist[maxkbsf];

/***
          _________________________________________________________________
         |              Edit KB Shortcuts                                  |
         |_________________________________________________________________|
         | Alt+G           Grid Lines              |  Blur                 |
         | Alt+U           Undo                    |  Bookmarks            |
         | Alt+R           Redo                    |  Captions             |
         | T               Trim/Rotate             |  Color Depth          |
         | V               View Main               |  Color Sat            |
         | Ctrl+Shift+V    View All                |  Copy to Cache        |
         | ...             ...                     |  Copy to Clipboard    |
         | Done            dialog button           |  Current Album        |     //  dialog button KB shortcut
         | ...             ...                     |  ...                  |
         |_________________________________________|_______________________|
         |                                                                 |
         | shortcut key: (enter key)  (no selection)                       |
         |                                                                 |
         |                                  [add] [remove] [done] [cancel] |
         |_________________________________________________________________|

***/

   zd = zdialog_new(E2X("Edit KB Shortcuts"),Mwin,Badd,Bdelete,Bdone,Bcancel,null);
   zdialog_add_widget(zd,"hbox","hblists","dialog",0,"expand");
   zdialog_add_widget(zd,"scrwin","scrlist","hblists",0,"space=5|expand");
   zdialog_add_widget(zd,"text","shortlist","scrlist",0,"expand");
   zdialog_add_widget(zd,"scrwin","scrmenus","hblists",0,"expand");
   zdialog_add_widget(zd,"text","menufuncs","scrmenus");
   zdialog_add_widget(zd,"hbox","hbshort","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labshort","hbshort",E2X("shortcut key:"),"space=5");
   zdialog_add_widget(zd,"label","shortkey","hbshort",E2X("(enter key)"),"size=10");
   zdialog_add_widget(zd,"label","shortfunc","hbshort",E2X("(no selection)"),"space=5");
   
   for (ii = 0; ii < Nkbsu; ii++) {                                              //  copy current shortcuts list
      kbsutab2[ii].key = zstrdup(kbsutab[ii].key);
      kbsutab2[ii].menu = zstrdup(kbsutab[ii].menu);
   }
   Nkbsu2 = Nkbsu;

   widget = zdialog_widget(zd,"shortlist");                                      //  show shortcuts list in dialog,
   textwidget_clear(widget);                                                     //    translated
   for (ii = 0; ii < Nkbsu2; ii++)
      textwidget_append(widget,0,"%-14s %s \n",
                           kbsutab2[ii].key, E2X(kbsutab2[ii].menu));

   textwidget_set_callbackfunc(widget,KBshorts_callbackfunc1);                   //  callback func for mouse clicks

   for (ii = 0; ii < Nkbsf; ii++)                                                //  copy eligible shortcut funcs,
      sortlist[ii] = zstrdup(E2X(kbsftab[ii].menu));                             //    translated

   HeapSort(sortlist,Nkbsf);                                                     //  sort copied list

   widget = zdialog_widget(zd,"menufuncs");                                      //  clear dialog
   textwidget_clear(widget);

   for (ii = 0; ii < Nkbsf; ii++)                                                //  show sorted translated funcs list
      textwidget_append(widget,0,"%s\n",sortlist[ii]);

   textwidget_set_callbackfunc(widget,KBshorts_callbackfunc2);                   //  callback func for mouse clicks

   widget = zdialog_widget(zd,"dialog");                                         //  capture KB keys pressed
   G_SIGNAL(widget,"key-press-event",KBshorts_keyfunc,0);

   zdialog_resize(zd,600,400);
   zdialog_run(zd,KBshorts_edit_dialog_event,"save");

   return;
}


//  mouse callback function to select existing shortcut from list
//    and stuff into dialog "shortfunc"

void KBshorts_callbackfunc1(GtkWidget *widget, int line, int pos, int kbkey)
{
   using namespace KBshortcutnames;

   char     *txline;
   char     shortkey[20];
   char     shortfunc[60];

   if (kbkey == GDK_KEY_F1) {                                                    //  key F1 pressed, show help
      showz_userguide(F1_help_topic);
      return;
   }

   txline = textwidget_line(widget,line,1);                                      //  get clicked line
   if (! txline || ! *txline) return;
   textwidget_highlight_line(widget,line);

   strncpy0(shortkey,txline,14);                                                 //  get shortcut key and menu
   strncpy0(shortfunc,txline+15,60);
   zfree(txline);

   strTrim(shortkey);
   strTrim(shortfunc);

   zdialog_stuff(zd,"shortkey",shortkey);                                        //  stuff into dialog
   zdialog_stuff(zd,"shortfunc",shortfunc);

   return;
}


//  mouse callback function to select new shortcut function from menu list
//    and stuff into dialog "shortfunc"

void KBshorts_callbackfunc2(GtkWidget *widget, int line, int pos, int kbkey)
{
   using namespace KBshortcutnames;
   
   char     *txline;
   
   if (kbkey == GDK_KEY_F1) {                                                    //  key F1 pressed, show help
      showz_userguide(F1_help_topic);
      return;
   }
   
   txline = textwidget_line(widget,line,1);                                      //  get clicked line
   if (! txline || ! *txline) return;
   textwidget_highlight_line(widget,line);

   zdialog_stuff(zd,"shortfunc",txline);                                         //  stuff into dialog
   zfree(txline);
   return;
}


//  intercept KB key events, stuff into dialog "shortkey"

int KBshorts_keyfunc(GtkWidget *dialog, GdkEventKey *event)
{
   using namespace KBshortcutnames;

   int      Ctrl = 0, Alt = 0, Shift = 0;
   int      key, ii, cc;
   char     keyname[20];

   key = event->keyval;

   if (event->state & GDK_CONTROL_MASK) Ctrl = 1;
   if (event->state & GDK_SHIFT_MASK) Shift = 1;
   if (event->state & GDK_MOD1_MASK) Alt = 1;
   
   if (key == GDK_KEY_Escape) return 0;                                          //  pass escape (cancel) to zdialog
   
   if (key == GDK_KEY_F1) {                                                      //  key is F1 (context help)
      KBevent(event);                                                            //  send to main app
      return 1;
   }

   if (key >= GDK_KEY_F2 && key <= GDK_KEY_F9) {                                 //  key is F2 to F9
      ii = key - GDK_KEY_F1;
      strcpy(keyname,"F1");
      keyname[1] += ii;
   }

   else if (key > 255) return 1;                                                 //  not a simple Ascii key

   else {
      *keyname = 0;                                                              //  build input key combination
      if (Ctrl) strcat(keyname,"Ctrl+");                                         //  [Ctrl+] [Alt+] [Shift+] key
      if (Alt) strcat(keyname,"Alt+");
      if (Shift) strcat(keyname,"Shift+");
      cc = strlen(keyname);
      keyname[cc] = toupper(key);                                                //  x --> X, Ctrl+x --> Ctrl+X
      keyname[cc+1] = 0;
   }

   for (ii = 0; ii < Nreserved; ii++)
      if (strmatch(keyname,reserved[ii])) break;
   if (ii < Nreserved) {
      zmessageACK(Mwin,E2X("\"%s\"  Reserved, cannot be used"),keyname);
      Ctrl = Alt = 0;
      return 1;
   }

   zdialog_stuff(zd,"shortkey",keyname);                                         //  stuff key name into dialog
   zdialog_stuff(zd,"shortfunc",E2X("(no selection)"));                          //  clear menu choice

   return 1;
}


//  dialog event and completion function

int KBshorts_edit_dialog_event(zdialog *zd, cchar *event)
{
   using namespace KBshortcutnames;

   int  KBshorts_edit_menufuncs_event(zdialog *zd, cchar *event);
   void KBshorts_callbackfunc2(GtkWidget *widget, int line, int pos, int kbkey);
   
   int         ii, jj;
   GtkWidget   *widget;
   char        shortkey[20];
   char        shortfunc[60];
   char        kbfile[200];
   FILE        *fid = 0;

   if (! zd->zstat) return 1;                                                    //  wait for completion
   
   if (zd->zstat == 1)                                                           //  add shortcut
   {
      zd->zstat = 0;                                                             //  keep dialog active

      if (Nkbsu2 == maxkbsu) {
         zmessageACK(Mwin,"exceed %d shortcuts",maxkbsu);
         return 1;
      }

      zdialog_fetch(zd,"shortkey",shortkey,20);                                  //  get shortcut key and menu
      zdialog_fetch(zd,"shortfunc",shortfunc,60);                                //    from dialog widgets
      if (*shortkey <= ' ' || *shortfunc <= ' ') return 0;

      for (ii = 0; ii < Nkbsu2; ii++)                                            //  find matching shortcut key in list
         if (strmatch(kbsutab2[ii].key,shortkey)) break;

      if (ii < Nkbsu2) {                                                         //  if found, remove from list
         zfree(kbsutab2[ii].key);
         zfree(kbsutab2[ii].menu);
         for (jj = ii; jj < Nkbsu2; jj++)
            kbsutab2[jj] = kbsutab2[jj+1];
         --Nkbsu2;
      }

      for (ii = 0; ii < Nkbsu2; ii++)                                            //  find matching shortcut func in list
         if (strmatch(shortfunc,E2X(kbsutab2[ii].menu))) break;

      if (ii < Nkbsu2) {                                                         //  if found, remove from list
         zfree(kbsutab2[ii].key);
         zfree(kbsutab2[ii].menu);
         for (jj = ii; jj < Nkbsu2; jj++)
            kbsutab2[jj] = kbsutab2[jj+1];
         --Nkbsu2;
      }
      
      for (ii = 0; ii < Nkbsf; ii++)                                             //  look up shortcut func in list
         if (strmatch(shortfunc,E2X(kbsftab[ii].menu))) break;                   //    of eligible menu funcs
      if (ii == Nkbsf) return 1;
      strncpy0(shortfunc,kbsftab[ii].menu,60);                                   //  english menu func

      ii = Nkbsu2++;                                                             //  add new shortcut to end of list
      kbsutab2[ii].key = zstrdup(shortkey);
      kbsutab2[ii].menu = zstrdup(shortfunc);

      widget = zdialog_widget(zd,"shortlist");                                   //  clear shortcuts list
      textwidget_clear(widget);

      for (ii = 0; ii < Nkbsu2; ii++)                                            //  show updated shortcuts in dialog
         textwidget_append2(widget,0,"%-14s %s \n",
                              kbsutab2[ii].key,E2X(kbsutab2[ii].menu));
      return 1;
   }
   
   if (zd->zstat == 2)                                                           //  remove shortcut
   {
      zd->zstat = 0;                                                             //  keep dialog active

      zdialog_fetch(zd,"shortkey",shortkey,20);                                  //  get shortcut key
      if (*shortkey <= ' ') return 0;

      for (ii = 0; ii < Nkbsu2; ii++)                                            //  find matching shortcut key in list
         if (strmatch(kbsutab2[ii].key,shortkey)) break;

      if (ii < Nkbsu2) {                                                         //  if found, remove from list
         zfree(kbsutab2[ii].key);
         zfree(kbsutab2[ii].menu);
         for (jj = ii; jj < Nkbsu2; jj++)
            kbsutab2[jj] = kbsutab2[jj+1];
         --Nkbsu2;
      }

      widget = zdialog_widget(zd,"shortlist");                                   //  clear shortcuts list
      textwidget_clear(widget);

      for (ii = 0; ii < Nkbsu2; ii++)                                            //  show updated shortcuts in dialog
         textwidget_append2(widget,0,"%-14s %s \n",
                              kbsutab2[ii].key,E2X(kbsutab2[ii].menu));

      zdialog_stuff(zd,"shortkey","");                                           //  clear entered key and menu
      zdialog_stuff(zd,"shortfunc",E2X("(no selection)"));
      return 1;
   }

   if (zd->zstat == 3)                                                           //  done - save new shortcut list
   {
      zdialog_free(zd);                                                          //  kill menu funcs list

      locale_filespec("user","KB-shortcuts4",kbfile);                            //  update shortcuts file
      fid = fopen(kbfile,"w");
      if (! fid) {
         zmessageACK(Mwin,strerror(errno));
         return 1;
      }
      for (ii = 0; ii < Nkbsu2; ii++)
         fprintf(fid,"%-14s %s \n",kbsutab2[ii].key,kbsutab2[ii].menu);
      fclose(fid);

      KBshortcuts_load();                                                        //  reload shortcuts list from file
      return 1;
   }

   zdialog_free(zd);                                                             //  cancel or [x]
   return 1;
}


//  read KB-shortcuts file and load shortcuts table in memory
//  also called from initzfunc() at fotoxx startup

int KBshortcuts_load()
{
   int         ii, err;
   char        kbfile[200], buff[200];
   char        *pp1, *pp2;
   char        *key, *menu;
   FILE        *fid;

   for (ii = 0; ii < Nkbsu; ii++) {                                              //  clear shortcuts data               18.01
      zfree(kbsutab[ii].key);
      zfree(kbsutab[ii].menu);
   }
   Nkbsu = 0;

   err = locale_filespec("user","KB-shortcuts4",kbfile);
   if (err) return 0;
   fid = fopen(kbfile,"r");
   if (! fid) return 0;
   
   for (ii = 0; ii < maxkbsu; )
   {
      pp1 = fgets_trim(buff,200,fid,1);                                          //  next record
      if (! pp1) break;
      if (*pp1 == '#') continue;                                                 //  comment
      if (*pp1 <= ' ') continue;                                                 //  blank
      pp2 = strchr(pp1,' ');
      if (! pp2) continue;
      if (pp2 - pp1 > 20) continue;
      *pp2 = 0;
      key = zstrdup(pp1);                                                        //  shortcut key or combination
      pp1 = pp2 + 1;
      while (*pp1 && *pp1 == ' ') pp1++;
      if (! *pp1) continue;
      menu = zstrdup(pp1);                                                       //  corresp. menu, English

      if (strstr(zdialog_button_shortcuts,menu))                                 //  if a dialog button shortcut
         zdialog_KB_addshortcut(key,menu);                                       //    register with zdialog

      kbsutab[ii].key = zstrdup(key);                                            //  add to shortcuts table
      kbsutab[ii].menu = zstrdup(menu);
      ii++;
   }

   Nkbsu = ii;

   fclose(fid);
   return 0;
}


/********************************************************************************/

//  show a brightness distribution graph - live update as image is edited

namespace brightgraph_names
{
   GtkWidget   *drawwin_dist, *drawwin_scale;                                    //  brightness distribution graph widgets
   int         RGBW[4] = { 0, 0, 0, 1 };                                         //     "  colors: red/green/blue/white (all)
}


//  menu function

void m_brightgraph(GtkWidget *, cchar *menu)                                     //  menu function
{
   using namespace brightgraph_names;

   int  show_brightgraph_dialog_event(zdialog *zd, cchar *event);

   GtkWidget   *frdist, *frscale, *widget;
   zdialog     *zd;

   m_viewmode(0,"F");                                                            //  file view mode                     19.0
   if (! curr_file) return;

   if (menu && strmatch(menu,"kill")) {
      if (zd_brightgraph) zdialog_free(zd_brightgraph);                          //  bugfix  18.01
      zd_brightgraph = 0;
      return;
   }

   if (zd_brightgraph) {                                                         //  dialog already present
      gtk_widget_queue_draw(drawwin_dist);                                       //  refresh drawing windows
      return;
   }

   if (menu) F1_help_topic = "brightness_graph";

   zd = zdialog_new(E2X("Brightness Distribution"),Mwin,null);
   zdialog_add_widget(zd,"frame","frdist","dialog",0,"expand");                  //  frames for 2 drawing areas
   zdialog_add_widget(zd,"frame","frscale","dialog");
   frdist = zdialog_widget(zd,"frdist");
   frscale = zdialog_widget(zd,"frscale");

   drawwin_dist = gtk_drawing_area_new();                                        //  histogram drawing area
   gtk_container_add(GTK_CONTAINER(frdist),drawwin_dist);
   G_SIGNAL(drawwin_dist,"draw",brightgraph_graph,RGBW);

   drawwin_scale = gtk_drawing_area_new();                                       //  brightness scale under histogram
   gtk_container_add(GTK_CONTAINER(frscale),drawwin_scale);
   gtk_widget_set_size_request(drawwin_scale,300,12);
   G_SIGNAL(drawwin_scale,"draw",brightgraph_scale,0);

   zdialog_add_widget(zd,"hbox","hbcolors","dialog");
   zdialog_add_widget(zd,"check","all","hbcolors",Ball,"space=5");
   zdialog_add_widget(zd,"check","red","hbcolors",Bred,"space=5");
   zdialog_add_widget(zd,"check","green","hbcolors",Bgreen,"space=5");
   zdialog_add_widget(zd,"check","blue","hbcolors",Bblue,"space=5");

   zdialog_stuff(zd,"red",RGBW[0]);
   zdialog_stuff(zd,"green",RGBW[1]);
   zdialog_stuff(zd,"blue",RGBW[2]);
   zdialog_stuff(zd,"all",RGBW[3]);

   zdialog_resize(zd,300,250);
   zdialog_run(zd,show_brightgraph_dialog_event,"save");

   widget = zdialog_widget(zd,"dialog");                                         //  stop focus on this window
   gtk_window_set_accept_focus(GTK_WINDOW(widget),0);

   zd_brightgraph = zd;
   return;
}


//  dialog event and completion function

int show_brightgraph_dialog_event(zdialog *zd, cchar *event)
{
   using namespace brightgraph_names;

   if (zd->zstat) {
      zdialog_free(zd);
      zd_brightgraph = 0;
      return 1;
   }

   if (strstr("all red green blue",event)) {                                     //  update chosen colors
      zdialog_fetch(zd,"red",RGBW[0]);
      zdialog_fetch(zd,"green",RGBW[1]);
      zdialog_fetch(zd,"blue",RGBW[2]);
      zdialog_fetch(zd,"all",RGBW[3]);
      gtk_widget_queue_draw(drawwin_dist);                                       //  refresh drawing window
   }

   return 1;
}


//  draw brightness distribution graph in drawing window 

void brightgraph_graph(GtkWidget *drawin, cairo_t *cr, int *rgbw)
{
   int         bin, Nbins = 256, brightgraph[256][4];                            //  bin count, R/G/B/all
   int         px, py, dx, dy;
   int         ww, hh, winww, winhh;
   int         ii, rgb, maxdist, bright;
   uint8       *pixi;

   if (! Fpxb) return;
   if (rgbw[0]+rgbw[1]+rgbw[2]+rgbw[3] == 0) return;

   winww = gtk_widget_get_allocated_width(drawin);                               //  drawing window size
   winhh = gtk_widget_get_allocated_height(drawin);

   for (bin = 0; bin < Nbins; bin++)                                             //  clear brightness distribution
   for (rgb = 0; rgb < 4; rgb++)
      brightgraph[bin][rgb] = 0;

   ww = Fpxb->ww;                                                                //  image area within window
   hh = Fpxb->hh;

   for (ii = 0; ii < ww * hh; ii++)                                              //  overhaul                           18.07
   {
      if (sa_stat == 3 && ! sa_pixmap[ii]) continue;                             //  stay within active select area

      py = ii / ww;                                                              //  image pixel
      px = ii - ww * py;

      dx = Mscale * px - Morgx + Dorgx;                                          //  stay within visible window
      if (dx < 0 || dx > Dww-1) continue;                                        //    for zoomed image
      dy = Mscale * py - Morgy + Dorgy;
      if (dy < 0 || dy > Dhh-1) continue;

      pixi = PXBpix(Fpxb,px,py);                                                 //  use displayed image

      for (rgb = 0; rgb < 3; rgb++) {                                            //  get R/G/B brightness levels
         bright = pixi[rgb] * Nbins / 256.0;                                     //  scale 0 to Nbins-1
         if (bright < 0 || bright > 255) {
            printz("pixel %d/%d: %d %d %d \n",px,py,pixi[0],pixi[1],pixi[2]);
            return;
         }
         ++brightgraph[bright][rgb];
      }

      bright = (pixi[0] + pixi[1] + pixi[2]) * 0.333 * Nbins / 256.0;            //  R+G+B, 0 to Nbins-1
      ++brightgraph[bright][3];
   }

   maxdist = 0;

   for (bin = 1; bin < Nbins-1; bin++)                                           //  find max. bin over all RGB
   for (rgb = 0; rgb < 3; rgb++)                                                 //    omit bins 0 and last
      if (brightgraph[bin][rgb] > maxdist)                                       //      which can be huge
         maxdist = brightgraph[bin][rgb];

   for (rgb = 0; rgb < 4; rgb++)                                                 //  R/G/B/white (all)
   {
      if (! rgbw[rgb]) continue;                                                 //  color not selected for graph
      if (rgb == 0) cairo_set_source_rgb(cr,1,0,0);
      if (rgb == 1) cairo_set_source_rgb(cr,0,1,0);
      if (rgb == 2) cairo_set_source_rgb(cr,0,0,1);
      if (rgb == 3) cairo_set_source_rgb(cr,0,0,0);                              //  color "white" = R+G+B uses black line

      cairo_move_to(cr,0,winhh-1);                                               //  start at (0,0)

      for (px = 0; px < winww; px++)                                             //  x from 0 to window width
      {
         bin = Nbins * px / winww;                                               //  bin = 0-Nbins for x = 0-width
         py = 0.9 * winhh * brightgraph[bin][rgb] / maxdist;                     //  height of bin in window
         py = winhh * sqrt(1.0 * py / winhh);
         py = winhh - py - 1;
         cairo_line_to(cr,px,py);                                                //  draw line from bin to bin
      }

      cairo_stroke(cr);
   }

   return;
}


/********************************************************************************/

//  Paint a horizontal stripe drawing area with a color progressing from
//  black to white. This represents a brightness scale from 0 to 255.

void brightgraph_scale(GtkWidget *drawarea, cairo_t *cr, int *)
{
   int      px, ww, hh;
   float    fbright;

   ww = gtk_widget_get_allocated_width(drawarea);                                //  drawing area size
   hh = gtk_widget_get_allocated_height(drawarea);

   for (px = 0; px < ww; px++)                                                   //  draw brightness scale
   {
      fbright = 1.0 * px / ww;
      cairo_set_source_rgb(cr,fbright,fbright,fbright);
      cairo_move_to(cr,px,0);
      cairo_line_to(cr,px,hh-1);
      cairo_stroke(cr);
   }

   return;
}


/********************************************************************************/

//  magnify image within a given radius of dragged mouse

namespace magnify_names
{
   int   magnify_dialog_event(zdialog* zd, cchar *event);
   void  magnify_mousefunc();
   void  magnify_dopixels(int ftf);

   float       Xmag;                                                             //  magnification, 1 - 5x
   int         Mx, My;                                                           //  mouse location, image space
   int         Mrad;                                                             //  mouse radius, image space
}


//  menu function

void m_magnify(GtkWidget *, cchar *)
{
   using namespace magnify_names;

   F1_help_topic = "magnify";

   cchar *   mess = E2X("Drag mouse on image. \n"
                        "Left click to cancel.");

   /***
             __________________________
            |    Magnify Image         |
            |                          |
            |  Drag mouse on image.    |
            |  Left click to cancel.   |
            |                          |
            |  radius  [_____]         |
            |  X-size  [_____]         |
            |                          |
            |                [cancel]  |
            |__________________________|

   ***/

   m_viewmode(0,"F");                                                            //  file view mode                     19.0
   if (! curr_file) return;

   if (zd_magnify) {                                                             //  toggle magnify mode 
      zdialog_send_event(zd_magnify,"kill");
      return;
   }

   else {
      zdialog *zd = zdialog_new(E2X("Magnify Image"),Mwin,Bcancel,null);
      zd_magnify = zd;

      zdialog_add_widget(zd,"label","labdrag","dialog",mess,"space=5");

      zdialog_add_widget(zd,"hbox","hbr","dialog",0,"space=3");
      zdialog_add_widget(zd,"label","labr","hbr",Bradius,"space=5");
      zdialog_add_widget(zd,"zspin","Mrad","hbr","100|500|10|200");
      zdialog_add_widget(zd,"hbox","hbx","dialog",0,"space=3");
      zdialog_add_widget(zd,"label","labx","hbx",E2X("X-size"),"space=5");
      zdialog_add_widget(zd,"zspin","Xmag","hbx","1.5|5|0.1|2");

      zdialog_fetch(zd,"Mrad",Mrad);                                             //  initial mouse radius
      zdialog_fetch(zd,"Xmag",Xmag);                                             //  initial magnification

      zdialog_resize(zd,200,0);
      zdialog_restore_inputs(zd);                                                //  preload prior user inputs
      zdialog_run(zd,magnify_dialog_event,"save");                               //  run dialog, parallel

      zdialog_send_event(zd,"Mrad");                                             //  initializations
      zdialog_send_event(zd,"Xmag");
   }

   takeMouse(magnify_mousefunc,dragcursor);                                      //  connect mouse function
   return;
}


//  dialog event and completion callback function

int magnify_names::magnify_dialog_event(zdialog *zd, cchar *event)
{
   using namespace magnify_names;
   
   if (strmatch(event,"kill")) zd->zstat = 1;                                    //  from slide show 

   if (zd->zstat) {                                                              //  terminate
      zd_magnify = 0;
      zdialog_free(zd);
      freeMouse();
      return 1;
   }

   if (strmatch(event,"focus"))                                                  //  toggle mouse capture
      takeMouse(magnify_mousefunc,dragcursor);

   if (strmatch(event,"Mrad")) {
      zdialog_fetch(zd,"Mrad",Mrad);                                             //  new mouse radius
      return 1;
   }

   if (strmatch(event,"Xmag")) {
      zdialog_fetch(zd,"Xmag",Xmag);                                             //  new magnification
      return 1;
   }

   return 1;
}


//  pixel paint mouse function

void magnify_names::magnify_mousefunc()
{
   using namespace magnify_names;

   static int     ftf = 1;
   
   if (! curr_file) return;
   if (FGWM != 'F') return;
   if (! zd_magnify) return;
   
   if (Mxdown) Fpaint2();                                                        //  drag start, erase prior if any
   Mxdown = 0;

   if (Mxdrag || Mydrag)                                                         //  drag in progress
   {
      Mx = Mxdrag;                                                               //  save mouse position
      My = Mydrag;
      Mxdrag = Mydrag = 0;
      magnify_dopixels(ftf);                                                     //  magnify pixels inside mouse
      gdk_window_set_cursor(gdkwin,blankcursor);
      ftf = 0;
   }

   else
   {
      if (! ftf) Fpaint2();                                                      //  refresh image
      ftf = 1;
      gdk_window_set_cursor(gdkwin,dragcursor);
   }

   return;
}


//  Get pixels from mouse circle within full size image Fpxb,
//    magnify and move into Mpxb, paint. 

void magnify_names::magnify_dopixels(int ftf)
{
   using namespace magnify_names;
   
   int         Fx, Fy, Fww, Fhh;                                                 //  mouse circle, enclosing box, Fpxb
   static int  pFx, pFy, pFww, pFhh;
   PIXBUF      *pxb1, *pxb2, *pxbx;
   int         ww1, hh1, rs1, ww2, hh2, rs2;
   uint8       *pixels1, *pixels2, *pix1, *pix2;
   int         nch = Fpxb->nc;
   int         px1, py1, px2, py2;
   int         xlo, xhi, ylo, yhi;
   int         xc, yc, dx, dy;
   float       R2, R2lim;
   cairo_t     *cr;

   //  get box enclosing PRIOR mouse circle, restore those pixels
   
   if (! ftf)                                                                    //  continuation of mouse drag
   {
      pxb1 = gdk_pixbuf_new_subpixbuf(Fpxb->pixbuf,pFx,pFy,pFww,pFhh);           //  unmagnified pixels, Fpxb
      if (! pxb1) return;

      ww1 = pFww * Mscale;                                                       //  scale to Mpxb
      hh1 = pFhh * Mscale;
      pxbx = gdk_pixbuf_scale_simple(pxb1,ww1,hh1,BILINEAR);
      g_object_unref(pxb1);
      pxb1 = pxbx;
      
      px1 = pFx * Mscale;                                                        //  copy into Mpxb
      py1 = pFy * Mscale;
      gdk_pixbuf_copy_area(pxb1,0,0,ww1,hh1,Mpxb->pixbuf,px1,py1);

      g_object_unref(pxb1);
   }

   //  get box enclosing current mouse circle in Fpxb

   Fx = Mx - Mrad;                                                               //  mouse circle, enclosing box
   Fy = My - Mrad;                                                               //  (Fpxb, 1x image)
   Fww = Fhh = Mrad * 2;
   
   //  clip current mouse box to keep within image

   if (Fx < 0) {
      Fww += Fx;
      Fx = 0;
   }
   
   if (Fy < 0) {
      Fhh += Fy;
      Fy = 0;
   }
   
   if (Fx + Fww > Fpxb->ww) 
      Fww = Fpxb->ww - Fx;

   if (Fy + Fhh > Fpxb->hh)
      Fhh = Fpxb->hh - Fy;
      
   if (Fww <= 0 || Fhh <= 0) return;
   
   pFx = Fx;                                                                     //  save this box for next time
   pFy = Fy;
   pFww = Fww;
   pFhh = Fhh;

   //  scale box for Mpxb, then magnify by Xmag
   
   pxb1 = gdk_pixbuf_new_subpixbuf(Fpxb->pixbuf,Fx,Fy,Fww,Fhh);
   if (! pxb1) return;
   
   ww1 = Fww * Mscale;
   hh1 = Fhh * Mscale;
   pxbx = gdk_pixbuf_scale_simple(pxb1,ww1,hh1,BILINEAR);
   g_object_unref(pxb1);
   pxb1 = pxbx;                                                                  //  unmagnified pixels scaled to Mpxb
   rs1 = gdk_pixbuf_get_rowstride(pxb1);
   pixels1 = gdk_pixbuf_get_pixels(pxb1);
   
   ww2 = ww1 * Xmag;
   hh2 = hh1 * Xmag;
   pxb2 = gdk_pixbuf_scale_simple(pxb1,ww2,hh2,BILINEAR);                        //  corresp. magnified pixels
   rs2 = gdk_pixbuf_get_rowstride(pxb2);
   pixels2 = gdk_pixbuf_get_pixels(pxb2);
   
   //  copy magnified pixels within mouse radius only
   
   xlo = (ww2 - ww1) / 2;                                                        //  pxb2 overlap area with pxb1
   xhi = ww2 - xlo;
   ylo = (hh2 - hh1) / 2;
   yhi = hh2 - ylo;

   xc = (Mx - Fx) * Mscale;                                                      //  mouse center in pxb1
   yc = (My - Fy) * Mscale;
   R2lim = Mrad * Mscale;                                                        //  mouse radius in pxb1
   
   for (py2 = ylo; py2 < yhi; py2++)                                             //  loop pxb2 pixels
   for (px2 = xlo; px2 < xhi; px2++)
   {
      px1 = px2 - xlo;                                                           //  corresp. pxb1 pixel
      py1 = py2 - ylo;
      if (px1 < 0 || px1 >= ww1) continue;
      if (py1 < 0 || py1 >= hh1) continue;
      dx = px1 - xc;
      dy = py1 - yc;
      R2 = sqrtf(dx * dx + dy * dy);
      if (R2 > R2lim) continue;                                                  //  outside mouse radius
      pix1 = pixels1 + py1 * rs1 + px1 * nch;
      pix2 = pixels2 + py2 * rs2 + px2 * nch;
      memcpy(pix1,pix2,nch);
   }

   px1 = Fx * Mscale;                                                            //  copy into Mpxb
   py1 = Fy * Mscale;
   gdk_pixbuf_copy_area(pxb1,0,0,ww1,hh1,Mpxb->pixbuf,px1,py1);

   g_object_unref(pxb1);
   g_object_unref(pxb2);

   cr = draw_context_create(gdkwin,draw_context);
   if (! cr) return;

   Fpaint4(Fx,Fy,Fww,Fhh,cr);

   draw_mousecircle(Mx,My,Mrad,0,cr);

   draw_context_destroy(draw_context);

   return;
}


/********************************************************************************/

//  find all duplicated files and create corresponding gallery of duplicates

namespace duplicates_names
{
   int         thumbsize;
   int         Nfiles;
   char        **files;
   int         Fallfiles, Fgallery;
}


//  menu function

void m_duplicates(GtkWidget *, const char *)                                     //  18.07
{
   using namespace duplicates_names;

   int  duplicates_dialog_event(zdialog *zd, cchar *event);
   void randomize();

   PIXBUF      *pxb;
   GError      *gerror;
   uint8       *pixels, *pix1;
   uint8       *pixelsii, *pixelsjj, *pixii, *pixjj;
   FILE        *fid = 0;
   zdialog     *zd;
   int         Ndups = 0;                                                        //  image file and duplicate counts
   int         thumbsize, pixdiff, pixcount;
   int         zstat, ii, jj, kk, cc, yn, err, ndup;
   int         ww, hh, rs, px, py;
   int         trgb, diff, sumdiff;
   int         percent;
   char        text[100], *pp;
   char        tempfile[200], albumfile[200];

   typedef struct                                                                //  thumbnail data
   {
      int      trgb;                                                             //  mean RGB sum
      int      ww, hh, rs;                                                       //  pixel dimensions
      char     *file;                                                            //  file name
      uint8    *pixels;                                                          //  image pixels
   }  Tdata_t;

   Tdata_t     **Tdata = 0;

   F1_help_topic = "find_duplicates";
   if (checkpend("all")) return;                                                 //  check nothing pending
   m_viewmode(0,"G");                                                            //  gallery view mode                  19.0

   if (Findexvalid == 0) {
      yn = zmessageYN(Mwin,Bnoindex);                                            //  no image index, offer to enable    18.07
      if (yn) index_rebuild(2,0);
      else return;
   }

   free_resources();
   Fblock = 1;
   
   //   randomize();          1-shot


/***
       _______________________________________________
      |         Find Duplicate Images                 |
      |                                               |
      | (o) all files   (o) current gallery           |
      | File count: nnnn                              |
      |                                               |
      | thumbnail size [ 64 ]  [calculate]            |
      | pixel difference [ 2 ]  pixel count [ 2 ]     |
      |                                               |
      | Thumbnails: nnnnnn nn%   Duplicates: nnn nn%  |
      |                                               |
      | /topfolder/subfolder1/subfolder2/...                |
      | imagefile.jpg                                 |
      |                                               |
      |                            [proceed] [cancel] |
      |_______________________________________________|

***/

   zd = zdialog_new(E2X("Find Duplicate Images"),Mwin,Bproceed,Bcancel,null);
   
   zdialog_add_widget(zd,"hbox","hbwhere","dialog",0,"space=5");
   zdialog_add_widget(zd,"radio","allfiles","hbwhere",E2X("All files"),"space=3");
   zdialog_add_widget(zd,"radio","gallery","hbwhere",E2X("Current gallery"),"space=8");
   
   zdialog_add_widget(zd,"hbox","hbfiles","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labfiles","hbfiles",E2X("File count:"),"space=3");
   zdialog_add_widget(zd,"label","filecount","hbfiles","0");
   
   zdialog_add_widget(zd,"hbox","hbthumb","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labthumb","hbthumb",E2X("Thumbnail size"),"space=3");
   zdialog_add_widget(zd,"zspin","thumbsize","hbthumb","32|512|16|256","space=3");
   zdialog_add_widget(zd,"zbutton","calculate","hbthumb",Bcalculate,"space=5");  

   zdialog_add_widget(zd,"hbox","hbdiff","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labdiff","hbdiff",E2X("Pixel difference"),"space=3");
   zdialog_add_widget(zd,"zspin","pixdiff","hbdiff","1|20|1|1","space=3");
   zdialog_add_widget(zd,"label","space","hbdiff",0,"space=8");
   zdialog_add_widget(zd,"label","labsum","hbdiff",E2X("Pixel count"),"space=3");
   zdialog_add_widget(zd,"zspin","pixcount","hbdiff","1|999|1|1","space=3");

   zdialog_add_widget(zd,"hbox","hbstats","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labthumbs1","hbstats","Thumbnails:","space=3");
   zdialog_add_widget(zd,"label","thumbs","hbstats","0");
   zdialog_add_widget(zd,"label","Tpct","hbstats","0%","space=3");
   zdialog_add_widget(zd,"label","space","hbstats",0,"space=5");
   zdialog_add_widget(zd,"label","labdups1","hbstats",E2X("Duplicates:"),"space=3");
   zdialog_add_widget(zd,"label","dups","hbstats","0");
   zdialog_add_widget(zd,"label","Dpct","hbstats","0%","space=3");

   zdialog_add_widget(zd,"hbox","hbfolder","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","currfolder","hbfolder"," ","space=8");
   zdialog_add_widget(zd,"hbox","hbfile","dialog");
   zdialog_add_widget(zd,"label","currfile","hbfile"," ","space=8");

   zdialog_stuff(zd,"allfiles",1);                                               //  default all files
   Fallfiles = 1;

   files = (char **) zmalloc(maximages * sizeof(char *));

   for (ii = jj = 0; ii < Nxxrec; ii++)                                          //  count all files
   {
      pp = xxrec_tab[ii]->file;
      pp = image2thumbfile(pp);                                                  //  omit those without a thumbnail
      if (! pp) continue;
      files[jj++] = pp;
   }
   
   Nfiles = jj;

   snprintf(text,20,"%d",Nfiles);                                                //  file count >> dialog
   zdialog_stuff(zd,"filecount",text);

   zdialog_run(zd,duplicates_dialog_event,"parent");                             //  run dialog
   zstat = zdialog_wait(zd);                                                     //  wait for user inputs
   if (zstat != 1) goto cleanup;                                                 //  canceled 

   if (Nfiles < 2) {
      zmessageACK(Mwin," <2 images");
      goto cleanup;
   }
   
   zdialog_fetch(zd,"thumbsize",thumbsize);                                      //  thumbnail size to use
   zdialog_fetch(zd,"pixdiff",pixdiff);                                          //  pixel difference threshold
   zdialog_fetch(zd,"pixcount",pixcount);                                        //  pixel count threshold
   
   cc = Nfiles * sizeof(Tdata_t);                                                //  allocate memory for thumbnail data
   Tdata = (Tdata_t **) zmalloc(cc);
   memset(Tdata,0,cc); 
   
   Ffuncbusy = 1;
   
   for (ii = 0; ii < Nfiles; ii++)                                               //  screen out thumbnails not 
   {                                                                             //    matching an image file
      pp = thumb2imagefile(files[ii]);
      if (pp) zfree(pp);
      else {
         zfree(files[ii]);
         files[ii] = 0;
      }
   }

   for (ii = 0; ii < Nfiles; ii++)                                               //  read thumbnails into memory
   {
      if (Fkillfunc) goto cleanup;

      if (! files[ii]) continue;

      Tdata[ii] = (Tdata_t *) zmalloc(sizeof(Tdata_t));
      Tdata[ii]->file = files[ii];                                               //  thumbnail file
      files[ii] = 0;

      kk = thumbsize;
      gerror = 0;                                                                //  read thumbnail >> pixbuf
      pxb = gdk_pixbuf_new_from_file_at_size(Tdata[ii]->file,kk,kk,&gerror);
      if (! pxb) {
         printz("file: %s \n %s",Tdata[ii]->file,gerror->message);
         zfree(Tdata[ii]->file);
         zfree(Tdata[ii]);
         Tdata[ii] = 0;
         continue;
      }

      ww = gdk_pixbuf_get_width(pxb);                                            //  pixbuf dimensions
      hh = gdk_pixbuf_get_height(pxb);
      rs = gdk_pixbuf_get_rowstride(pxb);
      pixels = gdk_pixbuf_get_pixels(pxb);

      Tdata[ii]->ww = ww;                                                        //  thumbnail dimensions
      Tdata[ii]->hh = hh;
      Tdata[ii]->rs = rs;
      cc = rs * hh;
      Tdata[ii]->pixels = (uint8 *) zmalloc(cc);
      memcpy(Tdata[ii]->pixels,pixels,cc);                                       //  thumbnail pixels
      
      trgb = 0;                                                                  //  compute mean RGB sum
      for (py = 0; py < hh; py++)
      for (px = 0; px < ww; px++) {
         pix1 = pixels + py * rs + px * 3;
         trgb += pix1[0] + pix1[1] + pix1[2];
      }
      Tdata[ii]->trgb = trgb / ww / hh;                                          //  thumbnail mean RGB sum

      g_object_unref(pxb);                                                       //  bugfix                             19.0

      snprintf(text,100,"%d",ii);                                                //  stuff thumbs read into dialog
      zdialog_stuff(zd,"thumbs",text);

      percent = 100.0 * ii / Nfiles;                                             //  and percent read
      snprintf(text,20,"%02d %c",percent,'%');
      zdialog_stuff(zd,"Tpct",text);

      zmainloop();                                                               //  keep GTK alive
   }
   
   for (ii = jj = 0; ii < Nfiles; ii++)                                          //  remove empty members of Tdata[]
      if (Tdata[ii]) Tdata[jj++] = Tdata[ii];
   Nfiles = jj;                                                                  //  new count

   for (ii = 0; ii < Nfiles; ii++)                                               //  replace thumbnail filespecs
   {                                                                             //    with corresp. image filespecs
      if (! Tdata[ii]) continue;
      pp = thumb2imagefile(Tdata[ii]->file);
      zfree(Tdata[ii]->file);
      Tdata[ii]->file = pp;
   }
   
   snprintf(tempfile,200,"%s/duplicate_images",temp_folder);                     //  open file for gallery output
   fid = fopen(tempfile,"w");
   if (! fid) goto filerror;
   
   Ndups = 0;                                                                    //  total duplicates

   for (ii = 0; ii < Nfiles; ii++)                                               //  loop all thumbnails ii
   {
      zmainloop();

      if (Fkillfunc) goto cleanup;

      percent = 100.0 * (ii+1) / Nfiles;                                         //  show percent processed
      snprintf(text,20,"%02d %c",percent,'%');
      zdialog_stuff(zd,"Dpct",text);

      if (! Tdata[ii]) continue;                                                 //  removed from list

      pp = strrchr(Tdata[ii]->file,'/');
      if (! pp) continue;
      *pp = 0;
      zdialog_stuff(zd,"currfolder",Tdata[ii]->file);                            //  update folder and file
      zdialog_stuff(zd,"currfile",pp+1);                                         //    in dialog
      *pp = '/';
      
      trgb = Tdata[ii]->trgb;
      ww = Tdata[ii]->ww;
      hh = Tdata[ii]->hh;
      rs = Tdata[ii]->rs;
      pixelsii = Tdata[ii]->pixels;

      ndup = 0;                                                                  //  file duplicates

      for (jj = ii+1; jj < Nfiles; jj++)                                         //  loop all thumbnails jj
      {
         if (! Tdata[jj]) continue;                                              //  removed from list
         
         if (abs(trgb - Tdata[jj]->trgb) > 1) continue;                          //  brightness not matching
         if (ww != Tdata[jj]->ww) continue;                                      //  size not matching
         if (hh != Tdata[jj]->hh) continue;

         pixelsjj = Tdata[jj]->pixels;
         sumdiff = 0;

         for (py = 0; py < hh; py++)
         for (px = 0; px < ww; px++)
         {
            pixii = pixelsii + py * rs + px * 3;
            pixjj = pixelsjj + py * rs + px * 3;

            diff = abs(pixii[0] - pixjj[0]) 
                 + abs(pixii[1] - pixjj[1]) 
                 + abs(pixii[2] - pixjj[2]);
            if (diff < pixdiff) continue;                                        //  pixels match withing threshold

            sumdiff++;                                                           //  count unmatched pixels
            if (sumdiff >= pixcount) {                                           //  if over threshhold,
               py = hh; px = ww; }                                               //    break out both loops
         }
         
         if (sumdiff >= pixcount) continue;                                      //  thumbnails not matching

         if (ndup == 0) {
            fprintf(fid,"%s\n",Tdata[ii]->file);                                 //  first duplicate, output file name
            ndup++;
            Ndups++;
         }

         fprintf(fid,"%s\n",Tdata[jj]->file);                                    //  output duplicate image file name
         zfree(Tdata[jj]->file);                                                 //  remove from list
         zfree(Tdata[jj]->pixels);
         zfree(Tdata[jj]);
         Tdata[jj] = 0;
         ndup++;
         Ndups++;

         snprintf(text,100,"%d",Ndups);                                          //  update total duplicates found
         zdialog_stuff(zd,"dups",text);

         zmainloop();
      }
   }

   fclose(fid);
   fid = 0;
   
   if (Ndups == 0) {
      zmessageACK(Mwin,"0 duplicates");
      goto cleanup;
   }

   navi::gallerytype = SEARCH;                                                   //  generate gallery of duplicate images
   gallery(tempfile,"initF",0);
   gallery(0,"paint",0);                                                         //  position at top
   m_viewmode(0,"G");
   
   snprintf(albumfile,200,"%s/Duplicate Images",albums_folder);                  //  save search results in the
   err = copyFile(tempfile,albumfile);                                           //    album "Duplicate Images"
   if (err) zmessageACK(Mwin,strerror(err));

cleanup:

   zdialog_free(zd);

   if (fid) fclose(fid);
   fid = 0;

   if (files) {
      for (ii = 0; ii < Nfiles; ii++)
         if (files[ii]) zfree(files[ii]);
      zfree(files);
   }

   if (Tdata) {   
      for (ii = 0; ii < Nfiles; ii++) {
         if (! Tdata[ii]) continue;
         zfree(Tdata[ii]->file);
         zfree(Tdata[ii]->pixels);
         zfree(Tdata[ii]);
      }
      zfree(Tdata);
   }

   Ffuncbusy = 0;
   Fkillfunc = 0;
   Fblock = 0;
   return;

filerror:
   zmessageACK(Mwin,"file error: %s",strerror(errno));
   goto cleanup;
}


//  dialog event and completion function

int  duplicates_dialog_event(zdialog *zd, cchar *event)
{
   using namespace duplicates_names;

   double      freemem, cachemem, reqmem;
   char        text[20], *pp, *pp2;
   int         nn, ii, jj;

   if (strmatch(event,"allfiles"))
   {
      zdialog_fetch(zd,"allfiles",nn);
      Fallfiles = nn;
      if (! Fallfiles) return 1;

      for (ii = jj = 0; ii < Nxxrec; ii++)                                       //  count all files
      {
         pp = xxrec_tab[ii]->file;
         pp = image2thumbfile(pp);                                               //  omit those without thumbnail
         if (! pp) continue;
         files[jj++] = pp;
      }
      
      Nfiles = jj;

      snprintf(text,20,"%d",Nfiles);                                             //  file count >> dialog
      zdialog_stuff(zd,"filecount",text);
      return 1;
   }

   if (strmatch(event,"gallery"))
   {
      zdialog_fetch(zd,"gallery",nn);
      Fgallery = nn;
      if (! Fgallery) return 1;

      for (ii = jj = 0; ii < navi::Nfiles; ii++)                                 //  scan current gallery
      {
         pp = gallery(0,"get",ii);
         if (! pp) break;
         if (*pp == '!') {                                                       //  skip folders
            zfree(pp);
            continue;
         }
         
         pp2 = image2thumbfile(pp);                                              //  get corresp. thumbnail file
         zfree(pp);
         if (! pp2) continue;
         files[jj++] = pp2;                                                      //  save thumbnail
      }
      
      Nfiles = jj;

      snprintf(text,20,"%d",Nfiles);                                             //  file count >> dialog
      zdialog_stuff(zd,"filecount",text);
      return 1;
   }
   
   if (strmatch(event,"calculate"))                                              //  calculate thumbnail size
   {
      zd->zstat = 0;                                                             //  keep dialog active

      parseprocfile("/proc/meminfo","MemFree:",&freemem,0);                      //  get amount of free memory
      parseprocfile("/proc/meminfo","Cached:",&cachemem,0);
      freemem += cachemem;                                                       //  KB
      freemem *= 1024;
      
      for (thumbsize = 16; thumbsize <= 512; thumbsize += 8) {                   //  find largets thumbnail size
         reqmem = 0.7 * thumbsize * thumbsize * 3 * Nfiles;                      //    that probably works
         if (reqmem > freemem) break;
      }
      
      thumbsize -= 8;                                                            //  biggest size that fits
      if (thumbsize < 16) {
         zmessageACK(Mwin,E2X("too many files, cannot continue"));
         return 1;
      }

      zdialog_stuff(zd,"thumbsize",thumbsize);                                   //  stuff into dialog
      return 1;
   }

   if (! zd->zstat) return 1;                                                    //  wait for user input
   if (zd->zstat != 1) Fkillfunc = 1;                                            //  [cancel] or [x]
   return 1;                                                                     //  [proceed]
}


//  Make small random changes to all images.
//  Used for testing and benchmarking Find Duplicates. 

void randomize()
{
   using namespace duplicates_names;

   char     *file;
   int      px, py;
   int      ii, jj, kk;
   float    *pixel;
   
   for (ii = 0; ii < Nxxrec; ii++)                                               //  loop all files
   {
      if (drandz() > 0.95) continue;                                             //  leave 5% duplicates
      
      zmainloop();                                                               //  keep GTK alive

      file = zstrdup(xxrec_tab[ii]->file);
      if (! file) continue;

      printf(" %d  %s \n",ii,file);                                              //  log progress
      
      f_open(file);                                                              //  open and read file
      E0pxm = PXM_load(file,1);
      if (! E0pxm) continue;

      jj = 2 + 49 * drandz();                                                    //  random 2-50 pixels

      for (kk = 0; kk < jj; kk++)
      {
         px = E0pxm->ww * drandz();                                              //  random pixel
         py = E0pxm->hh * drandz();
         pixel = PXMpix(E0pxm,px,py);
         pixel[0] = pixel[1] = pixel[2] = 0;                                     //  RGB = black
      }
      
      f_save(file,"jpg",8,0,1);                                                  //  write file
      
      PXM_free(E0pxm);
      zfree(file);
   }

   return;
}


/********************************************************************************/

//  Show RGB values for 1-9 pixels selected with mouse-clicks.
//  Additional pixel position tracks active mouse position

void  show_RGB_mousefunc();
int   show_RGB_timefunc(void *);

zdialog     *RGBSzd;
int         RGBSpixel[10][2];                                                    //  0-9 clicked pixels + current mouse
int         RGBSnpix = 0;                                                        //  no. clicked pixels, 0-9
int         RGBSdelta = 0;                                                       //  abs/delta mode
int         RGBSlabels = 0;                                                      //  pixel labels on/off
int         RGBSupdate = 0;                                                      //  window update needed
char        *RGBSfile = 0;


void m_show_RGB(GtkWidget *, cchar *)
{
   int   show_RGB_event(zdialog *zd, cchar *event);

   cchar       *mess = E2X("Click image to select pixels.");
   cchar       *header = " Pixel            Red     Green   Blue";
   char        hbx[8] = "hbx", pixx[8] = "pixx";                                 //  last char. is '0' to '9'
   int         ii;

   F1_help_topic = "show_RGB";

   m_viewmode(0,"F");                                                            //  file view mode                     19.0
   if (! curr_file) return;                                                      //  no image file

   if (! E0pxm && ! E1pxm && ! E3pxm) {
      E0pxm = PXM_load(curr_file,1);                                             //  never edited 
      if (! E0pxm) return;                                                       //  get poss. 16-bit file              19.5
      curr_file_bpc = f_load_bpc;
   }

/***
    _________________________________________
   |                                         |
   |  Click image to select pixels.          |
   |  [x] delta  [x] labels                  |
   |                                         |                                   //  remove EV option                   19.2
   |   Pixel           Red    Green  Blue    |
   |   A xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |   B xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |   C xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |   D xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |   E xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |   F xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |   G xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |   H xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |   I xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |     xxxxx xxxxx   xxxxx  xxxxx  xxxxx   |
   |                                         |
   |                        [clear] [done]   |
   |_________________________________________|

***/

   if (RGBSfile) zfree(RGBSfile);                                                //  detect file change                 19.5
   RGBSfile = zstrdup(curr_file);

   RGBSnpix = 0;                                                                 //  no clicked pixels yet
   RGBSlabels = 0;                                                               //  no labels yet                      19.5
   RGBSupdate = 0;                                                               //  stop window updates

   if (RGBSzd) zdialog_free(RGBSzd);                                             //  delete previous if any
   zdialog *zd = zdialog_new(E2X("Show RGB"),Mwin,Bclear,Bdone,null);
   RGBSzd = zd;

   zdialog_add_widget(zd,"hbox","hbmess","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labmess","hbmess",mess,"space=5");

   zdialog_add_widget(zd,"hbox","hbmym","dialog");
   zdialog_add_widget(zd,"check","delta","hbmym","delta","space=8");
   zdialog_add_widget(zd,"check","labels","hbmym","labels","space=8");

   if (RGBSdelta && E3pxm) zdialog_stuff(zd,"delta",1);

   zdialog_add_widget(zd,"vbox","vbdat","dialog",0,"space=5");                   //  vbox for pixel values
   zdialog_add_widget(zd,"hbox","hbpix","vbdat");
   zdialog_add_widget(zd,"label","labheader","hbpix",header);                    //  Pixel        Red    Green  Blue
   zdialog_labelfont(zd,"labheader","monospace 9",header);

   for (ii = 0; ii < 10; ii++)
   {                                                                             //  10 hbox's with 10 labels
      hbx[2] = '0' + ii;
      pixx[3] = '0' + ii;
      zdialog_add_widget(zd,"hbox",hbx,"vbdat");
      zdialog_add_widget(zd,"label",pixx,hbx);
   }

   zdialog_run(zd,show_RGB_event,"save");                                        //  run dialog
   takeMouse(show_RGB_mousefunc,dragcursor);                                     //  connect mouse function
   g_timeout_add(200,show_RGB_timefunc,0);                                       //  start timer function, 200 ms

   return;
}


//  dialog event function

int show_RGB_event(zdialog *zd, cchar *event)
{
   if (zd->zstat) {
      if (zd->zstat == 1) {                                                      //  clear
         zd->zstat = 0;                                                          //  keep dialog active
         RGBSnpix = 0;                                                           //  clicked pixel count = 0
         erase_toptext(102);                                                     //  erase labels on image
         RGBSupdate++;                                                           //  update display window
      }
      else {                                                                     //  done or kill
         freeMouse();                                                            //  disconnect mouse function
         zdialog_free(RGBSzd);                                                   //  kill dialog
         RGBSzd = 0;
         erase_toptext(102);
      }
      Fpaint2();
      return 0;                                                                  //  19.0
   }

   if (strmatch(event,"focus"))                                                  //  toggle mouse capture
      takeMouse(show_RGB_mousefunc,dragcursor);                                  //  connect mouse function

   if (strmatch(event,"delta")) {                                                //  set absolute/delta mode
      zdialog_fetch(zd,"delta",RGBSdelta);
      if (RGBSdelta && ! E3pxm) {
         RGBSdelta = 0;                                                          //  block delta mode if no edit underway
         zdialog_stuff(zd,"delta",0);
         zmessageACK(Mwin,E2X("Edit function must be active"));
      }
   }

   if (strmatch(event,"labels")) {                                               //  get labels on/off
      zdialog_fetch(zd,"labels",RGBSlabels);
      RGBSupdate++;                                                              //  update window
   }

   return 0;
}


//  mouse function
//  fill table positions 0-8 with last clicked pixel positions and RGB data
//  next table position tracks current mouse position and RGB data

void show_RGB_mousefunc()                                                        //  mouse function
{
   int      ii;
   PXM      *pxm;

   if (E3pxm) pxm = E3pxm;                                                       //  report image being edited
   else if (E1pxm) pxm = E1pxm;
   else if (E0pxm) pxm = E0pxm;
   else return;                                                                  //  must have E0/E1/E3                 19.5

   if (Mxposn <= 0 || Mxposn >= pxm->ww-1) return;                               //  mouse outside image, ignore
   if (Myposn <= 0 || Myposn >= pxm->hh-1) return;

   if (LMclick)                                                                  //  left click, add labeled position
   {
      LMclick = 0;

      if (RGBSnpix == 9) {                                                       //  if all 9 labeled positions filled,
         for (ii = 1; ii < 9; ii++) {                                            //    remove first (oldest) and
            RGBSpixel[ii-1][0] = RGBSpixel[ii][0];                               //      push the rest back
            RGBSpixel[ii-1][1] = RGBSpixel[ii][1];
         }
         RGBSnpix = 8;                                                           //  position for newest clicked pixel
      }

      ii = RGBSnpix;                                                             //  labeled position to fill, 0-8
      RGBSpixel[ii][0] = Mxclick;                                                //  save newest pixel
      RGBSpixel[ii][1] = Myclick;
      RGBSnpix++;                                                                //  count is 1-9
   }
   
   ii = RGBSnpix;                                                                //  fill last position from active mouse
   RGBSpixel[ii][0] = Mxposn;
   RGBSpixel[ii][1] = Myposn;
   
   RGBSupdate++;                                                                 //  update window
   return;
}


//  timer function
//  display RGB values for last 0-9 clicked pixels and current mouse position

int show_RGB_timefunc(void *arg)
{
   static char    label[9][4] = { " A ", " B ", " C ", " D ", " E ",             //  labels A-I for last 0-9 clicked pixels
                                  " F ", " G ", " H ", " I " };
   PXM         *pxm = 0;
   int         ii, jj, px, py;
   int         ww, hh;
   float       red3, green3, blue3;
   float       *ppixa, *ppixb;
   char        text[100], pixx[8] = "pixx";

   if (! RGBSzd) return 0;                                                       //  user quit, cancel timer
   
   if (! curr_file || ! strmatch(curr_file,RGBSfile)) {                          //  image change, start over           19.13
      m_show_RGB(0,0);
      return 0;
   }

   if (E3pxm) pxm = E3pxm;                                                       //  report image being edited
   else if (E1pxm) pxm = E1pxm;
   else if (E0pxm) pxm = E0pxm;
   else return 1;

   if (RGBSdelta && ! E3pxm) {
      RGBSdelta = 0;                                                             //  delta mode only if edit active
      zdialog_stuff(RGBSzd,"delta",RGBSdelta);                                   //  update dialog                      19.2
   }

   ww = pxm->ww;
   hh = pxm->hh;
   
   for (ii = 0; ii < RGBSnpix; ii++)                                             //  0-9 clicked pixels                 19.2
   {
      px = RGBSpixel[ii][0];                                                     //  next pixel to report
      py = RGBSpixel[ii][1];
      if (px >= 0 && px < ww && py >= 0 && py < hh) continue;                    //  within image limits

      for (jj = ii+1; jj < RGBSnpix + 1; jj++) {
         RGBSpixel[jj-1][0] = RGBSpixel[jj][0];                                  //  remove pixel outside limits
         RGBSpixel[jj-1][1] = RGBSpixel[jj][1];                                  //    and pack the remaining down
      }                                                                          //  include last+1 = curr. mouse position

      RGBSnpix--;
      RGBSupdate++;                                                              //  update dialog
   }
   
   if (! strmatch(curr_file,RGBSfile)) return 0;                                 //  image change, quit                 19.5

   if (! RGBSupdate) return 1;                                                   //  no update needed                   19.5
   RGBSupdate = 0;

   erase_toptext(102);

   if (RGBSlabels) {
      for (ii = 0; ii < RGBSnpix; ii++) {                                        //  show pixel labels on image
         px = RGBSpixel[ii][0];
         py = RGBSpixel[ii][1];
         add_toptext(102,px,py,label[ii],"Sans 8");
      }
   }

   for (ii = 0; ii < 10; ii++)                                                   //  loop positons 0 to 9
   {
      pixx[3] = '0' + ii;                                                        //  widget names "pix0" ... "pix9"

      if (ii > RGBSnpix) {                                                       //  no pixel there yet
         zdialog_stuff(RGBSzd,pixx,"");                                          //  blank report line
         continue;
      }

      px = RGBSpixel[ii][0];                                                     //  next pixel to report
      py = RGBSpixel[ii][1];

      ppixa = PXMpix(pxm,px,py);                                                 //  get pixel RGB values
      red3 = ppixa[0];
      green3 = ppixa[1];
      blue3 = ppixa[2];

      if (RGBSdelta) {                                                           //  delta RGB for edited image         19.2
         ppixb = PXMpix(E1pxm,px,py);                                            //  "before" image E1
         red3 -= ppixb[0];
         green3 -= ppixb[1];
         blue3 -= ppixb[2];
      }

      if (ii == RGBSnpix)                                                        //  last table position
         snprintf(text,100,"   %5d %5d  ",px,py);                                //  mouse pixel, format "   xxxx yyyy"
      else snprintf(text,100," %c %5d %5d  ",'A'+ii,px,py);                      //  clicked pixel, format " A xxxx yyyy"

      snprintf(text+14,86,"   %6.2f  %6.2f  %6.2f ",red3,green3,blue3);
      zdialog_labelfont(RGBSzd,pixx,"monospace 9",text);
   }

   Fpaint2();

   return 1;
}


/********************************************************************************/

//  convert color profile of current image

editfunc    EFcolorprof;
char        ICCprofilename[100];                                                 //  new color profile name 
char        colorprof1[200] = "/usr/share/color/icc/colord/AdobeRGB1998.icc";
char        colorprof2[200] = "/usr/share/color/icc/colord/sRGB.icc";


//  menu function

void m_color_profile(GtkWidget *, cchar *menu)
{
   int colorprof_dialog_event(zdialog *zd, cchar *event);

   zdialog     *zd;
   int         err;
   cchar       *exifkey[2], *exifdata[2];

   F1_help_topic = "color_profile";
   m_viewmode(0,"0");                                                            //  file view mode                     19.0

   EFcolorprof.menuname = menu;
   EFcolorprof.menufunc = m_color_profile;
   EFcolorprof.funcname = "color_profile";
   EFcolorprof.Frestart = 1;                                                     //  allow restart
   EFcolorprof.Fscript = 1;                                                      //  scripting supported
   if (! edit_setup(EFcolorprof)) return;                                        //  setup edit
   
   *ICCprofilename = 0;                                                          //  no color profile change

/***
       ________________________________________________________
      |       Change Color Profile                             |
      |                                                        |
      |  input profile  [___________________________] [Browse] |
      |  output profile [___________________________] [Browse] |
      |                                                        |
      |                                [Apply] [Done] [Cancel] |
      |________________________________________________________|

***/

   zd = zdialog_new(E2X("Change Color Profile"),Mwin,Bapply,Bdone,Bcancel,null);
   EFcolorprof.zd = zd;

   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","lab1","hb1",E2X("input profile"),"space=5");
   zdialog_add_widget(zd,"zentry","prof1","hb1",0,"expand|size=30");
   zdialog_add_widget(zd,"button","butt1","hb1",Bbrowse,"space=5");
   zdialog_add_widget(zd,"hbox","hb2","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","lab2","hb2",E2X("output profile"),"space=5");
   zdialog_add_widget(zd,"zentry","prof2","hb2",0,"expand|size=30");
   zdialog_add_widget(zd,"button","butt2","hb2",Bbrowse,"space=5");

   zdialog_stuff(zd,"prof1",colorprof1);
   zdialog_stuff(zd,"prof2",colorprof2);

   zdialog_run(zd,colorprof_dialog_event,"save");                                //  run dialog, parallel

   zdialog_wait(zd);                                                             //  wait for completion
   if (! *ICCprofilename) return;                                                //  no color profile change
   
   m_file_save_version(0,0);                                                     //  save as new version and re-open
   shell_quiet("rm -f %s/undo_*",temp_folder);                                   //  remove undo/redo files
   URS_pos = URS_max = 0;                                                        //  reset undo/redo stack

   exifkey[0] = exif_colorprof2_key;                                             //  remove embedded color profile
   exifdata[0] = "";
   exifkey[1] = exif_colorprof1_key;                                             //  set new color profile name
   exifdata[1] = ICCprofilename;
   err = exif_put(curr_file,exifkey,exifdata,2);
   if (err) zmessageACK(Mwin,E2X("Unable to change EXIF color profile"));

   zmessageACK(Mwin,E2X("automatic new version created"));
   return;
}


//  dialog event and completion callback function

int colorprof_dialog_event(zdialog *zd, cchar *event)
{
   cchar    *title = E2X("color profile");
   char     *file;
   float    *fpix1, *fpix2;
   float    f256 = 1.0 / 256.0;
   uint     Npix, nn;
   
   cmsHTRANSFORM  cmsxform;
   cmsHPROFILE    cmsprof1, cmsprof2;

   if (strmatch(event,"apply")) zd->zstat = 1;                                   //  from script
   if (strmatch(event,"done")) zd->zstat = 2;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  from f_open()
   
   if (strmatch(event,"butt1")) {
      zdialog_fetch(zd,"prof1",colorprof1,200);                                  //  select input profile
      file = zgetfile(title,MWIN,"file",colorprof1);
      if (! file) return 1;
      zdialog_stuff(zd,"prof1",file);
      zfree(file);
   }

   if (strmatch(event,"butt2")) {
      zdialog_fetch(zd,"prof2",colorprof2,200);                                  //  select output profile
      file = zgetfile(title,MWIN,"file",colorprof2);
      if (! file) return 1;
      zdialog_stuff(zd,"prof2",file);
      zfree(file);
   }

   if (! zd->zstat) return 1;                                                    //  wait for user completion

   if (zd->zstat == 1) zd->zstat = 0;                                            //  [apply] - keep dialog open

   if (zd->zstat)
   {
      if (zd->zstat == 2 && CEF->Fmods) edit_done(0);                            //  commit edit
      else {
         edit_cancel(0);                                                         //  discard edit
         *ICCprofilename = 0;                                                    //  no ICC profile change
      }
      return 1;
   }

   zdialog_fetch(zd,"prof1",colorprof1,200);                                     //  [apply] - get final profiles
   zdialog_fetch(zd,"prof2",colorprof2,200);

   cmsprof1 = cmsOpenProfileFromFile(colorprof1,"r");
   if (! cmsprof1) {
      zmessageACK(Mwin,E2X("unknown cms profile %s"),colorprof1);
      return 1;
   }

   cmsprof2 = cmsOpenProfileFromFile(colorprof2,"r");
   if (! cmsprof2) {
      zmessageACK(Mwin,E2X("unknown cms profile %s"),colorprof2);
      return 1;
   }

   //  calculate the color space transformation table

   Ffuncbusy = 1;
   zmainsleep(0.2);

   cmsxform = cmsCreateTransform(cmsprof1,TYPE_RGB_FLT,cmsprof2,TYPE_RGB_FLT,INTENT_PERCEPTUAL,0);
   if (! cmsxform) {
      zmessageACK(Mwin,"cmsCreateTransform() failed");
      Ffuncbusy = 0;
      return 1;
   }

   fpix1 = E0pxm->pixels;                                                        //  input and output pixels
   fpix2 = E3pxm->pixels;
   Npix = E0pxm->ww * E0pxm->hh;

   for (uint ii = 0; ii < 3 * Npix; ii++)                                        //  rescale to range 0 - 0.9999
      fpix2[ii] = f256 * fpix1[ii];

   while (Npix)                                                                  //  convert image pixels
   {
      nn = Npix;
      if (nn > 100000) nn = 100000;                                              //  do 100K per call
      cmsDoTransform(cmsxform,fpix2,fpix2,nn);                                   //  speed: 3 megapixels/sec for 3 GHz CPU
      fpix2 += nn * 3;
      Npix -= nn;
      zmainloop(20);                                                             //  keep GTK alive
   }

   fpix2 = E3pxm->pixels;
   Npix = E0pxm->ww * E0pxm->hh;
   for (uint ii = 0; ii < 3 * Npix; ii++) {                                      //  rescale back to 0 - 255.99
      fpix2[ii] = fpix2[ii] * 256.0;
      if (fpix2[ii] > 255.9) fpix2[ii] = 255.9;
      if (fpix2[ii] < 0) fpix2[ii] = 0;                                          //  compensate cms bug
   }

   cmsInfoType it = (cmsInfoType) 0;
   cmsGetProfileInfoASCII(cmsprof2,it,"en","US",ICCprofilename,100);             //  new color profile name

   cmsDeleteTransform(cmsxform);                                                 //  free resources
   cmsCloseProfile(cmsprof1);
   cmsCloseProfile(cmsprof2);

   Ffuncbusy = 0;
   CEF->Fmods++;                                                                 //  image is modified
   CEF->Fsaved = 0;
   Fpaint2();                                                                    //  update window image

   return 1;
}


/********************************************************************************/

//  printer color calibration tool

namespace calibprint
{
   int   dialog_event(zdialog *zd, cchar *event);
   void  printchart();
   void  scanchart();
   void  fixchart();
   void  processchart();

//  parameters for RGB step size of 23: 0 23 46 69 ... 253 (253 --> 255)
//  NC    colors per RGB dimension (12) (counting both 0 and 255)                //  step size from 16 to 23
//  CS    color step size (23) 
//  TS    tile size in pixels (70)
//  ROWS  chart rows (50)               ROWS x COLS must be >= NC*NC*NC
//  COLS  chart columns (35)

   #define NC 12
   #define CS 23
   #define TS 70
   #define ROWS 50
   #define COLS 35

   #define NC2 (NC*NC)
   #define NC3 (NC*NC*NC)

   int   RGBvals[NC];
   int   Ntiles = NC3;
   int   chartww = COLS * TS;             //  chart image size
   int   charthh = ROWS * TS;
   int   margin = 80;                     //  chart margins
   char  printchartfile[200];
   char  scanchartfile[200];
}


//  menu function

void m_calibrate_printer(GtkWidget *, cchar *menu)
{
   using namespace calibprint;
   
   zdialog     *zd;
   cchar       *title = E2X("Calibrate Printer");

   F1_help_topic = "calibrate_printer";
   m_viewmode(0,"F");                                                            //  file view mode                     19.0

   for (int ii = 0; ii < NC; ii++)                                               //  construct RGBvals table
      RGBvals[ii] = CS * ii;
   RGBvals[NC-1] = 255;                                                          //  set last value = 255

/***
       ______________________________________
      |         Calibrate Printer            |
      |                                      |
      |  (o) print color chart               |
      |  (o) scan and save color chart       |
      |  (o) align and trim color chart      |
      |  (o) open and process color chart    |
      |  (o) print image with revised colors |
      |                                      |
      |                  [Proceed] [Cancel]  |
      |______________________________________|

***/

   zd = zdialog_new(title,Mwin,Bproceed,Bcancel,null);

   zdialog_add_widget(zd,"radio","printchart","dialog",E2X("print color chart"));
   zdialog_add_widget(zd,"radio","scanchart","dialog",E2X("scan and save color chart"));
   zdialog_add_widget(zd,"radio","fixchart","dialog",E2X("align and trim color chart"));
   zdialog_add_widget(zd,"radio","processchart","dialog",E2X("open and process color chart"));
   zdialog_add_widget(zd,"radio","printimage","dialog",E2X("print image with revised colors"));

   zdialog_stuff(zd,"printchart",1);
   zdialog_stuff(zd,"scanchart",0);
   zdialog_stuff(zd,"fixchart",0);
   zdialog_stuff(zd,"processchart",0);
   zdialog_stuff(zd,"printimage",0);
   
   zdialog_resize(zd,250,0);
   zdialog_run(zd,dialog_event,"parent");
   return;
}


//  dialog event and completion function

int calibprint::dialog_event(zdialog *zd, cchar *event)
{
   using namespace calibprint;

   int      nn;
   
   F1_help_topic = "calibrate_printer";

   if (! zd->zstat) return 1;                                                    //  wait for [proceed] or [cancel]

   if (zd->zstat != 1) {                                                         //  cancel
      zdialog_free(zd);
      return 1;
   }
   
   zdialog_fetch(zd,"printchart",nn);
   if (nn) {
      printchart();
      return 1;
   }
   
   zdialog_fetch(zd,"scanchart",nn);
   if (nn) {
      scanchart();
      return 1;
   }
   
   zdialog_fetch(zd,"fixchart",nn);
   if (nn) {
      fixchart();
      return 1;
   }
   
   zdialog_fetch(zd,"processchart",nn);
   if (nn) {
      processchart();
      return 1;
   }

   zdialog_fetch(zd,"printimage",nn);
   if (nn) {
      print_calibrated();
      return 1;
   }
   
   zd->zstat = 0;                                                                //  keep dialog active
   return 1;
}


//  generate and print the color chart

void calibprint::printchart()
{
   using namespace calibprint;

   int      ii, cc, fww, fhh, rx, gx, bx;
   int      row, col, px, py;
   int      chartrs;
   uint8    *chartpixels, *pix1;
   PIXBUF   *chartpxb;
   GError   *gerror = 0;
   
   fww = chartww + 2 * margin;
   fhh = charthh + 2 * margin;

   chartpxb = gdk_pixbuf_new(GDKRGB,0,8,fww,fhh);                                //  make chart image
   if (! chartpxb) {
      zmessageACK(Mwin,"cannot create pixbuf");
      return;
   }

   chartpixels = gdk_pixbuf_get_pixels(chartpxb);                                //  clear to white
   chartrs = gdk_pixbuf_get_rowstride(chartpxb);
   cc = fhh * chartrs;
   memset(chartpixels,255,cc);
   
   for (py = 0; py < charthh; py++)                                              //  fill chart tiles with colors
   for (px = 0; px < chartww; px++)
   {
      row = py / TS;
      col = px / TS;
      ii = row * COLS + col;
      if (ii >= Ntiles) break;                                                   //  last chart positions may be unused
      rx = ii / NC2;
      gx = (ii - NC2 * rx) / NC;                                                 //  RGB index values for tile ii
      bx = ii - NC2 * rx - NC * gx;
      pix1 = chartpixels + (py + margin) * chartrs + (px + margin) * 3;
      pix1[0] = RGBvals[rx];
      pix1[1] = RGBvals[gx];
      pix1[2] = RGBvals[bx];
   }
   
   for (py = margin-10; py < fhh-margin+10; py++)                                //  add green margin around tiles
   for (px = margin-10; px < fww-margin+10; px++)                                //    for easier de-skew and trim
   {
      if (py > margin-1 && py < fhh-margin &&
          px > margin-1 && px < fww-margin) continue;
      pix1 = chartpixels + py * chartrs + px * 3;
      pix1[0] = pix1[2] = 0;
      pix1[1] = 255;
   }
   
   snprintf(printchartfile,200,"%s/printchart.png",printer_color_folder);
   gdk_pixbuf_save(chartpxb,printchartfile,"png",&gerror,null);
   if (gerror) {
      zmessageACK(Mwin,gerror->message);
      return;
   }
   
   g_object_unref(chartpxb);

   zmessageACK(Mwin,"Print chart in vertical orientation without margins.");
   print_image_file(Mwin,printchartfile);                                        //  print the chart

   return;
}


//  scan the color chart

void calibprint::scanchart()
{
   using namespace calibprint;

   zmessageACK(Mwin,E2X("Scan the printed color chart. \n"
                        "The darkest row is at the top. \n"
                        "Save in %s/"),printer_color_folder);
   return;
}


//  edit and fix the color chart

void calibprint::fixchart()
{
   using namespace calibprint;
   
   char     *pp;

   zmessageACK(Mwin,E2X("Open and edit the scanned color chart file. \n"
                        "Remove any skew or rotation from scanning. \n"
                        "(Use the Fix Perspective function for this). \n"
                        "Cut off the thin green margin ACCURATELY."));
   
   pp = zgetfile("scanned color chart file",MWIN,"file",printer_color_folder,1);
   if (! pp) return;
   strncpy0(scanchartfile,pp,200);
   f_open(scanchartfile,0,0,1,0);
   return;
}


//  process the scanned and fixed color chart

void calibprint::processchart()
{
   using namespace calibprint;

   PIXBUF   *chartpxb;
   GError   *gerror = 0;
   uint8    *chartpixels, *pix1;
   FILE     *fid;
   char     mapfile[200], *pp, *pp2;
   int      chartrs, chartnc, px, py;
   int      ii, nn, row, col, rx, gx, bx;
   int      xlo, xhi, ylo, yhi;
   float    fww, fhh;
   int      Rsum, Gsum, Bsum, Rout, Gout, Bout;
   int      r1, r2, ry, g1, g2, gy, b1, b2, by;
   int      ERR1[NC][NC][NC][3], ERR2[NC][NC][NC][3];

   zmessageACK(Mwin,E2X("Open the trimmed color chart file"));

   pp = zgetfile("trimmed color chart file",MWIN,"file",printer_color_folder,1);
   if (! pp) return;
   strncpy0(scanchartfile,pp,200);
   
   chartpxb = gdk_pixbuf_new_from_file(scanchartfile,&gerror);                   //  scanned chart without margins
   if (! chartpxb) {
      if (gerror) zmessageACK(Mwin,gerror->message);
      return;
   }
   
   chartww = gdk_pixbuf_get_width(chartpxb);
   charthh = gdk_pixbuf_get_height(chartpxb);
   chartpixels = gdk_pixbuf_get_pixels(chartpxb);
   chartrs = gdk_pixbuf_get_rowstride(chartpxb);
   chartnc = gdk_pixbuf_get_n_channels(chartpxb);
   fww = 1.0 * chartww / COLS;
   fhh = 1.0 * charthh / ROWS;

   for (row = 0; row < ROWS; row++)                                              //  loop each tile
   for (col = 0; col < COLS; col++)
   {
      ii = row * COLS + col;
      if (ii >= Ntiles) break;

      ylo = row * fhh;                                                           //  tile position within chart image
      yhi = ylo + fhh;
      xlo = col * fww;
      xhi = xlo + fww;

      Rsum = Gsum = Bsum = nn = 0;

      for (py = ylo+fhh/5; py < yhi-fhh/5; py++)                                 //  get tile pixels less 20% margins
      for (px = xlo+fww/5; px < xhi-fww/5; px++)
      {
         pix1 = chartpixels + py * chartrs + px * chartnc;
         Rsum += pix1[0];
         Gsum += pix1[1];
         Bsum += pix1[2];
         nn++;
      }

      Rout = Rsum / nn;                                                          //  average tile RGB values
      Gout = Gsum / nn;
      Bout = Bsum / nn;

      rx = ii / NC2;
      gx = (ii - NC2 * rx) / NC;
      bx = ii - NC2 * rx - NC * gx;
      
      ERR1[rx][gx][bx][0] = Rout - RGBvals[rx];                                  //  error = (scammed RGB) - (printed RGB)
      ERR1[rx][gx][bx][1] = Gout - RGBvals[gx];
      ERR1[rx][gx][bx][2] = Bout - RGBvals[bx];
   }

   g_object_unref(chartpxb);

   //  anneal the error values to reduce randomness
   
   for (int pass = 1; pass <= 4; pass++)                                         //  4 passes
   {
      for (rx = 0; rx < NC; rx++)                                                //  use neighbors in 3 channels
      for (gx = 0; gx < NC; gx++)
      for (bx = 0; bx < NC; bx++)
      {
         r1 = rx-1;
         r2 = rx+1;
         g1 = gx-1;
         g2 = gx+1;
         b1 = bx-1;
         b2 = bx+1;

         if (r1 < 0) r1 = 0;
         if (r2 > NC-1) r2 = NC-1;
         if (g1 < 0) g1 = 0;
         if (g2 > NC-1) g2 = NC-1;
         if (b1 < 0) b1 = 0;
         if (b2 > NC-1) b2 = NC-1;
         
         Rsum = Gsum = Bsum = nn = 0;

         for (ry = r1; ry <= r2; ry++)
         for (gy = g1; gy <= g2; gy++)
         for (by = b1; by <= b2; by++)
         {
            Rsum += ERR1[ry][gy][by][0];
            Gsum += ERR1[ry][gy][by][1];
            Bsum += ERR1[ry][gy][by][2];
            nn++;
         }

         ERR2[rx][gx][bx][0] = Rsum / nn;
         ERR2[rx][gx][bx][1] = Gsum / nn;
         ERR2[rx][gx][bx][2] = Bsum / nn;
      }

      for (rx = 0; rx < NC; rx++)
      for (gx = 0; gx < NC; gx++)
      for (bx = 0; bx < NC; bx++)
      {
         ERR1[rx][gx][bx][0] = ERR2[rx][gx][bx][0];
         ERR1[rx][gx][bx][1] = ERR2[rx][gx][bx][1];
         ERR1[rx][gx][bx][2] = ERR2[rx][gx][bx][2];
      }

      for (rx = 1; rx < NC-1; rx++)                                              //  use neighbors in same channel
      for (gx = 0; gx < NC; gx++)
      for (bx = 0; bx < NC; bx++)
         ERR2[rx][gx][bx][0] = 0.5 * (ERR1[rx-1][gx][bx][0] + ERR1[rx+1][gx][bx][0]);

      for (rx = 0; rx < NC; rx++)
      for (gx = 1; gx < NC-1; gx++)
      for (bx = 0; bx < NC; bx++)
         ERR2[rx][gx][bx][1] = 0.5 * (ERR1[rx][gx-1][bx][1] + ERR1[rx][gx+1][bx][1]);

      for (rx = 0; rx < NC; rx++)
      for (gx = 0; gx < NC; gx++)
      for (bx = 1; bx < NC-1; bx++)
         ERR2[rx][gx][bx][2] = 0.5 * (ERR1[rx][gx][bx-1][2] + ERR1[rx][gx][bx+1][2]);

      for (rx = 0; rx < NC; rx++)
      for (gx = 0; gx < NC; gx++)
      for (bx = 0; bx < NC; bx++)
      {
         ERR1[rx][gx][bx][0] = ERR2[rx][gx][bx][0];
         ERR1[rx][gx][bx][1] = ERR2[rx][gx][bx][1];
         ERR1[rx][gx][bx][2] = ERR2[rx][gx][bx][2];
      }
   }                                                                             //  pass loop

   //  save finished color map to user-selected file

   zmessageACK(Mwin,E2X("Set the name for the output calibration file \n"
                        "[your calibration name].dat"));

   snprintf(mapfile,200,"%s/%s",printer_color_folder,colormapfile);
   pp = zgetfile("Color Map File",MWIN,"save",mapfile,1);
   if (! pp) return;
   pp2 = strrchr(pp,'/');
   zfree(colormapfile);
   colormapfile = zstrdup(pp2+1);
   save_params();
   zfree(pp);
   
   snprintf(mapfile,200,"%s/%s",printer_color_folder,colormapfile);
   fid = fopen(mapfile,"w");
   if (! fid) return;

   for (rx = 0; rx < NC; rx++)
   for (gx = 0; gx < NC; gx++)
   for (bx = 0; bx < NC; bx++)
   {
      fprintf(fid,"RGB: %3d %3d %3d   ERR: %4d %4d %4d \n",
          RGBvals[rx], RGBvals[gx], RGBvals[bx],
          ERR2[rx][gx][bx][0], ERR2[rx][gx][bx][1], ERR2[rx][gx][bx][2]);
   }

   fclose(fid);

   return;
}


//  Print the current image file with adjusted colors
//  Also called from the file menu function m_print_calibrated()

void print_calibrated()
{
   using namespace calibprint;

   zdialog  *zd;
   int      zstat;
   cchar    *title = E2X("Color map file to use");
   char     mapfile[200];
   FILE     *fid;
   PIXBUF   *pixbuf;
   GError   *gerror = 0;
   uint8    *pixels, *pix1;
   char     *pp, *pp2, printfile[100];
   int      ww, hh, rs, nc, px, py, nn, err;
   int      R1, G1, B1, R2, G2, B2;
   int      RGB[NC][NC][NC][3], ERR[NC][NC][NC][3];
   int      rr1, rr2, gg1, gg2, bb1, bb2;
   int      rx, gx, bx;
   int      ii, Dr, Dg, Db;
   float    W[8], w, Wsum, D, Dmax, F;
   float    Er, Eg, Eb;
   
   F1_help_topic = "print_calibrated";
   m_viewmode(0,"F");                                                            //  file view mode                     19.0

   if (! curr_file) {
      zmessageACK(Mwin,E2X("Select the image file to print."));
      return;
   }
   
   for (int ii = 0; ii < NC; ii++)                                               //  construct RGBvals table
      RGBvals[ii] = CS * ii;
   RGBvals[NC-1] = 255;                                                          //  set last value = 255

   zd = zdialog_new(title,Mwin,Bbrowse,Bproceed,Bcancel,null);                   //  show current color map file 
   zdialog_add_widget(zd,"hbox","hbmap","dialog");                               //    and allow user to choose another
   zdialog_add_widget(zd,"label","labmap","hbmap",0,"space=3");
   zdialog_stuff(zd,"labmap",colormapfile);
   zdialog_resize(zd,250,0);
   zdialog_run(zd,0,"parent");
   zstat = zdialog_wait(zd);
   zdialog_free(zd);

   if (zstat == 1) {                                                             //  [browse]
      snprintf(mapfile,200,"%s/%s",printer_color_folder,colormapfile);
      pp = zgetfile("Color Map File",MWIN,"file",mapfile,1);
      if (! pp) return;
      pp2 = strrchr(pp,'/');
      if (colormapfile) zfree(colormapfile);
      colormapfile = zstrdup(pp2+1);
      zfree(pp);
   }
   
   else if (zstat != 2) return;                                                  //  not proceed: cancel
   
   snprintf(mapfile,200,"%s/%s",printer_color_folder,colormapfile);
   fid = fopen(mapfile,"r");                                                     //  read color map file
   if (! fid) return;
   
   for (R1 = 0; R1 < NC; R1++)
   for (G1 = 0; G1 < NC; G1++)
   for (B1 = 0; B1 < NC; B1++)
   {
      nn = fscanf(fid,"RGB: %d %d %d   ERR: %d %d %d ",
           &RGB[R1][G1][B1][0], &RGB[R1][G1][B1][1], &RGB[R1][G1][B1][2],
           &ERR[R1][G1][B1][0], &ERR[R1][G1][B1][1], &ERR[R1][G1][B1][2]);
      if (nn != 6) {
         zmessageACK(Mwin,E2X("file format error"));
         fclose(fid);
         return;
      }
   }
   
   fclose(fid);
   
   pixbuf = gdk_pixbuf_copy(Fpxb->pixbuf);                                       //  get image pixbuf to convert
   if (! pixbuf) {
      if (gerror) zmessageACK(Mwin,gerror->message);
      return;
   }
   
   if (checkpend("all")) return;
   Fblock = 1;
   Ffuncbusy = 1;

   ww = gdk_pixbuf_get_width(pixbuf);
   hh = gdk_pixbuf_get_height(pixbuf);
   pixels = gdk_pixbuf_get_pixels(pixbuf);
   rs = gdk_pixbuf_get_rowstride(pixbuf);
   nc = gdk_pixbuf_get_n_channels(pixbuf);
   
   poptext_window(MWIN,E2X("converting colors..."),300,200,0,-1);

   for (py = 0; py < hh; py++)
   for (px = 0; px < ww; px++)
   {
      pix1 = pixels + py * rs + px * nc;
      R1 = pix1[0];                                                              //  image RGB values
      G1 = pix1[1];
      B1 = pix1[2];
      
      rr1 = R1/CS;                                                               //  get color map values surrounding RGB
      rr2 = rr1 + 1;
      if (rr2 > NC-1) {                                                          //  if > last entry, use last entry
         rr1--; 
         rr2--; 
      }

      gg1 = G1/CS;
      gg2 = gg1 + 1;
      if (gg2 > NC-1) { 
         gg1--; 
         gg2--; 
      }

      bb1 = B1/CS;
      bb2 = bb1 + 1;
      if (bb2 > NC-1) { 
         bb1--; 
         bb2--; 
      }

      ii = 0;
      Wsum = 0;
      Dmax = CS;
      
      for (rx = rr1; rx <= rr2; rx++)                                            //  loop 8 enclosing error map nodes
      for (gx = gg1; gx <= gg2; gx++)
      for (bx = bb1; bx <= bb2; bx++)
      {
         Dr = R1 - RGBvals[rx];                                                  //  RGB distance from enclosing node
         Dg = G1 - RGBvals[gx];
         Db = B1 - RGBvals[bx];
         D = sqrtf(Dr*Dr + Dg*Dg + Db*Db);
         if (D > Dmax) W[ii] = 0;
         else W[ii] = (Dmax - D) / Dmax;                                         //  weight of node
         Wsum += W[ii];                                                          //  sum of weights
         ii++;
      }
      
      ii = 0;
      Er = Eg = Eb = 0;

      for (rx = rr1; rx <= rr2; rx++)                                            //  loop 8 enclosing error map nodes
      for (gx = gg1; gx <= gg2; gx++)
      for (bx = bb1; bx <= bb2; bx++)
      {
         w = W[ii] / Wsum;
         Er += w * ERR[rx][gx][bx][0];                                           //  weighted sum of map node errors 
         Eg += w * ERR[rx][gx][bx][1];
         Eb += w * ERR[rx][gx][bx][2];
         ii++;
      }
      
      F = 1.0;                                                                   //  use 100% of calculated error
      R2 = R1 - F * Er;                                                          //  adjusted RGB = image RGB - error
      G2 = G1 - F * Eg;
      B2 = B1 - F * Eb;

      if (R2 < 0) R2 = 0;
      if (G2 < 0) G2 = 0;
      if (B2 < 0) B2 = 0;
      
      if (R2 > 255) R2 = 255;                                                    //  preserving RGB ratio does not help
      if (G2 > 255) G2 = 255;
      if (B2 > 255) B2 = 255;

      pix1[0] = R2;
      pix1[1] = G2;
      pix1[2] = B2;
      
      zmainloop(100);                                                            //  keep GTK alive
   }
   
   poptext_killnow();

   Ffuncbusy = 0;
   Fblock = 0;

   snprintf(printfile,100,"%s/printfile.png",temp_folder);                       //  save revised pixbuf to print file
   gdk_pixbuf_save(pixbuf,printfile,"png",&gerror,"compression","1",null);
   if (gerror) {
      zmessageACK(Mwin,gerror->message);
      return;
   }
   
   g_object_unref(pixbuf);
   
   err = f_open(printfile,0,0,1,0);                                              //  open print file
   if (err) return;

   zmessageACK(Mwin,E2X("Image colors are converted for printing."));
   print_image_file(Mwin,printfile);

   return;
}


/********************************************************************************/

//  setup x and y grid lines - count/spacing, enable/disable, offsets

void m_gridlines(GtkWidget *widget, cchar *menu)
{
   int gridlines_dialog_event(zdialog *zd, cchar *event);

   zdialog     *zd;
   int         G;

   m_viewmode(0,"F");                                                            //  file view mode                     19.0

   if (menu && strmatchN(menu,"grid ",5))                                        //  grid N from some edit functions
      currgrid = menu[5] - '0';

   else if (! widget) {                                                          //  from KB shortcut
      toggle_grid(2);                                                            //  toggle grid lines on/off
      return;
   }

   else currgrid = 0;                                                            //  must be menu call

   if (currgrid == 0)
      F1_help_topic = "grid_lines";

/***
       ____________________________________________
      |      Grid Lines                            |
      |                                            |
      | x-spacing [____]    y-spacing [____]       |
      |  x-count  [____]     y-count  [____]       |
      | x-enable  [_]       y-enable  [_]          |
      |                                            |
      | x-offset =================[]=============  |
      | y-offset ==============[]================  |
      |                                            |
      |                                     [Done] |
      |____________________________________________|

***/

   zd = zdialog_new(E2X("Grid Lines"),Mwin,Bdone,null);

   zdialog_add_widget(zd,"hbox","hb0","dialog",0,"space=10");
   zdialog_add_widget(zd,"vbox","vb1","hb0",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vb2","hb0",0,"homog");
   zdialog_add_widget(zd,"vbox","vbspace","hb0",0,"space=5");
   zdialog_add_widget(zd,"vbox","vb3","hb0",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vb4","hb0",0,"homog");

   zdialog_add_widget(zd,"label","lab1x","vb1",E2X("x-spacing"));
   zdialog_add_widget(zd,"label","lab2x","vb1",E2X("x-count"));
   zdialog_add_widget(zd,"label","lab4x","vb1",E2X("x-enable"));

   zdialog_add_widget(zd,"zspin","spacex","vb2","10|200|1|50","space=2");
   zdialog_add_widget(zd,"zspin","countx","vb2","0|100|1|2","space=2");
   zdialog_add_widget(zd,"check","enablex","vb2",0);

   zdialog_add_widget(zd,"label","lab1y","vb3",E2X("y-spacing"));
   zdialog_add_widget(zd,"label","lab2y","vb3",E2X("y-count"));
   zdialog_add_widget(zd,"label","lab4y","vb3",E2X("y-enable"));

   zdialog_add_widget(zd,"zspin","spacey","vb4","10|200|1|50");
   zdialog_add_widget(zd,"zspin","county","vb4","0|100|1|2");
   zdialog_add_widget(zd,"check","enabley","vb4",0);

   zdialog_add_widget(zd,"hbox","hboffx","dialog");
   zdialog_add_widget(zd,"label","lab3x","hboffx",Bxoffset,"space=7");
   zdialog_add_widget(zd,"hscale","offsetx","hboffx","0|100|1|0","expand");
   zdialog_add_widget(zd,"label","space","hboffx",0,"space=20");

   zdialog_add_widget(zd,"hbox","hboffy","dialog");
   zdialog_add_widget(zd,"label","lab3y","hboffy",Byoffset,"space=7");
   zdialog_add_widget(zd,"hscale","offsety","hboffy","0|100|1|0","expand");
   zdialog_add_widget(zd,"label","space","hboffy",0,"space=20");

   G = currgrid;                                                                 //  grid logic overhaul

   if (! gridsettings[G][GXS])                                                   //  if [G] never set, get defaults
      for (int ii = 0; ii < 9; ii++)
         gridsettings[G][ii] = gridsettings[0][ii];

   zdialog_stuff(zd,"enablex",gridsettings[G][GX]);                              //  current settings >> dialog widgets
   zdialog_stuff(zd,"enabley",gridsettings[G][GY]);
   zdialog_stuff(zd,"spacex",gridsettings[G][GXS]);
   zdialog_stuff(zd,"spacey",gridsettings[G][GYS]);
   zdialog_stuff(zd,"countx",gridsettings[G][GXC]);
   zdialog_stuff(zd,"county",gridsettings[G][GYC]);
   zdialog_stuff(zd,"offsetx",gridsettings[G][GXF]);
   zdialog_stuff(zd,"offsety",gridsettings[G][GYF]);

   zdialog_set_modal(zd);
   zdialog_run(zd,gridlines_dialog_event,"parent");
   zdialog_wait(zd);
   zdialog_free(zd);
   return;
}


//  dialog event function

int gridlines_dialog_event(zdialog *zd, cchar *event)
{
   int      G = currgrid;

   if (zd->zstat)                                                                //  done or cancel
   {
      if (zd->zstat != 1) return 1;
      if (gridsettings[G][GX] || gridsettings[G][GY])
         gridsettings[G][GON] = 1;
      else gridsettings[G][GON] = 0;
      Fpaint2();
      return 1;
   }

   if (strmatch(event,"enablex"))                                                //  x/y grid enable or disable
      zdialog_fetch(zd,"enablex",gridsettings[G][GX]);

   if (strmatch(event,"enabley"))
      zdialog_fetch(zd,"enabley",gridsettings[G][GY]);

   if (strmatch(event,"spacex"))                                                 //  x/y grid spacing (if counts == 0)
      zdialog_fetch(zd,"spacex",gridsettings[G][GXS]);

   if (strmatch(event,"spacey"))
      zdialog_fetch(zd,"spacey",gridsettings[G][GYS]);

   if (strmatch(event,"countx"))                                                 //  x/y grid line counts
      zdialog_fetch(zd,"countx",gridsettings[G][GXC]);

   if (strmatch(event,"county"))
      zdialog_fetch(zd,"county",gridsettings[G][GYC]);

   if (strmatch(event,"offsetx"))                                                //  x/y grid starting offsets
      zdialog_fetch(zd,"offsetx",gridsettings[G][GXF]);

   if (strmatch(event,"offsety"))
      zdialog_fetch(zd,"offsety",gridsettings[G][GYF]);

   if (gridsettings[G][GX] || gridsettings[G][GY])                               //  if either grid enabled, show grid
      gridsettings[G][GON] = 1;

   Fpaint2();
   return 1;
}


//  toggle grid lines on or off
//  action: 0 = off, 1 = on, 2 = toggle: on > off, off > on

void toggle_grid(int action)
{
   int   G = currgrid;

   if (action == 0) gridsettings[G][GON] = 0;                                    //  grid off
   if (action == 1) gridsettings[G][GON] = 1;                                    //  grid on
   if (action == 2) gridsettings[G][GON] = 1 - gridsettings[G][GON];             //  toggle grid

   if (gridsettings[G][GON])
      if (! gridsettings[G][GX] && ! gridsettings[G][GY])                        //  if grid on and x/y both off,
         gridsettings[G][GX] = gridsettings[G][GY] = 1;                          //    set both grids on

   Fpaint2();
   return;
}


/********************************************************************************/

//  choose color for foreground lines
//  (area outline, mouse circle, trim rectangle ...)

void m_line_color(GtkWidget *, cchar *menu)
{
   int line_color_dialog_event(zdialog *zd, cchar *event);

   zdialog  *zd;

   F1_help_topic = "line_color";
   m_viewmode(0,"F");                                                            //  file view mode                     19.0

   zd = zdialog_new(E2X("Line Color"),Mwin,null);
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"radio",Bblack,"hb1",Bblack,"space=3");                 //  add radio button per color
   zdialog_add_widget(zd,"radio",Bwhite,"hb1",Bwhite,"space=3");
   zdialog_add_widget(zd,"radio",Bred,"hb1",Bred,"space=3");
   zdialog_add_widget(zd,"radio",Bgreen,"hb1",Bgreen,"space=3");

   zdialog_stuff(zd,Bblack,0);                                                   //  all are initially off
   zdialog_stuff(zd,Bwhite,0);
   zdialog_stuff(zd,Bred,0);
   zdialog_stuff(zd,Bgreen,0);

   if (LINE_COLOR[0] == BLACK[0] && LINE_COLOR[1] == BLACK[1] && LINE_COLOR[2] == BLACK[2])                         //  19.0
      zdialog_stuff(zd,Bblack,1);
   if (LINE_COLOR[0] == WHITE[0] && LINE_COLOR[1] == WHITE[1] && LINE_COLOR[2] == WHITE[2]) 
      zdialog_stuff(zd,Bwhite,1);
   if (LINE_COLOR[0] == RED[0] && LINE_COLOR[1] == RED[1] && LINE_COLOR[2] == RED[2]) 
      zdialog_stuff(zd,Bred,1);
   if (LINE_COLOR[0] == GREEN[0] && LINE_COLOR[1] == GREEN[1] && LINE_COLOR[2] == GREEN[2]) 
      zdialog_stuff(zd,Bgreen,1);

   zdialog_run(zd,line_color_dialog_event,"save");                               //  run dialog, parallel
   return;
}


//  dialog event and completion function

int line_color_dialog_event(zdialog *zd, cchar *event)
{
   if (strmatch(event,Bblack)) memcpy(LINE_COLOR,BLACK,3*sizeof(int));           //  set selected color                 19.0
   if (strmatch(event,Bwhite)) memcpy(LINE_COLOR,WHITE,3*sizeof(int));
   if (strmatch(event,Bred))   memcpy(LINE_COLOR,RED,3*sizeof(int));
   if (strmatch(event,Bgreen)) memcpy(LINE_COLOR,GREEN,3*sizeof(int));
   if (CEF && CEF->zd) zdialog_send_event(CEF->zd,"line_color");
   Fpaint2();

   if (zd->zstat) zdialog_free(zd);                                              // [x] button
   return 1;
}


/********************************************************************************/

//  dark_brite menu function
//  highlight darkest and brightest pixels

namespace darkbrite {
   float    darklim = 0;
   float    brightlim = 255;
}

void m_darkbrite(GtkWidget *, const char *)
{
   using namespace darkbrite;

   int    darkbrite_dialog_event(zdialog* zd, const char *event);

   cchar    *title = E2X("Darkest and Brightest Pixels");

   F1_help_topic = "darkbrite_pixels";

   m_viewmode(0,"F");                                                            //  file view mode                     19.0
   if (! curr_file) return;                                                      //  no image file

/**
       ______________________________________
      |    Darkest and Brightest Pixels      |
      |                                      |
      |  Dark Limit   ===[]============ NNN  |
      |  Bright Limit ============[]=== NNN  |
      |                                      |
      |                              [Done]  |
      |______________________________________|

**/

   zdialog *zd = zdialog_new(title,Mwin,Bdone,null);                             //  darkbrite dialog
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=5");
   zdialog_add_widget(zd,"vbox","vb1","hb1",0,"space=3|homog");
   zdialog_add_widget(zd,"vbox","vb2","hb1",0,"space=3|homog|expand");
   zdialog_add_widget(zd,"vbox","vb3","hb1",0,"homog|space=3");
   zdialog_add_widget(zd,"label","labD","vb1",E2X("Dark Limit"));
   zdialog_add_widget(zd,"label","labB","vb1",E2X("Bright Limit"));
   zdialog_add_widget(zd,"hscale","limD","vb2","0|255|1|0","expand");
   zdialog_add_widget(zd,"hscale","limB","vb2","0|255|1|255","expand");
   zdialog_add_widget(zd,"label","valD","vb3");
   zdialog_add_widget(zd,"label","valB","vb3");

   zdialog_rescale(zd,"limD",0,0,255);
   zdialog_rescale(zd,"limB",0,255,255);
   
   zdialog_stuff(zd,"limD",darklim);                                             //  start with prior values
   zdialog_stuff(zd,"limB",brightlim);

   zdialog_resize(zd,300,0);
   zdialog_run(zd,darkbrite_dialog_event,"save");                                //  run dialog - parallel
   zd_darkbrite = zd;                                                            //  global pointer for Fpaint*()

   zdialog_send_event(zd,"limD");                                                //  initz. NNN labels
   zdialog_send_event(zd,"limB");

   return;
}


//  darkbrite dialog event and completion function

int darkbrite_dialog_event(zdialog *zd, const char *event)                       //  darkbrite dialog event function
{
   using namespace darkbrite;

   char     text[8];

   if (zd->zstat)
   {
      zdialog_free(zd);
      zd_darkbrite = 0;
      Fpaint2();
      return 0;
   }

   if (strmatch(event,"limD")) {
      zdialog_fetch(zd,"limD",darklim);
      snprintf(text,8,"%.0f",darklim);
      zdialog_stuff(zd,"valD",text);
   }

   if (strmatch(event,"limB")) {
      zdialog_fetch(zd,"limB",brightlim);
      snprintf(text,8,"%.0f",brightlim);
      zdialog_stuff(zd,"valB",text);
   }

   Fpaint2();
   return 0;
}


//  this function called by Fpaint() if zd_darkbrite dialog active

void darkbrite_paint()
{
   using namespace darkbrite;

   int         px, py;
   uint8       *pix;
   float       P, D = darklim, B = brightlim;

   for (py = 0; py < Mpxb->hh; py++)                                             //  loop all image pixels
   for (px = 0; px < Mpxb->ww; px++)
   {
      pix = PXBpix(Mpxb,px,py);
      P = pixbright(pix);
      if (P < D) pix[0] = 100;                                                   //  dark pixel = mostly red
      else if (P > B) pix[0] = 0;                                                //  bright pixel = mostly green/blue
   }

   return;                                                                       //  not executed, avoid gcc warning
}


/********************************************************************************/

//  find and remove "dust" from an image (e.g. from a scanned dusty slide)
//  dust is defined as small dark areas surrounded by brighter areas
//  image 1   original with prior edits
//  image 3   accumulated dust removals that have been comitted
//  image 9   comitted dust removals + pending removal (work in process)

namespace dust_names
{
   editfunc    EFdust;

   int         spotspan;                                                         //  max. dustspot span, pixels
   int         spotspan2;                                                        //  spotspan **2
   float       brightness;                                                       //  brightness limit, 0 to 1 = white
   float       contrast;                                                         //  min. contrast, 0 to 1 = black/white
   int         *pixgroup;                                                        //  maps (px,py) to pixel group no.
   int         Fred;                                                             //  red pixels are on

   int         Nstack;

   struct spixstack {
      uint16      px, py;                                                        //  pixel group search stack
      uint16      direc;
   }  *pixstack;

   #define maxgroups 1000000
   int         Ngroups;
   int         groupcount[maxgroups];                                            //  count of pixels in each group
   float       groupbright[maxgroups];
   int         edgecount[maxgroups];                                             //  group edge pixel count
   float       edgebright[maxgroups];                                            //  group edge pixel brightness sum

   typedef struct {
      uint16      px1, py1, px2, py2;                                            //  pixel group extreme pixels
      int         span2;                                                         //  span from px1/py1 to px2/py2
   }  sgroupspan;

   sgroupspan    groupspan[maxgroups];
}


//  menu function

void m_remove_dust(GtkWidget *, const char *)
{
   using namespace dust_names;

   int    dust_dialog_event(zdialog *zd, cchar *event);
   void * dust_thread(void *);

   F1_help_topic = "remove_dust";
   m_viewmode(0,"F");                                                            //  file view mode                     19.0

   EFdust.menufunc = m_remove_dust;
   EFdust.funcname = "remove_dust";
   EFdust.Farea = 2;                                                             //  select area usable
   EFdust.Frestart = 1;                                                          //  restart allowed
   EFdust.threadfunc = dust_thread;                                              //  thread function
   if (! edit_setup(EFdust)) return;                                             //  setup edit

   E9pxm = PXM_copy(E3pxm);                                                      //  image 9 = copy of image3
   Fred = 0;

   int cc = E1pxm->ww * E1pxm->hh * sizeof(int);
   pixgroup = (int *) zmalloc(cc);                                               //  maps pixels to assigned groups

   cc = E1pxm->ww * E1pxm->hh * sizeof(spixstack);
   pixstack = (spixstack *) zmalloc(cc);                                         //  pixel group search stack

/***
                     Remove Dust

        spot size limit    =========[]===========
        max. brightness    =============[]=======
        min. contrast      ========[]============
        [erase] [red] [undo last] [apply]

                                  [Done] [Cancel]
***/

   zdialog *zd = zdialog_new(E2X("Remove Dust"),Mwin,Bdone,Bcancel,null);
   EFdust.zd = zd;

   zdialog_add_widget(zd,"hbox","hbssl","dialog",0,"space=1");
   zdialog_add_widget(zd,"label","labssl","hbssl",E2X("spot size limit"),"space=5");
   zdialog_add_widget(zd,"hscale","spotspan","hbssl","1|50|1|20","space=5|expand");
   zdialog_add_widget(zd,"hbox","hbmb","dialog",0,"space=1");
   zdialog_add_widget(zd,"label","labmb","hbmb",E2X("max. brightness"),"space=5");
   zdialog_add_widget(zd,"hscale","brightness","hbmb","1|999|1|700","space=5|expand");
   zdialog_add_widget(zd,"hbox","hbmc","dialog",0,"space=1");
   zdialog_add_widget(zd,"label","labmb","hbmc",E2X("min. contrast"),"space=5");
   zdialog_add_widget(zd,"hscale","contrast","hbmc","1|500|1|40","space=5|expand");
   zdialog_add_widget(zd,"hbox","hbbutts","dialog",0,"space=5");
   zdialog_add_widget(zd,"button","erase","hbbutts",Berase,"space=5");
   zdialog_add_widget(zd,"button","red","hbbutts",Bred,"space=5");
   zdialog_add_widget(zd,"button","undo1","hbbutts",Bundolast,"space=5");
   zdialog_add_widget(zd,"button","apply","hbbutts",Bapply,"space=5");

   zdialog_resize(zd,300,0);
   zdialog_restore_inputs(zd);                                                   //  preload prior user inputs

   zdialog_fetch(zd,"spotspan",spotspan);                                        //  max. dustspot span (pixels)
   spotspan2 = spotspan * spotspan;

   zdialog_fetch(zd,"brightness",brightness);                                    //  max. dustspot brightness
   brightness = 0.001 * brightness;                                              //  scale 0 to 1 = white

   zdialog_fetch(zd,"contrast",contrast);                                        //  min. dustspot contrast
   contrast = 0.001 * contrast;                                                  //  scale 0 to 1 = black/white

   zdialog_run(zd,dust_dialog_event,"save");                                     //  run dialog - parallel

   signal_thread();
   return;
}


//  dialog event and completion callback function

int dust_dialog_event(zdialog *zd, cchar *event)
{
   using namespace dust_names;

   void dust_erase();

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()
   
   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  done
         wrapup_thread(8);                                                       //  thread finish, exit
         PXM_free(E3pxm);
         E3pxm = E9pxm;                                                          //  image 3 = image 9
         E9pxm = 0;
         edit_done(0);                                                           //  commit edit
      }
      else {                                                                     //  cancel
         wrapup_thread(8);                                                       //  thread finish, exit
         PXM_free(E9pxm);
         edit_cancel(0);                                                         //  discard edit
      }
      zfree(pixgroup);                                                           //  free memory
      zfree(pixstack);
      return 1;
   }

   if (strstr("spotspan brightness contrast red",event))
   {
      zdialog_fetch(zd,"spotspan",spotspan);                                     //  max. dustspot span (pixels)
      spotspan2 = spotspan * spotspan;

      zdialog_fetch(zd,"brightness",brightness);                                 //  max. dustspot brightness
      brightness = 0.001 * brightness;                                           //  scale 0 to 1 = white

      zdialog_fetch(zd,"contrast",contrast);                                     //  min. dustspot contrast
      contrast = 0.001 * contrast;                                               //  scale 0 to 1 = black/white

      signal_thread();                                                           //  do the work
   }

   if (strmatch(event,"erase")) dust_erase();
   if (strmatch(event,"blendwidth")) dust_erase();

   if (strmatch(event,"undo1")) {
      PXM_free(E3pxm);
      E3pxm = PXM_copy(E9pxm);
      Fred = 0;
      Fpaint2();
   }

   if (strmatch(event,"apply")) {                                                //  button
      if (Fred) dust_erase();
      PXM_free(E9pxm);                                                           //  image 9 = copy of image 3
      E9pxm = PXM_copy(E3pxm);
      CEF->Fmods++;
      CEF->Fsaved = 0;
   }

   return 1;
}


//  dust find thread function - find the dust particles and mark them

void * dust_thread(void *)
{
   using namespace dust_names;

   int         xspan, yspan, span2;
   int         group, cc, ii, kk, Nremoved;
   int         px, py, dx, dy, ppx, ppy, npx, npy;
   float       gbright, pbright, pcontrast;
   float       ff = 1.0 / 256.0;
   uint16      direc;
   float       *pix3;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window updates

      PXM_free(E3pxm);
      E3pxm = PXM_copy(E9pxm);

      paintlock(0);                                                              //  unblock window updates

      cc = E1pxm->ww * E1pxm->hh * sizeof(int);                                  //  clear group arrays
      memset(pixgroup,0,cc);
      cc = maxgroups * sizeof(int);
      memset(groupcount,0,cc);
      memset(edgecount,0,cc);
      cc = maxgroups * sizeof(float );
      memset(groupbright,0,cc);
      memset(edgebright,0,cc);
      cc = maxgroups * sizeof(sgroupspan);
      memset(groupspan,0,cc);

      group = 0;

      for (py = 0; py < E1pxm->hh; py++)                                         //  loop all pixels
      for (px = 0; px < E1pxm->ww; px++)
      {
         ii = py * E1pxm->ww + px;
         if (sa_stat == 3 && ! sa_pixmap[ii]) continue;                          //  not in active area
         if (pixgroup[ii]) continue;                                             //  already assigned to a group

         pix3 = PXMpix(E3pxm,px,py);                                             //  get pixel brightness
         gbright = ff * pixbright(pix3);                                         //  0 to 1.0 = white
         if (gbright > brightness) continue;                                     //  ignore bright pixel

         if (group == maxgroups-1) break;                                        //  too many groups, make no more

         pixgroup[ii] = ++group;                                                 //  assign next group
         groupcount[group] = 1;
         groupbright[group] = gbright;

         pixstack[0].px = px;                                                    //  put pixel into stack with
         pixstack[0].py = py;                                                    //    direction = ahead
         pixstack[0].direc = 0;
         Nstack = 1;

         while (Nstack)
         {
            kk = Nstack - 1;                                                     //  get last pixel in stack
            px = pixstack[kk].px;
            py = pixstack[kk].py;
            direc = pixstack[kk].direc;                                          //  next search direction

            if (direc == 'x') {
               Nstack--;                                                         //  none left
               continue;
            }

            if (Nstack > 1) {
               ii = Nstack - 2;                                                  //  get prior pixel in stack
               ppx = pixstack[ii].px;
               ppy = pixstack[ii].py;
            }
            else {
               ppx = px - 1;                                                     //  if only one, assume prior = left
               ppy = py;
            }

            dx = px - ppx;                                                       //  vector from prior to this pixel
            dy = py - ppy;

            switch (direc)
            {
               case 0:
                  npx = px + dx;
                  npy = py + dy;
                  pixstack[kk].direc = 1;
                  break;

               case 1:
                  npx = px + dy;
                  npy = py + dx;
                  pixstack[kk].direc = 3;
                  break;

               case 2:
                  npx = px - dx;                                                 //  back to prior pixel
                  npy = py - dy;                                                 //  (this path never taken)
                  zappcrash("stack search bug");
                  break;

               case 3:
                  npx = px - dy;
                  npy = py - dx;
                  pixstack[kk].direc = 4;
                  break;

               case 4:
                  npx = px - dx;
                  npy = py + dy;
                  pixstack[kk].direc = 5;
                  break;

               case 5:
                  npx = px - dy;
                  npy = py + dx;
                  pixstack[kk].direc = 6;
                  break;

               case 6:
                  npx = px + dx;
                  npy = py - dy;
                  pixstack[kk].direc = 7;
                  break;

               case 7:
                  npx = px + dy;
                  npy = py - dx;
                  pixstack[kk].direc = 'x';
                  break;

               default:
                  npx = npy = 0;
                  zappcrash("stack search bug");
            }

            if (npx < 0 || npx > E1pxm->ww-1) continue;                          //  pixel off the edge
            if (npy < 0 || npy > E1pxm->hh-1) continue;

            ii = npy * E1pxm->ww + npx;
            if (pixgroup[ii]) continue;                                          //  pixel already assigned
            if (sa_stat == 3 && ! sa_pixmap[ii]) continue;                       //  pixel outside area

            pix3 = PXMpix(E3pxm,npx,npy);                                        //  pixel brightness
            pbright = ff * pixbright(pix3);
            if (pbright > brightness) continue;                                  //  brighter than limit

            pixgroup[ii] = group;                                                //  assign pixel to group
            ++groupcount[group];                                                 //  count pixels in group
            groupbright[group] += pbright;                                       //  sum brightness for group

            kk = Nstack++;                                                       //  put pixel into stack
            pixstack[kk].px = npx;
            pixstack[kk].py = npy;
            pixstack[kk].direc = 0;                                              //  search direction
         }
      }

      Ngroups = group;                                                           //  group numbers are 1-Ngroups
      Nremoved = 0;

      for (py = 0; py < E1pxm->hh; py++)                                         //  loop all pixels
      for (px = 0; px < E1pxm->ww; px++)
      {
         ii = py * E1pxm->ww + px;
         group = pixgroup[ii];
         if (! group) continue;
         if (groupspan[group].px1 == 0) {                                        //  first pixel found in this group
            groupspan[group].px1 = px;                                           //  group px1/py1 = this pixel
            groupspan[group].py1 = py;
            continue;
         }
         xspan = groupspan[group].px1 - px;                                      //  span from group px1/py1 to this pixel
         yspan = groupspan[group].py1 - py;
         span2 = xspan * xspan + yspan * yspan;
         if (span2 > groupspan[group].span2) {
            groupspan[group].span2 = span2;                                      //  if greater, group px2/py2 = this pixel
            groupspan[group].px2 = px;
            groupspan[group].py2 = py;
         }
      }

      for (py = 0; py < E1pxm->hh; py++)                                         //  loop all pixels
      for (px = 0; px < E1pxm->ww; px++)
      {
         ii = py * E1pxm->ww + px;
         group = pixgroup[ii];
         if (! group) continue;
         if (groupspan[group].span2 > spotspan2) continue;
         xspan = groupspan[group].px2 - px;                                      //  span from this pixel to group px2/py2
         yspan = groupspan[group].py2 - py;
         span2 = xspan * xspan + yspan * yspan;
         if (span2 > groupspan[group].span2) {
            groupspan[group].span2 = span2;                                      //  if greater, group px1/py1 = this pixel
            groupspan[group].px1 = px;
            groupspan[group].py1 = py;
         }
      }

      for (py = 0; py < E1pxm->hh; py++)                                         //  loop all pixels
      for (px = 0; px < E1pxm->ww; px++)
      {
         ii = py * E1pxm->ww + px;                                               //  eliminate group if span > limit
         group = pixgroup[ii];
         if (! group) continue;
         if (! groupcount[group]) pixgroup[ii] = 0;
         else if (groupspan[group].span2 > spotspan2) {
            pixgroup[ii] = 0;
            groupcount[group] = 0;
            Nremoved++;
         }
      }

      for (py = 1; py < E1pxm->hh-1; py++)                                       //  loop all pixels except image edges
      for (px = 1; px < E1pxm->ww-1; px++)
      {
         ii = py * E1pxm->ww + px;
         group = pixgroup[ii];
         if (group) continue;                                                    //  find pixels bordering group pixels
         pix3 = PXMpix(E3pxm,px,py);
         pbright = ff * pixbright(pix3);

         group = pixgroup[ii-E1pxm->ww-1];
         if (group) {
            ++edgecount[group];                                                  //  accumulate pixel count and
            edgebright[group] += pbright;                                        //      bordering the groups
         }

         group = pixgroup[ii-E1pxm->ww];
         if (group) {
            ++edgecount[group];
            edgebright[group] += pbright;
         }

         group = pixgroup[ii-E1pxm->ww+1];
         if (group) {
            ++edgecount[group];
            edgebright[group] += pbright;
         }

         group = pixgroup[ii-1];
         if (group) {
            ++edgecount[group];
            edgebright[group] += pbright;
         }

         group = pixgroup[ii+1];
         if (group) {
            ++edgecount[group];
            edgebright[group] += pbright;
         }

         group = pixgroup[ii+E1pxm->ww-1];
         if (group) {
            ++edgecount[group];
            edgebright[group] += pbright;
         }

         group = pixgroup[ii+E1pxm->ww];
         if (group) {
            ++edgecount[group];
            edgebright[group] += pbright;
         }

         group = pixgroup[ii+E1pxm->ww+1];
         if (group) {
            ++edgecount[group];
            edgebright[group] += pbright;
         }
      }

      for (group = 1; group <= Ngroups; group++)                                 //  compute group pixel and edge pixel
      {                                                                          //    mean brightness
         if (groupcount[group] && edgecount[group]) {
            edgebright[group] = edgebright[group] / edgecount[group];
            groupbright[group] = groupbright[group] / groupcount[group];
            pcontrast = edgebright[group] - groupbright[group];                  //  edge - group contrast
            if (pcontrast < contrast) {
               groupcount[group] = 0;
               Nremoved++;
            }
         }
      }

      for (py = 0; py < E1pxm->hh; py++)                                         //  loop all pixels
      for (px = 0; px < E1pxm->ww; px++)
      {
         ii = py * E1pxm->ww + px;                                               //  eliminate group if low contrast
         group = pixgroup[ii];
         if (! group) continue;
         if (! groupcount[group]) pixgroup[ii] = 0;
      }

      for (py = 0; py < E1pxm->hh; py++)                                         //  loop all pixels
      for (px = 0; px < E1pxm->ww; px++)
      {
         ii = py * E1pxm->ww + px;
         if (! pixgroup[ii]) continue;                                           //  not a dust pixel
         pix3 = PXMpix(E3pxm,px,py);                                             //  paint it red
         pix3[0] = 255;
         pix3[1] = pix3[2] = 0;
      }

      Fred = 1;

      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


//  erase the selected dust areas

void dust_erase()
{
   using namespace dust_names;

   int         cc, ii, px, py, inc;
   int         qx, qy, npx, npy;
   int         sx, sy, tx, ty;
   int         rad, dist, dist2, mindist2;
   float       slope, f1, f2;
   float       *pix1, *pix3;
   char        *pmap;
   int         nc = E1pxm->nc, pcc = nc * sizeof(float);

   Ffuncbusy = 1;
   PXM_free(E3pxm);                                                              //  not a thread
   E3pxm = PXM_copy(E9pxm);

   cc = E1pxm->ww * E1pxm->hh;                                                   //  allocate pixel done map
   pmap = (char *) zmalloc(cc);
   memset(pmap,0,cc);

   for (py = 0; py < E1pxm->hh; py++)                                            //  loop all pixels
   for (px = 0; px < E1pxm->ww; px++)
   {
      ii = py * E1pxm->ww + px;
      if (! pixgroup[ii]) continue;                                              //  not a dust pixel
      if (pmap[ii]) continue;                                                    //  skip pixels already done

      mindist2 = 999999;
      npx = npy = 0;

      for (rad = 1; rad < 10; rad++)                                             //  find nearest edge (10 pixel limit)
      {
         for (qx = px-rad; qx <= px+rad; qx++)                                   //  search within rad
         for (qy = py-rad; qy <= py+rad; qy++)
         {
            if (qx < 0 || qx >= E1pxm->ww) continue;                             //  off image edge
            if (qy < 0 || qy >= E1pxm->hh) continue;
            ii = qy * E1pxm->ww + qx;
            if (pixgroup[ii]) continue;                                          //  within dust area

            dist2 = (px-qx) * (px-qx) + (py-qy) * (py-qy);                       //  distance**2 to edge pixel
            if (dist2 < mindist2) {
               mindist2 = dist2;
               npx = qx;                                                         //  save nearest pixel found
               npy = qy;
            }
         }

         if (rad * rad >= mindist2) break;                                       //  can quit now
      }

      if (! npx && ! npy) continue;                                              //  should not happen

      qx = npx;                                                                  //  nearest edge pixel
      qy = npy;

      if (abs(qy - py) > abs(qx - px)) {                                         //  qx/qy = near edge from px/py
         slope = 1.0 * (qx - px) / (qy - py);
         if (qy > py) inc = 1;
         else inc = -1;
         for (sy = py; sy != qy+inc; sy += inc)                                  //  line from px/py to qx/qy
         {
            sx = px + slope * (sy - py);
            ii = sy * E1pxm->ww + sx;
            if (pmap[ii]) continue;
            pmap[ii] = 1;
            tx = qx + (qx - sx);                                                 //  tx/ty = parallel line from qx/qy
            ty = qy + (qy - sy);
            if (tx < 0) tx = 0;
            if (tx > E1pxm->ww-1) tx = E1pxm->ww-1;
            if (ty < 0) ty = 0;
            if (ty > E1pxm->hh-1) ty = E1pxm->hh-1;
            pix1 = PXMpix(E3pxm,tx,ty);                                          //  copy pixel from tx/ty to sx/sy
            pix3 = PXMpix(E3pxm,sx,sy);
            memcpy(pix3,pix1,pcc);
         }
      }

      else {
         slope = 1.0 * (qy - py) / (qx - px);
         if (qx > px) inc = 1;
         else inc = -1;
         for (sx = px; sx != qx+inc; sx += inc)
         {
            sy = py + slope * (sx - px);
            ii = sy * E1pxm->ww + sx;
            if (pmap[ii]) continue;
            pmap[ii] = 1;
            tx = qx + (qx - sx);
            ty = qy + (qy - sy);
            if (tx < 0) tx = 0;
            if (tx > E1pxm->ww-1) tx = E1pxm->ww-1;
            if (ty < 0) ty = 0;
            if (ty > E1pxm->hh-1) ty = E1pxm->hh-1;
            pix1 = PXMpix(E3pxm,tx,ty);
            pix3 = PXMpix(E3pxm,sx,sy);
            memcpy(pix3,pix1,pcc);
         }
      }
   }

   zfree(pmap);

   if (sa_stat == 3)                                                             //  area edge blending
   {
      for (ii = 0; ii < E1pxm->ww * E1pxm->hh; ii++)                             //  find pixels in select area
      {
         dist = sa_pixmap[ii];
         if (! dist || dist >= sa_blendwidth) continue;

         py = ii / E1pxm->ww;
         px = ii - py * E1pxm->ww;
         pix1 = PXMpix(E1pxm,px,py);                                             //  input pixel, unchanged image
         pix3 = PXMpix(E3pxm,px,py);                                             //  output pixel, changed image

         f2 = sa_blendfunc(dist);
         f1 = 1.0 - f2;

         pix3[0] = f1 * pix1[0] + f2 * pix3[0];                                  //  blend the pixels
         pix3[1] = f1 * pix1[1] + f2 * pix3[1];
         pix3[2] = f1 * pix1[2] + f2 * pix3[2];
      }
   }

   Fred = 0;
   Ffuncbusy = 0;
   Fpaint2();                                                                    //  update window
   return;
}


/********************************************************************************/

//  Find and fix stuck pixels (always bright or dark) from camera sensor defects.

namespace stuckpix_names
{
   editfunc    EFstuckpix;
   int         stuckpix_1x1, stuckpix_2x2, stuckpix_3x3;                         //  pixel blocks to search
   cchar       *stuckpix_mode;                                                   //  find or fix
   float       stuckpix_threshcon;                                               //  min. contrast threshold
   char        *stuckpix_file = 0;                                               //  file for saved stuck pixels

   struct stuckpix_t {                                                           //  memory for stuck pixels
      int      px, py;                                                           //  location (NW corner of block)
      int      size;                                                             //  size: 1/2/3 = 1x1/2x2/3x3 block
      float    pcon;                                                             //  contrast with surrounding pixels
      int      rgb[3];                                                           //  surrounding pixel mean RGB
   };
   stuckpix_t  stuckpix[1000];
   int         maxstuck = 1000, Nstuck;
}


//  menu function

void m_stuck_pixels(GtkWidget *, cchar *menu)
{
   using namespace stuckpix_names;

   int    stuckpix_dialog_event(zdialog *zd, cchar *event);
   void * stuckpix_thread(void *);

   zdialog     *zd;

   F1_help_topic = "stuck_pixels";
   m_viewmode(0,"F");                                                            //  file view mode                     19.0

   EFstuckpix.menufunc = m_stuck_pixels;
   EFstuckpix.funcname = "stuck_pixels";                                         //  function name
   EFstuckpix.threadfunc = stuckpix_thread;                                      //  thread function
   if (! edit_setup(EFstuckpix)) return;                                         //  setup edit

/***
    ________________________________________________
   |         Fix Stuck Pixels                       |
   |                                                |
   |   pixel group   [x] 1x1  [x] 2x2  [x] 3x3      |
   |   contrast      =========[]==================  |
   |   stuck pixels  12                             |
   |                                                |
   |         [open] [save] [apply] [done] [cancel]  |
   |________________________________________________|

***/

   zd = zdialog_new(E2X("Stuck Pixels"),Mwin,Bopen,Bsave,Bapply,Bdone,Bcancel,null);
   EFstuckpix.zd = zd;

   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"vbox","vb1","hb1",0,"space=5|homog");
   zdialog_add_widget(zd,"vbox","vb2","hb1",0,"space=5|expand|homog");
   zdialog_add_widget(zd,"label","labgroup","vb1",E2X("pixel group"));
   zdialog_add_widget(zd,"label","labcont","vb1",Bcontrast);
   zdialog_add_widget(zd,"hbox","hbgroup","vb2");
   zdialog_add_widget(zd,"check","1x1","hbgroup","1x1","space=3");
   zdialog_add_widget(zd,"check","2x2","hbgroup","2x2","space=3");
   zdialog_add_widget(zd,"check","3x3","hbgroup","3x3","space=3");
   zdialog_add_widget(zd,"hscale","contrast","vb2","0.1|1.0|0.001|0.5","expand");
   zdialog_add_widget(zd,"hbox","hb2","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labstuck","hb2",E2X("stuck pixels:"),"space=5");

   zdialog_stuff(zd,"1x1",1);                                                    //  initz. dialog controls
   zdialog_stuff(zd,"2x2",1);
   zdialog_stuff(zd,"3x3",1);

   stuckpix_1x1 = stuckpix_2x2 = stuckpix_3x3 = 1;                               //  corresp. data values
   stuckpix_threshcon = 0.5;

   if (! stuckpix_file) {                                                        //  default file
      stuckpix_file = (char *) zmalloc(200);                                     //  /.../.fotoxx/stuck-pixels
      snprintf(stuckpix_file,200,"%s/stuck-pixels",get_zhomedir());
   }

   zdialog_run(zd,stuckpix_dialog_event,"save");                                 //  run dialog
   zd_thread = zd;                                                               //  setup for thread event

   stuckpix_mode = "find";
   signal_thread();                                                              //  find and show the stuck pixels
   return;
}


//  dialog event function

int stuckpix_dialog_event(zdialog *zd, cchar *event)
{
   using namespace stuckpix_names;

   void  stuckpix_open();
   void  stuckpix_save();
   void  stuckpix_show();

   char  stuck_pixels[50];

   if (strmatch(event,"focus")) return 1;

   if (strmatch(event,"done")) zd->zstat = 3;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 4;                                  //  from f_open()
   
   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  open defects file
         zd->zstat = 0;                                                          //  keep dialog active
         stuckpix_open();
      }
      else if (zd->zstat == 2) {                                                 //  save defects file
         zd->zstat = 0;                                                          //  keep dialog active
         stuckpix_save();
      }
      else if (zd->zstat == 3) {                                                 //  apply                              18.01
         zd->zstat = 0;                                                          //  keep dialog active
         stuckpix_mode = "apply";
         signal_thread();                                                        //  fix the stuck pixels
         erase_topcircles();
      }
      else if (zd->zstat == 4) {                                                 //  done
         zd_thread = 0;
         stuckpix_mode = "apply";
         signal_thread();                                                        //  fix the stuck pixels
         edit_done(0);                                                           //  commit edit
         erase_topcircles();
      }
      else {                                                                     //  cancel
         zd_thread = 0;
         edit_cancel(0);                                                         //  discard edit
         erase_topcircles();
      }
      return 1;
   }

   if (strmatch(event,"stuck pixels")) {                                         //  update count in dialog
      snprintf(stuck_pixels,49,"%s %d",E2X("stuck pixels:"),Nstuck);
      if (Nstuck >= maxstuck) strcat(stuck_pixels,"+");
      zdialog_stuff(zd,"labstuck",stuck_pixels);
   }

   if (strstr("1x1 2x2 3x3 contrast",event)) {
      zdialog_fetch(zd,"1x1",stuckpix_1x1);                                      //  get dialog inputs
      zdialog_fetch(zd,"2x2",stuckpix_2x2);
      zdialog_fetch(zd,"3x3",stuckpix_3x3);
      zdialog_fetch(zd,"contrast",stuckpix_threshcon);
      stuckpix_mode = "find";
      signal_thread();                                                           //  find and show stuck pixels
   }

   return 1;
}


//  load stuck pixel list from a previously saved file

void stuckpix_open()
{
   using namespace stuckpix_names;

   int  stuckpix_open_dialog_event(zdialog *zd, cchar *event);

   zdialog     *zd;

   zd = zdialog_new(E2X("Load Stuck Pixels"),Mwin,Bopen,Bcancel,null);
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","lab1","hb1",E2X("File:"),"space=3");
   zdialog_add_widget(zd,"zentry","file","hb1",0,"expand|size=40");
   zdialog_add_widget(zd,"button","browse","hb1",Bbrowse,"space=5");

   zdialog_stuff(zd,"file",stuckpix_file);

   zdialog_set_modal(zd);
   zdialog_run(zd,stuckpix_open_dialog_event,"save");
   zdialog_wait(zd);
   zdialog_free(zd);

   return;
}


int stuckpix_open_dialog_event(zdialog *zd, cchar *event)
{
   using namespace stuckpix_names;

   char     *pp, file[200];
   FILE     *fid = 0;
   int      zstat, nn, ii, px, py, size;

   if (strmatch(event,"browse")) {
      pp = zgetfile(E2X("Stuck Pixels file"),MWIN,"file",stuckpix_file);
      if (! pp) return 1;
      zdialog_stuff(zd,"file",pp);
      zfree(pp);
   }

   zstat = zd->zstat;                                                            //  completion button

   if (zstat == 1)                                                               //  open
   {
      zdialog_fetch(zd,"file",file,200);                                         //  get file from dialog
      if (stuckpix_file) zfree(stuckpix_file);
      stuckpix_file = zstrdup(file);

      fid = fopen(stuckpix_file,"r");                                            //  open file
      if (! fid) {
         zmessageACK(Mwin,Bfilenotfound);
         return 1;
      }

      nn = fscanf(fid,"stuck pixels px py size");                                //  read headers

      for (ii = 0; ii < maxstuck; ii++)
      {
         nn = fscanf(fid," %5d %5d %5d ",&px,&py,&size);                         //  read stuck pixels data
         if (nn == EOF) break;
         if (nn != 3) break;
         stuckpix[ii].px = px;
         stuckpix[ii].py = py;
         stuckpix[ii].size = size;
         stuckpix[ii].pcon = 0;
      }

      Nstuck = ii;
      fclose(fid);

      if (! Nstuck || nn != EOF)
         zmessageACK(Mwin,E2X("file format error"));

      stuckpix_mode = "file";                                                    //  process pixels
      signal_thread();
   }

   return 1;
}


//  save stuck pixel list to a file or add them to a previous file

void stuckpix_save()                                                             //  simplified
{
   using namespace stuckpix_names;

   char     *file;
   FILE     *fid = 0;
   int      ii, px, py, size;

   file = zgetfile(E2X("Stuck Pixels file"),MWIN,"save",stuckpix_file);
   if (! file) return;

   if (stuckpix_file) zfree(stuckpix_file);
   stuckpix_file = file;

   fid = fopen(stuckpix_file,"w");                                               //  open file
   if (! fid) {
      zmessageACK(Mwin,strerror(errno));
      return;
   }

   fprintf(fid,"stuck pixels \n");                                               //  write headers
   fprintf(fid,"    px    py   size \n");

   for (ii = 0; ii < Nstuck; ii++)                                               //  write stuck pixel data
   {
      px = stuckpix[ii].px;
      py = stuckpix[ii].py;
      size = stuckpix[ii].size;
      fprintf(fid," %5d %5d %5d \n",px,py,size);
   }

   fclose(fid);
   return;
}


//  compare two pixels and return contrast
//  0 = no contrast, 1 = max. contrast (black:white)

inline float  stuckpix_getcon(float rgbA[3], float rgbB[3])
{
   float    match;
   match = PIXMATCH(rgbA,rgbB);                                                  //  0..1 = zero..perfect match
   return (1.0 - match);
}


//  perform the fix function
//  find pixel groups with contrast exceeding the limit
//  replace these pixels with surrounding ones

void * stuckpix_thread(void *)
{
   using namespace stuckpix_names;

   void   stuckpix_pixelblock(int px, int py, int size, float rgb[3]);
   void   stuckpix_surroundings(int px, int py, int size, float rgb[3]);
   void   stuckpix_insert(int px, int py, int size, float pcon, float rgb[3]);
   void   stuckpix_show();

   int         px, py, qx, qy, ii, size;
   float       rgbA[3], rgbB[3];
   float       threshcon, pcon;
   float       *ppix;

   while (true)
   {
      thread_idle_loop();

      if (strmatch(stuckpix_mode,"find"))                                        //  find stuck pixels in image
      {
         threshcon = stuckpix_threshcon;                                         //  threshold contrast, 0.1 to 1.0
         Nstuck = 0;                                                             //  count stuck pixels found

         if (stuckpix_3x3)                                                       //  find 3x3 pixel groups FIRST
         {
            for (py = 2; py < E3pxm->hh-5; py++)
            for (px = 2; px < E3pxm->ww-5; px++)
            {
               stuckpix_pixelblock(px,py,3,rgbA);                                //  get mean RGB for 3x3 pixel block
               stuckpix_surroundings(px,py,3,rgbB);                              //  get surrounding pixels mean RGB
               pcon = stuckpix_getcon(rgbA,rgbB);                                //  contrast with surrounding
               if (pcon > threshcon)
                  stuckpix_insert(px,py,3,pcon,rgbB);                            //  if > threshold, add to table
               if (Nstuck == maxstuck) goto findquit;
            }
         }

         if (stuckpix_2x2)                                                       //  find 2x2 pixel groups
         {
            for (py = 2; py < E3pxm->hh-4; py++)
            for (px = 2; px < E3pxm->ww-4; px++)
            {
               stuckpix_pixelblock(px,py,2,rgbA);                                //  get mean RGB for 2x2 pixel block
               stuckpix_surroundings(px,py,2,rgbB);                              //  get surrounding pixels mean RGB
               pcon = stuckpix_getcon(rgbA,rgbB);                                //  contrast with surrounding
               if (pcon > threshcon)
                  stuckpix_insert(px,py,2,pcon,rgbB);                            //  if > threshold, add to table
               if (Nstuck == maxstuck) goto findquit;
            }
         }

         if (stuckpix_1x1)                                                       //  find 1x1 pixel groups LAST
         {
            for (py = 2; py < E3pxm->hh-3; py++)
            for (px = 2; px < E3pxm->ww-3; px++)
            {
               stuckpix_pixelblock(px,py,1,rgbA);                                //  get mean RGB for 1x1 pixel block
               stuckpix_surroundings(px,py,1,rgbB);                              //  get surrounding pixels mean RGB
               pcon = stuckpix_getcon(rgbA,rgbB);                                //  contrast with surrounding
               if (pcon > threshcon)
                  stuckpix_insert(px,py,1,pcon,rgbB);                            //  if > threshold, add to table
               if (Nstuck == maxstuck) goto findquit;
            }
         }

         findquit:
         stuckpix_show();                                                        //  show the stuck pixels found
      }

      if (strmatch(stuckpix_mode,"file"))                                        //  process stuck pixels read from a file
      {
         for (ii = 0; ii < Nstuck; ii++)
         {
            px = stuckpix[ii].px;
            py = stuckpix[ii].py;
            size = stuckpix[ii].size;

            stuckpix_pixelblock(px,py,size,rgbA);
            stuckpix_surroundings(px,py,size,rgbB);
            pcon = stuckpix_getcon(rgbA,rgbB);

            stuckpix[ii].pcon = pcon;
            stuckpix[ii].rgb[0] = rgbB[0];
            stuckpix[ii].rgb[1] = rgbB[1];
            stuckpix[ii].rgb[2] = rgbB[2];
         }

         stuckpix_show();                                                        //  show the stuck pixels read
      }

      if (strmatch(stuckpix_mode,"apply"))                                       //  replace the stuck pixels
      {
         for (ii = 0; ii < Nstuck; ii++)                                         //  loop pixel groups found
         {
            px = stuckpix[ii].px;
            py = stuckpix[ii].py;
            size = stuckpix[ii].size;
            if (! size) continue;

            if (size == 1)                                                       //  1x1 pixel group
            {
               ppix = PXMpix(E3pxm,px,py);                                       //  replace pixel group
               ppix[0] = stuckpix[ii].rgb[0];
               ppix[1] = stuckpix[ii].rgb[1];
               ppix[2] = stuckpix[ii].rgb[2];
            }

            if (size == 2)                                                       //  2x2 pixel group
            {
               for (qy = py; qy < py+2; qy++)                                    //  replace pixel group
               for (qx = px; qx < px+2; qx++)
               {
                  ppix = PXMpix(E3pxm,qx,qy);
                  ppix[0] = stuckpix[ii].rgb[0];
                  ppix[1] = stuckpix[ii].rgb[1];
                  ppix[2] = stuckpix[ii].rgb[2];
               }
            }

            if (size == 3)                                                       //  3x3 pixel group
            {
               for (qy = py; qy < py+3; qy++)                                    //  replace pixel group
               for (qx = px; qx < px+3; qx++)
               {
                  ppix = PXMpix(E3pxm,qx,qy);
                  ppix[0] = stuckpix[ii].rgb[0];
                  ppix[1] = stuckpix[ii].rgb[1];
                  ppix[2] = stuckpix[ii].rgb[2];
               }
            }
         }

         CEF->Fmods++;                                                           //  image modified
         CEF->Fsaved = 0;

         Fpaint2();                                                              //  update window
      }
   }

   return 0;
}


//  get the mean RGB values for a block of (defective) pixels

void stuckpix_pixelblock(int px, int py, int size, float rgb[3])
{
   float    *pix1, *pix2, *pix3, *pix4, *pix5, *pix6, *pix7, *pix8, *pix9;

   if (size == 1)                                                                //  1x1 block, 1 pixel
   {
      pix1 = PXMpix(E3pxm,px,py);
      rgb[0] = pix1[0];
      rgb[1] = pix1[1];
      rgb[2] = pix1[2];
   }

   if (size == 2)                                                                //  2x2 block, 4 pixels
   {
      pix1 = PXMpix(E3pxm,px,py);
      pix2 = PXMpix(E3pxm,px+1,py);
      pix3 = PXMpix(E3pxm,px,py+1);
      pix4 = PXMpix(E3pxm,px+1,py+1);
      rgb[0] = (pix1[0] + pix2[0] + pix3[0] + pix4[0]) / 4;
      rgb[1] = (pix1[1] + pix2[1] + pix3[1] + pix4[1]) / 4;
      rgb[2] = (pix1[2] + pix2[2] + pix3[2] + pix4[2]) / 4;
   }

   if (size == 3)                                                                //  3x3 block, 9 pixels
   {
      pix1 = PXMpix(E3pxm,px,py); 
      pix2 = PXMpix(E3pxm,px+1,py);
      pix3 = PXMpix(E3pxm,px+2,py);
      pix4 = PXMpix(E3pxm,px,py+1);
      pix5 = PXMpix(E3pxm,px+1,py+1);
      pix6 = PXMpix(E3pxm,px+2,py+1);
      pix7 = PXMpix(E3pxm,px,py+2);
      pix8 = PXMpix(E3pxm,px+1,py+2);
      pix9 = PXMpix(E3pxm,px+2,py+2);

      rgb[0] = (pix1[0] + pix2[0] + pix3[0] + pix4[0] 
             + pix5[0] + pix6[0] + pix7[0] + pix8[0] + pix9[0]) / 9;
      rgb[1] = (pix1[1] + pix2[1] + pix3[1] + pix4[1] 
             + pix5[1] + pix1[1] + pix7[1] + pix8[1] + pix9[1]) / 9;
      rgb[2] = (pix1[2] + pix2[2] + pix3[2] + pix4[2] 
             + pix5[2] + pix6[2] + pix7[2] + pix8[2] + pix9[2]) / 9;
   }

   return;
}


//  get the mean RGB values for pixels surrounding a given pixel block

void stuckpix_surroundings(int px, int py, int size, float rgb[3])
{
   int      qx, qy, ii;
   float    red, green, blue;
   float    *ppix;

   int   n8x[8] = { -1, 0, 1,-1, 1,-1, 0, 1 };                                   //  8 neighbors of 1x1 group at [0,0]
   int   n8y[8] = { -1,-1,-1, 0, 0, 1, 1, 1 };

   int   n12x[12] = { -1, 0, 1, 2,-1, 2,-1, 2,-1, 0, 1, 2 };                     //  12 neighbors of 2x2 group at [0,0]
   int   n12y[12] = { -1,-1,-1,-1, 0, 0, 1, 1, 2, 2, 2, 2 };

   int   n16x[16] = { -1, 0, 1, 2, 3,-1, 3,-1, 3,-1, 3,-1, 0, 1, 2, 3 };         //  16 neighbors of 3x3 group at [0,0]
   int   n16y[16] = { -1,-1,-1,-1,-1, 0, 0, 1, 1, 2, 2, 3, 3, 3, 3, 3 };

   red = green = blue = 0;

   if (size == 1)
   {
      for (ii = 0; ii < 8; ii++)                                                 //  surrounding 8 pixels
      {
         qx = px + n8x[ii];
         qy = py + n8y[ii];
         ppix = PXMpix(E3pxm,qx,qy);
         red += ppix[0];
         green += ppix[1];
         blue += ppix[2];
      }

      red = red / 8;                                                             //  average surrounding pixels
      green = green / 8;
      blue = blue / 8;
   }

   if (size == 2)
   {
      for (ii = 0; ii < 12; ii++)                                                //  surrounding 12 pixels
      {
         qx = px + n12x[ii];
         qy = py + n12y[ii];
         ppix = PXMpix(E3pxm,qx,qy);
         red += ppix[0];
         green += ppix[1];
         blue += ppix[2];
      }

      red = red / 12;                                                            //  average surrounding pixels
      green = green / 12;
      blue = blue / 12;
   }

   if (size == 3)
   {
      for (ii = 0; ii < 16; ii++)                                                //  surrounding 16 pixels
      {
         qx = px + n16x[ii];
         qy = py + n16y[ii];
         ppix = PXMpix(E3pxm,qx,qy);
         red += ppix[0];
         green += ppix[1];
         blue += ppix[2];
      }

      red = red / 16;                                                            //  average surrounding pixels
      green = green / 16;
      blue = blue / 16;
   }

   rgb[0] = red;
   rgb[1] = green;
   rgb[2] = blue;

   return;
}


//  draw circles around the stuck pixels

void stuckpix_show()
{
   using namespace stuckpix_names;

   int      ii, px, py, size, rad;

   erase_topcircles();                                                           //  erase prior circles

   for (ii = 0; ii < Nstuck; ii++)                                               //  write circles around stuck pixels
   {
      px = stuckpix[ii].px;
      py = stuckpix[ii].py;
      size = stuckpix[ii].size;
      if (! size) continue;
      rad = 8 + 2 * size;
      px += size / 2;
      py += size / 2;
      add_topcircle(px,py,rad);
   }

   zd_thread = EFstuckpix.zd;                                                    //  send stuck pixel count to zdialog
   zd_thread_event = "stuck pixels";

   Fpaint2();                                                                    //  update window
   return;
}


//  Insert a new stuck pixel block into the stuck pixel table.
//  If the new entry touches on a previous entry,
//  then remove the entry with lesser contrast.

void stuckpix_insert(int px, int py, int size, float pcon, float rgb[3])
{
   using namespace stuckpix_names;

   int      ii, px1, py1, px2, py2, px3, py3, px4, py4;

   px1 = px - 1;                                                                 //  "touch" periphery
   py1 = py - 1;
   px2 = px + size + 1;
   py2 = py + size + 1;

   for (ii = 0; ii < Nstuck; ii++)                                               //  loop stuckpix table
   {
      if (stuckpix[ii].size == 0) continue;                                      //  skip deleted entry

      px3 = stuckpix[ii].px;
      py3 = stuckpix[ii].py;
      px4 = px3 + stuckpix[ii].size - 1;
      py4 = py3 + stuckpix[ii].size - 1;

      if ((px1 >= px3 && px1 <= px4 && py1 >= py3 && py1 <= py4) ||              //  test if old entry touches new
          (px2 >= px3 && px2 <= px4 && py2 >= py3 && py2 <= py4) ||
          (px1 >= px3 && px1 <= px4 && py2 >= py3 && py2 <= py4) ||
          (px2 >= px3 && px2 <= px4 && py1 >= py3 && py1 <= py4) ||
          (px3 >= px1 && px3 <= px2 && py3 >= py1 && py3 <= py2) ||
          (px4 >= px1 && px4 <= px2 && py4 >= py1 && py4 <= py2) ||
          (px3 >= px1 && px3 <= px2 && py4 >= py1 && py4 <= py2) ||
          (px4 >= px1 && px4 <= px2 && py3 >= py1 && py3 <= py2))
      {
         if (size < stuckpix[ii].size   &&                                       //  if new touches a larger block,
             pcon < 2.0 * stuckpix[ii].pcon) return;                             //    keep only if contrast much greater

         if (pcon < 1.0 * stuckpix[ii].pcon) return;                             //  keep only if contrast is greater

         stuckpix[ii].size = 0;                                                  //  delete old entry
      }
   }

   for (ii = 0; ii < Nstuck; ii++)                                               //  loop stuckpix table
      if (stuckpix[ii].size == 0) break;                                         //  find first empty slot or last + 1
   if (ii == maxstuck) return;                                                   //  table full

   stuckpix[ii].px = px;                                                         //  replace overlapping entry
   stuckpix[ii].py = py;                                                         //    or add to end of table
   stuckpix[ii].size = size;
   stuckpix[ii].pcon = pcon;
   stuckpix[ii].rgb[0] = rgb[0];
   stuckpix[ii].rgb[1] = rgb[1];
   stuckpix[ii].rgb[2] = rgb[2];

   if (ii == Nstuck) Nstuck++;                                                   //  incr. count if added to end
   return;
}


/********************************************************************************/

//  monitor color and contrast test function

void m_monitor_color(GtkWidget *, cchar *)
{
   char        file[200];
   int         err;
   char        *savecurrfile = 0;
   zdialog     *zd;
   cchar       *message = E2X("Brightness should show a gradual ramp \n"
                              "extending all the way to the edges.");

   F1_help_topic = "monitor_color";
   m_viewmode(0,"F");                                                            //  file view mode                     19.0

   if (checkpend("all")) return;                                                 //  check nothing pending
   Fblock = 1;

   if (curr_file) savecurrfile = zstrdup(curr_file);

   snprintf(file,200,"%s/colorchart.png",get_zimagedir());                       //  color chart .png file

   err = f_open(file,0,0,1);
   if (err) goto restore;

   Fzoom = 1;
   gtk_window_set_title(MWIN,"check monitor");

   zd = zdialog_new("check monitor",Mwin,Bdone,null);                            //  start user dialog
   if (message) {
      zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=5");
      zdialog_add_widget(zd,"label","lab1","hb1",message,"space=5");
   }

   zdialog_resize(zd,300,0);
   zdialog_set_modal(zd);
   zdialog_run(zd,0,"0/0");
   zdialog_wait(zd);                                                             //  wait for dialog complete
   zdialog_free(zd);

restore:

   Fzoom = 0;

   if (savecurrfile) {
      f_open(savecurrfile);
      zfree(savecurrfile);
   }
   else {
      curr_file = 0;
      if (curr_folder) {
         gallery(curr_folder,"init",0);
         gallery(0,"sort",-2);                                                   //  recall sort and position           18.01
      }
   }

   Fblock = 0;
   return;
}


/********************************************************************************/

//  check and adjust monitor gamma

void m_monitor_gamma(GtkWidget *, cchar *)
{
   int   mongamma_dialog_event(zdialog *zd, cchar *event);

   int         err;
   char        gammachart[200];
   zdialog     *zd;
   char        *savecurrfile = 0;

   cchar       *permit = "Chart courtesy of Norman Koren";
   cchar       *website = "http://www.normankoren.com/makingfineprints1A.html#gammachart";

   F1_help_topic = "monitor_gamma";
   m_viewmode(0,"F");                                                            //  file view mode                     19.0

   if (checkpend("all")) return;                                                 //  check nothing pending

   err = shell_quiet("which xgamma");                                            //  check for xgamma
   if (err) {
      zmessageACK(Mwin,"xgamma program is not installed");
      return;
   }

   Fblock = 1;

   if (curr_file) savecurrfile = zstrdup(curr_file);

   snprintf(gammachart,200,"%s/gammachart2.png",get_zimagedir());                //  gamma chart .png file
   err = f_open(gammachart);
   if (err) goto restore;

   Fzoom = 1;                                                                    //  scale 100% (required)
   gtk_window_set_title(MWIN,"monitor gamma");

   zd = zdialog_new("monitor gamma",Mwin,Bdone,null);                            //  start user dialog
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=8");
   zdialog_add_widget(zd,"label","labgamma","hb1","gamma","space=5");
   zdialog_add_widget(zd,"hscale","gamma","hb1","0.6|1.4|0.02|1.0","expand");
   zdialog_add_widget(zd,"hbox","hb2","dialog");
   zdialog_add_widget(zd,"link",permit,"hb2",website);

   zdialog_resize(zd,200,0);
   zdialog_restore_inputs(zd);                                                   //  preload prior user inputs
   zdialog_set_modal(zd);
   zdialog_run(zd,mongamma_dialog_event,"0/0");
   zdialog_wait(zd);                                                             //  wait for dialog complete
   zdialog_free(zd);

restore:

   Fzoom = 0;

   if (savecurrfile) {
      f_open(savecurrfile);                                                      //  back to current file
      zfree(savecurrfile);
   }
   else {
      curr_file = 0;
      if (curr_folder) {
         gallery(curr_folder,"init",0);
         gallery(0,"sort",-2);                                                   //  recall sort and position           18.01
      }
   }

   Fblock = 0;
   return;
}


//  dialog event function

int mongamma_dialog_event(zdialog *zd, cchar *event)
{
   double   gamma;

   if (strmatch(event,"gamma")) {
      zdialog_fetch(zd,"gamma",gamma);
      shell_ack("xgamma -quiet -gamma %.2f",gamma);
   }

   return 0;
}


/********************************************************************************/

//  set GUI language

void m_change_lang(GtkWidget *, cchar *)
{
   #define NL 8

   zdialog     *zd;
   int         ii, cc, val, zstat;
   char        lang1[NL], *pp;

   cchar  *langs[NL] = { "en English", "de German",                              //  english first
                         "ca Catalan", "es Spanish",
                         "fr French", "it Italian", 
                         "pt Portuguese", null  };

   cchar  *title = E2X("Available Translations");

   F1_help_topic = "change_language";

   zd = zdialog_new(E2X("Set Language"),Mwin,Bdone,Bcancel,null);
   zdialog_add_widget(zd,"label","title","dialog",title,"space=5");
   zdialog_add_widget(zd,"hbox","hb1","dialog");
   zdialog_add_widget(zd,"vbox","vb1","hb1");

   for (ii = 0; langs[ii]; ii++)                                                 //  make radio button per language
      zdialog_add_widget(zd,"radio",langs[ii],"vb1",langs[ii]);

   cc = strlen(zfuncs::zlocale);                                                 //  current language
   for (ii = 0; langs[ii]; ii++)                                                 //  match on lc_RC
      if (strmatchN(zfuncs::zlocale,langs[ii],cc)) break;
   if (! langs[ii])
      for (ii = 0; langs[ii]; ii++)                                              //  failed, match on lc alone
         if (strmatchN(zfuncs::zlocale,langs[ii],2)) break;
   if (! langs[ii]) ii = 0;                                                      //  failed, default english
   zdialog_stuff(zd,langs[ii],1);

   zdialog_resize(zd,200,0);
   zdialog_set_modal(zd);
   zdialog_run(zd,0,"mouse");                                                    //  run dialog
   zstat = zdialog_wait(zd);

   for (ii = 0; langs[ii]; ii++) {                                               //  get active radio button
      zdialog_fetch(zd,langs[ii],val);
      if (val) break;
   }

   zdialog_free(zd);                                                             //  kill dialog

   if (zstat != 1) return;                                                       //  user cancel
   if (! val) return;                                                            //  no selection

   strncpy0(lang1,langs[ii],NL);
   pp = strchr(lang1,' ');                                                       //  isolate lc_RC part
   *pp = 0;

   locale = zstrdup(lang1);                                                      //  set parameter 'locale'             19.0
   save_params();
   
   new_session("-p");                                                            //  replace readlink() etc.            19.0
   zsleep(1);                                                                    //  delay before SIGTERM in quitxx()   19.0
   quitxx();                                                                     //  exit

   return;
}


/********************************************************************************/

//  report missing translations in a popup window

void m_untranslated(GtkWidget *, cchar *)
{
   int      ftf = 1, Nmiss = 0;
   cchar    *missing;
   zdialog  *zd2;

   F1_help_topic = "missing_translations";

   if (strmatchN(zfuncs::zlocale,"en",2)) {
      zmessageACK(Mwin,"use a non-English locale");
      return;
   }

   zd2 = popup_report_open("missing translations",Mwin,400,200,0);

   while (true)
   {
      missing = E2X_missing(ftf);
      if (! missing) break;
      popup_report_write(zd2,0,"%s \n",missing);
      Nmiss++;
   }

   popup_report_write(zd2,0,"%d missing translations \n",Nmiss);

   return;
}


/********************************************************************************/

//  Dump CPU usage, zdialog statistics, memory statistics
//  counters are reset

void m_resources(GtkWidget *, cchar *)
{
   F1_help_topic = "resources";
   printz(" CPU time: %.3f seconds \n",CPUtime());
   return;
}


/********************************************************************************/

//  zappcrash test - make a segment fault

void m_zappcrash_test(GtkWidget *, cchar *)                                      //  19.0
{
   printz("zappcrash from menu Zappcrash Test \n");
   zappcrash("zappcrash from menu Zappcrash Test");
   return;
}



