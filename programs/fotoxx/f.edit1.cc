/********************************************************************************

   Fotoxx      edit photos and manage collections

   Copyright 2007-2019 Michael Cornelison
   source code URL: https://kornelix.net
   contact: kornelix@posteo.de

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version. See https://www.gnu.org/licenses

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
   See the GNU General Public License for more details.

*********************************************************************************

   Fotoxx image edit - Edit1 menu functions

   m_trim_rotate           trim/crop and rotate combination
   m_resize                resize (rescale) image
   m_retouch               brightness, contrast, color, white balance
   m_colorbal              white balance, black level, illumination temperature
   m_sharpen               sharpen an image
   m_blur                  blur an image
   m_blur_background       blur background (called via m_blur())
   m_denoise               remove noise from an image
   m_redeyes               remove red-eyes from flash photos
   m_color_mode            convert between color, B&W, sepia, positive, negative
   m_color_sat             adjust color saturation
   m_adjust_RGB            adjust brightness/color using RGB or CMY colors
   m_adjust_HSL            adjust color using HSL model
   m_add_text              add text to an image
   gentext                 create text graphic with attributes and transparency
   m_add_lines             add lines or arrows to an image
   genline                 create line/arrow graphic with attributes and transparency
   m_upright               upright a rotated image
   m_mirror                horizontal or vertical mirror image
   m_paint_edits           paint edit function gradually with the mouse
   m_lever_edits           apply edit function leveraged by RGB level or contrast

*********************************************************************************/

#define EX extern                                                                //  enable extern declarations
#include "fotoxx.h"                                                              //  (variables in fotoxx.h are refs)

/********************************************************************************/

//  trim (crop) and/or rotate an image                                           //  combine trim and rotate
//  preview mode and related code removed

//  fotoxx.h
//    int      trimx1, trimy1, trimx2, trimy2;                                   //  trim rectangle limits
//    int      trimww, trimhh;                                                   //  trim rectangle width and height

namespace trimrotate
{
   int      ptrimx1, ptrimy1, ptrimx2, ptrimy2;                                  //  prior trim rectangle (this image)
   int      ptrimww, ptrimhh;                                                    //  prior trim size (previous image)
   double   trimR;                                                               //  trim ratio, width/height
   double   rotate_goal, rotate_angle;                                           //  target and actual rotation
   int      E0ww, E0hh, E3ww, E3hh;                                              //  full size image and preview size
   int      Fguidelines, guidelineX, guidelineY;                                 //  horz/vert guidelines for rotate
   int      Fcorner, Fside, Frotate;
   int      KBcorner, KBside;
   int      Fmax = 0;

   editfunc EFtrimrotate;

   void   dialog();
   int    dialog_event(zdialog *zd, cchar *event);
   void   trim_customize();
   void   mousefunc();
   void   KBfunc(int key);
   void   trim_limits();
   void   trim_darkmargins();
   void   trim_final();
   void   rotate_func();
   void   drawlines(cairo_t *cr);
   void   autotrim();
}


//  menu function

void m_trim_rotate(GtkWidget *, cchar *menu)
{
   using namespace trimrotate;

   int         ii, xmargin, ymargin;
   cchar       *trim_message = E2X("drag middle to move \n"
                                   "drag corners to resize \n"
                                   "drag right edge to level");
   char        text[20];
   zdialog     *zd;
   
   F1_help_topic = "trim_rotate";

   EFtrimrotate.menufunc = m_trim_rotate;                                        //  menu function
   EFtrimrotate.funcname = "trim/rotate";
   EFtrimrotate.menuname = menu;
   EFtrimrotate.Frestart = 1;                                                    //  allow restart
   EFtrimrotate.Fscript = 1;                                                     //  allow script (rotate=0)
   EFtrimrotate.mousefunc = mousefunc;
   if (! edit_setup(EFtrimrotate)) return;                                       //  setup edit
   
   PXM_addalpha(E0pxm);
   PXM_addalpha(E1pxm);
   PXM_addalpha(E3pxm);

   E0ww = E0pxm->ww;                                                             //  full-size image dimensions
   E0hh = E0pxm->hh;
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

   if (E9pxm) PXM_free(E9pxm);                                                   //  E9 invalid 
   E9pxm = 0;

   if (menu && strmatch(menu,"keep")) {                                          //  keep preset trim rectangle
      trimww = trimx2 - trimx1;                                                  //  (full image scale)
      trimhh = trimy2 - trimy1;
      trimR = 1.0 * trimww / trimhh;                                             //  trim ratio = width/height
   }
   
   else if (menu && strmatch(menu,"max")) {
      trimww = E0ww;                                                             //  initial trim rectangle, 1x image size
      trimhh = E0hh;
      xmargin = ymargin = 0;
      trimx1 = 0;
      trimx2 = E0ww;
      trimy1 = 0;
      trimy2 = E0hh;
      trimR = 1.0 * E0ww / E0hh;                                                 //  trim ratio = width/height
   }   

   else
   {
      trimww = 0.8 * E0ww;                                                       //  initial trim rectangle, 80% image size
      trimhh = 0.8 * E0hh;
      xmargin = 0.5 * (E0ww - trimww);                                           //  set balanced margins
      ymargin = 0.5 * (E0hh - trimhh);
      trimx1 = xmargin;
      trimx2 = E0ww - xmargin;
      trimy1 = ymargin;
      trimy2 = E0hh - ymargin;
      trimR = 1.0 * trimww / trimhh;                                             //  trim ratio = width/height
   }

   ptrimx1 = 0;                                                                  //  set prior trim rectangle
   ptrimy1 = 0;                                                                  //    = 100% of image
   ptrimx2 = E3ww;
   ptrimy2 = E3hh;

   Fcorner = Fside = KBcorner = KBside = Frotate = 0;                            //  nothing underway
   rotate_goal = rotate_angle = 0;                                               //  initially no rotation

/***
       ______________________________________
      |                                      |
      |  drag middle to move                 |                                   //  19.0
      |  drag corners to resize              |
      |  drag right edge to level            |
      | - - - - - - - - - - - - - - - - - -  |
      |  width [____]  height [____]         |
      |  Ratio 1.5   [x] Lock Ratio          |
      | - - - - - - - - - - - - - - - - - -  |
      |  Set Ratio    [Customize]            |
      |  [1:1] [2:1] [3:2] [4:3] [16:9]      |
      | - - - - - - - - - - - - - - - - - -  |
      |  Size: [Max] [Prev] [Invert] [Auto]  |
      | - - - - - - - - - - - - - - - - - -  |
      |  Rotate: degrees [___]  [Auto Level] |
      |  Rotate 90° [###] [###]  180° [###]  |
      |                                      |
      |  [Grid] [Apply] [Done] [Cancel]      |
      |______________________________________|

***/

   zd = zdialog_new(E2X("Trim/Rotate"),Mwin,Bgrid,Bapply,Bdone,Bcancel,null);
   EFtrimrotate.zd = zd;

   zdialog_add_widget(zd,"label","labtrim","dialog",trim_message,"space=3");

   zdialog_add_widget(zd,"hsep","sep1","dialog",0,"space=5");

   zdialog_add_widget(zd,"hbox","hbwh","dialog");
   zdialog_add_widget(zd,"label","labW","hbwh",Bwidth,"space=5");
   zdialog_add_widget(zd,"zspin","width","hbwh","20|30000|1|1000");              //  fotoxx.h
   zdialog_add_widget(zd,"label","space","hbwh",0,"space=5");
   zdialog_add_widget(zd,"label","labH","hbwh",Bheight,"space=5");
   zdialog_add_widget(zd,"zspin","height","hbwh","20|30000|1|600");

   zdialog_add_widget(zd,"hbox","hbrat","dialog");
   zdialog_add_widget(zd,"label","labR","hbrat",E2X("ratio"),"space=5");
   zdialog_add_widget(zd,"label","ratio","hbrat","1.67   ");
   zdialog_add_widget(zd,"check","lock","hbrat",E2X("Lock Ratio"),"space=15");

   zdialog_add_widget(zd,"hbox","hbsz","dialog");
   zdialog_add_widget(zd,"button","max","hbsz",Bmax,"space=5");
   zdialog_add_widget(zd,"button","prev","hbsz",Bprev,"space=5");
   zdialog_add_widget(zd,"button","invert","hbsz",Binvert,"space=5");
   zdialog_add_widget(zd,"button","auto","hbsz",Bauto,"space=5");

   zdialog_add_widget(zd,"hsep","sep2","dialog",0,"space=5");

   zdialog_add_widget(zd,"hbox","hbsr1","dialog");
   zdialog_add_widget(zd,"label","labsr1","hbsr1",E2X("Set Ratio"),"space=5");
   zdialog_add_widget(zd,"button","custom","hbsr1",E2X("Customize"),"space=5");

   zdialog_add_widget(zd,"hbox","hbsr2","dialog",0,"space=2");
   for (ii = 0; ii < 5; ii++)
      zdialog_add_widget(zd,"button",trimbuttons[ii],"hbsr2",trimbuttons[ii],"space=5");
   
   zdialog_add_widget(zd,"hsep","sep3","dialog",0,"space=5");

   zdialog_add_widget(zd,"hbox","hbrot","dialog");
   zdialog_add_widget(zd,"label","labrotate","hbrot",E2X("Degrees:"),"space=5");
   zdialog_add_widget(zd,"zspin","degrees","hbrot","-180|180|0.1|0");
   zdialog_add_widget(zd,"button","level","hbrot",E2X("Auto Level"),"space=10");

   zdialog_add_widget(zd,"hbox","hb90","dialog",0,"space=2");
   zdialog_add_widget(zd,"imagebutt","-90","hb90","rotate-left.png","size=32|space=8");
   zdialog_add_widget(zd,"imagebutt","+90","hb90","rotate-right.png","size=32|space=8");
   zdialog_add_widget(zd,"imagebutt","180","hb90","rotate-180.png","size=32");

   zdialog_add_ttip(zd,"max",E2X("maximize trim box"));
   zdialog_add_ttip(zd,"prev",E2X("use previous size"));
   zdialog_add_ttip(zd,"invert",E2X("invert width/height"));
   zdialog_add_ttip(zd,"auto",E2X("trim transparent edges"));
   zdialog_add_ttip(zd,"lock",E2X("lock width/height ratio"));
   zdialog_add_ttip(zd,"level",E2X("use EXIF data if available"));

   zd = EFtrimrotate.zd;

   zdialog_restore_inputs(zd);                                                   //  restore all prior inputs

   zdialog_fetch(zd,"width",ptrimww);                                            //  preserve prior trim size
   zdialog_fetch(zd,"height",ptrimhh);                                           //    for use by [prev] button
   zdialog_stuff(zd,"width",trimww);                                             //  stuff width, height, ratio as
   zdialog_stuff(zd,"height",trimhh);                                            //    pre-calculated for this image
   snprintf(text,20,"%.3f  ",trimR);
   zdialog_stuff(zd,"ratio",text);
   zdialog_stuff(zd,"degrees",0);

   takeMouse(mousefunc,dragcursor);                                              //  connect mouse function
   currgrid = 1;                                                                 //  use trim/rotate grid
   Fzoom = 0;
   trim_darkmargins();

   zdialog_run(zd,dialog_event,"save");                                          //  run dialog - parallel
   
   if (menu && strmatch(menu,"auto")) zdialog_send_event(zd,"auto");

   if (Fmax) {
      Fmax = 0;
      zdialog_send_event(zd,"max");
   }
   
   return;
}


//  dialog event and completion callback function

int trimrotate::dialog_event(zdialog *zd, cchar *event)
{
   using namespace trimrotate;

   static int  flip = 0;
   int         width, height, delta;
   int         ii, rlock;
   double      r1, r2, ratio = 0;
   char        text[20];
   cchar       *pp;
   cchar       *exifkey[1] = { exif_rollangle_key };
   char        *ppv[1];
   
   if (strmatch(event,"done")) zd->zstat = 3;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 4;                                  //  cancel

   if (strmatch(event,"undo")) {                                                 //  'undo' button was used      bugfix 19.5
      edit_cancel(0);
      m_trim_rotate(0,0);
      return 1;
   }
   
   Fcorner = Fside = KBcorner = KBside = Frotate = 0;                            //  nothing underway

   if (zd->zstat)                                                                //  dialog complete
   {
      if (zd->zstat == 1) {                                                      //  [grid]
         zd->zstat = 0;                                                          //  keep dialog active
         if (! gridsettings[1][GON]) m_gridlines(0,"grid 1");
         else toggle_grid(2);
         return 1;
      }

      if (zd->zstat == 2) {                                                      //  [apply]
         trim_final();
         CEF->Fmods++;
         edit_done(0);
         m_trim_rotate(0,"max");                                                 //  restart edit
         return 1;
      }

      if (zd->zstat == 3) {                                                      //  [done]
         trim_final();
         CEF->Fmods++;
         currgrid = 0;                                                           //  restore normal grid settings
         edit_done(0);
         return 1;
      }

      draw_toplines(2,0);                                                        //  [cancel] - erase trim rectangle
      currgrid = 0;                                                              //  restore normal grid settings
      edit_cancel(0);
      return 1;
   }

   if (strmatch(event,"custom")) {                                               //  [customize] button
      draw_toplines(2,0);                                                        //  erase trim rectangle
      edit_cancel(0);                                                            //  cancel edit
      trim_customize();                                                          //  customize dialog
      m_trim_rotate(0,0);                                                        //  restart edit
      return 1;
   }

   if (strmatch(event,"focus")) {
      takeMouse(mousefunc,dragcursor);                                           //  connect mouse function
      return 1;
   }

   if (strmatch(event,"prev"))                                                   //  use width/height from prior
   {                                                                             //    image trim
      width = ptrimww;
      height = ptrimhh;
      if (width < 20 || height < 20) return 1;                                   //  no value established, ignore
      if (width > E0ww) width = E0ww;
      if (height > E0hh) height = E0hh;
      zdialog_stuff(zd,"width",width);
      zdialog_stuff(zd,"height",height);
      event = "width";                                                           //  process same as manual inputs
   }

   if (strstr("width height",event))                                             //  get direct width/height inputs
   {
      zdialog_fetch(zd,"width",width);                                           //  full image scale
      zdialog_fetch(zd,"height",height);

      zdialog_fetch(zd,"lock",rlock);                                            //  lock ratio on/off

      if (strmatch(event,"width")) {
         if (width > E3ww) width = E3ww;
         if (rlock) {                                                            //  ratio locked
            height = width / trimR + 0.5;                                        //  try to keep ratio
            if (height > E3hh) height = E3hh;
         }
      }

      if (strmatch(event,"height")) {
         if (height > E3hh) height = E3hh;
         if (rlock) {
            width = height * trimR + 0.5;
            if (width > E3ww) width = E3ww;
         }
      }

      flip = 1 - flip;                                                           //  alternates 0, 1, 0, 1 ...

      delta = width - trimww;

      if (delta > 0) {                                                           //  increased width
         trimx1 = trimx1 - delta / 2;                                            //  left and right sides equally
         trimx2 = trimx2 + delta / 2;
         if (delta % 2) {                                                        //  if increase is odd
            trimx1 = trimx1 - flip;                                              //    add 1 alternatively to each side
            trimx2 = trimx2 + 1 - flip;
         }
         if (trimx1 < 0) {                                                       //  add balance to upper limit
            trimx2 = trimx2 - trimx1;
            trimx1 = 0;
         }
         if (trimx2 > E3ww) {                                                    //  add balance to lower limit
            trimx1 = trimx1 - trimx2 + E3ww;
            trimx2 = E3ww;
         }
      }

      if (delta < 0) {                                                           //  decreased width
         trimx1 = trimx1 - delta / 2;
         trimx2 = trimx2 + delta / 2;
         if (delta % 2) {
            trimx1 = trimx1 + flip;
            trimx2 = trimx2 - 1 + flip;
         }
      }

      delta = height - trimhh;

      if (delta > 0) {                                                           //  increased height
         trimy1 = trimy1 - delta / 2;                                            //  top and bottom sides equally
         trimy2 = trimy2 + delta / 2;
         if (delta % 2) {                                                        //  if increase is odd
            trimy1 = trimy1 - flip;                                              //    add 1 alternatively to each side
            trimy2 = trimy2 + 1 - flip;
         }
         if (trimy1 < 0) {
            trimy2 = trimy2 - trimy1;
            trimy1 = 0;
         }
         if (trimy2 > E3hh) {
            trimy1 = trimy1 - trimy2 + E3hh;
            trimy2 = E3hh;
         }
      }

      if (delta < 0) {                                                           //  decreased height
         trimy1 = trimy1 - delta / 2;
         trimy2 = trimy2 + delta / 2;
         if (delta % 2) {
            trimy1 = trimy1 + flip;
            trimy2 = trimy2 - 1 + flip;
         }
      }

      if (trimx1 < 0) trimx1 = 0;                                                //  keep within limits
      if (trimx2 > E3ww) trimx2 = E3ww;                                          //  use ww not ww-1
      if (trimy1 < 0) trimy1 = 0;
      if (trimy2 > E3hh) trimy2 = E3hh;

      width = trimx2 - trimx1;                                                   //  new width and height
      height = trimy2 - trimy1;

      if (width > E3ww) width = E3ww;                                            //  limit to actual size
      if (height > E3hh) height = E3hh;

      trimww = width;                                                            //  new trim size
      trimhh = height;

      zdialog_stuff(zd,"width",width);                                           //  update dialog values
      zdialog_stuff(zd,"height",height);

      if (! rlock)                                                               //  set new ratio if not locked
         trimR = 1.0 * trimww / trimhh;

      snprintf(text,20,"%.3f  ",trimR);                                          //  stuff new ratio
      zdialog_stuff(zd,"ratio",text);

      trim_darkmargins();                                                        //  show trim area in image
      return 1;
   }

   if (strmatch(event,"max"))                                                    //  maximize trim rectangle
   {
      trimx1 = 0;
      trimy1 = 0;
      trimx2 = E3ww;
      trimy2 = E3hh;

      width = E3ww;                                                              //  full scale trim size
      height = E3hh;

      trimww = width;                                                            //  new trim size
      trimhh = height;

      zdialog_stuff(zd,"width",width);                                           //  update dialog values
      zdialog_stuff(zd,"height",height);

      trimR = 1.0 * trimww / trimhh;
      snprintf(text,20,"%.3f  ",trimR);                                          //  stuff new ratio
      zdialog_stuff(zd,"ratio",text);
      zdialog_stuff(zd,"lock",0);                                                //  set no ratio lock

      trim_darkmargins();                                                        //  show trim area in image
      Fpaintnow();
      return 1;
   }

   if (strmatch(event,"invert"))                                                 //  invert width/height dimensions
      ratio = 1.0 / trimR;                                                       //  mark ratio changed

   for (ii = 0; ii < 5; ii++)                                                    //  trim ratio buttons
      if (strmatch(event,trimbuttons[ii])) break;
   if (ii < 5) {
      r1 = r2 = ratio = 0;
      pp = strField(trimratios[ii],':',1);
      if (pp) r1 = atof(pp);
      pp = strField(trimratios[ii],':',2);
      if (pp) r2 = atof(pp);
      if (r1 > 0 && r2 > 0) ratio = r1/r2;
      if (ratio < 0.1 || ratio > 10) ratio = 1.0;
      if (! ratio) return 1;
      zdialog_stuff(zd,"lock",1);                                                //  assume lock is wanted
      trimR = ratio;
   }

   if (ratio)                                                                    //  ratio was changed
   {
      trimR = ratio;

      if (trimx2 - trimx1 > trimy2 - trimy1)                                     //  adjust smaller dimension
         trimy2 = trimy1 + (trimx2 - trimx1) / trimR + 0.5;                      //  round                              19.0
      else
         trimx2 = trimx1 + (trimy2 - trimy1) * trimR + 0.5;

      if (trimx2 > E3ww) {                                                       //  if off the right edge,
         trimx2 = E3ww;                                                          //  adjust height
         trimy2 = trimy1 + (trimx2 - trimx1) / trimR + 0.5;
      }

      if (trimy2 > E3hh) {                                                       //  if off the bottom edge,
         trimy2 = E3hh;                                                          //  adjust width
         trimx2 = trimx1 + (trimy2 - trimy1) * trimR + 0.5;
      }

      trimww = trimx2 - trimx1;                                                  //  new rectangle dimensions
      trimhh = trimy2 - trimy1;

      zdialog_stuff(zd,"width",trimww);                                          //  stuff width, height, ratio
      zdialog_stuff(zd,"height",trimhh);
      snprintf(text,20,"%.3f  ",trimR);
      zdialog_stuff(zd,"ratio",text);

      trim_darkmargins();                                                        //  update trim area in image
      return 1;
   }
   
   if (strmatch(event,"level"))                                                  //  auto level using EXIF RollAngle
   {
      exif_get(curr_file,exifkey,ppv,1);
      if (ppv[0]) {
         rotate_goal = atof(ppv[0]);
         zfree(ppv[0]);
         rotate_func();                                                          //  E3 is rotated E1
      }
   }

   if (strstr("+90 -90 180 degrees",event))                                      //  rotate action
   {
      if (strmatch(event,"+90"))                                                 //  90 deg. CW
         rotate_goal += 90;

      if (strmatch(event,"-90"))                                                 //  90 deg. CCW
         rotate_goal -= 90;

      if (strmatch(event,"180"))                                                 //  180 deg. (upside down)
         rotate_goal += 180;

      if (strmatch(event,"degrees"))                                             //  degrees adjustment
         zdialog_fetch(zd,"degrees",rotate_goal);
      
      if (rotate_goal > 180) rotate_goal -= 360;                                 //  keep within -180 to +180 deg.
      if (rotate_goal < -180) rotate_goal += 360;
      if (fabs(rotate_goal) < 0.01) rotate_goal = 0;                             //  = 0 within my precision
      zdialog_stuff(zd,"degrees",rotate_goal);

      rotate_func();                                                             //  E3 is rotated E1
   }

   if (strmatch(event,"auto"))                                                   //  auto trim margins
   {
      if (CEF->Fmods) {                                                          //  save pending edit
         dialog_event(zd,"max");
         trim_final();
         edit_done(0);
      }
      else edit_cancel(0);
      autotrim();                                                                //  do auto-trim
      m_trim_rotate(0,"keep");                                                   //  restart edit
      return 1;
   }

   return 1;
}


//  trim/rotate mouse function

void trimrotate::mousefunc()
{
   using namespace trimrotate;

   int         mpx, mpy, xdrag, ydrag;
   int         moveall = 0;
   int         dx, dy, dd, dc, ds;
   int         d1, d2, d3, d4;
   zdialog     *zd = EFtrimrotate.zd;

   if (! LMclick && ! RMclick && ! Mxdrag && ! Mydrag) {                         //  no click or drag
      Fcorner = Fside = Frotate = 0;
      return;
   }
   
   mpx = mpy = xdrag = ydrag = 0;                                                //  avoid gcc warnings                 19.0

   if (LMclick)                                                                  //  add vertical and horizontal
   {                                                                             //    lines at click position
      LMclick = 0;
      Fcorner = Fside = KBcorner = KBside = Frotate = 0;
      Fguidelines = 1;
      guidelineX = Mxclick;
      guidelineY = Myclick;
      trim_darkmargins();
      Fpaint2();
      return;
   }

   if (RMclick)                                                                  //  remove the lines
   {
      RMclick = 0;
      Fcorner = Fside = KBcorner = KBside = Frotate = 0;
      Fguidelines = 0;
      trim_darkmargins();
      Fpaint2();
      return;
   }

   if (Mxdrag || Mydrag)                                                         //  drag
   {
      mpx = Mxdrag;
      mpy = Mydrag;
      xdrag = Mxdrag - Mxdown;
      ydrag = Mydrag - Mydown;
      Mxdown = Mxdrag;                                                           //  reset drag origin
      Mydown = Mydrag;
      Mxdrag = Mydrag = 0;
   }

   if (Frotate) goto mouse_rotate;                                               //  continue rotate in progress

   else if (Fcorner)
   {
      if (Fcorner == 1) { trimx1 = mpx; trimy1 = mpy; }                          //  continue dragging corner with mouse
      if (Fcorner == 2) { trimx2 = mpx; trimy1 = mpy; }
      if (Fcorner == 3) { trimx2 = mpx; trimy2 = mpy; }
      if (Fcorner == 4) { trimx1 = mpx; trimy2 = mpy; }
   }
   
   else if (Fside)
   {
      if (Fside == 1) trimx1 = mpx;                                              //  continue dragging side with mouse
      if (Fside == 2) trimy1 = mpy;
      if (Fside == 3) trimx2 = mpx;
      if (Fside == 4) trimy2 = mpy;
   }

   else
   {
      moveall = 1;
      dd = 0.2 * (trimx2 - trimx1);                                              //  test if mouse is in the broad
      if (mpx < trimx1 + dd) moveall = 0;                                        //    middle of the rectangle
      if (mpx > trimx2 - dd) moveall = 0;
      dd = 0.2 * (trimy2 - trimy1);
      if (mpy < trimy1 + dd) moveall = 0;
      if (mpy > trimy2 - dd) moveall = 0;
   }

   if (moveall) {                                                                //  yes, move the whole rectangle
      trimx1 += xdrag;
      trimx2 += xdrag;
      trimy1 += ydrag;
      trimy2 += ydrag;
      Fcorner = Fside = KBcorner = KBside = Frotate = 0;
   }

   else if (! Fcorner && ! Fside)                                                //  no, find closest corner/side to mouse
   {
      dx = mpx - trimx1;
      dy = mpy - trimy1;
      d1 = sqrt(dx*dx + dy*dy);                                                  //  distance from NW corner

      dx = mpx - trimx2;
      dy = mpy - trimy1;
      d2 = sqrt(dx*dx + dy*dy);                                                  //  NE

      dx = mpx - trimx2;
      dy = mpy - trimy2;
      d3 = sqrt(dx*dx + dy*dy);                                                  //  SE

      dx = mpx - trimx1;
      dy = mpy - trimy2;
      d4 = sqrt(dx*dx + dy*dy);                                                  //  SW

      Fcorner = 1;                                                               //  find closest corner
      dc = d1;                                                                   //  NW
      if (d2 < dc) { Fcorner = 2; dc = d2; }                                     //  NE
      if (d3 < dc) { Fcorner = 3; dc = d3; }                                     //  SE
      if (d4 < dc) { Fcorner = 4; dc = d4; }                                     //  SW

      dx = mpx - trimx1;
      dy = mpy - (trimy1 + trimy2) / 2;
      d1 = sqrt(dx*dx + dy*dy);                                                  //  distance from left side middle

      dx = mpx - (trimx1 + trimx2) / 2;
      dy = mpy - trimy1;
      d2 = sqrt(dx*dx + dy*dy);                                                  //  top middle

      dx = mpx - trimx2;
      dy = mpy - (trimy1 + trimy2) / 2;
      d3 = sqrt(dx*dx + dy*dy);                                                  //  right side middle

      dx = mpx - (trimx1 + trimx2) / 2;
      dy = mpy - trimy2;
      d4 = sqrt(dx*dx + dy*dy);                                                  //  bottom middle

      Fside = 1;                                                                 //  find closest side
      ds = d1;                                                                   //  left
      if (d2 < ds) { Fside = 2; ds = d2; }                                       //  top
      if (d3 < ds) { Fside = 3; ds = d3; }                                       //  right
      if (d4 < ds) { Fside = 4; ds = d4; }                                       //  bottom

      if (dc < ds) Fside = 0;                                                    //  closer to corner
      else Fcorner = 0;                                                          //  closer to side
      
      if (Fside == 3 && E3ww - mpx < ds)                                         //  closest to right side,
         goto mouse_rotate;                                                      //    and even closer to right edge

      if (Fcorner == 1) { trimx1 = mpx; trimy1 = mpy; }                          //  move this corner to mouse
      if (Fcorner == 2) { trimx2 = mpx; trimy1 = mpy; }
      if (Fcorner == 3) { trimx2 = mpx; trimy2 = mpy; }
      if (Fcorner == 4) { trimx1 = mpx; trimy2 = mpy; }
      
      if (Fside == 1) trimx1 = mpx;                                              //  move this side to mouse
      if (Fside == 2) trimy1 = mpy;
      if (Fside == 3) trimx2 = mpx;
      if (Fside == 4) trimy2 = mpy;
      
      KBcorner = Fcorner;                                                        //  save last corner/side moved
      KBside = Fside;
   }

   trim_limits();                                                                //  check margin limits and adjust if req.
   trim_darkmargins();                                                           //  show trim area in image

   return;

// ------------------------------------------------------------------------

   mouse_rotate:

   Frotate = 1;
   Fcorner = Fside = KBcorner = KBside = 0;

   rotate_goal += 40.0 * ydrag / E3ww;                                           //  convert radians to degrees (reduced)
   if (rotate_goal > 180) rotate_goal -= 360;                                    //  keep within -180 to +180 deg.
   if (rotate_goal < -180) rotate_goal += 360;
   if (fabs(rotate_goal) < 0.01) rotate_goal = 0;                                //  = 0 within my precision
   zdialog_stuff(zd,"degrees",rotate_goal);                                      //  update dialog

   rotate_func();                                                                //  E3 is rotated E1
   return;
}


//  Keyboard function
//  KB arrow keys tweak the last selected corner or side

void trimrotate::KBfunc(int key)
{
   using namespace trimrotate;

   int      xstep, ystep;

   xstep = ystep = 0;
   if (key == GDK_KEY_Left) xstep = -1;
   if (key == GDK_KEY_Right) xstep = +1;
   if (key == GDK_KEY_Up) ystep = -1;
   if (key == GDK_KEY_Down) ystep = +1;

   if (KBcorner == 1) {                                                          //  NW
      trimx1 += xstep;
      trimy1 += ystep;
   }

   if (KBcorner == 2) {                                                          //  NE
      trimx2 += xstep;
      trimy1 += ystep;
   }

   if (KBcorner == 3) {                                                          //  SE
      trimx2 += xstep;
      trimy2 += ystep;
   }

   if (KBcorner == 4) {                                                          //  SW
      trimx1 += xstep;
      trimy2 += ystep;
   }
   
   if (KBside == 1) trimx1 += xstep;                                             //  left
   if (KBside == 2) trimy1 += ystep;                                             //  top
   if (KBside == 3) trimx2 += xstep;                                             //  right
   if (KBside == 4) trimy2 += ystep;                                             //  bottom
   
   trim_limits();                                                                //  check margin limits and adjust if req.
   trim_darkmargins();                                                           //  show trim area in image

   return;
}


//  check new margins for sanity and enforce locked aspect ratio

void trimrotate::trim_limits()
{
   using namespace trimrotate;

   int         rlock, chop;
   int         ww, hh, ww2, hh2;
   float       drr;
   char        text[20];
   zdialog     *zd = EFtrimrotate.zd;

   if (trimx1 > trimx2-10) trimx1 = trimx2-10;                                   //  sanity limits
   if (trimy1 > trimy2-10) trimy1 = trimy2-10;

   zdialog_fetch(zd,"lock",rlock);                                               //  w/h ratio locked
   if (rlock && Fcorner) {
      if (Fcorner < 3)
         trimy1 = trimy2 - 1.0 * (trimx2 - trimx1) / trimR + 0.5;                //  round                              19.0
      else
         trimy2 = trimy1 + 1.0 * (trimx2 - trimx1) / trimR + 0.5;
   }
   
   if (rlock && Fside) {
      ww = trimx2 - trimx1;
      hh = trimy2 - trimy1;
      ww2 = ww;
      hh2 = hh;
      if (Fside == 1 || Fside == 3) hh2 = ww / trimR + 0.5;
      else ww2 = hh * trimR + 0.5;
      ww2 = (ww2 - ww) / 2;
      hh2 = (hh2 - hh) / 2;
      trimx1 -= ww2;
      trimx2 += ww2;
      trimy1 -= hh2;
      trimy2 += hh2;
   }

   chop = 0;
   if (trimx1 < 0) {                                                             //  look for off the edge
      trimx1 = 0;                                                                //    after corner move
      chop = 1;
   }

   if (trimx2 > E3ww) {
      trimx2 = E3ww;
      chop = 2;
   }

   if (trimy1 < 0) {
      trimy1 = 0;
      chop = 3;
   }

   if (trimy2 > E3hh) {
      trimy2 = E3hh;
      chop = 4;
   }

   if (rlock && chop) {                                                          //  keep ratio if off edge
      if (chop < 3)
         trimy2 = trimy1 + 1.0 * (trimx2 - trimx1) / trimR + 0.5;                //  round                              19.0
      else
         trimx2 = trimx1 + 1.0 * (trimy2 - trimy1) * trimR + 0.5;
   }

   if (trimx1 > trimx2-10) trimx1 = trimx2-10;                                   //  sanity limits
   if (trimy1 > trimy2-10) trimy1 = trimy2-10;

   if (trimx1 < 0) trimx1 = 0;                                                   //  keep within visible area
   if (trimx2 > E3ww) trimx2 = E3ww;
   if (trimy1 < 0) trimy1 = 0;
   if (trimy2 > E3hh) trimy2 = E3hh;

   trimww = trimx2 - trimx1;                                                     //  new rectangle dimensions
   trimhh = trimy2 - trimy1;

   drr = 1.0 * trimww / trimhh;                                                  //  new w/h ratio
   if (! rlock) trimR = drr;

   zdialog_stuff(zd,"width",trimww);                                             //  stuff width, height, ratio
   zdialog_stuff(zd,"height",trimhh);
   snprintf(text,20,"%.3f  ",trimR);
   zdialog_stuff(zd,"ratio",text);

   return;
}


//  Darken image pixels outside of current trim margins.
//  messy logic: update pixmaps only for changed pixels to increase speed

void trimrotate::trim_darkmargins()
{
   using namespace trimrotate;

   int      ox1, oy1, ox2, oy2;                                                  //  outer trim rectangle
   int      nx1, ny1, nx2, ny2;                                                  //  inner trim rectangle
   int      px, py;
   float    *pix1, *pix3;
   int      nc = E1pxm->nc, pcc = nc * sizeof(float);

   if (trimx1 < 0) trimx1 = 0;                                                   //  keep within visible area
   if (trimx2 > E3ww) trimx2 = E3ww;
   if (trimy1 < 0) trimy1 = 0;
   if (trimy2 > E3hh) trimy2 = E3hh;

   if (ptrimx1 < trimx1) ox1 = ptrimx1;                                          //  outer rectangle
   else ox1 = trimx1;
   if (ptrimx2 > trimx2) ox2 = ptrimx2;
   else ox2 = trimx2;
   if (ptrimy1 < trimy1) oy1 = ptrimy1;
   else oy1 = trimy1;
   if (ptrimy2 > trimy2) oy2 = ptrimy2;
   else oy2 = trimy2;

   if (ptrimx1 > trimx1) nx1 = ptrimx1;                                          //  inner rectangle
   else nx1 = trimx1;
   if (ptrimx2 < trimx2) nx2 = ptrimx2;
   else nx2 = trimx2;
   if (ptrimy1 > trimy1) ny1 = ptrimy1;
   else ny1 = trimy1;
   if (ptrimy2 < trimy2) ny2 = ptrimy2;
   else ny2 = trimy2;

   ox1 -= 2;                                                                     //  expand outer rectangle
   oy1 -= 2;
   ox2 += 2;
   oy2 += 2;
   nx1 += 2;                                                                     //  reduce inner rectangle
   ny1 += 2;
   nx2 -= 2;
   ny2 -= 2;

   if (ox1 < 0) ox1 = 0;
   if (oy1 < 0) oy1 = 0;
   if (ox2 > E3ww) ox2 = E3ww;
   if (oy2 > E3hh) oy2 = E3hh;

   if (nx1 < ox1) nx1 = ox1;
   if (ny1 < oy1) ny1 = oy1;
   if (nx2 > ox2) nx2 = ox2;
   if (ny2 > oy2) ny2 = oy2;

   if (nx2 < nx1) nx2 = nx1;
   if (ny2 < ny1) ny2 = ny1;

   if (! E9pxm) E9pxm = PXM_copy(E3pxm);

   for (py = oy1; py < ny1; py++)                                                //  top band of pixels
   for (px = ox1; px < ox2; px++)
   {
      pix1 = PXMpix(E9pxm,px,py);
      pix3 = PXMpix(E3pxm,px,py);

      if (px < trimx1 || px >= trimx2 ||
          py < trimy1 || py >= trimy2) {
         pix3[0] = pix1[0] / 2;                                                  //  outside trim margins
         pix3[1] = pix1[1] / 2;                                                  //  50% brightness
         pix3[2] = pix1[2] / 2;
      }
      else memcpy(pix3,pix1,pcc);                                                //  inside, full brightness
   }

   for (py = oy1; py < oy2; py++)                                                //  right band
   for (px = nx2; px < ox2; px++)
   {
      pix1 = PXMpix(E9pxm,px,py);
      pix3 = PXMpix(E3pxm,px,py);

      if (px < trimx1 || px >= trimx2 || 
          py < trimy1 || py >= trimy2) {
         pix3[0] = pix1[0] / 2;
         pix3[1] = pix1[1] / 2;
         pix3[2] = pix1[2] / 2;
      }
      else memcpy(pix3,pix1,pcc);
   }

   for (py = ny2; py < oy2; py++)                                                //  bottom band
   for (px = ox1; px < ox2; px++)
   {
      pix1 = PXMpix(E9pxm,px,py);
      pix3 = PXMpix(E3pxm,px,py);

      if (px < trimx1 || px >= trimx2 || 
          py < trimy1 || py >= trimy2) {
         pix3[0] = pix1[0] / 2;
         pix3[1] = pix1[1] / 2;
         pix3[2] = pix1[2] / 2;
      }
      else memcpy(pix3,pix1,pcc);
   }

   for (py = oy1; py < oy2; py++)                                                //  left band
   for (px = ox1; px < nx1; px++)
   {
      pix1 = PXMpix(E9pxm,px,py);
      pix3 = PXMpix(E3pxm,px,py);

      if (px < trimx1 || px >= trimx2 || 
          py < trimy1 || py >= trimy2) {
         pix3[0] = pix1[0] / 2;
         pix3[1] = pix1[1] / 2;
         pix3[2] = pix1[2] / 2;
      }
      else memcpy(pix3,pix1,pcc);
   }

   cairo_t *cr = draw_context_create(gdkwin,draw_context);
   
   Fpaint3(ox1,oy1,ox2-ox1,ny1-oy1,cr);                                          //  4 updated rectangles
   Fpaint3(nx2,oy1,ox2-nx2,oy2-oy1,cr);
   Fpaint3(ox1,ny2,ox2-ox1,oy2-ny2,cr);
   Fpaint3(ox1,oy1,nx1-ox1,oy2-oy1,cr);

   drawlines(cr);                                                                //  draw trim margin lines

   draw_context_destroy(draw_context); 
   
   ptrimx1 = trimx1;                                                             //  set prior trim rectangle
   ptrimx2 = trimx2;                                                             //    from current trim rectangle
   ptrimy1 = trimy1;
   ptrimy2 = trimy2;

   if (gridsettings[currgrid][GON]) Fpaint2();                                   //  refresh gridlines

   return;
}


//  final trim - cut margins off
//  E3 is the input image, rotated
//  E3 is the output image from user trim rectangle

void trimrotate::trim_final()
{
   using namespace trimrotate;

   int      px1, py1, px2, py2;
   float    *pix3, *pix9;
   int      nc = E3pxm->nc, pcc = nc * sizeof(float);
   
   if (rotate_angle != 0)                                                        //  apply fine sharpen if rotated      18.07
      sharp_GR_callable(50,10);                                                  //  E3 = sharpened E3
   
   draw_toplines(2,0);                                                           //  erase trim rectangle

   if (trimx2 > E3ww) trimx2 = E3ww;                                             //  final check
   if (trimy2 > E3hh) trimy2 = E3hh;

   trimww = trimx2 - trimx1;                                                     //  new rectangle dimensions
   trimhh = trimy2 - trimy1;

   if (E9pxm) PXM_free(E9pxm);
   E9pxm = PXM_make(trimww,trimhh,nc);                                           //  new pixmap with requested size

   for (py1 = trimy1; py1 < trimy2; py1++)                                       //  copy E3 (rotated) to new size
   for (px1 = trimx1; px1 < trimx2; px1++)
   {
      px2 = px1 - trimx1;
      py2 = py1 - trimy1;
      pix3 = PXMpix(E3pxm,px1,py1);
      pix9 = PXMpix(E9pxm,px2,py2);
      memcpy(pix9,pix3,pcc);
   }

   PXM_free(E3pxm);
   E3pxm = E9pxm;
   E9pxm = 0;

   E3ww = E3pxm->ww;                                                             //  update E3 dimensions
   E3hh = E3pxm->hh;

   CEF->Fmods++;
   CEF->Fsaved = 0;

   Fpaint2();
   return;
}


//  rotate function
//  E3 = E1 with rotation

void trimrotate::rotate_func()
{
   using namespace trimrotate;

   zdialog     *zd = EFtrimrotate.zd;
   
   if (rotate_goal == rotate_angle) return;
   
   if (fabs(rotate_goal) < 0.01) {                                               //  no rotation
      rotate_goal = rotate_angle = 0.0;
      zdialog_stuff(zd,"degrees",0.0);
      PXM_free(E3pxm);                                                           //  E1 >> E3
      E3pxm = PXM_copy(E1pxm);
   }

   PXM_free(E3pxm);
   E3pxm = PXM_rotate(E1pxm,rotate_goal,0);                                      //  use quality rotate         bugfix  18.01.3
   rotate_angle = rotate_goal;

   if (E9pxm) PXM_free(E9pxm);                                                   //  E9 invalid 
   E9pxm = 0;

   E3ww = E3pxm->ww;                                                             //  update E3 dimensions
   E3hh = E3pxm->hh;

   if (trimx2 > E3ww) trimx2 = E3ww;                                             //  contract trim rectangle if needed
   if (trimy2 > E3hh) trimy2 = E3hh;

   trimww = trimx2 - trimx1;                                                     //  new rectangle dimensions
   trimhh = trimy2 - trimy1;

   zdialog_stuff(zd,"width",trimww);                                             //  stuff width, height
   zdialog_stuff(zd,"height",trimhh);

   ptrimx1 = 0;                                                                  //  set prior trim rectangle
   ptrimy1 = 0;
   ptrimx2 = E3ww;                                                               //    = 100% of image
   ptrimy2 = E3hh;
   
   CEF->Fmods++;

   Fpaintnow();
   return;
}


//  draw lines on image: trim rectangle, guidelines, gridlines

void trimrotate::drawlines(cairo_t *cr)
{
   using namespace trimrotate;

   Ntoplines = 4;                                                                //  outline trim rectangle
   toplines[0].x1 = trimx1;
   toplines[0].y1 = trimy1;
   toplines[0].x2 = trimx2;
   toplines[0].y2 = trimy1;
   toplines[0].type = 1;
   toplines[1].x1 = trimx2;
   toplines[1].y1 = trimy1;
   toplines[1].x2 = trimx2;
   toplines[1].y2 = trimy2;
   toplines[1].type = 1;
   toplines[2].x1 = trimx2;
   toplines[2].y1 = trimy2;
   toplines[2].x2 = trimx1;
   toplines[2].y2 = trimy2;
   toplines[2].type = 1;
   toplines[3].x1 = trimx1;
   toplines[3].y1 = trimy2;
   toplines[3].x2 = trimx1;
   toplines[3].y2 = trimy1;
   toplines[3].type = 1;

   if (Fguidelines) {                                                            //  vert/horz rotate guidelines
      Ntoplines = 6;
      toplines[4].x1 = guidelineX;
      toplines[4].y1 = 0;
      toplines[4].x2 = guidelineX;
      toplines[4].y2 = E3hh-1;
      toplines[4].type = 2;
      toplines[5].x1 = 0;
      toplines[5].y1 = guidelineY;
      toplines[5].x2 = E3ww-1;
      toplines[5].y2 = guidelineY;
      toplines[5].type = 2;
   }

   draw_toplines(1,cr);                                                          //  draw trim rectangle and guidelines

   return;
}


//  auto-trim image - set trim rectangle to exclude transparent regions

void trimrotate::autotrim()
{
   using namespace trimrotate;

   PXM      *pxm = 0;
   int      px1, py1, px2, py2;
   int      qx1, qy1, qx2, qy2;
   int      qx, qy, step1, step2;
   int      area1, area2, Fgrow = 0;
   float    *ppix;

   if (! E0pxm) return;                                                          //  no edit image
   pxm = E0pxm;
   if (pxm->nc < 4) return;                                                      //  no alpha channel

   px1 = 0.4 * pxm->ww;                                                          //  select small rectangle in the middle
   py1 = 0.4 * pxm->hh;
   px2 = 0.6 * pxm->ww;
   py2 = 0.6 * pxm->hh;

   step1 = 0.02 * (pxm->ww + pxm->hh);                                           //  start with big search steps
   step2 = 0.2 * step1;
   if (step2 < 1) step2 = 1;
   
   while (true)
   {
      while (true)
      {
         Fgrow = 0;

         area1 = (px2 - px1) * (py2 - py1);                                      //  area of current selection rectangle
         area2 = area1;

         for (qx1 = px1-step1; qx1 <= px1+step1; qx1 += step2)                   //  loop, vary NW and SE corners +/-
         for (qy1 = py1-step1; qy1 <= py1+step1; qy1 += step2)
         for (qx2 = px2-step1; qx2 <= px2+step1; qx2 += step2)
         for (qy2 = py2-step1; qy2 <= py2+step1; qy2 += step2)
         {
            if (qx1 < 0) continue;                                               //  check image limits
            if (qy1 < 0) continue;
            if (qx1 > 0.5 * pxm->ww) continue;
            if (qy1 > 0.5 * pxm->hh) continue;
            if (qx2 > pxm->ww-1) continue;
            if (qy2 > pxm->hh-1) continue;
            if (qx2 < 0.5 * pxm->ww) continue;
            if (qy2 < 0.5 * pxm->hh) continue;

            ppix = PXMpix(pxm,qx1,qy1);                                          //  check 4 corners are not
            if (ppix[3] < 1) continue;                                           //    in transparent zones
            ppix = PXMpix(pxm,qx2,qy1);
            if (ppix[3] < 1) continue;
            ppix = PXMpix(pxm,qx2,qy2);
            if (ppix[3] < 1) continue;
            ppix = PXMpix(pxm,qx1,qy2);
            if (ppix[3] < 1) continue;

            area2 = (qx2 - qx1) * (qy2 - qy1);                                   //  look for larger enclosed area
            if (area2 <= area1) continue;

            for (qx = qx1; qx < qx2; qx++) {                                     //  check top/bottom sides not intersect
               ppix = PXMpix(pxm,qx,qy1);                                        //    transparent zones
               if (ppix[3] < 1) break;
               ppix = PXMpix(pxm,qx,qy2);
               if (ppix[3] < 1) break;
            }
            if (qx < qx2) continue;

            for (qy = qy1; qy < qy2; qy++) {                                     //  also left/right sides
               ppix = PXMpix(pxm,qx1,qy);
               if (ppix[3] < 1) break;
               ppix = PXMpix(pxm,qx2,qy);
               if (ppix[3] < 1) break;
            }
            if (qy < qy2) continue;

            Fgrow = 1;                                                           //  successfully grew the rectangle
            px1 = qx1;                                                           //  new bigger rectangle coordinates
            py1 = qy1;                                                           //    for the next search iteration
            px2 = qx2;
            py2 = qy2;
            goto breakout;                                                       //  leave all 4 loops
         }

      breakout:
         if (! Fgrow) break;
      }

      if (step2 == 1) break;                                                     //  done

      step1 = 0.6 * step1;                                                       //  reduce search step size
      if (step1 < 2) step1 = 2;
      step2 = 0.2 * step1;
      if (step2 < 1) step2 = 1;
   }

   trimx1 = px1;                                                                 //  set parameters for trim function
   trimx2 = px2;
   trimy1 = py1;
   trimy2 = py2;

   return;
}


//  dialog to get custom trim button names and corresponding ratios

void trimrotate::trim_customize()
{
   using namespace trimrotate;
   
   char        text[20], blab[8], rlab[8];
   float       r1, r2, ratio;
   cchar       *pp;
   int         ii, zstat;

/***
       ___________________________________________________________
      |          Trim Buttons                                     |
      |                                                           |
      |  label  [ 5:4 ] [ 4:3 ] [ 8:5 ] [ 16:9 ] [ 2:1 ] [ gold ] |
      |  ratio  [ 5:4 ] [ 4:3 ] [ 8:5 ] [ 16:9 ] [ 2:1 ] [1.62:1] |
      |                                                           |
      |                                           [Done] [Cancel] |
      |___________________________________________________________|
      
***/

   zdialog *zd = zdialog_new(E2X("Trim Buttons"),Mwin,Bdone,Bcancel,null);
   zdialog_add_widget(zd,"hbox","hbb","dialog",0,"homog|space=3");
   zdialog_add_widget(zd,"hbox","hbr","dialog",0,"homog|space=3");
   zdialog_add_widget(zd,"label","label","hbb",E2X("label"),"space=3");
   zdialog_add_widget(zd,"label","ratio","hbr",E2X("ratio"),"space=3");

   strcpy(blab,"butt-0");
   strcpy(rlab,"ratio-0");

   for (ii = 0; ii < 5; ii++)                                                    //  add current trimbuttons to dialog
   {
      blab[5] = '0' + ii;
      rlab[6] = '0' + ii;
      zdialog_add_widget(zd,"zentry",blab,"hbb",trimbuttons[ii],"size=4");
      zdialog_add_widget(zd,"zentry",rlab,"hbr",trimratios[ii],"size=4");
   }

   zdialog_set_modal(zd);
   zdialog_run(zd,0,"mouse");                                                    //  run dialog
   zstat = zdialog_wait(zd);                                                     //  wait for complete

   if (zstat == 1)                                                               //  done
   {
      for (ii = 0; ii < 5; ii++)                                                 //  get new button names
      {
         blab[5] = '0' + ii;
         zdialog_fetch(zd,blab,text,12);
         strTrim2(text);
         if (! *text) continue;
         zfree(trimbuttons[ii]);
         trimbuttons[ii] = zstrdup(text);
      }

      for (ii = 0; ii < 5; ii++)                                                 //  get new ratios
      {
         rlab[6] = '0' + ii;
         zdialog_fetch(zd,rlab,text,12);
         strTrim2(text);
         r1 = r2 = ratio = 0;
         pp = strField(text,':',1);
         if (! pp) continue;
         r1 = atof(pp);
         pp = strField(text,':',2);
         if (! pp) continue;
         r2 = atof(pp);
         if (r1 > 0 && r2 > 0) ratio = r1/r2;
         if (ratio < 0.1 || ratio > 10) continue;
         zfree(trimratios[ii]);
         trimratios[ii] = zstrdup(text);
      }
   }

   zdialog_free(zd);                                                             //  kill dialog
   save_params();                                                                //  save parameters

   return;
}


/********************************************************************************/

//  Resize (rescale) image

namespace resize_names
{
   editfunc  EFresize;
   int       width, height;                                                      //  new image size
   int       orgwidth, orgheight;                                                //  original image size
   int       pctwidth, pctheight;                                                //  percent of original size
   int       Fprevsize;                                                          //  flag, use previous W/H sizes
   int       Fnewwidth, Fnewheight;                                              //  flags, width or height was changed
   int       Flockratio;                                                         //  flag, W/H ratio is locked
   float     WHratio;
}


void m_resize(GtkWidget *, cchar *menu)
{
   using namespace resize_names;

   int    resize_dialog_event(zdialog *zd, cchar *event);
   void * resize_thread(void *);

   F1_help_topic = "resize";
   
   char     rtext[12];

   EFresize.menuname = menu;
   EFresize.menufunc = m_resize;
   EFresize.funcname = "resize";
   EFresize.Frestart = 1;                                                        //  allow restart
   EFresize.Fscript = 1;                                                         //  scripting supported
   EFresize.threadfunc = resize_thread;                                          //  thread function
   if (! edit_setup(EFresize)) return;                                           //  setup edit

/****
       ________________________________________________________
      |                                                        |
      |             Pixels    Percent                          |
      |  Width     [_____]    [_____]      [_] Previous        |
      |  Height    [_____]    [_____]                          |
      |                                                        |
      |  [ ] 1/1  [_] 3/4  [_] 2/3  [_] 1/2  [_] 1/3  [_] 1/4  |
      |                                                        |
      |  W/H Ratio: 1.333    Lock [_]                          |
      |                                       [done] [cancel]  |
      |________________________________________________________|
    
****/

   zdialog *zd = zdialog_new(E2X("Resize Image"),Mwin,Bdone,Bcancel,null);
   EFresize.zd = zd;
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"vbox","vb11","hb1",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vb12","hb1",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vb13","hb1",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vb14","hb1",0,"homog|space=10");
   zdialog_add_widget(zd,"label","placeholder","vb11",0);
   zdialog_add_widget(zd,"label","labw","vb11",Bwidth);
   zdialog_add_widget(zd,"label","labh","vb11",Bheight);
   zdialog_add_widget(zd,"label","labpix","vb12","Pixels");
   zdialog_add_widget(zd,"zspin","width","vb12","20|30000|1|20");                //  fotoxx.h limits
   zdialog_add_widget(zd,"zspin","height","vb12","20|30000|1|20");
   zdialog_add_widget(zd,"label","labpct","vb13",Bpercent);
   zdialog_add_widget(zd,"zspin","pctwidth","vb13","1|400|1|100");
   zdialog_add_widget(zd,"zspin","pctheight","vb13","1|400|1|100");
   zdialog_add_widget(zd,"check","Fprevsize","vb14",E2X("Previous"));
   zdialog_add_widget(zd,"hbox","hb2","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","space","hb2",0);
   zdialog_add_widget(zd,"check","1/1","hb2","1/1");
   zdialog_add_widget(zd,"check","3/4","hb2","3/4");
   zdialog_add_widget(zd,"check","2/3","hb2","2/3");
   zdialog_add_widget(zd,"check","1/2","hb2","1/2");
   zdialog_add_widget(zd,"check","1/3","hb2","1/3");
   zdialog_add_widget(zd,"check","1/4","hb2","1/4");
   zdialog_add_widget(zd,"hbox","hb3","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labratio","hb3",E2X("W/H Ratio:"),"space=5");
   zdialog_add_widget(zd,"label","WHratio","hb3","1.6667");
   zdialog_add_widget(zd,"check","Flockratio","hb3",E2X("Lock"),"space=10");
   
   zdialog_add_ttip(zd,"Fprevsize",E2X("use previous settings"));

   orgwidth = E1pxm->ww;                                                         //  original width, height
   orgheight = E1pxm->hh;
   width = orgwidth;                                                             //  = initial width, height
   height = orgheight;
   WHratio = 1.0 * width / height;                                               //  initial W/H ratio
   pctwidth = pctheight = 100;
   Fnewwidth = Fnewheight = 0;
   Flockratio = 1;                                                               //  W/H ratio initially locked
   Fprevsize = 0;                                                                //  previous size not selected

   zdialog_stuff(zd,"width",width);
   zdialog_stuff(zd,"height",height);
   zdialog_stuff(zd,"pctwidth",pctwidth);
   zdialog_stuff(zd,"pctheight",pctheight);
   snprintf(rtext,12,"%.3f",WHratio);
   zdialog_stuff(zd,"WHratio",rtext);
   zdialog_stuff(zd,"Flockratio",1);
   
   zdialog_stuff(zd,"1/1",1);                                                    //  begin with full size
   zdialog_stuff(zd,"3/4",0);
   zdialog_stuff(zd,"2/3",0);
   zdialog_stuff(zd,"1/2",0);
   zdialog_stuff(zd,"1/3",0);
   zdialog_stuff(zd,"1/4",0);
   zdialog_stuff(zd,"Fprevsize",0);

   zdialog_run(zd,resize_dialog_event,"save");                                   //  run dialog
   return;
}


//  dialog event and completion callback function

int resize_dialog_event(zdialog *zd, cchar *event)                               //  logic revised                      18.07
{
   using namespace resize_names;

   int         nn;
   double      wwxhh, memreq, memavail;
   char        rtext[12];
   int         pixcc = 4 * sizeof(float);                                        //  pixel size, RGBA
   
   if (strmatch(event,"done")) zd->zstat = 1;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  cancel

   if (zd->zstat)                                                                //  dialog complete
   {
      if (zd->zstat == 1) {                                                      //  done
         editresize[0] = width;                                                  //  remember size used
         editresize[1] = height;
         edit_done(0);
      }
      else edit_cancel(0);                                                       //  cancel or kill
      Fzoom = 0;
      return 1;
   }
   
   if (strmatch(event,"focus")) return 1;                                        //  ignore focus

   Fnewwidth = Fnewheight = 0;                                                   //  reset flags

   if (strmatch(event,"pctwidth")) {                                             //  % width input
      zdialog_fetch(zd,"pctwidth",nn);
      if (nn != pctwidth) {
         width = nn / 100.0 * orgwidth + 0.5;
         Fnewwidth = 1;                                                          //  note changed
      }
   }
         
   if (strmatch(event,"pctheight")) {                                            //  same for height
      zdialog_fetch(zd,"pctheight",nn);
      if (nn != pctheight) {
         height = nn / 100.0 * orgheight + 0.5;
         Fnewheight = 1;
      }
   }

   if (strmatch(event,"width")) {                                                //  width input
      zdialog_fetch(zd,"width",nn);
      if (nn != width) {
         width = nn;                                                             //  set new width
         Fnewwidth = 1;                                                          //  note change
      }
   }
   
   if (strmatch(event,"height")) {                                               //  same for height
      zdialog_fetch(zd,"height",nn);
      if (nn != height) {
         height = nn;
         Fnewheight = 1;
      }
   }
   
   if (Fnewwidth || Fnewheight) {                                                //  width or height was set directly
      zdialog_stuff(zd,"1/1",0);                                                 //  uncheck all preset sizes
      zdialog_stuff(zd,"3/4",0);
      zdialog_stuff(zd,"2/3",0);
      zdialog_stuff(zd,"1/2",0);
      zdialog_stuff(zd,"1/3",0);
      zdialog_stuff(zd,"1/4",0);

      Fprevsize = 0;                                                             //  uncheck previous size
   }

   if (strstr("1/1 3/4 2/3 1/2 1/3 1/4",event)) {                                //  a ratio button was selected
      zdialog_stuff(zd,"1/1",0);                                                 //  uncheck all preset sizes
      zdialog_stuff(zd,"3/4",0);
      zdialog_stuff(zd,"2/3",0);
      zdialog_stuff(zd,"1/2",0);
      zdialog_stuff(zd,"1/3",0);
      zdialog_stuff(zd,"1/4",0);
      zdialog_stuff(zd,event,1);                                                 //  set selected button on

      Fprevsize = 0;                                                             //  uncheck previous size

      if (strmatch(event,"1/1")) {
         width = orgwidth;
         height = orgheight;
      }
         
      if (strmatch(event,"3/4")) {
         width = 0.75 * orgwidth;
         height = 0.75 * orgheight;
      }
         
      if (strmatch(event,"2/3")) {
         width = 0.6667 * orgwidth;
         height = 0.6667 * orgheight;
      }
         
      if (strmatch(event,"1/2")) {
         width = 0.5 * orgwidth;
         height = 0.5 * orgheight;
      }
         
      if (strmatch(event,"1/3")) {
         width = 0.3333 * orgwidth;
         height = 0.3333 * orgheight;
      }
         
      if (strmatch(event,"1/4")) {
         width = 0.25 * orgwidth;
         height = 0.25 * orgheight;
      }
      
      WHratio = 1.0 * orgwidth / orgheight;                                      //  new W/H ratio
      Flockratio = 1;                                                            //  lock W/H ratio
   }

   if (strmatch("Fprevsize",event)) {                                            //  Previous size button checked
      zdialog_stuff(zd,"1/1",0);                                                 //  uncheck all preset sizes
      zdialog_stuff(zd,"3/4",0);
      zdialog_stuff(zd,"2/3",0);
      zdialog_stuff(zd,"1/2",0);
      zdialog_stuff(zd,"1/3",0);
      zdialog_stuff(zd,"1/4",0);

      Fprevsize = 1;                                                             //  check previous size button
      
      width = editresize[0];                                                     //  set previous size
      height = editresize[1];

      WHratio = 1.0 * width / height;                                            //  new W/H ratio
      Flockratio = 1;                                                            //  lock W/H ratio
   }
   
   if (strmatch(event,"Flockratio"))                                             //  toggle W/H ratio lock 
      zdialog_fetch(zd,"Flockratio",Flockratio);
   
   if (Flockratio) {                                                             //  if W/H ratio locked,
      if (Fnewwidth) height = width / WHratio + 0.5;                             //    force same W/H ratio
      if (Fnewheight) width = height * WHratio + 0.5;                            //  round                              19.0
   }
   
   if (width < 20) {                                                             //  enforce lower limits
      width = 20;
      height = width / WHratio + 0.5;
   }

   if (height < 20) {
      height = 20;
      width = height * WHratio + 0.5;
   }
   
   if (width > wwhh_limit1) {                                                    //  enforce upper limits
      width = wwhh_limit1;
      height = width / WHratio + 0.5;
   }

   if (height > wwhh_limit1) {
      height = wwhh_limit1;
      width = height * WHratio + 0.5;
   }
      
   wwxhh = 1.0 * width * height;                                                 //  enforce image size limit
   if (wwxhh > wwhh_limit2) {
      width *= wwhh_limit2 / wwxhh;
      height *= wwhh_limit2 / wwxhh;
   }
   
   if (! Flockratio) WHratio = 1.0 * width / height;                             //  track actual W/H ratio

   pctwidth = 100.0 * width / orgwidth + 0.5;                                    //  final % size
   pctheight = 100.0 * height / orgheight + 0.5;
   
   zdialog_stuff(zd,"width",width);                                              //  update all zdialog data
   zdialog_stuff(zd,"height",height);
   zdialog_stuff(zd,"pctwidth",pctwidth);
   zdialog_stuff(zd,"pctheight",pctheight);
   zdialog_stuff(zd,"Fprevsize",Fprevsize);
   zdialog_stuff(zd,"Flockratio",Flockratio);
   snprintf(rtext,12,"%.3f",WHratio);
   zdialog_stuff(zd,"WHratio",rtext);

   memreq = 1.0 * width * height * pixcc;                                        //  memory for rescaled image          19.0
   memreq = memreq / 1024;                                                       //  peak required memory, KB
   parseprocfile("/proc/meminfo","MemAvailable:",&memavail,0);                   //  est. free memory (+ cache), KB
   memavail -= 200000;                                                           //  200 MB margin
   if (memreq > memavail) {
      zmessageACK(Mwin,E2X("insufficient memory, cannot proceed"));
      edit_cancel(0);
      return 1;
   }

   signal_thread();                                                              //  update image, no wait for idle
   return 1;
}


//  do the resize job

void * resize_thread(void *)
{
   using namespace resize_names;

   PXM   *pxmtemp;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for signal

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      pxmtemp = PXM_rescale(E1pxm,width,height);                                 //  rescale the edit image
      PXM_free(E3pxm);
      E3pxm = pxmtemp;

      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();
   }

   return 0;
}


/********************************************************************************/

//  Retouch
//  Adjust all aspects of brightness, contrast, color.
//  Brightness curves are used, overall and per RGB color.

namespace retouch_names
{
   editfunc    EFcombo;
   float       brightness;                                                       //  brightness input, -1 ... +1
   float       contrast;                                                         //  contrast input, -1 ... +1
   float       satlevel;                                                         //  saturation input, -1 ... +1 = saturated
   float       areaemph;                                                         //  emphasis input, -1 ... +1 = bright areas
   float       emphcurve[256];                                                   //  emphasis per brightness level, 0 ... 1
   int         Fapply = 0;                                                       //  flag, apply dialog controls to image
   int         Fdist = 0;                                                        //  flag, show brightness distribution
   float       amplify = 1.0;                                                    //  curve amplifier, 0 ... 2
   GtkWidget   *drawwin_dist, *drawwin_scale;                                    //  brightness distribution graph widgets
   int         RGBW[4] = { 0, 0, 0, 0 };                                         //     "  colors: red/green/blue/white (all)
}


//  menu function

void m_retouch(GtkWidget *, cchar *menu)
{
   using namespace retouch_names;

   int    retouch_dialog_event(zdialog* zd, cchar *event);
   void   retouch_curvedit(int spc);
   void * retouch_thread(void *);

   F1_help_topic = "retouch";

   EFcombo.menuname = menu;
   EFcombo.menufunc = m_retouch;
   EFcombo.funcname = "retouch";
   EFcombo.FprevReq = 1;                                                         //  use preview
   EFcombo.Farea = 2;                                                            //  select area usable
   EFcombo.Frestart = 1;                                                         //  restart allowed
   EFcombo.FusePL = 1;                                                           //  use with paint/lever edits OK
   EFcombo.Fscript = 1;                                                          //  scripting supported
   EFcombo.threadfunc = retouch_thread;
   if (! edit_setup(EFcombo)) return;                                            //  setup edit

/***
       _____________________________________________________
      |                    Retouch                          |
      |  _________________________________________________  |
      | |                                                 | |                    //  5 curves are maintained:
      | |                                                 | |                    //  curve 0: current display curve
      | |                                                 | |                    //        1: curve for all colors
      | |                                                 | |                    //  curve 0: current display curve
      | |                                                 | |                    //        1: curve for all colors
      | |               curve edit area                   | |                    //        2,3,4: red, green, blue
      | |                                                 | |
      | |                                                 | |
      | |                                                 | |                    //  curve 0: current display curve
      | |                                                 | |                    //        1: curve for all colors
      | |_________________________________________________| |
      | |_________________________________________________| |                    //  brightness scale: black to white stripe
      |   (o) all  (o) red  (o) green  (o) blue             |                    //  select curve to display
      |                                                     |
      |  Amplifier  ================[]=============  Max.   |                    //  curve amplifier
      |  Brightness ================[]=============  High   |                    //  brightness
      |   Contrast  ================[]=============  High   |                    //  contrast
      |   Low Color ====================[]=========  High   |                    //  color saturation
      |  Dark Areas ==========[]=================== Bright  |                    //  color emphasis
      |                                                     |
      |  [x] Show Brightness Distribution                   |
      |                                                     |
      |  Settings File [Load] [Save]                        |
      |                                                     |
      |                      [Reset] [Prev] [Done] [Cancel] |
      |_____________________________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Retouch"),Mwin,Breset,Bprev,Bdone,Bcancel,null);
   EFcombo.zd = zd;

   zdialog_add_widget(zd,"frame","frameH","dialog",0,"expand");                  //  edit-curve and distribution graph
   zdialog_add_widget(zd,"frame","frameB","dialog");                             //  black to white brightness scale

   zdialog_add_widget(zd,"hbox","hbrgb","dialog");
   zdialog_add_widget(zd,"radio","all","hbrgb",Ball,"space=5");
   zdialog_add_widget(zd,"radio","red","hbrgb",Bred,"space=3");
   zdialog_add_widget(zd,"radio","green","hbrgb",Bgreen,"space=3");
   zdialog_add_widget(zd,"radio","blue","hbrgb",Bblue,"space=3");

   zdialog_add_widget(zd,"hbox","hbcolor","dialog");
   zdialog_add_widget(zd,"label","space","hbcolor",0,"space=1");
   zdialog_add_widget(zd,"vbox","vbcolor1","hbcolor",0,"homog");
   zdialog_add_widget(zd,"label","space","hbcolor",0,"space=1");
   zdialog_add_widget(zd,"vbox","vbcolor2","hbcolor",0,"homog|expand");
   zdialog_add_widget(zd,"label","space","hbcolor",0,"space=1");
   zdialog_add_widget(zd,"vbox","vbcolor3","hbcolor",0,"homog");
   zdialog_add_widget(zd,"label","space","hbcolor",0,"space=1");

   zdialog_add_widget(zd,"label","labamp","vbcolor1",E2X("Amplifier"));
   zdialog_add_widget(zd,"label","labrite","vbcolor1",E2X("Brightness"));
   zdialog_add_widget(zd,"label","labcont","vbcolor1",E2X("Contrast"));
   zdialog_add_widget(zd,"label","labsat1","vbcolor1",E2X("Low Color"));
   zdialog_add_widget(zd,"label","labarea1","vbcolor1",E2X("Dark Areas"));

   zdialog_add_widget(zd,"hscale","amplify","vbcolor2","0.0|2.0|0.01|1.0");
   zdialog_add_widget(zd,"hscale","brightness","vbcolor2","-1.0|1.0|0.01|0");
   zdialog_add_widget(zd,"hscale","contrast","vbcolor2","-1.0|1.0|0.01|0");
   zdialog_add_widget(zd,"hscale","satlevel","vbcolor2","-1.0|1.0|0.01|0");
   zdialog_add_widget(zd,"hscale","areaemph","vbcolor2","0|1.0|0.01|0.5");

   zdialog_add_widget(zd,"label","labrite2","vbcolor3",E2X("Max."));
   zdialog_add_widget(zd,"label","labrite2","vbcolor3",E2X("High"));
   zdialog_add_widget(zd,"label","labcont2","vbcolor3",E2X("High"));
   zdialog_add_widget(zd,"label","labsat2","vbcolor3",E2X("High"));
   zdialog_add_widget(zd,"label","labarea2","vbcolor3",E2X("Bright"));

   zdialog_add_widget(zd,"hbox","hbdist","dialog");
   zdialog_add_widget(zd,"check","dist","hbdist",E2X("Brightness Distribution"),"space=3");

   zdialog_add_widget(zd,"hbox","hbset","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labset","hbset",E2X("Settings File"),"space=5");
   zdialog_add_widget(zd,"button","load","hbset",Bload,"space=3");
   zdialog_add_widget(zd,"button","save","hbset",Bsave,"space=3");

   zdialog_add_ttip(zd,Bprev,E2X("recall previous settings used"));

   zdialog_rescale(zd,"brightness",-1,0,1);                                      //  expand scale around neutral values
   zdialog_rescale(zd,"contrast",-1,0,1);
   zdialog_rescale(zd,"satlevel",-1,0,1);

   GtkWidget *frameH = zdialog_widget(zd,"frameH");                              //  setup edit curves
   spldat *sd = splcurve_init(frameH,retouch_curvedit);
   EFcombo.curves = sd;

   sd->Nscale = 1;                                                               //  horizontal line, neutral value
   sd->xscale[0][0] = 0.00;
   sd->yscale[0][0] = 0.50;
   sd->xscale[1][0] = 1.00;
   sd->yscale[1][0] = 0.50;

   for (int ii = 0; ii < 4; ii++)                                                //  loop curves 0-3
   {
      sd->nap[ii] = 3;                                                           //  initial curves are neutral
      sd->vert[ii] = 0;
      sd->fact[ii] = 0;
      sd->apx[ii][0] = 0.01;                                                     //  horizontal lines
      sd->apy[ii][0] = 0.50;
      sd->apx[ii][1] = 0.50;
      sd->apy[ii][1] = 0.50;                                                     //  curve 0 = overall brightness
      sd->apx[ii][2] = 0.99;
      sd->apy[ii][2] = 0.50;                                                     //  curve 1/2/3 = R/G/B adjustment
      splcurve_generate(sd,ii);
   }

   sd->Nspc = 4;                                                                 //  4 curves
   sd->fact[0] = 1;                                                              //  curve 0 active 
   zdialog_stuff(zd,"all",1);                                                    //  stuff default selection, all
   
   drawwin_dist = sd->drawarea;                                                  //  setup brightness distr. drawing area
   gtk_widget_set_size_request(drawwin_dist,150,150);
   G_SIGNAL(drawwin_dist,"draw",brightgraph_graph,RGBW);

   GtkWidget *frameB = zdialog_widget(zd,"frameB");                              //  setup brightness scale drawing area
   drawwin_scale = gtk_drawing_area_new();
   gtk_container_add(GTK_CONTAINER(frameB),drawwin_scale);
   gtk_widget_set_size_request(drawwin_scale,100,12);
   G_SIGNAL(drawwin_scale,"draw",brightgraph_scale,0);

   brightness = 0;                                                               //  neutral brightness
   contrast = 0;                                                                 //  neutral contrast
   amplify = 1.0;                                                                //  neutral amplifier
   satlevel = 0;                                                                 //  neutral saturation
   Fapply = Fdist = 0;

   areaemph = 0.5;                                                               //  even dark/bright area emphasis
   for (int ii = 0; ii < 256; ii++)
      emphcurve[ii] = 1;

   zdialog_resize(zd,350,450);
   zdialog_run(zd,retouch_dialog_event,"save");                                  //  run dialog - parallel

   return;
}


//  dialog event and completion callback function

int retouch_dialog_event(zdialog *zd, cchar *event)
{
   using namespace retouch_names;

   void  retouch_mousefunc();

   spldat      *sd = EFcombo.curves;
   float       c1, c2, xval, yval;
   float       bright0, dbrite, cont0, dcont;
   float       dx, dy;
   int         ii;
   
   Fapply = 0;
   
   if (strmatch(event,"done")) zd->zstat = 3;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 4;
   if (strmatch(event,"apply")) Fapply = 1;                                      //  from script

   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();                                                           //  get full size image
      signal_thread();
      return 1;
   }

   if (zd->zstat == 1)                                                           //  [reset]
   {
      zd->zstat = 0;                                                             //  keep dialog active

      zdialog_stuff(zd,"amplify",1);                                             //  neutral amplifier
      amplify = 1.0;
      zdialog_stuff(zd,"brightness",0);                                          //  neutral brightness
      zdialog_stuff(zd,"contrast",0);                                            //  neutral contrast
      brightness = contrast = 0;
      zdialog_stuff(zd,"satlevel",0);
      satlevel = 0;                                                              //  neutral saturation
      zdialog_stuff(zd,"areaemph",0.5);
      areaemph = 0.5;                                                            //  even area emphasis
      for (int ii = 0; ii < 256; ii++)                                           //  flat curve
         emphcurve[ii] = 1;

      for (int ii = 0; ii < 4; ii++)                                             //  loop brightness curves 0-3
      {
         sd->nap[ii] = 3;                                                        //  all curves are neutral
         sd->vert[ii] = 0;
         sd->apx[ii][0] = 0.01;
         sd->apy[ii][0] = 0.50;
         sd->apx[ii][1] = 0.50;
         sd->apy[ii][1] = 0.50;
         sd->apx[ii][2] = 0.99;
         sd->apy[ii][2] = 0.50;
         splcurve_generate(sd,ii);
         sd->fact[ii] = 0;
      }

      sd->fact[0] = 1;
      zdialog_stuff(zd,"all",1);
      zdialog_stuff(zd,"red",0); 
      zdialog_stuff(zd,"green",0);
      zdialog_stuff(zd,"blue",0);
      edit_reset();                                                              //  restore initial image
      event = "do_histogram";                                                    //  trigger graph update
   }

   if (zd->zstat == 2)                                                           //  [prev] restore previous settings
   {
      zd->zstat = 0;                                                             //  keep dialog active
      edit_load_prev_widgets(&EFcombo);
      Fapply = 1;                                                                //  trigger apply event
   }

   if (zd->zstat)                                                                //  [done] or [cancel]
   {
      freeMouse();
      if (zd->zstat == 3 && CEF->Fmods) {                                        //  [done]
         edit_fullsize();                                                        //  get full size image
         signal_thread();
         edit_save_last_widgets(&EFcombo);
         edit_done(0);
      }
      else edit_cancel(0);                                                       //  [cancel] or [x]
      return 1;
   }

   if (strmatch(event,"load"))                                                   //  load all settings from a file
      edit_load_widgets(&EFcombo);

   if (strmatch(event,"save"))                                                   //  save all settings to a file
      edit_save_widgets(&EFcombo);
      
   if (strmatch(event,"dist")) {                                                 //  distribution checkbox
      zdialog_fetch(zd,"dist",Fdist);                                            //  distribution graph on/off
      event = "do_histogram";                                                    //  trigger graph update
   }

   if (strstr("all red green blue",event))                                       //  new choice of curve
   {
      zdialog_fetch(zd,event,ii);
      if (! ii) return 0;                                                        //  button OFF event, wait for ON event

      for (ii = 0; ii < 4; ii++) sd->fact[ii] = 0;
      ii = strmatchV(event,"all","red","green","blue",null);
      ii = ii-1;                                                                 //  new active curve: 0, 1, 2, 3
      sd->fact[ii] = 1;

      splcurve_generate(sd,ii);                                                  //  regenerate curve
      gtk_widget_queue_draw(sd->drawarea);                                       //  draw curve
   }

   if (strmatch(event,"brightness"))                                             //  brightness slider, 0 ... 1
   {
      bright0 = brightness;
      zdialog_fetch(zd,"brightness",brightness);
      dbrite = brightness - bright0;                                             //  change in brightness, + -

      zdialog_stuff(zd,"all",1);                                                 //  active curve is "all"
      sd->fact[0] = 1;
      for (ii = 1; ii < 4; ii++)
         sd->fact[ii] = 0;

      for (ii = 0; ii < sd->nap[0]; ii++)                                        //  update curve 0 nodes from slider
      {
         dy = sd->apy[0][ii] + 0.5 * dbrite;                                     //  increment brightness
         if (dy < 0) dy = 0;
         if (dy > 1) dy = 1;
         sd->apy[0][ii] = dy;
      }

      splcurve_generate(sd,0);                                                   //  regenerate curve 0
      gtk_widget_queue_draw(sd->drawarea);                                       //  draw curve 0
   }

   if (strmatch(event,"contrast"))                                               //  contrast slider, 0 ... 1
   {
      cont0 = contrast;
      zdialog_fetch(zd,"contrast",contrast);
      dcont = contrast - cont0;                                                  //  change in contrast, + -

      zdialog_stuff(zd,"all",1);                                                 //  active curve is "all"
      sd->fact[0] = 1;
      for (ii = 1; ii < 4; ii++)
         sd->fact[ii] = 0;
      
      for (ii = 0; ii < sd->nap[0]; ii++)                                        //  update curve 0 nodes from slider
      {
         dx = sd->apx[0][ii];                                                    //  0 ... 0.5 ... 1
         if (dx < 0.0 || dx >= 1.0) continue;
         dy = dcont * (dx - 0.5);                                                //  -0.5 ... 0 ... +0.5 * dcont
         dy += sd->apy[0][ii];
         if (dy < 0) dy = 0;
         if (dy > 1) dy = 1;
         sd->apy[0][ii] = dy;
      }

      splcurve_generate(sd,0);                                                   //  regenerate curve 0
      gtk_widget_queue_draw(sd->drawarea);                                       //  draw curve 0
   }

   if (Fdist) {                                                                  //  distribution enabled
      zdialog_fetch(zd,"red",RGBW[0]);                                           //  get graph color choice
      zdialog_fetch(zd,"green",RGBW[1]);
      zdialog_fetch(zd,"blue",RGBW[2]);
      zdialog_fetch(zd,"all",RGBW[3]);
      if (RGBW[3]) RGBW[0] = RGBW[1] = RGBW[2] = 1;
      RGBW[3] = 0;
   }
   else RGBW[0] = RGBW[1] = RGBW[2] = RGBW[3] = 0;

   if (strmatch(event,"do_histogram"))                                           //  thread done or new color choice
      gtk_widget_queue_draw(drawwin_dist);                                       //  update distribution graph

   if (strmatch(event,"blendwidth")) Fapply = 1;                                 //  trigger apply event
   if (strstr("amplify brightness contrast",event)) Fapply = 1;
   if (strstr("satlevel areaemph wbtemp",event)) Fapply = 1;
   if (strmatch(event,"load")) Fapply = 1;

   if (! Fapply) return 1;                                                       //  wait for change

   for (ii = 0; ii < 4; ii++) splcurve_generate(sd,ii);                          //  regenerate curves                  19.0
                                                                                 //     (bugfix for scripts)
   if (Fdist) gtk_widget_queue_draw(drawwin_dist);                               //  update distribution graph

   zdialog_fetch(zd,"amplify",amplify);                                          //  get curve amplifier setting
   zdialog_fetch(zd,"brightness",brightness);                                    //  get brightness setting
   zdialog_fetch(zd,"contrast",contrast);                                        //  get contrast setting
   zdialog_fetch(zd,"satlevel",satlevel);                                        //  get saturation setting
   zdialog_fetch(zd,"areaemph",areaemph);                                        //  get dark/bright area emphasis

   if (areaemph < 0.5) {                                                         //  areaemph = 0 ... 0.5
      c1 = 1.2;                                                                  //  c1 = 1.2
      c2 = 2 * areaemph;                                                         //  c2 = 0 ... 1
   }
   else {                                                                        //  areaemph = 0.5 ... 1
      c1 = 2 * (1 - areaemph);                                                   //  c1 = 1 ... 0
      c2 = 1.2;                                                                  //  c2 = 1.2
   }

   for (ii = 0; ii < 256; ii++) {
      xval = ii / 256.0;                                                         //  xval = 0 ... 1
      yval = c1 + xval * (c2 - c1);                                              //  yval = c1 ... c2
      if (yval > 1.0) yval = 1.0;                                                //  limit to 1.0
      emphcurve[ii] = yval;
   }

   signal_thread();                                                              //  update the image
   return 1;
}


//  this function is called when a curve is edited

void retouch_curvedit(int spc)
{
   using namespace retouch_names;
   signal_thread();
   return;
}


//  thread function

void * retouch_thread(void *arg)
{
   using namespace retouch_names;

   void * retouch_wthread(void *);

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      do_wthreads(retouch_wthread,NWT);

      CEF->Fmods++;                                                              //  image3 modified
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window

      if (CEF->zd) {
         zd_thread = CEF->zd;                                                    //  signal dialog to update
         zd_thread_event = "do_histogram";                                       //    distribution graph
      }
      else zd_thread = 0;
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


void * retouch_wthread(void *arg)                                                //  worker thread function
{
   using namespace retouch_names;

   int         index = *((int *) arg);
   int         ii, dist = 0, px, py;
   float       *pix1, *pix3;
   float       R1, G1, B1, maxRGB;
   float       R3, G3, B3;
   float       pixbrite, F1, F2, ff;
   float       coeff = 1000.0 / 256.0;
   spldat      *sd = EFcombo.curves;

   for (py = index; py < E3pxm->hh; py += NWT)                                   //  loop all pixels
   for (px = 0; px < E3pxm->ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3pxm->ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      R1 = R3 = pix1[0];                                                         //  input RGB values
      G1 = G3 = pix1[1];
      B1 = B3 = pix1[2];
      
      //  apply saturation color shift

      if (satlevel != 0) {
         pixbrite = 0.333 * (R3 + G3 + B3);                                      //  pixel brightness, 0 to 255.9
         R3 = R3 + satlevel * (R3 - pixbrite);                                   //  satlevel is -1 ... +1
         G3 = G3 + satlevel * (G3 - pixbrite);
         B3 = B3 + satlevel * (B3 - pixbrite);
      }

      if (R3 < 0) R3 = 0;                                                        //  stop underflow
      if (G3 < 0) G3 = 0;
      if (B3 < 0) B3 = 0;

      if (R3 > 255.9 || G3 > 255.9 || B3 > 255.9) {                              //  stop overflow
         maxRGB = R3;
         if (G3 > maxRGB) maxRGB = G3;
         if (B3 > maxRGB) maxRGB = B3;
         R3 = R3 * 255.9 / maxRGB;
         G3 = G3 * 255.9 / maxRGB;
         B3 = B3 * 255.9 / maxRGB;
      }

      //  apply dark/bright area emphasis curve for color changes

      if (areaemph != 0.5) {
         maxRGB = R1;                                                            //  use original colors
         if (G1 > maxRGB) maxRGB = G1;
         if (B1 > maxRGB) maxRGB = B1;
         F1 = emphcurve[int(maxRGB)];                                            //  0 ... 1   neutral is flat curve = 1
         F2 = 1.0 - F1;
         R3 = F1 * R3 + F2 * R1;
         G3 = F1 * G3 + F2 * G1;
         B3 = F1 * B3 + F2 * B1;
      }

      //  apply brightness/contrast curves

      pixbrite = R3;                                                             //  use max. RGB value
      if (G3 > pixbrite) pixbrite = G3;
      if (B3 > pixbrite) pixbrite = B3;
      
      ii = coeff * pixbrite;                                                     //  "all" curve index 0-999
      if (ii < 0) ii = 0;
      if (ii > 999) ii = 999;
      ff = 1.0 + amplify * (sd->yval[0][ii] - 0.5);
      R3 = ff * R3;
      G3 = ff * G3;
      B3 = ff * B3;

      ii = coeff * R3;                                                           //  additional RGB curve adjustments
      if (ii < 0) ii = 0;
      if (ii > 999) ii = 999;
      R3 = R3 * (1.0 + sd->yval[1][ii] - 0.5);

      ii = coeff * G3;
      if (ii < 0) ii = 0;
      if (ii > 999) ii = 999;
      G3 = G3 * (1.0 + sd->yval[2][ii] - 0.5); 

      ii = coeff * B3;
      if (ii < 0) ii = 0;
      if (ii > 999) ii = 999;
      B3 = B3 * (1.0 + sd->yval[3][ii] - 0.5); 

      if (R3 < 0) R3 = 0;                                                        //  stop underflow
      if (G3 < 0) G3 = 0;
      if (B3 < 0) B3 = 0;

      if (R3 > 255.9 || G3 > 255.9 || B3 > 255.9) {                              //  stop overflow
         maxRGB = R3;
         if (G3 > maxRGB) maxRGB = G3;
         if (B3 > maxRGB) maxRGB = B3;
         R3 = R3 * 255.9 / maxRGB;
         G3 = G3 * 255.9 / maxRGB;
         B3 = B3 * 255.9 / maxRGB;
      }

      //  select area edge blending

      if (sa_stat == 3 && dist < sa_blendwidth) {
         F2 = sa_blendfunc(dist);
         F1 = 1.0 - F2;
         R3 = F2 * R3 + F1 * R1;
         G3 = F2 * G3 + F1 * G1;
         B3 = F2 * B3 + F1 * B1;
      }

      pix3[0] = R3;
      pix3[1] = G3;
      pix3[2] = B3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  color balance function                                                       //  19.0
//  auto white balance and spot white balance
//  auto black level and spot black level
//  illumination temperature adjustment

namespace colorbal_names
{
   editfunc    EFcolorbal;
   float       wbalR, wbalG, wbalB;                                              //  white balance RGB factors
   float       blackR, blackG, blackB;                                           //  black level RGB values
   int         sampWB, sampBL;                                                   //  sample size, % pixels
   int         spotWB, spotBL;                                                   //  spot white balance or black level active
   int         Fblack;                                                           //  flag, black level was set
   int         lutemp;                                                           //  illumination temp. input, 1K ... 8K
   float       tempR, tempG, tempB;                                              //  illumination temperature RGB factors
   float       combR, combG, combB;                                              //  combined RGB factors
   int         Fapply = 0;                                                       //  flag, apply dialog controls to image
}

void  blackbodyRGB(int K, float &R, float &G, float &B);


//  menu function

void m_colorbal(GtkWidget *, cchar *menu)
{
   using namespace colorbal_names;

   int    colorbal_dialog_event(zdialog* zd, cchar *event);
   void   colorbal_mousefunc();
   void * colorbal_thread(void *);

   F1_help_topic = "color_balance";

   EFcolorbal.menuname = menu;
   EFcolorbal.menufunc = m_colorbal;
   EFcolorbal.funcname = "color_balance";
   EFcolorbal.FprevReq = 1;                                                      //  use preview
   EFcolorbal.Farea = 2;                                                         //  select area usable
   EFcolorbal.Frestart = 1;                                                      //  restart allowed
   EFcolorbal.FusePL = 1;                                                        //  use with paint/lever edits OK
   EFcolorbal.Fscript = 1;                                                       //  scripting supported
   EFcolorbal.threadfunc = colorbal_thread;
   EFcolorbal.mousefunc = colorbal_mousefunc;
   if (! edit_setup(EFcolorbal)) return;                                         //  setup edit

/***
       _________________________________________
      |            Color Balance                |
      |                                         |
      |  [_] Auto white balance   [__] sample % |
      |  [_] Auto black level     [__] sample % |
      |  [x] Click gray spot for white balance  |                                //  click gray spot or dark spot
      |  [x] Click dark spot for black level    |
      |                                         |
      |  Warmer =========[]============ Cooler  |                                //  color temperature
      |                                         |
      |          [Reset] [Prev] [Done] [Cancel] |
      |_________________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Color Balance"),Mwin,Breset,Bprev,Bdone,Bcancel,null);
   EFcolorbal.zd = zd;
   
   zdialog_add_widget(zd,"hbox","hbawb","dialog");
   zdialog_add_widget(zd,"zbutton","autoWB","hbawb",E2X("Auto white balance"),"space=3");
   zdialog_add_widget(zd,"label","space","hbawb",0,"space=5");
   zdialog_add_widget(zd,"zspin","sampWB","hbawb","1|50|1|1","space=3|size=3");
   zdialog_add_widget(zd,"label","labsamp","hbawb",E2X("sample %"));

   zdialog_add_widget(zd,"hbox","hbabl","dialog");
   zdialog_add_widget(zd,"zbutton","autoBL","hbabl",E2X("Auto black level"),"space=3");
   zdialog_add_widget(zd,"label","space","hbabl",0,"space=5");
   zdialog_add_widget(zd,"zspin","sampBL","hbabl","1|50|1|1","space=3|size=3");
   zdialog_add_widget(zd,"label","labsamp","hbabl",E2X("sample %"));

   zdialog_add_widget(zd,"hbox","hbwb","dialog");
   zdialog_add_widget(zd,"check","spotWB","hbwb",E2X("Click gray spot for white balance"),"space=3");
   zdialog_add_widget(zd,"hbox","hbbl","dialog");
   zdialog_add_widget(zd,"check","spotBL","hbbl",E2X("Click dark spot for black level"),"space=3");

   zdialog_add_widget(zd,"hbox","hblut","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labwarm","hblut",E2X("Warmer"),"space=3");
   zdialog_add_widget(zd,"hscale","lutemp","hblut","2000|8000|1|5000","expand");
   zdialog_add_widget(zd,"label","labcool","hblut",E2X("Cooler"),"space=3");

   zdialog_add_ttip(zd,Bprev,E2X("use previous white balance"));

   wbalR = wbalG = wbalB = 1.0;                                                  //  neutral white balance
   lutemp = 5000;                                                                //  neutral illumination temp.
   blackbodyRGB(lutemp,tempR,tempG,tempB);                                       //  neutral temp. RGB factors
   combR = combG = combB = 1.0;                                                  //  neutral combined RGB factors
   blackR = blackG = blackB = 0.0;                                               //  zero black point
   Fblack = 0;

   zdialog_stuff(zd,"spotWB",0);                                                 //  reset mouse click status
   zdialog_stuff(zd,"spotBL",0);
   spotWB = spotBL = 0;

   zdialog_run(zd,colorbal_dialog_event,"save");                                 //  run dialog - parallel
   return;
}


//  dialog event and completion callback function

int colorbal_dialog_event(zdialog *zd, cchar *event)
{
   using namespace colorbal_names;

   void  colorbal_autoWB();
   void  colorbal_autoBL();
   void  colorbal_mousefunc();

   Fapply = 0;
   
   if (strmatch(event,"done")) zd->zstat = 3;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 4;
   if (strmatch(event,"apply")) Fapply = 1;                                      //  from script

   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();                                                           //  get full size image
      signal_thread();
      return 1;
   }

   if (zd->zstat == 1)                                                           //  [reset]
   {
      zd->zstat = 0;                                                             //  keep dialog active
      zdialog_stuff(zd,"spotWB",0);
      zdialog_stuff(zd,"spotBL",0);
      wbalR = wbalG = wbalB = 1.0;                                               //  neutral white balance
      lutemp = 5000;                                                             //  neutral illumination temp.
      zdialog_stuff(zd,"lutemp",lutemp);
      blackbodyRGB(lutemp,tempR,tempG,tempB);                                    //  neutral temp. RGB factors
      combR = combG = combB = 1.0;                                               //  neutral combined RGB factors
      blackR = blackG = blackB = 0.0;                                            //  zero black point 
      Fblack = 0;
      edit_reset();
      return 1;                                                                  //  19.0
   }
   
   if (zd->zstat == 2)                                                           //  [prev]
   {
      zd->zstat = 0;                                                             //  keep dialog active
      wbalR = whitebalance[0];                                                   //  use previous white balance settings
      wbalG = whitebalance[1];
      wbalB = whitebalance[2];
      lutemp = whitebalance[3];
      zdialog_stuff(zd,"lutemp",lutemp);
      Fapply = 1;
   }

   if (zd->zstat)                                                                //  [done] or [cancel]
   {
      freeMouse();
      if (zd->zstat == 3 && CEF->Fmods) {                                        //  [done]
         edit_fullsize();                                                        //  get full size image
         signal_thread();
         edit_save_last_widgets(&EFcolorbal);
         edit_done(0);
         whitebalance[0] = wbalR;                                                //  save settings for next time
         whitebalance[1] = wbalG;
         whitebalance[2] = wbalB;
         whitebalance[3] = lutemp;
      }
      else edit_cancel(0);                                                       //  [cancel] or [x]
      return 1;
   }

   if (strmatch(event,"autoWB")) {                                               //  auto white balance
      zdialog_fetch(zd,"sampWB",sampWB);                                         //  % brightest pixels to sample
      zdialog_stuff(zd,"spotWB",0);
      zdialog_stuff(zd,"spotBL",0);
      spotWB = spotBL = 0;
      colorbal_autoWB();
      Fapply = 1;
   }

   if (strmatch(event,"autoBL")) {                                               //  auto black level
      zdialog_fetch(zd,"sampBL",sampBL);                                         //  % darkest pixels to sample
      zdialog_stuff(zd,"spotWB",0);
      zdialog_stuff(zd,"spotBL",0);
      spotWB = spotBL = 0;
      colorbal_autoBL();
      Fapply = 1;
   }

   if (strmatch(event,"spotWB")) {                                               //  spot white balance checkbox
      zdialog_fetch(zd,"spotWB",spotWB);
      if (spotWB) {
         zdialog_stuff(zd,"spotBL",0);
         spotBL = 0;
      }
   }

   if (strmatch(event,"spotBL")) {                                               //  spot black level checkbox
      zdialog_fetch(zd,"spotBL",spotBL);
      if (spotBL) {
         zdialog_stuff(zd,"spotWB",0);
         spotWB = 0;
      }
   }

   if (strstr("spotWB spotBL",event)) {                                          //  spot white balance or black level
      if (spotWB || spotBL)                                                      //  one is on:
         takeMouse(colorbal_mousefunc,dragcursor);                               //    connect mouse function
      else freeMouse();                                                          //  both off: free mouse
      return 1;
   }
   
   if (strmatch(event,"blendwidth")) Fapply = 1;                                 //  area blend with change
   if (strstr("lutemp",event)) Fapply = 1;                                       //  illumination temp. change

   if (! Fapply) return 1;                                                       //  wait for change

   zdialog_fetch(zd,"lutemp",lutemp);                                            //  get illumination temp. setting
   blackbodyRGB(lutemp,tempR,tempG,tempB);                                       //  generate temp. adjustments

   signal_thread();                                                              //  update the image
   return 1;
}


//  use brightest pixels to estimate RGB values of illumination
//  and use these to adjust image RGB values for neutral illumination

void colorbal_autoWB()
{
   using namespace colorbal_names;

   int         pixR[256], pixG[256], pixB[256];                                  //  pixel counts per RGB value 0-255
   int         samplesize;                                                       //  sample size, pixel count
   int         E1ww, E1hh;                                                       //  image dimensions
   int         px, py, ii, dist;
   int         sampR, sampG, sampB;
   double      sumR, sumG, sumB;
   float       meanR, meanG, meanB;
   float       *pix;
   
   for (ii = 0; ii < 256; ii++)
      pixR[ii] = pixG[ii] = pixB[ii] = 0;
   
   E1ww = E1pxm->ww;                                                             //  get image dimensions
   E1hh = E1pxm->hh;

   for (py = 0; py < E1hh; py++)                                                 //  scan image file, get pixel counts
   for (px = 0; px < E1ww; px++)                                                 //    per RGB value 0-255
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E1pxm->ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel outside area
      }

      pix = PXMpix(E1pxm,px,py);
      ii = pix[0];                                                               //  pixel R value 0-255
      pixR[ii]++;                                                                //  accumulate pixel count for R value
      ii = pix[1];
      pixG[ii]++;
      ii = pix[2];
      pixB[ii]++;
   }
   
   samplesize = E1ww * E1hh;                                                     //  image pixel count
   if (sa_stat == 3) samplesize = sa_Npixel;                                     //  use area count
   samplesize = samplesize * 0.01 * sampWB;                                      //  % sample size >> sample pixel count
   sampR = sampG = sampB = 0;
   sumR = sumG = sumB = 0;
   
   for (ii = 254; ii > 0; ii--)                                                  //  sample brightest % pixels
   {                                                                             //  (avoid blowout value 255) 
      if (sampR < samplesize) {
         sampR += pixR[ii];                                                      //  pixels with R value ii
         sumR += ii * pixR[ii];                                                  //  sum product 
      }

      if (sampG < samplesize) {
         sampG += pixG[ii];
         sumG += ii * pixG[ii];
      }

      if (sampB < samplesize) {
         sampB += pixB[ii];
         sumB += ii * pixB[ii];
      }
   }

   meanR = sumR / sampR;                                                         //  mean RGB for brightest % pixels
   meanG = sumG / sampG;
   meanB = sumB / sampB;
   
   wbalR = meanG / meanR;                                                        //  = 1.0 if all RGB are equal
   wbalG = meanG / meanG;                                                        //  <1/>1 if RGB should be less/more
   wbalB = meanG / meanB;

   return;
}


//  use darkest pixels to estimate black point and reduce image RGB values

void colorbal_autoBL()
{
   using namespace colorbal_names;

   int         pixR[256], pixG[256], pixB[256];                                  //  pixel counts per RGB value 0-255
   int         samplesize;                                                       //  sample size, pixel count
   int         E1ww, E1hh;                                                       //  image dimensions
   int         px, py, ii, dist;
   int         sampR, sampG, sampB;
   double      sumR, sumG, sumB;
   float       meanR, meanG, meanB;
   float       *pix;
   
   for (ii = 0; ii < 256; ii++)
      pixR[ii] = pixG[ii] = pixB[ii] = 0;
   
   E1ww = E1pxm->ww;                                                             //  get image dimensions
   E1hh = E1pxm->hh;

   for (py = 0; py < E1hh; py++)                                                 //  scan image file, get pixel counts
   for (px = 0; px < E1ww; px++)                                                 //    per RGB value 0-255
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E1pxm->ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel outside area
      }

      pix = PXMpix(E1pxm,px,py);
      ii = pix[0];                                                               //  pixel R value 0-255
      pixR[ii]++;                                                                //  accumulate pixel count for R value
      ii = pix[1];
      pixG[ii]++;
      ii = pix[2];
      pixB[ii]++;
   }
   
   samplesize = E1ww * E1hh;                                                     //  image pixel count
   if (sa_stat == 3) samplesize = sa_Npixel;                                     //  use area count
   samplesize = samplesize * 0.01 * sampBL;                                      //  % sample size >> sample pixel count
   sampR = sampG = sampB = 0;
   sumR = sumG = sumB = 0;
   
   for (ii = 0; ii < 100; ii++)                                                  //  sample darkest % pixels
   {
      if (sampR < samplesize) {
         sampR += pixR[ii];                                                      //  pixels with R value ii
         sumR += ii * pixR[ii];                                                  //  sum product 
      }

      if (sampG < samplesize) {
         sampG += pixG[ii];
         sumG += ii * pixG[ii];
      }

      if (sampB < samplesize) {
         sampB += pixB[ii];
         sumB += ii * pixB[ii];
      }
   }
   
   meanR = sumR / sampR;                                                         //  mean RGB for darkest % pixels
   meanG = sumG / sampG;
   meanB = sumB / sampB;

   blackR = meanR;                                                               //  set black RGB levels
   blackG = meanG;
   blackB = meanB;
   Fblack = 1;

   return;
}


//  get nominal white color or black point from mouse click position

void colorbal_mousefunc()                                                        //  mouse function
{
   using namespace colorbal_names;

   int         px, py, dx, dy;
   float       red, green, blue;
   float       *ppix;
   char        mousetext[60];

   if (! LMclick) return;
   LMclick = 0;

   px = Mxclick;                                                                 //  mouse click position
   py = Myclick;
   
   if (px < 2) px = 2;                                                           //  pull back from edge
   if (px > E3pxm->ww-3) px = E3pxm->ww-3;
   if (py < 2) py = 2;
   if (py > E3pxm->hh-3) py = E3pxm->hh-3;

   red = green = blue = 0;

   for (dy = -1; dy <= 1; dy++)                                                  //  3x3 block around mouse position
   for (dx = -1; dx <= 1; dx++)
   {
      ppix = PXMpix(E1pxm,px+dx,py+dy);                                          //  input image
      red += ppix[0];
      green += ppix[1];
      blue += ppix[2];
   }

   red = red / 9.0;                                                              //  mean RGB levels
   green = green / 9.0;
   blue = blue / 9.0;

   snprintf(mousetext,60,"3x3 pixels RGB: %.0f %.0f %.0f \n",red,green,blue);
   poptext_mouse(mousetext,10,10,0,3);
   
   if (spotWB && 
      (red<5 || red>250 || green<5 || green>250 || blue<5 || blue>250)) {        //  refuse unscalable spot
      zmessageACK(Mwin,E2X("choose a better spot"));
      return;
   }

   if (spotWB) {                                                                 //  click pixel is new gray/white
      wbalR = green / red;                                                       //  = 1.0 if all RGB are equal
      wbalG = green / green;                                                     //  <1/>1 if RGB should be less/more
      wbalB = green / blue;
      signal_thread();                                                           //  trigger image update
   }

   if (spotBL) {                                                                 //  click pixel is new black point
      blackR = red;
      blackG = green;
      blackB = blue;
      Fblack = 1;
      signal_thread();                                                           //  update image
   }
   
   return;
}


//  thread function

void * colorbal_thread(void *arg)
{
   using namespace colorbal_names;

   void * colorbal_wthread(void *);

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint

      combR = wbalR * tempR / 256.0;                                             //  combine white balance colors
      combG = wbalG * tempG / 256.0;                                             //    and illumination temperature.
      combB = wbalB * tempB / 256.0;                                             //      <1/>1 if RGB should be less/more
      
      do_wthreads(colorbal_wthread,NWT);

      CEF->Fmods++;                                                              //  image3 modified
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint
      Fpaint2();                                                                 //  update window

      if (CEF->zd) {
         zd_thread = CEF->zd;                                                    //  signal dialog to update graph
         zd_thread_event = "do_histogram";
      }
      else zd_thread = 0;
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


void * colorbal_wthread(void *arg)                                               //  worker thread function
{
   using namespace colorbal_names;

   int         index = *((int *) arg);
   int         ii, dist = 0, px, py;
   float       *pix1, *pix3;
   float       R1, G1, B1;
   float       R3, G3, B3;
   float       Rbp, Gbp, Bbp;
   float       F1, F2, maxRGB;

   Rbp = Gbp = Bbp = 1.0;   
   if (Fblack) {                                                                 //  if RGB black points defined,
      Rbp = 256.0 / (256.0 - blackR);                                            //    rescale for full brightness
      Gbp = 256.0 / (256.0 - blackG);
      Bbp = 256.0 / (256.0 - blackB);
   }
   
   for (py = index; py < E3pxm->hh; py += NWT)                                   //  loop all pixels
   for (px = 0; px < E3pxm->ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3pxm->ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      R1 = pix1[0];                                                              //  input RGB values
      G1 = pix1[1];
      B1 = pix1[2];
      
      //  apply RGB black points to input RGB values
      
      if (Fblack) {
         R1 = R1 - blackR;                                                       //  clip black value at low end 
         R1 = R1 * Rbp;                                                          //  rescale so that high end = 256
         if (R1 < 0) R1 = 0;
         G1 = G1 - blackG;
         G1 = G1 * Gbp;
         if (G1 < 0) G1 = 0;
         B1 = B1 - blackB;
         B1 = B1 * Bbp;
         if (B1 < 0) B1 = 0;
      }

      //  apply white balance and temperature color shifts                       //  dampen as RGB values approach 256
      
      if (combR > 1) {
         F1 = (256 - R1) / 256;                                                  //  R1 = 0...256  >>  F1 = 1...0
         F2 = F1 * combR + 1 - F1;                                               //  F2 = combR ... 1
         R3 = F2 * R1;                                                           //  R3 is adjusted R1
      }
      else R3 = combR * R1;

      if (combG > 1) {                                                           //  same for G3 and B3
         F1 = (256 - G1) / 256;
         F2 = F1 * combG + 1 - F1;
         G3 = F2 * G1;
      }
      else G3 = combG * G1;

      if (combB > 1) {
         F1 = (256 - B1) / 256;
         F2 = F1 * combB + 1 - F1;
         B3 = F2 * B1;
      }
      else B3 = combB * B1;

      if (R3 > 255.9 || G3 > 255.9 || B3 > 255.9) {                              //  stop overflow
         maxRGB = R3;
         if (G3 > maxRGB) maxRGB = G3;
         if (B3 > maxRGB) maxRGB = B3;
         R3 = R3 * 255.9 / maxRGB;
         G3 = G3 * 255.9 / maxRGB;
         B3 = B3 * 255.9 / maxRGB;
      }

      //  select area edge blending

      if (sa_stat == 3 && dist < sa_blendwidth) {
         F2 = sa_blendfunc(dist);
         F1 = 1.0 - F2;
         R3 = F2 * R3 + F1 * R1;
         G3 = F2 * G3 + F1 * G1;
         B3 = F2 * B3 + F1 * B1;
      }

      pix3[0] = R3;
      pix3[1] = G3;
      pix3[2] = B3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  Return relative RGB illumination values for a light source
//     having a given input temperature of 1000-10000 deg. K
//  5000 K is neutral: all returned factors relative to 1.0

void blackbodyRGB(int K, float &R, float &G, float &B)
{
   float    kk[19] = { 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0 };
   float    r1[19] = { 255, 255, 255, 255, 255, 255, 255, 255, 254, 250, 242, 231, 220, 211, 204, 197, 192, 188, 184 };
   float    g1[19] = { 060, 148, 193, 216, 232, 242, 249, 252, 254, 254, 251, 245, 239, 233, 228, 224, 220, 217, 215 };
   float    b1[19] = { 000, 010, 056, 112, 158, 192, 219, 241, 253, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };

   static int     ftf = 1;
   static float   r2[10000], g2[10000], b2[10000];

   if (ftf) {                                                                    //  initialize spline curves
      spline1(19,kk,r1);
      for (int T = 1000; T < 10000; T++)
         r2[T] = spline2(0.001 * T);

      spline1(19,kk,g1);
      for (int T = 1000; T < 10000; T++)
         g2[T] = spline2(0.001 * T);

      spline1(19,kk,b1);
      for (int T = 1000; T < 10000; T++)
         b2[T] = spline2(0.001 * T);
      
      ftf = 0;
   }

   if (K < 1000 || K > 9999) zappcrash("blackbody bad K: %dK",K);

   R = r2[K];
   G = g2[K];
   B = b2[K];

   return;
}


/********************************************************************************/

//  image sharpen functions

namespace sharpen_names
{
   int      E3ww, E3hh;                                                          //  image dimensions
   int      UM_radius, UM_amount, UM_thresh;
   int      GR_amount, GR_thresh;
   int      KH_radius;
   int      MD_radius, MD_dark, MD_light, *MD_britemap;
   char     sharp_function[4] = "";

   int      brhood_radius;
   float    brhood_kernel[200][200];                                             //  up to radius = 99
   char     brhood_method;                                                       //  g = gaussian, f = flat distribution
   float    *brhood_brightness;                                                  //  neighborhood brightness per pixel

   VOL int  sharp_cancel, brhood_cancel;                                         //  avoid GCC optimizing code away

   editfunc    EFsharp;
}


//  menu function

void m_sharpen(GtkWidget *, cchar *menu)
{
   using namespace sharpen_names;

   int    sharp_dialog_event(zdialog *zd, cchar *event);
   void * sharp_thread(void *);
   int    ii;

   F1_help_topic = "sharpen";

   EFsharp.menuname = menu;
   EFsharp.menufunc = m_sharpen;
   EFsharp.funcname = "sharpen";
   EFsharp.Farea = 2;                                                            //  select area usable
   EFsharp.threadfunc = sharp_thread;                                            //  thread function
   EFsharp.Frestart = 1;                                                         //  allow restart
   EFsharp.FusePL = 1;                                                           //  use with paint/lever edits OK
   EFsharp.Fscript = 1;                                                          //  scripting supported
   if (! edit_setup(EFsharp)) return;                                            //  setup edit

   E3ww = E3pxm->ww;                                                             //  image dimensions
   E3hh = E3pxm->hh;

/***
          _________________________________________
         |                 Sharpen                 |
         |                                         |
         |  [_] unsharp mask        radius  [__]   |
         |                          amount  [__]   |
         |                        threshold [__]   |
         |                                         |
         |  [_] gradient            amount  [__]   |
         |                        threshold [__]   |
         |                                         |
         |  [_] Kuwahara            radius  [__]   |
         |                                         |
         |  [_] median diff         radius  [__]   |
         |                           dark   [__]   |
         |                           light  [__]   |
         |                                         |
         |        [reset] [apply] [done] [cancel]  |
         |_________________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Sharpen"),Mwin,Breset,Bapply,Bdone,Bcancel,null);
   EFsharp.zd = zd;

   zdialog_add_widget(zd,"hbox","hbum","dialog",0,"space=5");                    //  unsharp mask
   zdialog_add_widget(zd,"vbox","vb21","hbum",0,"space=2");
   zdialog_add_widget(zd,"label","space","hbum",0,"expand");
   zdialog_add_widget(zd,"vbox","vb22","hbum",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vb23","hbum",0,"homog|space=2");
   zdialog_add_widget(zd,"check","UM","vb21",E2X("unsharp mask"),"space=5");
   zdialog_add_widget(zd,"label","lab21","vb22",Bradius);
   zdialog_add_widget(zd,"label","lab22","vb22",Bamount);
   zdialog_add_widget(zd,"label","lab23","vb22",Bthresh);
   zdialog_add_widget(zd,"zspin","radiusUM","vb23","1|20|1|2");
   zdialog_add_widget(zd,"zspin","amountUM","vb23","1|200|1|100");
   zdialog_add_widget(zd,"zspin","threshUM","vb23","1|100|1|0");

   zdialog_add_widget(zd,"hsep","sep3","dialog");                                //  gradient
   zdialog_add_widget(zd,"hbox","hbgr","dialog",0,"space=5");
   zdialog_add_widget(zd,"vbox","vb31","hbgr",0,"space=2");
   zdialog_add_widget(zd,"label","space","hbgr",0,"expand");
   zdialog_add_widget(zd,"vbox","vb32","hbgr",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vb33","hbgr",0,"homog|space=2");
   zdialog_add_widget(zd,"check","GR","vb31","gradient","space=5");
   zdialog_add_widget(zd,"label","lab32","vb32",Bamount);
   zdialog_add_widget(zd,"label","lab33","vb32",Bthresh);
   zdialog_add_widget(zd,"zspin","amountGR","vb33","1|400|1|100");
   zdialog_add_widget(zd,"zspin","threshGR","vb33","1|100|1|0");

   zdialog_add_widget(zd,"hsep","sep4","dialog");                                //  kuwahara
   zdialog_add_widget(zd,"hbox","hbku","dialog",0,"space=5");
   zdialog_add_widget(zd,"check","KH","hbku","Kuwahara","space=3");
   zdialog_add_widget(zd,"label","space","hbku",0,"expand");
   zdialog_add_widget(zd,"label","lab42","hbku",Bradius,"space=3");
   zdialog_add_widget(zd,"zspin","radiusKH","hbku","1|9|1|1");

   zdialog_add_widget(zd,"hsep","sep5","dialog");                                //  median diff
   zdialog_add_widget(zd,"hbox","hbmd","dialog",0,"space=5");
   zdialog_add_widget(zd,"vbox","vb51","hbmd",0,"space=2");
   zdialog_add_widget(zd,"label","space","hbmd",0,"expand");
   zdialog_add_widget(zd,"vbox","vb52","hbmd",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vb53","hbmd",0,"homog|space=2");
   zdialog_add_widget(zd,"check","MD","vb51",E2X("median diff"),"space=5");
   zdialog_add_widget(zd,"label","lab51","vb52",Bradius);
   zdialog_add_widget(zd,"label","lab52","vb52",E2X("dark"));
   zdialog_add_widget(zd,"label","lab53","vb52",E2X("light"));
   zdialog_add_widget(zd,"zspin","radiusMD","vb53","1|20|1|3");
   zdialog_add_widget(zd,"zspin","darkMD","vb53","0|50|1|1");
   zdialog_add_widget(zd,"zspin","lightMD","vb53","0|50|1|1");

   zdialog_restore_inputs(zd);
   
   zdialog_fetch(zd,"UM",ii);                                                    //  set function from checkboxes
   if (ii) strcpy(sharp_function,"UM");
   zdialog_fetch(zd,"GR",ii);
   if (ii) strcpy(sharp_function,"GR");
   zdialog_fetch(zd,"KH",ii);
   if (ii) strcpy(sharp_function,"KH");
   zdialog_fetch(zd,"MD",ii);
   if (ii) strcpy(sharp_function,"MD");

   zdialog_run(zd,sharp_dialog_event,"save");                                    //  run dialog - parallel

   return;
}


//  dialog event and completion callback function

int sharp_dialog_event(zdialog *zd, cchar *event)                                //  reworked for script files
{
   using namespace sharpen_names;
   
   if (strmatch(event,"focus")) return 1;

   zdialog_fetch(zd,"radiusUM",UM_radius);                                       //  get all parameters
   zdialog_fetch(zd,"amountUM",UM_amount);
   zdialog_fetch(zd,"threshUM",UM_thresh);
   zdialog_fetch(zd,"amountGR",GR_amount);
   zdialog_fetch(zd,"threshGR",GR_thresh);
   zdialog_fetch(zd,"radiusKH",KH_radius);
   zdialog_fetch(zd,"radiusMD",MD_radius);
   zdialog_fetch(zd,"darkMD",MD_dark);
   zdialog_fetch(zd,"lightMD",MD_light);

   if (strmatch(event,"apply")) zd->zstat = 2;                                   //  from script file 
   if (strmatch(event,"done")) zd->zstat = 3;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 4;                                  //  from f_open()
   
   sharp_cancel = 0;

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  reset
         zd->zstat = 0;
         edit_reset();
         return 1;
      }

      if (zd->zstat == 2) {                                                      //  apply
         zd->zstat = 0;
         if (*sharp_function) signal_thread();
         else zmessageACK(Mwin,Bnoselection);                                    //  no choice made
         return 1;
      }

      if (zd->zstat == 3) {
         edit_done(0);                                                           //  done
         return 1;
      }

      sharp_cancel = 1;                                                          //  cancel or [x]
      edit_cancel(0);                                                            //  discard edit
      return 1;
   }

   if (strmatch(event,"blendwidth")) signal_thread();

   if (strmatchV(event,"UM","GR","KH","MD",null))
   {
      zdialog_stuff(zd,"UM",0);                                                  //  make checkboxes like radio buttons
      zdialog_stuff(zd,"GR",0);
      zdialog_stuff(zd,"KH",0);
      zdialog_stuff(zd,"MD",0);
      zdialog_stuff(zd,event,1);
      strcpy(sharp_function,event);                                              //  set chosen method
   }

   return 1;
}


//  sharpen image thread function

void * sharp_thread(void *)
{
   using namespace sharpen_names;

   int sharp_UM(void);
   int sharp_GR(void);
   int sharp_KH(void);
   int sharp_MD(void);

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      if (strmatch(sharp_function,"UM")) sharp_UM();
      if (strmatch(sharp_function,"GR")) sharp_GR();
      if (strmatch(sharp_function,"KH")) sharp_KH();
      if (strmatch(sharp_function,"MD")) sharp_MD();

      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


//  image sharpen function using unsharp mask

int sharp_UM()
{
   using namespace sharpen_names;

   void  britehood(int radius, char method);                                     //  compute neighborhood brightness
   void * sharp_UM_wthread(void *arg);

   int      cc;

   cc = E3ww * E3hh * sizeof(float);
   brhood_brightness = (float *) zmalloc(cc);

   britehood(UM_radius,'f');
   if (sharp_cancel) return 1;

   if (sa_stat == 3) Fbusy_goal = sa_Npixel;
   else  Fbusy_goal = E3ww * E3hh;
   Fbusy_done = 0;

   do_wthreads(sharp_UM_wthread,NWT);                                            //  worker threads

   Fbusy_goal = 0;

   zfree(brhood_brightness);
   return 1;
}


void * sharp_UM_wthread(void *arg)                                               //  worker thread function
{
   using namespace sharpen_names;

   int         index = *((int *) arg);
   int         px, py, ii, dist = 0;
   float       amount, thresh, bright;
   float       mean, incr, ratio, f1, f2;
   float       red1, green1, blue1, red3, green3, blue3;
   float       *pix1, *pix3;

   for (py = index; py < E3hh; py += NWT)                                        //  loop all image3 pixels
   for (px = 0; px < E3ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      amount = 0.01 * UM_amount;                                                 //  0.0 to 2.0
      thresh = 0.4 * UM_thresh;                                                  //  0 to 40 (256 max. possible)

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      bright = pixbright(pix1);
      if (bright < 1) continue;                                                  //  effectively black
      ii = py * E3ww + px;
      mean = brhood_brightness[ii];

      incr = (bright - mean);
      if (fabsf(incr) < thresh) continue;                                        //  omit low-contrast pixels

      incr = incr * amount;                                                      //  0.0 to 2.0
      if (bright + incr > 255) incr = 255 - bright;
      ratio = (bright + incr) / bright;
      if (ratio < 0) ratio = 0;

      red1 = pix1[0];                                                            //  input RGB
      green1 = pix1[1];
      blue1 = pix1[2];

      red3 = ratio * red1;                                                       //  output RGB
      if (red3 > 255) red3 = 255;
      green3 = ratio * green1;
      if (green3 > 255) green3 = 255;
      blue3 = ratio * blue1;
      if (blue3 > 255) blue3 = 255;

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         red3 = f1 * red3 + f2 * red1;
         green3 = f1 * green3 + f2 * green1;
         blue3 = f1 * blue3 + f2 * blue1;
      }

      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;

      Fbusy_done++;                                                              //  track progress
      if (sharp_cancel) break;
   }

   pthread_exit(0);
}


//  sharpen image by increasing brightness gradient

int sharp_GR()
{
   using namespace sharpen_names;

   void * sharp_GR_wthread(void *arg);

   if (sa_stat == 3) Fbusy_goal = sa_Npixel;
   else  Fbusy_goal = E3ww * E3hh;
   Fbusy_done = 0;

   do_wthreads(sharp_GR_wthread,NWT);                                            //  worker threads

   Fbusy_goal = 0;
   return 1;
}


//  callable sharp_GR() used by trim/rotate function
//  returns E3 = sharpened E3

void sharp_GR_callable(int amount, int thresh)                                   //  18.07
{
   using namespace sharpen_names;

   PXM *PXMtemp = E1pxm;                                                         //  save E1
   E1pxm = PXM_copy(E3pxm);                                                      //  copy E3 > E1
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;
   GR_amount = amount;
   GR_thresh = thresh;
   sharp_GR();                                                                   //  E3 = sharpened E1
   PXM_free(E1pxm);
   E1pxm = PXMtemp;                                                              //  restore org. E1
   return;
}


void * sharp_GR_wthread(void *arg)                                               //  worker thread function 
{
   using namespace sharpen_names;

   float       *pix1, *pix3;
   int         ii, px, py, dist = 0;
   int         nc = E1pxm->nc;
   float       amount, thresh;
   float       b1, b1x, b1y, b3x, b3y, b3, bf, f1, f2;
   float       red1, green1, blue1, red3, green3, blue3;

   int         index = *((int *) arg);

   amount = 1 + 0.01 * GR_amount;                                                //  1.0 - 5.0
   thresh = GR_thresh;                                                           //  0 - 100

   for (py = index + 1; py < E3hh; py += NWT)                                    //  loop all image pixels
   for (px = 1; px < E3ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel is outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      b1 = pixbright(pix1);                                                      //  pixel brightness, 0 - 256
      if (b1 == 0) continue;                                                     //  black, don't change
      b1x = b1 - pixbright(pix1-nc);                                             //  horiz. brightness gradient
      b1y = b1 - pixbright(pix1-nc * E3ww);                                      //  vertical
      f1 = fabsf(b1x + b1y);

      if (f1 < thresh)                                                           //  moderate brightness change for
         f1 = f1 / thresh;                                                       //    pixels below threshold gradient
      else  f1 = 1.0;
      f2 = 1.0 - f1;

      b1x = b1x * amount;                                                        //  amplified gradient
      b1y = b1y * amount;

      b3x = pixbright(pix1-nc) + b1x;                                            //  + prior pixel brightness
      b3y = pixbright(pix1-nc * E3ww) + b1y;                                     //  = new brightness
      b3 = 0.5 * (b3x + b3y);

      b3 = f1 * b3 + f2 * b1;                                                    //  possibly moderated

      bf = b3 / b1;                                                              //  ratio of brightness change
      if (bf < 0) bf = 0;
      if (bf > 4) bf = 4;

      red1 = pix1[0];                                                            //  input RGB
      green1 = pix1[1];
      blue1 = pix1[2];

      red3 = bf * red1;                                                          //  output RGB
      if (red3 > 255.9) red3 = 255.9;
      green3 = bf * green1;
      if (green3 > 255.9) green3 = 255.9;
      blue3 = bf * blue1;
      if (blue3 > 255.9) blue3 = 255.9;

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         red3 = f1 * red3 + f2 * red1;
         green3 = f1 * green3 + f2 * green1;
         blue3 = f1 * blue3 + f2 * blue1;
      }

      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;

      Fbusy_done++;                                                              //  track progress
      if (sharp_cancel) break;
   }

   pthread_exit(0);
}


//  sharpen edges using the Kuwahara algorithm

int sharp_KH()
{
   using namespace sharpen_names;

   void * sharp_KH_wthread(void *arg);
   
   if (sa_stat == 3) Fbusy_goal = sa_Npixel;
   else  Fbusy_goal = E3ww * E3hh;
   Fbusy_done = 0;

   do_wthreads(sharp_KH_wthread,NWT);                                            //  worker threads

   Fbusy_goal = 0;
   return 1;
}


void * sharp_KH_wthread(void *arg)                                               //  worker thread function
{
   using namespace sharpen_names;

   float       *pix1, *pix3;
   int         px, py, qx, qy, rx, ry;
   int         ii, rad, N, dist = 0;
   float       red, green, blue, red2, green2, blue2;
   float       vmin, vall, vred, vgreen, vblue;
   float       red3, green3, blue3;
   float       f1, f2;

   int      index = *((int *) arg);

   rad = KH_radius;                                                              //  user input radius
   N = (rad + 1) * (rad + 1);

   for (py = index + rad; py < E3hh-rad; py += NWT)                              //  loop all image pixels
   for (px = rad; px < E3ww-rad; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel is outside area
      }

      vmin = 99999;
      red3 = green3 = blue3 = 0;

      for (qy = py - rad; qy <= py; qy++)                                        //  loop all surrounding neighborhoods
      for (qx = px - rad; qx <= px; qx++)
      {
         red = green = blue = 0;
         red2 = green2 = blue2 = 0;

         for (ry = qy; ry <= qy + rad; ry++)                                     //  loop all pixels in neighborhood
         for (rx = qx; rx <= qx + rad; rx++)
         {
            pix1 = PXMpix(E1pxm,rx,ry);
            red += pix1[0];                                                      //  compute mean RGB and mean RGB**2
            red2 += pix1[0] * pix1[0];
            green += pix1[1];
            green2 += pix1[1] * pix1[1];
            blue += pix1[2];
            blue2 += pix1[2] * pix1[2];
         }

         red = red / N;                                                          //  mean RGB of neighborhood
         green = green / N;
         blue = blue / N;

         vred = red2 / N - red * red;                                            //  variance RGB
         vgreen = green2 / N - green * green;
         vblue = blue2 / N - blue * blue;

         vall = vred + vgreen + vblue;                                           //  save RGB values with least variance
         if (vall < vmin) {
            vmin = vall;
            red3 = red;
            green3 = green;
            blue3 = blue;
         }
      }

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  if select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         pix1 = PXMpix(E1pxm,px,py);                                             //  input pixel
         red3 = f1 * red3 + f2 * pix1[0];
         green3 = f1 * green3 + f2 * pix1[1];
         blue3 = f1 * blue3 + f2 * pix1[2];
      }

      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel
      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;

      Fbusy_done++;                                                              //  track progress
      if (sharp_cancel) break;
   }

   pthread_exit(0);
}


//  sharpen edges using the median difference algorithm

int sharp_MD()
{
   using namespace sharpen_names;

   void * sharp_MD_wthread(void *arg);
   
   int      px, py, ii;
   float    *pix1;
   
   MD_britemap = (int *) zmalloc(E3ww * E3hh * sizeof(int));
   
   for (py = 0; py < E3hh; py++)                                                 //  loop all pixels
   for (px = 0; px < E3ww; px++)
   {
      pix1 = PXMpix(E1pxm,px,py);                                                //  initz. pixel brightness map
      ii = py * E3ww + px;
      MD_britemap[ii] = pixbright(pix1);                                         //  18.07
   }

   if (sa_stat == 3) Fbusy_goal = sa_Npixel;
   else  Fbusy_goal = E3ww * E3hh;
   Fbusy_done = 0;
   
   do_wthreads(sharp_MD_wthread,NWT);

   Fbusy_goal = 0;

   zfree(MD_britemap);
   return 1;
}


void * sharp_MD_wthread(void *arg)                                               //  worker thread function
{
   using namespace sharpen_names;
   
   int         index = *((int *) arg);
   int         rad, dark, light, *britemap;
   int         ii, px, py, dist = 0;
   int         dy, dx, ns;
   float       R, G, B, R2, G2, B2;
   float       F, f1, f2;
   float       *pix1, *pix3;
   int         bright, median;
   int         bsortN[1681];                                                     //  radius <= 20 (41 x 41 pixels)
   
   rad = MD_radius;                                                              //  parameters from dialog
   dark = MD_dark;
   light = MD_light;
   britemap = MD_britemap;

   for (py = index+rad; py < E3hh-rad; py += NWT)                                //  loop all image3 pixels
   for (px = rad; px < E3ww-rad; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  source pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  target pixel

      ns = 0;

      for (dy = py-rad; dy <= py+rad; dy++)                                      //  loop surrounding pixels
      for (dx = px-rad; dx <= px+rad; dx++)                                      //  get brightness values
      {
         ii = dy * E3ww + dx;
         bsortN[ns] = britemap[ii];
         ns++;
      }

      HeapSort(bsortN,ns);                                                       //  sort the pixels
      median = bsortN[ns/2];                                                     //  median brightness
      
      R = pix3[0];
      G = pix3[1];
      B = pix3[2];
      
      bright = pixbright(pix3);                                                  //  18.07
      
      if (bright < median) {
         F = 1.0 - 0.1 * dark * (median - bright) / (median + 50);
         R2 = R * F;
         G2 = G * F;
         B2 = B * F;
         if (R2 > 0 && G2 > 0 && B2 > 0) {
            R = R2;
            G = G2;
            B = B2;
         }
      }
      
      if (bright > median) {
         F = 1.0 + 0.03 * light * (bright - median) / (median + 50);
         R2 = R * F;
         G2 = G * F;
         B2 = B * F;
         if (R2 < 255 && G2 < 255 && B2 < 255) {
            R = R2;
            G = G2;
            B = B2;
         }
      }

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         R = f1 * R + f2 * pix1[0];
         G = f1 * G + f2 * pix1[1];
         B = f1 * B + f2 * pix1[2];
      }
      
      pix3[0] = R;
      pix3[1] = G;
      pix3[2] = B;

      Fbusy_done++;                                                              //  track progress
   }

   pthread_exit(0);
}


//  Compute the mean brightness of all pixel neighborhoods,
//  using a Guassian or a flat distribution for the weightings.
//  If a select area is active, only inside pixels are calculated.
//  The flat method is 10-100x faster than the Guassian method.

void britehood(int radius, char method)
{
   using namespace sharpen_names;

   void * brhood_wthread(void *arg);

   int      rad, radflat2, dx, dy;
   float    kern;

   brhood_radius = radius;
   brhood_method = method;

   if (brhood_method == 'g')                                                     //  compute Gaussian kernel
   {                                                                             //  (not currently used)
      rad = brhood_radius;
      radflat2 = rad * rad;

      for (dy = -rad; dy <= rad; dy++)
      for (dx = -rad; dx <= rad; dx++)
      {
         if (dx*dx + dy*dy <= radflat2)                                          //  cells within radius
            kern = exp( - (dx*dx + dy*dy) / radflat2);
         else kern = 0;                                                          //  outside radius
         brhood_kernel[dy+rad][dx+rad] = kern;
      }
   }

   if (sa_stat == 3) Fbusy_goal = sa_Npixel;                                     //  set up progress tracking
   else Fbusy_goal = E3ww * E3hh;
   Fbusy_done = 0;

   do_wthreads(brhood_wthread,NWT);                                              //  worker threads

   Fbusy_goal = 0;
   return;
}


//  worker thread function

void * brhood_wthread(void *arg)
{
   using namespace sharpen_names;

   int      index = *((int *) arg);
   int      rad = brhood_radius;
   int      ii, px, py, qx, qy, Fstart;
   float    kern, bsum, bsamp, bmean;
   float    *pixel;

   if (brhood_method == 'g')                                                     //  use round gaussian distribution
   {
      for (py = index; py < E3hh; py += NWT)
      for (px = 0; px < E3ww; px++)
      {
         if (sa_stat == 3 && sa_mode != mode_image) {                            //  select area, not whole image
            ii = py * E3ww + px;                                                 //    use only inside pixels
            if (! sa_pixmap[ii]) continue;
         }

         bsum = bsamp = 0;

         for (qy = py-rad; qy <= py+rad; qy++)                                   //  computed weighted sum of brightness
         for (qx = px-rad; qx <= px+rad; qx++)                                   //    for pixels in neighborhood
         {
            if (qy < 0 || qy > E3hh-1) continue;
            if (qx < 0 || qx > E3ww-1) continue;
            kern = brhood_kernel[qy+rad-py][qx+rad-px];
            pixel = PXMpix(E1pxm,qx,qy);
            bsum += pixbright(pixel) * kern;                                     //  sum brightness * weight
            bsamp += kern;                                                       //  sum weights
         }

         bmean = bsum / bsamp;                                                   //  mean brightness
         ii = py * E3ww + px;
         brhood_brightness[ii] = bmean;                                          //  pixel value

         Fbusy_done++;                                                           //  track progress
         if (sharp_cancel) break;
      }
   }

   if (brhood_method == 'f')                                                     //  use square flat distribution
   {
      Fstart = 1;
      bsum = bsamp = 0;

      for (py = index; py < E3hh; py += NWT)
      for (px = 0; px < E3ww; px++)
      {
         if (sa_stat == 3 && sa_mode != mode_image) {                            //  select area, not whole image
            ii = py * E3ww + px;                                                 //     compute only inside pixels
            if (! sa_pixmap[ii]) {
               Fstart = 1;
               continue;
            }
         }

         if (px == 0) Fstart = 1;

         if (Fstart)
         {
            Fstart = 0;
            bsum = bsamp = 0;

            for (qy = py-rad; qy <= py+rad; qy++)                                //  add up all columns
            for (qx = px-rad; qx <= px+rad; qx++)
            {
               if (qy < 0 || qy > E3hh-1) continue;
               if (qx < 0 || qx > E3ww-1) continue;
               pixel = PXMpix(E1pxm,qx,qy);
               bsum += pixbright(pixel);
               bsamp += 1;
            }
         }
         else
         {
            qx = px-rad-1;                                                       //  subtract first-1 column
            if (qx >= 0) {
               for (qy = py-rad; qy <= py+rad; qy++)
               {
                  if (qy < 0 || qy > E3hh-1) continue;
                  pixel = PXMpix(E1pxm,qx,qy);
                  bsum -= pixbright(pixel);
                  bsamp -= 1;
               }
            }
            qx = px+rad;                                                         //  add last column
            if (qx < E3ww) {
               for (qy = py-rad; qy <= py+rad; qy++)
               {
                  if (qy < 0 || qy > E3hh-1) continue;
                  pixel = PXMpix(E1pxm,qx,qy);
                  bsum += pixbright(pixel);
                  bsamp += 1;
               }
            }
         }

         bmean = bsum / bsamp;                                                   //  mean brightness
         ii = py * E3ww + px;
         brhood_brightness[ii] = bmean;

         Fbusy_done++;                                                           //  track progress
         if (sharp_cancel) break;
      }
   }

   pthread_exit(0);
}


/********************************************************************************/

//  image blur function

namespace blur_names
{
   int         Fnormblur;                                                        //  normal blur
   float       Nblur_radius;                                                     //  blur radius
   float       blur_weight[1415];                                                //  blur radius limit 999 

   int         Fradblur;                                                         //  radial blur
   int         RBlen;                                                            //  radial blur length
   int         Cx, Cy;                                                           //  image center of radial blur
   
   int         Fdirblur;                                                         //  directed blur
   float       Dmdx, Dmdy, Dmdw, Dmdh;
   float       DD, Dspan, Dintens;
   
   int         Fgradblur;                                                        //  graduated blur
   float       gblur_radius;                                                     //  blur radius
   int         con_limit;                                                        //  contrast limit
   uint8       *pixcon;
   int         pixseq_done[122][122];                                            //  up to gblur_radius = 60
   int         pixseq_angle[1000];
   int         pixseq_dx[13000], pixseq_dy[13000];
   int         pixseq_rad[13000];
   int         max1 = 999, max2 = 12999;                                         //  for later overflow check
   
   int         Fpaintblur;                                                       //  paint blur
   int         pmode = 1;                                                        //  1/2 = blend/restore
   int         pblur_radius = 20;                                                //  mouse radius
   float       powcent, powedge;                                                 //  power at center and edge
   float       kernel[402][402];                                                 //  radius limit 200
   int         mousex, mousey;                                                   //  mouse click/drag position
   
   int         Fblurbackground;                                                  //  blur background via select area

   editfunc    EFblur;
   VOL int     Fcancel;
   PXM         *E2pxm;
   int         E3ww, E3hh;                                                       //  image dimensions
}


//  menu function

void m_blur(GtkWidget *, cchar *menu)                                            //  consolidate all blur functions     18.07
{
   using namespace blur_names;

   int    blur_dialog_event(zdialog *zd, cchar *event);
   void   blur_mousefunc();
   void * blur_thread(void *);

   cchar  *radblur_tip = E2X("Click to set center");
   cchar  *dirblur_tip = E2X("Pull image using the mouse");
   cchar  *paintblur_tip = E2X("left drag: blend image \n"
                               "right drag: restore image");

   F1_help_topic = "blur";

   EFblur.menuname = menu;
   EFblur.menufunc = m_blur;
   EFblur.funcname = "blur";
   EFblur.Farea = 2;                                                             //  select area usable
   EFblur.threadfunc = blur_thread;                                              //  thread function
   EFblur.mousefunc = blur_mousefunc;
   EFblur.Frestart = 1;                                                          //  allow restart
   EFblur.Fscript = 1;                                                           //  scripting supported
   if (! edit_setup(EFblur)) return;                                             //  setup edit

/***
          _________________________________________
         |             Blur Image                  |
         |                                         |
         |  [x] Normal Blur   Radius [____]        |
         |  - - - - - - - - - - - - - - - - - -    |
         |  [x] Radial Blur                        |
         |  Length [___]  Center X [___] Y [___]   |
         |  - - - - - - - - - - - - - - - - - -    |
         |  [X] Directed Blur                      |
         |  Blur Span  [___]  Intensity   [___]    |
         |  - - - - - - - - - - - - - - - - - -    |
         |  [X] Graduated Blur                     |
         |  Radius [___]  Contrast Limit [___]     |
         |  - - - - - - - - - - - - - - - - - -    |
         |  [X] Paint Blur                         |
         |  Radius [___]  Power [___]  Edge [___]  |
         |  - - - - - - - - - - - - - - - - - -    |
         |  [X] Blur Background                    |
         |                                         |
         |         [Reset] [Apply] [Done] [Cancel] |
         |_________________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Blur Radius"),Mwin,Breset,Bapply,Bdone,Bcancel,null);
   EFblur.zd = zd;

   zdialog_add_widget(zd,"hbox","hbnb","dialog");
   zdialog_add_widget(zd,"check","Fnormblur","hbnb",E2X("Normal Blur"),"space=2");
   zdialog_add_widget(zd,"label","space","hbnb",0,"space=5");
   zdialog_add_widget(zd,"label","labrad","hbnb",Bradius,"space=5");
   zdialog_add_widget(zd,"zspin","Nblur_radius","hbnb","1|999|1|10","space=5|size=3");

   zdialog_add_widget(zd,"hsep","sep1","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbrb1","dialog");
   zdialog_add_widget(zd,"check","Fradblur","hbrb1",E2X("Radial Blur"),"space=2");
   zdialog_add_widget(zd,"hbox","hbrb2","dialog");
   zdialog_add_widget(zd,"label","labrbl","hbrb2",Blength,"space=5");
   zdialog_add_widget(zd,"zspin","RBlen","hbrb2","1|999|1|100","space=3|size=3");
   zdialog_add_widget(zd,"label","space","hbrb2",0,"space=5");
   zdialog_add_widget(zd,"label","labc","hbrb2",Bcenter,"space=3");
   zdialog_add_widget(zd,"label","labcx","hbrb2","X","space=3");
   zdialog_add_widget(zd,"zentry","Cx","hbrb2",0,"space=3|size=3");
   zdialog_add_widget(zd,"label","space","hbrb2",0,"space=3");
   zdialog_add_widget(zd,"label","labcy","hbrb2","Y","space=3");
   zdialog_add_widget(zd,"zentry","Cy","hbrb2",0,"space=3|size=3");

   zdialog_add_widget(zd,"hsep","sep1","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbdb1","dialog");
   zdialog_add_widget(zd,"check","Fdirblur","hbdb1",E2X("Directed Blur"),"space=2");
   zdialog_add_widget(zd,"hbox","hbdb2","dialog");
   zdialog_add_widget(zd,"label","labspan","hbdb2",E2X("Blur Span"),"space=5");
   zdialog_add_widget(zd,"zspin","span","hbdb2","0.00|1.0|0.01|0.1","space=3|size=3");
   zdialog_add_widget(zd,"label","space","hbdb2",0,"space=5");
   zdialog_add_widget(zd,"label","labint","hbdb2",E2X("Intensity"));
   zdialog_add_widget(zd,"zspin","intens","hbdb2","0.00|1.0|0.01|0.2","space=3|size=3");

   zdialog_add_widget(zd,"hsep","sep1","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbgb1","dialog");
   zdialog_add_widget(zd,"check","Fgradblur","hbgb1",E2X("Graduated Blur"),"space=2");
   zdialog_add_widget(zd,"hbox","hbgb2","dialog");
   zdialog_add_widget(zd,"label","labgrad","hbgb2",Bradius,"space=5");
   zdialog_add_widget(zd,"zspin","gblur_radius","hbgb2","1|50|1|10","space=3|size=3");
   zdialog_add_widget(zd,"label","space","hbgb2",0,"space=5");
   zdialog_add_widget(zd,"label","lablim","hbgb2",E2X("Contrast Limit"));
   zdialog_add_widget(zd,"zspin","con_limit","hbgb2","1|255|1|50","space=3|size=3");

   zdialog_add_widget(zd,"hsep","sep1","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbpb1","dialog");
   zdialog_add_widget(zd,"check","Fpaintblur","hbpb1",E2X("Paint Blur"),"space=2");
   zdialog_add_widget(zd,"hbox","hbpb2","dialog");
   zdialog_add_widget(zd,"label","labpaint","hbpb2",Bradius,"space=5");
   zdialog_add_widget(zd,"zspin","pblur_radius","hbpb2","2|200|1|20","space=3|size=3");
   zdialog_add_widget(zd,"label","space","hbpb2",0,"space=5");
   zdialog_add_widget(zd,"label","labpow","hbpb2",E2X("Power"));
   zdialog_add_widget(zd,"zspin","powcent","hbpb2","0|100|1|30","space=3|size=3");
   zdialog_add_widget(zd,"label","space","hbpb2",0,"space=5");
   zdialog_add_widget(zd,"label","labedge","hbpb2",Bedge);
   zdialog_add_widget(zd,"zspin","powedge","hbpb2","0|100|1|10","space=3|size=3");
   
   zdialog_add_widget(zd,"hsep","sep1","dialog",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbbg","dialog");
   zdialog_add_widget(zd,"check","Fblurbackground","hbbg",E2X("Blur Background"),"space=2");

   zdialog_add_ttip(zd,"Fradblur",radblur_tip);
   zdialog_add_ttip(zd,"Fdirblur",dirblur_tip);
   zdialog_add_ttip(zd,"Fpaintblur",paintblur_tip);
   
   E3ww = E3pxm->ww;                                                             //  image dimensions
   E3hh = E3pxm->hh;

   Fcancel = 0;                                                                  //  initial status
   E2pxm = 0;

   Fnormblur = 0;                                                                //  default settings
   Nblur_radius = 10;

   Fradblur = 0;
   RBlen = 100;
   Cx = E3ww / 2;
   Cy = E3hh / 2;
   zdialog_stuff(zd,"Cx",Cx);
   zdialog_stuff(zd,"Cy",Cy);

   Fdirblur = 0;
   Dspan = 0.1;
   Dintens = 0.2;

   Fgradblur = 0;
   con_limit = 1;
   gblur_radius = 10;
   
   Fpaintblur = 0;
   pmode = 1;
   pblur_radius = 20;
   powcent = 30;
   powedge = 10;
   
   zdialog_restore_inputs(zd);

   zdialog_stuff(zd,"Fblurbackground",0);                                        //  no blur background at start
   Fblurbackground = 0;

   zdialog_run(zd,blur_dialog_event,"save");                                     //  run dialog - parallel
   zdialog_send_event(zd,"pblur_radius");                                        //  get kernel initialized
   return;
}


//  dialog event and completion callback function

int blur_dialog_event(zdialog * zd, cchar *event)
{
   using namespace blur_names;

   void   blur_mousefunc();

   float    frad, kern;
   int      rad, dx, dy;

   if (strmatch(event,"done")) zd->zstat = 3;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 4;                                  //  from f_open()
   
   if (strstr("Fnormblur Fradblur Fdirblur Fgradblur "                           //  checkboxes work like radio buttons
              "Fpaintblur Fblurbackground",event)) {
      zdialog_stuff(zd,"Fnormblur",0);
      zdialog_stuff(zd,"Fradblur",0);
      zdialog_stuff(zd,"Fdirblur",0);
      zdialog_stuff(zd,"Fgradblur",0);
      zdialog_stuff(zd,"Fpaintblur",0);
      zdialog_stuff(zd,"Fblurbackground",0);
      zdialog_stuff(zd,event,1);
      zdialog_fetch(zd,"Fnormblur",Fnormblur);
      zdialog_fetch(zd,"Fradblur",Fradblur);
      zdialog_fetch(zd,"Fdirblur",Fdirblur);
      zdialog_fetch(zd,"Fgradblur",Fgradblur);
      zdialog_fetch(zd,"Fpaintblur",Fpaintblur);
      zdialog_fetch(zd,"Fblurbackground",Fblurbackground);
   }
   
   if (Fradblur || Fdirblur)                                                     //  connect mouse
      takeMouse(blur_mousefunc,dragcursor);
   else if (Fpaintblur)
      takeMouse(blur_mousefunc,0);
   else freeMouse();

   zdialog_fetch(zd,"Fnormblur",Fnormblur);                                      //  get all dialog inputs
   zdialog_fetch(zd,"Nblur_radius",Nblur_radius);

   zdialog_fetch(zd,"Fradblur",Fradblur);
   zdialog_fetch(zd,"RBlen",RBlen);
   zdialog_fetch(zd,"Cx",Cx);
   zdialog_fetch(zd,"Cy",Cy);

   zdialog_fetch(zd,"Fdirblur",Fdirblur);
   zdialog_fetch(zd,"span",Dspan);
   zdialog_fetch(zd,"intens",Dintens);

   zdialog_fetch(zd,"Fgradblur",Fgradblur);
   zdialog_fetch(zd,"gblur_radius",gblur_radius);
   zdialog_fetch(zd,"con_limit",con_limit);

   zdialog_fetch(zd,"Fpaintblur",Fpaintblur);
   zdialog_fetch(zd,"pblur_radius",pblur_radius);
   zdialog_fetch(zd,"powcent",powcent);
   zdialog_fetch(zd,"powedge",powedge);
   
   zdialog_fetch(zd,"Fblurbackground",Fblurbackground);

   if (Fblurbackground) {                                                        //  cancel and start new function
      Fcancel = 1;
      edit_cancel(0);
      if (E2pxm) PXM_free(E2pxm);
      E2pxm = 0;
      m_blur_background(0,0);
      return 1;                                                                  //  19.0
   }

   if (strstr("pblur_radius powcent powedge",event))                             //  paint blur parameters
   {
      zdialog_fetch(zd,"pblur_radius",pblur_radius);                             //  mouse radius
      zdialog_fetch(zd,"powcent",powcent);                                       //  center transparency
      zdialog_fetch(zd,"powedge",powedge);                                       //  powedge transparency

      powcent = 0.01 * powcent;                                                  //  scale 0 ... 1
      powedge = 0.01 * powedge;
      rad = pblur_radius;

      for (dy = -rad; dy <= rad; dy++)                                           //  build kernel
      for (dx = -rad; dx <= rad; dx++)
      {
         frad = sqrt(dx*dx + dy*dy);
         kern = (rad - frad) / rad;                                              //  center ... powedge  >>  1 ... 0
         kern = kern * (powcent - powedge) + powedge;                            //  strength  center ... powedge
         if (kern < 0) kern = 0;
         if (kern > 1) kern = 1;
         if (frad > rad) kern = 2;                                               //  beyond radius, within square
         kernel[dx+rad][dy+rad] = kern;
      }
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1)                                                        //  [reset]
      {
         zd->zstat = 0;                                                          //  keep dialog active
         edit_reset();
      }

      else if (zd->zstat == 2)                                                   //  [apply]
      {
         zd->zstat = 0;                                                          //  keep dialog active
         signal_thread();                                                        //  trigger thread
         return 1;                                                               //  do not free E2                     19.0
      }

      else if (zd->zstat == 3)                                                   //  [done]
         edit_done(0);

      else {                                                                     //  [cancel]
         Fcancel = 1;
         edit_cancel(0);                                                         //  discard edit
      }

      if (E2pxm) PXM_free(E2pxm);                                                //  free memory
      E2pxm = 0;
   }

   return 1;
}


//  blur mouse function

void blur_mousefunc()                                                            //  mouse function
{
   using namespace blur_names;
   
   if (! CEF) return;
   
   if (Fnormblur) 
   {
      freeMouse();
      return;
   }

   if (Fradblur && LMclick)                                                      //  radial blur, new center
   {
      zdialog *zd = CEF->zd;
      Cx = Mxposn;
      Cy = Myposn;
      zdialog_stuff(zd,"Cx",Cx);
      zdialog_stuff(zd,"Cy",Cy);
      LMclick = 0;
      signal_thread();
   }

   if (Fdirblur && (Mxdrag || Mydrag))                                           //  directed blur, mouse drag
   {
      Dmdx = Mxdown;                                                             //  drag origin
      Dmdy = Mydown;
      Dmdw = Mxdrag - Mxdown;                                                    //  drag increment
      Dmdh = Mydrag - Mydown;
      Mxdrag = Mydrag = 0;
      signal_thread();
   }
   
   if (Fpaintblur)                                                               //  paint blur
   {
      int      px, py, rr;

      if (LMclick || RMclick)                                                    //  mouse click
      {
         if (LMclick) pmode = 1;                                                 //  left click, paint
         if (RMclick) pmode = 2;                                                 //  right click, erase
         mousex = Mxclick;
         mousey = Myclick;
         signal_thread();
      }

      else if (Mxdrag || Mydrag)                                                 //  mouse drag in progress
      {
         if (Mbutton == 1) pmode = 1;                                            //  left drag, paint
         if (Mbutton == 3) pmode = 2;                                            //  right drag, erase
         mousex = Mxdrag;
         mousey = Mydrag;
         signal_thread();
      }

      cairo_t *cr = draw_context_create(gdkwin,draw_context);

      px = mousex - pblur_radius - 1;                                            //  repaint modified area
      py = mousey - pblur_radius - 1;
      rr = 2 * pblur_radius + 3;
      Fpaint3(px,py,rr,rr,cr);

      draw_mousecircle(Mxposn,Myposn,pblur_radius,0,cr);                         //  redraw mouse circle
      draw_context_destroy(draw_context);

      LMclick = RMclick = Mxdrag = Mydrag = 0;                                   //  reset mouse
   }

   return;
}


//  image blur thread function

void * blur_thread(void *)
{
   using namespace blur_names;

   void * normblur_wthread(void *);
   void * radblur_wthread(void *);
   void * dirblur_wthread(void *);
   void * gradblur_wthread(void *);
   void * paintblur_wthread(void *);

   int         ii, jj;
   float       rad, w, wsum;
   float       dd, d1, d2, d3, d4;
   int         px, py, dx, dy, adx, ady;
   float       *pix1, *pix2;
   float       contrast, maxcon;
   float       rad1, rad2, angle, astep;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request
      
      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      if (Fnormblur)                                                             //  normal blur
      {
         if (E2pxm) PXM_free(E2pxm);
         E2pxm = PXM_copy(E1pxm);                                                //  intermediate image

         rad = Nblur_radius;
         wsum = 0;
         
         for (ii = 0; ii < rad; ii++)                                            //  set pixel weight per distance
         {                                                                       //      example, rad = 10
            w = 1.0 - ii / rad;                                                  //  dist:   0   1   2   3   5   7   9
            w = w * w;                                                           //  weight: 1  .81 .64 .49 .25 .09 .01
            blur_weight[ii] = w;
            wsum += w;
         }
         
         for (ii = 0; ii < rad; ii++)                                            //  make weights sum to 1.0
            blur_weight[ii] = blur_weight[ii] / wsum;

         if (sa_stat == 3) Fbusy_goal = sa_Npixel;                               //  setup progress counter
         else  Fbusy_goal = E3ww * E3hh;
         Fbusy_goal = 2 * Fbusy_goal;
         Fbusy_done = 0;

         do_wthreads(normblur_wthread,NWT);                                      //  worker threads
      }

      if (Fradblur)                                                              //  radial blur                        18.07
      {
         if (E2pxm) PXM_free(E2pxm);

         if (Fnormblur)                                                          //  if normal blur done before,
            E2pxm = PXM_copy(E3pxm);                                             //    use the blur output image
         else E2pxm = PXM_copy(E1pxm);                                           //  else use the original image

         if (sa_stat == 3) Fbusy_goal = sa_Npixel;                               //  setup progress counter
         else  Fbusy_goal = E3ww * E3hh;
         Fbusy_done = 0;

         do_wthreads(radblur_wthread,NWT);                                       //  worker threads
      }
      
      if (Fdirblur)                                                              //  directed blur
      {
         d1 = (Dmdx-0) * (Dmdx-0) + (Dmdy-0) * (Dmdy-0);                         //  distance, mouse to 4 corners
         d2 = (E3ww-Dmdx) * (E3ww-Dmdx) + (Dmdy-0) * (Dmdy-0);
         d3 = (E3ww-Dmdx) * (E3ww-Dmdx) + (E3hh-Dmdy) * (E3hh-Dmdy);
         d4 = (Dmdx-0) * (Dmdx-0) + (E3hh-Dmdy) * (E3hh-Dmdy);

         dd = d1;
         if (d2 > dd) dd = d2;                                                   //  find greatest corner distance
         if (d3 > dd) dd = d3;
         if (d4 > dd) dd = d4;

         DD = dd * 0.5 * Dspan;

         do_wthreads(dirblur_wthread,NWT);                                       //  worker threads
      }
      
      if (Fgradblur)                                                             //  graduated blur
      {
         pixcon = (uint8 *) zmalloc(E3ww * E3hh);                                //  pixel contrast map

         for (py = 1; py < E3hh-1; py++)                                         //  loop interior pixels
         for (px = 1; px < E3ww-1; px++)
         {
            pix1 = PXMpix(E1pxm,px,py);                                          //  this pixel in base image E1
            contrast = maxcon = 0.0;

            for (dx = px-1; dx <= px+1; dx++)                                    //  loop neighbor pixels
            for (dy = py-1; dy <= py+1; dy++)
            {
               pix2 = PXMpix(E1pxm,dx,dy);
               contrast = 1.0 - PIXMATCH(pix1,pix2);                             //  contrast, 0-1
               if (contrast > maxcon) maxcon = contrast;
            }

            ii = py * E3ww + px;                                                 //  ii maps to (px,py)
            pixcon[ii] = 255 * maxcon;                                           //  pixel contrast, 0 to 255
         }

         rad1 = gblur_radius;

         for (dy = 0; dy <= 2*rad1; dy++)                                        //  no pixels mapped yet
         for (dx = 0; dx <= 2*rad1; dx++)
            pixseq_done[dx][dy] = 0;

         ii = jj = 0;

         astep = 0.5 / rad1;                                                     //  0.5 pixel steps at rad1 from center

         for (angle = 0; angle < 2*PI; angle += astep)                           //  loop full circle
         {
            pixseq_angle[ii] = jj;                                               //  start pixel sequence for this angle
            ii++;

            for (rad2 = 1; rad2 <= rad1; rad2++)                                 //  loop rad2 from center to edge
            {
               dx = lround(rad2 * cos(angle));                                   //  pixel at angle and rad2
               dy = lround(rad2 * sin(angle));
               adx = rad1 + dx;
               ady = rad1 + dy;
               if (pixseq_done[adx][ady]) continue;                              //  pixel already mapped
               pixseq_done[adx][ady] = 1;                                        //  map pixel
               pixseq_dx[jj] = dx;                                               //  save pixel sequence for angle
               pixseq_dy[jj] = dy;
               pixseq_rad[jj] = rad2;                                            //  pixel radius
               jj++;
            }
            pixseq_rad[jj] = 9999;                                               //  mark end of pixels for angle
            jj++;
         }

         pixseq_angle[ii] = 9999;                                                //  mark end of angle steps

         if (ii > max1 || jj > max2)                                             //  should not happen
            zappcrash("gradblur array overflow");

         if (sa_stat == 3) Fbusy_goal = sa_Npixel;                               //  setup progress tracking
         else  Fbusy_goal = E3ww * E3hh;
         Fbusy_done = 0;

         do_wthreads(gradblur_wthread,NWT);                                      //  worker threads

         zfree(pixcon);
      }
      
      if (Fpaintblur)
         do_wthreads(paintblur_wthread,NWT);                                     //  worker threads
      
      Fbusy_goal = 0;
      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();
   }
   
   return 0;                                                                     //  not executed, stop g++ warning
}


//  normal blur worker thread

void * normblur_wthread(void *arg)                                               //  faster algorithm                   18.07 
{
   using namespace blur_names;

   int      index = *((int *) arg);
   int      rad = Nblur_radius;
   int      ii, dist = 0;
   int      px, py, qx, qy;
   int      ylo, yhi, xlo, xhi;
   float    R, G, B, w1, w2, f1, f2;
   float    *pix1, *pix3, *pix2;

   for (py = index; py < E3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < E3ww; px++)
   {
      if (Fcancel) pthread_exit(0);                                              //  user cancel

      ii = py * E3ww + px;

      if (sa_stat == 3) {                                                        //  select area active
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  source pixel
      pix2 = PXMpix(E2pxm,px,py);                                                //  target pixel - intermediate image
      
      ylo = py - rad;
      if (ylo < 0) ylo = 0;
      yhi = py + rad;
      if (yhi > E3hh-1) yhi = E3hh - 1;
      
      R = G = B = 0;
      w2 = 0;

      for (qy = ylo; qy <= yhi; qy++)                                            //  loop pixels in same column
      {
         if (sa_stat == 3) {  
            ii = qy * E3ww + px;                                                 //  don't use pixels outside area   
            dist = sa_pixmap[ii];
            if (! dist) continue;
         }
         
         w1 = blur_weight[abs(qy-py)];                                           //  weight based on radius
         pix1 = PXMpix(E1pxm,px,qy);
         R += w1 * pix1[0];                                                      //  accumulate RGB * weight
         G += w1 * pix1[1];
         B += w1 * pix1[2];
         w2 += w1;                                                               //  accumulate weights
      }
      
      R = R / w2;                                                                //  normalize
      G = G / w2;
      B = B / w2;

      pix2[0] = R;                                                               //  weighted average
      pix2[1] = G;
      pix2[2] = B;

      Fbusy_done++;                                                              //  track progress
   }

   for (py = index; py < E3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < E3ww; px++)
   {
      if (Fcancel) pthread_exit(0);                                              //  user cancel

      ii = py * E3ww + px;

      if (sa_stat == 3) {                                                        //  select area active
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      pix2 = PXMpix(E2pxm,px,py);                                                //  source pixel - intermediate image
      pix3 = PXMpix(E3pxm,px,py);                                                //  target pixel - final image
      
      xlo = px - rad;
      if (xlo < 0) xlo = 0;
      xhi = px + rad;
      if (xhi > E3ww-1) xhi = E3ww - 1;
      
      R = G = B = 0;
      w2 = 0;

      for (qx = xlo; qx <= xhi; qx++)                                            //  loop pixels in same row
      {
         if (sa_stat == 3) {  
            ii = py * E3ww + qx;                                                 //  don't use pixels outside area   
            dist = sa_pixmap[ii];
            if (! dist) continue;
         }
         
         w1 = blur_weight[abs(qx-px)];                                           //  weight based on radius
         pix2 = PXMpix(E2pxm,qx,py);
         R += w1 * pix2[0];                                                      //  accumulate RGB * weight
         G += w1 * pix2[1];
         B += w1 * pix2[2];
         w2 += w1;                                                               //  accumulate weights
      }
      
      R = R / w2;                                                                //  normalize
      G = G / w2;
      B = B / w2;

      pix3[0] = R;                                                               //  weighted average
      pix3[1] = G;
      pix3[2] = B;

      Fbusy_done++;                                                              //  track progress
   }

   if (sa_stat == 3 && sa_blendwidth > 0)                                        //  select area has edge blend
   {
      for (py = index; py < E3hh; py += NWT)                                     //  loop all image pixels
      for (px = 0; px < E3ww; px++)
      {
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];
         if (! dist) continue;                                                   //  omit pixels outside area
         if (dist >= sa_blendwidth) continue;                                    //  omit if > blendwidth from edge

         pix1 = PXMpix(E1pxm,px,py);                                             //  source pixel
         pix3 = PXMpix(E3pxm,px,py);                                             //  target pixel
         f1 = sa_blendfunc(dist);                                                //  blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         pix3[0] = f1 * pix3[0] + f2 * pix1[0];
         pix3[1] = f1 * pix3[1] + f2 * pix1[1];
         pix3[2] = f1 * pix3[2] + f2 * pix1[2];
      }
   }

   pthread_exit(0);
}


//  radial blur worker thread

void * radblur_wthread(void *arg)                                                //  18.07
{
   using namespace blur_names;

   int      index = *((int *) arg);
   int      ii, dist = 0;
   int      px, py, qx, qy, qz;
   float    *pix2, *pix3;
   float    R, Rx, Ry, Rz;
   float    f1, f2;
   int      Rsum, Gsum, Bsum, Npix;

   for (py = index+1; py < E3hh-1; py += NWT)                                    //  loop all image pixels
   for (px = 1; px < E3ww-1; px++)
   {
      if (Fcancel) pthread_exit(0);                                              //  user cancel

      ii = py * E3ww + px;

      if (sa_stat == 3) {                                                        //  select area active
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }
      
      Rsum = Gsum = Bsum = Npix = 0;                                             //  reset RGB sums
      
      R = sqrtf((px-Cx)*(px-Cx) + (py-Cy)*(py-Cy));                              //  distance (Cx,Cy) to (px,py)
      if (R == 0) continue;
      Rx = (px-Cx)/R;                                                            //  unit vector along (Cx,Cy) to (px,py)
      Ry = (py-Cy)/R;
      if (fabsf(Rx) > fabsf(Ry)) Rz = 1.0 / fabsf(Rx);                           //  Rz is 1.0 .. 1.414
      else Rz = 1.0 / fabsf(Ry);
      Rx = Rx * Rz;                                                              //  vector with max x/y component = 1
      Ry = Ry * Rz;
      
      for (qz = 0; qz < RBlen; qz++)                                             //  loop (qx,qy) from (px,py) 
      {                                                                          //    in direction to (Cx,Cy)
         qx = px - qz * Rx;                                                      //      for distance RBlen
         qy = py - qz * Ry;
         if (qx < 0 || qx > E3ww-1) break;
         if (qy < 0 || qy > E3hh-1) break;

         if (sa_stat == 3) {  
            ii = qy * E3ww + qx;                                                 //  don't use pixels outside area   
            dist = sa_pixmap[ii];
            if (! dist) continue;
         }

         pix2 = PXMpix(E2pxm,qx,qy);                                             //  sum RGB values
         Rsum += pix2[0];
         Gsum += pix2[1];
         Bsum += pix2[2];
         Npix++;                                                                 //  count pixels in sum
      }
      
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel is average of 
      pix3[0] = Rsum / Npix;                                                     //   pixels in line of length RBlen
      pix3[1] = Gsum / Npix;                                                     //    from (px,py) to (Cx,Cy)
      pix3[2] = Bsum / Npix;

      Fbusy_done++;                                                              //  track progress
   }

   if (sa_stat == 3 && sa_blendwidth > 0)                                        //  select area has edge blend
   {
      for (py = index; py < E3hh; py += NWT)                                     //  loop all image pixels
      for (px = 0; px < E3ww; px++)
      {
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];
         if (! dist) continue;                                                   //  omit pixels outside area
         if (dist >= sa_blendwidth) continue;                                    //  omit if > blendwidth from edge

         pix2 = PXMpix(E2pxm,px,py);                                             //  source pixel
         pix3 = PXMpix(E3pxm,px,py);                                             //  target pixel
         f1 = sa_blendfunc(dist);                                                //  blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         pix3[0] = f1 * pix3[0] + f2 * pix2[0];
         pix3[1] = f1 * pix3[1] + f2 * pix2[1];
         pix3[2] = f1 * pix3[2] + f2 * pix2[2];
      }
   }

   pthread_exit(0);
}


//  directed blur worker thread

void * dirblur_wthread(void *arg)
{
   using namespace blur_names;

   int      index = *((int *) arg);
   int      ii, px, py, dist = 0, vstat;
   float    d, mag, dispx, dispy;
   float    F1, F2, f1, f2;
   float    vpix[4], *pix1, *pix3;

   F1 = Dintens * Dintens;
   F2 = 1.0 - F1;

   for (py = index; py < E3hh; py += NWT)                                        //  process all pixels
   for (px = 0; px < E3ww; px++)
   {
      ii = py * E3ww + px;

      if (sa_stat == 3) {                                                        //  select area active                 18.07
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel outside area
      }

      d = (px-Dmdx)*(px-Dmdx) + (py-Dmdy)*(py-Dmdy);
      mag = (1.0 - d / DD);
      if (mag < 0) continue;

      mag = mag * mag;                                                           //  faster than pow(mag,4);
      mag = mag * mag;

      dispx = -Dmdw * mag;                                                       //  displacement = drag * mag
      dispy = -Dmdh * mag;

      vstat = vpixel(E3pxm,px+dispx,py+dispy,vpix);                              //  input virtual pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel
      if (vstat) {
         pix3[0] = F2 * pix3[0] + F1 * vpix[0];                                  //  output = input pixel blend
         pix3[1] = F2 * pix3[1] + F1 * vpix[1];
         pix3[2] = F2 * pix3[2] + F1 * vpix[2];
      }

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  within edge blend area             18.07
         pix1 = PXMpix(E1pxm,px,py);                                             //  input pixel
         f1 = sa_blendfunc(dist);
         f2 = 1.0 - f1;
         pix3[0] = f1 * pix3[0] + f2 * pix1[0];
         pix3[1] = f1 * pix3[1] + f2 * pix1[1];
         pix3[2] = f1 * pix3[2] + f2 * pix1[2];
      }
   }

   pthread_exit(0);                                                              //  exit thread
}


//  graduated blur worker thread

void * gradblur_wthread(void *arg)
{
   using namespace blur_names;

   int      index = *((int *) arg);
   int      ii, jj, npix, dist = 0;
   int      px, py, dx, dy;
   float    red, green, blue, f1, f2;
   float    *pix1, *pix3, *pixN;
   int      rad, blurrad, con;
   
   for (py = index+1; py < E3hh-1; py += NWT)                                    //  loop interior pixels
   for (px = 1; px < E3ww-1; px++)
   {
      if (Fcancel) pthread_exit(0);

      ii = py * E3ww + px;

      if (sa_stat == 3) {                                                        //  select area active
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel outside area
      }

      if (pixcon[ii] > con_limit) continue;                                      //  high contrast pixel

      pix1 = PXMpix(E1pxm,px,py);                                                //  source pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  target pixel

      red = pix1[0];                                                             //  blur center pixel
      green = pix1[1];
      blue = pix1[2];
      npix = 1;

      blurrad = 1.0 + gblur_radius * (con_limit - pixcon[ii]) / con_limit;       //  blur radius for pixel, 1 - gblur_radius

      for (ii = 0; ii < 2000; ii++)                                              //  loop angle around center pixel
      {
         jj = pixseq_angle[ii];                                                  //  1st pixel for angle step ii
         if (jj == 9999) break;                                                  //  none, end of angle loop

         while (true)                                                            //  loop pixels from center to radius
         {
            rad = pixseq_rad[jj];                                                //  next pixel step radius
            if (rad > blurrad) break;                                            //  stop here if beyond blur radius
            dx = pixseq_dx[jj];                                                  //  next pixel step px/py
            dy = pixseq_dy[jj];
            if (px+dx < 0 || px+dx > E3ww-1) break;                              //  stop here if off the edge
            if (py+dy < 0 || py+dy > E3hh-1) break;

            pixN = PXMpix(E1pxm,px+dx,py+dy);
            con = 255 * (1.0 - PIXMATCH(pix1,pixN));
            if (con > con_limit) break;

            red += pixN[0];                                                      //  add pixel RGB values
            green += pixN[1];
            blue += pixN[2];
            npix++;
            jj++;
         }
      }

      pix3[0] = red / npix;                                                      //  average pixel values
      pix3[1] = green / npix;
      pix3[2] = blue / npix;
      
      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  within edge blend area
         f1 = sa_blendfunc(dist);
         f2 = 1.0 - f1;
         pix3[0] = f1 * pix3[0] + f2 * pix1[0];
         pix3[1] = f1 * pix3[1] + f2 * pix1[1];
         pix3[2] = f1 * pix3[2] + f2 * pix1[2];
      }

      Fbusy_done++;                                                              //  track progress
   }

   pthread_exit(0);
}


//  paint blur worker thread

void * paintblur_wthread(void *arg)
{
   using namespace blur_names;
   
   int         index = *((int *) arg);
   float       *pix1, *pix3, *pixm;
   int         radius, radius2, npix;
   int         px, py, dx, dy, qx, qy, rx, ry, sx, sy;
   int         ii, dist = 0;
   float       kern, kern2, meanR, meanG, meanB;

   px = mousex;
   py = mousey;
   radius = pblur_radius;

   for (dy = -radius+index; dy <= radius; dy += NWT)                             //  loop within mouse radius
   for (dx = -radius; dx <= radius; dx++)
   {
      qx = px + dx;
      qy = py + dy;

      if (qx < 0 || qx > E3ww-1) continue;                                       //  off image
      if (qy < 0 || qy > E3hh-1) continue;

      if (sa_stat == 3) {                                                        //  select area active
         ii = qy * E3ww + qx;
         dist = sa_pixmap[ii];
         if (! dist) continue;                                                   //  pixel is outside area
      }

      kern = kernel[dx+radius][dy+radius];                                       //  mouse transparencies
      if (kern > 1) continue;                                                    //  outside mouse radius

      if (sa_stat == 3 && dist < sa_blendwidth)                                  //  within blend distance
         kern = kern * sa_blendfunc(dist);
         
      pix1 = PXMpix(E1pxm,qx,qy);                                                //  original pixel
      pix3 = PXMpix(E3pxm,qx,qy);                                                //  edited pixel
      
      meanR = meanG = meanB = npix = 0;
      radius2 = sqrtf(radius);                                                   //  radius = 2..99  >>  radius2 = 1..9
      
      for (ry = -radius2; ry <= radius2; ry++)
      for (rx = -radius2; rx <= radius2; rx++)
      {
         sx = qx + rx;
         sy = qy + ry;
         
         if (px - sx < -radius || px - sx > radius) continue;                    //  outside mouse radius
         if (py - sy < -radius || py - sy > radius) continue;

         if (sx < 0 || sx > E3ww-1) continue;                                    //  off image
         if (sy < 0 || sy > E3hh-1) continue;
         
         pixm = PXMpix(E3pxm,sx,sy);
         meanR += pixm[0];         
         meanG += pixm[1];         
         meanB += pixm[2];         
         npix++;
      }
      
      if (npix == 0) continue;
      
      meanR = meanR / npix;
      meanG = meanG / npix;
      meanB = meanB / npix;
      
      if (pmode == 1) {                                                          //  blend
         kern2 = 0.5 * kern;
         pix3[0] = kern2 * meanR + (1.0 - kern2) * pix3[0];                      //  pix3 tends to regional mean
         pix3[1] = kern2 * meanG + (1.0 - kern2) * pix3[1];
         pix3[2] = kern2 * meanB + (1.0 - kern2) * pix3[2];
      }

      if (pmode == 2) {                                                          //  restore
         kern2 = 0.1 * kern;
         pix3[0] = kern2 * pix1[0] + (1.0 - kern2) * pix3[0];                    //  pix3 tends to pix1
         pix3[1] = kern2 * pix1[1] + (1.0 - kern2) * pix3[1];
         pix3[2] = kern2 * pix1[2] + (1.0 - kern2) * pix3[2];
      }
   }

   pthread_exit(0);
}


/********************************************************************************/

//  Blur Background
//  Blur the image outside of a selected area or areas. 
//  Blur increases with distance from selected area edges.

namespace blur_BG_names 
{
   int         conrad, incrad;               //  constant or increasing blur
   int         conbrad;                      //  constant blur radius
   int         minbrad;                      //  min. blur radius
   int         maxbrad;                      //  max. blur radius
   VOL int     Fcancel;                      //  GCC inconsistent
   int         E3ww, E3hh;                   //  image dimensions
   int         maxdist;                      //  max. area edge distance
   editfunc    EFblurBG;
}


//  called from main blur function, no separate menu

void m_blur_background(GtkWidget *, const char *)
{
   using namespace blur_BG_names;

   int    blur_BG_dialog_event(zdialog* zd, const char *event);
   void * blur_BG_thread(void *);

   EFblurBG.menufunc = m_blur_background;
   EFblurBG.funcname = "blur_background";                                        //  function name
   EFblurBG.Farea = 2;                                                           //  select area usable (required)
   EFblurBG.threadfunc = blur_BG_thread;                                         //  thread function
      
   if (! edit_setup(EFblurBG)) return;                                           //  setup edit

   minbrad = 10;                                                                 //  defaults
   maxbrad = 20;
   conbrad = 10;
   conrad = 1;
   incrad = 0;
   Fcancel = 0;

   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;
   
/***
       ____________________________________
      |          Blur Background           |
      |                                    |
      |  [x] constant blur [ 20 ]          |
      |                                    |
      |  [x] increase blur with distance   |
      |    min. blur radius [ 20 ]         |
      |    max. blur radius [ 90 ]         |
      |                                    |
      |            [Apply] [Done] [Cancel] |
      |____________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Blur Background"),Mwin,Bapply,Bdone,Bcancel,null);
   CEF->zd = zd;
   
   zdialog_add_widget(zd,"hbox","hbcon","dialog",0,"space=5");
   zdialog_add_widget(zd,"check","conrad","hbcon",E2X("constant blur"),"space=3");
   zdialog_add_widget(zd,"zspin","conbrad","hbcon","1|100|1|10","space=8");
   zdialog_add_widget(zd,"hbox","hbinc","dialog");
   zdialog_add_widget(zd,"check","incrad","hbinc",E2X("increase blur with distance"),"space=3");
   zdialog_add_widget(zd,"hbox","hbmin","dialog");
   zdialog_add_widget(zd,"label","labmin","hbmin",E2X("min. blur radius"),"space=8");
   zdialog_add_widget(zd,"zspin","minbrad","hbmin","0|100|1|10","space=3");
   zdialog_add_widget(zd,"hbox","hbmax","dialog");
   zdialog_add_widget(zd,"label","labmax","hbmax",E2X("max. blur radius"),"space=8");
   zdialog_add_widget(zd,"zspin","maxbrad","hbmax","1|100|1|20","space=3");
   
   zdialog_stuff(zd,"conrad",conrad);
   zdialog_stuff(zd,"incrad",incrad);

   zdialog_resize(zd,300,0);
   zdialog_restore_inputs(zd);                                                   //  restore previous inputs
   zdialog_run(zd,blur_BG_dialog_event,"save");                                  //  run dialog - parallel
   
   if (sa_stat != 3) m_select(0,0);                                              //  start select area dialog
   return;
}


//  blur_BG dialog event and completion function

int blur_BG_dialog_event(zdialog *zd, const char *event)                         //  blur_BG dialog event function
{
   using namespace blur_BG_names;
   
   if (strmatch(event,"done")) zd->zstat = 2;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  from f_open()

   if (zd->zstat)
   {
      if (zd->zstat == 1)                                                        //  [apply]
      {
         zd->zstat = 0;                                                          //  keep dialog active

         if (sa_stat != 3) {
            zmessageACK(Mwin,E2X("no active Select Area"));
            m_select(0,0);
            return 1;
         }
         
         if (incrad && ! sa_calced)                                              //  if increading blur radius,
            sa_edgecalc();                                                       //    calc. area edge distances

         sa_show(0,0);
         edit_reset();
         signal_thread();
         return 1;
      }
      
      else if (zd->zstat == 2) {
         edit_done(0);                                                           //  [done]
         if (zd_sela) zdialog_send_event(zd_sela,"done");                        //  kill select area dialog
      }

      else {
         Fcancel = 1;                                                            //  kill threads
         edit_cancel(0);                                                         //  [cancel] or [x], discard edit
         if (zd_sela) zdialog_send_event(zd_sela,"done");
      }

      return 1;
   }
   
   if (strstr("conrad incrad",event)) {
      zdialog_stuff(zd,"conrad",0);
      zdialog_stuff(zd,"incrad",0);
      zdialog_stuff(zd,event,1);
   }
   
   zdialog_fetch(zd,"conrad",conrad);
   zdialog_fetch(zd,"incrad",incrad);
   zdialog_fetch(zd,"conbrad",conbrad);
   zdialog_fetch(zd,"minbrad",minbrad);
   zdialog_fetch(zd,"maxbrad",maxbrad);
   
   return 1;
}


//  thread function - multiple working threads to update image

void * blur_BG_thread(void *)
{
   using namespace blur_BG_names;

   void  * blur_BG_wthread(void *arg);                                           //  worker thread
   
   int      ii, dist;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request
      
      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      if (incrad && sa_calced) {                                                 //  if increasing blur radius,
         maxdist = 0;                                                            //    get max. area edge distance
         for (ii = 0; ii < E3ww * E3hh; ii++) {
            dist = sa_pixmap[ii];
            if (dist > maxdist) maxdist = dist;
         }
      }

      Fbusy_goal = sa_Npixel;
      Fbusy_done = 0;

      do_wthreads(blur_BG_wthread,NWT);                                          //  worker threads
      
      Fbusy_goal = Fbusy_done = 0;

      CEF->Fmods++;                                                              //  image modified
      CEF->Fsaved = 0;                                                           //  not saved

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop warning
}


void * blur_BG_wthread(void *arg)                                                //  worker thread function
{
   using namespace blur_BG_names;

   int         index = *((int *) (arg));
   int         ii, rad = 0, dist, npix;
   int         px, py, qx, qy;
   float       *pix1, *pix3;
   float       red, green, blue, F;
   
   for (py = index; py < E3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < E3ww; px++)
   {
      if (Fcancel) break;                                                        //  cancel edit

      ii = py * E3ww + px;
      dist = sa_pixmap[ii];                                                      //  area edge distance
      if (! dist) continue;                                                      //  pixel outside the area
      
      if (conrad) rad = conbrad;                                                 //  use constant blur radius

      if (incrad) {                                                              //  use increasing blur radius
         if (! sa_calced) pthread_exit(0);                                       //    depending on edge distance
         rad = minbrad + (maxbrad - minbrad) * dist / maxdist;
      }

      npix = 0;
      red = green = blue = 0;      

      for (qy = py-rad; qy <= py+rad; qy++)                                      //  average surrounding pixels
      for (qx = px-rad; qx <= px+rad; qx++)
      {
         if (qy < 0 || qy > E3hh-1) continue;
         if (qx < 0 || qx > E3ww-1) continue;
         ii = qy * E3ww + qx;
         if (! sa_pixmap[ii]) continue;
         pix1 = PXMpix(E1pxm,qx,qy);
         red += pix1[0];
         green += pix1[1];
         blue += pix1[2];
         npix++;
      }
      
      F = 0.999 / npix;
      red = F * red;                                                             //  blurred pixel RGB
      green = F * green;
      blue = F * blue;
      pix3 = PXMpix(E3pxm,px,py);
      pix3[0] = red;
      pix3[1] = green;
      pix3[2] = blue;

      Fbusy_done++;                                                              //  count pixels done
   }
   
   pthread_exit(0);                                                              //  exit thread
}


/********************************************************************************/

//  image noise reduction

namespace denoise_names
{
   enum     denoise_method { flatten, median, tophat, wavelets }
            denoise_method;

   int      denoise_radius;
   float    denoise_thresh;
   float    denoise_darkareas;

   zdialog  *zd_denoise_measure;
   cchar    *mformat = "  mean RGB:  %5.0f %5.0f %5.0f ";
   cchar    *nformat = " mean noise: %5.2f %5.2f %5.2f ";
   
   int      E3ww, E3hh;                                                          //  image dimensions
   int8     *Fpix, *Gpix;

   editfunc    EFdenoise;
   GtkWidget   *denoise_measure_drawwin;
}


//  menu function

void m_denoise(GtkWidget *, cchar *menu)
{
   using namespace denoise_names;

   int    denoise_dialog_event(zdialog *zd, cchar *event);
   void * denoise_thread(void *);
   
   cchar  *tip = E2X("Apply repeatedly while watching the image.");
   cchar  *Bmeasure = E2X("Measure");

   F1_help_topic = "denoise";

   EFdenoise.menuname = menu;
   EFdenoise.menufunc = m_denoise;
   EFdenoise.funcname = "denoise";
   EFdenoise.Farea = 2;                                                          //  select area usable
   EFdenoise.threadfunc = denoise_thread;                                        //  thread function
   EFdenoise.Frestart = 1;                                                       //  allow restart
   EFdenoise.FusePL = 1;                                                         //  use with paint/lever edits OK
   EFdenoise.Fscript = 1;                                                        //  scripting supported 
   if (! edit_setup(EFdenoise)) return;                                          //  setup edit

   E3ww = E3pxm->ww;                                                             //  image dimensions
   E3hh = E3pxm->hh;

/***
          _____________________________________________
         |           Noise Reduction                   |
         |  Apply repeatedly while watching the image. |
         |                                             |
         |                   Radius  Threshold         |
         |  (o)  Flatten      [___]    [___]           |
         |  (o)  Median       [___]    [___]           |
         |  (o)  Top Hat      [___]    [___]           |
         |  (o)  Wavelets              [___]           |
         |                                             |
         | dark areas =========[]=========== all areas |
         |                                             |
         |   [Measure] [Apply] [Reset] [Done] [Cancel] |
         |_____________________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Noise Reduction"),Mwin,Bmeasure,Bapply,Breset,Bdone,Bcancel,null);
   EFdenoise.zd = zd;
   
   zdialog_add_widget(zd,"label","labtip","dialog",tip,"space=3");

   zdialog_add_widget(zd,"hbox","hbvb","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","space","hbvb",0,"space=3");
   zdialog_add_widget(zd,"vbox","vb1","hbvb",0,"homog");
   zdialog_add_widget(zd,"label","space","hbvb",0,"space=3");
   zdialog_add_widget(zd,"vbox","vb2","hbvb",0,"homog");
   zdialog_add_widget(zd,"label","space","hbvb",0,"space=10");
   zdialog_add_widget(zd,"vbox","vb3","hbvb",0,"homog");
   zdialog_add_widget(zd,"label","space","hbvb",0,"space=10");
   zdialog_add_widget(zd,"vbox","vb4","hbvb",0,"homog");

   zdialog_add_widget(zd,"label","space","vb1");
   zdialog_add_widget(zd,"radio","flatten","vb1");
   zdialog_add_widget(zd,"radio","median","vb1");
   zdialog_add_widget(zd,"radio","tophat","vb1");
   zdialog_add_widget(zd,"radio","wavelets","vb1");

   zdialog_add_widget(zd,"label","space","vb2");
   zdialog_add_widget(zd,"label","labflatten","vb2",E2X("Flatten"));
   zdialog_add_widget(zd,"label","labmedian","vb2",E2X("Median"));
   zdialog_add_widget(zd,"label","labtophat","vb2",E2X("Top Hat"));
   zdialog_add_widget(zd,"label","labwavelets","vb2",E2X("Wavelets"));

   zdialog_add_widget(zd,"label","labraius","vb3",Bradius);
   zdialog_add_widget(zd,"hbox","hb31","vb3");
   zdialog_add_widget(zd,"zspin","radflatten","hb31","1|9|1|3","size=4");
   zdialog_add_widget(zd,"hbox","hb32","vb3");
   zdialog_add_widget(zd,"zspin","radmedian","hb32","1|9|1|2","size=4");
   zdialog_add_widget(zd,"hbox","hb33","vb3");
   zdialog_add_widget(zd,"zspin","radtophat","hb33","1|9|1|2","size=4");
   zdialog_add_widget(zd,"label","space","vb3");

   zdialog_add_widget(zd,"label","labthresh","vb4",Bthresh);
   zdialog_add_widget(zd,"hbox","hb41","vb4");
   zdialog_add_widget(zd,"zspin","flattenthresh","hb41","1|255|1|20","size=4|space=12");
   zdialog_add_widget(zd,"hbox","hb42","vb4");
   zdialog_add_widget(zd,"zspin","medianthresh","hb42","1|255|1|20","size=4|space=12");
   zdialog_add_widget(zd,"hbox","hb43","vb4");
   zdialog_add_widget(zd,"zspin","tophatthresh","hb43","1|255|1|20","size=4|space=12");
   zdialog_add_widget(zd,"hbox","hb44","vb4");
   zdialog_add_widget(zd,"zspin","waveletsthresh","hb44","0.1|8.0|0.1|2","size=4|space=12");

   zdialog_add_widget(zd,"hbox","hbdark","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labdark","hbdark",E2X("dark areas"),"space=8");
   zdialog_add_widget(zd,"hscale","darkareas","hbdark","0|256|1|256","expand");                             //  bugfix  19.5
   zdialog_add_widget(zd,"label","laball","hbdark",E2X("all areas"),"space=8");

   zdialog_restore_inputs(zd);
   zdialog_run(zd,denoise_dialog_event,"save");                                  //  run dialog - parallel
   return;
}


//  dialog event and completion callback function

int denoise_dialog_event(zdialog * zd, cchar *event)                             //  reworked for script files
{
   using namespace denoise_names;

   void   denoise_measure();

   int    ii;

   wait_thread_idle();
   
   if (strmatch(event,"apply")) zd->zstat = 2;                                   //  from script file
   if (strmatch(event,"done")) zd->zstat = 4;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 5;                                  //  from f_open()

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  [measure] 
         zd->zstat = 0;                                                          //  keep dialog active
         denoise_measure();
         return 1;
      }
      
      if (zd->zstat == 2) {                                                      //  [apply]
         zd->zstat = 0;                                                          //  keep dialog active
         signal_thread();
         return 1;
      }

      if (zd->zstat == 3) {                                                      //  [reset]
         edit_undo();                                                            //  undo edits
         zd->zstat = 0;                                                          //  keep dialog active
         return 1;
      }

      if (zd->zstat == 4) edit_done(0);                                          //  [done]  commit edit
      else edit_cancel(0);                                                       //  [cancel] or [x]  discard edit

      if (zd_denoise_measure) {                                                  //  kill measure dialog 
         freeMouse();
         zdialog_free(zd_denoise_measure);
         zd_denoise_measure = 0;
      }

      return 1;
   }
   
   zdialog_fetch(zd,"darkareas",denoise_darkareas);
   
   if (strmatch(event,"blendwidth")) signal_thread();
   
   if (strstr("flatten median tophat wavelets",event)) {                         //  capture choice
      zdialog_stuff(zd,"flatten",0);
      zdialog_stuff(zd,"median",0);
      zdialog_stuff(zd,"tophat",0);
      zdialog_stuff(zd,"wavelets",0);
      zdialog_stuff(zd,event,1);
   }

   zdialog_fetch(zd,"flatten",ii);
   if (ii) {
      denoise_method = flatten;
      zdialog_fetch(zd,"radflatten",denoise_radius);
      zdialog_fetch(zd,"flattenthresh",denoise_thresh);
   }

   zdialog_fetch(zd,"median",ii);
   if (ii) {
      denoise_method = median;
      zdialog_fetch(zd,"radmedian",denoise_radius);
      zdialog_fetch(zd,"medianthresh",denoise_thresh);
   }
   
   zdialog_fetch(zd,"tophat",ii);
   if (ii) {
      denoise_method = tophat;
      zdialog_fetch(zd,"radtophat",denoise_radius);
      zdialog_fetch(zd,"tophatthresh",denoise_thresh);
   }
   
   zdialog_fetch(zd,"wavelets",ii);
   if (ii) {
      denoise_method = wavelets;
      zdialog_fetch(zd,"waveletsthresh",denoise_thresh);
   }
   
   return 1;
}


//  image noise reduction thread

void * denoise_thread(void *)
{
   using namespace denoise_names;

   void * denoise_wthread(void *arg);
   void * denoise_wavelet_wthread(void *arg);

   int      ii, px, py, dist = 0;
   float    *pix3, *pix9;
   int      nc = E3pxm->nc, pcc = nc * sizeof(float);

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      E9pxm = PXM_copy(E3pxm);                                                   //  image3 is source, image9 is modified

      if (denoise_method == wavelets)                                            //  wavelet method
         do_wthreads(denoise_wavelet_wthread,3);                                 //  worker threads, 1 per RGB color
      
      else                                                                       //  other method
      {
         if (sa_stat == 3) Fbusy_goal = sa_Npixel;
         else  Fbusy_goal = E3ww * E3hh;
         Fbusy_done = 0;

         do_wthreads(denoise_wthread,NWT);                                       //  worker threads

         Fbusy_goal = 0;
      }

      if (denoise_darkareas < 256)                                               //  if brightness threshhold set,      19.5
      {                                                                          //    revert brighter areas
         for (py = 0; py < E3hh; py++)
         for (px = 0; px < E3ww; px++)
         {
            if (sa_stat == 3) {                                                  //  select area active
               ii = py * E3ww + px;
               dist = sa_pixmap[ii];                                             //  distance from edge
               if (! dist) continue;                                             //  outside pixel
            }

            pix3 = PXMpix(E3pxm,px,py);                                          //  source pixel
            pix9 = PXMpix(E9pxm,px,py);                                          //  target pixel

            if (pix3[0] + pix3[1] + pix3[2] > 3 * denoise_darkareas)             //  revert brighter pixels
               memcpy(pix9,pix3,pcc);
         }
      }

      PXM_free(E3pxm);                                                           //  image9 >> image3
      E3pxm = E9pxm;
      E9pxm = 0;

      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


void * denoise_wthread(void *arg)                                                //  worker thread for methods 1-5
{
   using namespace denoise_names;

   void  denoise_flatten(float *pix3, float *pix9);
   void  denoise_median(float *pix3, float *pix9);
   void  denoise_tophat(float *pix3, float *pix9);

   int         index = *((int *) arg);
   int         ii, px, py, rad, dist = 0;
   float       f1, f2;
   float       *pix1, *pix3, *pix9;

   rad = denoise_radius;

   for (py = index+rad; py < E3hh-rad; py += NWT)                                //  loop all image3 pixels
   for (px = rad; px < E3ww-rad; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      pix3 = PXMpix(E3pxm,px,py);                                                //  source pixel
      pix9 = PXMpix(E9pxm,px,py);                                                //  target pixel

      if (denoise_method == flatten) denoise_flatten(pix3,pix9);
      if (denoise_method == median) denoise_median(pix3,pix9);
      if (denoise_method == tophat) denoise_tophat(pix3,pix9);

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //  blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         pix1 = PXMpix(E1pxm,px,py);                                             //  source pixel
         pix9[0] = f1 * pix9[0] + f2 * pix1[0];
         pix9[1] = f1 * pix9[1] + f2 * pix1[1];                                  //  remove int(*)
         pix9[2] = f1 * pix9[2] + f2 * pix1[2];
      }

      Fbusy_done++;                                                              //  track progress
   }

   pthread_exit(0);
}


// ------------------------------------------------------------------------------

//  Flatten: 
//  Flatten outlyer pixels within neighborhood group.
//  An outlier pixel has an RGB value outside one sigma of
//  the mean for all pixels within a given radius.

void denoise_flatten(float *pix3, float *pix9)
{
   using namespace denoise_names;

   int         rgb, dy, dx, rad, nn;
   float       nn1, val, sum, sum2, mean, variance, sigma;
   float       *pixN;
   int         nc = E3pxm->nc;

   rad = denoise_radius;
   nn = (rad * 2 + 1);
   nn = nn * nn - 1;
   nn1 = 1.0 / nn;

   for (rgb = 0; rgb < 3; rgb++)                                                 //  loop RGB color
   {
      sum = sum2 = 0;

      for (dy = -rad; dy <= rad; dy++)                                           //  loop surrounding pixels
      for (dx = -rad; dx <= rad; dx++)
      {
         if (dy == 0 && dx == 0) continue;                                       //  skip self
         pixN = pix3 + (dy * E3ww + dx) * nc;
         val = pixN[rgb];
         sum += val;
         sum2 += val * val;
      }

      mean = nn1 * sum;
      variance = nn1 * (sum2 - 2.0 * mean * sum) + mean * mean;
      sigma = sqrtf(variance);

      if (pix3[rgb] > mean + sigma && pix3[rgb] - mean < denoise_thresh)         //  if | pixel - mean | > sigma
         pix9[rgb] = mean + 0.8 * sigma;                                         //    flatten pixel
      else if (pix3[rgb] < mean - sigma) 
         pix9[rgb] = mean - 0.8 * sigma;
   }

   return;
}


// ------------------------------------------------------------------------------

//  Median:
//  Use median RGB brightness for pixels within radius

void denoise_median(float *pix3, float *pix9)
{
   using namespace denoise_names;

   int         dy, dx, rad, ns, rgb, median;
   float       bsortN[400], *pixN;
   int         nc = E3pxm->nc;

   rad = denoise_radius;

   for (rgb = 0; rgb < 3; rgb++)                                                 //  loop all RGB colors
   {
      ns = 0;

      for (dy = -rad; dy <= rad; dy++)                                           //  loop surrounding pixels
      for (dx = -rad; dx <= rad; dx++)                                           //  get brightness values
      {
         pixN = pix3 + (dy * E3ww + dx) * nc;
         bsortN[ns] = pixN[rgb];
         ns++;
      }

      HeapSort(bsortN,ns);                                                       //  get median rgb value
      median = bsortN[ns/2];

      if (pix3[rgb] > median && pix3[rgb] < median + denoise_thresh)             //  if rgb > median and < threshold    19.0
         pix9[rgb] = median;                                                     //    moderate rgb

      else if (pix3[rgb] < median && pix3[rgb] > median - denoise_thresh)
         pix9[rgb] = 0.5 * pix9[rgb] + 0.5 * median;                             //  asymmetric                         19.0
   }

   return;
}


// ------------------------------------------------------------------------------

//  Top Hat:
//  Execute with increasing radius from 1 to limit.
//  Detect outlier by comparing with pixels along outer radius only.

void denoise_tophat(float *pix3, float *pix9)
{
   using namespace denoise_names;

   int         dy, dx, rad;
   float       minR, minG, minB, maxR, maxG, maxB;
   float       *pixN;
   int         nc = E3pxm->nc;

   for (rad = 1; rad <= denoise_radius; rad++)
   for (int loops = 0; loops < 2; loops++)
   {
      minR = minG = minB = 255;
      maxR = maxG = maxB = 0;

      for (dy = -rad; dy <= rad; dy++)                                           //  loop all pixels within rad
      for (dx = -rad; dx <= rad; dx++)
      {
         if (dx > -rad && dx < rad) continue;                                    //  skip inner pixels
         if (dy > -rad && dy < rad) continue;

         pixN = pix3 + (dy * E3ww + dx) * nc;
         if (pixN[0] < minR) minR = pixN[0];                                     //  find min and max per color
         if (pixN[0] > maxR) maxR = pixN[0];                                     //    among outermost pixels
         if (pixN[1] < minG) minG = pixN[1];
         if (pixN[1] > maxG) maxG = pixN[1];
         if (pixN[2] < minB) minB = pixN[2];
         if (pixN[2] > maxB) maxB = pixN[2];
      }

      if (pix3[0] < minR && pix9[0] < 254) pix9[0] += 2;                         //  if central pixel is outlier,
      if (pix3[0] > maxR && pix9[0] > 3)   pix9[0] -= 3;                         //    moderate its values
      if (pix3[1] < minG && pix9[1] < 254) pix9[1] += 2;
      if (pix3[1] > maxG && pix9[1] > 3)   pix9[1] -= 3;
      if (pix3[2] < minB && pix9[2] < 254) pix9[2] += 2;
      if (pix3[2] > maxB && pix9[2] > 3)   pix9[2] -= 3;
   }

   return;
}


// ------------------------------------------------------------------------------

//  Wavelet:
//  worker thread for wavelets method
//  do wavelet denoise for one color in each of 3 threads

void * denoise_wavelet_wthread(void *arg)
{
   using namespace denoise_names;

   void denoise_wavelet(float *fimg[3], uint ww2, uint hh2, float);

   int      rgb = *((int *) arg);                                                //  rgb color 0/1/2
   int      ii, jj;
   float    *fimg[3];
   float    f256 = 1.0 / 256.0;
   int      nc = E3pxm->nc;

   if (sa_stat == 3) goto denoise_area;                                          //  select area is active

   fimg[0] = (float *) zmalloc(E3ww * E3hh * sizeof(float));
   fimg[1] = (float *) zmalloc(E3ww * E3hh * sizeof(float));
   fimg[2] = (float *) zmalloc(E3ww * E3hh * sizeof(float));

   for (ii = 0; ii < E3ww * E3hh; ii++)                                          //  extract one noisy color from E3
      fimg[0][ii] = E3pxm->pixels[nc*ii+rgb] * f256;

   denoise_wavelet(fimg,E3ww,E3hh,denoise_thresh);

   for (ii = 0; ii < E3ww * E3hh; ii++)                                          //  save one denoised color to E9
      E9pxm->pixels[nc*ii+rgb] = 256.0 * fimg[0][ii];

   zfree(fimg[0]);
   zfree(fimg[1]);
   zfree(fimg[2]);

   pthread_exit(0);

denoise_area:

   int      px, py, pxl, pxh, pyl, pyh, ww2, hh2, dist;
   float    f1, f2;

   pxl = sa_minx - 16;
   if (pxl < 0) pxl = 0;
   pxh = sa_maxx + 16;
   if (pxh > E3ww) pxh = E3ww;

   pyl = sa_miny - 16;
   if (pyl < 0) pyl = 0;
   pyh = sa_maxy + 16;
   if (pyh > E3hh) pyh = E3hh;

   ww2 = pxh - pxl;
   hh2 = pyh - pyl;

   fimg[0] = (float *) zmalloc(ww2 * hh2 * sizeof(float));
   fimg[1] = (float *) zmalloc(ww2 * hh2 * sizeof(float));
   fimg[2] = (float *) zmalloc(ww2 * hh2 * sizeof(float));

   for (py = 0; py < hh2; py++)
   for (px = 0; px < ww2; px++)
   {
      ii = py * ww2 + px;
      jj = (py + pyl) * E3ww + (px + pxl);
      fimg[0][ii] = E3pxm->pixels[nc*jj+rgb] * f256;
   }

   denoise_wavelet(fimg,ww2,hh2,denoise_thresh);

   for (py = 0; py < hh2; py++)
   for (px = 0; px < ww2; px++)
   {
      ii = py * ww2 + px;
      jj = (py + pyl) * E3ww + (px + pxl);

      dist = sa_pixmap[jj];
      if (! dist) continue;

      if (dist < sa_blendwidth) {
         f1 = sa_blendfunc(dist);
         f2 = 1.0 - f1;
         E9pxm->pixels[nc*jj+rgb] = f1 * 256.0 * fimg[0][ii] + f2 * E3pxm->pixels[nc*jj+rgb];
      }
      else E9pxm->pixels[nc*jj+rgb] = 256.0 * fimg[0][ii];
   }

   zfree(fimg[0]);
   zfree(fimg[1]);
   zfree(fimg[2]);

   pthread_exit(0);
}


//  wavelet denoise algorithm
//  Adapted from Gimp wavelet plugin (and ultimately DCraw by Dave Coffin).
//  fimg[0][rows][cols] = one color of image to denoise
//  fimg[1] and [2] = working space
//  thresh (0-10) is the adjustable parameter

void denoise_wavelet(float *fimg[3], uint ww2, uint hh2, float thresh)
{
   void denoise_wavelet_avgpix(float *temp, float *fimg, int st, int size, int sc);

   float    *temp, thold, stdev[5];
   uint     ii, lev, lpass, hpass, size, col, row;
   uint     samples[5];

   size = ww2 * hh2;
   temp = (float *) zmalloc ((ww2 + hh2) * sizeof(float));
   hpass = 0;

   for (lev = 0; lev < 5; lev++)
   {
      lpass = ((lev & 1) + 1);                                                   //  1, 2, 1, 2, 1

      for (row = 0; row < hh2; row++)                                            //  average row pixels
      {
         denoise_wavelet_avgpix(temp, fimg[hpass] + row * ww2, 1, ww2, 1 << lev);

         for (col = 0; col < ww2; col++)
            fimg[lpass][row * ww2 + col] = temp[col];
      }

      for (col = 0; col < ww2; col++)                                            //  average column pixels
      {
         denoise_wavelet_avgpix(temp, fimg[lpass] + col, ww2, hh2, 1 << lev);

         for (row = 0; row < hh2; row++)
            fimg[lpass][row * ww2 + col] = temp[row];
      }

      thold = 5.0 / (1 << 6) * exp (-2.6 * sqrt (lev + 1)) * 0.8002 / exp (-2.6);

      stdev[0] = stdev[1] = stdev[2] = stdev[3] = stdev[4] = 0.0;
      samples[0] = samples[1] = samples[2] = samples[3] = samples[4] = 0;

      for (ii = 0; ii < size; ii++)
      {
         fimg[hpass][ii] -= fimg[lpass][ii];

         if (fimg[hpass][ii] < thold && fimg[hpass][ii] > -thold)
         {
            if (fimg[lpass][ii] > 0.8) {
               stdev[4] += fimg[hpass][ii] * fimg[hpass][ii];
               samples[4]++;
            }
            else if (fimg[lpass][ii] > 0.6) {
               stdev[3] += fimg[hpass][ii] * fimg[hpass][ii];
               samples[3]++;
            }
            else if (fimg[lpass][ii] > 0.4) {
               stdev[2] += fimg[hpass][ii] * fimg[hpass][ii];
               samples[2]++;
            }
            else if (fimg[lpass][ii] > 0.2) {
               stdev[1] += fimg[hpass][ii] * fimg[hpass][ii];
               samples[1]++;
            }
            else {
               stdev[0] += fimg[hpass][ii] * fimg[hpass][ii];
               samples[0]++;
            }
         }
      }

      stdev[0] = sqrt (stdev[0] / (samples[0] + 1));
      stdev[1] = sqrt (stdev[1] / (samples[1] + 1));
      stdev[2] = sqrt (stdev[2] / (samples[2] + 1));
      stdev[3] = sqrt (stdev[3] / (samples[3] + 1));
      stdev[4] = sqrt (stdev[4] / (samples[4] + 1));

      for (ii = 0; ii < size; ii++)                                              //  do thresholding
      {
         if (fimg[lpass][ii] > 0.8)
            thold = thresh * stdev[4];
         else if (fimg[lpass][ii] > 0.6)
            thold = thresh * stdev[3];
         else if (fimg[lpass][ii] > 0.4)
            thold = thresh * stdev[2];
         else if (fimg[lpass][ii] > 0.2)
            thold = thresh * stdev[1];
         else
            thold = thresh * stdev[0];

         if (fimg[hpass][ii] < -thold)
            fimg[hpass][ii] += thold;
         else if (fimg[hpass][ii] > thold)
            fimg[hpass][ii] -= thold;
         else
            fimg[hpass][ii] = 0;

         if (hpass) fimg[0][ii] += fimg[hpass][ii];
      }

      hpass = lpass;
   }

   for (ii = 0; ii < size; ii++)
      fimg[0][ii] = fimg[0][ii] + fimg[lpass][ii];

   zfree(temp);
   return;
}


//  average pixels in one column or row
//  st = row stride (row length) or column stride (1)
//  sc = 1, 2, 4, 8, 16 = pixels +/- from target pixel to average

void denoise_wavelet_avgpix(float *temp, float *fimg, int st, int size, int sc)
{
  int ii;

  for (ii = 0; ii < sc; ii++)
    temp[ii] = 0.25*(2*fimg[st*ii] + fimg[st*(sc-ii)] + fimg[st*(ii+sc)]);

  for (NOP; ii < size - sc; ii++)
    temp[ii] = 0.25*(2*fimg[st*ii] + fimg[st*(ii-sc)] + fimg[st*(ii+sc)]);

  for (NOP; ii < size; ii++)
    temp[ii] = 0.25*(2*fimg[st*ii] + fimg[st*(ii-sc)] + fimg[st*(2*size-2-(ii+sc))]);

  return;
}


// ------------------------------------------------------------------------------

//  dialog to measure noise at mouse position

void denoise_measure()
{
   using namespace denoise_names;

   int denoise_measure_dialog_event(zdialog *zd, cchar *event);

   GtkWidget   *frdraw, *drawwin;
   char        text[100];
   cchar       *title = E2X("Measure Noise");
   cchar       *mousemess = E2X("Move mouse in a monotone image area.");

/***
          _______________________________________
         |           Measure Noise               |
         |                                       |
         |  Move mouse in a monotone image area. |
         | _____________________________________ |
         ||                                     ||
         ||                                     ||
         ||.....................................||
         ||                                     ||
         ||                                     ||
         ||_____________________________________||       drawing area
         ||                                     ||
         ||                                     ||
         ||.....................................||
         ||                                     ||
         ||_____________________________________||
         | center                           edge |
         |                                       |
         |   mean RGB:   100   150   200         |
         |  mean noise:  1.51  1.23  0.76        |
         |                                       |
         |                              [cancel] |
         |_______________________________________|

***/

   if (zd_denoise_measure) return;

   zdialog *zd = zdialog_new(title,Mwin,Bcancel,null);                           //  measure noise dialog
   zd_denoise_measure = zd;

   zdialog_add_widget(zd,"label","clab","dialog",mousemess,"space=5");

   zdialog_add_widget(zd,"frame","frdraw","dialog",0,"expand");                  //  frame for drawing areas
   frdraw = zdialog_widget(zd,"frdraw");
   drawwin = gtk_drawing_area_new();                                             //  drawing area
   gtk_container_add(GTK_CONTAINER(frdraw),drawwin);
   denoise_measure_drawwin = drawwin;
   
   zdialog_add_widget(zd,"hbox","hbce","dialog");
   zdialog_add_widget(zd,"label","labcen","hbce",Bcenter,"space=3");
   zdialog_add_widget(zd,"label","space","hbce",0,"expand");
   zdialog_add_widget(zd,"label","labend","hbce",Bedge,"space=5");

   snprintf(text,100,mformat,0.0,0.0,0.0);                                       //  mean RGB:     0     0     0
   zdialog_add_widget(zd,"label","mlab","dialog",text);
   snprintf(text,100,nformat,0.0,0.0,0.0);                                       //  mean noise:  0.00  0.00  0.00
   zdialog_add_widget(zd,"label","nlab","dialog",text);

   zdialog_resize(zd,300,300);
   zdialog_run(zd,denoise_measure_dialog_event,"save");                          //  run dialog
   return;
}


//  dialog event and completion function

int denoise_measure_dialog_event(zdialog *zd, cchar *event)
{
   using namespace denoise_names;

   void denoise_measure_mousefunc();
   
   if (strmatch(event,"focus"))
      takeMouse(denoise_measure_mousefunc,dragcursor);                           //  connect mouse function
   
   if (zd->zstat) {
      freeMouse();                                                               //  free mouse
      zdialog_free(zd);
      zd_denoise_measure = 0;
   }
   
   return 1;
}


//  mouse function
//  sample noise where the mouse is clicked
//  assumed: mouse is on a monotone image area

void denoise_measure_mousefunc()
{
   using namespace denoise_names;

   GtkWidget   *drawwin;
   GdkWindow   *gdkwin;
   cairo_t     *cr;
   zdialog     *zd = zd_denoise_measure;

   char        text[100];
   int         mx, my, px, py, qx, qy, Npix;
   float       *pix3, R;
   float       Rm, Gm, Bm, Rn, Gn, Bn, Ro, Go, Bo;
   int         dww, dhh;
   float       max, xscale, yscale;
   float       rx, ry;
   float       Noise[400][3];
   double      dashes[2] = { 1, 3 };
   
   if (! E3pxm) return;
   if (! zd) return;
   if (Fthreadbusy) return;                                                      //  bugfix                             18.07

   mx = Mxposn;                                                                  //  mouse position
   my = Myposn;

   if (mx < 13 || mx >= E3ww-13) return;                                         //  must be 12+ pixels from image edge
   if (my < 13 || my >= E3hh-13) return;
   
   draw_mousecircle(Mxposn,Myposn,10,0,0);                                       //  draw mouse circle, radius 10

   Npix = 0;
   Rm = Gm = Bm = 0;
   
   for (py = my-10; py <= my+10; py++)                                           //  loop pixels within mouise circle
   for (px = mx-10; px <= mx+10; px++)                                           //  (approx. 314 pixels)
   {
      R = sqrtf((px-mx)*(px-mx) + (py-my)*(py-my));                              //  distance from center
      if (R > 10) continue;

      pix3 = PXMpix(E3pxm,px,py);                                                //  get pixel RGB values
      Rm += pix3[0];                                                             //  accumulate
      Gm += pix3[1];
      Bm += pix3[2];

      Npix++;
   }

   Rm = Rm / Npix;                                                               //  mean RGB values
   Gm = Gm / Npix;                                                               //    for pixels within mouse
   Bm = Bm / Npix;

   Npix = 0;
   Rn = Gn = Bn = 0;

   for (py = my-10; py <= my+10; py++)                                           //  loop pixels within mouise circle
   for (px = mx-10; px <= mx+10; px++)                                           //  (approx. 314 pixels)
   {
      R = sqrtf((px-mx)*(px-mx) + (py-my)*(py-my));                              //  distance from center
      if (R > 10) continue;

      Ro = Go = Bo = 0;

      for (qy = py-2; qy <= py+2; qy++)                                          //  for each pixel, get mean RGB
      for (qx = px-2; qx <= px+2; qx++)                                          //    for 5x5 surrounding pixels
      {
         pix3 = PXMpix(E3pxm,qx,qy);
         Ro += pix3[0];
         Go += pix3[1];
         Bo += pix3[2];
      }
      
      Ro = Ro / 25;                                                              //  mean RGB for surrounding pixels
      Go = Go / 25;
      Bo = Bo / 25;

      pix3 = PXMpix(E3pxm,px,py);                                                //  get pixel RGB noise levels

      Noise[Npix][0] = pix3[0] - Ro;                                             //  noise = pixel value - mean
      Noise[Npix][1] = pix3[1] - Go;
      Noise[Npix][2] = pix3[2] - Bo;
      
      Rn += fabsf(Noise[Npix][0]);                                               //  accumulate absolute values
      Gn += fabsf(Noise[Npix][1]);
      Bn += fabsf(Noise[Npix][2]);

      Npix++;
   }

   Rn = Rn / Npix;                                                               //  mean RGB noise levels 
   Gn = Gn / Npix;                                                               //    for pixels within mouse
   Bn = Bn / Npix;

   snprintf(text,100,mformat,0.0,0.0,0.0);                                       //  clear dialog data
   zdialog_stuff(zd,"mlab",text);
   snprintf(text,100,nformat,0.0,0.0,0.0);
   zdialog_stuff(zd,"nlab",text);

   snprintf(text,100,mformat,Rm,Gm,Bm);                                          //  mean RGB:   NNN   NNN   NNN
   zdialog_stuff(zd,"mlab",text);

   snprintf(text,100,nformat,Rn,Gn,Bn);                                          //  mean noise:  N.NN  N.NN  N.NN
   zdialog_stuff(zd,"nlab",text);

   max = Rn;
   if (Gn > max) max = Gn;
   if (Bn > max) max = Bn;

   drawwin = denoise_measure_drawwin;
   gdkwin = gtk_widget_get_window(drawwin);                                      //  GDK drawing window

   dww = gtk_widget_get_allocated_width(drawwin);                                //  drawing window size
   dhh = gtk_widget_get_allocated_height(drawwin);

   xscale = dww / 10.0;                                                          //  x scale:  0 to max radius
   yscale = dhh / 20.0;                                                          //  y scale: -10 to +10 noise level
   
   cr = draw_context_create(gdkwin,draw_context);
   
   cairo_set_source_rgb(cr,1,1,1);                                               //  white background
   cairo_paint(cr);
   
   cairo_set_source_rgb(cr,0,0,0);                                               //  paint black

   cairo_set_line_width(cr,2);                                                   //  center line
   cairo_set_dash(cr,dashes,0,0);
   cairo_move_to(cr,0,0.5*dhh);
   cairo_line_to(cr,dww,0.5*dhh);
   cairo_stroke(cr);
   
   cairo_set_dash(cr,dashes,2,0);                                                //  dash lines at -5 and +5
   cairo_move_to(cr,0,0.25*dhh);
   cairo_line_to(cr,dww,0.25*dhh);
   cairo_move_to(cr,0,0.75*dhh);
   cairo_line_to(cr,dww,0.75*dhh);
   cairo_stroke(cr);

   cairo_set_source_rgb(cr,1,0,0);

   Npix = 0;

   for (py = my-10; py <= my+10; py++)                                           //  loop pixels within mouise circle
   for (px = mx-10; px <= mx+10; px++)                                           //  (approx. 314 pixels)
   {
      R = sqrtf((px-mx)*(px-mx) + (py-my)*(py-my));                              //  distance from center
      if (R > 10) continue;
      Rn = Noise[Npix][0];                                                       //  RED noise
      rx = R * xscale;                                                           //  px, 0 to dww
      ry = 0.5 * dhh - Rn * yscale;                                              //  red py, 0 to dhh
      cairo_move_to(cr,rx,ry);
      cairo_arc(cr,rx,ry,1,0,2*PI);
      Npix++;
   }      

   cairo_stroke(cr);

   cairo_set_source_rgb(cr,0,1,0);

   Npix = 0;

   for (py = my-10; py <= my+10; py++)                                           //  same for GREEN noise
   for (px = mx-10; px <= mx+10; px++)
   {
      R = sqrtf((px-mx)*(px-mx) + (py-my)*(py-my));
      if (R > 10) continue;
      Gn = Noise[Npix][1];
      rx = R * xscale;
      ry = 0.5 * dhh - Gn * yscale;
      cairo_move_to(cr,rx,ry);
      cairo_arc(cr,rx,ry,1,0,2*PI);
      Npix++;
   }      

   cairo_stroke(cr);

   cairo_set_source_rgb(cr,0,0,1);

   Npix = 0;

   for (py = my-10; py <= my+10; py++)                                           //  same for BLUE noise
   for (px = mx-10; px <= mx+10; px++)
   {
      R = (px-mx)*(px-mx) + (py-my)*(py-my);
      if (R > 100) continue;
      R = 0.1 * R;
      Bn = Noise[Npix][2];
      rx = R * xscale;
      ry = 0.5 * dhh - Bn * yscale;
      cairo_move_to(cr,rx,ry);
      cairo_arc(cr,rx,ry,1,0,2*PI);
      Npix++;
   }      

   cairo_stroke(cr);

   draw_context_destroy(draw_context); 
   return;
}


/********************************************************************************/

//  red eye removal function

namespace redeye_names
{
   struct sredmem {                                                              //  red-eye struct in memory
      char        type, space[3];
      int         cx, cy, ww, hh, rad, clicks;
      float       thresh, tstep;
   };
   sredmem  redmem[100];                                                         //  store up to 100 red-eyes

   int      E3ww, E3hh;
   int      Nredmem = 0, maxredmem = 100;

   editfunc    EFredeye;

   #define pixred(pix) (25*pix[0]/(pixbright(pix)+1))                            //  red brightness 0-100%
}


//  menu function

void m_redeyes(GtkWidget *, cchar *)
{
   using namespace redeye_names;

   int      redeye_dialog_event(zdialog *zd, cchar *event);
   void     redeye_mousefunc();

   cchar    *redeye_message = E2X(
               "Method 1:\n"
               "  Left-click on red-eye to darken.\n"
               "Method 2:\n"
               "  Drag down and right to enclose red-eye.\n"
               "  Left-click on red-eye to darken.\n"
               "Undo red-eye:\n"
               "  Right-click on red-eye.");

   F1_help_topic = "redeyes";

   EFredeye.menufunc = m_redeyes;
   EFredeye.funcname = "redeyes";
   EFredeye.Farea = 1;                                                           //  select area ignored
   EFredeye.mousefunc = redeye_mousefunc;
   if (! edit_setup(EFredeye)) return;                                           //  setup edit
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

   zdialog *zd = zdialog_new(E2X("Red Eye Reduction"),Mwin,Bdone,Bcancel,null);
   EFredeye.zd = zd;

   zdialog_add_widget(zd,"label","lab1","dialog",redeye_message);
   zdialog_run(zd,redeye_dialog_event,"save");                                   //  run dialog - parallel

   Nredmem = 0;
   takeMouse(redeye_mousefunc,dragcursor);                                       //  connect mouse function
   return;
}


//  dialog event and completion callback function

int redeye_dialog_event(zdialog *zd, cchar *event)
{
   using namespace redeye_names;

   void     redeye_mousefunc();

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()
   
   if (zd->zstat)
   {
      if (Nredmem > 0) {
         CEF->Fmods++;
         CEF->Fsaved = 0;
      }
      if (zd->zstat == 1) edit_done(0);                                          //  commit edit
      else edit_cancel(0);                                                       //  discard edit
      return 1;
   }

   if (strmatch(event,"focus"))                                                  //  toggle mouse capture
      takeMouse(redeye_mousefunc,dragcursor);                                    //  connect mouse function

   return 1;
}


int      redeye_createF(int px, int py);                                         //  create 1-click red-eye (type F)
int      redeye_createR(int px, int py, int ww, int hh);                         //  create robust red-eye (type R)
void     redeye_darken(int ii);                                                  //  darken red-eye
void     redeye_distr(int ii);                                                   //  build pixel redness distribution
int      redeye_find(int px, int py);                                            //  find red-eye at mouse position
void     redeye_remove(int ii);                                                  //  remove red-eye at mouse position
int      redeye_radlim(int cx, int cy);                                          //  compute red-eye radius limit

void redeye_mousefunc()
{
   using namespace redeye_names;

   int         ii, px, py, ww, hh;

   if (Nredmem == maxredmem) {
      zmessageACK(Mwin,"%d red-eye limit reached",maxredmem);                    //  too many red-eyes
      return;
   }

   if (LMclick)                                                                  //  left mouse click
   {
      px = Mxclick;                                                              //  click position
      py = Myclick;
      if (px < 0 || px > E3ww-1 || py < 0 || py > E3hh-1)                        //  outside image area
         return;

      ii = redeye_find(px,py);                                                   //  find existing red-eye
      if (ii < 0) ii = redeye_createF(px,py);                                    //  or create new type F
      redeye_darken(ii);                                                         //  darken red-eye
      Fpaint2();
   }

   if (RMclick)                                                                  //  right mouse click
   {
      px = Mxclick;                                                              //  click position
      py = Myclick;
      ii = redeye_find(px,py);                                                   //  find red-eye
      if (ii >= 0) redeye_remove(ii);                                            //  if found, remove
      Fpaint2();
   }

   LMclick = RMclick = 0;

   if (Mxdrag || Mydrag)                                                         //  mouse drag underway
   {
      px = Mxdown;                                                               //  initial position
      py = Mydown;
      ww = Mxdrag - Mxdown;                                                      //  increment
      hh = Mydrag - Mydown;
      Mxdrag = Mydrag = 0;
      if (ww < 2 && hh < 2) return;
      if (ww < 2) ww = 2;
      if (hh < 2) hh = 2;
      if (px < 1) px = 1;                                                        //  keep within image area
      if (py < 1) py = 1;
      if (px + ww > E3ww-1) ww = E3ww-1 - px;
      if (py + hh > E3hh-1) hh = E3hh-1 - py;
      ii = redeye_find(px,py);                                                   //  find existing red-eye
      if (ii >= 0) redeye_remove(ii);                                            //  remove it
      ii = redeye_createR(px,py,ww,hh);                                          //  create new red-eye type R
   }

   return;
}


//  create type F redeye (1-click automatic)

int redeye_createF(int cx, int cy)
{
   using namespace redeye_names;

   int         cx0, cy0, cx1, cy1, px, py, rad, radlim;
   int         loops, ii;
   int         Tnpix, Rnpix, R2npix;
   float       rd, rcx, rcy, redpart;
   float       Tsum, Rsum, R2sum, Tavg, Ravg, R2avg;
   float       sumx, sumy, sumr;
   float       *ppix;

   cx0 = cx;
   cy0 = cy;

   for (loops = 0; loops < 8; loops++)
   {
      cx1 = cx;
      cy1 = cy;

      radlim = redeye_radlim(cx,cy);                                             //  radius limit (image edge)
      Tsum = Tavg = Ravg = Tnpix = 0;

      for (rad = 0; rad < radlim-2; rad++)                                       //  find red-eye radius from (cx,cy)
      {
         Rsum = Rnpix = 0;
         R2sum = R2npix = 0;

         for (py = cy-rad-2; py <= cy+rad+2; py++)
         for (px = cx-rad-2; px <= cx+rad+2; px++)
         {
            rd = sqrt((px-cx)*(px-cx) + (py-cy)*(py-cy));
            ppix = PXMpix(E3pxm,px,py);
            redpart = pixred(ppix);

            if (rd <= rad + 0.5 && rd > rad - 0.5) {                             //  accum. redness at rad
               Rsum += redpart;
               Rnpix++;
            }
            else if (rd <= rad + 2.5 && rd > rad + 1.5) {                        //  accum. redness at rad+2
               R2sum += redpart;
               R2npix++;
            }
         }

         Tsum += Rsum;
         Tnpix += Rnpix;
         Tavg = Tsum / Tnpix;                                                    //  avg. redness over 0-rad
         Ravg = Rsum / Rnpix;                                                    //  avg. redness at rad
         R2avg = R2sum / R2npix;                                                 //  avg. redness at rad+2
         if (R2avg > Ravg || Ravg > Tavg) continue;
         if ((Ravg - R2avg) < 0.2 * (Tavg - Ravg)) break;                        //  0.1 --> 0.2
      }

      sumx = sumy = sumr = 0;
      rad = int(1.2 * rad + 1);
      if (rad > radlim) rad = radlim;

      for (py = cy-rad; py <= cy+rad; py++)                                      //  compute center of gravity for
      for (px = cx-rad; px <= cx+rad; px++)                                      //   pixels within rad of (cx,cy)
      {
         rd = sqrt((px-cx)*(px-cx) + (py-cy)*(py-cy));
         if (rd > rad + 0.5) continue;
         ppix = PXMpix(E3pxm,px,py);
         redpart = pixred(ppix);                                                 //  weight by redness
         sumx += redpart * (px - cx);
         sumy += redpart * (py - cy);
         sumr += redpart;
      }

      rcx = cx + 1.0 * sumx / sumr;                                              //  new center of red-eye
      rcy = cy + 1.0 * sumy / sumr;
      if (fabsf(cx0 - rcx) > 0.6 * rad) break;                                   //  give up if big movement
      if (fabsf(cy0 - rcy) > 0.6 * rad) break;
      cx = int(rcx + 0.5);
      cy = int(rcy + 0.5);
      if (cx == cx1 && cy == cy1) break;                                         //  done if no change
   }

   radlim = redeye_radlim(cx,cy);
   if (rad > radlim) rad = radlim;

   ii = Nredmem++;                                                               //  add red-eye to memory
   redmem[ii].type = 'F';
   redmem[ii].cx = cx;
   redmem[ii].cy = cy;
   redmem[ii].rad = rad;
   redmem[ii].clicks = 0;
   redmem[ii].thresh = 0;
   return ii;
}


//  create type R red-eye (drag an ellipse over red-eye area)

int redeye_createR(int cx, int cy, int ww, int hh)
{
   using namespace redeye_names;

   int      rad, radlim;

   draw_mousearc(cx,cy,2*ww,2*hh,0,0);                                           //  draw ellipse around mouse pointer

   if (ww > hh) rad = ww;
   else rad = hh;
   radlim = redeye_radlim(cx,cy);
   if (rad > radlim) rad = radlim;

   int ii = Nredmem++;                                                           //  add red-eye to memory
   redmem[ii].type = 'R';
   redmem[ii].cx = cx;
   redmem[ii].cy = cy;
   redmem[ii].ww = 2 * ww;
   redmem[ii].hh = 2 * hh;
   redmem[ii].rad = rad;
   redmem[ii].clicks = 0;
   redmem[ii].thresh = 0;
   return ii;
}


//  darken a red-eye and increase click count

void redeye_darken(int ii)
{
   using namespace redeye_names;

   int         cx, cy, ww, hh, px, py, rad, clicks;
   float       rd, thresh, tstep;
   char        type;
   float       *ppix;

   type = redmem[ii].type;
   cx = redmem[ii].cx;
   cy = redmem[ii].cy;
   ww = redmem[ii].ww;
   hh = redmem[ii].hh;
   rad = redmem[ii].rad;
   thresh = redmem[ii].thresh;
   tstep = redmem[ii].tstep;
   clicks = redmem[ii].clicks++;

   if (thresh == 0)                                                              //  1st click
   {
      redeye_distr(ii);                                                          //  get pixel redness distribution
      thresh = redmem[ii].thresh;                                                //  initial redness threshhold
      tstep = redmem[ii].tstep;                                                  //  redness step size
      draw_mousearc(0,0,0,0,1,0);                                                //  erase mouse ellipse
   }

   tstep = (thresh - tstep) / thresh;                                            //  convert to reduction factor
   thresh = thresh * pow(tstep,clicks);                                          //  reduce threshhold by total clicks

   for (py = cy-rad; py <= cy+rad; py++)                                         //  darken pixels over threshhold
   for (px = cx-rad; px <= cx+rad; px++)
   {
      if (type == 'R') {
         if (px < cx - ww/2) continue;
         if (px > cx + ww/2) continue;
         if (py < cy - hh/2) continue;
         if (py > cy + hh/2) continue;
      }
      rd = sqrt((px-cx)*(px-cx) + (py-cy)*(py-cy));
      if (rd > rad + 0.5) continue;
      ppix = PXMpix(E3pxm,px,py);                                                //  set redness = threshhold
      if (pixred(ppix) > thresh)
         ppix[0] = int(thresh * (0.65 * ppix[1] + 0.10 * ppix[2] + 1) / (25 - 0.25 * thresh));
   }

   return;
}


//  Build a distribution of redness for a red-eye. Use this information
//  to set initial threshhold and step size for stepwise darkening.

void redeye_distr(int ii)
{
   using namespace redeye_names;

   int         cx, cy, ww, hh, rad, px, py;
   int         bin, npix, dbins[20], bsum, blim;
   float       rd, maxred, minred, redpart, dbase, dstep;
   char        type;
   float       *ppix;

   type = redmem[ii].type;
   cx = redmem[ii].cx;
   cy = redmem[ii].cy;
   ww = redmem[ii].ww;
   hh = redmem[ii].hh;
   rad = redmem[ii].rad;

   maxred = 0;
   minred = 100;

   for (py = cy-rad; py <= cy+rad; py++)
   for (px = cx-rad; px <= cx+rad; px++)
   {
      if (type == 'R') {
         if (px < cx - ww/2) continue;
         if (px > cx + ww/2) continue;
         if (py < cy - hh/2) continue;
         if (py > cy + hh/2) continue;
      }
      rd = sqrt((px-cx)*(px-cx) + (py-cy)*(py-cy));
      if (rd > rad + 0.5) continue;
      ppix = PXMpix(E3pxm,px,py);
      redpart = pixred(ppix);
      if (redpart > maxred) maxred = redpart;
      if (redpart < minred) minred = redpart;
   }

   dbase = minred;
   dstep = (maxred - minred) / 19.99;

   for (bin = 0; bin < 20; bin++) dbins[bin] = 0;
   npix = 0;

   for (py = cy-rad; py <= cy+rad; py++)
   for (px = cx-rad; px <= cx+rad; px++)
   {
      if (type == 'R') {
         if (px < cx - ww/2) continue;
         if (px > cx + ww/2) continue;
         if (py < cy - hh/2) continue;
         if (py > cy + hh/2) continue;
      }
      rd = sqrt((px-cx)*(px-cx) + (py-cy)*(py-cy));
      if (rd > rad + 0.5) continue;
      ppix = PXMpix(E3pxm,px,py);
      redpart = pixred(ppix);
      bin = int((redpart - dbase) / dstep);
      ++dbins[bin];
      ++npix;
   }

   bsum = 0;
   blim = int(0.5 * npix);

   for (bin = 0; bin < 20; bin++)                                                //  find redness level for 50% of
   {                                                                             //    pixels within red-eye radius
      bsum += dbins[bin];
      if (bsum > blim) break;
   }

   redmem[ii].thresh = dbase + dstep * bin;                                      //  initial redness threshhold
   redmem[ii].tstep = dstep;                                                     //  redness step (5% of range)

   return;
}


//  find a red-eye (nearly) overlapping the mouse click position

int redeye_find(int cx, int cy)
{
   using namespace redeye_names;

   for (int ii = 0; ii < Nredmem; ii++)
   {
      if (cx > redmem[ii].cx - 2 * redmem[ii].rad &&
          cx < redmem[ii].cx + 2 * redmem[ii].rad &&
          cy > redmem[ii].cy - 2 * redmem[ii].rad &&
          cy < redmem[ii].cy + 2 * redmem[ii].rad)
            return ii;                                                           //  found
   }
   return -1;                                                                    //  not found
}


//  remove a red-eye from memory

void redeye_remove(int ii)
{
   using namespace redeye_names;

   int      cx, cy, rad, px, py;
   float    *pix1, *pix3;
   int      nc = E1pxm->nc, pcc = nc * sizeof(float);

   cx = redmem[ii].cx;
   cy = redmem[ii].cy;
   rad = redmem[ii].rad;

   for (px = cx-rad; px <= cx+rad; px++)
   for (py = cy-rad; py <= cy+rad; py++)
   {
      pix1 = PXMpix(E1pxm,px,py);
      pix3 = PXMpix(E3pxm,px,py);
      memcpy(pix3,pix1,pcc);
   }

   for (ii++; ii < Nredmem; ii++)
      redmem[ii-1] = redmem[ii];
   Nredmem--;

   draw_mousearc(0,0,0,0,1,0);                                                   //  erase mouse ellipse
   return;
}


//  compute red-eye radius limit: smaller of 100 and nearest image edge

int redeye_radlim(int cx, int cy)
{
   using namespace redeye_names;

   int radlim = 100;
   if (cx < 100) radlim = cx;
   if (E3ww-1 - cx < 100) radlim = E3ww-1 - cx;
   if (cy < 100) radlim = cy;
   if (E3hh-1 - cy < 100) radlim = E3hh-1 - cy;
   return radlim;
}


/********************************************************************************/

//  make a black & white or color positive or negative, or sepia image

namespace colormode_names
{
   editfunc    EFcolormode;
   int         mode;
   float       blend;
}


//  menu function

void m_color_mode(GtkWidget *, cchar *menu)
{
   using namespace colormode_names;

   int    colormode_dialog_event(zdialog *zd, cchar *event);
   void * colormode_thread(void *);

   F1_help_topic = "color_mode";

   EFcolormode.menuname = menu;
   EFcolormode.menufunc = m_color_mode;
   EFcolormode.funcname = "color_mode";
   EFcolormode.threadfunc = colormode_thread;
   EFcolormode.FprevReq = 1;                                                     //  use preview                        19.5
   EFcolormode.Farea = 2;                                                        //  select area usable
   EFcolormode.Frestart = 1;                                                     //  allow restart
   EFcolormode.Fscript = 1;                                                      //  scripting supported

   if (! edit_setup(EFcolormode)) return;                                        //  setup edit: no preview
   
/***
          _____________________________
         |        Color Mode           |
         |                             |
         | [_] reset                   |                                         //  check boxes                        18.07
         | [_] black/white positive    |
         | [_] black/white negative    |
         | [_] color negative          |
         | [_] RGB -> GBR              |
         | [_] RGB -> BRG              |
         | [_] sepia                   |
         |                             |
         |  0% ===========[]==== 100%  |
         |                             |
         |             [Done] [Cancel] |
         |_____________________________|

***/

   zdialog *zd = zdialog_new(E2X("Color Mode"),Mwin,Bdone,Bcancel,null);
   EFcolormode.zd = zd;

   zdialog_add_widget(zd,"check","reset","dialog",E2X("reset"));
   zdialog_add_widget(zd,"check","b&wpos","dialog",E2X("black/white positive"));
   zdialog_add_widget(zd,"check","b&wneg","dialog",E2X("black/white negative"));
   zdialog_add_widget(zd,"check","colneg","dialog",E2X("color negative"));
   zdialog_add_widget(zd,"check","rgb-gbr","dialog",E2X("RGB -> GBR"));          //                                     19.5
   zdialog_add_widget(zd,"check","rgb-brg","dialog",E2X("RGB -> BRG"));
   zdialog_add_widget(zd,"check","sepia","dialog",E2X("sepia"));
   zdialog_add_widget(zd,"hbox","hbblend","dialog");
   zdialog_add_widget(zd,"label","lab0","hbblend","0%","space=5");
   zdialog_add_widget(zd,"hscale","blend","hbblend","0.0|1.0|0.01|1.0","expand");
   zdialog_add_widget(zd,"label","lab100","hbblend","100%","space=5");

   zdialog_resize(zd,200,0);
   zdialog_run(zd,colormode_dialog_event,"save");                                //  run dialog - parallel

   return;
}


//  dialog event and completion callback function

int colormode_dialog_event(zdialog *zd, cchar *event)
{
   using namespace colormode_names;
   
   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()

   if (strmatch(event,"fullsize")) {                                             //  from select area                   19.5
      edit_fullsize();                                                           //  get full size image
      signal_thread();
      return 1;
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1) {
         edit_fullsize();                                                        //  get full size image                19.5
         signal_thread();
         edit_done(0);                                                           //  commit edit
      }
      else edit_cancel(0);                                                       //  discard edit
      return 1;
   }
   
   if (strmatch(event,"focus")) return 1;
   
   if (strstr("reset b&wpos b&wneg colneg rgb-gbr rgb-brg sepia",event))
   {
      zdialog_stuff(zd,"reset",0);                                               //  18.07
      zdialog_stuff(zd,"b&wpos",0);
      zdialog_stuff(zd,"b&wneg",0);
      zdialog_stuff(zd,"colneg",0);
      zdialog_stuff(zd,"rgb-gbr",0);                                             //  19.5
      zdialog_stuff(zd,"rgb-brg",0);
      zdialog_stuff(zd,"sepia",0);
      zdialog_stuff(zd,event,1);
      if (strmatch(event,"reset")) mode = 0;
      if (strmatch(event,"b&wpos")) mode = 1;
      if (strmatch(event,"b&wneg")) mode = 2;
      if (strmatch(event,"colneg")) mode = 3;
      if (strmatch(event,"rgb-gbr")) mode = 4;
      if (strmatch(event,"rgb-brg")) mode = 5;
      if (strmatch(event,"sepia")) mode = 6;
   }
   
   zdialog_fetch(zd,"blend",blend);
   
   if (mode == 0) {
      edit_reset();
      return 1;
   }

   paintlock(1);                                                                 //  block window paint                 19.5

   signal_thread();
   wait_thread_idle();

   paintlock(0);                                                                 //  unblock window paint               19.5
   Fpaint2();                                                                    //  update window

   return 1;
}


//  thread function

void * colormode_thread(void *)
{
   using namespace colormode_names;

   void * colormode_wthread(void *arg);
   
   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      do_wthreads(colormode_wthread,NWT);                                        //  worker threads

      CEF->Fmods++;                                                              //  image modified
      CEF->Fsaved = 0;                                                           //  not saved
   }

   return 0;                                                                     //  not executed, stop warning
}


//  worker thread functions

void * colormode_wthread(void *arg)
{
   using namespace colormode_names;

   int         index = *((int *) (arg));
   int         E3ww, E3hh;
   int         ii, dist, px, py;
   float       red1, green1, blue1;
   float       red3, green3, blue3;
   float       *pix1, *pix3;
   float       brite, ff1, ff2;
   
   E3ww = E3pxm->ww;                                                             //  19.5
   E3hh = E3pxm->hh;

   dist = 0;                                                                     //  stop compiler warnings

   for (py = index; py < E3hh; py += NWT)
   for (px = 0; px < E3ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      red1 = pix1[0];
      green1 = pix1[1];
      blue1 = pix1[2];

      red3 = pix3[0];
      green3 = pix3[1];
      blue3 = pix3[2];

      switch (mode)                                                              //  bugfix: use E1 only as input       19.6
      {
         case 1: {                                                               //  black and white positive
            red3 = green3 = blue3 = 0.333 * (red1 + green1 + blue1);
            break;
         }

         case 2: {                                                               //  black and white negative
            red3 = green3 = blue3 = 255.9 - 0.333 * (red1 + green1 + blue1);
            break;
         }

         case 3: {                                                               //  color negative
            red3 = 255.9 - red1;
            green3 = 255.9 - green1;
            blue3 = 255.9 - blue1;
            break;
         }
         
         case 4: {                                                               //  RGB - GBR
            red3 = green1;
            green3 = blue1;
            blue3 = red1;
            break;
         }

         case 5: {                                                               //  RGB - BRG                          19.5
            red3 = blue1;
            green3 = red1;
            blue3 = green1;
            break;
         }

         case 6: {                                                               //  sepia
            brite = red1;
            if (green1 > brite) brite = green1;                                  //  max. color level
            if (blue1 > brite) brite = blue1;
            brite = 0.2 * brite + 0.2666 * (red1 + green1 + blue1);              //  brightness, 0.0 ... 255.9
            brite = brite * 0.003906;                                            //              0.0 ... 1.0

            ff1 = 1.0 - 0.7 * brite;                                             //  sepia part, 1.0 ... 0.3            19.0
            ff2 = 1.0 - ff1;                                                     //  B & W part, 0.0 ... 0.7

            red3 = ff1 * 255.0 + ff2 * 256;                                      //  combine max. sepia with white
            green3 = ff1 * 150.0 + ff2 * 256;
            blue3 = ff1 * 46.0 + ff2 * 256;

            brite = 0.8333 * (brite + 0.2);                                      //  add brightness at low end
            red3 = red3 * brite;                                                 //  output = combined color * brightness
            green3 = green3 * brite;
            blue3 = blue3 * brite;
            break;
         }
      }
      
      if (red3 < 0) red3 = 0;                                                    //  prevent underflow/overlfow
      if (red3 > 255.9) red3 = 255.9;
      if (green3 < 0) green3 = 0;
      if (green3 > 255.9) green3 = 255.9;
      if (blue3 < 0) blue3 = 0;
      if (blue3 > 255.9) blue3 = 255.9;
      
      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  blend changes over blendwidth
         ff1 = sa_blendfunc(dist);
         ff2 = 1.0 - ff1;
         red3 = ff1 * red3 + ff2 * red1;
         green3 = ff1 * green3 + ff2 * green1;
         blue3 = ff1 * blue3 + ff2 * blue1;
      }
      
      if (blend < 1.0) {                                                         //  blend slider
         ff1 = blend;
         ff2 = 1.0 - ff1;
         red3 = ff1 * red3 + ff2 * red1;
         green3 = ff1 * green3 + ff2 * green1;
         blue3 = ff1 * blue3 + ff2 * blue1;
      }

      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;
   }
   
   CEF->Fmods++;
   CEF->Fsaved = 0;

   pthread_exit(0);
}

/********************************************************************************/

//  image color saturation adjustment

namespace colorsat_names
{
   float       colorsat;
   editfunc    EFcolorsat;
   int         E3ww, E3hh;
}


//  menu function

void m_color_sat(GtkWidget *, cchar *menu)
{
   using namespace colorsat_names;

   int    colorsat_dialog_event(zdialog *zd, cchar *event);
   void * colorsat_thread(void *);

   cchar  *colsatmess = E2X("Color Saturation");

   F1_help_topic = "color_sat";

   EFcolorsat.menuname = menu;
   EFcolorsat.menufunc = m_color_sat;
   EFcolorsat.funcname = "color_saturation";
   EFcolorsat.FprevReq = 1;                                                      //  use preview
   EFcolorsat.Farea = 2;                                                         //  select area usable
   EFcolorsat.FusePL = 1;                                                        //  use with paint/lever edits OK
   EFcolorsat.Fscript = 1;                                                       //  scripting supported
   EFcolorsat.threadfunc = colorsat_thread;                                      //  thread function
   if (! edit_setup(EFcolorsat)) return;                                         //  setup edit
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

/***
          __________________________
         |  Color Saturation        |
         |                          |
         |  ===========[]=========  |
         |                          |
         |          [Done] [Cancel] |
         |__________________________|

***/

   zdialog *zd = zdialog_new(colsatmess,Mwin,Bdone,Bcancel,null);
   EFcolorsat.zd = zd;
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","lab1","hb1",colsatmess,"space=3");
   zdialog_add_widget(zd,"hbox","hb2","dialog",0,"space=5");
   zdialog_add_widget(zd,"hscale","colorsat","hb2","-1.0|1.0|0.01|0","expand|space=5");

   colorsat = 1.0;

   zdialog_resize(zd,250,0);
   zdialog_run(zd,colorsat_dialog_event,"save");                                 //  run dialog, parallel

   return;
}


//  dialog event and completion callback function

int colorsat_dialog_event(zdialog *zd, cchar *event)
{
   using namespace colorsat_names;

   if (strmatch(event,"focus")) return 1;
   if (strmatch(event,"apply")) event = "colorsat";                              //  from script
   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()

   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();
      E3ww = E3pxm->ww;
      E3hh = E3pxm->hh;
      signal_thread();
      return 1;
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  done
         edit_fullsize();                                                        //  get full size image
         E3ww = E3pxm->ww;
         E3hh = E3pxm->hh;
         signal_thread();
         edit_done(0);                                                           //  commit edit
      }
      else edit_cancel(0);                                                       //  discard edit
      return 1;
   }

   if (strmatch(event,"undo")) edit_undo();
   if (strmatch(event,"redo")) edit_redo();
   if (strmatch(event,"blendwidth")) signal_thread();

   if (strmatch(event,"colorsat")) {
      zdialog_fetch(zd,"colorsat",colorsat);
      signal_thread();
   }

   return 0;
}


//  image color saturation thread function

void * colorsat_thread(void *)
{
   using namespace colorsat_names;

   int         ii, px, py, dist = 0;
   float       *pix1, *pix3;
   float       red1, green1, blue1;
   float       red3, green3, blue3;
   float       pixbrite, maxRGB, F1, F2;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      for (py = 0; py < E3hh; py++)
      for (px = 0; px < E3ww; px++)
      {
         if (sa_stat == 3) {                                                     //  select area active
            ii = py * E3ww + px;
            dist = sa_pixmap[ii];                                                //  distance from edge
            if (! dist) continue;                                                //  outside pixel
         }

         pix1 = PXMpix(E1pxm,px,py);                                             //  input pixel
         pix3 = PXMpix(E3pxm,px,py);                                             //  output pixel
         
         red1 = pix1[0];
         green1 = pix1[1];
         blue1 = pix1[2];

         pixbrite = 0.333 * (red1 + green1 + blue1);                             //  pixel brightness, 0 to 255.9
         red3 = red1 + colorsat * (red1 - pixbrite);                             //  colorsat: -1 ... +1
         green3 = green1 + colorsat * (green1 - pixbrite);
         blue3 = blue1 + colorsat * (blue1 - pixbrite);

         if (red3 < 0) red3 = 0;                                                 //  stop underflow
         if (green3 < 0) green3 = 0;
         if (blue3 < 0) blue3 = 0;

         maxRGB = red3;                                                          //  stop overflow
         if (green3 > maxRGB) maxRGB = green3;
         if (blue3 > maxRGB) maxRGB = blue3;
         if (maxRGB > 255.9) {
            red3 = red3 * 255.9 / maxRGB;
            green3 = green3 * 255.9 / maxRGB;
            blue3 = blue3 * 255.9 / maxRGB;
         }

         if (sa_stat == 3 && dist < sa_blendwidth) {                             //  select area edge blending
            F2 = sa_blendfunc(dist);
            F1 = 1.0 - F2;
            red3 = F2 * red3 + F1 * red1;
            green3 = F2 * green3 + F1 * green1;
            blue3 = F2 * blue3 + F1 * blue1;
         }

         pix3[0] = red3;
         pix3[1] = green3;
         pix3[2] = blue3;
      }

      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


/********************************************************************************/

//  Adjust RGB menu function
//  Adjust Brightness, contrast, and color levels using RGB or CMY colors

namespace adjust_RGB_names
{
   editfunc    EF_RGB;                                                           //  edit function data
   float       RGB_inputs[8];
   int         E3ww, E3hh;
}


//  menu function

void m_adjust_RGB(GtkWidget *, cchar *menu)
{
   using namespace adjust_RGB_names;

   int    RGB_dialog_event(zdialog *zd, cchar *event);
   void * RGB_thread(void *);

   F1_help_topic = "adjust_RGB";

   EF_RGB.menuname = menu;
   EF_RGB.menufunc = m_adjust_RGB;
   EF_RGB.funcname = "adjust_RGB";                                               //  function name
   EF_RGB.FprevReq = 1;                                                          //  use preview
   EF_RGB.Farea = 2;                                                             //  select area usable
   EF_RGB.Frestart = 1;                                                          //  allow restart
   EF_RGB.Fscript = 1;                                                           //  scripting supported
   EF_RGB.FusePL = 1;                                                            //  paint/lever edits supported     18.07
   EF_RGB.threadfunc = RGB_thread;                                               //  thread function
   if (! edit_setup(EF_RGB)) return;                                             //  setup edit
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

/***
    ________________________________
   |                                |
   |   +Brightness    =====[]=====  |
   |    +Red -Cyan    =====[]=====  |
   | +Green -Magenta  =====[]=====  |
   |   +Blue -Yellow  =====[]=====  |
   |                                |
   |     Contrast     =====[]=====  |
   |       Red        =====[]=====  |
   |      Green       =====[]=====  |
   |       Blue       =====[]=====  |
   |                                |
   |        [reset] [done] [cancel] |
   |________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Adjust RGB"),Mwin,Breset,Bdone,Bcancel,null); 
   EF_RGB.zd = zd;

   zdialog_add_widget(zd,"hbox","hb2","dialog");
   zdialog_add_widget(zd,"vbox","vb1","hb2",0,"homog");
   zdialog_add_widget(zd,"vbox","vb2","hb2",0,"homog|expand");
   zdialog_add_widget(zd,"label","labBriteDens","vb1",E2X("+Brightness"));
   zdialog_add_widget(zd,"label","labRedDens","vb1",E2X("+Red -Cyan"));
   zdialog_add_widget(zd,"label","labGreenDens","vb1",E2X("+Green -Magenta"));
   zdialog_add_widget(zd,"label","labBlueDens","vb1",E2X("+Blue -Yellow"));
   zdialog_add_widget(zd,"hsep","sep1","vb1");
   zdialog_add_widget(zd,"label","labContrast","vb1","Contrast All");
   zdialog_add_widget(zd,"label","labRedCon","vb1",E2X("Contrast Red"));
   zdialog_add_widget(zd,"label","labGreenCon","vb1",E2X("Contrast Green"));
   zdialog_add_widget(zd,"label","labBlueCon","vb1",E2X("Contrast Blue"));
   zdialog_add_widget(zd,"hscale","BriteDens","vb2","-1|+1|0.001|0","expand");
   zdialog_add_widget(zd,"hscale","RedDens","vb2","-1|+1|0.001|0","expand");
   zdialog_add_widget(zd,"hscale","GreenDens","vb2","-1|+1|0.001|0","expand");
   zdialog_add_widget(zd,"hscale","BlueDens","vb2","-1|+1|0.001|0","expand");
   zdialog_add_widget(zd,"hsep","sep2","vb2");
   zdialog_add_widget(zd,"hscale","Contrast","vb2","-1|+1|0.001|0","expand");
   zdialog_add_widget(zd,"hscale","RedCon","vb2","-1|+1|0.001|0","expand");
   zdialog_add_widget(zd,"hscale","GreenCon","vb2","-1|+1|0.001|0","expand");
   zdialog_add_widget(zd,"hscale","BlueCon","vb2","-1|+1|0.001|0","expand");

   zdialog_rescale(zd,"BriteDens",-1,0,+1);
   zdialog_rescale(zd,"RedDens",-1,0,+1);
   zdialog_rescale(zd,"GreenDens",-1,0,+1);
   zdialog_rescale(zd,"BlueDens",-1,0,+1);
   zdialog_rescale(zd,"Contrast",-1,0,+1);
   zdialog_rescale(zd,"RedCon",-1,0,+1);
   zdialog_rescale(zd,"GreenCon",-1,0,+1);
   zdialog_rescale(zd,"BlueCon",-1,0,+1);

   zdialog_resize(zd,300,0);
   zdialog_restore_inputs(zd);                                                   //  restore prior inputs
   zdialog_run(zd,RGB_dialog_event,"save");                                      //  run dialog - parallel
   
   zdialog_send_event(zd,"apply");
   return;
}


//  RGB dialog event and completion function

int RGB_dialog_event(zdialog *zd, cchar *event)                                  //  RGB dialog event function
{
   using namespace adjust_RGB_names;

   int      mod = 0;

   if (strmatch(event,"done")) zd->zstat = 2;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  from f_open()
   
   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();
      E3ww = E3pxm->ww;
      E3hh = E3pxm->hh;
      signal_thread();
      return 1;
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  reset
         zd->zstat = 0;                                                          //  keep dialog active
         RGB_inputs[0] = 0;
         RGB_inputs[1] = 0;
         RGB_inputs[2] = 0;
         RGB_inputs[3] = 0;
         RGB_inputs[4] = 0;
         RGB_inputs[5] = 0;
         RGB_inputs[6] = 0;
         RGB_inputs[7] = 0;
         zdialog_stuff(zd,"BriteDens",0);
         zdialog_stuff(zd,"RedDens",0);
         zdialog_stuff(zd,"GreenDens",0);
         zdialog_stuff(zd,"BlueDens",0);
         zdialog_stuff(zd,"Contrast",0);
         zdialog_stuff(zd,"RedCon",0);
         zdialog_stuff(zd,"GreenCon",0);
         zdialog_stuff(zd,"BlueCon",0);
         edit_reset();
         return 1;                                                               //  19.0
      }
      else if (zd->zstat == 2) {                                                 //  done
         edit_fullsize();                                                        //  get full size image
         E3ww = E3pxm->ww;
         E3hh = E3pxm->hh;
         signal_thread();
         edit_done(0);                                                           //  commit edit
         return 1;
      }
      else {
         edit_cancel(0);                                                         //  discard edit
         return 1;
      }
   }

   if (strmatch("focus",event)) return 1;
   
   zdialog_fetch(zd,"BriteDens",RGB_inputs[0]);                                  //  get all inputs
   zdialog_fetch(zd,"RedDens",RGB_inputs[1]);
   zdialog_fetch(zd,"GreenDens",RGB_inputs[2]);
   zdialog_fetch(zd,"BlueDens",RGB_inputs[3]);
   zdialog_fetch(zd,"Contrast",RGB_inputs[4]);
   zdialog_fetch(zd,"RedCon",RGB_inputs[5]);
   zdialog_fetch(zd,"GreenCon",RGB_inputs[6]);
   zdialog_fetch(zd,"BlueCon",RGB_inputs[7]);

   if (RGB_inputs[0]) mod++;
   if (RGB_inputs[1]) mod++;
   if (RGB_inputs[2]) mod++;
   if (RGB_inputs[3]) mod++;
   if (RGB_inputs[4]) mod++;
   if (RGB_inputs[5]) mod++;
   if (RGB_inputs[6]) mod++;
   if (RGB_inputs[7]) mod++;
   
   if (mod) signal_thread();                                                     //  trigger update thread
   return 1;
}


//  thread function - multiple working threads to update image

void * RGB_thread(void *)
{
   using namespace adjust_RGB_names;

   void  * RGB_wthread(void *arg);                                               //  worker thread

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      if (sa_stat == 3) Fbusy_goal = sa_Npixel;                                  //  set up progress monitor
      else  Fbusy_goal = E3ww * E3hh;
      Fbusy_done = 0;

      do_wthreads(RGB_wthread,NWT);                                              //  worker threads

      Fbusy_goal = Fbusy_done = 0;
      CEF->Fmods++;                                                              //  image modified
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop warning
}


//  worker thread function

void * RGB_wthread(void *arg)                                                    //  overhauled
{
   using namespace adjust_RGB_names;

   float    R1, G1, B1, R3, G3, B3;
   float    briA, briR, briG, briB;
   float    conA, conR, conG, conB;
   float    R, G, B;

   int      index = *((int *) (arg));
   int      px, py, ii, dist = 0;
   float    *pix1, *pix3;
   float    cmax, F, f1, f2;

   briA = RGB_inputs[0];                                                         //  color brightness inputs, -1 to +1
   briR = RGB_inputs[1];
   briG = RGB_inputs[2];
   briB = RGB_inputs[3];

   R = briR - 0.5 * briG - 0.5 * briB + briA;                                    //  red = red - green - blue + all
   G = briG - 0.5 * briR - 0.5 * briB + briA;                                    //  etc.
   B = briB - 0.5 * briR - 0.5 * briG + briA;

   R += 1;                                                                       //  -1 ... 0 ... +1  >>  0 ... 1 ... 2
   G += 1;                                                                       //  increase the range
   B += 1;

   briR = R;                                                                     //  final color brightness factors
   briG = G;
   briB = B;

   conA = RGB_inputs[4];                                                         //  contrast inputs, -1 to +1
   conR = RGB_inputs[5];
   conG = RGB_inputs[6];
   conB = RGB_inputs[7];

   if (conA < 0) conA = 0.5 * conA + 1;                                          //  -1 ... 0  >>  0.5 ... 1.0
   else conA = conA + 1;                                                         //   0 ... 1  >>  1.0 ... 2.0
   if (conR < 0) conR = 0.5 * conR + 1;
   else conR = conR + 1;
   if (conG < 0) conG = 0.5 * conG + 1;
   else conG = conG + 1;
   if (conB < 0) conB = 0.5 * conB + 1;
   else conB = conB + 1;

   conR = conR * conA;                                                           //  apply overall contrast
   conG = conG * conA;
   conB = conB * conA;

   for (py = index; py < E3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < E3ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      R1 = pix1[0];                                                              //  input RGB values, 0-255
      G1 = pix1[1];
      B1 = pix1[2];

      R3 = R1 * briR;                                                            //  apply color brightness factors
      G3 = G1 * briG;
      B3 = B1 * briB;

      R3 = conR * (R3 - 128) + 128;                                              //  apply contrast factors
      G3 = conG * (G3 - 128) + 128;
      B3 = conB * (B3 - 128) + 128;

      if (R3 < 0) R3 = 0;                                                        //  stop underflow
      if (G3 < 0) G3 = 0;
      if (B3 < 0) B3 = 0;

      if (R3 > 255.9 || G3 > 255.9 || B3 > 255.9) {                              //  stop overflow
         cmax = R3;
         if (G3 > cmax) cmax = G3;
         if (B3 > cmax) cmax = B3;
         F = 255.9 / cmax;
         R3 *= F;
         G3 *= F;
         B3 *= F;
      }

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         R3 = f1 * R3 + f2 * R1;
         G3 = f1 * G3 + f2 * G1;
         B3 = f1 * B3 + f2 * B1;
      }

      pix3[0] = R3;                                                              //  output RGB values
      pix3[1] = G3;
      pix3[2] = B3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  HSL color menu function
//  Adjust colors using the HSL (hue/saturation/lightness) color model

namespace adjust_HSL_names
{
   GtkWidget   *RGBframe, *RGBcolor;
   GtkWidget   *Hframe, *Hscale;
   editfunc    EFHSL;                                                            //  edit function data
   int         E3ww, E3hh;
   int         Huse, Suse, Luse;                                                 //  "match using" flags, 0 or 1
   int         Hout, Sout, Lout;                                                 //  "output color" flags, 0 or 1
   float       Rm, Gm, Bm;                                                       //  RGB image color to match
   float       Hm, Sm, Lm;                                                       //  corresp. HSL color
   float       Mlev;                                                             //  match level 0..1 = 100%
   float       Hc, Sc, Lc;                                                       //  new color to add
   float       Rc, Gc, Bc;                                                       //  corresp. RGB color
   float       Adj;                                                              //  color adjustment, 0..1 = 100%
}

void HSLtoRGB(float H, float S, float L, float &R, float &G, float &B);
void RGBtoHSL(float R, float G, float B, float &H, float &S, float &L);


//  menu function

void m_adjust_HSL(GtkWidget *, const char *)                                     //  overhauled
{
   using namespace adjust_HSL_names;

   void   HSL_RGBcolor(GtkWidget *drawarea, cairo_t *cr, int *);
   void   HSL_Hscale(GtkWidget *drawarea, cairo_t *cr, int *);
   int    HSL_dialog_event(zdialog *zd, cchar *event);
   void   HSL_mousefunc();
   void * HSL_thread(void *);
   
   F1_help_topic = "adjust_HSL";

   EFHSL.menufunc = m_adjust_HSL;
   EFHSL.funcname = "adjust_HSL";                                                //  function name
   EFHSL.FprevReq = 1;                                                           //  use preview
   EFHSL.Farea = 2;                                                              //  select area usable
   EFHSL.Frestart = 1;                                                           //  allow restart
   EFHSL.FusePL = 1;                                                             //  use with paint/lever edits OK
   EFHSL.mousefunc = HSL_mousefunc;                                              //  mouse function
   EFHSL.threadfunc = HSL_thread;                                                //  thread function
   if (! edit_setup(EFHSL)) return;                                              //  setup edit
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

/***
       ___________________________________________________
      |                                                   |
      |  Input color to match and adjust: [#####]         |
      |  Match using: [] Hue  [] Saturation  [] Lightness |
      |  Match Level: ==================[]========== 100% |
      |  - - - - - - - - - - - - - - - - - - - - - - - -  |
      |  Output Color                                     |
      |  [########]  [#################################]  |                      //  new color and hue spectrum
      |  [] Color Hue   ================[]==============  |
      |  [] Saturation  =====================[]=========  |
      |  [] Lightness   ===========[]===================  |
      |  Adjustment  ===================[]========== 100% |
      |                                                   |
      |                          [reset] [done] [cancel]  |
      |___________________________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Adjust HSL"),Mwin,Breset,Bdone,Bcancel,null);
   EFHSL.zd = zd;

   zdialog_add_widget(zd,"hbox","hb1","dialog");
   zdialog_add_widget(zd,"label","labmatch","hb1",E2X("Input color to match and adjust:"),"space=5");
   zdialog_add_widget(zd,"colorbutt","matchRGB","hb1","0|0|0");
   zdialog_add_ttip(zd,"matchRGB",E2X("shift+click on image to select color"));
   
   zdialog_add_widget(zd,"hbox","hbmu","dialog");
   zdialog_add_widget(zd,"label","labmu","hbmu",E2X("Match using:"),"space=5");
   zdialog_add_widget(zd,"check","Huse","hbmu",E2X("Hue"),"space=3");
   zdialog_add_widget(zd,"check","Suse","hbmu",E2X("Saturation"),"space=3");
   zdialog_add_widget(zd,"check","Luse","hbmu",E2X("Lightness"),"space=3");
   
   zdialog_add_widget(zd,"hbox","hbmatch","dialog");
   zdialog_add_widget(zd,"label","labmatch","hbmatch",Bmatchlevel,"space=5");
   zdialog_add_widget(zd,"hscale","Mlev","hbmatch","0|1|0.001|1.0","expand");
   zdialog_add_widget(zd,"label","lab100%","hbmatch","100%","space=4");

   zdialog_add_widget(zd,"hsep","sep","dialog",0,"space=5");
   
   zdialog_add_widget(zd,"hbox","hb1","dialog");
   zdialog_add_widget(zd,"label","laboutput","hb1",E2X("Output Color"));

   zdialog_add_widget(zd,"hbox","hb2","dialog");
   zdialog_add_widget(zd,"vbox","vb1","hb2",0,"homog");
   zdialog_add_widget(zd,"vbox","vb2","hb2",0,"homog|expand");

   zdialog_add_widget(zd,"frame","RGBframe","vb1",0,"space=1");                  //  drawing area for RGB color
   RGBframe = zdialog_widget(zd,"RGBframe");
   RGBcolor = gtk_drawing_area_new();
   gtk_container_add(GTK_CONTAINER(RGBframe),RGBcolor);
   gtk_widget_set_size_request(RGBcolor,0,16);
   G_SIGNAL(RGBcolor,"draw",HSL_RGBcolor,0);

   zdialog_add_widget(zd,"frame","Hframe","vb2",0,"space=1");                    //  drawing area for hue scale
   Hframe = zdialog_widget(zd,"Hframe");
   Hscale = gtk_drawing_area_new();
   gtk_container_add(GTK_CONTAINER(Hframe),Hscale);
   gtk_widget_set_size_request(Hscale,200,16);
   G_SIGNAL(Hscale,"draw",HSL_Hscale,0);

   zdialog_add_widget(zd,"check","Hout","vb1",E2X("Color Hue"));
   zdialog_add_widget(zd,"check","Sout","vb1",E2X("Saturation"));
   zdialog_add_widget(zd,"check","Lout","vb1",E2X("Lightness"));
   zdialog_add_widget(zd,"label","labadjust","vb1",E2X("Adjustment"));

   zdialog_add_widget(zd,"hscale","Hc","vb2","0|359.9|0.1|180","expand");
   zdialog_add_widget(zd,"hscale","Sc","vb2","0|1|0.001|0.5","expand");
   zdialog_add_widget(zd,"hscale","Lc","vb2","0|1|0.001|0.5","expand");
   zdialog_add_widget(zd,"hbox","vb2hb","vb2");
   zdialog_add_widget(zd,"hscale","Adj","vb2hb","0|1|0.001|0.0","expand");
   zdialog_add_widget(zd,"label","lab100%","vb2hb","100%","space=4");

   zdialog_stuff(zd,"Huse",1);                                                   //  default: match on hue and saturation
   zdialog_stuff(zd,"Suse",1);
   zdialog_stuff(zd,"Luse",0);
   zdialog_stuff(zd,"Hout",1);                                                   //  default: replace only hue
   zdialog_stuff(zd,"Sout",0);
   zdialog_stuff(zd,"Lout",0);

   Rm = Gm = Bm = 0;                                                             //  color to match = black = not set
   Hm = Sm = Lm = 0;
   Huse = Suse = 1;
   Luse = 0;
   Hout = 1;
   Sout = Lout = 0;
   Mlev = 1.0;
   Hc = 180;                                                                     //  new HSL color to set / mix
   Sc = 0.5;
   Lc = 0.5;
   Adj = 0.0;
   
   zdialog_run(zd,HSL_dialog_event,"save");                                      //  run dialog - parallel
   takeMouse(HSL_mousefunc,arrowcursor);                                         //  connect mouse function
   return;
}


//  Paint RGBcolor drawing area with RGB color from new HSL color

void HSL_RGBcolor(GtkWidget *drawarea, cairo_t *cr, int *)
{
   using namespace adjust_HSL_names;

   int      ww, hh;

   ww = gtk_widget_get_allocated_width(drawarea);                                //  drawing area size
   hh = gtk_widget_get_allocated_height(drawarea);
   
   HSLtoRGB(Hc,Sc,Lc,Rc,Gc,Bc);                                                  //  new RGB color

   cairo_set_source_rgb(cr,Rc,Gc,Bc);
   cairo_rectangle(cr,0,0,ww-1,hh-1);
   cairo_fill(cr);

   return;
}


//  Paint Hscale drawing area with all hue values in a horizontal scale

void HSL_Hscale(GtkWidget *drawarea, cairo_t *cr, int *)
{
   using namespace adjust_HSL_names;

   int      px, ww, hh;
   float    H, S, L, R, G, B;

   ww = gtk_widget_get_allocated_width(drawarea);                                //  drawing area size
   hh = gtk_widget_get_allocated_height(drawarea);
   
   S = L = 0.5;

   for (px = 0; px < ww; px++)                                                   //  paint hue color scale
   {
      H = 360 * px / ww;
      HSLtoRGB(H,S,L,R,G,B);
      cairo_set_source_rgb(cr,R,G,B);
      cairo_move_to(cr,px,0);
      cairo_line_to(cr,px,hh-1);
      cairo_stroke(cr);
   }

   return;
}


//  HSL dialog event and completion function

int HSL_dialog_event(zdialog *zd, cchar *event)                                  //  HSL dialog event function
{
   using namespace adjust_HSL_names;

   void   HSL_mousefunc();

   int      mod = 0;

   if (strmatch(event,"done")) zd->zstat = 2;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  from f_open()
   
   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();
      E3ww = E3pxm->ww;
      E3hh = E3pxm->hh;
      signal_thread();
      return 1;
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  reset
         zd->zstat = 0;                                                          //  keep dialog active
         Mlev = 1.0;
         Hc = 180;                                                               //  set defaults
         Sc = 0.5;
         Lc = 0.5;
         Adj = 0.0;
         zdialog_stuff(zd,"Mlev",Mlev);
         zdialog_stuff(zd,"Hc",Hc);
         zdialog_stuff(zd,"Sc",Sc);
         zdialog_stuff(zd,"Lc",Lc);
         zdialog_stuff(zd,"Adj",Adj);
         edit_reset();
         return 1;                                                               //  19.0
      }
      else if (zd->zstat == 2) {                                                 //  done
         edit_fullsize();                                                        //  get full size image
         E3ww = E3pxm->ww;
         E3hh = E3pxm->hh;
         signal_thread();
         edit_done(0);                                                           //  commit edit
         return 1;
      }
      else {
         edit_cancel(0);                                                         //  discard edit
         return 1;
      }
   }

   if (strmatch("focus",event)) {
      takeMouse(HSL_mousefunc,arrowcursor);
      return 1;
   }
   
   if (strmatch(event,"Huse")) {                                                 //  match on Hue, 0/1
      zdialog_fetch(zd,"Huse",Huse);
      mod = 1;
   }
   
   if (strmatch(event,"Suse")) {                                                 //  match on Saturation, 0/1
      zdialog_fetch(zd,"Suse",Suse);
      mod = 1;
   }
   
   if (strmatch(event,"Luse")) {                                                 //  match on Lightness, 0/1
      zdialog_fetch(zd,"Luse",Luse);
      mod = 1;
   }
   
   if (strmatch(event,"Hout")) {                                                 //  replace Hue, 0/1
      zdialog_fetch(zd,"Hout",Hout);
      mod = 1;
   }
   
   if (strmatch(event,"Sout")) {                                                 //  replace Saturation, 0/1
      zdialog_fetch(zd,"Sout",Sout);
      mod = 1;
   }
   
   if (strmatch(event,"Lout")) {                                                 //  replace Lightness, 0/1
      zdialog_fetch(zd,"Lout",Lout);
      mod = 1;
   }
   
   if (strmatch("Mlev",event)) {                                                 //  color match 0..1 = 100%
      zdialog_fetch(zd,"Mlev",Mlev);
      mod = 1;
   }

   if (strmatch("Hc",event)) {                                                   //  new color hue 0-360
      zdialog_fetch(zd,"Hc",Hc);
      mod = 1;
   }
      
   if (strmatch("Sc",event)) {                                                   //  saturation 0-1
      zdialog_fetch(zd,"Sc",Sc);
      mod = 1;
   }

   if (strmatch("Lc",event)) {                                                   //  lightness 0-1
      zdialog_fetch(zd,"Lc",Lc);
      mod = 1;
   }

   if (strmatch("Adj",event)) {                                                  //  adjustment 0..1 = 100%
      zdialog_fetch(zd,"Adj",Adj);
      mod = 1;
   }
   
   if (strmatch("blendwidth",event)) mod = 1;                                    //  area blend width changed
   
   if (mod) {
      gtk_widget_queue_draw(RGBcolor);                                           //  draw current RGB color
      signal_thread();                                                           //  trigger update thread
   }

   return 1;
}


//  mouse function
//  click on image to set the color to match and change

void HSL_mousefunc()
{
   using namespace adjust_HSL_names;

   int         mx, my, px, py;
   char        color[20];
   float       *pix1, R, G, B;
   float       f256 = 1.0 / 256.0;
   zdialog     *zd = EFHSL.zd;

   if (! KBshiftkey) return;                                                     //  check shift + left or right click
   if (! LMclick && ! RMclick) return;

   mx = Mxclick;                                                                 //  clicked pixel on image
   my = Myclick;

   if (mx < 1) mx = 1;                                                           //  pull back from image edge
   if (mx > E3ww - 2) mx = E3ww - 2;
   if (my < 1) my = 1;
   if (my > E3hh - 2) my = E3hh - 2;
   
   R = G = B = 0;

   for (py = my-1; py <= my+1; py++)                                             //  compute average RGB for 3x3        18.01
   for (px = mx-1; px <= mx+1; px++)                                             //    block of pixels
   {
      pix1 = PXMpix(E1pxm,px,py);
      R += pix1[0];
      G += pix1[1];
      B += pix1[2];
   }
   
   R = R / 9;
   G = G / 9;
   B = B / 9;

   if (LMclick)                                                                  //  left mouse click
   {                                                                             //  pick MATCH color from image
      LMclick = 0;
      snprintf(color,19,"%.0f|%.0f|%.0f",R,G,B);                                 //  draw new match color button
      if (zd) zdialog_stuff(zd,"matchRGB",color);
      Rm = R * f256;
      Gm = G * f256;
      Bm = B * f256;
      RGBtoHSL(Rm,Gm,Bm,Hm,Sm,Lm);                                               //  set HSL color to match
   }
   
   if (RMclick)                                                                  //  right mouse click
   {                                                                             //  pick OUTPUT color from image
      RMclick = 0;
      R = R * f256;
      G = G * f256;
      B = B * f256;
      RGBtoHSL(R,G,B,Hc,Sc,Lc);                                                  //  output HSL
      zdialog_stuff(zd,"Hc",Hc);
      zdialog_stuff(zd,"Sc",Sc);
      zdialog_stuff(zd,"Lc",Lc);
      gtk_widget_queue_draw(RGBcolor);                                           //  draw current RGB color
      signal_thread();                                                           //  trigger update thread
   }

   return;
}


//  thread function - multiple working threads to update image

void * HSL_thread(void *)
{
   using namespace adjust_HSL_names;

   void  * HSL_wthread(void *arg);                                               //  worker thread

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      do_wthreads(HSL_wthread,NWT);                                              //  worker threads

      CEF->Fmods++;                                                              //  image modified
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop warning
}


//  worker thread function

void * HSL_wthread(void *arg)
{
   using namespace adjust_HSL_names;
   
   int      index = *((int *) (arg));
   int      px, py, ii, dist = 0;
   float    *pix1, *pix3;
   float    R1, G1, B1, R3, G3, B3;
   float    H1, S1, L1, H3, S3, L3;
   float    dH, dS, dL, match;
   float    a1, a2, f1, f2;
   float    f256 = 1.0 / 256.0;

   for (py = index; py < E3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < E3ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel
      
      R1 = f256 * pix1[0];                                                       //  input pixel RGB
      G1 = f256 * pix1[1];
      B1 = f256 * pix1[2];
      
      RGBtoHSL(R1,G1,B1,H1,S1,L1);                                               //  convert to HSL

      match = 1.0;                                                               //  compare image pixel to match HSL

      if (Huse) {
         dH = fabsf(Hm - H1);
         if (360 - dH < dH) dH = 360 - dH;
         dH *= 0.002778;                                                         //  H difference, normalized 0..1
         match = 1.0 - dH;
      }

      if (Suse) {
         dS = fabsf(Sm - S1);                                                    //  S difference, 0..1
         match *= (1.0 - dS);
      }

      if (Luse) {
         dL = fabsf(Lm - L1);                                                    //  L difference, 0..1
         match *= (1.0 - dL);
      }

      a1 = pow(match, 10.0 * Mlev);                                              //  color selectivity, 0..1 = max
      a1 = Adj * a1;
      a2 = 1.0 - a1;
      
      if (Hout) H3 = a1 * Hc + a2 * H1;                                          //  output HSL = a1 * new HSL
      else H3 = H1;                                                              //             + a2 * old HSL
      if (Sout) S3 = a1 * Sc + a2 * S1;
      else S3 = S1;
      if (Lout) L3 = a1 * Lc + a2 * L1;
      else L3 = L1;
      
      HSLtoRGB(H3,S3,L3,R3,G3,B3);
      
      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         R3 = f1 * R3 + f2 * R1;
         G3 = f1 * G3 + f2 * G1;
         B3 = f1 * B3 + f2 * B1;
      }
      
      pix3[0] = 255.0 * R3;
      pix3[1] = 255.0 * G3;
      pix3[2] = 255.0 * B3;
   }

   pthread_exit(0);
}


//  HSL to RGB converter (Wikipedia)
//  H = 0-360 deg.  S = 0-1   L = 0-1
//  output RGB values = 0-1

void HSLtoRGB(float H, float S, float L, float &R, float &G, float &B)
{
   float    C, X, M;
   float    h1, h2;
   
   h1 = H / 60;
   h2 = h1 - 2 * int(h1/2);
   
   C = (1 - fabsf(2*L-1)) * S;
   X = C * (1 - fabsf(h2-1));
   M = L - C/2;
   
   if (H < 60) {
      R = C;
      G = X;
      B = 0;
   }
   
   else if (H < 120) {
      R = X;
      G = C;
      B = 0;
   }
   
   else if (H < 180) {
      R = 0;
      G = C;
      B = X;
   }
   
   else if (H < 240) {
      R = 0;
      G = X;
      B = C;
   }
   
   else if (H < 300) {
      R = X;
      G = 0;
      B = C;
   }
   
   else {
      R = C;
      G = 0;
      B = X;
   }
   
   R = R + M;
   G = G + M;
   B = B + M;
   
   return;
}


//  RGB to HSL converter 
//  input RGB values 0-1
//  outputs: H = 0-360 deg.  S = 0-1   L = 0-1

void RGBtoHSL(float R, float G, float B, float &H, float &S, float &L)
{
   float    max, min, D;

   max = R;
   if (G > max) max = G;
   if (B > max) max = B;
   
   min = R;
   if (G < min) min = G;
   if (B < min) min = B;
   
   D = max - min;
   
   L = 0.5 * (max + min);
   
   if (D < 0.004) {
      H = S = 0;
      return;
   }
   
   if (L > 0.5)
      S = D / (2 - max - min);
   else
      S = D / (max + min);
   
   if (max == R) 
      H = (G - B) / D;
   else if (max == G)
      H = 2 + (B - R) / D;
   else
      H = 4 + (R - G) / D;

   H = H * 60;
   if (H < 0) H += 360;

   return;
}


/********************************************************************************/

//  add text on top of the image

namespace addtext
{
   #define Bversion E2X("+Version")

   textattr_t  attr;                                                             //  text attributes and image

   char     file[1000] = "";                                                     //  file for write_text data
   char     metakey[60] = "";
   int      px, py;                                                              //  text position on image
   int      textpresent;                                                         //  flag, text present on image

   int   dialog_event(zdialog *zd, cchar *event);                                //  dialog event function
   void  mousefunc();                                                            //  mouse event function
   void  write(int mode);                                                        //  write text on image

   editfunc    EFaddtext;
}


//  menu function

void m_add_text(GtkWidget *, cchar *menu)
{
   using namespace addtext;

   cchar    *title = E2X("Add Text to Image");
   cchar    *tip = E2X("Enter text, click/drag on image, right click to remove");

   F1_help_topic = "add_text";                                                   //  user guide topic

   EFaddtext.menufunc = m_add_text;
   EFaddtext.funcname = "add_text";
   EFaddtext.Farea = 1;                                                          //  select area ignored
   EFaddtext.Frestart = 1;                                                       //  allow restart
   EFaddtext.mousefunc = mousefunc;                                              //  18.01
   if (! edit_setup(EFaddtext)) return;                                          //  setup edit

/***
       ____________________________________________________________________
      |                     Add Text to Image                              |
      |                                                                    |
      |  Enter text, click/drag on image, right click to remove.           |
      |                                                                    |
      |  Use settings file  [Open] [Save]                                  |     Bopen Bsave
      |  Text [____________________________________________________]       |     text
      |  Use metadata key [________________________________] [Fetch]       |     metakey Bfetch
      |  [Font] [FreeSans_________]  Size [ 44|v]                          |     Bfont fontname fontsize
      |                                                                    |
      |            color   transp.   width     angle                       |
      |  text     [#####] [_______]           [______]                     |     txcolor txtransp txangle
      |  backing  [#####] [_______]                                        |     bgcolor bgtransp
      |  outline  [#####] [_______] [_______]                              |     tocolor totransp towidth
      |  shadow   [#####] [_______] [_______] [______]                     |     shcolor shtransp shwidth shangle
      |                                                                    |
      |        [Clear] [Replace] [+Version] [Next] [Apply] [Done] [Cancel] |
      |____________________________________________________________________|

      [Clear]     clear text and metadata key
      [Replace]   edit_done(), replace current file, restart dialog
      [+Version]  edit_done(), create new file version, restart dialog
      [Next]      edit_done(), replace current file, move to next file, restart dialog, write same text
      [Apply]     edit_done, restart dialog
      [Done]      edit_done()
      [Cancel]    edit_cancel()

***/

   zdialog *zd = zdialog_new(title,Mwin,Bclear,Breplace,Bversion,Bnext,Bapply,Bdone,Bcancel,null);
   EFaddtext.zd = zd;
   EFaddtext.mousefunc = mousefunc;
   EFaddtext.menufunc = m_add_text;                                              //  allow restart

   zdialog_add_widget(zd,"label","tip","dialog",tip,"space=5");

   zdialog_add_widget(zd,"hbox","hbfile","dialog",0,"space=2");
   zdialog_add_widget(zd,"label","labfile","hbfile",E2X("Use settings file"),"space=3");
   zdialog_add_widget(zd,"button",Bopen,"hbfile",Bopen);
   zdialog_add_widget(zd,"button",Bsave,"hbfile",Bsave);

   zdialog_add_widget(zd,"hbox","hbtext","dialog",0,"space=2");
   zdialog_add_widget(zd,"label","labtext","hbtext",E2X("Text"),"space=5");
   zdialog_add_widget(zd,"frame","frtext","hbtext",0,"expand");
   zdialog_add_widget(zd,"edit","text","frtext","text","expand|wrap");
   
   zdialog_add_widget(zd,"hbox","hbmeta","dialog",0,"space=2");
   zdialog_add_widget(zd,"label","labmeta","hbmeta",E2X("Use metadata key"),"space=5");
   zdialog_add_widget(zd,"zentry","metakey","hbmeta",0,"space=2|expand");
   zdialog_add_widget(zd,"button",Bfetch,"hbmeta",Bfetch);

   zdialog_add_widget(zd,"hbox","hbfont","dialog",0,"space=2");
   zdialog_add_widget(zd,"button",Bfont,"hbfont",Bfont);
   zdialog_add_widget(zd,"zentry","fontname","hbfont","FreeSans","space=2|size=20");
   zdialog_add_widget(zd,"label","space","hbfont",0,"space=10");
   zdialog_add_widget(zd,"label","labfsize","hbfont",Bsize);
   zdialog_add_widget(zd,"zspin","fontsize","hbfont","8|500|1|40","space=3");

   zdialog_add_widget(zd,"hbox","hbattr","dialog",0,"space=3");
   zdialog_add_widget(zd,"vbox","vbattr1","hbattr",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vbattr2","hbattr",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vbattr3","hbattr",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vbattr4","hbattr",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vbattr5","hbattr",0,"homog|space=2");

   zdialog_add_widget(zd,"label","space","vbattr1");
   zdialog_add_widget(zd,"label","labtext","vbattr1",E2X("text"));
   zdialog_add_widget(zd,"label","labback","vbattr1",E2X("backing"));
   zdialog_add_widget(zd,"label","laboutln","vbattr1",E2X("outline"));
   zdialog_add_widget(zd,"label","labshadow","vbattr1",E2X("shadow"));

   zdialog_add_widget(zd,"label","labcol","vbattr2",Bcolor);
   zdialog_add_widget(zd,"colorbutt","txcolor","vbattr2","0|0|0");
   zdialog_add_widget(zd,"colorbutt","bgcolor","vbattr2","255|255|255");
   zdialog_add_widget(zd,"colorbutt","tocolor","vbattr2","255|0|0");
   zdialog_add_widget(zd,"colorbutt","shcolor","vbattr2","255|0|0");

   zdialog_add_widget(zd,"label","labtran","vbattr3","transp.");
   zdialog_add_widget(zd,"zspin","txtransp","vbattr3","0|100|1|0");
   zdialog_add_widget(zd,"zspin","bgtransp","vbattr3","0|100|1|0");
   zdialog_add_widget(zd,"zspin","totransp","vbattr3","0|100|1|0");
   zdialog_add_widget(zd,"zspin","shtransp","vbattr3","0|100|1|0");

   zdialog_add_widget(zd,"label","labw","vbattr4",Bwidth);
   zdialog_add_widget(zd,"label","space","vbattr4");
   zdialog_add_widget(zd,"label","space","vbattr4");
   zdialog_add_widget(zd,"zspin","towidth","vbattr4","0|30|1|0");
   zdialog_add_widget(zd,"zspin","shwidth","vbattr4","0|50|1|0");

   zdialog_add_widget(zd,"label","labw","vbattr5",Bangle);
   zdialog_add_widget(zd,"zspin","txangle","vbattr5","-360|360|0.5|0");
   zdialog_add_widget(zd,"label","space","vbattr5");
   zdialog_add_widget(zd,"label","space","vbattr5");
   zdialog_add_widget(zd,"zspin","shangle","vbattr5","-360|360|1|0");

   zdialog_add_ttip(zd,Breplace,E2X("save to current file"));
   zdialog_add_ttip(zd,Bversion,E2X("save as new file version"));
   zdialog_add_ttip(zd,Bnext,E2X("save to current file \n"
                                 "open next file with same text"));

   zdialog_restore_inputs(zd);                                                   //  restore prior inputs

   memset(&attr,0,sizeof(attr));

   zdialog_fetch(zd,"text",attr.text,1000);                                      //  get defaults or prior inputs
   zdialog_fetch(zd,"fontname",attr.font,80);
   zdialog_fetch(zd,"fontsize",attr.size);
   zdialog_fetch(zd,"txcolor",attr.color[0],20);
   zdialog_fetch(zd,"txtransp",attr.transp[0]);
   zdialog_fetch(zd,"txangle",attr.angle);
   zdialog_fetch(zd,"bgcolor",attr.color[1],20);
   zdialog_fetch(zd,"bgtransp",attr.transp[1]);
   zdialog_fetch(zd,"tocolor",attr.color[2],20);
   zdialog_fetch(zd,"totransp",attr.transp[2]);
   zdialog_fetch(zd,"towidth",attr.towidth);
   zdialog_fetch(zd,"shcolor",attr.color[3],20);
   zdialog_fetch(zd,"shtransp",attr.transp[3]);
   zdialog_fetch(zd,"shwidth",attr.shwidth);
   zdialog_fetch(zd,"shangle",attr.shangle);
   zdialog_fetch(zd,"metakey",metakey,60);

   gentext(&attr);                                                               //  initial text

   takeMouse(mousefunc,dragcursor);                                              //  connect mouse function
   textpresent = 0;                                                              //  no text on image yet
   zdialog_run(zd,dialog_event,"save");                                          //  run dialog, parallel
   if (*metakey) zdialog_send_event(zd,Bfetch);                                  //  metadata key active, get text
   return;
}


//  dialog event and completion callback function

int addtext::dialog_event(zdialog *zd, cchar *event)
{
   using namespace addtext;

   GtkWidget   *font_dialog;
   char        font[100];                                                        //  font name and size
   int         size, err;
   char        *newfilename, *pp;
   cchar       *keyname[1];
   char        *keyvals[1];

   if (strmatch(event,"done")) zd->zstat = 6;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 7;                                  //  cancel

   if (zd->zstat)
   {
      if (zd->zstat < 0 || zd->zstat > 7) zd->zstat = 7;                         //  cancel

      if (zd->zstat == 1) {                                                      //  clear all inputs
         *attr.text = 0;
         *metakey = 0;
         zdialog_stuff(zd,"text","");
         zdialog_stuff(zd,"metakey","");
         zd->zstat = 0;                                                          //  keep dialog active
         write(2);                                                               //  erase text on image         bugfix 19.0
         return 1;
      }
   
      if (zd->zstat == 2) {                                                      //  replace current file
         if (textpresent) edit_done(0);                                          //  finish
         else edit_cancel(0);
         f_save(curr_file,curr_file_type,curr_file_bpc,0,1);                     //  replace curr. file
         curr_file_size = f_save_size;
         m_add_text(0,0);                                                        //  start again
         return 1;
      }

      if (zd->zstat == 3) {                                                      //  make a new file version
         if (textpresent) edit_done(0);                                          //  finish
         else edit_cancel(0);
         newfilename = file_new_version(curr_file);                              //  get next avail. file version name
         if (! newfilename) return 1;
         err = f_save(newfilename,curr_file_type,curr_file_bpc,0,1);             //  save file
         if (! err) f_open_saved();                                              //  open saved file with edit hist
         zfree(newfilename);
         m_add_text(0,0);                                                        //  start again
         return 1;
      }

      if (zd->zstat == 4) {                                                      //  finish and go to next image file
         if (textpresent) edit_done(0);                                          //  save mods
         else edit_cancel(0);
         f_save(curr_file,curr_file_type,curr_file_bpc,0,1);                     //  replace curr. file
         curr_file_size = f_save_size;
         m_next(0,0);                                                            //  open next file
         m_add_text(0,0);                                                        //  start again
         write(1);                                                               //  put same text etc. onto image
         return 1;
      }
      
      if (zd->zstat == 5) {                                                      //  apply
         if (! textpresent) return 1;
         edit_done(0);                                                           //  save mods
         m_add_text(0,0);                                                        //  restart
         return 1;
      }

      if (zd->zstat == 6) {                                                      //  done
         if (textpresent) edit_done(0);                                          //  save mods
         else edit_cancel(0);
         return 1;
      }

      if (zd->zstat == 7) {
         edit_cancel(0);                                                         //  cancel or [x]
         return 1;
      }
   }

   if (strmatch(event,"focus")) {                                                //  toggle mouse capture
      takeMouse(mousefunc,dragcursor);                                           //  connect mouse function
      return 1;
   }

   if (strmatch(event,Bopen))                                                    //  load zdialog fields from a file
   {
      load_text(zd);
      zdialog_fetch(zd,"text",attr.text,1000);                                   //  get all zdialog fields
      zdialog_fetch(zd,"fontname",attr.font,80);
      zdialog_fetch(zd,"fontsize",attr.size);
      zdialog_fetch(zd,"txcolor",attr.color[0],20);
      zdialog_fetch(zd,"txtransp",attr.transp[0]);
      zdialog_fetch(zd,"txangle",attr.angle);
      zdialog_fetch(zd,"bgcolor",attr.color[1],20);
      zdialog_fetch(zd,"bgtransp",attr.transp[1]);
      zdialog_fetch(zd,"tocolor",attr.color[2],20);
      zdialog_fetch(zd,"totransp",attr.transp[2]);
      zdialog_fetch(zd,"towidth",attr.towidth);
      zdialog_fetch(zd,"shcolor",attr.color[3],20);
      zdialog_fetch(zd,"shtransp",attr.transp[3]);
      zdialog_fetch(zd,"shwidth",attr.shwidth);
      zdialog_fetch(zd,"shangle",attr.shangle);
   }

   if (strmatch(event,Bsave)) {                                                  //  save zdialog fields to file
      save_text(zd);
      return 1;
   }
   
   if (strmatch(event,Bfetch)) {                                                 //  load text from metadata keyname
      zdialog_fetch(zd,"metakey",metakey,60);
      if (*metakey < ' ') return 1;
      keyname[0] = metakey;
      exif_get(curr_file,keyname,keyvals,1);
      if (! keyvals[0]) return 1;
      if (strlen(keyvals[0]) > 999) keyvals[0][999] = 0;
      repl_1str(keyvals[0],attr.text,"\\n","\n");                                //  replace "\n" with newlines
      zfree(keyvals[0]);
      zdialog_stuff(zd,"text",attr.text);                                        //  stuff dialog with metadata
   }

   if (strmatch(event,"text"))                                                   //  get text from dialog
      zdialog_fetch(zd,"text",attr.text,1000);

   if (strmatch(event,Bfont)) {                                                  //  select new font
      snprintf(font,100,"%s %d",attr.font,attr.size);
      font_dialog = gtk_font_chooser_dialog_new(E2X("select font"),MWIN);
      gtk_font_chooser_set_font(GTK_FONT_CHOOSER(font_dialog),font);
      gtk_dialog_run(GTK_DIALOG(font_dialog));
      pp = gtk_font_chooser_get_font(GTK_FONT_CHOOSER(font_dialog));
      gtk_widget_destroy(font_dialog);

      if (pp) {                                                                  //  should have "fontname nn"
         strncpy0(font,pp,100);
         g_free(pp);
         pp = font + strlen(font);
         while (*pp != ' ') pp--;
         if (pp > font) {
            size = atoi(pp);
            if (size < 8) size = 8;
            zdialog_stuff(zd,"fontsize",size);
            attr.size = size;
            *pp = 0;
            strncpy0(attr.font,font,80);                                         //  get fontname = new font name
            zdialog_stuff(zd,"fontname",font);
         }
      }
   }

   if (strmatch(event,"fontsize"))                                               //  new font size
      zdialog_fetch(zd,"fontsize",attr.size);

   if (strmatch(event,"txangle"))
      zdialog_fetch(zd,"txangle",attr.angle);

   if (strmatch(event,"txcolor"))                                                //  foreground (text) color
      zdialog_fetch(zd,"txcolor",attr.color[0],20);

   if (strmatch(event,"bgcolor"))                                                //  background color
      zdialog_fetch(zd,"bgcolor",attr.color[1],20);

   if (strmatch(event,"tocolor"))                                                //  text outline color
      zdialog_fetch(zd,"tocolor",attr.color[2],20);

   if (strmatch(event,"shcolor"))                                                //  text shadow color
      zdialog_fetch(zd,"shcolor",attr.color[3],20);

   if (strmatch(event,"txtransp"))                                               //  foreground transparency
      zdialog_fetch(zd,"txtransp",attr.transp[0]);

   if (strmatch(event,"bgtransp"))                                               //  background transparency
      zdialog_fetch(zd,"bgtransp",attr.transp[1]);

   if (strmatch(event,"totransp"))                                               //  text outline transparency
      zdialog_fetch(zd,"totransp",attr.transp[2]);

   if (strmatch(event,"shtransp"))                                               //  text shadow transparency
      zdialog_fetch(zd,"shtransp",attr.transp[3]);

   if (strmatch(event,"towidth"))                                                //  text outline width
      zdialog_fetch(zd,"towidth",attr.towidth);

   if (strmatch(event,"shwidth"))                                                //  text shadow width
      zdialog_fetch(zd,"shwidth",attr.shwidth);

   if (strmatch(event,"shangle"))                                                //  text shadow angle
      zdialog_fetch(zd,"shangle",attr.shangle);

   gentext(&attr);                                                               //  build text image from text and attributes
   zmainloop();                                                                  //  18.07

   if (textpresent) write(1);                                                    //  update text on image
   return 1;
}


//  mouse function, set position for text on image

void addtext::mousefunc()
{
   using namespace addtext;

   if (LMclick) {                                                                //  left mouse click
      px = Mxclick;                                                              //  new text position on image
      py = Myclick;
      write(1);                                                                  //  erase old, write new text on image
   }

   if (RMclick) {                                                                //  right mouse click
      write(2);                                                                  //  erase old text, if any
      px = py = -1;
   }

   if (Mxdrag || Mydrag)                                                         //  mouse dragged
   {
      px = Mxdrag;                                                               //  new text position on image
      py = Mydrag;
      write(1);                                                                  //  erase old, write new text on image
   }

   LMclick = RMclick = Mxdrag = Mydrag = 0;
   return;
}


//  write text on image at designated location
//  mode: 1  erase old and write to new position
//        2  erase old and write nothing

void addtext::write(int mode)
{
   using namespace addtext;

   float       *pix1, *pix3;
   uint8       *pixT;
   int         px1, py1, px3, py3, done;
   float       e3part, Ot, Om, Ob;
   static int  orgx1, orgy1, ww1, hh1;                                           //  old text image overlap rectangle
   int         orgx2, orgy2, ww2, hh2;                                           //  new overlap rectangle
   int         nc = E1pxm->nc, pcc = nc * sizeof(float);

   cairo_t *cr = draw_context_create(gdkwin,draw_context);
   
   if (textpresent)
   {
      for (py3 = orgy1; py3 < orgy1 + hh1; py3++)                                //  erase prior text image
      for (px3 = orgx1; px3 < orgx1 + ww1; px3++)                                //  replace E3 pixels with E1 pixels
      {                                                                          //    in prior overlap rectangle
         if (px3 < 0 || px3 >= E3pxm->ww) continue;
         if (py3 < 0 || py3 >= E3pxm->hh) continue;
         pix1 = PXMpix(E1pxm,px3,py3);
         pix3 = PXMpix(E3pxm,px3,py3);
         memcpy(pix3,pix1,pcc);
      }
   }

   done = 0;
   if (mode == 2) done = 1;                                                      //  erase only
   if (! *attr.text) done = 2;                                                   //  no text defined
   if (px < 0 && py < 0) done = 3;                                               //  no position defined

   if (done) {
      if (textpresent) {
         Fpaint3(orgx1,orgy1,ww1,hh1,cr);                                        //  update window to erase old text
         textpresent = 0;                                                        //  mark no text present
         CEF->Fmods--;
      }

      draw_context_destroy(draw_context); 
      return;
   }
   
   ww2 = attr.pxb_text->ww;                                                      //  text image size
   hh2 = attr.pxb_text->hh;

   if (px > E3pxm->ww) px = E3pxm->ww;                                           //  if off screen, pull back in sight
   if (py > E3pxm->hh) py = E3pxm->hh;

   orgx2 = px - ww2/2;                                                           //  copy-to image3 location
   orgy2 = py - hh2/2;

   for (py1 = 0; py1 < hh2; py1++)                                               //  loop all pixels in text image
   for (px1 = 0; px1 < ww2; px1++)
   {
      px3 = orgx2 + px1;                                                         //  copy-to image3 pixel
      py3 = orgy2 + py1;

      if (px3 < 0 || px3 >= E3pxm->ww) continue;                                 //  omit parts beyond edges
      if (py3 < 0 || py3 >= E3pxm->hh) continue;

      pixT = PXBpix(attr.pxb_text,px1,py1);                                      //  copy-from text pixel
      pix3 = PXMpix(E3pxm,px3,py3);                                              //  copy-to image pixel

      e3part = pixT[3] / 256.0;                                                  //  text image transparency
      
      pix3[0] = pixT[0] + e3part * pix3[0];                                      //  combine text part + image part
      pix3[1] = pixT[1] + e3part * pix3[1];
      pix3[2] = pixT[2] + e3part * pix3[2];

      if (nc > 3) {
         Ot = (1.0 - e3part);                                                    //  text opacity
         Om = pix3[3] / 256.0;                                                   //  image opacity
         Ob = 1.0 - (1.0 - Ot) * (1.0 - Om);                                     //  combined opacity
         pix3[3] = 255.0 * Ob;
      }
   }

   if (textpresent) {
      Fpaint3(orgx1,orgy1,ww1,hh1,cr);                                           //  update window to erase old text
      textpresent = 0;
      CEF->Fmods--;
   }
                                                                                 //  (updates together to reduce flicker)
   Fpaint3(orgx2,orgy2,ww2,hh2,cr);                                              //  update window for new text

   draw_context_destroy(draw_context); 

   textpresent = 1;                                                              //  mark text is present
   CEF->Fmods++;                                                                 //  increase mod count
   CEF->Fsaved = 0;

   orgx1 = orgx2;                                                                //  remember overlap rectangle
   orgy1 = orgy2;                                                                //    for next call
   ww1 = ww2;
   hh1 = hh2;
   return;
}


//  load text and attributes from a file

void load_text(zdialog *zd)
{
   FILE        *fid;
   int         err, nn;
   char        *pp, *pp2, *file, buff[1200];
   cchar       *dialogtitle = "load text data from a file";
   textattr_t  attr;

   file = zgetfile(dialogtitle,MWIN,"file",addtext_folder);                      //  get input file from user
   if (! file) return;

   fid = fopen(file,"r");                                                        //  open for read
   if (! fid) {
      zmessageACK(Mwin,strerror(errno));
      zfree(file);
      return;
   }

   pp = fgets_trim(buff,1200,fid);                                               //  read text string
   if (! pp) goto badfile;
   if (! strmatchN(pp,"text string: ",13)) goto badfile;
   pp += 13;
   if (strlen(pp) < 2) goto badfile;
   repl_Nstrs(pp,attr.text,"\\n","\n",null);                                     //  replace "\n" with newline char.

   pp = fgets_trim(buff,1200,fid);                                               //  read font and size
   if (! pp) goto badfile;
   if (! strmatchN(pp,"font: ",6)) goto badfile;
   pp += 6;
   strTrim(pp);
   pp2 = strstr(pp,"size: ");
   if (! pp2) goto badfile;
   *pp2 = 0;
   pp2 += 6;
   strncpy0(attr.font,pp,80);
   attr.size = atoi(pp2);
   if (attr.size < 8) goto badfile;

   pp = fgets_trim(buff,1200,fid);                                               //  read text attributes
   if (! pp) goto badfile;
   nn = sscanf(pp,"attributes: %f  %s %s %s %s  %d %d %d %d  %d %d %d",
            &attr.angle, attr.color[0], attr.color[1], attr.color[2], attr.color[3],
            &attr.transp[0], &attr.transp[1], &attr.transp[2], &attr.transp[3],
            &attr.towidth, &attr.shwidth, &attr.shangle);
   if (nn != 12) goto badfile;

   err = fclose(fid);
   if (err) {
      zmessageACK(Mwin,strerror(errno));
      zfree(file);
      return;
   }

   zdialog_stuff(zd,"text",attr.text);                                           //  stuff zdialog fields
   zdialog_stuff(zd,"fontname",attr.font);
   zdialog_stuff(zd,"fontsize",attr.size);
   zdialog_stuff(zd,"txangle",attr.angle);
   zdialog_stuff(zd,"txcolor",attr.color[0]);
   zdialog_stuff(zd,"bgcolor",attr.color[1]);
   zdialog_stuff(zd,"tocolor",attr.color[2]);
   zdialog_stuff(zd,"shcolor",attr.color[3]);
   zdialog_stuff(zd,"txtransp",attr.transp[0]);
   zdialog_stuff(zd,"bgtransp",attr.transp[1]);
   zdialog_stuff(zd,"totransp",attr.transp[2]);
   zdialog_stuff(zd,"shtransp",attr.transp[3]);
   zdialog_stuff(zd,"towidth",attr.towidth);
   zdialog_stuff(zd,"shwidth",attr.shwidth);
   zdialog_stuff(zd,"shangle",attr.shangle);
   return;

badfile:                                                                         //  project file had a problem
   fclose(fid);
   zmessageACK(Mwin,E2X("text file is defective"));
   printz("buff: %s\n",buff);
}


//  save text to a file

void save_text(zdialog *zd)
{
   cchar       *dialogtitle = "save text data to a file";
   FILE        *fid;
   char        *file, text2[1200];
   textattr_t  attr;

   file = zgetfile(dialogtitle,MWIN,"save",addtext_folder);                      //  get output file from user
   if (! file) return;

   fid = fopen(file,"w");                                                        //  open for write
   if (! fid) {
      zmessageACK(Mwin,strerror(errno));
      zfree(file);
      return;
   }

   zdialog_fetch(zd,"text",attr.text,1000);                                      //  get text and attributes from zdialog
   zdialog_fetch(zd,"fontname",attr.font,80);
   zdialog_fetch(zd,"fontsize",attr.size);
   zdialog_fetch(zd,"txangle",attr.angle);
   zdialog_fetch(zd,"txcolor",attr.color[0],20);
   zdialog_fetch(zd,"bgcolor",attr.color[1],20);
   zdialog_fetch(zd,"tocolor",attr.color[2],20);
   zdialog_fetch(zd,"shcolor",attr.color[3],20);
   zdialog_fetch(zd,"txtransp",attr.transp[0]);
   zdialog_fetch(zd,"bgtransp",attr.transp[1]);
   zdialog_fetch(zd,"totransp",attr.transp[2]);
   zdialog_fetch(zd,"shtransp",attr.transp[3]);
   zdialog_fetch(zd,"towidth",attr.towidth);
   zdialog_fetch(zd,"shwidth",attr.shwidth);
   zdialog_fetch(zd,"shangle",attr.shangle);

   repl_Nstrs(attr.text,text2,"\n","\\n",null);                                  //  replace newlines with "\n"

   fprintf(fid,"text string: %s\n",text2);

   fprintf(fid,"font: %s  size: %d \n",attr.font, attr.size);

   fprintf(fid,"attributes: %.4f  %s %s %s %s  %d %d %d %d  %d %d %d \n",
            attr.angle, attr.color[0], attr.color[1], attr.color[2], attr.color[3],
            attr.transp[0], attr.transp[1], attr.transp[2], attr.transp[3],
            attr.towidth, attr.shwidth, attr.shangle);

   fclose(fid);
   return;
}


/********************************************************************************/

/***

   Create a graphic image with text, any color, any font, any angle.
   Add outline and shadow colors. Apply transparencies.

   struct textattr_t                                     //  attributes for gentext() function
      char     text[1000];                               //  text to generate image from
      char     font[80];                                 //  font name
      int      size;                                     //  font size
      int      tww, thh;                                 //  generated image size, unrotated
      float    angle;                                    //  text angle, degrees
      float    sinT, cosT;                               //  trig funcs for text angle
      char     color[4][20];                             //  text, backing, outline, shadow "R|G|B"
      int      transp[4];                                //  corresponding transparencies 0-255
      int      towidth;                                  //  outline width, pixels
      int      shwidth;                                  //  shadow width
      int      shangle;                                  //  shadow angle -180...+180
      PXB      *pxb_text;                                //  image with text/outline/shadow

***/

int gentext(textattr_t *attr)
{
   PXB * gentext_outline(textattr_t *, PXB *);
   PXB * gentext_shadow(textattr_t *, PXB *);

   PangoFontDescription    *pfont;
   PangoLayout             *playout;
   cairo_surface_t         *surface;
   cairo_t                 *cr;

   float          angle;
   int            txred, txgreen, txblue;
   int            bgred, bggreen, bgblue;
   int            tored, togreen, toblue;
   int            shred, shgreen, shblue;
   float          txtransp, bgtransp, totransp, shtransp;

   PXB            *pxb_temp1, *pxb_temp2;
   uint8          *cairo_data, *cpix;
   uint8          *pix1, *pix2;
   int            px, py, ww, hh;
   char           font[100];                                                     //  font name and size
   int            red, green, blue;
   float          txpart, topart, shpart, bgpart;

   if (! *attr->text) strcpy(attr->text,"null");

   if (attr->pxb_text) PXB_free(attr->pxb_text);

   snprintf(font,100,"%s %d",attr->font,attr->size);                             //  font name and size together
   angle = attr->angle;                                                          //  text angle, degrees
   attr->sinT = sin(angle/57.296);                                               //  trig funcs for text angle
   attr->cosT = cos(angle/57.296);
   txred = atoi(strField(attr->color[0],'|',1));                                 //  get text foreground color
   txgreen = atoi(strField(attr->color[0],'|',2));
   txblue = atoi(strField(attr->color[0],'|',3));
   bgred = atoi(strField(attr->color[1],'|',1));                                 //  get text background color
   bggreen = atoi(strField(attr->color[1],'|',2));
   bgblue = atoi(strField(attr->color[1],'|',3));
   tored = atoi(strField(attr->color[2],'|',1));                                 //  get text outline color
   togreen = atoi(strField(attr->color[2],'|',2));
   toblue = atoi(strField(attr->color[2],'|',3));
   shred = atoi(strField(attr->color[3],'|',1));                                 //  get text shadow color
   shgreen = atoi(strField(attr->color[3],'|',2));
   shblue = atoi(strField(attr->color[3],'|',3));
   txtransp = 0.01 * attr->transp[0];                                            //  get transparencies
   bgtransp = 0.01 * attr->transp[1];                                            //  text, background, outline, shadow
   totransp = 0.01 * attr->transp[2];
   shtransp = 0.01 * attr->transp[3];

   pfont = pango_font_description_from_string(font);                             //  make layout with text
   playout = gtk_widget_create_pango_layout(Fdrawin,null);                       //  Fdrawin instead of Cdrawin
   if (! playout) zappcrash("gentext(): cannot create pango layout");
   pango_layout_set_font_description(playout,pfont);
   pango_layout_set_text(playout,attr->text,-1);

   pango_layout_get_pixel_size(playout,&ww,&hh);
   ww += 2 + 0.2 * attr->size;                                                   //  compensate bad font metrics
   hh += 2 + 0.1 * attr->size;
   attr->tww = ww;                                                               //  save text image size before rotate
   attr->thh = hh;

   surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24,ww,hh);               //  cairo output image
   cr = cairo_create(surface);
   pango_cairo_show_layout(cr,playout);                                          //  write text layout to image

   cairo_data = cairo_image_surface_get_data(surface);                           //  get text image pixels

   pxb_temp1 = PXB_make(ww+5,hh,3);                                              //  create PXB                         18.01

   for (py = 0; py < hh; py++)                                                   //  copy text image to PXB
   for (px = 0; px < ww; px++)
   {
      cpix = cairo_data + 4 * (ww * py + px);                                    //  pango output is monocolor
      pix2 = PXBpix(pxb_temp1,px+4,py);                                          //  small pad before text              18.01
      pix2[0] = cpix[3];                                                         //  use red [0] for text intensity
      pix2[1] = pix2[2] = 0;
   }

   pango_font_description_free(pfont);                                           //  free resources
   g_object_unref(playout);
   cairo_destroy(cr);
   cairo_surface_destroy(surface);

   pxb_temp2 = gentext_outline(attr,pxb_temp1);                                  //  add text outline if any
   if (pxb_temp2) {                                                              //    using green [1] for outline intensity
      PXB_free(pxb_temp1);
      pxb_temp1 = pxb_temp2;
   }

   pxb_temp2 = gentext_shadow(attr,pxb_temp1);                                   //  add text shadow color if any
   if (pxb_temp2) {                                                              //    using blue [2] for shadow intensity
      PXB_free(pxb_temp1);
      pxb_temp1 = pxb_temp2;
   }

   if (fabsf(angle) > 0.1) {                                                     //  rotate text if wanted
      pxb_temp2 = PXB_rotate(pxb_temp1,angle);
      PXB_free(pxb_temp1);
      pxb_temp1 = pxb_temp2;
   }

   ww = pxb_temp1->ww;                                                           //  text image input PXB
   hh = pxb_temp1->hh;
   pxb_temp2 = PXB_make(ww,hh,4);                                                //  text image output PXB

   for (py = 0; py < hh; py++)                                                   //  loop all pixels in text image
   for (px = 0; px < ww; px++)
   {
      pix1 = PXBpix(pxb_temp1,px,py);                                            //  copy-from pixel (text + outline + shadow)
      pix2 = PXBpix(pxb_temp2,px,py);                                            //  copy-to pixel

      txpart = pix1[0] / 256.0;                                                  //  text opacity 0-1
      topart = pix1[1] / 256.0;                                                  //  outline
      shpart = pix1[2] / 256.0;                                                  //  shadow
      bgpart = (1.0 - txpart - topart - shpart);                                 //  background 
      txpart = txpart * (1.0 - txtransp);                                        //  reduce for transparencies
      topart = topart * (1.0 - totransp);
      shpart = shpart * (1.0 - shtransp);
      bgpart = bgpart * (1.0 - bgtransp);

      red = txpart * txred + topart * tored + shpart * shred + bgpart * bgred;
      green = txpart * txgreen + topart * togreen + shpart * shgreen + bgpart * bggreen;
      blue = txpart * txblue + topart * toblue + shpart * shblue + bgpart * bgblue;

      pix2[0] = red;                                                             //  output total red, green, blue
      pix2[1] = green;
      pix2[2] = blue;

      pix2[3] = 255 * (1.0 - txpart - topart - shpart - bgpart);                 //  image part visible through text
   }

   PXB_free(pxb_temp1);
   attr->pxb_text = pxb_temp2;
   return 0;
}


//  add an outline color to the text character edges
//  red color [0] is original monocolor text
//  use green color [1] for added outline

PXB * gentext_outline(textattr_t *attr, PXB *pxb1)
{
   PXB         *pxb2;
   int         toww, ww1, hh1, ww2, hh2;
   int         px, py, dx, dy;
   uint8       *pix1, *pix2;
   float       theta;

   toww = attr->towidth;                                                         //  text outline color width
   if (toww == 0) return 0;                                                      //  zero

   ww1 = pxb1->ww;                                                               //  input PXB dimensions
   hh1 = pxb1->hh;
   ww2 = ww1 + toww * 2;                                                         //  add margins for outline width
   hh2 = hh1 + toww * 2;
   pxb2 = PXB_make(ww2,hh2,3);                                                   //  output PXB

   for (py = 0; py < hh1; py++)                                                  //  copy text pixels to outline pixels
   for (px = 0; px < ww1; px++)                                                  //    displaced by outline width
   {
      pix1 = PXBpix(pxb1,px,py);
      pix2 = PXBpix(pxb2,px+toww,py+toww);
      pix2[0] = pix1[0];
   }

   theta = 0.7 / toww;
   for (theta = 0; theta < 6.3; theta += 0.7/toww)                               //  displace outline pixels in all directions
   {
      dx = roundf(toww * sinf(theta));
      dy = roundf(toww * cosf(theta));

      for (py = 0; py < hh1; py++)
      for (px = 0; px < ww1; px++)
      {
         pix1 = PXBpix(pxb1,px,py);
         pix2 = PXBpix(pxb2,px+toww+dx,py+toww+dy);
         if (pix2[1] < pix1[0] - pix2[0])                                        //  compare text to outline brightness
            pix2[1] = pix1[0] - pix2[0];                                         //  brighter part is outline pixel
      }
   }

   return pxb2;                                                                  //  pix2[0] / pix2[1] = text / outline
}


//  add a shadow to the text character edges
//  red color [0] is original monocolor text intensity
//  green color [1] is added outline if any
//  use blue color [2] for added shadow

PXB * gentext_shadow(textattr_t *attr, PXB *pxb1)
{
   PXB         *pxb2;
   int         shww, ww1, hh1, ww2, hh2;
   int         px, py, dx, dy;
   uint8       *pix1, *pix2;
   float       theta;

   shww = attr->shwidth;                                                         //  text shadow width
   if (shww == 0) return 0;                                                      //  zero

   ww1 = pxb1->ww;                                                               //  input PXB dimensions
   hh1 = pxb1->hh;
   ww2 = ww1 + shww * 2;                                                         //  add margins for shadow width
   hh2 = hh1 + shww * 2;
   pxb2 = PXB_make(ww2,hh2,3);                                                   //  output PXB

   for (py = 0; py < hh1; py++)                                                  //  copy text pixels to shadow pixels
   for (px = 0; px < ww1; px++)                                                  //    displaced by shadow width
   {
      pix1 = PXBpix(pxb1,px,py);
      pix2 = PXBpix(pxb2,px+shww,py+shww);
      pix2[0] = pix1[0];
      pix2[1] = pix1[1];
   }

   theta = (90 - attr->shangle) / 57.3;                                          //  degrees to radians, 0 = to the right
   dx = roundf(shww * sinf(theta));
   dy = roundf(shww * cosf(theta));

   for (py = 0; py < hh1; py++)                                                  //  displace text by shadow width
   for (px = 0; px < ww1; px++)
   {
      pix1 = PXBpix(pxb1,px,py);
      pix2 = PXBpix(pxb2,px+shww+dx,py+shww+dy);
      if (pix2[2] < pix1[0] + pix1[1] - pix2[0] - pix2[1])                       //  compare text+outline to shadow pixels
         pix2[2] = pix1[0] + pix1[1] - pix2[0] - pix2[1];                        //  brighter part is shadow pixel
   }

   return pxb2;                                                                  //  pix2[0] / pix2[1] / pix2[2]
}                                                                                //    = text / outline / shadow brightness


/********************************************************************************/

//  write a line or arrow on top of the image

namespace addlines
{
   lineattr_t  attr;                                                             //  line/arrow attributes and image

   int      mpx, mpy;                                                            //  mouse position on image
   int      linepresent;                                                         //  flag, line present on image
   int      orgx1, orgy1, ww1, hh1;                                              //  old line image overlap rectangle
   int      orgx2, orgy2, ww2, hh2;                                              //  new overlap rectangle
   zdialog  *zd;

   int   dialog_event(zdialog *zd, cchar *event);                                //  dialog event function
   void  mousefunc();                                                            //  mouse event function
   void  write(int mode);                                                        //  write line on image

   editfunc    EFaddline;
}


void m_add_lines(GtkWidget *, cchar *menu)
{
   using namespace addlines;

   cchar    *intro = E2X("Enter line or arrow properties in dialog, \n"
                         "click/drag on image, right click to remove");

   F1_help_topic = "add_lines";                                                  //  user guide topic

   EFaddline.menufunc = m_add_lines;
   EFaddline.funcname = "add_lines";
   EFaddline.Farea = 1;                                                          //  select area ignored
   EFaddline.mousefunc = mousefunc;                                              //  18.01
   if (! edit_setup(EFaddline)) return;                                          //  setup edit

/***
       ___________________________________________________
      |         Add lines or arrows to an image           |
      |                                                   |
      |  Enter line or arrow properties in dialog,        |
      |  click/drag on image, right click to remove.      |
      |                                                   |
      |  Use settings file  [Open] [Save]                 |
      |                                                   |
      |  Line length [____]  width [____]                 |
      |  Arrow head  [x] left   [x] right                 |
      |                                                   |
      |            color   transp.   width     angle      |
      |  line     [#####] [_______]           [______]    |                      lncolor lntransp lnangle
      |  backing  [#####] [_______]                       |                      bgcolor bgtransp
      |  outline  [#####] [_______] [_______]             |                      tocolor totransp towidth
      |  shadow   [#####] [_______] [_______] [______]    |                      shcolor shtransp shwidth shangle
      |                                                   |
      |                           [Apply] [Done] [Cancel] |
      |___________________________________________________|

***/

   zd = zdialog_new(E2X("Add lines or arrows to an image"),Mwin,Bapply,Bdone,Bcancel,null);
   EFaddline.zd = zd;
   EFaddline.mousefunc = mousefunc;
   EFaddline.menufunc = m_add_lines;                                             //  allow restart

   zdialog_add_widget(zd,"label","intro","dialog",intro,"space=3");

   zdialog_add_widget(zd,"hbox","hbfile","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labfile","hbfile",E2X("Use settings file"),"space=3");
   zdialog_add_widget(zd,"button",Bopen,"hbfile",Bopen);
   zdialog_add_widget(zd,"button",Bsave,"hbfile",Bsave);

   zdialog_add_widget(zd,"hbox","hbline","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","lablength","hbline",E2X("Line length"),"space=5");
   zdialog_add_widget(zd,"zspin","length","hbline","2|9999|1|20");
   zdialog_add_widget(zd,"label","space","hbline",0,"space=10");
   zdialog_add_widget(zd,"label","labwidth","hbline",Bwidth,"space=5");
   zdialog_add_widget(zd,"zspin","width","hbline","1|99|1|2");

   zdialog_add_widget(zd,"hbox","hbarrow","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labarrow","hbarrow",E2X("Arrow head"),"space=5");
   zdialog_add_widget(zd,"check","larrow","hbarrow",Bleft);
   zdialog_add_widget(zd,"label","space","hbarrow",0,"space=10");
   zdialog_add_widget(zd,"check","rarrow","hbarrow",Bright);

   zdialog_add_widget(zd,"hbox","hbcol","dialog");
   zdialog_add_widget(zd,"vbox","vbcol1","hbcol",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vbcol2","hbcol",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vbcol3","hbcol",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vbcol4","hbcol",0,"homog|space=2");
   zdialog_add_widget(zd,"vbox","vbcol5","hbcol",0,"homog|space=2");

   zdialog_add_widget(zd,"label","space","vbcol1");
   zdialog_add_widget(zd,"label","labline","vbcol1",E2X("line"));
   zdialog_add_widget(zd,"label","labback","vbcol1",E2X("backing"));
   zdialog_add_widget(zd,"label","laboutln","vbcol1",E2X("outline"));
   zdialog_add_widget(zd,"label","labshadow","vbcol1",E2X("shadow"));

   zdialog_add_widget(zd,"label","labcol","vbcol2",Bcolor);
   zdialog_add_widget(zd,"colorbutt","lncolor","vbcol2","0|0|0");
   zdialog_add_widget(zd,"colorbutt","bgcolor","vbcol2","255|255|255");
   zdialog_add_widget(zd,"colorbutt","tocolor","vbcol2","255|0|0");
   zdialog_add_widget(zd,"colorbutt","shcolor","vbcol2","255|0|0");

   zdialog_add_widget(zd,"label","labcol","vbcol3","Transp.");
   zdialog_add_widget(zd,"zspin","lntransp","vbcol3","0|100|1|0");
   zdialog_add_widget(zd,"zspin","bgtransp","vbcol3","0|100|1|0");
   zdialog_add_widget(zd,"zspin","totransp","vbcol3","0|100|1|0");
   zdialog_add_widget(zd,"zspin","shtransp","vbcol3","0|100|1|0");

   zdialog_add_widget(zd,"label","labw","vbcol4",Bwidth);
   zdialog_add_widget(zd,"label","space","vbcol4");
   zdialog_add_widget(zd,"label","space","vbcol4");
   zdialog_add_widget(zd,"zspin","towidth","vbcol4","0|30|1|0");
   zdialog_add_widget(zd,"zspin","shwidth","vbcol4","0|50|1|0");

   zdialog_add_widget(zd,"label","labw","vbcol5",Bangle);
   zdialog_add_widget(zd,"zspin","lnangle","vbcol5","-360|360|0.1|0");
   zdialog_add_widget(zd,"label","space","vbcol5");
   zdialog_add_widget(zd,"label","space","vbcol5");
   zdialog_add_widget(zd,"zspin","shangle","vbcol5","-360|360|1|0");

   zdialog_add_ttip(zd,Bapply,E2X("fix line/arrow in layout \n start new line/arrow"));

   zdialog_restore_inputs(zd);                                                   //  restore prior inputs

   memset(&attr,0,sizeof(attr));

   zdialog_fetch(zd,"length",attr.length);                                       //  get defaults or prior inputs
   zdialog_fetch(zd,"width",attr.width);
   zdialog_fetch(zd,"larrow",attr.larrow);
   zdialog_fetch(zd,"rarrow",attr.rarrow);
   zdialog_fetch(zd,"lnangle",attr.angle);
   zdialog_fetch(zd,"lncolor",attr.color[0],20);
   zdialog_fetch(zd,"bgcolor",attr.color[1],20);
   zdialog_fetch(zd,"tocolor",attr.color[2],20);
   zdialog_fetch(zd,"shcolor",attr.color[3],20);
   zdialog_fetch(zd,"lntransp",attr.transp[0]);
   zdialog_fetch(zd,"bgtransp",attr.transp[1]);
   zdialog_fetch(zd,"totransp",attr.transp[2]);
   zdialog_fetch(zd,"shtransp",attr.transp[3]);
   zdialog_fetch(zd,"towidth",attr.towidth);
   zdialog_fetch(zd,"shwidth",attr.shwidth);
   zdialog_fetch(zd,"shangle",attr.shangle);

   genline(&attr);                                                               //  generate initial line

   takeMouse(mousefunc,dragcursor);                                              //  connect mouse function
   linepresent = 0;                                                              //  no line on image yet
   mpx = mpy = -1;                                                               //  no position defined yet

   zdialog_run(zd,dialog_event,"save");                                          //  run dialog, parallel
   return;
}


//  dialog event and completion callback function

int addlines::dialog_event(zdialog *zd, cchar *event)
{
   using namespace addlines;

   if (strmatch(event,"done")) zd->zstat = 2;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  cancel

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  Apply, commit present line to image
         write(1);
         linepresent = 0;                                                        //  (no old line to erase)
         mpx = mpy = -1;
         zd->zstat = 0;
         edit_done(0);                                                           //  finish
         m_add_lines(0,0);                                                       //  start again
         return 1;
      }

      if (zd->zstat == 2 && CEF->Fmods) edit_done(0);                            //  Done, complete pending edit
      else edit_cancel(0);                                                       //  Cancel or kill
      return 1;
   }

   if (strmatch(event,"focus")) {                                                //  toggle mouse capture
      takeMouse(mousefunc,dragcursor);                                           //  connect mouse function
      return 1;
   }
   
   if (strmatch(event,Bopen))                                                    //  load zdialog fields from a file
   {
      load_line(zd);
      zdialog_fetch(zd,"length",attr.length);
      zdialog_fetch(zd,"width",attr.width);
      zdialog_fetch(zd,"larrow",attr.larrow);
      zdialog_fetch(zd,"rarrow",attr.rarrow);
      zdialog_fetch(zd,"lncolor",attr.color[0],20);
      zdialog_fetch(zd,"lntransp",attr.transp[0]);
      zdialog_fetch(zd,"lnangle",attr.angle);
      zdialog_fetch(zd,"bgcolor",attr.color[1],20);
      zdialog_fetch(zd,"bgtransp",attr.transp[1]);
      zdialog_fetch(zd,"tocolor",attr.color[2],20);
      zdialog_fetch(zd,"totransp",attr.transp[2]);
      zdialog_fetch(zd,"towidth",attr.towidth);
      zdialog_fetch(zd,"shcolor",attr.color[3],20);
      zdialog_fetch(zd,"shtransp",attr.transp[3]);
      zdialog_fetch(zd,"shwidth",attr.shwidth);
      zdialog_fetch(zd,"shangle",attr.shangle);
   }

   if (strmatch(event,Bsave)) {                                                  //  save zdialog fields to file
      save_line(zd);
      return 1;
   }

   if (strmatch(event,"length"))                                                 //  line length
      zdialog_fetch(zd,"length",attr.length);

   if (strmatch(event,"width"))                                                  //  line width
      zdialog_fetch(zd,"width",attr.width);

   if (strmatch(event,"larrow"))                                                 //  left arrow head
      zdialog_fetch(zd,"larrow",attr.larrow);

   if (strmatch(event,"rarrow"))                                                 //  right arrow head
      zdialog_fetch(zd,"rarrow",attr.rarrow);

   if (strmatch(event,"lnangle"))                                                //  line angle
      zdialog_fetch(zd,"lnangle",attr.angle);

   if (strmatch(event,"lncolor"))                                                //  foreground (line) color
      zdialog_fetch(zd,"lncolor",attr.color[0],20);

   if (strmatch(event,"bgcolor"))                                                //  background color
      zdialog_fetch(zd,"bgcolor",attr.color[1],20);

   if (strmatch(event,"tocolor"))                                                //  line outline color
      zdialog_fetch(zd,"tocolor",attr.color[2],20);

   if (strmatch(event,"shcolor"))                                                //  line shadow color
      zdialog_fetch(zd,"shcolor",attr.color[3],20);

   if (strmatch(event,"lntransp"))                                               //  foreground transparency
      zdialog_fetch(zd,"lntransp",attr.transp[0]);

   if (strmatch(event,"bgtransp"))                                               //  background transparency
      zdialog_fetch(zd,"bgtransp",attr.transp[1]);

   if (strmatch(event,"totransp"))                                               //  line outline transparency
      zdialog_fetch(zd,"totransp",attr.transp[2]);

   if (strmatch(event,"shtransp"))                                               //  line shadow transparency
      zdialog_fetch(zd,"shtransp",attr.transp[3]);

   if (strmatch(event,"towidth"))                                                //  line outline width
      zdialog_fetch(zd,"towidth",attr.towidth);

   if (strmatch(event,"shwidth"))                                                //  line shadow width
      zdialog_fetch(zd,"shwidth",attr.shwidth);

   if (strmatch(event,"shangle"))                                                //  line shadow angle
      zdialog_fetch(zd,"shangle",attr.shangle);

   genline(&attr);                                                               //  build line image from attributes
   write(1);                                                                     //  write on image
   zmainloop();                                                                  //  18.07
   return 1;
}


//  mouse function, set new position for line on image

void addlines::mousefunc()
{
   using namespace addlines;

   float    ax1, ay1, ax2, ay2;                                                  //  line/arrow end points
   float    angle, rad, l2;
   float    amx, amy;
   float    d1, d2, sinv;

   if (RMclick) {                                                                //  right mouse click
      write(2);                                                                  //  erase old line, if any
      mpx = mpy = -1;
      LMclick = RMclick = Mxdrag = Mydrag = 0;
      return;
   }

   if (LMclick + Mxdrag + Mydrag == 0) return;

   if (LMclick) {
      mpx = Mxclick;                                                             //  new line position on image
      mpy = Myclick;
   }
   else {
      mpx = Mxdrag;
      mpy = Mydrag;
   }

   LMclick = RMclick = Mxdrag = Mydrag = 0;

   if (! linepresent) {
      orgx2 = mpx;
      orgy2 = mpy;
      write(1);
      return;
   }

   //  move the closest line endpoint to the mouse position and leave the other endpoint fixed

   angle = attr.angle;
   rad = -angle / 57.296;
   l2 = attr.length / 2.0;

   ww2 = attr.pxb_line->ww;                                                      //  line image buffer
   hh2 = attr.pxb_line->hh;

   amx = ww2 / 2.0;                                                              //  line midpoint within line image
   amy = hh2 / 2.0;
   
   ax1 = amx - l2 * cosf(rad) + 0.5;                                             //  line end points
   ay1 = amy + l2 * sinf(rad) + 0.5;
   ax2 = amx + l2 * cosf(rad) + 0.5;
   ay2 = amy - l2 * sinf(rad) + 0.5;
   
   d1 = (mpx-ax1-orgx1) * (mpx-ax1-orgx1) + (mpy-ay1-orgy1) * (mpy-ay1-orgy1);
   d2 = (mpx-ax2-orgx1) * (mpx-ax2-orgx1) + (mpy-ay2-orgy1) * (mpy-ay2-orgy1);

   d1 = sqrtf(d1);                                                               //  mouse - end point distance
   d2 = sqrtf(d2);

   if (d1 < d2) {                                                                //  move ax1/ay1 end to mouse
      ax2 += orgx1;
      ay2 += orgy1;
      ax1 = mpx;
      ay1 = mpy;
      attr.length = d2 + 0.5;
      sinv = (ay1-ay2) / d2;
      if (sinv > 1.0) sinv = 1.0;
      if (sinv < -1.0) sinv = -1.0;
      rad = asinf(sinv);
      angle = -57.296 * rad;
      if (mpx > ax2) angle = -180 - angle;
   }

   else {                                                                        //  move ax2/ay2 end to mouse
      ax1 += orgx1;
      ay1 += orgy1;
      ax2 = mpx;
      ay2 = mpy;
      attr.length = d1 + 0.5;
      sinv = (ay1-ay2) / d1;
      if (sinv > 1.0) sinv = 1.0;
      if (sinv < -1.0) sinv = -1.0;
      rad = asinf(sinv);
      angle = -57.296 * rad;
      if (mpx < ax1) angle = -180 - angle;
   }

   if (angle < -180) angle += 360;
   if (angle > 180) angle -= 360;
   attr.angle = angle;
   genline(&attr);
   ww2 = attr.pxb_line->ww;
   hh2 = attr.pxb_line->hh;
   amx = (ax1 + ax2) / 2.0;
   amy = (ay1 + ay2) / 2.0;
   orgx2 = amx - ww2 / 2.0;
   orgy2 = amy - hh2 / 2.0;
   write(1);

   zdialog_stuff(zd,"lnangle",attr.angle);
   zdialog_stuff(zd,"length",attr.length);
   return;
}


//  write line on image at designated location
//  mode: 1  erase old and write to new position
//        2  erase old and write nothing

void addlines::write(int mode)
{
   using namespace addlines;

   float       *pix1, *pix3;
   uint8       *pixL;
   int         px1, py1, px3, py3, done;
   float       e3part, Ot, Om, Ob;
   int         nc = E1pxm->nc, pcc = nc * sizeof(float);

   cairo_t *cr = draw_context_create(gdkwin,draw_context);

   if (linepresent)
   {
      for (py3 = orgy1; py3 < orgy1 + hh1; py3++)                                //  erase prior line image
      for (px3 = orgx1; px3 < orgx1 + ww1; px3++)                                //  replace E3 pixels with E1 pixels
      {                                                                          //    in prior overlap rectangle
         if (px3 < 0 || px3 >= E3pxm->ww) continue;
         if (py3 < 0 || py3 >= E3pxm->hh) continue;
         pix1 = PXMpix(E1pxm,px3,py3);
         pix3 = PXMpix(E3pxm,px3,py3);
         memcpy(pix3,pix1,pcc);
      }
   }

   done = 0;
   if (mode == 2) done = 1;                                                      //  erase only
   if (mpx < 0 && mpy < 0) done = 1;                                             //  no position defined

   if (done) {
      if (linepresent) {
         Fpaint3(orgx1,orgy1,ww1,hh1,cr);                                        //  update window to erase old line
         linepresent = 0;                                                        //  mark no line present
      }

      draw_context_destroy(draw_context);
      return;
   }

   ww2 = attr.pxb_line->ww;                                                      //  line image buffer
   hh2 = attr.pxb_line->hh;

   for (py1 = 0; py1 < hh2; py1++)                                               //  loop all pixels in line image
   for (px1 = 0; px1 < ww2; px1++)
   {
      px3 = orgx2 + px1;                                                         //  copy-to image3 pixel
      py3 = orgy2 + py1;

      if (px3 < 0 || px3 >= E3pxm->ww) continue;                                 //  omit parts beyond edges
      if (py3 < 0 || py3 >= E3pxm->hh) continue;

      pixL = PXBpix(attr.pxb_line,px1,py1);                                      //  copy-from line pixel
      pix3 = PXMpix(E3pxm,px3,py3);                                              //  copy-to image pixel

      e3part = pixL[3] / 256.0;                                                  //  line image transparency

      pix3[0] = pixL[0] + e3part * pix3[0];                                      //  combine line part + image part
      pix3[1] = pixL[1] + e3part * pix3[1];
      pix3[2] = pixL[2] + e3part * pix3[2];

      if (nc > 3) {
         Ot = (1.0 - e3part);                                                    //  line opacity
         Om = pix3[3] / 256.0;                                                   //  image opacity
         Ob = 1.0 - (1.0 - Ot) * (1.0 - Om);                                     //  combined opacity
         pix3[3] = 255.0 * Ob;
      }
   }

   if (linepresent) {
      Fpaint3(orgx1,orgy1,ww1,hh1,cr);                                           //  update window to erase old line
      linepresent = 0;
   }
                                                                                 //  (updates together to reduce flicker)
   Fpaint3(orgx2,orgy2,ww2,hh2,cr);                                              //  update window for new line

   draw_context_destroy(draw_context);

   CEF->Fmods++;
   CEF->Fsaved = 0;
   linepresent = 1;                                                              //  mark line is present

   orgx1 = orgx2;                                                                //  remember overlap rectangle
   orgy1 = orgy2;                                                                //    for next call
   ww1 = ww2;
   hh1 = hh2;

   return;
}


//  load line attributes from a file

void load_line(zdialog *zd)
{
   FILE        *fid;
   int         err, nn;
   char        *pp, *file, buff[200];
   cchar       *dialogtitle = "load text data from a file";
   lineattr_t  attr;

   file = zgetfile(dialogtitle,MWIN,"file",addline_folder);                      //  get input file from user
   if (! file) return;

   fid = fopen(file,"r");                                                        //  open for read
   if (! fid) {
      zmessageACK(Mwin,strerror(errno));
      zfree(file);
      return;
   }

   pp = fgets_trim(buff,200,fid);                                                //  read line attributes
   if (! pp) goto badfile;
   nn = sscanf(pp,"line attributes: %d %d %d %d %f  %s %s %s %s  %d %d %d %d  %d %d %d",
            &attr.length, &attr.width, &attr.larrow, &attr.rarrow, &attr.angle, 
            attr.color[0], attr.color[1], attr.color[2], attr.color[3],
            &attr.transp[0], &attr.transp[1], &attr.transp[2], &attr.transp[3],
            &attr.towidth, &attr.shwidth, &attr.shangle);
   if (nn != 16) goto badfile;

   err = fclose(fid);
   if (err) {
      zmessageACK(Mwin,strerror(errno));
      zfree(file);
      return;
   }

   zdialog_stuff(zd,"length",attr.length);                                       //  stuff line attributes into zdialog
   zdialog_stuff(zd,"width",attr.width);
   zdialog_stuff(zd,"larrow",attr.larrow);
   zdialog_stuff(zd,"rarrow",attr.rarrow);
   zdialog_stuff(zd,"lnangle",attr.angle);
   zdialog_stuff(zd,"lncolor",attr.color[0]);
   zdialog_stuff(zd,"bgcolor",attr.color[1]);
   zdialog_stuff(zd,"tocolor",attr.color[2]);
   zdialog_stuff(zd,"shcolor",attr.color[3]);
   zdialog_stuff(zd,"lntransp",attr.transp[0]);
   zdialog_stuff(zd,"bgtransp",attr.transp[1]);
   zdialog_stuff(zd,"totransp",attr.transp[2]);
   zdialog_stuff(zd,"shtransp",attr.transp[3]);
   zdialog_stuff(zd,"towidth",attr.towidth);
   zdialog_stuff(zd,"shwidth",attr.shwidth);
   zdialog_stuff(zd,"shangle",attr.shangle);
   return;

badfile:
   fclose(fid);
   zmessageACK(Mwin,E2X("text file is defective"));
   printz("buff: %s\n",buff);
}


//  save line attributes to a file

void save_line(zdialog *zd)
{
   cchar       *dialogtitle = "save text data to a file";
   FILE        *fid;
   char        *file;
   lineattr_t  attr;

   file = zgetfile(dialogtitle,MWIN,"save",addline_folder);                      //  get output file from user
   if (! file) return;

   fid = fopen(file,"w");                                                        //  open for write
   if (! fid) {
      zmessageACK(Mwin,strerror(errno));
      zfree(file);
      return;
   }

   zdialog_fetch(zd,"length",attr.length);                                       //  get line attributes from zdialog
   zdialog_fetch(zd,"width",attr.width);
   zdialog_fetch(zd,"larrow",attr.larrow);
   zdialog_fetch(zd,"rarrow",attr.rarrow);
   zdialog_fetch(zd,"lnangle",attr.angle);
   zdialog_fetch(zd,"lncolor",attr.color[0],20);
   zdialog_fetch(zd,"bgcolor",attr.color[1],20);
   zdialog_fetch(zd,"tocolor",attr.color[2],20);
   zdialog_fetch(zd,"shcolor",attr.color[3],20);
   zdialog_fetch(zd,"lntransp",attr.transp[0]);
   zdialog_fetch(zd,"bgtransp",attr.transp[1]);
   zdialog_fetch(zd,"totransp",attr.transp[2]);
   zdialog_fetch(zd,"shtransp",attr.transp[3]);
   zdialog_fetch(zd,"towidth",attr.towidth);
   zdialog_fetch(zd,"shwidth",attr.shwidth);
   zdialog_fetch(zd,"shangle",attr.shangle);

   fprintf(fid,"line attributes: %d %d %d %d %.4f  %s %s %s %s  %d %d %d %d  %d %d %d \n",
            attr.length, attr.width, attr.larrow, attr.rarrow, attr.angle, 
            attr.color[0], attr.color[1], attr.color[2], attr.color[3],
            attr.transp[0], attr.transp[1], attr.transp[2], attr.transp[3],
            attr.towidth, attr.shwidth, attr.shangle);

   fclose(fid);
   return;
}


/********************************************************************************/

//  Create a graphic image of a line or arrow, any color,
//  any size, any angle. Add outline and shadow colors.

int genline(lineattr_t *attr)
{
   PXB * genline_outline(lineattr_t *, PXB *);
   PXB * genline_shadow(lineattr_t *, PXB *);

   cairo_surface_t         *surface;
   cairo_t                 *cr;

   float          angle;
   int            lnred, lngreen, lnblue;
   int            bgred, bggreen, bgblue;
   int            tored, togreen, toblue;
   int            shred, shgreen, shblue;
   float          lntransp, bgtransp, totransp, shtransp;

   PXB            *pxb_temp1, *pxb_temp2;
   uint8          *cairo_data, *cpix;
   uint8          *pix1, *pix2;
   float          length, width;
   int            px, py, ww, hh;
   int            red, green, blue;
   float          lnpart, topart, shpart, bgpart;

   if (attr->pxb_line) PXB_free(attr->pxb_line);

   angle = attr->angle;                                                          //  line angle, degrees
   attr->sinT = sin(angle/57.296);                                               //  trig funcs for line angle
   attr->cosT = cos(angle/57.296);
   lnred = atoi(strField(attr->color[0],'|',1));                                 //  get line foreground color
   lngreen = atoi(strField(attr->color[0],'|',2));
   lnblue = atoi(strField(attr->color[0],'|',3));
   bgred = atoi(strField(attr->color[1],'|',1));                                 //  get line background color
   bggreen = atoi(strField(attr->color[1],'|',2));
   bgblue = atoi(strField(attr->color[1],'|',3));
   tored = atoi(strField(attr->color[2],'|',1));                                 //  get line outline color
   togreen = atoi(strField(attr->color[2],'|',2));
   toblue = atoi(strField(attr->color[2],'|',3));
   shred = atoi(strField(attr->color[3],'|',1));                                 //  get line shadow color
   shgreen = atoi(strField(attr->color[3],'|',2));
   shblue = atoi(strField(attr->color[3],'|',3));
   lntransp = 0.01 * attr->transp[0];                                            //  get transparencies
   bgtransp = 0.01 * attr->transp[1];                                            //  line, background, outline, shadow
   totransp = 0.01 * attr->transp[2];
   shtransp = 0.01 * attr->transp[3];

   length = attr->length;                                                        //  line dimensions
   width = attr->width;

   ww = length + 20;                                                             //  create cairo surface
   hh = width + 20;                                                              //    with margins all around

   if (attr->larrow || attr->rarrow)                                             //  wider if arrow head used
      hh += width * 4;

   attr->lww = ww;
   attr->lhh = hh;

   surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24,ww,hh);
   cr = cairo_create(surface);

   cairo_set_antialias(cr,CAIRO_ANTIALIAS_BEST);
   cairo_set_line_width(cr,width);
   cairo_set_line_cap(cr,CAIRO_LINE_CAP_ROUND);

   cairo_move_to(cr, 10, hh/2.0);                                                //  draw line in middle of surface
   cairo_line_to(cr, length+10, hh/2.0);

   if (attr->larrow) {                                                           //  add arrow heads if req.
      cairo_move_to(cr, 10, hh/2.0);
      cairo_line_to(cr, 10 + 2 * width, hh/2.0 - 2 * width);
      cairo_move_to(cr, 10, hh/2.0);
      cairo_line_to(cr, 10 + 2 * width, hh/2.0 + 2 * width);
   }

   if (attr->rarrow) {
      cairo_move_to(cr, length+10, hh/2.0);
      cairo_line_to(cr, length+10 - 2 * width, hh/2.0 - 2 * width);
      cairo_move_to(cr, length+10, hh/2.0);
      cairo_line_to(cr, length+10 - 2 * width, hh/2.0 + 2 * width);
   }

   cairo_stroke(cr);

   cairo_data = cairo_image_surface_get_data(surface);                           //  cairo image pixels

   pxb_temp1 = PXB_make(ww,hh,3);                                                //  create PXB

   for (py = 0; py < hh; py++)                                                   //  copy image to PXB
   for (px = 0; px < ww; px++)
   {
      cpix = cairo_data + 4 * (ww * py + px);                                    //  pango output is monocolor
      pix2 = PXBpix(pxb_temp1,px,py);
      pix2[0] = cpix[3];                                                         //  use red [0] for line intensity
      pix2[1] = pix2[2] = 0;
   }

   cairo_destroy(cr);                                                            //  free resources
   cairo_surface_destroy(surface);

   pxb_temp2 = genline_outline(attr,pxb_temp1);                                  //  add line outline if any
   if (pxb_temp2) {                                                              //    using green [1] for outline intensity
      PXB_free(pxb_temp1);
      pxb_temp1 = pxb_temp2;
   }

   pxb_temp2 = genline_shadow(attr,pxb_temp1);                                   //  add line shadow color if any
   if (pxb_temp2) {                                                              //    using blue [2] for shadow intensity
      PXB_free(pxb_temp1);
      pxb_temp1 = pxb_temp2;
   }

   if (fabsf(angle) > 0.1) {                                                     //  rotate line if wanted
      pxb_temp2 = PXB_rotate(pxb_temp1,angle);
      PXB_free(pxb_temp1);
      pxb_temp1 = pxb_temp2;
   }

   ww = pxb_temp1->ww;                                                           //  line image input PXB
   hh = pxb_temp1->hh;
   pxb_temp2 = PXB_make(ww,hh,4);                                                //  line image output PXB

   for (py = 0; py < hh; py++)                                                   //  loop all pixels in line image
   for (px = 0; px < ww; px++)
   {
      pix1 = PXBpix(pxb_temp1,px,py);                                            //  copy-from pixel (line + outline + shadow)
      pix2 = PXBpix(pxb_temp2,px,py);                                            //  copy-to pixel

      lnpart = pix1[0] / 256.0;
      topart = pix1[1] / 256.0;
      shpart = pix1[2] / 256.0;
      bgpart = (1.0 - lnpart - topart - shpart);
      lnpart = lnpart * (1.0 - lntransp);
      topart = topart * (1.0 - totransp);
      shpart = shpart * (1.0 - shtransp);
      bgpart = bgpart * (1.0 - bgtransp);

      red = lnpart * lnred + topart * tored + shpart * shred + bgpart * bgred;
      green = lnpart * lngreen + topart * togreen + shpart * shgreen + bgpart * bggreen;
      blue = lnpart * lnblue + topart * toblue + shpart * shblue + bgpart * bgblue;

      pix2[0] = red;                                                             //  output total red, green blue
      pix2[1] = green;
      pix2[2] = blue;

      pix2[3] = 255 * (1.0 - lnpart - topart - shpart - bgpart);                 //  image part visible through line
   }

   PXB_free(pxb_temp1);
   attr->pxb_line = pxb_temp2;
   return 0;
}


//  add an outline color to the line edges
//  red color [0] is original monocolor line
//  use green color [1] for added outline

PXB * genline_outline(lineattr_t *attr, PXB *pxb1)
{
   PXB         *pxb2;
   int         toww, ww1, hh1, ww2, hh2;
   int         px, py, dx, dy;
   uint8       *pix1, *pix2;
   float       theta;

   toww = attr->towidth;                                                         //  line outline color width
   if (toww == 0) return 0;                                                      //  zero

   ww1 = pxb1->ww;                                                               //  input PXB dimensions
   hh1 = pxb1->hh;
   ww2 = ww1 + toww * 2;                                                         //  add margins for outline width
   hh2 = hh1 + toww * 2;
   pxb2 = PXB_make(ww2,hh2,3);                                                   //  output PXB

   for (py = 0; py < hh1; py++)                                                  //  copy line pixels to outline pixels
   for (px = 0; px < ww1; px++)                                                  //    displaced by outline width
   {
      pix1 = PXBpix(pxb1,px,py);
      pix2 = PXBpix(pxb2,px+toww,py+toww);
      pix2[0] = pix1[0];
   }

   theta = 0.7 / toww;
   for (theta = 0; theta < 6.3; theta += 0.7/toww)                               //  displace outline pixels in all directions
   {
      dx = roundf(toww * sinf(theta));
      dy = roundf(toww * cosf(theta));

      for (py = 0; py < hh1; py++)
      for (px = 0; px < ww1; px++)
      {
         pix1 = PXBpix(pxb1,px,py);
         pix2 = PXBpix(pxb2,px+toww+dx,py+toww+dy);
         if (pix2[1] < pix1[0] - pix2[0])                                        //  compare line to outline brightness
            pix2[1] = pix1[0] - pix2[0];                                         //  brighter part is outline pixel
      }
   }

   return pxb2;                                                                  //  pix2[0] / pix2[1] = line / outline
}


//  add a shadow to the line edges
//  red color [0] is original monocolor line intensity
//  green color [1] is added outline if any
//  use blue color [2] for added shadow

PXB * genline_shadow(lineattr_t *attr, PXB *pxb1)
{
   PXB         *pxb2;
   int         shww, ww1, hh1, ww2, hh2;
   int         px, py, dx, dy;
   uint8       *pix1, *pix2;
   float       theta;

   shww = attr->shwidth;                                                         //  line shadow width
   if (shww == 0) return 0;                                                      //  zero

   ww1 = pxb1->ww;                                                               //  input PXB dimensions
   hh1 = pxb1->hh;
   ww2 = ww1 + shww * 2;                                                         //  add margins for shadow width
   hh2 = hh1 + shww * 2;
   pxb2 = PXB_make(ww2,hh2,3);                                                   //  output PXB

   for (py = 0; py < hh1; py++)                                                  //  copy line pixels to shadow pixels
   for (px = 0; px < ww1; px++)                                                  //    displaced by shadow width
   {
      pix1 = PXBpix(pxb1,px,py);
      pix2 = PXBpix(pxb2,px+shww,py+shww);
      pix2[0] = pix1[0];
      pix2[1] = pix1[1];
   }

   theta = (90 - attr->shangle) / 57.3;                                          //  degrees to radians, 0 = to the right
   dx = roundf(shww * sinf(theta));
   dy = roundf(shww * cosf(theta));

   for (py = 0; py < hh1; py++)                                                  //  displace line by shadow width
   for (px = 0; px < ww1; px++)
   {
      pix1 = PXBpix(pxb1,px,py);
      pix2 = PXBpix(pxb2,px+shww+dx,py+shww+dy);
      if (pix2[2] < pix1[0] + pix1[1] - pix2[0] - pix2[1])                       //  compare line+outline to shadow pixels
         pix2[2] = pix1[0] + pix1[1] - pix2[0] - pix2[1];                        //  brighter part is shadow pixel
   }

   return pxb2;                                                                  //  pix2[0] / pix2[1] / pix2[2]
}                                                                                //    = line / outline / shadow brightness


/********************************************************************************/

//  Upright a rotated image: -90, +90, 180 degrees.
//  This is not an edit transaction: file is replaced and re-opened.
//  No loss of resolution.

void m_upright(GtkWidget *, cchar *menu)
{
   int upright_dialog_event(zdialog *zd, cchar *event);
   
   zdialog  *zd;

   F1_help_topic = "upright";

   if (checkpend("all")) return;
   
   if (clicked_file) {                                                           //  use clicked file if present
      if (! curr_file || ! strmatch(clicked_file,curr_file))                     //                             bugfix  19.5
         f_open(clicked_file,0,0,1,0);                                           //  avoid f_open() re-entry            18.01
      clicked_file = 0;
   }

   if (! curr_file) {
      if (zd_upright) zdialog_free(zd_upright);
      zd_upright = 0;
      return;
   }

/***
       _____________________________________________________
      |      Upright Image                                  |
      |                                                     |
      | Image File: xxxxxxxxxxxxxxx.jpg                     |
      |   _______   _______   _______   _______   _______   |
      |  |       | |       | |       | |       | |       |  |
      |  |upright| |  -90  | |  +90  | |  180  | |   X   |  |
      |  |_______| |_______| |_______| |_______| |_______|  |
      |_____________________________________________________|

***/

   if (! zd_upright) {
      zd = zdialog_new(E2X("Upright Image"),Mwin,null);
      zd_upright = zd;
      zdialog_add_widget(zd,"hbox","hbf","dialog",0,"space=5");
      zdialog_add_widget(zd,"label","labf","hbf",E2X("Image File:"),"space=3");
      zdialog_add_widget(zd,"label","file","hbf","filename.jpg","space=5");
      zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
      zdialog_add_widget(zd,"button","upright","hb1",E2X("Upright"),"space=5");
      zdialog_add_widget(zd,"imagebutt","-90","hb1","rotate-left.png","size=32|space=5");
      zdialog_add_widget(zd,"imagebutt","+90","hb1","rotate-right.png","size=32|space=5");
      zdialog_add_widget(zd,"imagebutt","180","hb1","rotate-180.png","size=32|space=5");
      zdialog_add_widget(zd,"imagebutt","cancel","hb1","cancel.png","size=32|space=5");
      zdialog_run(zd,upright_dialog_event,"save");
   }
   
   zd = zd_upright;

   char *pp = strrchr(curr_file,'/');                                            //  stuff file name in dialog
   if (pp) zdialog_stuff(zd,"file",pp+1);

   return;
}


//  dialog event and completion function

int upright_dialog_event(zdialog *zd, cchar *event)
{
   int      angle = 0;
   char     orientation = 0, *ppv[1];
   cchar    *exifkey[1] = { exif_orientation_key };
   cchar    *exifdata[1];
   
   if (! curr_file) return 1;

   if (zd->zstat) {                                                              //  [x] button
      zdialog_free(zd);
      zd_upright = 0;
      return 1;
   }

   if (strmatch(event,"cancel")) {                                               //  cancel button
      zdialog_free(zd);
      zd_upright = 0;
      return 1;
   }

   if (! strstr("upright -90 +90 180",event)) return 1;                          //  ignore other events
   
   if (strmatch(event,"upright"))                                                //  auto upright button
   {
      exif_get(curr_file,exifkey,ppv,1);                                         //  get EXIF: Orientation
      if (ppv[0]) {
         orientation = *ppv[0];                                                  //  single character
         zfree(ppv[0]);
      }
      if (orientation == '6') angle = +90;                                       //  rotate clockwise 90 deg.
      if (orientation == '8') angle = -90;                                       //  counterclockwise 90 deg.
      if (orientation == '3') angle = 180;                                       //  180 deg.
      if (! angle) {
         zmessageACK(Mwin,E2X("rotation unknown"));
         return 1;
      }
   }

   if (strmatch(event,"+90")) angle += 90;                                       //  other buttons
   if (strmatch(event,"-90")) angle -= 90;
   if (strmatch(event,"180")) angle += 180;

   if (! angle) return 1;                                                        //  ignore other events
   
   if (checkpend("all")) return 1;

   Fblock = 1;
   E0pxm = PXM_load(curr_file,1);                                                //  load poss. 16-bit image
   if (E0pxm) {
      E3pxm = PXM_rotate(E0pxm,angle);                                           //  rotate (threaded)
      PXM_free(E0pxm);
      E0pxm = E3pxm;
      E3pxm = 0;
   }

   f_save(curr_file,curr_file_type,curr_file_bpc,0,1);

   exifdata[0] = (char *) "";                                                    //  remove EXIF orientation data
   exif_put(curr_file,exifkey,exifdata,1);                                       //  (f_save() omits if no edit made)

   update_image_index(curr_file);                                                //  update index data

   Fblock = 0;

   f_open(curr_file);
   gallery(curr_file,"paint",0);                                                 //  repaint gallery
   return 1;
}


/********************************************************************************/

//  mirror an image horizontally or vertically

editfunc    EFmirror;

void m_mirror(GtkWidget *, cchar *menu)
{
   int mirror_dialog_event(zdialog *zd, cchar *event);

   F1_help_topic = "mirror";

   EFmirror.menuname = menu;
   EFmirror.menufunc = m_mirror;
   EFmirror.funcname = "mirror";
   EFmirror.Fscript = 1;                                                         //  allow scripting
   EFmirror.Frestart = 1;                                                        //  allow restart
   if (! edit_setup(EFmirror)) return;                                           //  setup edit

/***
          _____________________________
         |      Mirror Image           |
         |                             |
         | [] horizontal  [] vertical  |
         |                             |
         |             [Done] [Cancel] |
         |_____________________________|
         
***/

   zdialog *zd = zdialog_new(E2X("Mirror Image"),Mwin,Bdone,Bcancel,null);
   EFmirror.zd = zd;
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"check","horz","hb1",E2X("horizontal"),"space=5");
   zdialog_add_widget(zd,"check","vert","hb1",E2X("vertical"),"space=5");

   zdialog_run(zd,mirror_dialog_event,"save");                                   //  run dialog, parallel

   return;
}


//  dialog event and completion callback function

int mirror_dialog_event(zdialog *zd, cchar *event)
{
   int mirror_horz(void);
   int mirror_vert(void);
   
   int      nn;

   if (strmatch(event,"focus")) return 1;
   if (strmatch(event,"done")) zd->zstat = 1;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  cancel

   if (zd->zstat)                                                                //  dialog complete
   {
      if (zd->zstat == 1) edit_done(0);
      else edit_cancel(0);
      return 1;
   }
   
   if (strmatch(event,"apply")) {                                                //  from script
      zdialog_fetch(zd,"horz",nn);
      if (nn) mirror_horz();
      zdialog_fetch(zd,"vert",nn);
      if (nn) mirror_vert();
   }

   if (strmatch(event,"horz")) mirror_horz();
   if (strmatch(event,"vert")) mirror_vert();

   return 1;
}


int mirror_horz()
{
   int      px, py;
   float    *pix3, *pix9;
   int      nc = E3pxm->nc, pcc = nc * sizeof(float);

   E9pxm = PXM_copy(E3pxm);

   for (py = 0; py < E3pxm->hh; py++)
   for (px = 0; px < E3pxm->ww; px++)
   {
      pix3 = PXMpix(E3pxm,px,py);                                                //  image9 = mirrored image3
      pix9 = PXMpix(E9pxm,E3pxm->ww-1-px,py);
      memcpy(pix9,pix3,pcc);
   }

   PXM_free(E3pxm);                                                              //  image9 >> image3
   E3pxm = E9pxm;
   E9pxm = 0;

   CEF->Fmods++;
   CEF->Fsaved = 0;
   Fpaint2();
   return 0;
}


int mirror_vert()
{
   int      px, py;
   float    *pix3, *pix9;
   int      nc = E3pxm->nc, pcc = nc * sizeof(float);

   E9pxm = PXM_copy(E3pxm);

   for (py = 0; py < E3pxm->hh; py++)
   for (px = 0; px < E3pxm->ww; px++)
   {
      pix3 = PXMpix(E3pxm,px,py);                                                //  image9 = mirrored image3
      pix9 = PXMpix(E9pxm,px,E3pxm->hh-1-py);
      memcpy(pix9,pix3,pcc);
   }

   PXM_free(E3pxm);                                                              //  image9 >> image3
   E3pxm = E9pxm;
   E9pxm = 0;

   CEF->Fmods++;
   CEF->Fsaved = 0;
   Fpaint2();
   return 0;
}


/********************************************************************************/

//  Select area and edit in parallel
//  Current edit function is applied to areas painted with the mouse.
//  Mouse can be weak or strong, and edits are applied incrementally.
//  
//  method:
//  entire image is a select area with all pixel edge distance = 0 (outside area)
//  blendwidth = 10000 (infinite)
//  pixels painted with mouse have increasing edge distance to amplify edits

int   paint_edits_radius;
int   paint_edits_cpower;
int   paint_edits_epower;

void m_paint_edits(GtkWidget *, cchar *)                                         //  menu function
{
   int   paint_edits_dialog_event(zdialog *, cchar *event);                      //  dialog event function
   void  paint_edits_mousefunc();                                                //  mouse function

   cchar    *title = E2X("Paint Edits");
   cchar    *helptext = E2X("Press F1 for help");
   int      yn;

   F1_help_topic = "paint_edits";

   if (sa_stat || zd_sela) {                                                     //  warn select area will be lost
      yn = zmessageYN(Mwin,E2X("Select area cannot be kept.\n"
                               "Continue?"));
      if (! yn) return;
      sa_clear();                                                                //  clear area
      if (zd_sela) zdialog_free(zd_sela);
   }

   if (! CEF) {                                                                  //  edit func must be active
      zmessageACK(Mwin,E2X("Edit function must be active"));
      return;
   }
   
   if (! CEF->FusePL) {
      zmessageACK(Mwin,E2X("Cannot use Paint Edits"));
      return;
   }
   
   if (CEF->Fpreview) 
      zdialog_send_event(CEF->zd,"fullsize");                                    //  use full-size image

/***
    ______________________________________
   |         Press F1 for help            |
   |     Edit Function must be active     |
   |                                      |
   | mouse radius [____]                  |
   | power:  center [____]  edge [____]   |
   | [reset area]                         |
   |                              [done]  |
   |______________________________________|

***/

   zd_sela = zdialog_new(title,Mwin,Bdone,null);
   zdialog_add_widget(zd_sela,"label","labhelp1","dialog",helptext,"space=5");
   zdialog_add_widget(zd_sela,"label","labspace","dialog");
   zdialog_add_widget(zd_sela,"hbox","hbr","dialog",0,"space=3");
   zdialog_add_widget(zd_sela,"label","labr","hbr",Bmouseradius,"space=5");
   zdialog_add_widget(zd_sela,"zspin","radius","hbr","2|500|1|50");
   zdialog_add_widget(zd_sela,"hbox","hbt","dialog",0,"space=3");
   zdialog_add_widget(zd_sela,"label","labtc","hbt",E2X("power:  center"),"space=5");
   zdialog_add_widget(zd_sela,"zspin","center","hbt","0|100|1|50");
   zdialog_add_widget(zd_sela,"label","labte","hbt",Bedge,"space=5");
   zdialog_add_widget(zd_sela,"zspin","edge","hbt","0|100|1|0");
   zdialog_add_widget(zd_sela,"hbox","hbra","dialog",0,"space=5");
   zdialog_add_widget(zd_sela,"button","reset","hbra",E2X("reset area"),"space=5");

   paint_edits_radius = 50;
   paint_edits_cpower = 50;
   paint_edits_epower = 0;

   sa_pixmap_create();                                                           //  allocate select area pixel maps    19.0

   sa_minx = 0;                                                                  //  enclosing rectangle
   sa_maxx = Fpxb->ww;
   sa_miny = 0;
   sa_maxy = Fpxb->hh;

   sa_Npixel = Fpxb->ww * Fpxb->hh;
   sa_stat = 3;                                                                  //  area status = complete
   sa_mode = mode_image;                                                         //  area mode = whole image
   sa_calced = 1;                                                                //  edge calculation complete
   sa_blendwidth = 10000;                                                        //  "blend width"
   sa_fww = Fpxb->ww;                                                            //  valid image dimensions
   sa_fhh = Fpxb->hh;
   areanumber++;                                                                 //  next sequential number

   zdialog_run(zd_sela,paint_edits_dialog_event,"save");                         //  run dialog - parallel
   return;
}


//  Adjust whole image area to increase edit power for pixels within the mouse radius
//  sa_pixmap[*]  = 0 = never touched by mouse
//                = 1 = minimum edit power (barely painted)
//                = sa_blendwidth = maximum edit power (edit fully applied)

int paint_edits_dialog_event(zdialog *zd, cchar *event)
{
   void  paint_edits_mousefunc();                                                //  mouse function

   if (zd->zstat)                                                                //  done or cancel
   {
      freeMouse();                                                               //  disconnect mouse function
      if (CEF) zdialog_send_event(CEF->zd,"done");                               //  complete edit
      zdialog_free(zd_sela);                                                     //  kill dialog
      sa_clear();                                                                //  clear area
      return 1;
   }

   if (sa_stat != 3) return 1;                                                   //  area gone
   if (! sa_validate()) return 1;                                                //  area invalid for curr. image file

   if (strmatch(event,"focus")) {                                                //  toggle mouse capture
      if (CEF) takeMouse(paint_edits_mousefunc,0);
      else freeMouse();                                                          //  disconnect mouse
   }

   if (strmatch(event,"radius"))
      zdialog_fetch(zd,"radius",paint_edits_radius);                             //  set mouse radius

   if (strmatch(event,"center"))
      zdialog_fetch(zd,"center",paint_edits_cpower);                             //  set mouse center power

   if (strmatch(event,"edge"))
      zdialog_fetch(zd,"edge",paint_edits_epower);                               //  set mouse edge power

   if (strmatch(event,"reset")) {
      sa_clear();                                                                //  clear current area if any
      sa_pixmap_create();                                                        //  allocate select area pixel maps    19.0

      sa_minx = 0;                                                               //  enclosing rectangle
      sa_maxx = Fpxb->ww;
      sa_miny = 0;
      sa_maxy = Fpxb->hh;

      sa_Npixel = Fpxb->ww * Fpxb->hh;
      sa_stat = 3;                                                               //  area status = complete
      sa_mode = mode_image;                                                      //  area mode = whole image
      sa_calced = 1;                                                             //  edge calculation complete
      sa_blendwidth = 10000;                                                     //  "blend width"
      sa_fww = Fpxb->ww;                                                         //  valid image dimensions
      sa_fhh = Fpxb->hh;
      areanumber++;                                                              //  next sequential number
   }

   return 1;
}


//  mouse function - adjust edit strength for areas within mouse radius
//  "edge distance" is increased for more strength, decreased for less

void paint_edits_mousefunc()
{
   int      ii, px, py, rx, ry;
   int      radius, radius2, cpower, epower;
   float    rad, rad2, power;

   if (! CEF) return;                                                            //  no active edit
   if (sa_stat != 3) return;                                                     //  area gone?

   radius = paint_edits_radius;                                                  //  pixel selection radius
   radius2 = radius * radius;
   cpower = paint_edits_cpower;
   epower = paint_edits_epower;

   draw_mousecircle(Mxposn,Myposn,radius,0,0);                                   //  show mouse selection circle

   if (LMclick || RMclick)                                                       //  mouse click, process normally
      return;

   if (Mbutton != 1 && Mbutton != 3)                                             //  button released
      return;

   Mxdrag = Mydrag = 0;                                                          //  neutralize drag

   for (rx = -radius; rx <= radius; rx++)                                        //  loop every pixel in radius
   for (ry = -radius; ry <= radius; ry++)
   {
      rad2 = rx * rx + ry * ry;
      if (rad2 > radius2) continue;                                              //  outside radius
      px = Mxposn + rx;
      py = Myposn + ry;
      if (px < 0 || px > Fpxb->ww-1) continue;                                   //  off the image edge
      if (py < 0 || py > Fpxb->hh-1) continue;

      ii = Fpxb->ww * py + px;
      rad = sqrt(rad2);
      power = cpower + rad / radius * (epower - cpower);                         //  power at pixel radius

      if (Mbutton == 1) {                                                        //  left mouse button
         sa_pixmap[ii] += 5.0 * power;                                           //  increase edit power
         if (sa_pixmap[ii] > sa_blendwidth) sa_pixmap[ii] = sa_blendwidth;
      }

      if (Mbutton == 3) {                                                        //  right mouse button
         if (sa_pixmap[ii] <= 5.0 * power) sa_pixmap[ii] = 0;                    //  weaken edit power
         else sa_pixmap[ii] -= 5.0 * power;
      }
   }

   zdialog_send_event(CEF->zd,"blendwidth");                                     //  notify edit dialog

   draw_mousecircle(Mxposn,Myposn,radius,0,0);                                   //  show mouse selection circle
   return;
}


/********************************************************************************/

//  Use the image brightness or color values to lever subsequent edits.
//  Method:
//  Select the whole image as an area.
//  Set "edge distance" 1 to 999 from pixel brightness or RGB color.
//  Set "blend width" to 999.
//  Edit function coefficient = edge distance / blend width.

spldat   *leveds_curve;
int      leveds_type, leveds_color;
int      leveds_ptype, leveds_pcolor;
float    *leveds_lever = 0;


//  menu function

void m_lever_edits(GtkWidget *, cchar *)
{
   int    leveds_event(zdialog *, cchar *event);                                 //  dialog event and completion func
   void   leveds_curve_update(int spc);                                          //  curve update callback function

   cchar    *title = E2X("Lever Edits");
   cchar    *legend = E2X("Edit Function Amplifier");
   int      yn, cc;

   F1_help_topic = "lever_edits";

   if (sa_stat || zd_sela) {                                                     //  warn select area will be lost
      yn = zmessageYN(Mwin,E2X("Select area cannot be kept.\n"
                               "Continue?"));
      if (! yn) return;
      sa_clear();                                                                //  clear area
      if (zd_sela) zdialog_free(zd_sela);
   }

   if (! CEF) {                                                                  //  edit func must be active
      zmessageACK(Mwin,E2X("Edit function must be active"));
      return;
   }

   if (! CEF->FusePL) {
      zmessageACK(Mwin,E2X("Cannot use Lever Edits"));
      return;
   }

   if (CEF->Fpreview) 
      zdialog_send_event(CEF->zd,"fullsize");                                    //  use full-size image

/***
                  Edit Function Amplifier
             ------------------------------------------
            |                                          |
            |                                          |
            |           curve drawing area             |
            |                                          |
            |                                          |
             ------------------------------------------
             minimum                            maximum

             (o) Brightness  (o) Contrast
             (o) All  (o) Red  (o) Green  (o) Blue
             Curve File: [ Open ] [ Save ]
                                              [ Done ]
***/

   zd_sela = zdialog_new(title,Mwin,Bdone,null);

   zdialog_add_widget(zd_sela,"label","labt","dialog",legend);
   zdialog_add_widget(zd_sela,"frame","fr1","dialog",0,"expand");
   zdialog_add_widget(zd_sela,"hbox","hba","dialog");
   zdialog_add_widget(zd_sela,"label","labda","hba",E2X("minimum"),"space=5");
   zdialog_add_widget(zd_sela,"label","space","hba",0,"expand");
   zdialog_add_widget(zd_sela,"label","labba","hba",E2X("maximum"),"space=5");

   zdialog_add_widget(zd_sela,"hbox","hbbr1","dialog");
   zdialog_add_widget(zd_sela,"radio","bright","hbbr1",Bbrightness,"space=5");
   zdialog_add_widget(zd_sela,"radio","contrast","hbbr1",Bcontrast,"space=5");
   zdialog_add_widget(zd_sela,"hbox","hbbr2","dialog");
   zdialog_add_widget(zd_sela,"radio","all","hbbr2",Ball,"space=5");
   zdialog_add_widget(zd_sela,"radio","red","hbbr2",Bred,"space=5");
   zdialog_add_widget(zd_sela,"radio","green","hbbr2",Bgreen,"space=5");
   zdialog_add_widget(zd_sela,"radio","blue","hbbr2",Bblue,"space=5");

   zdialog_add_widget(zd_sela,"hbox","hbcf","dialog",0,"space=5");
   zdialog_add_widget(zd_sela,"label","labcf","hbcf",Bcurvefile,"space=5");
   zdialog_add_widget(zd_sela,"button","loadcurve","hbcf",Bopen,"space=5");
   zdialog_add_widget(zd_sela,"button","savecurve","hbcf",Bsave,"space=5");

   GtkWidget *frame = zdialog_widget(zd_sela,"fr1");                             //  setup for curve editing
   spldat *sd = splcurve_init(frame,leveds_curve_update);
   leveds_curve = sd;

   sd->Nspc = 1;
   sd->vert[0] = 0;
   sd->nap[0] = 3;                                                               //  initial curve anchor points
   sd->fact[0] = 1;
   sd->apx[0][0] = 0.01;
   sd->apy[0][0] = 0.5;
   sd->apx[0][1] = 0.50;
   sd->apy[0][1] = 0.5;
   sd->apx[0][2] = 0.99;
   sd->apy[0][2] = 0.5;
   splcurve_generate(sd,0);                                                      //  generate curve data

   zdialog_stuff(zd_sela,"bright",1);                                            //  type lever = brightness
   zdialog_stuff(zd_sela,"contrast",0);
   zdialog_stuff(zd_sela,"all",1);                                               //  color used = all
   zdialog_stuff(zd_sela,"red",0);
   zdialog_stuff(zd_sela,"green",0);
   zdialog_stuff(zd_sela,"blue",0);
   leveds_type = 1;
   leveds_color = 1;
   leveds_ptype = 0;
   leveds_pcolor = 0;

   sa_pixmap_create();                                                           //  allocate select area pixel maps    19.0

   sa_minx = 0;                                                                  //  enclosing rectangle
   sa_maxx = Fpxb->ww;
   sa_miny = 0;
   sa_maxy = Fpxb->hh;

   sa_Npixel = Fpxb->ww * Fpxb->hh;
   sa_stat = 3;                                                                  //  area status = complete
   sa_mode = mode_image;                                                         //  area mode = whole image
   sa_calced = 1;                                                                //  edge calculation complete
   sa_blendwidth = 999;                                                          //  "blend width" = 999
   sa_fww = Fpxb->ww;                                                            //  valid image dimensions
   sa_fhh = Fpxb->hh;
   areanumber++;                                                                 //  next sequential number

   if (leveds_lever) zfree(leveds_lever);
   cc = E1pxm->ww * E1pxm->hh * sizeof(float);                                   //  allocate memory for lever
   leveds_lever = (float *) zmalloc(cc);

   zdialog_resize(zd_sela,0,360);
   zdialog_run(zd_sela,leveds_event,"save");                                     //  run dialog - parallel
   leveds_event(zd_sela,"init");                                                 //  initialize default params
   return;
}


//  dialog event and completion function

int leveds_event(zdialog *zd, cchar *event)
{
   int         ii, kk, pixdist;
   float       xval, yval, lever;
   float       *pixel0, *pixel1, *pixel2, *pixel3, *pixel4;
   spldat      *sd = leveds_curve;
   
   if (zd->zstat) {                                                              //  done or cancel
      if (CEF && CEF->zd) zdialog_send_event(CEF->zd,"done");                    //  notify edit dialog
      sa_clear();                                                                //  clear area
      zdialog_free(zd_sela);
      zfree(sd);                                                                 //  free curve edit memory
      zfree(leveds_lever);
      leveds_lever = 0;
      return 1;
   }

   if (! sa_validate() || sa_stat != 3) {                                        //  select area gone
      zdialog_free(zd_sela);
      zfree(sd);                                                                 //  free curve edit memory
      zfree(leveds_lever);
      leveds_lever = 0;
      return 1;
   }

   if (strmatch(event,"loadcurve")) {                                            //  load saved curve
      splcurve_load(sd);
      if (CEF && CEF->zd) zdialog_send_event(CEF->zd,"blendwidth");              //  notify edit dialog
      return 1;
   }

   if (strmatch(event,"savecurve")) {                                            //  save curve to file
      splcurve_save(sd);
      return 1;
   }

   ii = strmatchV(event,"bright","contrast",null);                               //  new lever type
   if (ii >= 1 && ii <= 2) leveds_type = ii;

   ii = strmatchV(event,"all","red","green","blue",null);                        //  new lever color
   if (ii >= 1 && ii <= 4) leveds_color = ii;

   if (leveds_type != leveds_ptype || leveds_color != leveds_pcolor)             //  test for change
   {
      if (! CEF) return 1;                                                       //  edit canceled

      leveds_ptype = leveds_type;
      leveds_pcolor = leveds_color;

      for (int ipy = 1; ipy < E1pxm->hh-1; ipy++)
      for (int ipx = 1; ipx < E1pxm->ww-1; ipx++)
      {
         pixel0 = PXMpix(E1pxm,ipx,ipy);                                         //  target pixel to measure
         lever = 0;

         if (leveds_type == 1)                                                   //  lever type = brightness
         {
            if (leveds_color == 1)
               lever = 0.333 * (pixel0[0] + pixel0[1] + pixel0[2]);              //  use all colors
            else {
               ii = leveds_color - 2;                                            //  use single color
               lever = pixel0[ii];
            }
         }

         else if (leveds_type == 2)                                              //  lever type = contrast
         {
            pixel1 = PXMpix(E1pxm,ipx-1,ipy);
            pixel2 = PXMpix(E1pxm,ipx+1,ipy);
            pixel3 = PXMpix(E1pxm,ipx,ipy-1);
            pixel4 = PXMpix(E1pxm,ipx,ipy+1);
            
            if (leveds_color == 1)                                               //  use all colors
            {
               lever  = fabsf(pixel0[0] - pixel1[0]) + fabsf(pixel0[0] - pixel2[0])
                      + fabsf(pixel0[0] - pixel3[0]) + fabsf(pixel0[0] - pixel4[0]);
               lever += fabsf(pixel0[1] - pixel1[1]) + fabsf(pixel0[1] - pixel2[1])
                      + fabsf(pixel0[1] - pixel3[1]) + fabsf(pixel0[1] - pixel4[1]);
               lever += fabsf(pixel0[2] - pixel1[2]) + fabsf(pixel0[2] - pixel2[2])
                      + fabsf(pixel0[2] - pixel3[2]) + fabsf(pixel0[2] - pixel4[2]);
               lever = lever / 12.0;
            }
            else                                                                 //  use single color
            {
               ii = leveds_color - 2;
               lever  = fabsf(pixel0[ii] - pixel1[ii]) + fabsf(pixel0[ii] - pixel2[ii])
                      + fabsf(pixel0[ii] - pixel3[ii]) + fabsf(pixel0[ii] - pixel4[ii]);
               lever = lever / 4.0;
            }
         }

         lever = lever / 1000.0;                                                 //  scale 0.0 to 0.999
         lever = pow(lever,0.3);                                                 //  log scale: 0.1 >> 0.5, 0.8 >> 0.94

         ii = ipy * E1pxm->ww + ipx;                                             //  save lever for each pixel
         leveds_lever[ii] = lever;
      }
   }

   if (strmatch(event,"edit")) {
      splcurve_generate(sd,0);                                                   //  regenerate the curve
      gtk_widget_queue_draw(sd->drawarea);
   }

   if (! CEF) return 1;                                                          //  edit canceled

   for (int ipy = 1; ipy < E1pxm->hh-1; ipy++)
   for (int ipx = 1; ipx < E1pxm->ww-1; ipx++)
   {
      pixel0 = PXMpix(E1pxm,ipx,ipy);                                            //  target pixel to measure
      ii = ipy * E1pxm->ww + ipx;
      lever = leveds_lever[ii];                                                  //  lever to apply, 0.0 to 0.999
      xval = lever;                                                              //  curve x-value, 0.0 to 0.999
      kk = 1000 * xval;
      if (kk > 999) kk = 999;
      yval = sd->yval[0][kk];                                                    //  y-value, 0 to 0.999
      pixdist = 1 + 998 * yval;                                                  //  pixel edge distance, 1 to 999
      sa_pixmap[ii] = pixdist;
   }

   if (CEF->zd) zdialog_send_event(CEF->zd,"blendwidth");                        //  notify edit dialog
   return 1;
}


//  this function is called when curve is edited using mouse

void  leveds_curve_update(int)
{
   if (! zd_sela) return;
   leveds_event(zd_sela,"edit");
   return;
}


