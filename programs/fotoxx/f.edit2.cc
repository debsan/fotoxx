/********************************************************************************

   Fotoxx      edit photos and manage collections

   Copyright 2007-2019 Michael Cornelison
   source code URL: https://kornelix.net
   contact: kornelix@posteo.de

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version. See https://www.gnu.org/licenses

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
   See the GNU General Public License for more details.

*********************************************************************************

   Fotoxx image edit - Edit 2 menu functions

   m_voodoo1               automatic image enhancement with limited smarts
   m_voodoo2               automatic image enhancement with limited smarts
   m_edit_brightness       flatten and/or expand brightness distribution
   m_gradients             magnify brightness gradients to enhance details
   m_flatten               flatten brightness distribution
   m_gretinex              rescale pixel RGB brightness range - for global image
   m_zretinex              rescale pixel RGB brightness range - for image zones
   m_zonal_colors          shift RGB colors in selected image areas
   m_match_colors          adjust image colors to match those in a chosen image
   m_colordepth            reduce color depth from 16 ... 1 bits
   m_smart_erase           replace pixels inside selected areas with background
   m_bright_ramp           adjust brightness graduaally across the image
   m_paint_image           paint on the image with a color
   color_chooser           select color from color chooser file
   HSL_chooser             select color from HSL chooser dialog
   m_copypixels1           copy pixels within one image
   m_copypixels2           copy pixels from source image to target image
   m_copypixels3           copypixels2 source image process
   m_paint_transp          paint more or less transparency on an image
   m_color_fringes         stretch/shrink RGB color planes to remove color fringes
   m_anti_alias            suppress pixel steps (jaggies) from image edges
   m_plugins               plugins menu function
   m_edit_plugins          add/revise/delete plugin menu functions
   m_run_plugin            run a plugin menu command and update image

*********************************************************************************/

#define EX extern                                                                //  enable extern declarations
#include "fotoxx.h"                                                              //  (variables in fotoxx.h are refs)


/********************************************************************************/

//  automatic image tuneup without user guidance

float       voodoo1_brdist[256];                                                 //  pixel count per brightness level
int         voodoo_01, voodoo_99;                                                //  1% and 99% brightness levels (0 - 255)
void        voodoo1_distribution();                                              //  compute brightness distribution
void *      voodoo1_thread(void *);
editfunc    EFvoodoo1;


void m_voodoo1(GtkWidget *, cchar *menu)
{
   F1_help_topic = "voodoo";

   EFvoodoo1.menuname = menu;
   EFvoodoo1.menufunc = m_voodoo1;
   EFvoodoo1.funcname = "voodoo1";
   EFvoodoo1.Farea = 1;                                                          //  select area ignored
   EFvoodoo1.Fscript = 1;                                                        //  scripting supported
   EFvoodoo1.threadfunc = voodoo1_thread;

   if (! edit_setup(EFvoodoo1)) return;                                          //  setup edit
   voodoo1_distribution();                                                       //  compute brightness distribution
   signal_thread();                                                              //  start update thread
   edit_done(0);                                                                 //  edit done
   return;
}
   

//  compute brightness distribution for image

void voodoo1_distribution()
{
   int         px, py, ii;
   int         ww = E1pxm->ww, hh = E1pxm->hh;
   float       bright1;
   float       *pix1;

   for (ii = 0; ii < 256; ii++)                                                  //  clear brightness distribution data
      voodoo1_brdist[ii] = 0;

   for (py = 0; py < hh; py++)                                                   //  compute brightness distribution
   for (px = 0; px < ww; px++)
   {
      pix1 = PXMpix(E1pxm,px,py);
      bright1 = pixbright(pix1);
      voodoo1_brdist[int(bright1)]++;
   }

   for (ii = 1; ii < 256; ii++)                                                  //  cumulative brightness distribution
      voodoo1_brdist[ii] += voodoo1_brdist[ii-1];                                //   0 ... (ww * hh)

   voodoo_01 = 0;
   voodoo_99 = 255;

   for (ii = 0; ii < 256; ii++)                                                  //  compute index values (0 - 255)
   {                                                                             //    for darkest 1% and brightest 1%
      if (voodoo1_brdist[ii] < 0.01 * ww * hh) voodoo_01 = ii;
      if (voodoo1_brdist[ii] < 0.99 * ww * hh) voodoo_99 = ii;
   }

   for (ii = 0; ii < 256; ii++)
      voodoo1_brdist[ii] = voodoo1_brdist[ii]                                    //  multiplier per brightness level
                    / (ww * hh) * 256.0 / (ii + 1);                              //  ( = 1 for a flat distribution)
   return;
}


//  thread function - use multiple working threads

void * voodoo1_thread(void *)
{
   void  * voodoo1_wthread(void *arg);

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      do_wthreads(voodoo1_wthread,NWT);

      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
   }

   return 0;                                                                     //  not executed, stop g++ warning
}

void * voodoo1_wthread(void *arg)                                                //  worker thread function
{
   int         index = *((int *) (arg));
   int         px, py;
   float       *pix1, *pix3;
   float       bright1, bright2, bright3, cmax;
   float       red1, green1, blue1, red3, green3, blue3;
   float       flat1 = 0.3;                                                      //  strength of distribution flatten
   float       flat2 = 1.0 - flat1;
   float       sat1 = 0.3, sat2;                                                 //  strength of saturation increase
   float       f1, f2;
   float       expand = 256.0 / (voodoo_99 - voodoo_01 + 1);                     //  brightness range expander

   for (py = index; py < E1pxm->hh; py += NWT)                                   //  voodoo brightness distribution
   for (px = 0; px < E1pxm->ww; px++)
   {
      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      red1 = pix1[0];
      green1 = pix1[1];
      blue1 = pix1[2];

      bright2 = 0.25 * red1 + 0.65 * green1 + 0.10 * blue1;                      //  input brightness, 0 - 256
      bright2 = (bright2 - voodoo_01) * expand;                                  //  expand to clip low / high 1%
      if (bright2 < 0) bright2 = 0;
      if (bright2 > 255) bright2 = 255;

      bright1 = voodoo1_brdist[int(bright2)];                                    //  factor for flat output brightness
      bright3 = flat1 * bright1 + flat2;                                         //  attenuate

      f1 = (256.0 - bright2) / 256.0;                                            //  bright2 = 0 - 256  >>  f1 = 1 - 0
      f2 = 1.0 - f1;                                                             //  prevent banding in bright areas
      bright3 = f1 * bright3 + f2;                                               //  tends to 1.0 for brighter pixels

      red3 = red1 * bright3;                                                     //  blend new and old brightness
      green3 = green1 * bright3;
      blue3 = blue1 * bright3;

      bright3 = 0.333 * (red3 + green3 + blue3);                                 //  mean color brightness
      sat2 = sat1 * (256.0 - bright3) / 256.0;                                   //  bright3 = 0 - 256  >>  sat2 = sat1 - 0
      red3 = red3 + sat2 * (red3 - bright3);                                     //  amplified color, max for dark pixels
      green3 = green3 + sat2 * (green3 - bright3);
      blue3 = blue3 + sat2 * (blue3 - bright3);

      if (red3 < 0) red3 = 0;                                                    //  stop possible underflow
      if (green3 < 0) green3 = 0;
      if (blue3 < 0) blue3 = 0;

      cmax = red3;                                                               //  stop overflow, keep color balance
      if (green3 > cmax) cmax = green3;
      if (blue3 > cmax) cmax = blue3;
      if (cmax > 255.9) {
         cmax = 255.9 / cmax;
         red3 = red3 * cmax;
         green3 = green3 * cmax;
         blue3 = blue3 * cmax;
      }

      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  flatten brightness distribution based on the distribution of nearby zones

namespace voodoo2_names
{
   float    flatten = 30;                          //  flatten value, 30%
   float    deband = 70;                           //  deband value, 70%
   int      Iww, Ihh;                              //  image dimensions
   int      NZ;                                    //  no. of image zones
   int      Zsize, Zrows, Zcols;                   //  zone parameters
   float    *Br;                                   //  Br[py][px]  pixel brightness
   int      *Zxlo, *Zylo, *Zxhi, *Zyhi;            //  Zxlo[ii] etc.  zone ii pixel range
   int      *Zcen;                                 //  Zcen[ii][2]  zone ii center (py,px)
   uint8    *Zn;                                   //  Zn[py][px][9]  9 nearest zones for pixel: 0-200 (255 = none)
   uint8    *Zw;                                   //  Zw[py][px][9]  zone weights for pixel: 0-100 = 1.0
   float    *Zff;                                  //  Zff[ii][1000]  zone ii flatten factors

   editfunc EFvoodoo2;                             //  edit function
   void * wthread(void *);                         //  working thread
}


void m_voodoo2(GtkWidget *, cchar *menu)
{
   using namespace voodoo2_names;

   int      gNZ, pNZ;
   int      px, py, cx, cy;
   int      rx, ii, jj, kk;
   int      zww, zhh, row, col;
   float    *pix1, bright;
   float    weight[9], sumweight;
   float    D, Dthresh;
   
   F1_help_topic = "voodoo";

   EFvoodoo2.menuname = menu;
   EFvoodoo2.menufunc = m_voodoo2;
   EFvoodoo2.funcname = "voodoo2";
   EFvoodoo2.Farea = 1;                                                          //  select area ignored
   EFvoodoo2.Fscript = 1;                                                        //  scripting supported

   if (! edit_setup(EFvoodoo2)) return;                                          //  setup edit

   Iww = E1pxm->ww;                                                              //  image dimensions
   Ihh = E1pxm->hh;
   
   if (Iww * Ihh > wwhh_limit2) {
      zmessageACK(Mwin,"image too big");
      edit_cancel(0);
      return;
   }

   Ffuncbusy = 1;

   NZ = 40;                                                                      //  zone count
   gNZ = pNZ = NZ;                                                               //  zone count goal

   while (true)
   {
      Zsize = sqrtf(Iww * Ihh / gNZ);                                            //  approx. zone size
      Zrows = Ihh / Zsize + 0.5;                                                 //  get appropriate rows and cols
      Zcols = Iww / Zsize + 0.5;
      NZ = Zrows * Zcols;                                                        //  NZ matching rows x cols
      if (NZ >= pNZ) break;
      gNZ++;
   }

   Br = (float *) zmalloc(Iww * Ihh * sizeof(float));                            //  allocate memory
   Zn = (uint8 *) zmalloc(Iww * Ihh * 9 * sizeof(uint8));                        //  cannot exceed 2048 M
   Zw = (uint8 *) zmalloc(Iww * Ihh * 9 * sizeof(uint8));
   Zxlo = (int *) zmalloc(NZ * sizeof(int));
   Zylo = (int *) zmalloc(NZ * sizeof(int));
   Zxhi = (int *) zmalloc(NZ * sizeof(int));
   Zyhi = (int *) zmalloc(NZ * sizeof(int));
   Zcen = (int *) zmalloc(NZ * 2 * sizeof(int));
   Zff = (float *) zmalloc(NZ * 1000 * sizeof(float));

   for (py = 0; py < Ihh; py++)                                                  //  get initial pixel brightness levels
   for (px = 0; px < Iww; px++)
   {
      pix1 = PXMpix(E1pxm,px,py);
      bright = pixbright(pix1);
      ii = py * Iww + px;
      Br[ii] = bright;
      zmainloop(1000);                                                           //  keep GTK alive
   }

   zww = Iww / Zcols;                                                            //  actual zone size
   zhh = Ihh / Zrows;

   for (row = 0; row < Zrows; row++)
   for (col = 0; col < Zcols; col++)                                             //  set low/high bounds per zone
   {
      ii = row * Zcols + col;
      Zxlo[ii] = col * zww;
      Zylo[ii] = row * zhh;
      Zxhi[ii] = Zxlo[ii] + zww;
      Zyhi[ii] = Zylo[ii] + zhh;
      Zcen[2*ii] = Zylo[ii] + zhh/2;
      Zcen[2*ii+1] = Zxlo[ii] + zww/2;
   }

   for (ii = 0; ii < NZ; ii++)                                                   //  compute brightness distributiion
   {                                                                             //    for each zone
      for (jj = 0; jj < 1000; jj++)
         Zff[1000*ii+jj] = 0;

      for (py = Zylo[ii]; py < Zyhi[ii]; py++)                                   //  brightness distribution
      for (px = Zxlo[ii]; px < Zxhi[ii]; px++)
      {
         pix1 = PXMpix(E1pxm,px,py);
         bright = 1000.0 / 256.0 * pixbright(pix1);
         Zff[1000*ii+int(bright)]++;
      }

      for (jj = 1; jj < 1000; jj++)                                              //  cumulative brightness distribution
         Zff[1000*ii+jj] += Zff[1000*ii+jj-1];

      for (jj = 0; jj < 1000; jj++)                                              //  multiplier per brightness level
         Zff[1000*ii+jj] = Zff[1000*ii+jj] / (zww*zhh) * 1000 / (jj+1);          //    to make distribution flat

      zmainloop(1000);                                                           //  keep GTK alive
   }

   for (py = 0; py < Ihh; py++)                                                  //  set 9 nearest zones per pixel
   for (px = 0; px < Iww; px++)
   {
      rx = (py * Iww + px) * 9;                                                  //  index for 9 nearest zones to px/py

      row = py / zhh;                                                            //  zone containing pixel
      col = px / zww;

      ii = 0;
      for (jj = row-1; jj <= row+1; jj++)                                        //  loop 3x3 surrounding zones
      for (kk = col-1; kk <= col+1; kk++)
      {
         if (jj >= 0 && jj < Zrows && kk >= 0 && kk < Zcols)                     //  zone is not off the edge
            Zn[rx+ii] = jj * Zcols + kk;
         else Zn[rx+ii] = 255;                                                   //  edge row/col: missing neighbor
         ii++;
      }
   }

   if (zww < zhh)                                                                //  pixel to zone distance threshold
      Dthresh = 1.5 * zww;                                                       //  area influence = 0 beyond this distance
   else Dthresh = 1.5 * zhh;

   for (py = 0; py < Ihh; py++)                                                  //  set zone weights per pixel
   for (px = 0; px < Iww; px++)
   {
      rx = (py * Iww + px) * 9;                                                  //  index for 9 nearest zones to px/py

      for (ii = 0; ii < 9; ii++)                                                 //  distance to each zone center
      {
         jj = Zn[rx+ii];
         if (jj == 255) {
            weight[ii] = 0;                                                      //  missing zone weight = 0
            continue;
         }
         cy = Zcen[2*jj];
         cx = Zcen[2*jj+1];
         D = sqrtf((py-cy)*(py-cy) + (px-cx)*(px-cx));                           //  distance from pixel to zone center
         D = D / Dthresh;
         if (D > 1) D = 1;                                                       //  zone influence reaches zero at Dthresh
         weight[ii] = 1.0 - D;
      }

      sumweight = 0;
      for (ii = 0; ii < 9; ii++)                                                 //  zone weights based on distance
         sumweight += weight[ii];

      for (ii = 0; ii < 9; ii++)                                                 //  normalize weights, sum = 1.0
         Zw[rx+ii] = 100 * weight[ii] / sumweight;                               //  0-100 = 1.0

      zmainloop(1000);                                                           //  keep GTK alive
   }

   do_wthreads(wthread,NWT);

   zfree(Br);                                                                    //  free memory
   zfree(Zn);
   zfree(Zw);
   zfree(Zxlo);
   zfree(Zylo);
   zfree(Zxhi);
   zfree(Zyhi);
   zfree(Zcen);
   zfree(Zff);

   Ffuncbusy = 0;
   CEF->Fmods++;
   CEF->Fsaved = 0;
   edit_done(0);                                                                 //  edit done
   return;
}


//  worker thread for each CPU processor core

void * voodoo2_names::wthread(void *arg)
{
   using namespace voodoo2_names;

   int         index = *((int *) (arg));
   int         px, py, rx, ii, jj;
   float       *pix1, *pix3;
   float       fold, fnew, cmax;
   float       red1, green1, blue1, red3, green3, blue3;
   float       FF, weight, bright, debandx;

   for (py = index; py < Ihh; py += NWT)                                         //  loop all image pixels
   for (px = 0; px < Iww; px++)
   {
      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      red1 = pix1[0];
      green1 = pix1[1];
      blue1 = pix1[2];

      bright = 0.25 * red1 + 0.65 * green1 + 0.10 * blue1;                       //  input pixel brightness 0-255.9
      bright *= 1000.0 / 256.0;                                                  //  0-999

      rx = (py * Iww + px) * 9;                                                  //  index for 9 nearest zones to px/py

      FF = 0;
      for (ii = 0; ii < 9; ii++) {                                               //  loop 9 nearest zones
         weight = Zw[rx+ii];
         if (weight > 0) {                                                       //  0-100 = 1.0
            jj = Zn[rx+ii];
            FF += 0.01 * weight * Zff[1000*jj+int(bright)];                      //  sum weight * flatten factor
         }
      }

      red3 = FF * red1;                                                          //  fully flattened brightness
      green3 = FF * green1;
      blue3 = FF * blue1;

      debandx = (1000.0 - bright) / 1000.0;                                      //  bright = 0 to 1000  >>  debandx = 1 to 0
      debandx += (1.0 - debandx) * (1.0 - 0.01 * deband);                        //  debandx = 1 to 1  >>  1 to 0

      fnew = 0.01 * flatten;                                                     //  how much to flatten, 0 to 1
      fnew = debandx * fnew;                                                     //  attenuate flatten of brighter pixels
      fold = 1.0 - fnew;                                                         //  how much to retain, 1 to 0

      red3 = fnew * red3 + fold * red1;                                          //  blend new and old brightness
      green3 = fnew * green3 + fold * green1;
      blue3 = fnew * blue3 + fold * blue1;

      cmax = red3;                                                               //  stop overflow, keep color balance
      if (green3 > cmax) cmax = green3;
      if (blue3 > cmax) cmax = blue3;
      if (cmax > 255.9) {
         cmax = 255.9 / cmax;
         red3 = red3 * cmax;
         green3 = green3 * cmax;
         blue3 = blue3 * cmax;
      }

      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  adjust brightness distribution by flattening and/or expanding range

namespace edit_brightness_names
{
   int      ww, hh;
   float    LC, HC;                                                              //  low, high cutoff levels
   float    LF, MF, HF;                                                          //  low, mid, high flatten parms
   float    LS, MS, HS;                                                          //  low, mid, high stretch parms
   float    BB[1000];                                                            //  adjusted B for input B 0-999

   int    dialog_event(zdialog* zd, cchar *event);
   void   compute_BB();
   void * thread(void *);
   void * wthread(void *);

   editfunc    EFbrightdist;
}


//  menu function

void m_edit_brightness(GtkWidget *, cchar *menu)
{
   using namespace edit_brightness_names;

   cchar  *title = E2X("Adjust Brightness Distribution");

   F1_help_topic = "brightness";

   EFbrightdist.menuname = menu;
   EFbrightdist.menufunc = m_edit_brightness;
   EFbrightdist.funcname = "brightness";
   EFbrightdist.FprevReq = 1;                                                    //  preview
   EFbrightdist.Farea = 2;                                                       //  select area usable
   EFbrightdist.Frestart = 1;                                                    //  restart allowed
   EFbrightdist.Fscript = 1;                                                     //  scripting supported 
   EFbrightdist.threadfunc = thread;
   if (! edit_setup(EFbrightdist)) return;                                       //  setup edit

/***
          __________________________________________
         |      Adjust Brightness Distribution      |
         |                                          |
         | Low Cutoff   ==========[]==============  |
         | High Cutoff  ==============[]==========  |
         | Low Flatten  =========[]===============  |
         | Mid Flatten  ============[]============  |
         | High Flatten ==============[]==========  |
         | Low Stretch  ================[]========  |
         | Mid Stretch  =============[]===========  |
         | High Stretch =========[]===============  |
         |                                          |
         |                  [Reset] [Done] [Cancel] |
         |__________________________________________|

***/

   zdialog *zd = zdialog_new(title,Mwin,Breset,Bdone,Bcancel,null);
   EFbrightdist.zd = zd;

   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=5");
   zdialog_add_widget(zd,"vbox","vb1","hb1",0,"homog|space=3");
   zdialog_add_widget(zd,"vbox","vb2","hb1",0,"homog|expand");

   zdialog_add_widget(zd,"label","labLC","vb1",E2X("Low Cutoff"));
   zdialog_add_widget(zd,"label","labHC","vb1",E2X("High Cutoff"));
   zdialog_add_widget(zd,"label","labLF","vb1",E2X("Low Flatten"));
   zdialog_add_widget(zd,"label","labMF","vb1",E2X("Mid Flatten"));
   zdialog_add_widget(zd,"label","labHF","vb1",E2X("High Flatten"));
   zdialog_add_widget(zd,"label","labLS","vb1",E2X("Low Stretch"));
   zdialog_add_widget(zd,"label","labMS","vb1",E2X("Mid Stretch"));
   zdialog_add_widget(zd,"label","labHS","vb1",E2X("High Stretch"));

   zdialog_add_widget(zd,"hscale","LC","vb2","0|1.0|0.002|0","expand");
   zdialog_add_widget(zd,"hscale","HC","vb2","0|1.0|0.002|0","expand");
   zdialog_add_widget(zd,"hscale","LF","vb2","0|1.0|0.002|0","expand");
   zdialog_add_widget(zd,"hscale","MF","vb2","0|1.0|0.002|0","expand");
   zdialog_add_widget(zd,"hscale","HF","vb2","0|1.0|0.002|0","expand");
   zdialog_add_widget(zd,"hscale","LS","vb2","0|1.0|0.002|0","expand");
   zdialog_add_widget(zd,"hscale","MS","vb2","0|1.0|0.002|0","expand");
   zdialog_add_widget(zd,"hscale","HS","vb2","0|1.0|0.002|0","expand");

   zdialog_resize(zd,300,0);
   zdialog_run(zd,dialog_event,"save");                                          //  run dialog - parallel

   m_brightgraph(0,0);                                                           //  popup brightness histogram
   
   LC = HC = LF = MF = HF = LS = MS = HS = 0.0;
   compute_BB();
   
   return;
}


//  dialog event and completion function

int edit_brightness_names::dialog_event(zdialog *zd, cchar *event)
{
   using namespace edit_brightness_names;
   
   if (strmatch(event,"done")) zd->zstat = 2;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  cancel

   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();                                                           //  get full size image
      compute_BB();
      signal_thread();
      return 1;
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  reset
         zd->zstat = 0;                                                          //  keep dialog active
         LC = HC = LF = MF = HF = LS = MS = HS = 0.0;
         zdialog_stuff(zd,"LC",LC);
         zdialog_stuff(zd,"HC",HC);
         zdialog_stuff(zd,"LF",LF);
         zdialog_stuff(zd,"MF",MF);
         zdialog_stuff(zd,"HF",HF);
         zdialog_stuff(zd,"LS",LS);
         zdialog_stuff(zd,"MS",MS);
         zdialog_stuff(zd,"HS",HS);
         compute_BB();
         edit_reset();                                                           //  19.0
      }
      else if (zd->zstat == 2 && CEF->Fmods) {                                   //  done
         edit_fullsize();                                                        //  get full size image
         compute_BB();
         signal_thread();
         m_brightgraph(0,"kill");                                                //  kill distribution graph
         edit_done(0);                                                           //  commit edit
      }
      else {
         edit_cancel(0);                                                         //  cancel - discard edit
         m_brightgraph(0,"kill");                                                //  kill distribution graph
      }

      return 1;                                                                  //  19.0
   }

   if (strmatch(event,"blendwidth"))                                             //  select area blendwidth change
      signal_thread();
   
   if (strstr("LC HC LF MF HF LS MS HS apply",event))
   {
      zdialog_fetch(zd,"LC",LC);
      zdialog_fetch(zd,"HC",HC);
      zdialog_fetch(zd,"LF",LF);
      zdialog_fetch(zd,"MF",MF);
      zdialog_fetch(zd,"HF",HF);
      zdialog_fetch(zd,"LS",LS);
      zdialog_fetch(zd,"MS",MS);
      zdialog_fetch(zd,"HS",HS);
      compute_BB();
      signal_thread();
   }

   wait_thread_idle();                                                           //  no overlap window update and threads
   Fpaintnow();
   return 1;
}


//  compute flattened brightness levels for preview size or full size image
//  FB[B] = flattened brightness level for brightness B, scaled 0-1000

void edit_brightness_names::compute_BB()
{
   using namespace edit_brightness_names;

   int      ii, npix, py, px, iB;
   float    *pix1;
   float    B, LC2, HC2;
   float    FB[1000];
   float    LF2, MF2, HF2, LS2, MS2, HS2;
   float    LFB, MFB, HFB, LSB, MSB, HSB;
   float    LFW, MFW, HFW, LSW, MSW, HSW, TWB;

   ww = E1pxm->ww;
   hh = E1pxm->hh;

   for (ii = 0; ii < 1000; ii++)                                                 //  clear brightness distribution data
      FB[ii] = 0;

   if (sa_stat == 3)                                                             //  process selected area
   {
      for (ii = npix = 0; ii < ww * hh; ii++)
      {
         if (! sa_pixmap[ii]) continue;
         py = ii / ww;
         px = ii - py * ww;
         pix1 = PXMpix(E1pxm,px,py);
         B = 1000.0 * (pix1[0] + pix1[1] + pix1[2]) / 768.0;
         FB[int(B)]++;
         npix++;
      }

      for (ii = 1; ii < 1000; ii++)                                              //  cumulative brightness distribution
         FB[ii] += FB[ii-1];                                                     //   0 ... npix

      for (ii = 0; ii < 1000; ii++)
         FB[ii] = FB[ii] / npix * 999.0;                                         //  flattened brightness level
   }

   else                                                                          //  process whole image
   {
      for (py = 0; py < hh; py++)                                                //  compute brightness distribution
      for (px = 0; px < ww; px++)
      {
         pix1 = PXMpix(E1pxm,px,py);
         B = 1000.0 * (pix1[0] + pix1[1] + pix1[2]) / 768.0;
         FB[int(B)]++;
      }

      for (ii = 1; ii < 1000; ii++)                                              //  cumulative brightness distribution
         FB[ii] += FB[ii-1];                                                     //   0 ... (ww * hh)
   
      for (ii = 0; ii < 1000; ii++)
         FB[ii] = FB[ii] / (ww * hh) * 999.0;                                    //  flattened brightness level
   }

   LC2 = 500 * LC;                                                               //  low cutoff, 0 ... 500
   HC2 = 1000 - 500 * HC;                                                        //  high cutoff, 1000 ... 500

   for (iB = 0; iB < 1000; iB++)                                                 //  loop brightness 0 - 1000   
   {
      B = iB;

      if (LC2 > 0 || HC2 < 1000) {                                               //  stretch to cutoff limits
         if (B < LC2) B = 0;
         else if (B > HC2) B = 999;
         else B = 1000.0 * (B - LC2) / (HC2 - LC2);
      }
      
      LF2 = LF * (1000 - B) / 1000;                                              //  low flatten  LF ... 0
      LF2 = LF2 * LF2;
      LFB = LF2 * FB[iB] + (1.0 - LF2) * B;
      
      LS2 = LS * (1000 - B) / 1000;                                              //  low stretch  LS ... 0
      LS2 = LS2 * LS2;
      LSB = B * (1 - LS2);
      
      MF2 = MF * (500 - fabsf(B - 500)) / 500;                                   //  mid flatten  0 ... MF ... 0
      MF2 = sqrtf(MF2);
      MFB = MF2 * FB[iB] + (1.0 - MF2) * B;

      MS2 = MS * (B - 500) / 500;                                                //  mid stretch  -MS ... 0 ... MS
      MSB = B * (1 + 0.5 * MS2);
      
      HF2 = HF * B / 1000;                                                       //  high flatten  0 ... HF
      HF2 = HF2 * HF2;
      HFB = HF2 * FB[iB] + (1.0 - HF2) * B;

      HS2 = HS * B / 1000;                                                       //  high stretch  0 ... HS
      HS2 = HS2 * HS2;
      HSB = B * (1 + HS2);
      
      LFW = fabsf(B - LFB) / (B + 1);                                            //  weight of each component
      LSW = fabsf(B - LSB) / (B + 1);
      MFW = fabsf(B - MFB) / (B + 1);
      MSW = fabsf(B - MSB) / (B + 1);
      HFW = fabsf(B - HFB) / (B + 1);
      HSW = fabsf(B - HSB) / (B + 1);
      
      TWB = LFW + LSW + MFW + MSW + HFW + HSW;                                   //  add weighted components
      if (TWB == 0) BB[iB] = B;
      else BB[iB] = (LFW * LFB + LSW * LSB
                   + MFW * MFB + MSW * MSB 
                   + HFW * HFB + HSW * HSB) / TWB;
   }
   
   return;
}


//  adjust brightness distribution thread function

void * edit_brightness_names::thread(void *)
{
   using namespace edit_brightness_names;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      do_wthreads(wthread,NWT);

      paintlock(0);                                                              //  unblock window paint               19.0

      CEF->Fmods++;                                                              //  image modified, not saved
      CEF->Fsaved = 0;
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


//  worker thread for each CPU processor core

void * edit_brightness_names::wthread(void *arg)
{
   using namespace edit_brightness_names;

   int         index = *((int *) (arg));
   int         iB, px, py, ii, dist = 0;
   float       B, *pix1, *pix3;
   float       dold, dnew, cmax;
   float       red1, green1, blue1, red3, green3, blue3;
   
   for (py = index; py < E1pxm->hh; py += NWT)                                   //  flatten brightness distribution
   for (px = 0; px < E1pxm->ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E1pxm->ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel
      
      B = (pix1[0] + pix1[1] + pix1[2]) / 768.0 * 1000.0;                        //  pixel brightness scaled 0-1000
      iB = int(B);
      if (B > 0) B = BB[iB] / B;
      
      red1 = pix1[0];
      green1 = pix1[1];
      blue1 = pix1[2];
      
      red3 = B * red1;
      green3 = B * green1;
      blue3 = B * blue1;

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         dnew = sa_blendfunc(dist);                                              //    blend edge
         dold = 1.0 - dnew;
         red3 = dnew * red3 + dold * red1;
         green3 = dnew * green3 + dold * green1;
         blue3 = dnew * blue3 + dold * blue1;
      }

      cmax = red3;                                                               //  stop overflow, keep color balance
      if (green3 > cmax) cmax = green3;
      if (blue3 > cmax) cmax = blue3;
      if (cmax > 255.9) {
         cmax = 255.9 / cmax;
         red3 = red3 * cmax;
         green3 = green3 * cmax;
         blue3 = blue3 * cmax;
      }

      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;
   }

   pthread_exit(0);
}


/********************************************************************************

   Magnify Gradients function
   enhance detail by magnifying brightness gradients

   methodology:
   get brightness gradients for each pixel in 4 directions: SE SW NE NW
   amplify gradients using the edit curve: new gradient = F(old gradient)
   integrate 4 new brightness surfaces from the amplified gradients:
     - pixel brightness = prior pixel brightness + amplified gradient
     - the Amplify control varies amplification from zero to maximum
   new pixel brightness = average from 4 calculated brightness surfaces

*********************************************************************************/

namespace gradients_names
{
   float       *brmap1, *brmap2;
   float       *brmap4[4];
   int         contrast99;
   float       amplify;
   int         ww, hh;
   editfunc    EFgradients;

   void   gradients_initz(zdialog *zd);
   int    gradients_dialog_event(zdialog *zd, cchar *event);
   void   gradients_curvedit(int);
   void * gradients_thread(void *);
   void * gradients_wthread1(void *arg);
   void * gradients_wthread2(void *arg);
   void * gradients_wthread3(void *arg);

}

//  menu function

void m_gradients(GtkWidget *, cchar *menu)
{
   using namespace gradients_names;

   F1_help_topic = "gradients";

   cchar    *title = E2X("Magnify Gradients");
   
   EFgradients.menuname = menu;
   EFgradients.menufunc = m_gradients;
   EFgradients.funcname = "gradients";
   EFgradients.Farea = 2;                                                        //  select area usable
   EFgradients.Frestart = 1;                                                     //  restart allowed
   EFgradients.FusePL = 1;                                                       //  use with paint/lever edits OK
   EFgradients.Fscript = 1;                                                      //  scripting supported
   EFgradients.threadfunc = gradients_thread;
   if (! edit_setup(EFgradients)) return;                                        //  setup: no preview, select area OK

/***
          ________________________________
         |      Magnify Gradients         |
         |  ____________________________  |
         | |                            | |
         | |                            | |
         | |    curve drawing area      | |
         | |                            | |
         | |____________________________| |
         |  low       contrast      high  |
         |                                |
         |  Amplify ========[]==========  |
         |                                |
         |  Curve File: [Open] [Save]     |
         |                                |
         |                [Done] [Cancel] |
         |________________________________|

***/

   zdialog *zd = zdialog_new(title,Mwin,Bdone,Bcancel,null);
   EFgradients.zd = zd;

   zdialog_add_widget(zd,"frame","frame","dialog",0,"expand");
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labcL","hb1",E2X("low"),"space=4");
   zdialog_add_widget(zd,"label","labcM","hb1",Bcontrast,"expand");
   zdialog_add_widget(zd,"label","labcH","hb1",E2X("high"),"space=5");

   zdialog_add_widget(zd,"hbox","hb2","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labcon","hb2",E2X("Amplify"),"space=5");
   zdialog_add_widget(zd,"hscale","amplify","hb2","0|1|0.005|0","expand");
   zdialog_add_widget(zd,"label","ampval","hb2",0,"space=5");

   zdialog_add_widget(zd,"hbox","hbcf","dialog",0,"space=8");
   zdialog_add_widget(zd,"label","labcf","hbcf",Bcurvefile,"space=5");
   zdialog_add_widget(zd,"button","loadcurve","hbcf",Bopen,"space=5");
   zdialog_add_widget(zd,"button","savecurve","hbcf",Bsave,"space=5");

   GtkWidget *frame = zdialog_widget(zd,"frame");                                //  set up curve edit
   spldat *sd = splcurve_init(frame,gradients_curvedit);
   EFgradients.curves = sd;

   sd->Nspc = 1;
   sd->fact[0] = 1;
   sd->vert[0] = 0;
   sd->nap[0] = 2;                                                               //  initial curve anchor points
   sd->apx[0][0] = 0.01;
   sd->apy[0][0] = 0.10;
   sd->apx[0][1] = 0.99;
   sd->apy[0][1] = 0.10;

   splcurve_generate(sd,0);                                                      //  generate curve data

   gradients_initz(zd);                                                          //  initialize
   return;
}


//  initialization for new image file

void gradients_names::gradients_initz(zdialog *zd)
{
   using namespace gradients_names;

   int         ii, cc, px, py;
   float       b1, *pix1;
   int         jj, sum, limit, condist[100];

   ww = E1pxm->ww;                                                               //  image dimensions
   hh = E1pxm->hh;

   cc = ww * hh * sizeof(float);                                                 //  allocate brightness map memory
   brmap1 = (float *) zmalloc(cc);
   brmap2 = (float *) zmalloc(cc);
   for (ii = 0; ii < 4; ii++)
      brmap4[ii] = (float *) zmalloc(cc);

   for (py = 0; py < hh; py++)                                                   //  map initial image brightness
   for (px = 0; px < ww; px++)
   {
      ii = py * ww + px;
      pix1 = PXMpix(E1pxm,px,py);
      b1 = 0.333 * (pix1[0] + pix1[1] + pix1[2]);                                //  pixel brightness 0-255.9
      if (b1 < 1) b1 = 1;
      brmap1[ii] = b1;
   }

   for (ii = 0; ii < 100; ii++)
      condist[ii] = 0;

   for (py = 1; py < hh; py++)                                                   //  map contrast distribution
   for (px = 1; px < ww; px++)
   {
      ii = py * ww + px;
      jj = 0.388 * fabsf(brmap1[ii] - brmap1[ii-1]);                             //  horiz. contrast, ranged 0 - 99
      if (jj > 99) jj = 99;                                                      //  trap bad image data                18.07
      condist[jj]++;
      jj = 0.388 * fabsf(brmap1[ii] - brmap1[ii-ww]);                            //  vertical
      if (jj > 99) jj = 99;                                                      //                                     18.07
      condist[jj]++;
   }

   sum = 0;
   limit = 0.99 * 2 * (ww-1) * (hh-1);                                           //  find 99th percentile contrast

   for (ii = 0; ii < 100; ii++) {
      sum += condist[ii];
      if (sum > limit) break;
   }

   contrast99 = 255.0 * ii / 100.0;                                              //  0 to 255
   if (contrast99 < 4) contrast99 = 4;                                           //  rescale low-contrast image

   zdialog_resize(zd,250,300);
   zdialog_run(zd,gradients_dialog_event,"save");                                //  run dialog - parallel

   amplify = 0;
   return;
}


//  dialog event and completion callback function

int gradients_names::gradients_dialog_event(zdialog *zd, cchar *event)
{
   using namespace gradients_names;

   spldat   *sd = EFgradients.curves;
   char     text[8];

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  cancel

   if (zd->zstat) {                                                              //  dialog complete
      if (zd->zstat == 1) edit_done(0);
      else edit_cancel(0);
      zfree(brmap1);                                                             //  free memory
      zfree(brmap2);
      brmap1 = brmap2 = 0;
      for (int ii = 0; ii < 4; ii++) {
         zfree(brmap4[ii]);
         brmap4[ii] = 0;
      }
      return 1;
   }
   
   if (strmatch(event,"apply"))                                                  //  from script
      gradients_dialog_event(zd,"amplify");

   if (strmatch(event,"blendwidth")) signal_thread();

   if (strmatch(event,"amplify")) {                                              //  slider value
      zdialog_fetch(zd,"amplify",amplify);
      snprintf(text,8,"%.2f",amplify);                                           //  numeric feedback
      zdialog_stuff(zd,"ampval",text);
      signal_thread();                                                           //  trigger update thread
   }

   if (strmatch(event,"loadcurve")) {                                            //  load saved curve
      splcurve_load(sd);
      signal_thread();
      return 0;
   }

   if (strmatch(event,"savecurve")) {                                            //  save curve to file
      splcurve_save(sd);
      return 0;
   }

   return 0;
}


//  this function is called when the curve is edited

void gradients_names::gradients_curvedit(int)
{
   signal_thread();
   return;
}


//  thread function

void * gradients_names::gradients_thread(void *)
{
   using namespace gradients_names;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      do_wthreads(gradients_wthread1,4);
      do_wthreads(gradients_wthread2,4);
      do_wthreads(gradients_wthread3,NWT);

      CEF->Fmods++;
      CEF->Fsaved = 0;
      if (amplify == 0) CEF->Fmods = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


//  working threads

void * gradients_names::gradients_wthread1(void *arg)
{
   using namespace gradients_names;

   int         ii, kk, bii, pii, dist = 0;
   int         px, px1, px2, pxinc;
   int         py, py1, py2, pyinc;
   float       b1, b4, xval, yval, grad;
   float       amp, con99;
   spldat      *sd = EFgradients.curves;

   con99 = contrast99;                                                           //  99th percentile contrast
   con99 = 1.0 / con99;                                                          //  inverted

   amp = pow(amplify,0.5);                                                       //  get amplification, 0 to 1

   bii = *((int *) arg);                                                         //  thread 0...3

   if (bii == 0) {                                                               //  direction SE
      px1 = 1; px2 = ww; pxinc = 1;
      py1 = 1; py2 = hh; pyinc = 1;
      pii = - 1 - ww;
   }

   else if (bii == 1) {                                                          //  direction SW
      px1 = ww-2; px2 = 0; pxinc = -1;
      py1 = 1; py2 = hh; pyinc = 1;
      pii = + 1 - ww;
   }

   else if (bii == 2) {                                                          //  direction NE
      px1 = 1; px2 = ww; pxinc = 1;
      py1 = hh-2; py2 = 0; pyinc = -1;
      pii = - 1 + ww;
   }

   else {   /* bii == 3 */                                                       //  direction NW
      px1 = ww-2; px2 = 0; pxinc = -1;
      py1 = hh-2; py2 = 0; pyinc = -1;
      pii = + 1 + ww;
   }

   for (ii = 0; ii < ww * hh; ii++)                                              //  initial brightness map
      brmap4[bii][ii] = brmap1[ii];

   for (py = py1; py != py2; py += pyinc)                                        //  loop all image pixels
   for (px = px1; px != px2; px += pxinc)
   {
      ii = py * ww + px;

      if (sa_stat == 3) {                                                        //  select area active
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      b1 = brmap1[ii];                                                           //  this pixel brightness
      grad = b1 - brmap1[ii+pii];                                                //   - prior pixel --> gradient

      xval = fabsf(grad) * con99;                                                //  gradient scaled 0 to 1+
      kk = 1000.0 * xval;
      if (kk > 999) kk = 999;
      yval = 1.0 + 5.0 * sd->yval[0][kk];                                        //  amplifier = 1...6
      grad = grad * yval;                                                        //  magnified gradient

      b4 = brmap4[bii][ii+pii] + grad;                                           //  pixel brightness = prior + gradient
      b4 = (1.0 - amp) * b1 + amp * b4;                                          //  apply amplifier: b4 range = b1 --> b4

      brmap4[bii][ii] = b4;                                                      //  new pixel brightness
   }

   pthread_exit(0);
}


void * gradients_names::gradients_wthread2(void *arg)
{
   using namespace gradients_names;

   int      index, ii, px, py, dist = 0;
   float    b4;

   index = *((int *) arg);                                                       //  thread 0...3

   for (py = index; py < hh; py += 4)                                            //  loop all image pixels
   for (px = 0; px < ww; px++)
   {
      ii = py * ww + px;

      if (sa_stat == 3) {                                                        //  select area active
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel is outside area
      }

      b4 = brmap4[0][ii] + brmap4[1][ii]                                         //  new brightness = average of four
         + brmap4[2][ii] + brmap4[3][ii];                                        //    calculated brightness surfaces
      b4 = 0.25 * b4;

      if (b4 < 1) b4 = 1;

      brmap2[ii] = b4;
   }

   pthread_exit(0);
}


void * gradients_names::gradients_wthread3(void *arg)
{
   using namespace gradients_names;

   float       *pix1, *pix3;
   int         index, ii, px, py, dist = 0;
   float       b1, b2, bf, f1, f2;
   float       red1, green1, blue1, red3, green3, blue3;

   index = *((int *) arg);

   for (py = index; py < hh; py += NWT)                                          //  loop all image pixels
   for (px = 0; px < ww; px++)
   {
      ii = py * ww + px;

      if (sa_stat == 3) {                                                        //  select area active
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel is outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      b1 = brmap1[ii];                                                           //  initial pixel brightness
      b2 = brmap2[ii];                                                           //  calculated new brightness

      bf = b2 / b1;                                                              //  brightness ratio

      red1 = pix1[0];                                                            //  input RGB
      green1 = pix1[1];
      blue1 = pix1[2];

      red3 = bf * red1;                                                          //  output RGB
      if (red3 > 255) red3 = 255;
      green3 = bf * green1;
      if (green3 > 255) green3 = 255;
      blue3 = bf * blue1;
      if (blue3 > 255) blue3 = 255;

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes
         f2 = 1.0 - f1;
         red3 = f1 * red3 + f2 * red1;
         green3 = f1 * green3 + f2 * green1;
         blue3 = f1 * blue3 + f2 * blue1;
      }

      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  flatten brightness distribution based on the distribution of nearby zones

namespace flatten_names
{
   int      Zinit;                                 //  zone initialization needed
   float    flatten;                               //  flatten amount, 0 - 100
   float    deband1, deband2;                      //  deband dark, bright, 0 - 100
   int      Iww, Ihh;                              //  image dimensions
   int      NZ;                                    //  no. of image zones
   int      pNZ;                                   //  prior image zones
   int      Zsize, Zrows, Zcols;                   //  zone parameters
   float    *Br;                                   //  Br[py][px]  pixel brightness
   int      *Zxlo, *Zylo, *Zxhi, *Zyhi;            //  Zxlo[ii] etc.  zone ii pixel range
   int      *Zcen;                                 //  Zcen[ii][2]  zone ii center (py,px)
   int16    *Zn;                                   //  Zn[py][px][9]  9 nearest zones for pixel: 0-200 (-1 = none)
   uint8    *Zw;                                   //  Zw[py][px][9]  zone weights for pixel: 0-100 = 1.0
   float    *Zff;                                  //  Zff[ii][1000]  zone ii flatten factors

   editfunc    EFflatten;

   int    dialog_event(zdialog* zd, cchar *event);
   void   doflatten();
   void   calczones();
   void   initzones();
   void * thread(void *);
   void * wthread(void *);
}


//  menu function

void m_flatten(GtkWidget *, cchar *menu)
{
   using namespace flatten_names;

   cchar  *title = E2X("Flatten Brightness");

   F1_help_topic = "flatten";

   Iww = Fpxb->ww;
   Ihh = Fpxb->hh;

   if (Iww * Ihh > wwhh_limit2) {
      zmessageACK(Mwin,"image too big");
      edit_cancel(0);
      return;
   }

   EFflatten.menuname = menu;
   EFflatten.menufunc = m_flatten;
   EFflatten.funcname = "flatten";
   EFflatten.FprevReq = 1;                                                       //  use preview image
   EFflatten.Farea = 2;                                                          //  select area usable
   EFflatten.Frestart = 1;                                                       //  restartable
   EFflatten.FusePL = 1;                                                         //  use with paint/lever edits OK
   EFflatten.Fscript = 1;                                                        //  scripting supported
   EFflatten.threadfunc = thread;
   if (! edit_setup(EFflatten)) return;                                          //  setup edit

   Iww = E0pxm->ww;
   Ihh = E0pxm->hh;

   if (Iww * Ihh > wwhh_limit2) {
      zmessageACK(Mwin,"image too big");
      edit_cancel(0);
      return;
   }

/***
          ______________________________________
         |        Flatten Brightness            |
         |                                      |
         | Zones  [ 123 ]   [Apply]          |
         | Flatten  =========[]============ NN  |
         | Deband Dark =========[]========= NN  |
         | Deband Bright ==========[]====== NN  |
         |                                      |
         |                     [Done] [Cancel]  |
         |______________________________________|

***/

   zdialog *zd = zdialog_new(title,Mwin,Bdone,Bcancel,null);
   EFflatten.zd = zd;

   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labreg","hb1",E2X("Zones"),"space=5");
   zdialog_add_widget(zd,"zspin","zones","hb1","1|999|1|100");                   //  zone range 1-999                   18.01
   zdialog_add_widget(zd,"button","apply","hb1",Bapply,"space=10");
   zdialog_add_widget(zd,"hbox","hb2","dialog");
   zdialog_add_widget(zd,"label","labflatten","hb2",Bflatten,"space=5");
   zdialog_add_widget(zd,"hscale","flatten","hb2","0|100|1|0","expand");
   zdialog_add_widget(zd,"hbox","hb3","dialog");
   zdialog_add_widget(zd,"label","labdeband1","hb3",E2X("Deband Dark"),"space=5");
   zdialog_add_widget(zd,"hscale","deband1","hb3","0|100|1|0","expand");
   zdialog_add_widget(zd,"hbox","hb4","dialog");
   zdialog_add_widget(zd,"label","labdeband2","hb4",E2X("Deband Bright"),"space=5");
   zdialog_add_widget(zd,"hscale","deband2","hb4","0|100|1|0","expand");

   zdialog_resize(zd,300,0);
   zdialog_run(zd,dialog_event,"save");                                          //  run dialog - parallel

   NZ = pNZ = 40;                                                                //  default zone count
   calczones();                                                                  //  adjust to fit image
   flatten = deband1 = deband2 = 0;                                              //  dialog controls = neutral
   Zinit = 1;                                                                    //  zone initialization needed
   Br = 0;                                                                       //  no memory allocated
   return;
}


//  dialog event and completion function

int flatten_names::dialog_event(zdialog *zd, cchar *event)
{
   using namespace flatten_names;
   
   if (strmatch(event,"done")) zd->zstat = 1;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  cancel

   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();
      Zinit = 1;
      doflatten();
      return 1;
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  done
         edit_fullsize();                                                        //  get full size image
         Zinit = 1;                                                              //  recalculate zones
         doflatten();                                                            //  flatten full image
         edit_done(0);                                                           //  commit edit
      }
      else edit_cancel(0);                                                       //  discard edit

      if (Br) {
         zfree(Br);                                                              //  free memory
         zfree(Zn);
         zfree(Zw);
         zfree(Zxlo);
         zfree(Zylo);
         zfree(Zxhi);
         zfree(Zyhi);
         zfree(Zcen);
         zfree(Zff);
         Br = 0;
      }

      return 1;
   }

   if (strmatch(event,"apply")) {                                                //  [apply]  (also from script)
      dialog_event(zd,"zones");
      dialog_event(zd,"flatten");
      dialog_event(zd,"deband1");
      dialog_event(zd,"deband2");
      doflatten();
   }

   if (strmatch(event,"blendwidth"))                                             //  select area blendwidth change
      doflatten();

   if (strmatch(event,"zones")) {                                                //  get zone count input
      zdialog_fetch(zd,"zones",NZ);
      if (NZ == pNZ) return 1;
      calczones();                                                               //  adjust to fit image
      zdialog_stuff(zd,"zones",NZ);                                              //  update dialog
      Zinit = 1;                                                                 //  zone initialization needed
   }

   if (strmatch(event,"flatten")) {
      zdialog_fetch(zd,"flatten",flatten);                                       //  get slider values
      doflatten();
   }

   if (strmatch(event,"deband1")) {
      zdialog_fetch(zd,"deband1",deband1);
      doflatten();
   }

   if (strmatch(event,"deband2")) {
      zdialog_fetch(zd,"deband2",deband2);
      doflatten();
   }

   return 1;
}


//  perform the flatten calculations and update the image

void flatten_names::doflatten()
{
   if (flatten > 0) {
      signal_thread();
      wait_thread_idle();                                                        //  no overlap window update and threads
      Fpaintnow();
   }
   else edit_reset();
   return;
}


//  recalculate zone count based on what fits the image dimensions
//  done only when the user zone count input changes
//  outputs: NZ, Zrows, Zcols

void flatten_names::calczones()
{
   int      gNZ, dNZ;

   Iww = E1pxm->ww;                                                              //  image dimensions
   Ihh = E1pxm->hh;

   gNZ = NZ;                                                                     //  new zone count goal
   dNZ = NZ - pNZ;                                                               //  direction of change

   while (true)
   {
      Zsize = sqrtf(Iww * Ihh / gNZ);                                            //  approx. zone size
      Zrows = Ihh / Zsize + 0.5;                                                 //  get appropriate rows and cols
      Zcols = Iww / Zsize + 0.5;
      if (Zrows < 1) Zrows = 1;                                                  //  bugfix                             18.01
      if (Zcols < 1) Zcols = 1;
      NZ = Zrows * Zcols;                                                        //  NZ matching rows x cols

      if (dNZ > 0 && NZ <= pNZ) {                                                //  no increase, try again
         if (NZ >= 999) break;
         gNZ++;
         continue;
      }

      if (dNZ < 0 && NZ >= pNZ) {                                                //  no decrease, try again
         if (NZ <= 20) break;
         gNZ--;
         continue;
      }

      if (dNZ == 0) break;
      if (dNZ > 0 && NZ > pNZ) break;
      if (dNZ < 0 && NZ < pNZ) break;
   }

   pNZ = NZ;                                                                     //  final zone count
   dNZ = 0;
   return;
}


//  build up the zones data when NZ or the image size changes
//  (preview or full size image)

void flatten_names::initzones()
{
   int      px, py, cx, cy;
   int      rx, ii, jj, kk;
   int      zww, zhh, row, col;
   float    *pix1, bright;
   float    weight[9], sumweight;
   float    D, Dthresh;

   if (Br) {
      zfree(Br);                                                                 //  free memory
      zfree(Zn);
      zfree(Zw);
      zfree(Zxlo);
      zfree(Zylo);
      zfree(Zxhi);
      zfree(Zyhi);
      zfree(Zcen);
      zfree(Zff);
      Br = 0;
   }

   Iww = E1pxm->ww;                                                              //  image dimensions
   Ihh = E1pxm->hh;

   Br = (float *) zmalloc(Iww * Ihh * sizeof(float));                            //  allocate memory
   Zn = (int16 *) zmalloc(Iww * Ihh * 9 * sizeof(int16));
   Zw = (uint8 *) zmalloc(Iww * Ihh * 9 * sizeof(uint8));
   Zxlo = (int *) zmalloc(NZ * sizeof(int));
   Zylo = (int *) zmalloc(NZ * sizeof(int));
   Zxhi = (int *) zmalloc(NZ * sizeof(int));
   Zyhi = (int *) zmalloc(NZ * sizeof(int));
   Zcen = (int *) zmalloc(NZ * 2 * sizeof(int));
   Zff = (float *) zmalloc(NZ * 1000 * sizeof(float));

   for (py = 0; py < Ihh; py++)                                                  //  get initial pixel brightness levels
   for (px = 0; px < Iww; px++)
   {
      pix1 = PXMpix(E1pxm,px,py);
      bright = pixbright(pix1);
      ii = py * Iww + px;
      Br[ii] = bright;
   }

   zww = Iww / Zcols;                                                            //  actual zone size
   zhh = Ihh / Zrows;

   for (row = 0; row < Zrows; row++)
   for (col = 0; col < Zcols; col++)                                             //  set low/high bounds per zone
   {
      ii = row * Zcols + col;
      Zxlo[ii] = col * zww;
      Zylo[ii] = row * zhh;
      Zxhi[ii] = Zxlo[ii] + zww;
      Zyhi[ii] = Zylo[ii] + zhh;
      Zcen[2*ii] = Zylo[ii] + zhh/2;
      Zcen[2*ii+1] = Zxlo[ii] + zww/2;
   }

   for (ii = 0; ii < NZ; ii++)                                                   //  compute brightness distributiion
   {                                                                             //    for each zone
      for (jj = 0; jj < 1000; jj++)
         Zff[1000*ii+jj] = 0;

      for (py = Zylo[ii]; py < Zyhi[ii]; py++)                                   //  brightness distribution
      for (px = Zxlo[ii]; px < Zxhi[ii]; px++)
      {
         pix1 = PXMpix(E1pxm,px,py);
         kk = pixbright(pix1);
         if (kk > 255) kk = 255;
         bright = 3.906 * kk;                                                    //  1000 / 256 * kk
         Zff[1000*ii+int(bright)]++;
      }

      for (jj = 1; jj < 1000; jj++)                                              //  cumulative brightness distribution
         Zff[1000*ii+jj] += Zff[1000*ii+jj-1];

      for (jj = 0; jj < 1000; jj++)                                              //  multiplier per brightness level
         Zff[1000*ii+jj] = Zff[1000*ii+jj] / (zww*zhh) * 1000 / (jj+1);          //    to make distribution flat
   }

   for (py = 0; py < Ihh; py++)                                                  //  set 9 nearest zones per pixel
   for (px = 0; px < Iww; px++)
   {
      rx = (py * Iww + px) * 9;                                                  //  index for 9 nearest zones to px/py

      row = py / zhh;                                                            //  zone containing pixel
      col = px / zww;

      ii = 0;
      for (jj = row-1; jj <= row+1; jj++)                                        //  loop 3x3 surrounding zones
      for (kk = col-1; kk <= col+1; kk++)
      {
         if (jj >= 0 && jj < Zrows && kk >= 0 && kk < Zcols)                     //  zone is not off the edge
            Zn[rx+ii] = jj * Zcols + kk;
         else Zn[rx+ii] = -1;                                                    //  edge row/col: missing neighbor
         ii++;
      }
   }

   if (zww < zhh)                                                                //  pixel to zone distance threshold
      Dthresh = 1.5 * zww;                                                       //  area influence = 0 beyond this distance
   else Dthresh = 1.5 * zhh;

   for (py = 0; py < Ihh; py++)                                                  //  set zone weights per pixel
   for (px = 0; px < Iww; px++)
   {
      rx = (py * Iww + px) * 9;                                                  //  index for 9 nearest zones to px/py

      for (ii = 0; ii < 9; ii++)                                                 //  distance to each zone center
      {
         jj = Zn[rx+ii];
         if (jj == -1) {                                                         //  missign zone
            weight[ii] = 0;                                                      //  weight = 0
            continue;
         }
         cy = Zcen[2*jj];
         cx = Zcen[2*jj+1];
         D = sqrtf((py-cy)*(py-cy) + (px-cx)*(px-cx));                           //  distance from pixel to zone center
         D = D / Dthresh;
         if (D > 1) D = 1;                                                       //  zone influence reaches zero at Dthresh
         weight[ii] = 1.0 - D;
      }

      sumweight = 0;
      for (ii = 0; ii < 9; ii++)                                                 //  zone weights based on distance
         sumweight += weight[ii];

      for (ii = 0; ii < 9; ii++)                                                 //  normalize weights, sum = 1.0
         Zw[rx+ii] = 100 * weight[ii] / sumweight;                               //  0-100 = 1.0
   }

   return;
}


//  adjust brightness distribution thread function

void * flatten_names::thread(void *)
{
   using namespace flatten_names;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      if (Zinit) initzones();                                                    //  reinitialize zones
      Zinit = 0;

      do_wthreads(wthread,NWT);

      paintlock(0);                                                              //  unblock window paint               19.0

      if (! flatten) CEF->Fmods = 0;                                             //  no modification
      else {
         CEF->Fmods++;                                                           //  image modified, not saved
         CEF->Fsaved = 0;
      }
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


//  worker thread for each CPU processor core

void * flatten_names::wthread(void *arg)
{
   using namespace flatten_names;

   int         index = *((int *) (arg));
   int         px, py, rx, ii, jj, dist = 0;
   float       *pix1, *pix3;
   float       fnew1, fnew2, fnew, fold;
   float       dold, dnew, cmax;
   float       red1, green1, blue1, red3, green3, blue3;
   float       FF, weight, bright;

   for (py = index; py < Ihh; py += NWT)                                         //  loop all image pixels
   for (px = 0; px < Iww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E1pxm->ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      red1 = pix1[0];
      green1 = pix1[1];
      blue1 = pix1[2];

      bright = 0.976 * red1 + 2.539 * green1 + 0.39 * blue1;                     //  brightness scaled 0-999.9          18.01
      
      rx = (py * Iww + px) * 9;                                                  //  index for 9 nearest zones to px/py

      FF = 0;
      for (ii = 0; ii < 9; ii++) {                                               //  loop 9 nearest zones
         weight = Zw[rx+ii];
         if (weight > 0) {                                                       //  0-100 = 1.0
            jj = Zn[rx+ii];
            FF += 0.01 * weight * Zff[1000*jj+int(bright)];                      //  sum weight * flatten factor
         }
      }

      red3 = FF * red1;                                                          //  fully flattened brightness
      green3 = FF * green1;
      blue3 = FF * blue1;
      
      fnew1 = 1 - 0.01 * deband1 * 0.001 * (1000 - bright);                      //  attenuate dark pixels
      fnew2 = 1 - 0.01 * deband2 * 0.001 * bright;                               //  attenuate bright pixels
      fnew = fnew1 * fnew2;

      fnew = fnew * 0.01 * flatten;                                              //  how much to flatten, 0 to 1
      fold = 1.0 - fnew;                                                         //  how much to retain, 1 to 0

      red3 = fnew * red3 + fold * red1;                                          //  blend new and old brightness
      green3 = fnew * green3 + fold * green1;
      blue3 = fnew * blue3 + fold * blue1;

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         dnew = sa_blendfunc(dist);                                              //  blend changes over sa_blendwidth
         dold = 1.0 - dnew;
         red3 = dnew * red3 + dold * red1;
         green3 = dnew * green3 + dold * green1;
         blue3 = dnew * blue3 + dold * blue1;
      }

      cmax = red3;                                                               //  stop overflow, keep color balance
      if (green3 > cmax) cmax = green3;
      if (blue3 > cmax) cmax = blue3;
      if (cmax > 255.9) {
         cmax = 255.9 / cmax;
         red3 = red3 * cmax;
         green3 = green3 * cmax;
         blue3 = blue3 * cmax;
      }
      
      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  Global Retinex function
//  Rescale RGB values based on entire image - eliminate color caste and reduce fog/haze.

namespace gretinex_names 
{
   editfunc    EFgretinex;                                                       //  edit function data
   int         e3ww, e3hh;
   cchar       *thread_command;

   float       Rdark, Gdark, Bdark;
   float       Rbrite, Gbrite, Bbrite;
   float       Rmpy, Gmpy, Bmpy;
   float       pRdark, pGdark, pBdark;                                           //  prior values 
   float       pRbrite, pGbrite, pBbrite;
   float       pRmpy, pGmpy, pBmpy;
   float       blend, reducebright;
   int         Fbrightpoint, Fdarkpoint;
}


//  menu function

void m_gretinex(GtkWidget *, cchar *menu)                                        //  separated from zonal retinex       18.07
{
   using namespace gretinex_names;

   int    gretinex_dialog_event(zdialog *zd, cchar *event);
   void   gretinex_mousefunc();
   void * gretinex_thread(void *);

   F1_help_topic = "Gretinex";

   EFgretinex.menuname = menu;
   EFgretinex.menufunc = m_gretinex;
   EFgretinex.funcname = "Gretinex";                                             //  function name
   EFgretinex.Farea = 2;                                                         //  select area usable
   EFgretinex.Frestart = 1;                                                      //  allow restart
   EFgretinex.Fscript = 1;                                                       //  scripting supported
   EFgretinex.threadfunc = gretinex_thread;                                      //  thread function
   EFgretinex.mousefunc = gretinex_mousefunc;                                    //  mouse function

   if (! edit_setup(EFgretinex)) return;                                         //  setup edit

   e3ww = E3pxm->ww;                                                             //  image size
   e3hh = E3pxm->hh;
   E9pxm = 0;
   
/***
          ______________________________________
         |           Global Retinex             |
         |                                      |
         |               Red  Green  Blue   +/- |
         |  Dark Point:  [__]  [__]  [__]  [__] |        0 ... 255      neutral point is 0
         | Bright Point: [__]  [__]  [__]  [__] |        0 ... 255      neutral point is 255
         |  Multiplyer:  [__]  [__]  [__]  [__] |        0 ... 5        neutral point is 1
         |                                      |
         | [auto] brightness rescale            |
         | [x] bright point  [x] dark point     |        mouse click spots
         | blend: =======[]===================  |
         | reduce bright: ==========[]========  |
         |                                      |
         |             [reset] [done] [cancel]  |
         |______________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Global Retinex"),Mwin,Breset,Bdone,Bcancel,null); 
   EFgretinex.zd = zd;
   
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","space","hb1","","space=2");
   zdialog_add_widget(zd,"vbox","vb1","hb1",0,"homog");
   zdialog_add_widget(zd,"label","space","hb1","","space=2");
   zdialog_add_widget(zd,"vbox","vb2","hb1",0,"homog");
   zdialog_add_widget(zd,"label","space","hb1","","space=2");
   zdialog_add_widget(zd,"vbox","vb3","hb1",0,"homog");
   zdialog_add_widget(zd,"label","space","hb1","","space=2");
   zdialog_add_widget(zd,"vbox","vb4","hb1",0,"homog");
   zdialog_add_widget(zd,"label","space","hb1","","space=2");
   zdialog_add_widget(zd,"vbox","vb5","hb1",0,"homog");
   
   zdialog_add_widget(zd,"label","space","vb1","");
   zdialog_add_widget(zd,"label","labdark","vb1",E2X("Dark Point"));
   zdialog_add_widget(zd,"label","labbrite","vb1",E2X("Bright Point"));
   zdialog_add_widget(zd,"label","labmpy","vb1",E2X("Multiplyer"));

   zdialog_add_widget(zd,"label","labred","vb2",Bred);
   zdialog_add_widget(zd,"zspin","Rdark","vb2","0|255|1|0","size=3");
   zdialog_add_widget(zd,"zspin","Rbrite","vb2","0|255|1|255","size=3");
   zdialog_add_widget(zd,"zspin","Rmpy","vb2","0.1|5|0.01|1","size=3");

   zdialog_add_widget(zd,"label","labgreen","vb3",Bgreen);
   zdialog_add_widget(zd,"zspin","Gdark","vb3","0|255|1|0","size=3");
   zdialog_add_widget(zd,"zspin","Gbrite","vb3","0|255|1|255","size=3");
   zdialog_add_widget(zd,"zspin","Gmpy","vb3","0.1|5|0.01|1","size=3");

   zdialog_add_widget(zd,"label","labred","vb4",Bblue);
   zdialog_add_widget(zd,"zspin","Bdark","vb4","0|255|1|0","size=3");
   zdialog_add_widget(zd,"zspin","Bbrite","vb4","0|255|1|255","size=3");
   zdialog_add_widget(zd,"zspin","Bmpy","vb4","0.1|5|0.01|1","size=3");

   zdialog_add_widget(zd,"label","laball","vb5",Ball);
   zdialog_add_widget(zd,"zspin","dark+-","vb5","-1|+1|1|0","size=3");
   zdialog_add_widget(zd,"zspin","brite+-","vb5","-1|+1|1|0","size=3");
   zdialog_add_widget(zd,"zspin","mpy+-","vb5","-1|+1|1|0","size=3");

   zdialog_add_widget(zd,"hbox","hbauto","dialog");
   zdialog_add_widget(zd,"button","autoscale","hbauto",Bauto,"space=3");
   zdialog_add_widget(zd,"label","labauto","hbauto",E2X("brightness rescale"),"space=5");

   zdialog_add_widget(zd,"hbox","hbclicks","dialog");
   zdialog_add_widget(zd,"check","brightpoint","hbclicks",E2X("click bright point"),"space=3");
   zdialog_add_widget(zd,"check","darkpoint","hbclicks",E2X("click dark point"),"space=5");
   
   zdialog_add_widget(zd,"hbox","hbb","dialog");
   zdialog_add_widget(zd,"label","labb","hbb",E2X("blend"),"space=5");
   zdialog_add_widget(zd,"hscale","blend","hbb","0|1.0|0.01|1.0","expand");
   zdialog_add_widget(zd,"hbox","hbrd","dialog");
   zdialog_add_widget(zd,"label","labrd","hbrd",E2X("reduce bright"),"space=5");
   zdialog_add_widget(zd,"hscale","reduce bright","hbrd","0|1.0|0.01|0.0","expand");

   zdialog_run(zd,gretinex_dialog_event,"save");                                 //  run dialog - parallel
   zdialog_send_event(zd,"reset");
   return;
}


//  dialog event and completion function

int gretinex_dialog_event(zdialog *zd, cchar *event)
{
   using namespace gretinex_names;
   
   void  gretinex_mousefunc();

   int      adddark, addbrite, addmpy;
   int      Fchange = 0;
   
   if (strmatch(event,"focus")) return 1;                                        //  stop loss of button event (?)      19.0
   if (strmatch(event,"done")) zd->zstat = 2;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  from f_open()
   if (strmatch(event,"reset")) zd->zstat = 1;                                   //  initz. 
   
   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  reset
         zd->zstat = 0;                                                          //  keep dialog active
         Rdark = Gdark = Bdark = 0;                                              //  set neutral values
         Rbrite = Gbrite = Bbrite = 255;
         pRdark = pGdark = pBdark = 0;                                           //  prior values = same
         pRbrite = pGbrite = pBbrite = 255;                                      //  (no change)
         Rmpy = Gmpy = Bmpy = 1.0;
         Fbrightpoint = Fdarkpoint = 0;
         blend = 1.0;
         reducebright = 0.0;

         zdialog_stuff(zd,"Rdark",0);                                            //  stuff neutral values into dialog
         zdialog_stuff(zd,"Gdark",0);
         zdialog_stuff(zd,"Bdark",0);
         zdialog_stuff(zd,"Rbrite",255);
         zdialog_stuff(zd,"Gbrite",255);
         zdialog_stuff(zd,"Bbrite",255);
         zdialog_stuff(zd,"Rmpy",1.0);
         zdialog_stuff(zd,"Gmpy",1.0);
         zdialog_stuff(zd,"Bmpy",1.0);
         zdialog_stuff(zd,"brightpoint",0);
         zdialog_stuff(zd,"darkpoint",0);

         edit_reset();
         if (E9pxm) PXM_free(E9pxm);                                             //  free memory
         E9pxm = 0;
         return 1;
      }

      else if (zd->zstat == 2) {                                                 //  done
         edit_done(0);                                                           //  commit edit
         if (E9pxm) PXM_free(E9pxm);                                             //  free memory
         E9pxm = 0;
         return 1;
      }

      else {
         edit_cancel(0);                                                         //  discard edit
         if (E9pxm) PXM_free(E9pxm);                                             //  free memory
         E9pxm = 0;
         return 1;
      }
   }

   Fbrightpoint = Fdarkpoint = 0;                                                //  disable mouse

   if (strmatch(event,"autoscale"))                                              //  auto set dark and bright points
   {
      edit_reset();

      thread_command = "autoscale";                                              //  get dark/bright limits from
      signal_thread();                                                           //    darkest/brightest pixels
      wait_thread_idle();

      zdialog_stuff(zd,"Rdark",Rdark);                                           //  update dialog widgets
      zdialog_stuff(zd,"Gdark",Gdark);
      zdialog_stuff(zd,"Bdark",Bdark);
      zdialog_stuff(zd,"Rbrite",Rbrite);
      zdialog_stuff(zd,"Gbrite",Gbrite);
      zdialog_stuff(zd,"Bbrite",Bbrite);
      zdialog_stuff(zd,"Rmpy",1.0);
      zdialog_stuff(zd,"Gmpy",1.0);
      zdialog_stuff(zd,"Bmpy",1.0);
      zdialog_stuff(zd,"brightpoint",0);
      zdialog_stuff(zd,"darkpoint",0);

      pRdark = Rdark;                                                            //  prior values = same
      pGdark = Gdark;
      pBdark = Bdark;
      pRbrite = Rbrite; 
      pGbrite = Gbrite; 
      pBbrite = Bbrite;
      pRmpy = Rmpy;
      pGmpy = Gmpy;
      pBmpy = Bmpy;
      return 1;
   }
   
   if (strmatch(event,"brightpoint")) {
      zdialog_fetch(zd,"brightpoint",Fbrightpoint);
      if (Fbrightpoint) {
         zdialog_stuff(zd,"darkpoint",0);
         Fdarkpoint = 0;
      }
   }

   if (strmatch(event,"darkpoint")) {
      zdialog_fetch(zd,"darkpoint",Fdarkpoint);
      if (Fdarkpoint) {
         zdialog_stuff(zd,"brightpoint",0);
         Fbrightpoint = 0;
      }
   }
   
   if (strstr("brightpoint darkpoint",event)) {                                  //  brightpoint or darkpoint
      takeMouse(gretinex_mousefunc,dragcursor);                                  //     connect mouse function
      return 1;
   }
   else {
      zdialog_stuff(zd,"brightpoint",0);                                         //  reset zdialog checkboxes
      zdialog_stuff(zd,"darkpoint",0);
   }
   
   adddark = addbrite = addmpy = 0;
   if (strmatch(event,"dark+-")) zdialog_fetch(zd,"dark+-",adddark);             //  [+/-] button
   if (strmatch(event,"brite+-")) zdialog_fetch(zd,"brite+-",addbrite);
   if (strmatch(event,"mpy+-")) zdialog_fetch(zd,"mpy+-",addmpy);
   
   if (adddark) {
      zdialog_fetch(zd,"Rdark",Rdark);                                           //  increment dark levels
      zdialog_fetch(zd,"Gdark",Gdark);
      zdialog_fetch(zd,"Bdark",Bdark);
      Rdark += adddark;
      Gdark += adddark;
      Bdark += adddark;
      if (Rdark < 0) Rdark = 0;
      if (Gdark < 0) Gdark = 0;
      if (Bdark < 0) Bdark = 0;
      if (Rdark > 255) Rdark = 255;
      if (Gdark > 255) Gdark = 255;
      if (Bdark > 255) Bdark = 255;
      zdialog_stuff(zd,"Rdark",Rdark);
      zdialog_stuff(zd,"Gdark",Gdark);
      zdialog_stuff(zd,"Bdark",Bdark);
   }

   if (addbrite) {                                                               //  increment bright levels
      zdialog_fetch(zd,"Rbrite",Rbrite);
      zdialog_fetch(zd,"Gbrite",Gbrite);
      zdialog_fetch(zd,"Bbrite",Bbrite);
      Rbrite += addbrite;
      Gbrite += addbrite;
      Bbrite += addbrite;
      if (Rbrite < 0) Rbrite = 0;
      if (Gbrite < 0) Gbrite = 0;
      if (Bbrite < 0) Bbrite = 0;
      if (Rbrite > 255) Rbrite = 255;
      if (Gbrite > 255) Gbrite = 255;
      if (Bbrite > 255) Bbrite = 255;
      zdialog_stuff(zd,"Rbrite",Rbrite);
      zdialog_stuff(zd,"Gbrite",Gbrite);
      zdialog_stuff(zd,"Bbrite",Bbrite);
   }

   if (addmpy) {                                                                 //  increment mpy factors
      zdialog_fetch(zd,"Rmpy",Rmpy);
      zdialog_fetch(zd,"Gmpy",Gmpy);
      zdialog_fetch(zd,"Bmpy",Bmpy);
      Rmpy += 0.01 * addmpy;
      Gmpy += 0.01 * addmpy;
      Bmpy += 0.01 * addmpy;
      if (Rmpy < 0.1) Rmpy = 0.1;
      if (Gmpy < 0.1) Gmpy = 0.1;
      if (Bmpy < 0.1) Bmpy = 0.1;
      if (Rmpy > 5) Rmpy = 5;
      if (Gmpy > 5) Gmpy = 5;
      if (Bmpy > 5) Bmpy = 5;
      zdialog_stuff(zd,"Rmpy",Rmpy);
      zdialog_stuff(zd,"Gmpy",Gmpy);
      zdialog_stuff(zd,"Bmpy",Bmpy);
   }
   
   zdialog_fetch(zd,"Rdark",Rdark);                                              //  get all params
   zdialog_fetch(zd,"Gdark",Gdark);
   zdialog_fetch(zd,"Bdark",Bdark);
   zdialog_fetch(zd,"Rbrite",Rbrite);
   zdialog_fetch(zd,"Gbrite",Gbrite);
   zdialog_fetch(zd,"Bbrite",Bbrite);
   zdialog_fetch(zd,"Rmpy",Rmpy);
   zdialog_fetch(zd,"Gmpy",Gmpy);
   zdialog_fetch(zd,"Bmpy",Bmpy);
   
   Fchange = 0;
   if (Rdark != pRdark) Fchange = 1;                                             //  detect changed params
   if (Gdark != pGdark) Fchange = 1;
   if (Bdark != pBdark) Fchange = 1;
   if (Rbrite != pRbrite) Fchange = 1;
   if (Gbrite != pGbrite) Fchange = 1;
   if (Bbrite != pBbrite) Fchange = 1;
   if (Rmpy != pRmpy) Fchange = 1;
   if (Gmpy != pGmpy) Fchange = 1;
   if (Bmpy != pBmpy) Fchange = 1;
   
   pRdark = Rdark;                                                               //  remember values for change detection
   pGdark = Gdark;
   pBdark = Bdark;
   pRbrite = Rbrite; 
   pGbrite = Gbrite; 
   pBbrite = Bbrite; 
   pRmpy = Rmpy; 
   pGmpy = Gmpy; 
   pBmpy = Bmpy; 
   
   if (Fchange) {                                                                //  global params changed
      thread_command = "global";
      signal_thread();                                                           //  update image
      wait_thread_idle();
      Fchange = 0;
   }
   
   if (strstr("blend, reduce bright",event)) {                                   //  blend params changed
      zdialog_fetch(zd,"blend",blend);
      zdialog_fetch(zd,"reduce bright",reducebright);
      thread_command = "blend";
      signal_thread();                                                           //  update image
      wait_thread_idle();
   }

   return 1;
}


//  get dark point or bright point from mouse click position

void gretinex_mousefunc()
{
   using namespace gretinex_names;
   
   int         px, py, dx, dy;
   float       red, green, blue;
   float       *ppix;
   char        mousetext[60];
   zdialog     *zd = EFgretinex.zd;
   
   if (! zd) {
      freeMouse();
      return;
   }

   if (! Fbrightpoint && ! Fdarkpoint) {
      freeMouse();
      return;
   }

   if (! LMclick) return;
   LMclick = 0;

   px = Mxclick;                                                                 //  mouse click position
   py = Myclick;

   if (px < 2) px = 2;                                                           //  pull back from edge
   if (px > E3pxm->ww-3) px = E3pxm->ww-3;
   if (py < 2) py = 2;
   if (py > E3pxm->hh-3) py = E3pxm->hh-3;

   red = green = blue = 0;

   for (dy = -1; dy <= 1; dy++)                                                  //  3x3 block around mouse position
   for (dx = -1; dx <= 1; dx++)
   {
      ppix = PXMpix(E1pxm,px+dx,py+dy);                                          //  input image
      red += ppix[0];
      green += ppix[1];
      blue += ppix[2];
   }

   red = red / 9.0;                                                              //  mean RGB levels
   green = green / 9.0;
   blue = blue / 9.0;

   snprintf(mousetext,60,"3x3 pixels RGB: %.0f %.0f %.0f \n",red,green,blue);
   poptext_mouse(mousetext,10,10,0,3);
   
   if (Fbrightpoint) {                                                           //  click pixel is new bright point
      Rbrite= red;
      Gbrite = green;
      Bbrite = blue;
      zdialog_stuff(zd,"Rbrite",Rbrite);                                         //  stuff values into dialog
      zdialog_stuff(zd,"Gbrite",Gbrite);
      zdialog_stuff(zd,"Bbrite",Bbrite);
   }

   if (Fdarkpoint) {                                                             //  click pixel is new dark point
      Rdark = red;
      Gdark = green;
      Bdark = blue;
      zdialog_stuff(zd,"Rdark",Rdark);                                           //  stuff values into dialog
      zdialog_stuff(zd,"Gdark",Gdark);
      zdialog_stuff(zd,"Bdark",Bdark);
   }
   
   if (Fbrightpoint || Fdarkpoint) {
      thread_command = "global";
      signal_thread();                                                           //  trigger image update
      wait_thread_idle();
   }
   
   return;
}


//  thread function - multiple working threads to update image

void * gretinex_thread(void *)
{
   using namespace gretinex_names;

   void  * gretinex_wthread1(void *arg);                                         //  worker threads
   void  * gretinex_wthread2(void *arg);
   void  * gretinex_wthread3(void *arg);
   
   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request
      
      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      if (strmatch(thread_command,"autoscale")) {
         gretinex_wthread1(0);                                                   //  worker thread for autoscale
         thread_command = "global";
      }
      
      if (strmatch(thread_command,"global"))
      {
         do_wthreads(gretinex_wthread2,NWT);                                     //  worker thread for image RGB rescale

         if (E9pxm) PXM_free(E9pxm);                                             //  E9 image = global retinex output
         E9pxm = PXM_copy(E3pxm);

         thread_command = "blend";                                               //  auto blend after global retinex
      }
      
      if (strmatch(thread_command,"blend"))
      {
         if (! E9pxm) E9pxm = PXM_copy(E3pxm);                                   //  stop thread crash                  19.0
         do_wthreads(gretinex_wthread3,NWT);                                     //  worker thread for image blend
      }

      CEF->Fmods++;                                                              //  image modified
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop warning
}


//  worker thread function - autoscale retinex

void * gretinex_wthread1(void *)
{
   using namespace gretinex_names;
   
   int      ii, dist = 0;
   int      px, py, dx, dy;
   int      red, green, blue;
   float    *pix1;

   Rdark = Gdark = Bdark = 255;
   Rbrite = Gbrite = Bbrite = 0;
   Rmpy = Gmpy = Bmpy = 1.0;
   Fbrightpoint = Fdarkpoint = 0;

   for (py = 1; py < E1pxm->hh-1; py++)
   for (px = 1; px < E1pxm->ww-1; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E1pxm->ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside area
      }

      red = green = blue = 0;

      for (dy = -1; dy <= 1; dy++)                                               //  3x3 block around mouse position
      for (dx = -1; dx <= 1; dx++)
      {
         pix1 = PXMpix(E1pxm,px+dx,py+dy);                                       //  input image
         red += pix1[0];
         green += pix1[1];
         blue += pix1[2];
      }

      red = red / 9.0;                                                           //  mean RGB levels
      green = green / 9.0;
      blue = blue / 9.0;
      
      if (red < Rdark) Rdark = red;                                              //  update limits
      if (green < Gdark) Gdark = green;
      if (blue < Bdark) Bdark = blue;
      if (red > Rbrite) Rbrite = red;
      if (green > Gbrite) Gbrite = green;
      if (blue > Bbrite) Bbrite = blue;
   }

   return 0;                                                                     //  not executed, avoid gcc warning
}


//  worker thread function - scale image RGB values

void * gretinex_wthread2(void *arg)
{
   using namespace gretinex_names;
   
   int      ii, index, dist = 0;
   int      px, py;
   float    R1, G1, B1, R3, G3, B3;
   float    f1, f2, F, cmax;
   float    *pix1, *pix3;

   index = *((int *) arg);
   
   for (py = index; py < E3pxm->hh; py += NWT)                                   //  loop all image pixels
   for (px = 0; px < E3pxm->ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * E3pxm->ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel

      R1 = pix1[0];                                                              //  input RGB values
      G1 = pix1[1];
      B1 = pix1[2];
      
      R1 = 255 * (R1 - Rdark) / (Rbrite - Rdark);                                //  rescale for full 0-255 range
      G1 = 255 * (G1 - Gdark) / (Gbrite - Gdark);
      B1 = 255 * (B1 - Bdark) / (Bbrite - Bdark);

      if (R1 < 0) R1 = 0;
      if (G1 < 0) G1 = 0;
      if (B1 < 0) B1 = 0;

      R3 = R1 * Rmpy;
      G3 = G1 * Gmpy;
      B3 = B1 * Bmpy;

      if (R3 > 255 || G3 > 255 || B3 > 255) {                                    //  stop overflow
         cmax = R3;
         if (G3 > cmax) cmax = G3;
         if (B3 > cmax) cmax = B3;
         F = 255 / cmax;
         R3 *= F;
         G3 *= F;
         B3 *= F;
      }

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         R3 = f1 * R3 + f2 * R1;
         G3 = f1 * G3 + f2 * G1;
         B3 = f1 * B3 + f2 * B1;
      }

      pix3[0] = R3;                                                              //  output RGB values
      pix3[1] = G3;
      pix3[2] = B3;
   }

   pthread_exit(0);
}


//  worker thread function - blend input and output images, attenuate bright pixels

void * gretinex_wthread3(void *arg)
{
   using namespace gretinex_names;

   int      index = *((int *) arg);
   int      px, py;
   int      ii, dist = 0;
   float    *pix1, *pix2, *pix3;
   float    R1, G1, B1, R2, G2, B2, R3, G3, B3;
   float    F1, F2, Lmax;
   
   for (py = index; py < e3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < e3ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * e3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside area
      }
      
      pix1 = PXMpix(E1pxm,px,py);                                                //  input image pixel
      pix2 = PXMpix(E9pxm,px,py);                                                //  global retinex image pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output image pixel

      R1 = pix1[0];                                                              //  input color - original image
      G1 = pix1[1];
      B1 = pix1[2];

      R2 = pix2[0];                                                              //  input color - retinex image
      G2 = pix2[1];
      B2 = pix2[2];

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         F1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         F2 = 1.0 - F1;
         R2 = F1 * R2 + F2 * R1;
         G2 = F1 * G2 + F2 * G1;
         B2 = F1 * B2 + F2 * B1;
      }

      Lmax = R1;                                                                 //  max. RGB input
      if (G1 > Lmax) Lmax = G1;
      if (B1 > Lmax) Lmax = B1;
      
      F1 = blend;                                                                //  0 ... 1  >>  E1 ... E3
      F2 = reducebright;                                                         //  0 ... 1
      F1 = F1 * (1.0 - F2 * Lmax / 255.0);                                       //  reduce F1 for high F2 * Lmax

      R3 = F1 * R2 + (1.0 - F1) * R1;                                            //  output RGB = blended inputs
      G3 = F1 * G2 + (1.0 - F1) * G1;
      B3 = F1 * B2 + (1.0 - F1) * B1;
      
      pix3[0] = R3;                                                              //  output RGB values
      pix3[1] = G3;
      pix3[2] = B3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  Zonal Retinex function
//  Rescale RGB values based on surrounding zones: increase local brightness range.

namespace zretinex_names 
{
   editfunc    EFzretinex;                                                       //  edit function data
   int         e3ww, e3hh;
   cchar       *thread_command;
   float       blend, reducedark, reducebright;
   
   int         maxzones = 10000;
   int         Nzones = 100, Pzones = 0;                                         //  zone count, 1-10000
   int         zsize, zrows, zcols, zww, zhh;                                    //  zone data

   typedef struct {                                                              //  zone structure
      int      cx, cy;                                                           //  zone center in image
      float    minR, minG, minB;                                                 //  RGB minimum values in zone
      float    maxR, maxG, maxB;                                                 //  RGB mazimum values in zone
   }           zone_t;

   zone_t      *zones = 0;                                                       //  up to 10000 zones

   int         *zoneindex = 0;                                                   //  zoneindex[ii][z] pixel ii, 25 zones
   float       *zoneweight = 0;                                                  //  zoneweight[ii][z] zone weights
}


//  menu function

void m_zretinex(GtkWidget *, cchar *menu)                                        //  overhauled 18.07
{
   using namespace zretinex_names;

   int    zretinex_dialog_event(zdialog *zd, cchar *event);
   void * zretinex_thread(void *);

   F1_help_topic = "Zretinex";

   EFzretinex.menuname = menu;
   EFzretinex.menufunc = m_zretinex;
   EFzretinex.funcname = "Zretinex";                                             //  function name
   EFzretinex.Farea = 2;                                                         //  select area usable
   EFzretinex.Frestart = 1;                                                      //  allow restart
   EFzretinex.Fscript = 1;                                                       //  scripting supported
   EFzretinex.threadfunc = zretinex_thread;                                      //  thread function

   if (! edit_setup(EFzretinex)) return;                                         //  setup edit

   e3ww = E3pxm->ww;                                                             //  image size
   e3hh = E3pxm->hh;
   
   double zmalloc_size = e3ww * e3hh * 25 * sizeof(float);                       //  25 zones per pixel
   if (zmalloc_size > 8000.0 * MEGA) {
      zmessageACK(Mwin,E2X("image is too large"));                               //  about 80 megapixels
      edit_cancel(0);
      return;
   }
   
   E9pxm = PXM_copy(E1pxm);
   
/***
          ______________________________________
         |            Zonal Retinex             |
         |                                      |
         | zone count: [___]  [apply]           |
         | blend: =======[]===================  |
         | reduce dark: ==========[]==========  |                                //  18.07
         | reduce bright: ==========[]========  |
         |                                      |
         |             [reset] [done] [cancel]  |
         |______________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Zonal Retinex"),Mwin,Breset,Bdone,Bcancel,null); 
   EFzretinex.zd = zd;
   
   zdialog_add_widget(zd,"hbox","hbz","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labzs","hbz",E2X("zone count:"),"space=5");
   zdialog_add_widget(zd,"zspin","zone count","hbz","20|10000|10|100");
   zdialog_add_widget(zd,"button","apply","hbz",Bapply,"space=3");
   zdialog_add_widget(zd,"hbox","hbb","dialog");
   zdialog_add_widget(zd,"label","labb","hbb",E2X("blend"),"space=5");
   zdialog_add_widget(zd,"hscale","blend","hbb","0|1.0|0.01|1.0","expand");
   zdialog_add_widget(zd,"hbox","hbrd","dialog");
   zdialog_add_widget(zd,"label","labrd","hbrd",E2X("reduce dark"),"space=5");
   zdialog_add_widget(zd,"hscale","reduce dark","hbrd","0|1.0|0.01|0.0","expand");
   zdialog_add_widget(zd,"hbox","hbrl","dialog");
   zdialog_add_widget(zd,"label","labrd","hbrl",E2X("reduce bright"),"space=5");
   zdialog_add_widget(zd,"hscale","reduce bright","hbrl","0|1.0|0.01|0.0","expand");

   zdialog_run(zd,zretinex_dialog_event,"save");                                 //  run dialog - parallel
   zdialog_send_event(zd,"reset");
   return;
}


//  dialog event and completion function

int zretinex_dialog_event(zdialog *zd, cchar *event)
{
   using namespace zretinex_names;

   void   zretinex_zonesetup(zdialog *zd);
   
   if (strmatch(event,"done")) zd->zstat = 2;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  from f_open()
   if (strmatch(event,"reset")) zd->zstat = 1;                                   //  initz. 
   
   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  reset
         zd->zstat = 0;                                                          //  keep dialog active
         blend = 1.0;
         reducedark = 0.0;
         reducebright = 0.0;
         edit_reset();
         PXM_free(E9pxm);
         E9pxm = PXM_copy(E1pxm);
         return 1;
      }

      else if (zd->zstat == 2) {                                                 //  done
         edit_done(0);                                                           //  commit edit
         PXM_free(E9pxm);
         if (zones) zfree(zones);
         if (zoneindex) zfree(zoneindex);
         if (zoneweight) zfree(zoneweight);
         zones = 0;
         zoneindex = 0;
         zoneweight = 0;
         return 1;
      }

      else {
         edit_cancel(0);                                                         //  discard edit
         PXM_free(E9pxm);
         if (zones) zfree(zones);
         if (zoneindex) zfree(zoneindex);
         if (zoneweight) zfree(zoneweight);
         zones = 0;
         zoneindex = 0;
         zoneweight = 0;
         return 1;
      }
   }

   if (strmatch(event,"apply")) {                                                //  get new zone count
      zretinex_zonesetup(zd);                                                    //  zone setups
      thread_command = "apply";
      signal_thread();                                                           //  update image
      wait_thread_idle();
   }

   if (strmatch(event,"blend")) {                                                //  blend param changed
      zdialog_fetch(zd,"blend",blend);
      thread_command = "blend";
      signal_thread();                                                           //  update image
      wait_thread_idle();
   }

   if (strstr(event,"reduce")) {                                                 //  reduce param changed
      zdialog_fetch(zd,"reduce dark",reducedark);                                //  18.07
      zdialog_fetch(zd,"reduce bright",reducebright);
      thread_command = "blend";
      signal_thread();                                                           //  update image
      wait_thread_idle();
   }

   return 1;
}


//  setup new zone parameters and allocate memory

void zretinex_zonesetup(zdialog *zd)
{
   using namespace zretinex_names;

   int      goal, dirc;
   int      row, col, xlo, ylo;
   int      ii, nn, cx, cy;

   Pzones = Nzones;                                                              //  prior count
   zdialog_fetch(zd,"zone count",Nzones);                                        //  new count

   goal = Nzones;                                                                //  new zone count goal
   dirc = Nzones - Pzones;                                                       //  direction of change
   if (dirc == 0) dirc = 1;

   while (true)
   {
      zsize = sqrt(e3ww * e3hh / goal);                                          //  approx. zone size
      zrows = e3hh / zsize;                                                      //  get appropriate rows and cols
      zcols = e3ww / zsize;
      if (zrows < 1) zrows = 1;
      if (zcols < 1) zcols = 1;
      Nzones = zrows * zcols;                                                    //  matching rows x cols

      if (dirc > 0 && Nzones <= Pzones) {                                        //  no increase, try again
         if (Nzones == maxzones) break;
         goal++;
         continue;
      }

      if (dirc < 0 && Nzones >= Pzones) {                                        //  no decrease, try again
         if (Nzones == 1) break;
         goal--;
         continue;
      }

      if (dirc > 0 && Nzones > Pzones) break;                                    //  done
      if (dirc < 0 && Nzones < Pzones) break;
   }
   
   zdialog_stuff(zd,"zone count",Nzones);                                        //  update zdialog widget

   if (zones) zfree(zones);                                                      //  allocate zone memory
   zones = (zone_t *) zmalloc(Nzones * sizeof(zone_t));

   zww = e3ww / zcols;                                                           //  zone width, height
   zhh = e3hh / zrows;

   nn = e3ww * e3hh * 25;                                                        //  25 neighbor zones per pixel

   if (zoneindex) zfree(zoneindex);                                              //  allocate pixel zone index
   zoneindex = (int *) zmalloc(nn * sizeof(int));

   if (zoneweight) zfree(zoneweight);                                            //  allocate pixel zone weight
   zoneweight = (float *) zmalloc(nn * sizeof(float));

   for (row = 0; row < zrows; row++)                                             //  loop all zones
   for (col = 0; col < zcols; col++)
   {
      xlo = col * zww;                                                           //  zone low pixel
      ylo = row * zhh;
      cx = xlo + zww/2;                                                          //  zone center pixel
      cy = ylo + zhh/2;
      ii = row * zcols + col;                                                    //  zone index
      zones[ii].cx = cx;                                                         //  zone center
      zones[ii].cy = cy;
   }

   return;
}


//  thread function - multiple working threads to update image

void * zretinex_thread(void *)
{
   using namespace zretinex_names;

   void * zretinex_wthread1(void *arg);
   void * zretinex_wthread2(void *arg);
   void * zretinex_wthread3(void *arg);
   
   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      if (strmatch(thread_command,"apply"))                                      //  update zones and image
      {
         do_wthreads(zretinex_wthread1,NWT);                                     //  compute zone RGB levels and weights
         do_wthreads(zretinex_wthread2,NWT);                                     //  compute new pixel RGB values
         thread_command = "blend";                                               //  auto blend after
      }
      
      if (strmatch(thread_command,"blend"))
         do_wthreads(zretinex_wthread3,NWT);                                     //  blend input and retinex images

      CEF->Fmods++;                                                              //  image modified
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop warning
}


//  worker thread to compute zone RGB levels and weights per image pixel

void * zretinex_wthread1(void *arg)
{
   using namespace zretinex_names;

   int      index = *((int *) arg);
   int      px, py, row, col;
   int      xlo, xhi, ylo, yhi, cx, cy;
   int      ii, jj, kk;
   float    minR, minG, minB, maxR, maxG, maxB;
   float    dist, maxdist, weight, sumweight;
   float    *pix;

   for (row = index; row < zrows; row += NWT)                                    //  loop all zones
   for (col = 0; col < zcols; col++)
   {
      xlo = col * zww;                                                           //  zone low pixel
      ylo = row * zhh;
      xhi = xlo + zww;                                                           //  zone high pixel
      yhi = ylo + zhh;

      minR = minG = minB = 256;
      maxR = maxG = maxB = 0;

      for (py = ylo; py < yhi; py++)                                             //  find min/max RGB in zone
      for (px = xlo; px < xhi; px++)
      {
         pix = PXMpix(E1pxm,px,py);
         if (pix[0] < minR) minR = pix[0];
         if (pix[1] < minG) minG = pix[1];
         if (pix[2] < minB) minB = pix[2];
         if (pix[0] > maxR) maxR = pix[0];
         if (pix[1] > maxG) maxG = pix[1];
         if (pix[2] > maxB) maxB = pix[2];
      }
      
      ii = row * zcols + col;                                                    //  zone index
      zones[ii].minR = minR;                                                     //  zone RGB range
      zones[ii].minG = minG;
      zones[ii].minB = minB;
      zones[ii].maxR = maxR;
      zones[ii].maxG = maxG;
      zones[ii].maxB = maxB;
   }

   for (py = index; py < e3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < e3ww; px++)
   {
      row = py / zhh;                                                            //  zone containing pixel
      col = px / zww;

      ii = (py * e3ww + px) * 25;                                                //  base zone index for pixel
      
      for (jj = row-2; jj <= row+2; jj++)                                        //  loop 25 neighboring zones
      for (kk = col-2; kk <= col+2; kk++)
      {
         if (jj >= 0 && jj < zrows && kk >= 0 && kk < zcols)                     //  neighbor zone number
            zoneindex[ii] = jj * zcols + kk;                                     //    --> pixel zone index            
         else zoneindex[ii] = -1;                                                //  pixel zone on edge, missing neighbor
         ii++;
      }
   }
   
   if (zww < zhh) maxdist = 2.5 * zww;
   else maxdist = 2.5 * zhh;

   for (py = index; py < e3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < e3ww; px++)
   {
      row = py / zhh;                                                            //  zone containing pixel
      col = px / zww;

      ii = (py * e3ww + px) * 25;                                                //  base zone index for pixel
      
      for (jj = 0; jj < 25; jj++)                                                //  loop neighbor zones
      {
         kk = zoneindex[ii+jj];
         if (kk < 0) {                                                           //  neighbor missing 
            zoneweight[ii+jj] = 0;
            continue;
         }
         cx = zones[kk].cx;                                                      //  zone center
         cy = zones[kk].cy;
         dist = sqrtf((px-cx)*(px-cx) + (py-cy)*(py-cy));                        //  distance from (px,py)
         weight = 1.0 - dist / maxdist;                                          //  dist 0..max --> weight 1..0
         if (weight < 0) weight = 0;
         zoneweight[ii+jj] = weight;
      }

      sumweight = 0;
      for (jj = 0; jj < 25; jj++)                                                //  get sum of zone weights
         sumweight += zoneweight[ii+jj];
      
      for (jj = 0; jj < 25; jj++)                                                //  make weights add up to 1.0
         zoneweight[ii+jj] = zoneweight[ii+jj] / sumweight;
   }

   pthread_exit(0);
}


//  worker thread to compute new image RGB values

void * zretinex_wthread2(void *arg)
{
   using namespace zretinex_names;

   int      index = *((int *) arg);
   int      px, py, ii, jj, kk, dist = 0;
   float    minR, minG, minB, maxR, maxG, maxB;
   float    R1, G1, B1, R2, G2, B2;
   float    weight, F, f1, f2;
   float    *pix1, *pix2;
   
   for (py = index; py < e3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < e3ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * e3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside area
      }

      minR = minG = minB = 0;
      maxR = maxG = maxB = 0;
      
      ii = (py * e3ww + px) * 25;                                                //  base zone index for pixel
      
      for (jj = 0; jj < 25; jj++)                                                //  loop neighbor zones
      {
         kk = zoneindex[ii+jj];                                                  //  zone number
         if (kk < 0) continue;
         weight = zoneweight[ii+jj];                                             //  zone weight
         minR += weight * zones[kk].minR;                                        //  sum weighted RGB range
         minG += weight * zones[kk].minG;                                        //    for neighbor zones
         minB += weight * zones[kk].minB;
         maxR += weight * zones[kk].maxR;
         maxG += weight * zones[kk].maxG;
         maxB += weight * zones[kk].maxB;
      }
      
      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix2 = PXMpix(E9pxm,px,py);                                                //  output pixel

      R1 = pix1[0];
      G1 = pix1[1];
      B1 = pix1[2];
      
      R2 = 255 * (R1 - minR) / (maxR - minR + 1);                                //  avoid poss. divide by zero
      G2 = 255 * (G1 - minG) / (maxG - minG + 1);
      B2 = 255 * (B1 - minB) / (maxB - minB + 1);

      if (R2 < 0) R2 = 0;                                                        //  stop underflow
      if (G2 < 0) G2 = 0;
      if (B2 < 0) B2 = 0;
      
      if (R2 > 255 || G2 > 255 || B2 > 255) {                                    //  stop overflow
         F = R2;
         if (G2 > F) F = G2;
         if (B2 > F) F = B2;
         F = 255 / F;
         R2 *= F;
         G2 *= F;
         B2 *= F;
      }

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         f1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         f2 = 1.0 - f1;
         R2 = f1 * R2 + f2 * R1;
         G2 = f1 * G2 + f2 * G1;
         B2 = f1 * B2 + f2 * B1;
      }

      pix2[0] = R2;
      pix2[1] = G2;
      pix2[2] = B2;
   }

   pthread_exit(0);
}


//  worker thread to blend input image with retinex image
//  and attenuate bright pixels

void * zretinex_wthread3(void *arg)
{
   using namespace zretinex_names;

   int      index = *((int *) arg);
   int      px, py;
   int      ii, dist = 0;
   float    *pix1, *pix2, *pix3;
   float    R1, G1, B1, R2, G2, B2, R3, G3, B3;
   float    F1, F2, Lmax;
   
   for (py = index; py < e3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < e3ww; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * e3ww + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside area
      }

      pix1 = PXMpix(E1pxm,px,py);                                                //  input image pixel
      pix2 = PXMpix(E9pxm,px,py);                                                //  retinex image pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output image pixel

      R1 = pix1[0];                                                              //  input color - original image
      G1 = pix1[1];
      B1 = pix1[2];

      R2 = pix2[0];                                                              //  input color - retinex image
      G2 = pix2[1];
      B2 = pix2[2];

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  select area is active,
         F1 = sa_blendfunc(dist);                                                //    blend changes over sa_blendwidth
         F2 = 1.0 - F1;
         R2 = F1 * R2 + F2 * R1;
         G2 = F1 * G2 + F2 * G1;
         B2 = F1 * B2 + F2 * B1;
      }

      Lmax = R1;                                                                 //  max. RGB input
      if (G1 > Lmax) Lmax = G1;
      if (B1 > Lmax) Lmax = B1;
      Lmax = Lmax / 255.0;                                                       //  scale 0 - 1
      
      F1 = blend;                                                                //  0 ... 1  >>  E1 ... E3

      F2 = reducedark;                                                           //  18.07
      F1 = F1 * (1.0 - F2 * (1.0 - Lmax));                                       //  reduce F1 for high F2 * (1 - Lmax)

      F2 = reducebright;                                                         //  0 ... 1
      F1 = F1 * (1.0 - F2 * Lmax);                                               //  reduce F1 for high F2 * Lmax

      R3 = F1 * R2 + (1.0 - F1) * R1;                                            //  output RGB = blended inputs
      G3 = F1 * G2 + (1.0 - F1) * G1;
      B3 = F1 * B2 + (1.0 - F1) * B1;
      
      pix3[0] = R3;                                                              //  output RGB values
      pix3[1] = G3;
      pix3[2] = B3;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  Revise color in selected image areas.
//  Show RGB values for 1-9 pixels selected with mouse-clicks.
//  Revise RGB values for the selected pixels and then revise all other
//  pixels to match, using a distance-weighted average of the selected pixels.

namespace zonal_colors_names
{
   int      zonal_colors_pixel[9][2];                                            //  last 9 pixel locations clicked
   float    zonal_colors_val1[9][3];                                             //  original RGB values, 0 to 255.9
   float    zonal_colors_val3[9][3];                                             //  revised RGB values, 0 to 255.9
   int      zonal_colors_metric = 1;                                             //  1/2/3 = /RGB/EV/OD
   int      zonal_colors_delta = 0;                                              //  flag, delta mode
   int      zonal_colors_npix;                                                   //  no. of selected pixels
   double   zonal_colors_time;                                                   //  last click time
   int      zonal_colors_blend;                                                  //  blend factor, 1 to 100

   editfunc EFrgbrevise;                                                         //  edit function parameters
   int      E3ww, E3hh;

   #define RGB2EV(rgb) (log2(rgb)-7)                                             //  RGB 0.1 to 255.9 >> EV -10 to +1
   #define EV2RGB(ev) (pow(2,ev+7))                                              //  inverse
   #define RGB2OD(rgb) (2-log10(100*rgb/256))                                    //  RGB 0.1 to 255.9 >> OD 3.4 to 0.000017
   #define OD2RGB(od) (pow(10,2.40824-od))                                       //  inverse
}


//  menu function

void m_zonal_colors(GtkWidget *, cchar *menu)
{
   using namespace zonal_colors_names;

   int    zonal_colors_dialog_event(zdialog *zd, cchar *event);
   void   zonal_colors_mousefunc();
   void * zonal_colors_thread(void *);
   void   zonal_colors_stuff();

   cchar       *limits;
   zdialog     *zd;
   cchar       *mess = E2X("Click image to select areas.");

   F1_help_topic = "zonal_colors";

   EFrgbrevise.menufunc = m_zonal_colors;
   EFrgbrevise.funcname = "zonal_colors";                                        //  setup edit function
   EFrgbrevise.threadfunc = zonal_colors_thread;
   EFrgbrevise.mousefunc = zonal_colors_mousefunc;
   EFrgbrevise.Farea = 1;
   if (! edit_setup(EFrgbrevise)) return;
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

/***

      Click image to select areas.
      [x] delta
      Metric: (o) RGB  (o) EV  (o) OD
      Pixel          Red    Green   Blue
      A xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values
      B xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values
      C xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values
      D xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values
      E xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values
      F xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values
      G xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values
      H xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values
      I xxxx xxxx   [    ]  [   ]  [   ]                                         //  spin buttons for RGB/EV/OD values

      Blend  ==============[]================

                     [reset] [done] [cancel]
***/

   zd = zdialog_new(E2X("Local Color"),Mwin,Breset,Bdone,Bcancel,null);
   EFrgbrevise.zd = zd;

   zdialog_add_widget(zd,"hbox","hbmess","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labmess","hbmess",mess,"space=5");

   zdialog_add_widget(zd,"hbox","hbmym","dialog");
   zdialog_add_widget(zd,"check","delta","hbmym","delta","space=5");
   zdialog_stuff(zd,"delta",zonal_colors_delta);

   zdialog_add_widget(zd,"hbox","hbmetr","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labmetr","hbmetr",E2X("Metric:"),"space=5");
   zdialog_add_widget(zd,"radio","radRGB","hbmetr","RGB","space=3");
   zdialog_add_widget(zd,"radio","radEV","hbmetr","EV","space=3");
   zdialog_add_widget(zd,"radio","radOD","hbmetr","OD","space=3");

   if (zonal_colors_metric == 1) zdialog_stuff(zd,"radRGB",1);                   //  set current metric
   if (zonal_colors_metric == 2) zdialog_stuff(zd,"radEV",1);
   if (zonal_colors_metric == 3) zdialog_stuff(zd,"radOD",1);

   zdialog_add_widget(zd,"hbox","hbdata","dialog");
   zdialog_add_widget(zd,"vbox","vbpix","hbdata",0,"space=3|homog");             //  vbox for pixel locations
   zdialog_add_widget(zd,"hbox","hbpix","vbpix");
   zdialog_add_widget(zd,"label","labpix","hbpix","Pixel");                      //  header

   char hbpixx[8] = "hbpixx", pixx[8] = "pixx";

   for (int ii = 1; ii < 10; ii++) {                                             //  add labels for pixel locations
      hbpixx[5] = '0' + ii;
      pixx[3] = '0' + ii;
      zdialog_add_widget(zd,"hbox",hbpixx,"vbpix");                              //  add hbox "hbpix1" to "hbpix9"
      zdialog_add_widget(zd,"label",pixx,hbpixx);                                //  add label "pix1" to "pix9"
   }

   if (zonal_colors_metric == 1) limits = "-255|255|1|0.0";                      //  metric = RGB
   else if (zonal_colors_metric == 2) limits = "-8|8|0.01|0.0";                  //           EV
   else limits = "-3|3|0.01|0.0";                                                //           OD

   zdialog_add_widget(zd,"vbox","vbdat","hbdata",0,"space=3|homog");             //  vbox for pixel RGB values
   zdialog_add_widget(zd,"hbox","hbrgb","vbdat",0,"homog");
   zdialog_add_widget(zd,"label","labr","hbrgb",Bred);
   zdialog_add_widget(zd,"label","labg","hbrgb",Bgreen);
   zdialog_add_widget(zd,"label","labb","hbrgb",Bblue);

   char hbdatx[8] = "hbdatx", redx[8] = "redx", greenx[8] = "greenx", bluex[8] = "bluex";

   for (int ii = 1; ii < 10; ii++) {
      hbdatx[5] = '0' + ii;
      redx[3] = '0' + ii;
      greenx[5] = '0' + ii;
      bluex[4] = '0' + ii;
      zdialog_add_widget(zd,"hbox",hbdatx,"vbdat");                              //  add hbox "hbdat1" to "hbdat9"
      zdialog_add_widget(zd,"zspin",redx,hbdatx,limits,"space=3");               //  add spin buttons "red1" to "red9" etc.
      zdialog_add_widget(zd,"zspin",greenx,hbdatx,limits,"space=3");             //  zspin
      zdialog_add_widget(zd,"zspin",bluex,hbdatx,limits,"space=3");
   }

   zdialog_add_widget(zd,"hbox","hbsoft","dialog","space=5");
   zdialog_add_widget(zd,"label","labsoft","hbsoft",E2X("Blend"),"space=3");
   zdialog_add_widget(zd,"hscale","blend","hbsoft","0|100|1|20","space=5|expand");
   zdialog_add_widget(zd,"label","softval","hbsoft","20");

   if (strmatch(menu,"restart"))                                                 //  stuff current pixels if restart
      zonal_colors_stuff();
   else {
      zonal_colors_blend = 20;                                                   //  default slider value
      zonal_colors_npix = 0;                                                     //  no pixels selected yet
   }

   zdialog_run(zd,zonal_colors_dialog_event,"save");                             //  run dialog

   takeMouse(zonal_colors_mousefunc,dragcursor);                                 //  connect mouse function
   return;
}


//  dialog event function

int zonal_colors_dialog_event(zdialog *zd, cchar *event)
{
   using namespace zonal_colors_names;

   void  zonal_colors_mousefunc();
   void  zonal_colors_stuff();

   int         button;
   int         ii, jj;
   char        text[8];
   float       val1, val3;

   if (strmatch(event,"done")) zd->zstat = 2;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  from f_open()
   
   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  Reset
         zd->zstat = 0;                                                          //  keep dialog active
         zonal_colors_npix = 0;                                                  //  no pixels selected yet
         zonal_colors_stuff();                                                   //  clear dialog
         edit_reset();                                                           //  reset edits
         return 1;
      }

      if (zd->zstat == 2) edit_done(0);                                          //  commit edit
      else edit_cancel(0);                                                       //  discard edit
      EFrgbrevise.zd = 0;
      erase_toptext(101);                                                        //  erase pixel labels from image
      return 1;
   }

   if (strmatch(event,"focus"))                                                  //  toggle mouse capture
      takeMouse(zonal_colors_mousefunc,dragcursor);                              //  connect mouse function

   if (strmatch(event,"blend")) {                                                //  new blend factor
      zdialog_fetch(zd,"blend",zonal_colors_blend);
      snprintf(text,8,"%d",zonal_colors_blend);                                  //  numeric feedback
      zdialog_stuff(zd,"softval",text);
      signal_thread();
      return 1;
   }

   if (strmatchN(event,"rad",3))                                                 //  metric was changed
   {
      if (strmatch(event,"radRGB")) {                                            //  RGB
         zdialog_fetch(zd,event,button);
         if (button) zonal_colors_metric = 1;
      }

      if (strmatch(event,"radEV")) {                                             //  EV
         zdialog_fetch(zd,event,button);
         if (button) zonal_colors_metric = 2;
      }

      if (strmatch(event,"radOD")) {                                             //  OD
         zdialog_fetch(zd,event,button);
         if (button) zonal_colors_metric = 3;
      }

      if (button) {                                                              //  restart with new limits
         edit_cancel(0);
         m_zonal_colors(0,"restart");
      }

      return 1;
   }

   if (strmatch(event,"delta")) {                                                //  change absolute/delta mode
      zdialog_fetch(zd,"delta",zonal_colors_delta);
      edit_cancel(0);
      m_zonal_colors(0,"restart");
      return 1;
   }

   ii = -1;                                                                      //  no RGB change yet

   if (strmatchN(event,"red",3)) {                                               //  red1 - red9 was changed
      ii = event[3] - '1';                                                       //  pixel index 0-8
      jj = 0;                                                                    //  color = red
   }

   if (strmatchN(event,"green",5)) {                                             //  green1 - green9
      ii = event[5] - '1';
      jj = 1;
   }

   if (strmatchN(event,"blue",4)) {                                              //  blue1 - blue9
      ii = event[4] - '1';
      jj = 2;
   }

   if (ii >= 0 && ii < 9)                                                        //  RGB value was revised
   {
      val1 = zonal_colors_val1[ii][jj];                                          //  original pixel RGB value
      if (zonal_colors_metric == 2) val1 = RGB2EV(val1);                         //  convert to EV or OD units
      if (zonal_colors_metric == 3) val1 = RGB2OD(val1);

      zdialog_fetch(zd,event,val3);                                              //  revised RGB/EV/OD value from dialog
      if (zonal_colors_delta) val3 += val1;                                      //  if delta mode, make absolute

      if (zonal_colors_metric == 2) val3 = EV2RGB(val3);                         //  convert EV/OD to RGB value
      if (zonal_colors_metric == 3) val3 = OD2RGB(val3);

      if (fabsf(zonal_colors_val3[ii][jj] - val3) < 0.001) return 1;             //  ignore re-entry after change

      if (val3 < 0.1) val3 = 0.1;                                                //  limit RGB within 0.1 to 255.9
      if (val3 > 255.9) val3 = 255.9;

      zonal_colors_val3[ii][jj] = val3;                                          //  new RGB value for pixel

      if (zonal_colors_metric == 2) val3 = RGB2EV(val3);                         //  convert RGB to EV/OD units
      if (zonal_colors_metric == 3) val3 = RGB2OD(val3);

      if (zonal_colors_delta) val3 -= val1;                                      //  absolute back to relative
      zdialog_stuff(zd,event,val3);                                              //  limited value back to dialog

      signal_thread();                                                           //  signal thread to update image
   }

   return 1;
}


//  mouse function

void zonal_colors_mousefunc()                                                    //  mouse function
{
   using namespace zonal_colors_names;

   void  zonal_colors_stuff();

   int         ii;
   float       *pix3;

   if (! LMclick) return;
   LMclick = 0;

   zonal_colors_time = get_seconds();                                            //  mark time of pixel click

   if (zonal_colors_npix == 9) {                                                 //  if table is full (9 entries)
      for (ii = 1; ii < 9; ii++) {                                               //    move positions 1-8 up
         zonal_colors_pixel[ii-1][0] = zonal_colors_pixel[ii][0];                //      to fill positions 0-7
         zonal_colors_pixel[ii-1][1] = zonal_colors_pixel[ii][1];
      }
      zonal_colors_npix = 8;                                                     //  count is now 8 entries
   }

   ii = zonal_colors_npix;                                                       //  next table position to fill
   zonal_colors_pixel[ii][0] = Mxclick;                                          //  newest pixel
   zonal_colors_pixel[ii][1] = Myclick;

   pix3 = PXMpix(E3pxm,Mxclick,Myclick);                                         //  get initial RGB values from
   zonal_colors_val1[ii][0] = zonal_colors_val3[ii][0] = pix3[0];                //    modified image E3
   zonal_colors_val1[ii][1] = zonal_colors_val3[ii][1] = pix3[1];
   zonal_colors_val1[ii][2] = zonal_colors_val3[ii][2] = pix3[2];

   zonal_colors_npix++;                                                          //  up pixel count
   zonal_colors_stuff();                                                         //  stuff pixels and values into dialog
   return;
}


//  stuff dialog with current pixels and their RGB/EV/OD values

void zonal_colors_stuff()
{
   using namespace zonal_colors_names;

   static char    lab1[9][4] = { "A", "B", "C", "D", "E", "F", "G", "H", "I" };
   static char    lab2[9][4] = { " A ", " B ", " C ", " D ", " E ", " F ", " G ", " H ", " I " };

   int         px, py;
   float       red1, green1, blue1, red3, green3, blue3;
   char        text[100], pixx[8] = "pixx";
   char        redx[8] = "redx", greenx[8] = "greenx", bluex[8] = "bluex";
   zdialog     *zd = EFrgbrevise.zd;

   erase_toptext(101);                                                           //  erase prior labels from image

   for (int ii = 0; ii < 9; ii++)                                                //  loop slots 0-8
   {
      pixx[3] = '1' + ii;                                                        //  widget names "pix1" ... "pix9"
      redx[3] = '1' + ii;                                                        //  widget names "red1" ... "red9"
      greenx[5] = '1' + ii;
      bluex[4] = '1' + ii;

      px = zonal_colors_pixel[ii][0];                                            //  next pixel to report
      py = zonal_colors_pixel[ii][1];
      if (ii >= zonal_colors_npix) {                                             //  > last pixel selected
         zdialog_stuff(zd,pixx,"");
         zdialog_stuff(zd,redx,0);                                               //  blank pixel and zero values
         zdialog_stuff(zd,greenx,0);
         zdialog_stuff(zd,bluex,0);
         continue;
      }

      snprintf(text,100,"%s %4d %4d",lab1[ii],px,py);                            //  format pixel "A nnnn nnnn"
      zdialog_stuff(zd,pixx,text);                                               //  pixel >> widget

      add_toptext(101,px,py,lab2[ii],"Sans 8");                                  //  paint label on image at pixel

      red1 = zonal_colors_val1[ii][0];                                           //  original RGB values for pixel
      green1 = zonal_colors_val1[ii][1];
      blue1 = zonal_colors_val1[ii][2];
      red3 = zonal_colors_val3[ii][0];                                           //  revised RGB values
      green3 = zonal_colors_val3[ii][1];
      blue3 = zonal_colors_val3[ii][2];

      if (zonal_colors_metric == 2) {                                            //  convert to EV units if needed
         red1 = RGB2EV(red1);
         green1 = RGB2EV(green1);
         blue1 = RGB2EV(blue1);
         red3 = RGB2EV(red3);
         green3 = RGB2EV(green3);
         blue3 = RGB2EV(blue3);
      }

      if (zonal_colors_metric == 3) {                                            //  or OD units
         red1 = RGB2OD(red1);
         green1 = RGB2OD(green1);
         blue1 = RGB2OD(blue1);
         red3 = RGB2OD(red3);
         green3 = RGB2OD(green3);
         blue3 = RGB2OD(blue3);
      }

      if (zonal_colors_delta) {                                                  //  dialog is delta mode
         red3 -= red1;
         green3 -= green1;
         blue3 -= blue1;
      }

      zdialog_stuff(zd,redx,red3);
      zdialog_stuff(zd,greenx,green3);
      zdialog_stuff(zd,bluex,blue3);
   }

   zdialog_stuff(zd,"blend",zonal_colors_blend);

   signal_thread();
   return;
}


//  thread function - multiple working threads to update image

void * zonal_colors_thread(void *)
{
   using namespace zonal_colors_names;

   void * zonal_colors_wthread(void *arg);                                       //  worker thread

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request
      zsleep(0.3);                                                               //  more time for dialog controls

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      if (zonal_colors_npix < 1) continue;                                       //  must have 1+ pixels in table

      do_wthreads(zonal_colors_wthread,NWT);                                     //  worker threads

      CEF->Fmods++;                                                              //  image modified
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop warning
}


void * zonal_colors_wthread(void *arg)                                           //  worker thread function
{
   using namespace zonal_colors_names;

   int         index = *((int *) (arg));
   int         px1, py1, px2, py2, ii;
   float       *pix1, *pix3;
   float       dist[9], weight[9], sumdist;
   float       blend, delta, red, green, blue, max;

   blend = E1pxm->ww;
   if (E1pxm->hh > E1pxm->ww) blend = E1pxm->hh;
   blend = blend * blend;
   blend = 0.0002 * zonal_colors_blend * zonal_colors_blend * blend + 100;       //  100 to 2 * E1pxm->ww**2

   for (ii = 0; ii < 9; ii++) {                                                  //  stop misleading gcc warnings
      dist[ii] = 1;
      weight[ii] = 1;                                                            //  avoid gcc warning
   }

   for (py1 = index; py1 < E3hh; py1 += NWT)                                     //  loop all image pixels
   for (px1 = 0; px1 < E3ww; px1++)
   {
      for (sumdist = ii = 0; ii < zonal_colors_npix; ii++)                       //  compute weight of each revision point
      {
         px2 = zonal_colors_pixel[ii][0];
         py2 = zonal_colors_pixel[ii][1];
         dist[ii] = (px1-px2)*(px1-px2) + (py1-py2)*(py1-py2);                   //  distance (px1,py1) to (px2,py2)
         dist[ii] = 1.0 / (dist[ii] + blend);                                    //  blend reduces peaks at revision points
         sumdist += dist[ii];                                                    //  sum inverse distances
      }

      for (ii = 0; ii < zonal_colors_npix; ii++)                                 //  weight of each point
         weight[ii] = dist[ii] / sumdist;

      pix1 = PXMpix(E1pxm,px1,py1);                                              //  input pixel
      pix3 = PXMpix(E3pxm,px1,py1);                                              //  output pixel

      red = pix1[0];
      green = pix1[1];
      blue = pix1[2];

      for (ii = 0; ii < zonal_colors_npix; ii++) {                               //  apply weighted color changes
         delta = zonal_colors_val3[ii][0] - zonal_colors_val1[ii][0];            //    to each color
         red += weight[ii] * delta;
         delta = zonal_colors_val3[ii][1] - zonal_colors_val1[ii][1];
         green += weight[ii] * delta;
         delta = zonal_colors_val3[ii][2] - zonal_colors_val1[ii][2];
         blue += weight[ii] * delta;
      }

      max = red;
      if (green > max) max = green;
      if (blue > max) max = blue;

      if (max > 255.9) {                                                         //  stop overflow/underflow
         red = red * 255.9 / max;
         green = green * 255.9 / max;
         blue = blue * 255.9 / max;
      }

      if (red < 0) red = 0;
      if (green < 0) green = 0;
      if (blue < 0) blue = 0;

      pix3[0] = red;
      pix3[1] = green;
      pix3[2] = blue;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  match_colors edit function
//  Adjust colors of image 2 to match the colors of image 1
//  using small selected areas in each image as the match standard.

namespace match_colors_names
{
   float    match_colors_RGB1[3];                                                //  image 1 base colors to match
   float    match_colors_RGB2[3];                                                //  image 2 target colors to match
   int      match_colors_radius = 10;                                            //  mouse radius
   int      match_colors_mode = 0;
   int      E3ww, E3hh;

   editfunc    EFmatchcolors;
}


//  menu function

void m_match_colors(GtkWidget *, const char *)
{
   using namespace match_colors_names;

   int    match_colors_dialog_event(zdialog* zd, const char *event);
   void * match_colors_thread(void *);
   void   match_colors_mousefunc();

   cchar    *title = E2X("Color Match Images");

   F1_help_topic = "match_colors";
   if (checkpend("all")) return;                                                 //  check, no block: edit_setup() follows

/***
          ____________________________________________
         |       Color Match Images                   |
         |                                            |
         | 1  [ 10 ]   mouse radius for color sample  |
         | 2  [Open]   image for source color         |
         | 3  click on image to get source color      |
         | 4  [Open]   image for target color         |
         | 5  click on image to set target color      |
         |                                            |
         |                            [done] [cancel] |
         |____________________________________________|

***/

   zdialog *zd = zdialog_new(title,Mwin,Bdone,Bcancel,null);                     //  match_colors dialog
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=2");
   zdialog_add_widget(zd,"vbox","vb1","hb1",0,"homog|space=3");
   zdialog_add_widget(zd,"vbox","vb2","hb1",0,"homog|space=3");
   zdialog_add_widget(zd,"label","labn1","vb1","1");
   zdialog_add_widget(zd,"label","labn2","vb1","2");
   zdialog_add_widget(zd,"label","labn3","vb1","3");
   zdialog_add_widget(zd,"label","labn4","vb1","4");
   zdialog_add_widget(zd,"label","labn5","vb1","5");
   zdialog_add_widget(zd,"hbox","hbrad","vb2");
   zdialog_add_widget(zd,"zspin","radius","hbrad","1|20|1|10","space=5");
   zdialog_add_widget(zd,"label","labrad","hbrad",E2X("mouse radius for color sample"));
   zdialog_add_widget(zd,"hbox","hbop1","vb2");
   zdialog_add_widget(zd,"button","open1","hbop1",E2X("Open"),"space=5");
   zdialog_add_widget(zd,"label","labop1","hbop1",E2X("image for source color"));
   zdialog_add_widget(zd,"hbox","hbclik1","vb2");
   zdialog_add_widget(zd,"label","labclik1","hbclik1",E2X("click on image to get source color"));
   zdialog_add_widget(zd,"hbox","hbop2","vb2");
   zdialog_add_widget(zd,"button","open2","hbop2",E2X("Open"),"space=5");
   zdialog_add_widget(zd,"label","labop2","hbop2",E2X("image to set matching color"));
   zdialog_add_widget(zd,"hbox","hbclik2","vb2");
   zdialog_add_widget(zd,"label","labclik2","hbclik2",E2X("click on image to set matching color"));

   zdialog_stuff(zd,"radius",match_colors_radius);                               //  remember last radius

   EFmatchcolors.funcname = "match_colors";
   EFmatchcolors.Farea = 1;                                                      //  select area ignored
   EFmatchcolors.zd = zd;
   EFmatchcolors.threadfunc = match_colors_thread;
   EFmatchcolors.mousefunc = match_colors_mousefunc;

   match_colors_mode = 0;
   if (curr_file) {
      match_colors_mode = 1;                                                     //  image 1 ready to click
      takeMouse(match_colors_mousefunc,0);                                       //  connect mouse function
   }

   zdialog_run(zd,match_colors_dialog_event,"parent");                           //  run dialog - parallel
   return;
}


//  match_colors dialog event and completion function

int match_colors_dialog_event(zdialog *zd, const char *event)
{
   using namespace match_colors_names;

   void   match_colors_mousefunc();

   int      err;
   char     *file;

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()
   
   if (zd->zstat)
   {
      if (match_colors_mode == 4) {                                              //  edit was started
         if (zd->zstat == 1) edit_done(0);                                       //  commit edit
         else edit_cancel(0);                                                    //  discard edit
         match_colors_mode = 0;
         return 1;
      }

      freeMouse();                                                               //  abandoned
      zdialog_free(zd);
      match_colors_mode = 0;
      return 1;
   }

   if (strmatch(event,"radius"))                                                 //  set new mouse radius
      zdialog_fetch(zd,"radius",match_colors_radius);

   if (strmatch(event,"open1"))                                                  //  get image 1 for color source
   {
      if (match_colors_mode == 4) edit_cancel(1);                                //  cancel edit, keep dialog
      match_colors_mode = 0;
      file = gallery_select1(null);                                              //  open image 1
      if (file) {
         err = f_open(file);
         if (! err) match_colors_mode = 1;                                       //  image 1 ready to click
      }
   }

   if (strmatch(event,"open2"))                                                  //  get image 2 to set matching color
   {
      if (match_colors_mode < 2) {
         zmessageACK(Mwin,E2X("select source image color first"));               //  check that RGB1 has been set
         return 1;
      }
      match_colors_mode = 2;
      file = gallery_select1(null);                                              //  open image 2
      if (! file) return 1;
      err = f_open(file);
      if (err) return 1;
      match_colors_mode = 3;                                                     //  image 2 ready to click
   }

   takeMouse(match_colors_mousefunc,0);                                          //  reconnect mouse function
   return 1;
}


//  mouse function - click on image and get colors to match

void match_colors_mousefunc()
{
   using namespace match_colors_names;

   void  match_colors_getRGB(int px, int py, float rgb[3]);

   int      px, py;

   if (match_colors_mode < 1) return;                                            //  no image available yet

   draw_mousecircle(Mxposn,Myposn,match_colors_radius,0,0);                      //  draw circle around pointer

   if (LMclick)
   {
      LMclick = 0;
      px = Mxclick;
      py = Myclick;

      if (match_colors_mode == 1 || match_colors_mode == 2)                      //  image 1 ready to click
      {
         match_colors_getRGB(px,py,match_colors_RGB1);                           //  get RGB1 color
         match_colors_mode = 2;
         return;
      }

      if (match_colors_mode == 3 || match_colors_mode == 4)                      //  image 2 ready to click
      {
         if (match_colors_mode == 4) edit_reset();
         else {
            if (! edit_setup(EFmatchcolors)) return;                             //  setup edit - thread will launch
            E3ww = E3pxm->ww;
            E3hh = E3pxm->hh;
            match_colors_mode = 4;                                               //  edit waiting for cancel or done
         }

         match_colors_getRGB(px,py,match_colors_RGB2);                           //  get RGB2 color
         signal_thread();                                                        //  update the target image
         return;
      }
   }

   return;
}


//  get the RGB averages for pixels within mouse radius

void match_colors_getRGB(int px, int py, float rgb[3])
{
   using namespace match_colors_names;

   int      radflat1 = match_colors_radius;
   int      radflat2 = radflat1 * radflat1;
   int      rad, npix, qx, qy;
   float    red, green, blue;
   float    *pix1;
   PXM      *pxm;

   pxm = PXM_load(curr_file,1);                                                  //  popup ACK if error
   if (! pxm) return;

   npix = 0;
   red = green = blue = 0;

   for (qy = py-radflat1; qy <= py+radflat1; qy++)
   for (qx = px-radflat1; qx <= px+radflat1; qx++)
   {
      if (qx < 0 || qx > pxm->ww-1) continue;
      if (qy < 0 || qy > pxm->hh-1) continue;
      rad = (qx-px) * (qx-px) + (qy-py) * (qy-py);
      if (rad > radflat2) continue;
      pix1 = PXMpix(pxm,qx,qy);
      red += pix1[0];
      green += pix1[1];
      blue += pix1[2];
      npix++;
   }

   rgb[0] = red / npix;
   rgb[1] = green / npix;
   rgb[2] = blue / npix;

   PXM_free(pxm);
   return;
}


//  thread function - start multiple working threads

void * match_colors_thread(void *)
{
   using namespace match_colors_names;

   void * match_colors_wthread(void *arg);

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      do_wthreads(match_colors_wthread,NWT);                                     //  worker threads

      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, avoid warning
}


void * match_colors_wthread(void *arg)                                           //  worker thread function
{
   using namespace match_colors_names;

   int         index = *((int *) (arg));
   int         px, py;
   float       *pix3;
   float       Rred, Rgreen, Rblue;
   float       red, green, blue, cmax;

   Rred = match_colors_RGB1[0] / match_colors_RGB2[0];                           //  color adjustment ratios
   Rgreen = match_colors_RGB1[1] / match_colors_RGB2[1];
   Rblue = match_colors_RGB1[2] / match_colors_RGB2[2];

   for (py = index; py < E3hh; py += NWT)                                        //  loop all image pixels
   for (px = 0; px < E3ww; px++)
   {
      pix3 = PXMpix(E3pxm,px,py);

      red = pix3[0] * Rred;                                                      //  adjust colors
      green = pix3[1] * Rgreen;
      blue = pix3[2] * Rblue;

      cmax = red;                                                                //  check for overflow
      if (green > cmax) cmax = green;
      if (blue > cmax) cmax = blue;

      if (cmax > 255.9) {                                                        //  fix overflow
         red = red * 255.9 / cmax;
         green = green * 255.9 / cmax;
         blue = blue * 255.9 / cmax;
      }

      pix3[0] = red;
      pix3[1] = green;
      pix3[2] = blue;
   }

   pthread_exit(0);
}


/********************************************************************************/

//  image color-depth reduction

namespace colordepth_names
{
   int         colordepth_depth = 16;                                            //  bits per RGB color
   editfunc    EFcolordepth;
   int         E3ww, E3hh;
}


//  menu function

void m_colordepth(GtkWidget *, cchar *menu)
{
   using namespace colordepth_names;

   int    colordepth_dialog_event(zdialog *zd, cchar *event);
   void * colordepth_thread(void *);

   cchar  *colmess = E2X("Set color depth to 1-16 bits");

   F1_help_topic = "color_depth";

   EFcolordepth.menuname = menu;
   EFcolordepth.menufunc = m_colordepth;
   EFcolordepth.funcname = "color_depth";
   EFcolordepth.FprevReq = 1;                                                    //  use preview
   EFcolordepth.Farea = 2;                                                       //  select area usable
   EFcolordepth.FusePL = 1;                                                      //  use with paint/lever edits OK
   EFcolordepth.Fscript = 1;                                                     //  scripting supported
   EFcolordepth.threadfunc = colordepth_thread;                                  //  thread function
   if (! edit_setup(EFcolordepth)) return;                                       //  setup edit
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;
   
/***
          _____________________________
         |   Set Color Depth           |
         |                             |
         | Set color depth 1-16 bits   |
         | [___]                       |
         |                             |
         |             [Done] [Cancel] |
         |_____________________________|

***/

   zdialog *zd = zdialog_new(E2X("Set Color Depth"),Mwin,Bdone,Bcancel,null);
   EFcolordepth.zd = zd;
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","lab1","hb1",colmess,"space=3");
   zdialog_add_widget(zd,"hbox","hb2","dialog",0,"space=5");
   zdialog_add_widget(zd,"zspin","colors","hb2","1|16|1|16","space=5");

   colordepth_depth = 16;

   zdialog_resize(zd,200,0);
   zdialog_run(zd,colordepth_dialog_event,"save");                               //  run dialog, parallel

   return;
}


//  dialog event and completion callback function

int colordepth_dialog_event(zdialog *zd, cchar *event)
{
   using namespace colordepth_names;

   if (strmatch(event,"focus")) return 1;
   if (strmatch(event,"apply")) event = "colors";                                //  from script
   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()

   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();
      E3ww = E3pxm->ww;
      E3hh = E3pxm->hh;
      signal_thread();
      return 1;
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  done
         edit_fullsize();                                                        //  get full size image
         E3ww = E3pxm->ww;
         E3hh = E3pxm->hh;
         signal_thread();
         edit_done(0);                                                           //  commit edit
      }
      else edit_cancel(0);                                                       //  discard edit
      return 1;
   }

   if (strmatch(event,"undo")) edit_undo();
   if (strmatch(event,"redo")) edit_redo();
   if (strmatch(event,"blendwidth")) signal_thread();

   if (strmatch(event,"colors")) {
      zdialog_fetch(zd,"colors",colordepth_depth);
      signal_thread();
   }

   return 0;
}


//  image color depth thread function

void * colordepth_thread(void *)
{
   using namespace colordepth_names;

   int         ii, px, py, rgb, dist = 0;
   uint16      m1, m2, val1, val3;
   float       *pix1, *pix3;
   float       f1, f2;

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      m1 = 0xFFFF << (16 - colordepth_depth);                                    //  5 > 1111100000000000
      m2 = 0x8000 >> colordepth_depth;                                           //  5 > 0000010000000000

      for (py = 0; py < E3hh; py++)
      for (px = 0; px < E3ww; px++)
      {
         if (sa_stat == 3) {                                                     //  select area active
            ii = py * E3ww + px;
            dist = sa_pixmap[ii];                                                //  distance from edge
            if (! dist) continue;                                                //  outside pixel
         }

         pix1 = PXMpix(E1pxm,px,py);                                             //  input pixel
         pix3 = PXMpix(E3pxm,px,py);                                             //  output pixel

         for (rgb = 0; rgb < 3; rgb++)
         {
            val1 = 256 * pix1[rgb];                                              //  16 bit integer <= 65535
            val3 = (val1 + m2) & m1;                                             //  round: nearest value within bits   18.07
            if (val3 > m1) val3 = m1;

            if (sa_stat == 3 && dist < sa_blendwidth) {                          //  select area is active,
               f2 = sa_blendfunc(dist);                                          //    blend changes over sa_blendwidth
               f1 = 1.0 - f2;
               val3 = int(f1 * val1 + f2 * val3);
            }

            pix3[rgb] = val3 / 256;
         }
      }

      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


/********************************************************************************/

//  Smart Erase menu function - Replace pixels inside a select area
//    with a reflection of pixels outside the area.

namespace smarterase_names
{
   editfunc    EFsmarterase;
   int         E3ww, E3hh;
}


//  menu function

void m_smart_erase(GtkWidget *, const char *)
{
   using namespace smarterase_names;

   int smart_erase_dialog_event(zdialog* zd, const char *event);

   cchar    *erase_message = E2X("Drag mouse to select. Erase. Repeat. \n"
                                 "Click: extend selection to mouse.");
   F1_help_topic = "smart_erase";

   EFsmarterase.menufunc = m_smart_erase;
   EFsmarterase.funcname = "smart_erase";
   EFsmarterase.Farea = 0;                                                       //  select area deleted
   EFsmarterase.mousefunc = sa_mouse_select;                                     //  mouse function (use select area)
   if (! edit_setup(EFsmarterase)) return;                                       //  setup edit
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

/***
       _________________________________________
      |                                         |
      | Drag mouse to select. Erase. Repeat.    |
      | Click: extend selection to mouse.       |
      |                                         |
      | Radius [ 10 ]    Blur [ 1.5 ]           |
      | [New Area] [Show] [Hide] [Erase] [Undo] |
      |                                         |
      |                                 [Done]  |
      |_________________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Smart Erase"),Mwin,Bdone,null);
   EFsmarterase.zd = zd;

   zdialog_add_widget(zd,"label","lab1","dialog",erase_message,"space=3");
   zdialog_add_widget(zd,"hbox","hb2","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labr","hb2",E2X("Radius"),"space=5");
   zdialog_add_widget(zd,"zspin","radius","hb2","1|20|1|10");
   zdialog_add_widget(zd,"label","labb","hb2",E2X("Blur"),"space=10");
   zdialog_add_widget(zd,"zspin","blur","hb2","0|9|0.5|1");
   zdialog_add_widget(zd,"hbox","hb3","dialog",0,"space=5");
   zdialog_add_widget(zd,"button","newarea","hb3",E2X("New Area"),"space=3");
   zdialog_add_widget(zd,"button","show","hb3",E2X(Bshow),"space=3");
   zdialog_add_widget(zd,"button","hide","hb3",Bhide,"space=3");
   zdialog_add_widget(zd,"button","erase","hb3",Berase,"space=3");
   zdialog_add_widget(zd,"button","undo1","hb3",Bundo,"space=3");

   sa_clear();                                                                   //  clear area if any
   sa_pixmap_create();                                                           //  allocate select area pixel maps    19.0
   sa_mode = mode_mouse;                                                         //  mode = select by mouse
   sa_stat = 1;                                                                  //  status = active edit
   sa_fww = E1pxm->ww;
   sa_fhh = E1pxm->hh;
   sa_searchrange = 1;                                                           //  search within mouse radius
   sa_mouseradius = 10;                                                          //  initial mouse select radius
   sa_lastx = sa_lasty = 0;                                                      //  initz. for sa_mouse_select         18.07
   takeMouse(sa_mouse_select,0);                                                 //  use select area mouse function
   sa_show(1,0);

   zdialog_run(zd,smart_erase_dialog_event,"save");                              //  run dialog - parallel
   return;
}


//  dialog event and completion function

int smart_erase_dialog_event(zdialog *zd, const char *event)                     //  overhauled
{
   using namespace smarterase_names;

   void smart_erase_func(int mode);
   int  smart_erase_blur(float radius);

   float       radius;

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()

   if (zd->zstat)
   {
      sa_clear();                                                                //  clear select area
      freeMouse();                                                               //  disconnect mouse
      if (zd->zstat == 1) edit_done(0);                                          //  commit edit
      else edit_cancel(0);                                                       //  discard edit
      return 1;
   }

   if (strmatch(event,"radius"))
      zdialog_fetch(zd,"radius",sa_mouseradius);

   if (strmatch(event,"newarea")) {
      sa_clear();
      sa_lastx = sa_lasty = 0;                                                   //  forget last click                  19.0
      sa_pixmap_create();                                                        //  allocate select area pixel maps    19.0
      sa_mode = mode_mouse;                                                      //  mode = select by mouse
      sa_stat = 1;                                                               //  status = active edit
      sa_fww = E1pxm->ww;
      sa_fhh = E1pxm->hh;
      sa_show(1,0);
      takeMouse(sa_mouse_select,0);
   }

   if (strmatch(event,"show")) {
      sa_show(1,0);
      takeMouse(sa_mouse_select,0);
   }

   if (strmatch(event,"hide")) {
      sa_show(0,0);
      freeMouse();
   }

   if (strmatch(event,"erase")) {                                                //  do smart erase
      sa_finish_auto();                                                          //  finish the area
      smart_erase_func(1);
      zdialog_fetch(zd,"blur",radius);                                           //  add optional blur
      if (radius > 0) smart_erase_blur(radius);
      sa_show(0,0);
   }

   if (strmatch(event,"undo1"))                                                  //  dialog undo, undo last erase
      smart_erase_func(2);

   return 1;
}


//  erase the area or restore last erased area
//  mode = 1 = erase, mode = 2 = restore

void smart_erase_func(int mode)
{
   using namespace smarterase_names;

   int         px, py, npx, npy;
   int         qx, qy, sx, sy, tx, ty;
   int         ww, hh, ii, rad, inc, cc;
   int         dist2, mindist2;
   float       slope;
   char        *pmap;
   float       *pix1, *pix3;
   int         nc = E1pxm->nc, pcc = nc * sizeof(float);

   if (sa_stat != 3) return;                                                     //  nothing selected
   if (! sa_validate()) return;                                                  //  area invalid for curr. image file

   ww = E1pxm->ww;
   hh = E1pxm->hh;

   for (py = sa_miny; py < sa_maxy; py++)                                        //  undo all pixels in area
   for (px = sa_minx; px < sa_maxx; px++)
   {
      ii = py * ww + px;
      if (! sa_pixmap[ii]) continue;                                             //  pixel not selected

      pix1 = PXMpix(E1pxm,px,py);                                                //  input pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  output pixel
      memcpy(pix3,pix1,pcc);
   }

   Fpaint2();                                                                    //  update window

   if (mode == 2) return;                                                        //  mode = undo, done

   cc = ww * hh;                                                                 //  allocate pixel done map
   pmap = (char *) zmalloc(cc);
   memset(pmap,0,cc);

   for (py = sa_miny; py < sa_maxy; py++)                                        //  loop all pixels in area
   for (px = sa_minx; px < sa_maxx; px++)
   {
      ii = py * ww + px;
      if (! sa_pixmap[ii]) continue;                                             //  pixel not selected
      if (pmap[ii]) continue;                                                    //  pixel already done

      mindist2 = 999999;                                                         //  find nearest edge
      npx = npy = 0;

      for (rad = 1; rad < 50; rad++)                                             //  50 pixel limit
      {
         for (qx = px-rad; qx <= px+rad; qx++)                                   //  search within rad
         for (qy = py-rad; qy <= py+rad; qy++)
         {
            if (qx < 0 || qx >= ww) continue;                                    //  off image edge
            if (qy < 0 || qy >= hh) continue;
            ii = qy * ww + qx;
            if (sa_pixmap[ii]) continue;                                         //  within selected area

            dist2 = (px-qx) * (px-qx) + (py-qy) * (py-qy);                       //  distance**2 to edge pixel
            if (dist2 < mindist2) {
               mindist2 = dist2;
               npx = qx;                                                         //  save nearest edge pixel found
               npy = qy;
            }
         }

         if (rad * rad >= mindist2) break;                                       //  found edge, can quit now
      }

      if (! npx && ! npy) continue;                                              //  edge not found, should not happen

      qx = npx;                                                                  //  nearest edge pixel from px/py
      qy = npy;

      if (abs(qy - py) > abs(qx - px))                                           //  line px/py to qx/qy is more
      {                                                                          //    vertical than horizontal
         slope = 1.0 * (qx - px) / (qy - py);
         if (qy > py) inc = 1;
         else inc = -1;

         for (sy = py; sy != qy; sy += inc) {                                    //  sx/sy = line from px/py to qx/qy
            sx = px + slope * (sy - py);

            ii = sy * ww + sx;
            if (pmap[ii]) continue;                                              //  skip done pixels
            pmap[ii] = 1;

            tx = qx + (qx - sx);                                                 //  tx/ty = extended line from qx/qy
            ty = qy + (qy - sy);

            if (tx < 0) tx = 0;                                                  //  don't go off edge
            if (tx > ww-1) tx = ww-1;
            if (ty < 0) ty = 0;
            if (ty > hh-1) ty = hh-1;

            pix1 = PXMpix(E1pxm,tx,ty);                                          //  copy pixel from tx/ty to sx/sy     18.07
            pix3 = PXMpix(E3pxm,sx,sy);                                          //  simplified                         18.07
            memcpy(pix3,pix1,pcc);
         }
      }

      else                                                                       //  more horizontal than vertical
      {
         slope = 1.0 * (qy - py) / (qx - px);
         if (qx > px) inc = 1;
         else inc = -1;

         for (sx = px; sx != qx; sx += inc) {
            sy = py + slope * (sx - px);

            ii = sy * ww + sx;
            if (pmap[ii]) continue;
            pmap[ii] = 1;

            tx = qx + (qx - sx);
            ty = qy + (qy - sy);

            if (tx < 0) tx = 0;
            if (tx > ww-1) tx = ww-1;
            if (ty < 0) ty = 0;
            if (ty > hh-1) ty = hh-1;

            pix1 = PXMpix(E1pxm,tx,ty);
            pix3 = PXMpix(E3pxm,sx,sy);
            memcpy(pix3,pix1,pcc);
         }
      }
   }

   zfree(pmap);                                                                  //  free memory
   CEF->Fmods++;
   CEF->Fsaved = 0;

   Fpaint2();                                                                    //  update window
   return;
}


//  add blur to the erased area to help mask the side-effects

int smart_erase_blur(float radius)
{
   using namespace smarterase_names;

   int         ii, px, py, dx, dy, adx, ady;
   float       blur_weight[12][12];                                              //  up to blur radius = 10
   float       rad, radflat2;
   float       m, d, w, sum, weight;
   float       red, green, blue;
   float       *pix9, *pix3, *pixN;
   int         nc = E3pxm->nc;

   if (sa_stat != 3) return 0;

   rad = radius - 0.2;
   radflat2 = rad * rad;

   for (dx = 0; dx < 12; dx++)                                                   //  clear weights array
   for (dy = 0; dy < 12; dy++)
      blur_weight[dx][dy] = 0;

   for (dx = -rad-1; dx <= rad+1; dx++)                                          //  blur_weight[dx][dy] = no. of pixels
   for (dy = -rad-1; dy <= rad+1; dy++)                                          //    at distance (dx,dy) from center
      ++blur_weight[abs(dx)][abs(dy)];

   m = sqrt(radflat2 + radflat2);                                                //  corner pixel distance from center
   sum = 0;

   for (dx = 0; dx <= rad+1; dx++)                                               //  compute weight of pixel
   for (dy = 0; dy <= rad+1; dy++)                                               //    at distance dx, dy
   {
      d = sqrt(dx*dx + dy*dy);
      w = (m + 1.2 - d) / m;
      w = w * w;
      sum += blur_weight[dx][dy] * w;
      blur_weight[dx][dy] = w;
   }

   for (dx = 0; dx <= rad+1; dx++)                                               //  make weights add up to 1.0
   for (dy = 0; dy <= rad+1; dy++)
      blur_weight[dx][dy] = blur_weight[dx][dy] / sum;

   E9pxm = PXM_copy(E3pxm);                                                      //  copy edited image

   for (py = sa_miny; py < sa_maxy; py++)                                        //  loop all pixels in area
   for (px = sa_minx; px < sa_maxx; px++)
   {
      ii = py * E1pxm->ww + px;
      if (! sa_pixmap[ii]) continue;                                             //  pixel not in area

      pix9 = PXMpix(E9pxm,px,py);                                                //  source pixel
      pix3 = PXMpix(E3pxm,px,py);                                                //  target pixel

      rad = radius;
      red = green = blue = 0;

      for (dy = -rad-1; dy <= rad+1; dy++)                                       //  loop neighbor pixels within radius
      for (dx = -rad-1; dx <= rad+1; dx++)
      {
         if (px+dx < 0 || px+dx >= E3ww) continue;                               //  omit pixels off edge
         if (py+dy < 0 || py+dy >= E3hh) continue;
         adx = abs(dx);
         ady = abs(dy);
         pixN = pix9 + (dy * E3ww + dx) * nc;
         weight = blur_weight[adx][ady];                                         //  weight at distance (dx,dy)
         red += pixN[0] * weight;                                                //  accumulate contributions
         green += pixN[1] * weight;
         blue += pixN[2] * weight;
      }

      pix3[0] = red;
      pix3[1] = green;
      pix3[2] = blue;
   }

   PXM_free(E9pxm);

   CEF->Fmods++;
   CEF->Fsaved = 0;

   Fpaint2();                                                                    //  update window
   return 0;
}


/********************************************************************************/

//  add a brightness/color curved ramp to an image, in a chosen direction

namespace bright_ramp_names 
{
   editfunc    EFbright_ramp;
   int         Fline, linex1, liney1, linex2, liney2;
   float       A, B, C;
   float       ex1, ey1, ex2, ey2;
   int         E3ww, E3hh;
}


//  menu function

void m_bright_ramp(GtkWidget *, cchar *menu) 
{
   using namespace bright_ramp_names;

   void   bright_ramp_curvedit(int spc);
   int    bright_ramp_dialog_event(zdialog* zd, cchar *event);
   void * bright_ramp_thread(void *);
   void   bright_ramp_mousefunc();

   cchar    *mess = E2X("Draw a line across the image in \n"
                        "direction of brightness change.");

   F1_help_topic = "bright_ramp";

   EFbright_ramp.menuname = menu;
   EFbright_ramp.menufunc = m_bright_ramp;
   EFbright_ramp.funcname = "bright_ramp";
   EFbright_ramp.FprevReq = 1;                                                   //  use preview
   EFbright_ramp.Farea = 2;                                                      //  select area usable
   EFbright_ramp.threadfunc = bright_ramp_thread;
   EFbright_ramp.mousefunc = bright_ramp_mousefunc;                              //  bugfix                             18.01

   if (! edit_setup(EFbright_ramp)) return;                                      //  setup edit

   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

   Fline = 0;                                                                    //  no drawn line initially

/***
          _______________________________________________
         |              Brightness Ramp                  |
         |                                               |
         |    Draw a line across the image in            |
         |    direction of brightness change.            |
         |  ___________________________________________  |
         | |                                           | |                       //  5 curves are maintained:
         | |                                           | |                       //  curve 0: current display curve
         | |                                           | |                       //        1: curve for all colors
         | |         curve edit area                   | |                       //        2,3,4: red, green, blue
         | |                                           | |
         | |                                           | |
         | |                                           | |
         | |___________________________________________| |
         |   (o) all  (o) red  (o) green  (o) blue       |                       //  select curve to display
         |                                               |
         |                      [reset] [Done] [Cancel]  |
         |_______________________________________________|

***/

   zdialog *zd = zdialog_new(E2X("Brightness Ramp"),Mwin,Breset,Bdone,Bcancel,null);
   EFbright_ramp.zd = zd;
   zdialog_add_widget(zd,"label","labmess","dialog",mess);
   zdialog_add_widget(zd,"frame","frameH","dialog",0,"expand");                  //  edit-curves
   zdialog_add_widget(zd,"hbox","hbrgb","dialog");                               //  radio buttons all/red/green/blue
   zdialog_add_widget(zd,"radio","all","hbrgb",Ball,"space=5");
   zdialog_add_widget(zd,"radio","red","hbrgb",Bred,"space=3");
   zdialog_add_widget(zd,"radio","green","hbrgb",Bgreen,"space=3");
   zdialog_add_widget(zd,"radio","blue","hbrgb",Bblue,"space=3");

   GtkWidget *frameH = zdialog_widget(zd,"frameH");                              //  setup edit curves
   spldat *sd = splcurve_init(frameH,bright_ramp_curvedit);
   EFbright_ramp.curves = sd;

   sd->Nscale = 1;                                                               //  horizontal fixed line, neutral curve
   sd->xscale[0][0] = 0.01;
   sd->yscale[0][0] = 0.50;
   sd->xscale[1][0] = 0.99;
   sd->yscale[1][0] = 0.50;

   for (int ii = 0; ii < 4; ii++)                                                //  loop curves 0-3
   {
      sd->nap[ii] = 3;                                                           //  initial curves are neutral
      sd->vert[ii] = 0;
      sd->fact[ii] = 0;
      sd->apx[ii][0] = 0.01;
      sd->apx[ii][1] = 0.50;                                                     //  curve 0 = overall brightness
      sd->apx[ii][2] = 0.99;                                                     //  curve 1/2/3 = R/G/B adjustment
      sd->apy[ii][0] = 0.5;
      sd->apy[ii][1] = 0.5;
      sd->apy[ii][2] = 0.5;
      splcurve_generate(sd,ii);
   }

   sd->Nspc = 4;                                                                 //  4 curves
   sd->fact[0] = 1;                                                              //  curve 0 active
   zdialog_stuff(zd,"all",1);                                                    //  stuff default selection, all

   zdialog_resize(zd,200,200);
   zdialog_run(zd,bright_ramp_dialog_event,"save");                              //  run dialog - parallel

   takeMouse(bright_ramp_mousefunc,dragcursor);                                  //  connect mouse
   return;
}


//  dialog event and completion callback function

int bright_ramp_dialog_event(zdialog *zd, cchar *event)
{
   using namespace bright_ramp_names;

   void   bright_ramp_mousefunc();

   int      ii, Fupdate = 0;

   spldat *sd = EFbright_ramp.curves;

   if (strmatch(event,"done")) zd->zstat = 2;                                    //  apply and quit
   if (strmatch(event,"cancel")) zd->zstat = 3;                                  //  cancel
   if (strmatch(event,"apply")) Fupdate++;                                       //  from script
   
   if (strmatch(event,"focus")) takeMouse(bright_ramp_mousefunc,dragcursor);     //  connect mouse
   
   if (strmatch(event,"fullsize")) {                                             //  from select area
      edit_fullsize();
      E3ww = E3pxm->ww;
      E3hh = E3pxm->hh;
      signal_thread();
      return 1;
   }

   if (zd->zstat)
   {
      if (zd->zstat == 1)                                                        //  reset
      {
         for (int ii = 0; ii < 4; ii++) {                                        //  loop curves 0-3
            sd->nap[ii] = 3;                                                     //  all curves are neutral
            sd->vert[ii] = 0;
            sd->fact[ii] = 0;
            sd->apx[ii][0] = 0.01;
            sd->apx[ii][1] = 0.50;
            sd->apx[ii][2] = 0.99;
            sd->apy[ii][0] = 0.5;
            sd->apy[ii][1] = 0.5;
            sd->apy[ii][2] = 0.5;
            splcurve_generate(sd,ii);
            sd->fact[ii] = 0;
         }

         sd->fact[0] = 1;
         gtk_widget_queue_draw(sd->drawarea);                                    //  draw curve 0
         zdialog_stuff(zd,"all",1);
         zdialog_stuff(zd,"red",0); 
         zdialog_stuff(zd,"green",0);
         zdialog_stuff(zd,"blue",0);
         edit_reset();                                                           //  restore initial image
         zd->zstat = 0;
         return 1;
      }
         
      if (zd->zstat == 2) {                                                      //  done
         edit_fullsize();                                                        //  get full size image
         E3ww = E3pxm->ww;
         E3hh = E3pxm->hh;
         signal_thread();
         edit_done(0);                                                           //  commit edit
      }
      else edit_cancel(0);                                                       //  discard edit

      Ntoplines = 0;
      Fpaint2();
      return 1;
   }

   if (strstr("all red green blue",event))                                       //  new choice of curve
   {
      zdialog_fetch(zd,event,ii);
      if (! ii) return 0;                                                        //  button OFF event, wait for ON event

      for (ii = 0; ii < 4; ii++)
         sd->fact[ii] = 0;
      ii = strmatchV(event,"all","red","green","blue",null);
      ii = ii-1;                                                                 //  new active curve: 0, 1, 2, 3
      sd->fact[ii] = 1;

      splcurve_generate(sd,ii);                                                  //  regenerate curve
      gtk_widget_queue_draw(sd->drawarea);                                       //  draw curve

      Fupdate = 1;
   }

   if (strmatch(event,"blendwidth")) Fupdate = 1;

   if (Fupdate) signal_thread();                                                 //  trigger image update

   return 1;
}


//  bright_ramp mouse function

void bright_ramp_mousefunc()
{
   using namespace bright_ramp_names;
   
   int      mx = 0, my = 0;
   float    d1, d2;
   
   if (! (LMclick || RMclick || Mxdrag || Mydrag)) return;                       //  ignore mouse movement
   
   if (LMclick || RMclick) {                                                     //  left or right mouse click
      mx = Mxclick;
      my = Myclick;
      LMclick = RMclick = 0;
   }

   if (Mxdrag || Mydrag) {                                                       //  mouse drag
      mx = Mxdrag;
      my = Mydrag;
      Mxdrag = Mydrag = 0;
   }
   
   if (! Fline && (mx || my))                                                    //  19.0
   {
      Fline = 1;
      linex1 = mx;                                                               //  draw arbitrary line to start with
      liney1 = my;
      linex2 = mx + 100;
      liney2 = my + 100;
   }
   
   else                                                                          //  move nearest line end point to mouse
   {
      d1 = (linex1 - mx) * (linex1 - mx) + (liney1 - my) * (liney1 - my);
      d2 = (linex2 - mx) * (linex2 - mx) + (liney2 - my) * (liney2 - my);

      if (d1 < d2) {
         linex1 = mx;
         liney1 = my;
      }
      else {
         linex2 = mx;
         liney2 = my;
      }
   }
   
   Ntoplines = 1;                                                                //  update line data
   toplines[0].x1 = linex1;
   toplines[0].y1 = liney1;
   toplines[0].x2 = linex2;
   toplines[0].y2 = liney2;
   toplines[0].type = 1;

   signal_thread();                                                              //  update image
   return;
}


//  this function is called when a curve is edited

void bright_ramp_curvedit(int spc)
{
   signal_thread();
   return;
}


//  bright_ramp thread function

void * bright_ramp_thread(void *arg)
{
   using namespace bright_ramp_names;

   void bright_ramp_equation();
   void bright_ramp_rampline();
   void * bright_ramp_wthread(void *);

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      ex1 = linex1;                                                              //  ramp line end points
      ey1 = liney1;
      ex2 = linex2;
      ey2 = liney2;

      bright_ramp_equation();                                                    //  compute line equation
      bright_ramp_rampline();                                                    //  compute new end points

      do_wthreads(bright_ramp_wthread,NWT);                                      //  worker threads

      CEF->Fmods++;                                                              //  image3 modified
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


void * bright_ramp_wthread(void *arg)                                            //  worker thread function
{
   using namespace bright_ramp_names;

   void bright_ramp_posn(int px, int py, float &rx, float &ry);

   int         index = *((int *) arg);
   int         ii, dist = 0, px3, py3;
   float       x3, y3;
   float       d1, d2, rampval;
   float       *pix1, *pix3;
   float       red1, green1, blue1, maxrgb;
   float       red3, green3, blue3;
   float       Fall, Fred, Fgreen, Fblue;
   float       dold, dnew;

   spldat *sd = EFbright_ramp.curves;

   for (py3 = index; py3 < E3hh; py3 += NWT)                                     //  loop output pixels
   for (px3 = 0; px3 < E3ww; px3++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py3 * E3ww + px3;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  pixel outside area
      }

      bright_ramp_posn(px3,py3,x3,y3);                                           //  nearest point on ramp line

      d1 = sqrtf((x3-ex1) * (x3-ex1) + (y3-ey1) * (y3-ey1));                     //  compute ramp value
      d2 = sqrtf((x3-ex2) * (x3-ex2) + (y3-ey2) * (y3-ey2));
      rampval = d1 / (d1 + d2);                                                  //  0.0 ... 1.0

      ii = 999.0 * rampval;                                                      //  corresp. curve index 0-999

      Fall = sd->yval[0][ii] * 2.0;                                              //  curve values 0.0 - 1.0
      Fred = sd->yval[1][ii] * 2.0;                                              //  (0.5 is neutral value)
      Fgreen = sd->yval[2][ii] * 2.0;
      Fblue = sd->yval[3][ii] * 2.0;
      
      pix1 = PXMpix(E1pxm,px3,py3);                                              //  input pixel
      red1 = pix1[0];
      green1 = pix1[1];
      blue1 = pix1[2];
      
      red3 = red1 * Fall;                                                        //  curve "all" adjustment
      green3 = green1 * Fall;                                                    //    projected on each RGB color
      blue3 = blue1 * Fall;

      red3 = red3 * Fred;                                                        //  add additional RGB adjustments
      green3 = green3 * Fgreen;
      blue3 = blue3 * Fblue;

      maxrgb = red3;                                                             //  stop overflows
      if (green3 > maxrgb) maxrgb = green3;
      if (blue3 > maxrgb) maxrgb = blue3;
      if (maxrgb > 255.9) {
         red3 = red3 * 255.9 / maxrgb;
         green3 = green3 * 255.9 / maxrgb;
         blue3 = blue3 * 255.9 / maxrgb;
      }

      if (sa_stat == 3 && dist < sa_blendwidth) {                                //  blend changes over blendwidth
         dnew = sa_blendfunc(dist);
         dold = 1.0 - dnew;
         red3 = dnew * red3 + dold * red1;
         green3 = dnew * green3 + dold * green1;
         blue3 = dnew * blue3 + dold * blue1;
      }

      pix3 = PXMpix(E3pxm,px3,py3);                                              //  output pixel
      pix3[0] = red3;
      pix3[1] = green3;
      pix3[2] = blue3;
   }

   pthread_exit(0);
}


//  get equation of ramp line in the form  Ax + By + C = 0
//  end points are (ex1,ey1) and (ex2,ey2)

void bright_ramp_equation()
{
   using namespace bright_ramp_names;

   if (ex1 != ex2)
   {
      A = (ey2 - ey1) / (ex2 - ex1);
      B = -1;
      C = ey1 - A * ex1;
   }
   else {
      A = 1;
      B = 0;
      C = -ex1;
   }
   return;
}


//  compute nearest point on ramp line for given image pixel position

void bright_ramp_posn(int px, int py, float &rx, float &ry)
{
   using namespace bright_ramp_names;
   
   float    F1, F2;

   F1 = B * px - A * py;
   F2 = A * A + B * B;
   rx = (B * F1 - A * C) / F2;
   ry = (-A * F1 - B * C) / F2;

   return;
}


//  extend ramp line end points long enough for entire image

void bright_ramp_rampline()
{
   using namespace bright_ramp_names;

   void bright_ramp_posn(int px, int py, float &rx, float &ry);

   float    rx, ry, d1, d2;
   
   if (B == 0) {                                                                 //  vertical line
      ey1 = 0;
      ey2 = E3hh - 1;
      return;
   }

   if (A == 0) {                                                                 //  horizontal line
      ex1 = 0;
      ex2 = E3ww - 1;
      return;
   }

   bright_ramp_posn(0,0,rx,ry);
   if (rx < 0 || ry < 0) {
      d1 = (rx - ex1) * (rx - ex1) + (ry - ey1) * (ry - ey1);
      d2 = (rx - ex2) * (rx - ex2) + (ry - ey2) * (ry - ey2);
      if (d1 < d2) {
         ex1 = rx;
         ey1 = ry;
      }
      else {
         ex2 = rx;
         ey2 = ry;
      }
   }      

   bright_ramp_posn(E3ww,0,rx,ry);
   if (rx > E3ww || ry < 0) {
      d1 = (rx - ex1) * (rx - ex1) + (ry - ey1) * (ry - ey1);
      d2 = (rx - ex2) * (rx - ex2) + (ry - ey2) * (ry - ey2);
      if (d1 < d2) {
         ex1 = rx;
         ey1 = ry;
      }
      else {
         ex2 = rx;
         ey2 = ry;
      }
   }      

   bright_ramp_posn(E3ww,E3hh,rx,ry);
   if (rx > E3ww || ry > E3hh) {
      d1 = (rx - ex1) * (rx - ex1) + (ry - ey1) * (ry - ey1);
      d2 = (rx - ex2) * (rx - ex2) + (ry - ey2) * (ry - ey2);
      if (d1 < d2) {
         ex1 = rx;
         ey1 = ry;
      }
      else {
         ex2 = rx;
         ey2 = ry;
      }
   }      

   bright_ramp_posn(0,E3hh,rx,ry);
   if (rx < 0 || ry > E3hh) {
      d1 = (rx - ex1) * (rx - ex1) + (ry - ey1) * (ry - ey1);
      d2 = (rx - ex2) * (rx - ex2) + (ry - ey2) * (ry - ey2);
      if (d1 < d2) {
         ex1 = rx;
         ey1 = ry;
      }
      else {
         ex2 = rx;
         ey2 = ry;
      }
   }      

   return;
}


/********************************************************************************/

//  Pixel paint function - paint individual pixels with the mouse.
//  The mouse circle paints a selected color.

namespace paint_image_names
{
   int   paint_dialog_event(zdialog* zd, cchar *event);
   void  paint_mousefunc();
   void  paint_dopixels(int px, int py);                                         //  update pixel block
   void  paint_savepixB(int px, int py);                                         //  save pixel block for poss. undo
   void  paint_undolastB();                                                      //  undo last pixel block, free memory
   void  paint_freefirstB();                                                     //  free memory for first pixel block
   void  paint_freeallB();                                                       //  free memory for all pixel blocks

   uint8    RGB[3] = { 0, 0, 0 };                                                //  color to paint
   int      mode;                                                                //  1/2 = paint / erase
   int      Mradius;                                                             //  mouse radius
   int      Fptran = 0;                                                          //  flag, paint over transparent areas
   int      Fdrag = 0;                                                           //  flag, mouse drags image
   int      nc, ac;                                                              //  no. channels, alpha channel
   float    kernel[202][202];                                                    //  Mradius <= 100

   int64    maxmem = (int64) 4000 * MEGA;                                        //  max. pixel block memory (4 GB)
   int64    totmem;                                                              //  pixB memory allocated
   int      maxpixB = 10000;                                                     //  max. pixel blocks
   int      totpixB = 0;                                                         //  total pixel blocks
   int      pixBseq = 0;                                                         //  last pixel block sequence no.

   typedef struct {                                                              //  pixel block before edit
      int         seq;                                                           //  block sequence no.
      uint16      px, py;                                                        //  center pixel (radius org.)
      uint16      radius;                                                        //  radius of pixel block
      float       pixel[][4];                                                    //  array of pixel[npix][4]
   }  pixBmem_t;
   
   pixBmem_t   **pixBmem = 0;                                                    //  *pixBmem_t[]

   int   pixBmem_cc = 12;                                                        //  all except pixel array + pad
   int   pcc4 = 4 * sizeof(float);                                               //  pixel cc: RGBA = 4 channels

   editfunc    EFpaint;
}


//  menu function

void m_paint_image(GtkWidget *, cchar *)                                         //  separate paint and copy
{
   using namespace paint_image_names;

   cchar    *mess1 = E2X("shift + left click: pick color from image \n"
                         "left drag: paint color on image \n"                    //  remove click actions
                         "right drag: restore original image");
   cchar    *dash = "  \xcc\xb6 ";

   F1_help_topic = "paint_image";

   EFpaint.menufunc = m_paint_image;
   EFpaint.funcname = "paint_image";
   EFpaint.Farea = 2;                                                            //  select area OK
   EFpaint.mousefunc = paint_mousefunc;                                          //  mouse function
   if (! edit_setup(EFpaint)) return;                                            //  setup edit

   /****
             __________________________________________________
            |                 Paint on Image                   |
            |                                                  |
            |  shift + left click: pick color from image       |
            |  left drag: paint color on image                 |
            |  right drag: restore original image              |
            |                                                  |
            |  paint color [######]     [palette]   [HSL]      |
            |                                                  |
            |  brush size      NN ============[]=============  |
            |  opacity center  NN ==================[]=======  |
            |  opacity edge    NN ====[]=====================  |
            |                                                  |
            |  (o) paint  (o) erase    [undo last] [undo all]  |
            |  [x] include transparent areas                   |
            |  [x] drag image     zoom image [+] [-]           |
            |                                                  |
            |                                 [done] [cancel]  |
            |__________________________________________________|

   ****/

   zdialog *zd = zdialog_new(E2X("Paint on Image"),Mwin,Bdone,Bcancel,null);
   EFpaint.zd = zd;

   zdialog_add_widget(zd,"label","labm","dialog",mess1,"space=5");

   zdialog_add_widget(zd,"hbox","hbp","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labp","hbp",E2X("paint color"),"space=5");
   zdialog_add_widget(zd,"colorbutt","colorbutt","hbp","255|0|0");
   zdialog_add_widget(zd,"label","space","hbp",0,"space=10");
   zdialog_add_widget(zd,"button","palette","hbp","palette","space=10");
   zdialog_add_widget(zd,"button","HSL","hbp","HSL");

   zdialog_add_widget(zd,"hbox","hbbru","dialog",0,"space=5");
   zdialog_add_widget(zd,"vbox","vbbru1","hbbru",0,"homog|space=1");
   zdialog_add_widget(zd,"vbox","vbbru2","hbbru",0,"homog|space=1");
   zdialog_add_widget(zd,"vbox","vbbru3","hbbru",0,"homog|expand|space=1");

   zdialog_add_widget(zd,"label","labbr","vbbru1",E2X("brush size"));
   zdialog_add_widget(zd,"label","laboc","vbbru1",Bopacitycenter);
   zdialog_add_widget(zd,"label","laboe","vbbru1",Bopacityedge);

   zdialog_add_widget(zd,"label","labbrNN","vbbru2","NN");
   zdialog_add_widget(zd,"label","labocNN","vbbru2","NNN");
   zdialog_add_widget(zd,"label","laboeNN","vbbru2","NNN");

   zdialog_add_widget(zd,"hscale","Mradius","vbbru3","1|100|1|20","expand");
   zdialog_add_widget(zd,"hscale","opccent","vbbru3","1|100|1|50","expand");
   zdialog_add_widget(zd,"hscale","opcedge","vbbru3","0|100|1|100","expand");

   zdialog_add_widget(zd,"hbox","hbp","dialog",0,"space=3");
   zdialog_add_widget(zd,"radio","paint","hbp",E2X("paint"),"space=3");
   zdialog_add_widget(zd,"radio","erase","hbp",E2X("erase"));
   zdialog_add_widget(zd,"button","undlast","hbp",Bundolast,"space=5");
   zdialog_add_widget(zd,"button","undall","hbp",Bundoall);

   zdialog_add_widget(zd,"hbox","hbt","dialog",0,"space=3");
   zdialog_add_widget(zd,"check","Fptran","hbt",E2X("include transparent areas"),"space=3");
   
   zdialog_add_widget(zd,"hbox","hbd","dialog");
   zdialog_add_widget(zd,"check","Fdrag","hbd",E2X("drag image"),"space=3");
   zdialog_add_widget(zd,"label","space","hbd",0,"space=10");
   zdialog_add_widget(zd,"label","labzoom","hbd",E2X("zoom image"),"space=3");
   zdialog_add_widget(zd,"button","zoom+","hbd"," + ","space=3");
   zdialog_add_widget(zd,"button","zoom-","hbd",dash,"space=3");

   zdialog_rescale(zd,"Mradius",1,2,100);                                        //  stretch scales at sensitive end
   zdialog_rescale(zd,"opccent",1,2,100);
   zdialog_rescale(zd,"opcedge",0,1,100);
   
   zdialog_restore_inputs(zd);                                                   //  preload prior user inputs
   
   zdialog_stuff(zd,"Fptran",0);                                                 //  initialize
   zdialog_stuff(zd,"paint",1);
   zdialog_stuff(zd,"erase",0);
   zdialog_stuff(zd,"Fdrag",0);
   mode = 1;
   Fdrag = 0;

   zdialog_run(zd,paint_dialog_event,"save");                                    //  run dialog, parallel
   
   zdialog_send_event(zd,"colorbutt");                                           //  initialize paint color
   zdialog_send_event(zd,"Mradius");                                             //  get kernel initialized
   zdialog_fetch(zd,"Fptran",Fptran);                                            //  paint over transparent areas

   totmem = 0;                                                                   //  memory used
   pixBmem = 0;                                                                  //  pixel block memory
   totpixB = 0;                                                                  //  pixel blocks
   pixBseq = 0;
   
   ac = 0;
   nc = E1pxm->nc;                                                               //  channels, RGBA
   if (nc > 3) ac = 1;                                                           //  alpha channel present

   takeMouse(paint_mousefunc,drawcursor);                                        //  connect mouse function
   return;
}


//  dialog event and completion callback function

int paint_image_names::paint_dialog_event(zdialog *zd, cchar *event)
{
   using namespace paint_image_names;

   cchar       *pp;
   char        color[20], text[20];
   float       opccent, opcedge;                                                 //  center and edge opacities
   int         paint, radius, dx, dy, err;
   float       rad, kern;

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()

   if (zd->zstat)
   {
      if (zd->zstat == 1) edit_done(0);                                          //  commit edit
      else edit_cancel(0);                                                       //  discard edit
      paint_freeallB();                                                          //  free pixel block memory
      return 1;
   }

   draw_mousecircle(0,0,0,1,0);                                                  //  erase mouse circle

   if (strmatch(event,"focus"))                                                  //  toggle mouse capture
      takeMouse(paint_mousefunc,drawcursor);

   if (strmatch(event,"colorbutt"))
   {
      zdialog_fetch(zd,"colorbutt",color,19);                                    //  get paint color from color wheel
      pp = strField(color,"|",1);
      if (pp) RGB[0] = atoi(pp);
      pp = strField(color,"|",2);
      if (pp) RGB[1] = atoi(pp);
      pp = strField(color,"|",3);
      if (pp) RGB[2] = atoi(pp);
   }
   
   if (strmatch(event,"palette")) 
      err = color_chooser(zd,"colorbutt",RGB);                                   //  select color from palette          19.0

   if (strmatch(event,"HSL")) {
      err = HSL_chooser(zd,"colorbutt",RGB);                                     //  select color from palette          19.0
      if (err) return 1;
      snprintf(color,20,"%d|%d|%d",RGB[0],RGB[1],RGB[2]);
      zdialog_stuff(zd,"colorbutt",color);
   }

   if (strstr("Mradius opccent opcedge",event))
   {
      zdialog_fetch(zd,"Mradius",Mradius);                                       //  get new brush attributes
      zdialog_fetch(zd,"opccent",opccent);
      zdialog_fetch(zd,"opcedge",opcedge);

      sprintf(text,"%3d",Mradius);                                               //  stuff corresp. number values
      zdialog_stuff(zd,"labbrNN",text);
      sprintf(text,"%3.0f",opccent);
      zdialog_stuff(zd,"labocNN",text);
      sprintf(text,"%3.0f",opcedge);
      zdialog_stuff(zd,"laboeNN",text);

      opccent = 0.01 * opccent;                                                  //  opacity  0 ... 1
      opcedge = 0.01 * opcedge;
      opccent = pow(opccent,2.0);                                                //  change response curve
      opcedge = opccent * opcedge;                                               //  edge relative to center

      radius = Mradius;

      for (dy = -radius; dy <= radius; dy++)                                     //  build kernel
      for (dx = -radius; dx <= radius; dx++)
      {
         rad = sqrt(dx*dx + dy*dy);
         kern = (radius - rad) / radius;                                         //  center ... edge  >>  1 ... 0
         kern = kern * (opccent - opcedge) + opcedge;                            //  opacity  center ... edge
         if (rad > radius) kern = 0;                                             //  beyond radius, within square
         if (kern < 0) kern = 0;
         if (kern > 1) kern = 1;
         kernel[dx+radius][dy+radius] = kern;
      }
   }

   if (strmatch(event,"undlast"))                                                //  undo last edit (click or drag)
      paint_undolastB();

   if (strmatch(event,"undall")) {                                               //  undo all edits
      edit_reset();
      paint_freeallB();
   }

   if (strmatch(event,"Fptran"))                                                 //  flag, paint over transparency
      zdialog_fetch(zd,"Fptran",Fptran);
   
   if (strstr("paint erase",event)) {                                            //  set paint or erase mode
      zdialog_fetch(zd,"paint",paint);
      if (paint) mode = 1;
      else mode = 2;
   }
   
   if (strmatch(event,"Fdrag"))                                                  //  mouse drags image
      zdialog_fetch(zd,"Fdrag",Fdrag);
   
   if (strmatch(event,"zoom+")) m_zoom(0,"in");                                  //  zoom image in or out
   if (strmatch(event,"zoom-")) m_zoom(0,"out");

   return 1;
}


//  mouse function

void paint_image_names::paint_mousefunc()                                              //  no action on clicks
{
   using namespace paint_image_names;

   static int  pmxdown = 0, pmydown = 0;
   static int  pmxdrag = 0, pmydrag = 0;
   char        color[20];
   float       *pix3;
   zdialog     *zd = EFpaint.zd;
   
   if (Fdrag) return;                                                            //  pass through to main()

   if (LMclick && KBshiftkey)                                                    //  shift + left mouse click
   {
      LMclick = 0;
      pix3 = PXMpix(E3pxm,Mxclick,Myclick);                                      //  pick new color from image
      RGB[0] = pix3[0];
      RGB[1] = pix3[1];
      RGB[2] = pix3[2];
      snprintf(color,19,"%d|%d|%d",RGB[0],RGB[1],RGB[2]);
      if (zd) zdialog_stuff(zd,"colorbutt",color);
      return;
   }
   
   if (Mxdrag || Mydrag)                                                         //  drag in progress
   {
      if (Mxdown != pmxdown || Mydown != pmydown) {                              //  new drag
         pixBseq++;                                                              //  new undo seq. no.
         pmxdown = Mxdown;
         pmydown = Mydown;
      }

      if (Mxdrag == pmxdrag && Mydrag == pmydrag) {                              //  no movement
         Mxdrag = Mydrag = 0;
         return;
      }

      pmxdrag = Mxdrag;
      pmydrag = Mydrag;

      paint_dopixels(Mxdrag,Mydrag);                                             //  do 1 block of pixels
   }

   draw_mousecircle(Mxposn,Myposn,Mradius,0,0);                                  //  draw mouse circle

   Mxdrag = Mydrag = LMclick = 0;
   return;
}


//  paint or erase 1 block of pixels within mouse radius of px, py

void paint_image_names::paint_dopixels(int px, int py)
{
   using namespace paint_image_names;

   float       *pix1, *pix3;
   int         radius, dx, dy, qx, qy;
   int         ii, ww, hh, dist = 0;
   int         pot = ac * Fptran;                                                //  paint over transparent areas
   float       red, green, blue;
   float       kern;

   cairo_t *cr = draw_context_create(gdkwin,draw_context);

   draw_mousecircle(0,0,0,1,cr);                                                 //  erase mouse circle

   ww = E3pxm->ww;
   hh = E3pxm->hh;

   paint_savepixB(px,py);                                                        //  save pixels for poss. undo

   red = RGB[0];                                                                 //  paint color
   green = RGB[1];
   blue = RGB[2];

   radius = Mradius;

   if (mode == 1) {                                                              //  paint
      CEF->Fmods++;
      CEF->Fsaved = 0;
   }

   for (dy = -radius; dy <= radius; dy++)                                        //  loop surrounding block of pixels
   for (dx = -radius; dx <= radius; dx++)
   {
      qx = px + dx;
      qy = py + dy;

      if (qx < 0 || qx > ww-1) continue;
      if (qy < 0 || qy > hh-1) continue;

      if (sa_stat == 3) {                                                        //  select area active
         ii = qy * ww + qx;
         dist = sa_pixmap[ii];
         if (! dist) continue;                                                   //  pixel is outside area
      }

      kern = kernel[dx+radius][dy+radius];                                       //  mouse opacities
      if (kern == 0) continue;                                                   //  outside mouse radius

      if (sa_stat == 3 && dist < sa_blendwidth)                                  //  select area edge blend,
         kern = kern * sa_blendfunc(dist);                                       //    reduce opacity

      pix1 = PXMpix(E1pxm,qx,qy);                                                //  source image pixel
      pix3 = PXMpix(E3pxm,qx,qy);                                                //  edited image pixel
      
      if (mode == 1 && Mbutton < 2)                                              //  paint
      {
         kern = kern * (1.0 + 50.0 * wacom_pressure);                            //  wacom 
         if (kern > 1.0) kern = 1.0;
         pix3[0] = kern * red   + (1.0 - kern) * pix3[0];                        //  overpaints accumulate
         pix3[1] = kern * green + (1.0 - kern) * pix3[1];
         pix3[2] = kern * blue  + (1.0 - kern) * pix3[2];
         if (pot) pix3[3] = kern * 255.0  + (1.0 - kern) * pix3[3];              //  overpaint transparent area
      }

      if (mode == 2 || Mbutton >= 2)                                             //  erase
      {
         kern = kern * (2.0 + 50.0 * wacom_pressure);                            //  wacom 
         if (kern > 1.0) kern = 1.0;
         if (kern > 1) kern = 1;
         pix3[0] = kern * pix1[0] + (1.0 - kern) * pix3[0];                      //  gradual erase
         pix3[1] = kern * pix1[1] + (1.0 - kern) * pix3[1];
         pix3[2] = kern * pix1[2] + (1.0 - kern) * pix3[2];
         if (pot) pix3[3] = kern * pix1[3] + (1.0 - kern) * pix3[3];
      }
   }

   px = px - radius - 1;                                                         //  repaint modified area
   py = py - radius - 1;
   ww = 2 * radius + 3;
   Fpaint3(px,py,ww,ww,cr);

   draw_context_destroy(draw_context);
   return;
}


//  save 1 block of pixels for possible undo

void paint_image_names::paint_savepixB(int px, int py)
{
   using namespace paint_image_names;

   int            cc, npix, radius, dx, dy;
   float          *pix3;
   pixBmem_t      *paintsave1;
   
   if (! pixBmem) {                                                              //  first time
      pixBmem = (pixBmem_t **) zmalloc(maxpixB * sizeof(void *));
      totpixB = 0;
      totmem = 0;
   }

   if (totmem > maxmem || totpixB == maxpixB)                                    //  free memory for oldest updates
      while (totmem > 0.7 * maxmem || totpixB > 0.7 * maxpixB)
         paint_freefirstB();                                     

   radius = Mradius;
   npix = 0;

   for (dy = -radius; dy <= radius; dy++)                                        //  count pixels in block
   for (dx = -radius; dx <= radius; dx++)
   {
      if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
      if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
      npix++;
   }

   cc = npix * pcc4 + pixBmem_cc;
   paintsave1 = (pixBmem_t *) zmalloc(cc);                                       //  allocate memory for block
   pixBmem[totpixB] = paintsave1;
   totpixB += 1;
   totmem += cc;

   paintsave1->seq = pixBseq;                                                    //  save pixel block poop
   paintsave1->px = px;
   paintsave1->py = py;
   paintsave1->radius = radius;

   npix = 0;

   for (dy = -radius; dy <= radius; dy++)                                        //  save pixels in block
   for (dx = -radius; dx <= radius; dx++)
   {
      if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
      if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
      pix3 = PXMpix(E3pxm,(px+dx),(py+dy));                                      //  edited image pixel
      paintsave1->pixel[npix][0] = pix3[0];
      paintsave1->pixel[npix][1] = pix3[1];
      paintsave1->pixel[npix][2] = pix3[2];
      if (ac) paintsave1->pixel[npix][3] = pix3[3];
      npix++;
   }

   return;
}


//  undo last pixel block (newest edit) and free memory

void paint_image_names::paint_undolastB()
{
   using namespace paint_image_names;

   int            ii, cc, npix, radius;
   int            ww, px, py, dx, dy;
   float          *pix3;
   pixBmem_t      *paintsave1;

   for (ii = totpixB-1; ii >= 0; ii--)
   {
      paintsave1 = pixBmem[ii];
      if (paintsave1->seq != pixBseq) break;
      px = paintsave1->px;
      py = paintsave1->py;
      radius = paintsave1->radius;

      npix = 0;
      for (dy = -radius; dy <= radius; dy++)
      for (dx = -radius; dx <= radius; dx++)
      {
         if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
         if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
         pix3 = PXMpix(E3pxm,(px+dx),(py+dy));
         pix3[0] = paintsave1->pixel[npix][0];
         pix3[1] = paintsave1->pixel[npix][1];
         pix3[2] = paintsave1->pixel[npix][2];
         if (ac) pix3[3] = paintsave1->pixel[npix][3];
         npix++;
      }

      px = px - radius - 1;
      py = py - radius - 1;
      ww = 2 * radius + 3;
      Fpaint3(px,py,ww,ww,0);

      zfree(paintsave1);
      pixBmem[ii] = 0;
      cc = npix * pcc4 + pixBmem_cc;
      totmem -= cc;
      totpixB--;
   }

   if (pixBseq > 0) --pixBseq;
   return;
}


//  free memory for first pixel block (oldest edit)

void paint_image_names::paint_freefirstB()
{
   using namespace paint_image_names;

   int            firstseq;
   int            ii, jj, cc, npix, radius;
   int            px, py, dx, dy;
   pixBmem_t      *paintsave1;
   
   if (! totpixB) return;
   firstseq = pixBmem[0]->seq;
   
   for (ii = 0; ii < totpixB; ii++)
   {
      paintsave1 = pixBmem[ii];
      if (paintsave1->seq != firstseq) break;
      px = paintsave1->px;
      py = paintsave1->py;
      radius = paintsave1->radius;
      npix = 0;
      for (dy = -radius; dy <= radius; dy++)
      for (dx = -radius; dx <= radius; dx++)
      {
         if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
         if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
         npix++;
      }

      zfree(paintsave1);
      pixBmem[ii] = 0;
      cc = npix * pcc4 + pixBmem_cc;
      totmem -= cc;
   }
   
   for (jj = 0; ii < totpixB; jj++, ii++)
      pixBmem[jj] = pixBmem[ii];   
   
   totpixB = jj;
   return;
}


//  free all pixel block memory

void paint_image_names::paint_freeallB()
{
   using namespace paint_image_names;

   int            ii;
   pixBmem_t      *paintsave1;

   for (ii = totpixB-1; ii >= 0; ii--)
   {
      paintsave1 = pixBmem[ii];
      zfree(paintsave1);
   }

   if (pixBmem) zfree(pixBmem);
   pixBmem = 0;

   pixBseq = 0;
   totpixB = 0;
   totmem = 0;

   return;
}


/********************************************************************************/

//  Select a color from a color chooser image file.
//  Returns color button in callers zdialog.
//  Returns selected RGB color in prgb[3] argument.

namespace color_chooser_names
{
// char        *color_chooser_file;                                              //  defined in fotoxx.h
   char        color[20];
   zdialog     *pzdialog;
   cchar       *pcolorbutt;
   uint8       *pRGB;
   PIXBUF      *pixbuf = 0;
   GtkWidget   *frame, *drawarea;
}


int color_chooser(zdialog *pzd, cchar *pbutt, uint8 prgb[3])                     //  19.0
{
   using namespace color_chooser_names;

   int   color_chooser_dialog_event(zdialog *zd, cchar *event);                  //  dialog events function
   int   color_chooser_draw(GtkWidget *window, cairo_t *cr);                     //  window draw function
   int   color_chooser_mouse(GtkWidget *window, GdkEventButton *);               //  window mouse button event
   
   pzdialog = pzd;                                                               //  copy args from caller
   pcolorbutt = pbutt;
   pRGB = prgb;

   /***
          ____________________________________________
         |             Color Chooser                  |
         |                                            |
         |          click on desired color            |
         |  ________________________________________  |
         | |                                        | |
         | |                                        | |
         | |                                        | |
         | |                  image                 | |
         | |                                        | |
         | |                                        | |
         | |                                        | |
         | |                                        | |
         | |________________________________________| |
         |                                            |
         | [_______________________________] [browse] |
         |                                            |
         |                                   [cancel] |
         |____________________________________________|

   ***/

   zdialog *zd = zdialog_new(E2X("Color Chooser"),Mwin,Bcancel,null);

   zdialog_add_widget(zd,"label","labclick","dialog",E2X("click on desired color"));

   zdialog_add_widget(zd,"frame","frame","dialog");
   frame = zdialog_widget(zd,"frame");
   drawarea = gtk_drawing_area_new();
   gtk_widget_set_size_request(drawarea,-1,200);
   gtk_container_add(GTK_CONTAINER(frame),drawarea);

   zdialog_add_widget(zd,"hbox","hbfile","dialog",0,"space=3");
   zdialog_add_widget(zd,"zentry","file","hbfile",0,"space=3|expand");
   zdialog_add_widget(zd,"button","browse","hbfile",Bbrowse,"space=3");
   zdialog_stuff(zd,"file",color_chooser_file);
   
   G_SIGNAL(drawarea,"draw",color_chooser_draw,0);
   G_SIGNAL(drawarea,"button-press-event",color_chooser_mouse,0);
   gtk_widget_add_events(drawarea,GDK_BUTTON_PRESS_MASK);
   
   zdialog_resize(zd,300,0);
   zdialog_run(zd,color_chooser_dialog_event,"save");

   return 0;
}


//  dialog event and completion function

int color_chooser_dialog_event(zdialog *zd, cchar *event)
{
   using namespace color_chooser_names;

   char     *pp;
   
   if (strmatch(event,"browse")) 
   {
      pp = gallery_select1(color_chooser_file);
      if (! pp) return 1;
      zdialog_stuff(zd,"file",pp);
      if (color_chooser_file) zfree(color_chooser_file);
      color_chooser_file = pp;
      gtk_widget_queue_draw(drawarea);
   }
   
   if (zd->zstat) zdialog_free(zd);
   return 1;
}


//  color chooser window draw function

int color_chooser_draw(GtkWidget *widget, cairo_t *cr)
{
   using namespace color_chooser_names;
   
   PIXBUF      *pixbuf1;
   GError      *gerror = 0;
   GdkWindow   *gdkwin;
   int         ww1, hh1, ww2, hh2;

   if (*color_chooser_file != '/') return 1;                                     //  load last color chooser file

   pixbuf1 = gdk_pixbuf_new_from_file_at_size(color_chooser_file,500,500,&gerror);
   if (! pixbuf1) {
      printz("pixbuf error: %s \n",gerror->message);                             //  popup message >> draw event loop
      return 1;                                                                  //     GTK 3.22.11
   }

   ww1 = gdk_pixbuf_get_width(pixbuf1);                                          //  image dimensions
   hh1 = gdk_pixbuf_get_height(pixbuf1);

   gdkwin = gtk_widget_get_window(widget);                                       //  set drawing area to match
   ww2 = gdk_window_get_width(gdkwin);                                           //    aspect ratio
   hh2 = ww2 * hh1 / ww1;
   gtk_widget_set_size_request(widget,-1,hh2);

   if (pixbuf) g_object_unref(pixbuf);
   pixbuf = gdk_pixbuf_scale_simple(pixbuf1,ww2,hh2,BILINEAR);
   g_object_unref(pixbuf1);
   if (! pixbuf) return 1;

   gdk_cairo_set_source_pixbuf(cr,pixbuf,0,0);                                   //  draw image
   cairo_paint(cr);

   return 1;
}


//  color chooser mouse click function

int color_chooser_mouse(GtkWidget *widget, GdkEventButton *event)
{
   using namespace color_chooser_names;

   int      mx, my, rs, nc;
   uint8    *pixels, *pix1;
   
   if (! pixbuf) return 1;

   rs = gdk_pixbuf_get_rowstride(pixbuf);
   nc = gdk_pixbuf_get_n_channels(pixbuf);
   pixels = gdk_pixbuf_get_pixels(pixbuf);

   mx = event->x;
   my = event->y;
   pix1 = pixels + my * rs + mx * nc;

   snprintf(color,20,"%d|%d|%d",pix1[0],pix1[1],pix1[2]);                        //  update caller's color button       19.0
   zdialog_stuff(pzdialog,pcolorbutt,color);
   
   pRGB[0] = pix1[0];                                                            //  update caller's paint color        19.0
   pRGB[1] = pix1[1];
   pRGB[2] = pix1[2];
   
   return 1;
}


/********************************************************************************/

//  HSL color chooser function
//  Returns color button in callers zdialog.
//  Returns selected RGB color in prgb[3] argument.

namespace HSL_chooser_names
{
   zdialog     *HSLzdialog;
   GtkWidget   *RGBframe, *RGBcolor;
   GtkWidget   *Hframe, *Hscale;
   zdialog     *pzdialog;
   cchar       *pcolorbutt;
   uint8       *pRGB;
   float       H, S, L;                                                          //  chosen HSL color
   float       R, G, B;                                                          //  corresp. RGB color
}

int HSL_chooser(zdialog *pzd, cchar *pbutt, uint8 prgb[3])                       //  19.0
{
   using namespace HSL_chooser_names;

   void   HSL_chooser_RGB(GtkWidget *drawarea, cairo_t *cr, int *);
   void   HSL_chooser_Hscale(GtkWidget *drawarea, cairo_t *cr, int *);
   int    HSL_chooser_dialog_event(zdialog *zd, cchar *event);
   
   pzdialog = pzd;                                                               //  copy args from caller
   pcolorbutt = pbutt;
   pRGB = prgb;
   
/***
       ________________________________________________
      |                                                |
      |  [#######]   [##############################]  |
      |  Color Hue   ================[]==============  |
      |  Saturation  =====================[]=========  |
      |  Lightness   ===========[]===================  |
      |                                                |
      |                                      [cancel]  |
      |________________________________________________|

***/

   zdialog *zd = zdialog_new("Adjust HSL",Mwin,Bcancel,null);
   HSLzdialog = zd;

   zdialog_add_widget(zd,"hbox","hb2","dialog");
   zdialog_add_widget(zd,"vbox","vb1","hb2",0,"homog|space=0");
   zdialog_add_widget(zd,"vbox","vb2","hb2",0,"homog|expand|space=0");

   zdialog_add_widget(zd,"frame","RGBframe","vb1",0,"space=1");                  //  drawing area for RGB color
   RGBframe = zdialog_widget(zd,"RGBframe");
   RGBcolor = gtk_drawing_area_new();
   gtk_container_add(GTK_CONTAINER(RGBframe),RGBcolor);
   gtk_widget_set_size_request(RGBcolor,0,16);
   G_SIGNAL(RGBcolor,"draw",HSL_chooser_RGB,0);

   zdialog_add_widget(zd,"frame","Hframe","vb2",0,"space=1");                    //  drawing area for hue scale
   Hframe = zdialog_widget(zd,"Hframe");
   Hscale = gtk_drawing_area_new();
   gtk_container_add(GTK_CONTAINER(Hframe),Hscale);
   gtk_widget_set_size_request(Hscale,200,16);
   G_SIGNAL(Hscale,"draw",HSL_chooser_Hscale,0);
   
   zdialog_add_widget(zd,"label","labhue","vb1",E2X("Color Hue"));
   zdialog_add_widget(zd,"label","labsat","vb1",E2X("Saturation"));
   zdialog_add_widget(zd,"label","lablgt","vb1",E2X("Lightness"));

   zdialog_add_widget(zd,"hscale","H","vb2","0|359.9|0.1|180","expand");
   zdialog_add_widget(zd,"hscale","S","vb2","0|1|0.001|0.5","expand");
   zdialog_add_widget(zd,"hscale","L","vb2","0|1|0.001|0.5","expand");

   H = 180;                                                                      //  chosen HSL color = not set
   S = 0.5;
   L = 0.5;

   zdialog_run(zd,HSL_chooser_dialog_event,"save");                              //  run dialog - parallel
   return 1;
}


//  Paint RGBcolor drawing area with RGB color from chosen HSL color

void HSL_chooser_RGB(GtkWidget *drawarea, cairo_t *cr, int *)
{
   using namespace HSL_chooser_names;

   int      ww, hh;
   int      r, g, b;
   char     color[20];

   ww = gtk_widget_get_allocated_width(drawarea);                                //  drawing area size
   hh = gtk_widget_get_allocated_height(drawarea);
   
   HSLtoRGB(H,S,L,R,G,B);                                                        //  RGB color from chosen HSL

   cairo_set_source_rgb(cr,R,G,B);
   cairo_rectangle(cr,0,0,ww-1,hh-1);
   cairo_fill(cr);

   r = 255 * R;
   g = 255 * G;
   b = 255 * B;

   snprintf(color,20,"%d|%d|%d",r,g,b);                                          //  update caller's color button       19.0
   zdialog_stuff(pzdialog,pcolorbutt,color);
   
   pRGB[0] = r;                                                                  //  update caller's paint color        19.0
   pRGB[1] = g;
   pRGB[2] = b;

   return;
}


//  Paint Hscale drawing area with all hue values in a horizontal scale

void HSL_chooser_Hscale(GtkWidget *drawarea, cairo_t *cr, int *)
{
   using namespace HSL_chooser_names;

   int      px, ww, hh;
   float    H, S, L, R, G, B;

   ww = gtk_widget_get_allocated_width(drawarea);                                //  drawing area size
   hh = gtk_widget_get_allocated_height(drawarea);
   
   S = L = 0.5;
   
   for (px = 0; px < ww; px++)                                                   //  paint hue color scale
   {
      H = 360 * px / ww;
      HSLtoRGB(H,S,L,R,G,B);
      cairo_set_source_rgb(cr,R,G,B);
      cairo_move_to(cr,px,0);
      cairo_line_to(cr,px,hh-1);
      cairo_stroke(cr);
   }

   return;
}


//  HSL dialog event and completion function

int HSL_chooser_dialog_event(zdialog *zd, cchar *event)                          //  HSL dialog event function
{
   using namespace HSL_chooser_names;

   if (zd->zstat) {                                                              //  zdialog complete
      zdialog_free(zd);
      freeMouse();                                                               //  18.01
      HSLzdialog = 0;
      return 1;
   }

   if (strstr("H S L",event)) {                                                  //  HSL inputs changed
      zdialog_fetch(zd,"H",H);
      zdialog_fetch(zd,"S",S);
      zdialog_fetch(zd,"L",L);
      gtk_widget_queue_draw(RGBcolor);                                           //  draw corresp. RGB color
   }

   return 1;
}


/********************************************************************************/

//  Copy Pixels function.
//  Copy from one image area to another with variable opacity.

namespace copypixels1
{
   int   dialog_event(zdialog* zd, cchar *event);
   void  mousefunc();
   void  dopixels(int px, int py);                                               //  update pixel block
   void  savepixB(int px, int py);                                               //  save pixel block for poss. undo
   void  undolastB();                                                            //  undo last pixel block, free memory
   void  freefirstB();                                                           //  free memory for first pixel block
   void  freeallB();                                                             //  free memory for all pixel blocks

   int      mode;                                                                //  1/2 = paint / erase
   int      Mradius;                                                             //  mouse radius
   int      imagex, imagey;                                                      //  source image location
   float    kernel[402][402];                                                    //  radius <= 200
   int      Fptran = 0;                                                          //  flag, paint over transparent areas
   int      nc, ac;                                                              //  no. channels, alpha channel

   int64    maxmem = (int64) 4000 * MEGA;                                        //  max. pixel block memory
   int64    totmem;                                                              //  pixB memory allocated
   int      maxpixB = 10000;                                                     //  max. pixel blocks
   int      totpixB = 0;                                                         //  total pixel blocks
   int      pixBseq = 0;                                                         //  last pixel block sequence no.

   typedef struct {                                                              //  pixel block before edit
      int         seq;                                                           //  block sequence no.
      uint16      px, py;                                                        //  center pixel (radius org.)
      uint16      radius;                                                        //  radius of pixel block
      float       pixel[][4];                                                    //  array of pixel[npix][4]
   }  pixBmem_t;
   
   pixBmem_t   **pixBmem = 0;                                                    //  *pixBmem_t[]

   int   pixBmem_cc = 12;                                                        //  all except pixel array + pad
   int   pcc4 = 4 * sizeof(float);                                               //  pixel cc: RGBA = 4 channels

   editfunc    EFcopypix1;
}


//  menu function

void m_copypixels1(GtkWidget *, cchar *)                                         //  separate paint and copy
{
   using namespace copypixels1;

   cchar    *mess1 = E2X("shift + left click: pick position to copy \n"
                         "left click or drag: copy image to mouse \n"
                         "right click or drag: restore original image");

   F1_help_topic = "copy_pixels_1";

   EFcopypix1.menufunc = m_copypixels1;
   EFcopypix1.funcname = "copy_pixels_1";
   EFcopypix1.Farea = 2;                                                         //  select area OK
   EFcopypix1.mousefunc = mousefunc;                                             //  mouse function
   if (! edit_setup(EFcopypix1)) return;                                         //  setup edit

   /***
             ____________________________________________________
            |                 Copy Pixels (1 image)              |
            |                                                    |
            |  shift + left click: pick image position to copy   |
            |  left click or drag: copy image to mouse position  |
            |  right click or drag: restore original image       |
            |                                                    |
            |  brush size      [____]     [undo last]            |
            |  opacity center  [____]     [undo all]             |
            |  opacity edge    [____]                            |
            |                                                    |
            |  [x] paint transparent areas                       |
            |                                                    |
            |                                   [done] [cancel]  |
            |____________________________________________________|

   ***/

   zdialog *zd = zdialog_new(E2X("Copy Pixels (1 image)"),Mwin,Bdone,Bcancel,null);
   EFcopypix1.zd = zd;

   zdialog_add_widget(zd,"hbox","hbr","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labm","dialog",mess1,"space=5");
   zdialog_add_widget(zd,"hbox","hbbri","dialog",0,"space=5");
   zdialog_add_widget(zd,"vbox","vbbr1","hbbri",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vbbr2","hbbri",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","space","hbbri",0,"space=10");
   zdialog_add_widget(zd,"vbox","vbbr3","hbbri",0,"space=10");
   zdialog_add_widget(zd,"label","labbr","vbbr1",E2X("brush size"));
   zdialog_add_widget(zd,"label","labtc","vbbr1",Bopacitycenter);
   zdialog_add_widget(zd,"label","labte","vbbr1",Bopacityedge);
   zdialog_add_widget(zd,"zspin","Mradius","vbbr2","1|200|1|30");
   zdialog_add_widget(zd,"zspin","opccent","vbbr2","1|100|1|10");
   zdialog_add_widget(zd,"zspin","opcedge","vbbr2","0|100|1|0");
   zdialog_add_widget(zd,"button","undlast","vbbr3",Bundolast);
   zdialog_add_widget(zd,"button","undall","vbbr3",Bundoall);
   zdialog_add_widget(zd,"hbox","hb4","dialog",0,"space=3");
   zdialog_add_widget(zd,"check","Fptran","hb4",E2X("paint over transparent areas"),"space=5");

   zdialog_restore_inputs(zd);                                                   //  preload prior user inputs
   zdialog_run(zd,dialog_event,"save");                                          //  run dialog, parallel

   zdialog_fetch(zd,"Fptran",Fptran);                                            //  paint over transparent areas

   zdialog_send_event(zd,"Mradius");                                             //  get kernel initialized

   totmem = 0;                                                                   //  memory used
   pixBmem = 0;                                                                  //  pixel block memory
   totpixB = 0;                                                                  //  pixel blocks
   pixBseq = 0;
   imagex = imagey = 0;                                                          //  no source pixels
   
   ac = 0;
   nc = E1pxm->nc;                                                               //  channels, RGBA
   if (nc > 3) ac = 1;                                                           //  alpha channel present

   takeMouse(mousefunc,drawcursor);                                              //  connect mouse function
   return;
}


//  dialog event and completion callback function

int copypixels1::dialog_event(zdialog *zd, cchar *event)
{
   using namespace copypixels1;

   int         radius, dx, dy;
   float       rad, kern, opccent, opcedge;

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()

   if (zd->zstat)
   {
      if (zd->zstat == 1) edit_done(0);                                          //  commit edit
      else edit_cancel(0);                                                       //  discard edit
      freeallB();                                                                //  free pixel block memory
      return 1;
   }

   draw_mousecircle(0,0,0,1,0);                                                  //  erase mouse circle
   draw_mousecircle2(0,0,0,1,0);                                                 //  erase source tracking circle

   if (strmatch(event,"focus"))                                                  //  toggle mouse capture
      takeMouse(mousefunc,drawcursor);

   if (strstr("Mradius opccent opcedge",event))                                  //  get new brush attributes
   {
      zdialog_fetch(zd,"Mradius",Mradius);
      zdialog_fetch(zd,"opccent",opccent);
      zdialog_fetch(zd,"opcedge",opcedge);

      opccent = 0.01 * opccent;                                                  //  scale 0 ... 1
      opcedge = 0.01 * opcedge;
      opccent = pow(opccent,2);                                                  //  change response curve
      opcedge = opccent * opcedge;                                               //  edge relative to center

      radius = Mradius;

      for (dy = -radius; dy <= radius; dy++)                                     //  build kernel
      for (dx = -radius; dx <= radius; dx++)
      {
         rad = sqrt(dx*dx + dy*dy);
         kern = (radius - rad) / radius;                                         //  center ... edge  >>  1 ... 0
         kern = kern * (opccent - opcedge) + opcedge;                            //  opacity  center ... edge
         if (rad > radius) kern = 0;                                             //  beyond radius, within square
         if (kern < 0) kern = 0;
         if (kern > 1) kern = 1;
         kernel[dx+radius][dy+radius] = kern;
      }
   }

   if (strmatch(event,"undlast"))                                                //  undo last edit (click or drag)
      undolastB();

   if (strmatch(event,"undall")) {                                               //  undo all edits
      edit_reset();
      freeallB();
   }

   if (strmatch(event,"Fptran"))                                                 //  flag, paint over transparency
      zdialog_fetch(zd,"Fptran",Fptran);

   return 1;
}


//  pixel paint mouse function

void copypixels1::mousefunc()
{
   using namespace copypixels1;

   static int  pmxdown = 0, pmydown = 0;
   int         px, py;

   if (LMclick && KBshiftkey)                                                    //  shift + left mouse click
   {
      imagex = Mxclick;                                                          //  new source image location
      imagey = Myclick;
   }

   else if (LMclick || RMclick)
   {
      if (LMclick) mode = 1;                                                     //  left click, paint
      if (RMclick) mode = 2;                                                     //  right click, erase

      px = Mxdown = Mxclick;
      py = Mydown = Myclick;
 
      pixBseq++;                                                                 //  new undo seq. no.

      dopixels(px,py);                                                           //  do 1 block of pixels
   }

   else if (Mxdrag || Mydrag)                                                    //  drag in progress
   {
      if (Mbutton == 1) mode = 1;                                                //  left drag, paint
      if (Mbutton == 3) mode = 2;                                                //  right drag, erase

      px = Mxdrag;
      py = Mydrag;

      if (Mxdown != pmxdown || Mydown != pmydown) {                              //  new drag
         pixBseq++;                                                              //  new undo seq. no.
         pmxdown = Mxdown;
         pmydown = Mydown;
      }

      dopixels(px,py);                                                           //  do 1 block of pixels
   }

   draw_mousecircle(Mxposn,Myposn,Mradius,0,0);                                  //  draw mouse circle

   if (mode == 1 && (Mxdown || Mydown)) {                                        //  2nd circle tracks source pixels
      px = imagex + Mxposn - Mxdown;
      py = imagey + Myposn - Mydown;
      if (px > 0 && px < E3pxm->ww-1 && py > 0 && py < E3pxm->hh-1)
         draw_mousecircle2(px,py,Mradius,0,0);
   }
   else draw_mousecircle2(0,0,0,1,0);                                            //  no 2nd circle

   LMclick = RMclick = Mxdrag = Mydrag = 0;
   return;
}


//  paint or erase 1 block of pixels within mouse radius of px, py

void copypixels1::dopixels(int px, int py)
{
   using namespace copypixels1;

   float       *pix1, *pix3;
   int         radius, dx, dy, qx, qy, sx, sy;
   int         ii, ww, hh, dist = 0;
   int         pot = ac * Fptran;                                                //  paint over transparent areas
   float       kern;

   if (! imagex && ! imagey) return;                                             //  no source area defined

   cairo_t *cr = draw_context_create(gdkwin,draw_context);

   draw_mousecircle(0,0,0,1,cr);                                                 //  erase mouse circle
   draw_mousecircle2(0,0,0,1,cr);                                                //  erase source tracking circle

   ww = E3pxm->ww;
   hh = E3pxm->hh;

   savepixB(px,py);                                                              //  save pixels for poss. undo

   radius = Mradius;

   if (mode == 1) {
      CEF->Fmods++;
      CEF->Fsaved = 0;
   }

   for (dy = -radius; dy <= radius; dy++)                                        //  loop surrounding block of pixels
   for (dx = -radius; dx <= radius; dx++)
   {
      qx = px + dx;
      qy = py + dy;

      if (qx < 0 || qx > ww-1) continue;
      if (qy < 0 || qy > hh-1) continue;

      if (sa_stat == 3) {                                                        //  select area active
         ii = qy * ww + qx;
         dist = sa_pixmap[ii];
         if (! dist) continue;                                                   //  pixel is outside area
      }

      kern = kernel[dx+radius][dy+radius];                                       //  mouse opacities
      if (kern == 0) continue;                                                   //  outside mouse radius

      if (sa_stat == 3 && dist < sa_blendwidth)                                  //  select area edge blend,
         kern = kern * sa_blendfunc(dist);                                       //    reduce opacity

      pix1 = PXMpix(E1pxm,qx,qy);                                                //  source image pixel
      pix3 = PXMpix(E3pxm,qx,qy);                                                //  edited image pixel

      if (mode == 1)                                                             //  paint
      {
         sx = imagex + qx - Mxdown;                                              //  image location + mouse drag shift
         sy = imagey + qy - Mydown;
         if (sx < 0) sx = 0;
         if (sx > ww-1) sx = ww-1;
         if (sy < 0) sy = 0;
         if (sy > hh-1) sy = hh-1;
         pix1 = PXMpix(E1pxm,sx,sy);                                             //  source image pixel at location

         kern = 0.3 * kern;
         pix3[0] = kern * pix1[0] + (1.0 - kern) * pix3[0];                      //  overpaints accumulate
         pix3[1] = kern * pix1[1] + (1.0 - kern) * pix3[1];
         pix3[2] = kern * pix1[2] + (1.0 - kern) * pix3[2];
         if (pot) pix3[3] = kern * pix1[3] + (1.0 - kern) * pix3[3];             //  overpaint transparent area
      }

      if (mode == 2)                                                             //  erase
      {
         pix3[0] = kern * pix1[0] + (1.0 - kern) * pix3[0];                      //  gradual erase
         pix3[1] = kern * pix1[1] + (1.0 - kern) * pix3[1];
         pix3[2] = kern * pix1[2] + (1.0 - kern) * pix3[2];
         if (pot) pix3[3] = kern * pix1[3] + (1.0 - kern) * pix3[3];
      }
   }

   px = px - radius - 1;                                                         //  repaint modified area
   py = py - radius - 1;
   ww = 2 * radius + 3;
   Fpaint3(px,py,ww,ww,cr);

   draw_context_destroy(draw_context);
   return;
}


//  save 1 block of pixels for possible undo

void copypixels1::savepixB(int px, int py)
{
   using namespace copypixels1;

   int            cc, npix, radius, dx, dy;
   float          *pix3;
   pixBmem_t      *save1B;
   
   if (! pixBmem) {                                                              //  first time
      pixBmem = (pixBmem_t **) zmalloc(maxpixB * sizeof(void *));
      totpixB = 0;
      totmem = 0;
   }

   if (totmem > maxmem || totpixB == maxpixB)                                    //  free memory for oldest updates
      while (totmem > 0.7 * maxmem || totpixB > 0.7 * maxpixB)
         freefirstB();                                     

   radius = Mradius;
   npix = 0;

   for (dy = -radius; dy <= radius; dy++)                                        //  count pixels in block
   for (dx = -radius; dx <= radius; dx++)
   {
      if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
      if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
      npix++;
   }

   cc = npix * pcc4 + pixBmem_cc;
   save1B = (pixBmem_t *) zmalloc(cc);                                           //  allocate memory for block
   pixBmem[totpixB] = save1B;
   totpixB += 1;
   totmem += cc;

   save1B->seq = pixBseq;                                                        //  save pixel block poop
   save1B->px = px;
   save1B->py = py;
   save1B->radius = radius;

   npix = 0;

   for (dy = -radius; dy <= radius; dy++)                                        //  save pixels in block
   for (dx = -radius; dx <= radius; dx++)
   {
      if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
      if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
      pix3 = PXMpix(E3pxm,(px+dx),(py+dy));                                      //  edited image pixel
      save1B->pixel[npix][0] = pix3[0];
      save1B->pixel[npix][1] = pix3[1];
      save1B->pixel[npix][2] = pix3[2];
      if (ac) save1B->pixel[npix][3] = pix3[3];
      npix++;
   }

   return;
}


//  undo last pixel block (newest edit) and free memory

void copypixels1::undolastB()
{
   using namespace copypixels1;

   int            ii, cc, npix, radius;
   int            ww, px, py, dx, dy;
   float          *pix3;
   pixBmem_t      *save1B;

   for (ii = totpixB-1; ii >= 0; ii--)
   {
      save1B = pixBmem[ii];
      if (save1B->seq != pixBseq) break;
      px = save1B->px;
      py = save1B->py;
      radius = save1B->radius;

      npix = 0;
      for (dy = -radius; dy <= radius; dy++)
      for (dx = -radius; dx <= radius; dx++)
      {
         if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
         if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
         pix3 = PXMpix(E3pxm,(px+dx),(py+dy));
         pix3[0] = save1B->pixel[npix][0];
         pix3[1] = save1B->pixel[npix][1];
         pix3[2] = save1B->pixel[npix][2];
         if (ac) pix3[3] = save1B->pixel[npix][3];
         npix++;
      }

      px = px - radius - 1;
      py = py - radius - 1;
      ww = 2 * radius + 3;
      Fpaint3(px,py,ww,ww,0);

      zfree(save1B);
      pixBmem[ii] = 0;
      cc = npix * pcc4 + pixBmem_cc;
      totmem -= cc;
      totpixB--;
   }

   if (pixBseq > 0) --pixBseq;
   return;
}


//  free memory for first pixel block (oldest edit)

void copypixels1::freefirstB() 
{
   using namespace copypixels1;

   int            firstseq;
   int            ii, jj, cc, npix, radius;
   int            px, py, dx, dy;
   pixBmem_t      *save1B;
   
   if (! totpixB) return;
   firstseq = pixBmem[0]->seq;
   
   for (ii = 0; ii < totpixB; ii++)
   {
      save1B = pixBmem[ii];
      if (save1B->seq != firstseq) break;
      px = save1B->px;
      py = save1B->py;
      radius = save1B->radius;
      npix = 0;
      for (dy = -radius; dy <= radius; dy++)
      for (dx = -radius; dx <= radius; dx++)
      {
         if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
         if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
         npix++;
      }

      zfree(save1B);
      pixBmem[ii] = 0;
      cc = npix * pcc4 + pixBmem_cc;
      totmem -= cc;
   }
   
   for (jj = 0; ii < totpixB; jj++, ii++)
      pixBmem[jj] = pixBmem[ii];   
   
   totpixB = jj;
   return;
}


//  free all pixel block memory

void copypixels1::freeallB()
{
   using namespace copypixels1;

   int            ii;
   pixBmem_t      *save1B;

   for (ii = totpixB-1; ii >= 0; ii--)
   {
      save1B = pixBmem[ii];
      zfree(save1B);
   }

   if (pixBmem) zfree(pixBmem);
   pixBmem = 0;

   pixBseq = 0;
   totpixB = 0;
   totmem = 0;

   return;
}


/********************************************************************************/

//  Copy Pixels function.
//  'Paint' a target image with pixels from a different source image.

namespace copypixels2
{
   int   dialog_event(zdialog* zd, cchar *event);
   void  mousefunc();                                                            //  generate mouse circle
   void  dopixels();                                                             //  copy pixel block to target
   void  save_pixblock();                                                        //  save pixel block for poss. undo
   void  undo_lastblock();                                                       //  undo last pixel block, free memory
   void  free_firstblock();                                                      //  free memory for first pixel block
   void  free_allblocks();                                                       //  free memory for all pixel blocks

   editfunc    EFcopypix2;

   int      mode;                                                                //  1/2 = paint / erase
   int      Fptran;                                                              //  flag, paint over transparent areas

   int      mpxC, mpyC;                                                          //  center of pixel copy area
   int      mpx, mpy;                                                            //  center of moving mouse circle
   int      mrad;                                                                //  radius of pixel copy area

   typedef struct {                                                              //  shared memory data
      int      killsource;                                                       //  source image process should exit
      int      mpxC, mpyC;                                                       //  center of pixel copy area
      int      mpx, mpy;                                                         //  mouse drag position
      int      mrad;                                                             //  mouse radius
      int      Fvalid;                                                           //  mouse data valid flag
      float    Fscale;                                                           //  source image scale factor
      int      Freq;                                                             //  source/target coord. flag
      float    pixels[402*402*4];                                                //  pixel block, max. mrad = 200
   }  mmap_data_t;
   
   mmap_data_t    *mmap_data;                                                    //  shared memory pointer

   float    kernel[402][402];                                                    //  mrad <= 200

   int64    maxmem = (int64) 4000 * MEGA;                                        //  max. pixel block memory
   int64    totmem;                                                              //  pixblock memory allocated
   int      maxpixblock = 10000;                                                 //  max. pixel blocks
   int      totpixblock = 0;                                                     //  total pixel blocks
   int      pixblockseq = 0;                                                     //  last pixel block sequence no.

   typedef struct {                                                              //  saved pixel block before edit
      int         seq;                                                           //  block sequence no.
      uint16      mpx, mpy;                                                      //  center pixel (mrad org.)
      uint16      mrad;                                                          //  radius of pixel block
      float       pixels[][4];                                                   //  array of pixels, rows x cols
   }  pixblockmem_t;
   
   pixblockmem_t   **pixblockmem;
   int      pixblockmem_cc = 12;                                                 //  all except pixel array + pad
}


//  menu function

void m_copypixels2(GtkWidget *, cchar *)                                         //  split source/target processes      18.07
{
   using namespace copypixels2;
   
   int      err, fd;
   size_t   cc;

   fd = shm_open("/fotoxx_copy_pixels2",O_RDWR+O_CREAT,0600);                    //  identify memory mapped region
   if (fd < 0) {
      zmessageACK(Mwin,"shm_open() failure: %s",strerror(errno));
      return;
   }
   
   cc = sizeof(mmap_data_t);
   err = ftruncate(fd,cc);
   if (err) {
      zmessageACK(Mwin,"ftruncate() failure: %s",strerror(errno));
      return;
   }

   mmap_data = (mmap_data_t *) mmap(0,cc,PROT_WRITE,MAP_SHARED,fd,0);            //  create memory mapped region
   if (mmap_data == (void *) -1) {
      zmessageACK(Mwin,"mmap() failure: %s",strerror(errno));
      return;
   }
   
   memset(mmap_data,0,cc);

   mpxC = mpyC = -1;                                                             //  no pixel copy area selected
   mrad = -1;
   mmap_data->Fvalid = 0;                                                        //  no valid target mouse data yet
   mmap_data->Fscale = 1.0;                                                      //  defaule source image scale factor
   
   EFcopypix2.menufunc = m_copypixels2;                                          //  start edit function for target image
   EFcopypix2.funcname = "copy_pixels_2";
   EFcopypix2.Farea = 2;                                                         //  select area OK
   EFcopypix2.mousefunc = mousefunc;                                             //  mouse function
   if (! edit_setup(EFcopypix2)) return;     

   F1_help_topic = "copy_pixels_2";

   PXM_addalpha(E1pxm);
   PXM_addalpha(E3pxm);

   pixblockmem = (pixblockmem_t **) zmalloc(maxpixblock * sizeof(void *));       //  saved pixel blocks list
   totmem = 0;                                                                   //  memory used
   totpixblock = 0;                                                              //  pixel blocks
   pixblockseq = 0;
   
   /****
             __________________________________________________
            |              Copy Pixels (2 images)              |
            |                                                  |
            |  left click: synchronize copy position           |
            |  left click or drag: copy source image to mouse  |
            |  right click or drag: restore original image     |
            |                                                  |
            |  source image scale [____]                       |
            |                                                  |
            |  brush size      [____]     [undo last]          |
            |  opacity center  [____]     [undo all]           |
            |  opacity edge    [____]                          |
            |                                                  |
            |  [x] paint over transparent areas                |
            |                                                  |
            |                                 [done] [cancel]  |
            |__________________________________________________|

   ****/

   cchar    *mess1 = E2X("left click: synchronize copy position \n"
                         "left click or drag: copy source image to mouse \n"
                         "right click or drag: restore original image");

   zdialog *zd = zdialog_new(E2X("Copy Pixels (2 images)"),Mwin,Bdone,Bcancel,null);
   EFcopypix2.zd = zd;

   zdialog_add_widget(zd,"hbox","hbr","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labm","dialog",mess1,"space=5");
   zdialog_add_widget(zd,"hbox","hbsc","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labsc","hbsc",E2X("source image scale"),"space=3");
   zdialog_add_widget(zd,"zspin","scale","hbsc","0.2|5.0|0.01|1.0","space=3");
   zdialog_add_widget(zd,"hbox","hbbri","dialog",0,"space=3");
   zdialog_add_widget(zd,"vbox","vbbr1","hbbri",0,"homog|space=3");
   zdialog_add_widget(zd,"vbox","vbbr2","hbbri",0,"homog|space=3");
   zdialog_add_widget(zd,"vbox","space","hbbri",0,"space=10");
   zdialog_add_widget(zd,"vbox","vbbr3","hbbri",0,"space=10");
   zdialog_add_widget(zd,"label","labbr","vbbr1",E2X("brush size"));
   zdialog_add_widget(zd,"label","labtc","vbbr1",Bopacitycenter);
   zdialog_add_widget(zd,"label","labte","vbbr1",Bopacityedge);
   zdialog_add_widget(zd,"zspin","mrad","vbbr2","1|200|1|30");
   zdialog_add_widget(zd,"zspin","opccent","vbbr2","1|100|1|10");
   zdialog_add_widget(zd,"zspin","opcedge","vbbr2","0|100|1|0");
   zdialog_add_widget(zd,"button","undlast","vbbr3",Bundolast);
   zdialog_add_widget(zd,"button","undall","vbbr3",Bundoall);
   zdialog_add_widget(zd,"hbox","hb4","dialog",0,"space=3");
   zdialog_add_widget(zd,"check","Fptran","hb4",E2X("paint over transparent areas"),"space=5");

   zdialog_restore_inputs(zd);                                                   //  preload prior user inputs
   zdialog_run(zd,dialog_event,"save");                                          //  run dialog, parallel

   zdialog_fetch(zd,"Fptran",Fptran);                                            //  paint over transparent areas

   zdialog_send_event(zd,"mrad");                                                //  get kernel initialized

   takeMouse(mousefunc,drawcursor);                                              //  connect mouse function

   new_session("-x 1 -m \"Copy Pixels 3\" ");                                    //  start source image fotoxx process

   return;
}


//  dialog event and completion callback function

int copypixels2::dialog_event(zdialog *zd, cchar *event)
{
   using namespace copypixels2;

   int         dx, dy;
   float       rad, kern, opccent, opcedge;
   
   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()

   if (zd->zstat)
   {
      if (zd->zstat == 1) edit_done(0);                                          //  commit edit
      else edit_cancel(0);                                                       //  discard edit
      free_allblocks();                                                          //  free pixel block memory
      zfree(pixblockmem);
      mmap_data->killsource = 1;                                                 //  make source image process exit
      return 1;
   }

   if (strmatch(event,"focus"))                                                  //  toggle mouse capture
      takeMouse(mousefunc,drawcursor);
   
   if (strmatch(event,"scale"))                                                  //  source image scale change
      zdialog_fetch(zd,"scale",mmap_data->Fscale);

   if (strstr("mrad opccent opcedge",event))                                     //  get new brush attributes
   {
      zdialog_fetch(zd,"mrad",mrad);
      zdialog_fetch(zd,"opccent",opccent);
      zdialog_fetch(zd,"opcedge",opcedge);

      opccent = 0.01 * opccent;                                                  //  scale 0 ... 1
      opcedge = 0.01 * opcedge;
      opccent = pow(opccent,2);                                                  //  change response curve
      opcedge = opccent * opcedge;                                               //  edge relative to center

      for (dy = -mrad; dy <= mrad; dy++)                                         //  rebuild kernel
      for (dx = -mrad; dx <= mrad; dx++)
      {
         rad = sqrt(dx*dx + dy*dy);
         kern = (mrad - rad) / mrad;                                             //  center ... edge  >>  1 ... 0
         kern = kern * (opccent - opcedge) + opcedge;                            //  opacity  center ... edge
         if (rad > mrad) kern = 0;                                               //  beyond mrad, within square
         if (kern < 0) kern = 0;
         if (kern > 1) kern = 1;
         kernel[dx+mrad][dy+mrad] = kern;
      }
   }

   if (strmatch(event,"undlast"))                                                //  undo last edit (click or drag)
      undo_lastblock();

   if (strmatch(event,"undall")) {                                               //  undo all edits
      edit_reset();
      free_allblocks();
   }

   if (strmatch(event,"Fptran"))                                                 //  flag, paint over transparency
      zdialog_fetch(zd,"Fptran",Fptran);

   return 1;
}


//  mouse function

void copypixels2::mousefunc()
{
   using namespace copypixels2;

   static int  pmxdown = 0, pmydown = 0;
   static int  pmpx, pmpy;
   
   mode = 0;

   mpx = Mxposn;
   mpy = Myposn;

   mmap_data->mpx = mpx;                                                         //  inform source image,
   mmap_data->mpy = mpy;                                                         //    mouse position,
   mmap_data->mrad = mrad;                                                       //    mouse radius
   
   if (LMclick)                                                                  //  left mouse click
   {
      Mxdown = Mxclick;                                                          //  click position
      Mydown = Myclick;
      mmap_data->mpxC = Mxdown;                                                  //  inform source image,
      mmap_data->mpyC = Mydown;                                                  //    new mouse drag start
      mmap_data->Fvalid = 1;
      pixblockseq++;                                                             //  new undo seq. no.
      mode = 1;

      if (! mmap_data->Freq) {                                                   //  request pixel block from source
         mmap_data->Freq = 1;
         pmpx = mpx;                                                             //  save mouse position
         pmpy = mpy;
      }
   }

   else if (Mxdrag || Mydrag)                                                    //  drag in progress
   {
      if (Mbutton == 1) mode = 1;                                                //  left drag, paint
      if (Mbutton == 3) mode = 2;                                                //  right drag, erase

      if (Mxdown != pmxdown || Mydown != pmydown) {                              //  new drag
         pmxdown = Mxdown;
         pmydown = Mydown;
         if (mode == 1) pixblockseq++;                                           //  new undo seq. no.
      }

      if (mode == 1 && ! mmap_data->Freq) {                                      //  request pixel block from source
         mmap_data->Freq = 1;
         pmpx = mpx;                                                             //  save mouse position
         pmpy = mpy;
      }
   }

   if (mode == 1 && mmap_data->Freq == 2) {                                      //  new pixel block from source avail.
      mpx = pmpx;                                                                //  synch mouse position
      mpy = pmpy;
      dopixels();                                                                //  paint pixel block on target image
      mmap_data->Freq = 0;                                                       //  ready for next 
   }
   
   if (mode == 2) dopixels();                                                    //  erase target image
   
   draw_mousecircle(0,0,0,1,0);                                                  //  erase mouse circle
   draw_mousecircle(mpx,mpy,mrad,0,0);                                           //  draw mouse circle

   LMclick = RMclick = Mxdrag = Mydrag = 0;
   return;
}


//  paint or erase 1 block of pixels within mouse radius of mpx, mpy

void copypixels2::dopixels()
{
   using namespace copypixels2;

   float       *pix1, *pix2, *pix3, pixF[4];
   int         dx, dy, qx, qy, px, py;
   int         ii, ww, hh, rs, dist = 0;
   int         pot = Fptran;                                                     //  paint over transparent areas
   float       kern, alpha1, alpha2;
   
   ww = E3pxm->ww;
   hh = E3pxm->hh;
   rs = 2 * mrad + 1;                                                            //  pixel block row stride

   save_pixblock();                                                              //  save pixels for poss. undo

   for (dy = -mrad; dy <= mrad; dy++)                                            //  loop surrounding block of pixels
   for (dx = -mrad; dx <= mrad; dx++)
   {
      qx = mpx + dx;
      qy = mpy + dy;

      if (qx < 0 || qx > ww-1) continue;
      if (qy < 0 || qy > hh-1) continue;

      if (sa_stat == 3) {                                                        //  select area active
         ii = qy * ww + qx;
         dist = sa_pixmap[ii];
         if (! dist) continue;                                                   //  pixel is outside area
      }

      kern = kernel[dx+mrad][dy+mrad];                                           //  mouse opacities
      if (kern == 0) continue;                                                   //  outside mouse radius

      if (sa_stat == 3 && dist < sa_blendwidth)                                  //  select area edge blend,
         kern = kern * sa_blendfunc(dist);                                       //    reduce opacity

      pix1 = PXMpix(E1pxm,qx,qy);                                                //  input target image pixel
      ii = (dy + mrad) * rs + (dx + mrad);                                       //  source image pixel >> target
      pix2 = mmap_data->pixels + ii * 4;  
      pix3 = PXMpix(E3pxm,qx,qy);                                                //  output target image pixel

      if (mode == 1)                                                             //  paint
      {
         if (pot) alpha1 = 1;                                                    //  paint over transparent areas
         else alpha1 = 0.00392 * pix1[3];                                        //  input target image opacity
         alpha2 = 0.00392 * pix2[3];                                             //  source image opacity

         if (alpha2 < 0.99) {   
            pixF[0] = pix2[0] * alpha2 + pix1[0] * (1 - alpha2);                 //  construct output target pixel
            pixF[1] = pix2[1] * alpha2 + pix1[1] * (1 - alpha2);                 //   = source image pixel
            pixF[2] = pix2[2] * alpha2 + pix1[2] * (1 - alpha2);                 //   + target image pixel
            pixF[3] = pix2[3] * alpha2 + pix1[3] * (1 - alpha2);
         }
         else {
            pixF[0] = pix2[0];                                                   //  (no transparency case)
            pixF[1] = pix2[1];
            pixF[2] = pix2[2];
            pixF[3] = pix2[3];
         }
         
         if (alpha1 < 0.99) {
            pixF[0] = pixF[0] * alpha1;                                          //  apply target image opacity
            pixF[1] = pixF[1] * alpha1;
            pixF[2] = pixF[2] * alpha1;
            pixF[3] = pixF[3] * alpha1;
         }

         kern = 0.2 * kern;                                                      //  output target image changes
         pix3[0] = kern * pixF[0] + (1 - kern) * pix3[0];                        //    gradually to source image
         pix3[1] = kern * pixF[1] + (1 - kern) * pix3[1];
         pix3[2] = kern * pixF[2] + (1 - kern) * pix3[2];
         pix3[3] = kern * pixF[3] + (1 - kern) * pix3[3];
      }

      if (mode == 2)                                                             //  erase
      {
         kern = 0.4 * kern;
         pix3[0] = kern * pix1[0] + (1.0 - kern) * pix3[0];                      //  gradual erase
         pix3[1] = kern * pix1[1] + (1.0 - kern) * pix3[1];
         pix3[2] = kern * pix1[2] + (1.0 - kern) * pix3[2];
         pix3[3] = kern * pix1[3] + (1.0 - kern) * pix3[3];
      }
   }
   
   if (mode == 1) {
      CEF->Fmods++;
      CEF->Fsaved = 0;
   }

   px = mpx - mrad - 1;                                                          //  repaint modified area
   py = mpy - mrad - 1;
   ww = 2 * mrad + 3;
   Fpaint3(px,py,ww,ww,0);

   return;
}


//  save 1 block of pixels for possible undo

void copypixels2::save_pixblock()
{
   using namespace copypixels2;

   int            cc, npix, dx, dy;
   int            pcc = 4 * sizeof(float);
   float          *pix3;
   pixblockmem_t  *save1B;

   if (totmem > maxmem || totpixblock == maxpixblock)                            //  free memory for oldest updates
      while (totmem > 0.7 * maxmem || totpixblock > 0.7 * maxpixblock)
         free_firstblock();                                     

   npix = 0;
   for (dy = -mrad; dy <= mrad; dy++)                                            //  count pixels in block
   for (dx = -mrad; dx <= mrad; dx++)
   {
      if (mpx + dx < 0 || mpx + dx > E3pxm->ww-1) continue;
      if (mpy + dy < 0 || mpy + dy > E3pxm->hh-1) continue;
      npix++;
   }

   cc = npix * pcc + pixblockmem_cc;
   save1B = (pixblockmem_t *) zmalloc(cc);                                       //  allocate memory for block
   pixblockmem[totpixblock] = save1B;
   totpixblock += 1;
   totmem += cc;

   save1B->seq = pixblockseq;                                                    //  save pixel block poop
   save1B->mpx = mpx;
   save1B->mpy = mpy;
   save1B->mrad = mrad;

   npix = 0;
   for (dy = -mrad; dy <= mrad; dy++)                                            //  save pixels in block
   for (dx = -mrad; dx <= mrad; dx++)
   {
      if (mpx + dx < 0 || mpx + dx > E3pxm->ww-1) continue;
      if (mpy + dy < 0 || mpy + dy > E3pxm->hh-1) continue;
      pix3 = PXMpix(E3pxm,(mpx+dx),(mpy+dy));                                    //  edited image pixel
      memcpy(save1B->pixels[npix],pix3,pcc);
      npix++;
   }

   return;
}


//  undo last pixel block (newest edit) and free memory

void copypixels2::undo_lastblock()
{
   using namespace copypixels2;

   int            ii, cc, npix;
   int            ww, px, py, dx, dy;
   int            pcc = 4 * sizeof(float);
   float          *pix3;
   pixblockmem_t  *save1B;

   for (ii = totpixblock-1; ii >= 0; ii--)
   {
      save1B = pixblockmem[ii];
      if (save1B->seq != pixblockseq) break;
      px = save1B->mpx;
      py = save1B->mpy;
      mrad = save1B->mrad;

      npix = 0;
      for (dy = -mrad; dy <= mrad; dy++)
      for (dx = -mrad; dx <= mrad; dx++)
      {
         if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
         if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
         pix3 = PXMpix(E3pxm,(px+dx),(py+dy));
         memcpy(pix3,save1B->pixels[npix],pcc);
         npix++;
      }

      px = px - mrad - 1;
      py = py - mrad - 1;
      ww = 2 * mrad + 3;
      Fpaint3(px,py,ww,ww,0);

      zfree(save1B);
      pixblockmem[ii] = 0;
      cc = npix * pcc + pixblockmem_cc;
      totmem -= cc;
      totpixblock--;
   }

   if (pixblockseq > 0) --pixblockseq;
   return;
}


//  free memory for first pixel block (oldest edit)

void copypixels2::free_firstblock() 
{
   using namespace copypixels2;

   int            firstseq;
   int            ii, jj, cc, npix;
   int            px, py, dx, dy;
   int            pcc = 4 * sizeof(float);
   pixblockmem_t  *save1B;
   
   if (! totpixblock) return;
   firstseq = pixblockmem[0]->seq;
   
   for (ii = 0; ii < totpixblock; ii++)
   {
      save1B = pixblockmem[ii];
      if (save1B->seq != firstseq) break;
      px = save1B->mpx;
      py = save1B->mpy;
      mrad = save1B->mrad;

      npix = 0;
      for (dy = -mrad; dy <= mrad; dy++)
      for (dx = -mrad; dx <= mrad; dx++)
      {
         if (px + dx < 0 || px + dx > E3pxm->ww-1) continue;
         if (py + dy < 0 || py + dy > E3pxm->hh-1) continue;
         npix++;
      }

      zfree(save1B);
      pixblockmem[ii] = 0;
      cc = npix * pcc + pixblockmem_cc;
      totmem -= cc;
   }
   
   for (jj = 0; ii < totpixblock; jj++, ii++)
      pixblockmem[jj] = pixblockmem[ii];   
   
   totpixblock = jj;
   return;
}


//  free all pixel block memory

void copypixels2::free_allblocks()
{
   using namespace copypixels2;

   int            ii;
   pixblockmem_t  *save1B;

   for (ii = totpixblock-1; ii >= 0; ii--)
   {
      save1B = pixblockmem[ii];
      zfree(save1B);
   }

   pixblockseq = 0;
   totpixblock = 0;
   totmem = 0;

   return;
}


/********************************************************************************/

//  Copy Pixels source image function.
//  Started by m_copypixels2() as a separate session to view the source image.

namespace copypixels3
{
   void  mousefunc();
   void  dopixels();                                                             //  update pixel block
   
   PXM      *pxm_1x = 0, *pxm_scaled = 0;
   int      mpxC, mpyC;                                                          //  center of pixel copy area
   int      mpx, mpy;                                                            //  center of moving mouse circle
   int      mrad;                                                                //  radius of pixel copy area
   float    Fscale = -1;

   typedef struct {                                                              //  shared memory data
      int      killsource;                                                       //  source image process should exit
      int      mpxC, mpyC;                                                       //  center of pixel copy area
      int      mpx, mpy;                                                         //  mouse drag position
      int      mrad;                                                             //  mouse radius
      int      Fvalid;                                                           //  mouse data valid flag
      float    Fscale;                                                           //  source image scale factor
      int      Freq;                                                             //  source/target coord. flag
      float    pixels[402*402*4];                                                //  pixel block, max. mrad = 200
   }  mmap_data_t;
   
   mmap_data_t    *mmap_data;                                                    //  shared memory pointer
}


//  menu function

void m_copypixels3(GtkWidget *, cchar *)                                         //  split source/target processes      18.07
{
   using namespace copypixels3;

   int      err, fd;
   int      pmpx = 0, pmpy = 0, pmrad = 0;
   size_t   cc;

   F1_help_topic = "copy_pixels_2";

   fd = shm_open("/fotoxx_copy_pixels2",O_RDWR+O_CREAT,0600);                    //  identify memory mapped region
   if (fd < 0) {
      zmessageACK(Mwin,"shm_open() failure: %s",strerror(errno));
      return;
   }
   
   cc = sizeof(mmap_data_t);
   err = ftruncate(fd,cc);
   if (err) {
      zmessageACK(Mwin,"ftruncate() failure: %s",strerror(errno));
      return;
   }

   mmap_data = (mmap_data_t *) mmap(0,cc,PROT_WRITE,MAP_SHARED,fd,0);            //  create memory mapped region
   if (mmap_data == (void *) -1) {
      zmessageACK(Mwin,"mmap() failure: %s",strerror(errno));
      return;
   }
   
   char     *Pcurr_file = 0;
   int      Frefresh = 1;
   int      ww, hh;
   int      mpx2, mpy2, mrad2;

   mpxC = mpyC = -1;                                                             //  no pixel copy area selected
   mrad = -1;
   
   while (true)
   {
      zsleep(0.001);
      zmainloop(10);
      
      if (! curr_file) continue;
      if (mmap_data->killsource) break;                                          //  target image process is done

      if (! Pcurr_file || ! strmatch(curr_file,Pcurr_file)) {                    //  detect file change
         if (Pcurr_file) zfree(Pcurr_file);
         Pcurr_file = zstrdup(curr_file);
         Frefresh = 1;
      }
      
      if (CEF) {
         Frefresh = 1;
         continue;
      }
      
      if (mmap_data->Fscale != Fscale) Frefresh = 1;
      
      if (Frefresh)
      {
         if (pxm_1x) PXM_free(pxm_1x);                                           //  refresh edited image
         if (E0pxm) pxm_1x = PXM_copy(E0pxm);
         else pxm_1x = PXM_load(curr_file,1);                                    //  load new image
         if (! pxm_1x) break;
         Fscale = mmap_data->Fscale;
         ww = pxm_1x->ww * Fscale;
         hh = pxm_1x->hh * Fscale;
         if (pxm_scaled) PXM_free(pxm_scaled);
         pxm_scaled = PXM_rescale(pxm_1x,ww,hh);
         PXM_addalpha(pxm_scaled);
         mpxC = mpyC = -1;                                                       //  no copy area selected
         takeMouse(mousefunc,dragcursor);                                        //  connect mouse function
         Frefresh = 0;
      }
      
      if (mpxC < 0) continue;                                                    //  no source pixel copy area
      if (! mmap_data->Fvalid) continue;                                         //  no valid target mouse data

      mpx = mpxC + mmap_data->mpx - mmap_data->mpxC;                             //  convert target copy area
      mpy = mpyC + mmap_data->mpy - mmap_data->mpyC;                             //    to source copy area
      mrad = mmap_data->mrad;

      if (mpx != pmpx || mpy != pmpy || mrad != pmrad) {                         //  mouse circle changed
         pmpx = mpx;
         pmpy = mpy;
         pmrad = mrad;
         mpx2 = mpx / Fscale;
         mpy2 = mpy / Fscale;
         mrad2 = mrad / Fscale;
         gdk_window_freeze_updates(gdkwin);
         draw_mousecircle(0,0,0,1,0);                                            //  erase mouse circle
         draw_mousecircle(mpx2,mpy2,mrad2,0,0);                                  //  draw mouse circle
         gdk_window_thaw_updates(gdkwin);
         zmainloop();
      }

      if (mmap_data->Freq == 1) {                                                //  pixels wanted from target image
         dopixels();                                                             //  copy pixel block to shared memory
         mmap_data->Freq = 2;                                                    //  pixel block available
      }
   }

   if (CEF) edit_cancel(0);   
   printz("m_copypixels3() exit \n");
   m_quit(0,0);
}


//  mouse function

void copypixels3::mousefunc()
{
   using namespace copypixels3;
   
   if (LMclick) {                                                                //  left mouse click
      mpxC = Mxclick * Fscale;                                                   //  new source pixel copy area
      mpyC = Myclick * Fscale;
      LMclick = 0;
   }

   return;
}


//  copy block of pixels within mouse circle to shared memory

void copypixels3::dopixels()
{
   using namespace copypixels3;

   float       *pix0, *pix1;
   float       pixnull[4] = { 0, 0, 0, 0 };                                      //  black, alpha = 0
   int         dx, dy, qx, qy;
   int         ii, ww, hh, rs;
   int         pcc = 4 * sizeof(float);

   ww = pxm_scaled->ww;
   hh = pxm_scaled->hh;
   
   rs = 2 * mrad + 1;                                                            //  pixel block row stride
   
   for (dy = -mrad; dy <= mrad; dy++)                                            //  loop surrounding block of pixels
   for (dx = -mrad; dx <= mrad; dx++)
   {
      qx = mpx + dx;
      qy = mpy + dy;

      if (qx < 0 || qx > ww-1 || qy < 0 || qy > hh-1) pix0 = pixnull;            //  pixel outside source image
      else pix0 = PXMpix(pxm_scaled,qx,qy);                                      //  source image pixel
      ii = (dy + mrad) * rs + (dx + mrad);
      pix1 = mmap_data->pixels + ii * 4;                                         //  to shared memory
      memcpy(pix1,pix0,pcc);
   }

   return;
}


/********************************************************************************/

//  Paint transparency function - paint pixel alpha channel with the mouse.

namespace paint_transp_names
{
   int      mode = 1;                                                            //  1/2 = more/less transparency
   int      Fgrad = 1;                                                           //  gradual or instant transparency
   int      Mradius;                                                             //  mouse radius
   float    kernel[400][400];                                                    //  radius <= 199
   int      E3ww, E3hh;

   editfunc    EFpaintransp;
}


//  menu function

void m_paint_transp(GtkWidget *, cchar *)
{
   using namespace paint_transp_names;

   int   paint_transp_dialog_event(zdialog* zd, cchar *event);
   void  paint_transp_mousefunc();

   cchar    *mess1 = E2X("left drag: add transparency \n"
                         "right drag: add opacity");

   F1_help_topic = "paint_transp";

   EFpaintransp.menufunc = m_paint_transp;
   EFpaintransp.funcname = "paint_transp";
   EFpaintransp.Farea = 2;                                                       //  select area OK
   EFpaintransp.mousefunc = paint_transp_mousefunc;                                        //  mouse function
   if (! edit_setup(EFpaintransp)) return;                                       //  setup edit
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

   PXM_addalpha(E1pxm);                                                          //  add an alpha channel if req.
   PXM_addalpha(E3pxm);
   Fpaintnow();

   /***
             __________________________________
            |       Paint transparency         |
            |                                  |
            |  left drag: add transparency     |
            |  right drag: add opacity         |
            |                                  |
            |  paintbrush radius  [____]       |
            |  strength center    [____]       |
            |  strength edge      [____]       |
            |  [x] gradual paint               |
            |                                  |
            |                 [done] [cancel]  |
            |__________________________________|

   ***/

   zdialog *zd = zdialog_new(E2X("Paint Transparency"),Mwin,Bdone,Bcancel,null);
   EFpaintransp.zd = zd;

   zdialog_add_widget(zd,"hbox","hbr","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labm","dialog",mess1,"space=5");
   zdialog_add_widget(zd,"hbox","hbbri","dialog",0,"space=5");
   zdialog_add_widget(zd,"vbox","vbbr1","hbbri",0,"homog|space=5");
   zdialog_add_widget(zd,"vbox","vbbr2","hbbri",0,"homog|space=5");
   zdialog_add_widget(zd,"label","labbr","vbbr1",E2X("paintbrush radius"));
   zdialog_add_widget(zd,"label","labsc","vbbr1",E2X("strength center"));
   zdialog_add_widget(zd,"label","labse","vbbr1",E2X("strength edge"));
   zdialog_add_widget(zd,"zspin","radius","vbbr2","1|199|1|30");
   zdialog_add_widget(zd,"zspin","stcent","vbbr2","0|100|1|95");
   zdialog_add_widget(zd,"zspin","stedge","vbbr2","0|100|1|100");
   zdialog_add_widget(zd,"hbox","hb4","dialog",0,"space=3");
   zdialog_add_widget(zd,"check","Fgrad","hb4",E2X("gradual paint"),"space=5");
   
   zdialog_stuff(zd,"Fgrad",1);

   zdialog_restore_inputs(zd);                                                   //  preload prior user inputs

   zdialog_run(zd,paint_transp_dialog_event,"save");                             //  run dialog, parallel
   zdialog_send_event(zd,"radius");                                              //  get kernel initialized

   zdialog_fetch(zd,"Fgrad",Fgrad);                                              //  instant/gradual paint
   mode = 1;                                                                     //  start with paint mode

   takeMouse(paint_transp_mousefunc,drawcursor);                                 //  connect mouse function
   return;
}


//  dialog event and completion callback function

int paint_transp_dialog_event(zdialog *zd, cchar *event)
{
   using namespace paint_transp_names;

   void  paint_transp_mousefunc();

   int         radius, dx, dy;
   float       rad, kern, stcent, stedge;

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()

   if (zd->zstat)
   {
      if (zd->zstat == 1) edit_done(0);                                          //  commit edit
      else edit_cancel(0);                                                       //  discard edit
      return 1;
   }

   if (strmatch(event,"focus"))                                                  //  toggle mouse capture
      takeMouse(paint_transp_mousefunc,drawcursor);

   if (strstr("radius stcent stedge",event))                                     //  get new brush attributes
   {
      zdialog_fetch(zd,"radius",Mradius);                                        //  mouse radius
      zdialog_fetch(zd,"stcent",stcent);                                         //  center transparency
      zdialog_fetch(zd,"stedge",stedge);                                         //  edge transparency

      stcent = 0.01 * stcent;                                                    //  scale 0 ... 1
      stedge = 0.01 * stedge;

      radius = Mradius;

      for (dy = -radius; dy <= radius; dy++)                                     //  build kernel
      for (dx = -radius; dx <= radius; dx++)
      {
         rad = sqrt(dx*dx + dy*dy);
         kern = (radius - rad) / radius;                                         //  center ... edge  >>  1 ... 0
         kern = kern * (stcent - stedge) + stedge;                               //  strength  center ... edge
         if (kern < 0) kern = 0;
         if (kern > 1) kern = 1;
         if (rad > radius) kern = 2;                                             //  beyond radius, within square
         kernel[dx+radius][dy+radius] = kern;
      }
   }

   if (strmatch(event,"Fgrad"))                                                  //  flag, gradual overpaints
      zdialog_fetch(zd,"Fgrad",Fgrad);
   
   return 1;
}


//  pixel paint mouse function

void paint_transp_mousefunc()
{
   using namespace paint_transp_names;

   void  paint_transp_dopixels(int px, int py);

   static int  pmxdown = 0, pmydown = 0;
   int         px, py;

   if (LMclick || RMclick)
   {
      if (LMclick) mode = 1;                                                     //  left click, paint
      if (RMclick) mode = 2;                                                     //  right click, erase

      px = Mxclick;
      py = Myclick;
      paint_transp_dopixels(px,py);                                              //  do 1 block of pixels
   }

   else if (Mxdrag || Mydrag)                                                    //  drag in progress
   {
      if (Mbutton == 1) mode = 1;                                                //  left drag, paint
      if (Mbutton == 3) mode = 2;                                                //  right drag, erase

      px = Mxdrag;
      py = Mydrag;

      if (Mxdown != pmxdown || Mydown != pmydown) {                              //  new drag
         pmxdown = Mxdown;
         pmydown = Mydown;
      }

      paint_transp_dopixels(px,py);                                              //  do 1 block of pixels

      LMclick = RMclick = Mxdrag = Mydrag = 0;
      return;
   }

   LMclick = RMclick = Mxdrag = Mydrag = 0;
   draw_mousecircle(Mxposn,Myposn,Mradius,0,0);                                  //  draw mouse circle

   return;
}


//  paint or erase 1 block of pixels within mouse radius of px, py

void paint_transp_dopixels(int px, int py)
{
   using namespace paint_transp_names;

   float       *pix3;
   int         radius, dx, dy, qx, qy;
   int         ii, rr, dist = 0;
   float       kern;

   cairo_t *cr = draw_context_create(gdkwin,draw_context);

   radius = Mradius;

   for (dy = -radius; dy <= radius; dy++)                                        //  loop surrounding block of pixels
   for (dx = -radius; dx <= radius; dx++)
   {
      qx = px + dx;
      qy = py + dy;

      if (qx < 0 || qx > E3ww-1) continue;
      if (qy < 0 || qy > E3hh-1) continue;

      if (sa_stat == 3) {                                                        //  select area active
         ii = qy * E3ww + qx;
         dist = sa_pixmap[ii];
         if (! dist) continue;                                                   //  pixel is outside area
      }

      kern = kernel[dx+radius][dy+radius];                                       //  mouse transparencies
      if (kern > 1) continue;                                                    //  outside mouse radius

      if (sa_stat == 3 && dist < sa_blendwidth)                                  //  within blend distance,
         kern = kern * sa_blendfunc(dist);                                       //    kern = kern ... 0 at edge
         
      pix3 = PXMpix(E3pxm,qx,qy);                                                //  edited image pixel

      if (mode == 1) {                                                           //  add transparency
         if (Fgrad) {
            pix3[3] -= 2 * kern;                                                 //  accumulate transparency
            if (pix3[3] < 0) pix3[3] = 0;
         }
         else pix3[3] = 0;                                                       //  instant
      }

      if (mode == 2) {                                                           //  add opacity
         if (Fgrad) { 
            pix3[3] += 2 * kern;                                                 //  accumulate 
            if (pix3[3] > 255.0) pix3[3] = 255.0;
         }
         else pix3[3] = 255.0;                                                   //  instant
      }
   }

   CEF->Fmods++;
   CEF->Fsaved = 0;

   px = px - radius - 1;                                                         //  repaint modified area
   py = py - radius - 1;
   rr = 2 * radius + 3;
   Fpaint3(px,py,rr,rr,cr);

   draw_mousecircle(Mxposn,Myposn,Mradius,0,cr);                                 //  draw mouse circle

   draw_context_destroy(draw_context);
   return;
}


/********************************************************************************/

//  Correct chromatic abberation (1st order only) by stretching
//  or shrinking a selected RGB color plane.

namespace color_fringes_names
{
   editfunc    EFcolorfringes;
   float       colorfringesRed, colorfringesBlue;
   int         colorfringesFarea;
   int         E3ww, E3hh;
}


//  menu function

void m_color_fringes(GtkWidget *, cchar *)
{
   using namespace color_fringes_names;

   int    colorfringes_dialog_event(zdialog *zd, cchar *event);
   void * colorfringes_thread(void *);

   cchar  *colorfringes_message = E2X(" Adjust each RGB color to minimize \n"
                                      " color fringes at the image extremes. ");

   F1_help_topic = "color_fringes";

   EFcolorfringes.menufunc = m_color_fringes;
   EFcolorfringes.funcname = "color_fringes";
   EFcolorfringes.Farea = 2;                                                     //  select area usable
   EFcolorfringes.threadfunc = colorfringes_thread;
   if (! edit_setup(EFcolorfringes)) return;                                     //  setup edit
   
   E3ww = E3pxm->ww;
   E3hh = E3pxm->hh;

/***
          ______________________________________
         |        Chromatic Abberation          |
         |                                      |
         | Adjust each RGB color to minimize    |
         | color fringes at the image extremes. |
         |                                      |
         | Red   =======[]============  [-2.1]  |
         | Blue  ============[]=======  [+2.3]  |
         |                                      |
         |                      [done] [cancel] |
         |______________________________________|
           
***/

   zdialog *zd = zdialog_new(E2X("Color Fringes"),Mwin,Bdone,Bcancel,null);
   EFcolorfringes.zd = zd;

   zdialog_add_widget(zd,"label","lab1","dialog",colorfringes_message,"space=3");
   zdialog_add_widget(zd,"hbox","hb1","dialog",0,"space=5");
   zdialog_add_widget(zd,"vbox","vb1","hb1",0,"space=5|homog");
   zdialog_add_widget(zd,"vbox","vb2","hb1",0,"space=5|homog|expand");
   zdialog_add_widget(zd,"vbox","vb3","hb1",0,"space=5|homog");
   zdialog_add_widget(zd,"label","labred","vb1",Bred);
   zdialog_add_widget(zd,"label","labblue","vb1",Bblue);
   zdialog_add_widget(zd,"hscale","red","vb2","-5.0|5.0|0.1|0.0");
   zdialog_add_widget(zd,"hscale","blue","vb2","-5.0|5.0|0.1|0.10");
   zdialog_add_widget(zd,"label","redval","vb3"," 0.0");
   zdialog_add_widget(zd,"label","blueval","vb3"," 0.0");

   colorfringesRed = colorfringesBlue = 0;
   colorfringesFarea = 0;

   zdialog_run(zd,colorfringes_dialog_event,"save");                             //  run dialog, parallel
   return;
}


//  dialog event and completion callback function

int colorfringes_dialog_event(zdialog *zd, cchar *event)
{
   using namespace color_fringes_names;

   char  text[8];

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()
   
   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  done
         if (colorfringesFarea && sa_stat != 3)                                  //  extend to whole image if
            signal_thread();                                                     //    former select area now deleted
         edit_done(0);                                                           //  commit edit
      }
      else edit_cancel(0);                                                       //  discard edit
      return 1;
   }

   if (strmatch(event,"red")) {
      zdialog_fetch(zd,"red",colorfringesRed);
      snprintf(text,8,"%+.1f",colorfringesRed);
      zdialog_stuff(zd,"redval",text);
      signal_thread();
      return 1;
   }

   if (strmatch(event,"blue")) {
      zdialog_fetch(zd,"blue",colorfringesBlue);
      snprintf(text,8,"%+.1f",colorfringesBlue);
      zdialog_stuff(zd,"blueval",text);
      signal_thread();
      return 1;
   }

   return 1;
}


//  colorfringes image and accumulate colorfringes memory

void * colorfringes_thread(void *)
{
   using namespace color_fringes_names;

   void  * colorfringes_wthread(void *);                                         //  worker thread process

   while (true)
   {
      thread_idle_loop();                                                        //  wait for work or exit request

      while (Fpaintrequest) zmainsleep(0.01);
      paintlock(1);                                                              //  block window paint                 19.0

      do_wthreads(colorfringes_wthread,NWT);                                     //  worker threads

      CEF->Fmods++;
      CEF->Fsaved = 0;

      paintlock(0);                                                              //  unblock window paint               19.0
      Fpaint2();                                                                 //  update window
   }

   return 0;                                                                     //  not executed, stop g++ warning
}


void * colorfringes_wthread(void *arg)                                           //  worker thread
{
   using namespace color_fringes_names;

   int         index = *((int *) (arg));
   int         px3, py3, ii;
   int         xlo, xhi, ylo, yhi;
   float       px1, py1;
   float       vpix1[4], *pix3;
   float       cx, cy, fx, fy;

   cx = E3ww / 2.0;
   cy = E3hh / 2.0;

   if (sa_stat == 3) {
      colorfringesFarea = 1;                                                     //  remember if area active
      xlo = sa_minx + index;                                                     //  set area limits
      xhi = sa_maxx;
      ylo = sa_miny + index;
      yhi = sa_maxy;
   }

   else {
      colorfringesFarea = 0;
      xlo = index;                                                               //  set whole image limits
      xhi = E3ww;
      ylo = index;
      yhi = E3hh;
   }

   for (py3 = ylo; py3 < yhi; py3 += NWT)                                        //  loop all output pixels
   for (px3 = xlo; px3 < xhi; px3++)
   {
      if (colorfringesFarea) {                                                   //  area active
         ii = py3 * E3ww + px3;
         if (! sa_pixmap[ii]) continue;                                          //  pixel not in area
      }

      pix3 = PXMpix(E3pxm,px3,py3);                                              //  output pixel

      fx = (px3 - cx) / cx;                                                      //  -1 to 0 to +1
      fy = (py3 - cy) / cy;

      px1 = px3 + fx * colorfringesRed;                                          //  red shift
      py1 = py3 + fy * colorfringesRed;
      vpixel(E1pxm,px1,py1,vpix1);
      pix3[0] = vpix1[0];

      px1 = px3 + fx * colorfringesBlue;                                         //  blue shift
      py1 = py3 + fy * colorfringesBlue;
      vpixel(E1pxm,px1,py1,vpix1);
      pix3[2] = vpix1[2];
   }

   pthread_exit(0);
}


/********************************************************************************/

//  anti-alias menu function

editfunc    EFantialias;                                                         //  edit function data


//  menu function

void m_anti_alias(GtkWidget *, const char *)
{
   int    antialias_dialog_event(zdialog* zd, const char *event);

   zdialog     *zd;

   F1_help_topic = "anti_alias";

   EFantialias.menufunc = m_anti_alias;
   EFantialias.funcname = "anti_alias";                                          //  function name
   EFantialias.Farea = 2;                                                        //  select area usable
   if (! edit_setup(EFantialias)) return;                                        //  setup edit

   zd = zdialog_new("Anti-Alias",Mwin,Bapply,Bcancel,null);                      //  setup dialog
   EFantialias.zd = zd;

   zdialog_resize(zd,200,0);
   zdialog_run(zd,antialias_dialog_event,"save");                                //  run dialog - parallel

   return;
}


//  antialias dialog event and completion function

int antialias_dialog_event(zdialog *zd, const char *event)                       //  antialias dialog event function
{
   void  antialias_func();

   if (strmatch(event,"done")) zd->zstat = 1;                                    //  from edit_setup() or f_save()
   if (strmatch(event,"cancel")) zd->zstat = 2;                                  //  from f_open()
   
   if (zd->zstat)
   {
      if (zd->zstat == 1) {                                                      //  done
         antialias_func();                                                       //  apply
         edit_done(0);                                                           //  commit edit
      }
      else edit_cancel(0);                                                       //  discard edit
      return 1;
   }

   return 1;
}


//  compare two pixels
//  1.0 = perfect match, 0.0 = perfect mismatch (black/white)

inline float antialias_match(float *pix1, float *pix2)
{
   return PIXMATCH(pix1,pix2);                                                   //  0..1 = zero..perfect match
}


//  antialias function

/***
      Algorithm is like scale2x but adapted for photos
      instead of computer generated pixel art.
       _________________
      |     |     |     |
      |  A  |  B  |  C  |
      |_____|_____|_____|         _____________
      |     |     |     |        |      |      |
      |  D  |  E  |  F  |        |  E0  |  E1  |
      |_____|_____|_____|        |______|______|
      |     |     |     |        |      |      |
      |  G  |  H  |  I  |        |  E2  |  E3  |
      |_____|_____|_____|        |______|______|


      1. make output image = 2x input image dimensions
      2. loop for each pixel E in input image:
      3.    for corresponding pixels E0 E1 E2 E3 in output image:
      4.       if D:B match more than E:D and E:B
                  E0 = 0.333 * (D + B + E)
               else E0 = E
      5.       same for E1, E2, E3
      6. add some annealing and blur

***/

void antialias_func()
{
   int      ww1, hh1, ww2, hh2;
   int      px, py, px2, py2, qx, qy;
   int      ii, dist, rgb;
   float    *pixB, *pixD, *pixE, *pixF, *pixH;
   float    *pixE0, *pixE1, *pixE2, *pixE3;
   float    matchDB, matchBF, matchDH, matchHF;
   float    matchEB, matchED, matchEF, matchEH;
   float    red, green, blue, *pixel;
   float    blurf1, blurf2;

   ww1 = E1pxm->ww;
   hh1 = E1pxm->hh;

   ww2 = 2 * ww1;                                                                //  create 2x output image
   hh2 = 2 * hh1;
   E9pxm = PXM_rescale(E1pxm,ww2,hh2);

//  calculate each output pixel group E0 E1 E2 E3
//  from input pixel E and its 8 neighbors

   for (py = 1; py < hh1-1; py++)                                                //  loop all (inside) input pixels
   for (px = 1; px < ww1-1; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py * ww1 + px;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      pixB = PXMpix(E1pxm,px  ,py-1);
      pixD = PXMpix(E1pxm,px-1,py);
      pixE = PXMpix(E1pxm,px,  py);
      pixF = PXMpix(E1pxm,px+1,py);
      pixH = PXMpix(E1pxm,px  ,py+1);

      px2 = px * 2;                                                              //  2x2 pixel block in output image
      py2 = py * 2;                                                              //    corresponding to (px,py)

      pixE0 = PXMpix(E9pxm,px2,py2);
      pixE1 = PXMpix(E9pxm,px2+1,py2);
      pixE2 = PXMpix(E9pxm,px2,py2+1);
      pixE3 = PXMpix(E9pxm,px2+1,py2+1);

      matchDB = antialias_match(pixD,pixB);
      matchBF = antialias_match(pixB,pixF);
      matchDH = antialias_match(pixD,pixH);
      matchHF = antialias_match(pixH,pixF);
      matchEB = antialias_match(pixE,pixB);
      matchED = antialias_match(pixE,pixD);
      matchEF = antialias_match(pixE,pixF);
      matchEH = antialias_match(pixE,pixH);

      for (rgb = 0; rgb < 3; rgb++)                                              //  fill 2x2 pixel block
      {
         if (matchDB > matchED && matchDB > matchEB)
            pixE0[rgb] = 0.25 * (pixD[rgb] + pixB[rgb]) + 0.5 * pixE[rgb];
         else pixE0[rgb] = pixE[rgb];

         if (matchBF > matchEB && matchBF > matchEF)
            pixE1[rgb] = 0.25 * (pixB[rgb] + pixF[rgb]) + 0.5 * pixE[rgb];
         else pixE1[rgb] = pixE[rgb];

         if (matchDH > matchED && matchDH > matchEH)
            pixE2[rgb] = 0.25 * (pixD[rgb] + pixH[rgb]) + 0.5 * pixE[rgb];
         else pixE2[rgb] = pixE[rgb];

         if (matchHF > matchEH && matchHF > matchEF)
            pixE3[rgb] = 0.25 * (pixH[rgb] + pixF[rgb]) + 0.5 * pixE[rgb];
         else pixE3[rgb] = pixE[rgb];
      }
   }

//  apply a little bit of blur to each output pixel

   blurf1 = 0.6;                                                                 //  contribution of original pixel
   blurf2 = (1.0 - blurf1) / 9.0;                                                //  contribution of surrounding pixels

   for (py = 1; py < hh2-1; py++)
   for (px = 1; px < ww2-1; px++)
   {
      if (sa_stat == 3) {                                                        //  select area active
         ii = py/2 * ww1 + px/2;
         dist = sa_pixmap[ii];                                                   //  distance from edge
         if (! dist) continue;                                                   //  outside pixel
      }

      red = green = blue = 0;

      for (qy = py-1; qy <= py+1; qy++)
      for (qx = px-1; qx <= px+1; qx++)
      {
         pixel = PXMpix(E9pxm,qx,qy);
         red += pixel[0];
         green += pixel[1];
         blue += pixel[2];
      }

      pixel = PXMpix(E9pxm,px,py);
      pixel[0] = blurf1 * pixel[0] + blurf2 * red;
      pixel[1] = blurf1 * pixel[1] + blurf2 * green;
      pixel[2] = blurf1 * pixel[2] + blurf2 * blue;
   }

   PXM_free(E3pxm);                                                              //  E3 = E9 resized 1/2
   E3pxm = PXM_rescale(E9pxm,ww1,hh1);
   E9pxm = 0;

   if (sa_stat) sa_clear();                                                      //  area is no longer valid

   CEF->Fmods++;                                                                 //  image modified
   CEF->Fsaved = 0;

   Fpaint2();                                                                    //  update window
   return;
}


/********************************************************************************/

//  Plugin menu functions

namespace plugins_names
{
   #define maxplugins 100
   int         Nplugins;                                                         //  plugin menu items
   char        *plugins[100];
   GtkWidget   *popup_plugmenu = 0;
   editfunc    EFplugin;
}


//  edit plugins menu or choose and run a plugin function

void m_plugins(GtkWidget *, cchar *)
{
   using namespace plugins_names;

   void  m_edit_plugins(GtkWidget *, cchar *);
   void  m_run_plugin(GtkWidget *, cchar *);

   char        plugfile[200], buff[200], *pp;
   FILE        *fid;
   STATB       stbuff;
   int         ii, err;

   F1_help_topic = "plugins";

   if (popup_plugmenu) {
      popup_menu(Mwin,popup_plugmenu);                                           //  popup the plugins menu
      return;
   }

   snprintf(plugfile,200,"%s/plugins",get_zhomedir());                           //  plugins file

   err = stat(plugfile,&stbuff);                                                 //  exists?
   if (err)
   {
      fid = fopen(plugfile,"w");                                                 //  no, create default
      fprintf(fid,"Gimp = gimp %%s \n");
      fprintf(fid,"auto-gamma = mogrify -auto-gamma %%s \n");
      fprintf(fid,"whiteboard cleanup = mogrify "                                //  ImageMagick white board cleanup
                  "-morphology Convolve DoG:15,100,0 "
                  "-negate -normalize -blur 0x1 -channel RBG "
                  "-level 60%%,91%%,0.1 %%s \n");
      fclose(fid);
   }

   fid = fopen(plugfile,"r");                                                    //  open plugins file
   if (! fid) {
      zmessageACK(Mwin,"plugins file: %s",strerror(errno));
      return;
   }

   for (ii = 0; ii < 99; ii++)                                                   //  read list of plugins
   {
      pp = fgets_trim(buff,200,fid,1);
      if (! pp) break;
      plugins[ii] = zstrdup(buff);
   }

   fclose(fid);
   Nplugins = ii;

   popup_plugmenu = create_popmenu();                                            //  create popup menu for plugins

   add_popmenu_item(popup_plugmenu, E2X("Edit Plugins"),                         //  1st entry is Edit Plugins
                     m_edit_plugins, 0, E2X("Edit plugins menu"));

   for (ii = 0; ii < Nplugins; ii++)                                             //  add the plugin menu functions
   {
      char *pp = strstr(plugins[ii]," = ");
      if (! pp) continue;
      *pp = 0;
      add_popmenu_item(popup_plugmenu, plugins[ii], m_run_plugin, 0,
                                 E2X("Run as Fotoxx edit function"));
      *pp = ' ';
   }

   popup_menu(Mwin,popup_plugmenu);                                              //  popup the menu
   return;
}


//  edit plugins menu

void m_edit_plugins(GtkWidget *, cchar *)                                        //  overhauled 
{
   using namespace plugins_names;

   int   edit_plugins_event(zdialog *zd, cchar *event);

   int         ii;
   char        *pp;
   zdialog     *zd;

   F1_help_topic = "plugins";

   if (FGWM != 'F' && FGWM != 'G') return;                                       //  19.0

/***
       ___________________________________
      |         Edit Plugins              |
      |                                   |
      |  menu name [_____________|v]      |     e.g. edit with gimp
      |  command   [___________________]  |     e.g. gimp %s
      |                                   |
      |             [Add] [Remove] [Done] |
      |___________________________________|

***/

   zd = zdialog_new(E2X("Edit Plugins"),Mwin,Badd,Bremove,Bdone,null);
   zdialog_add_widget(zd,"hbox","hbm","dialog",0,"space=3");
   zdialog_add_widget(zd,"label","labm","hbm",E2X("menu name"),"space=5");
   zdialog_add_widget(zd,"comboE","menuname","hbm",0,"space=5");
   zdialog_add_widget(zd,"hbox","hbc","dialog",0,"space=5");
   zdialog_add_widget(zd,"label","labc","hbc",E2X("command"),"space=5");
   zdialog_add_widget(zd,"zentry","command","hbc",0,"space=5|expand");

   for (ii = 0; ii < Nplugins; ii++)                                             //  stuff combo box with available menus
   {
      pp = strstr(plugins[ii]," = ");                                            //  menu name = command line
      if (! pp) continue;
      *pp = 0;
      zdialog_cb_app(zd,"menuname",plugins[ii]);
      *pp = ' ';
   }

   zdialog_set_modal(zd);
   zdialog_run(zd,edit_plugins_event,"mouse");
   return;
}


//  dialog event function

int edit_plugins_event(zdialog *zd, cchar *event)
{
   using namespace plugins_names;

   int      ii, jj, cc, zstat;
   char     *pp, menuname[100], command[200];
   char     buff[200];
   FILE     *fid;

   if (strmatch(event,"menuname"))
   {
      zdialog_fetch(zd,"menuname",menuname,100);

      for (ii = 0; ii < Nplugins; ii++)                                          //  find selected menu name
      {
         pp = strstr(plugins[ii]," = ");
         if (! pp) continue;
         *pp = 0;
         jj = strmatch(menuname,plugins[ii]);
         *pp = ' ';
         if (jj) {
            zdialog_stuff(zd,"command",pp+3);                                    //  stuff corresp. command in dialog
            break;
         }
      }

      return 1;
   }

   zstat = zd->zstat;                                                            //  wait for dialog completion
   if (! zstat) return 1;

   if (zstat < 1 || zstat > 3) {                                                 //  cancel
      zdialog_free(zd);
      return 1;
   }

   if (zstat == 1)                                                               //  add plugin or replace same menu name
   {
      zd->zstat = 0;                                                             //  keep dialog active

      if (Nplugins == maxplugins) {
         zmessageACK(Mwin,"too many plugins");
         return 1;
      }

      zdialog_fetch(zd,"menuname",menuname,100);
      zdialog_fetch(zd,"command",command,200);

      pp = strstr(command," %s");
      if (! pp) zmessageACK(Mwin,"Warning: command without \"%%s\" ");

      for (ii = 0; ii < Nplugins; ii++)                                          //  find existing plugin record
      {
         pp = strstr(plugins[ii]," = ");
         if (! pp) continue;
         *pp = 0;
         jj = strmatch(menuname,plugins[ii]);
         *pp = ' ';
         if (jj) break;
      }

      if (ii == Nplugins) {                                                      //  new plugin record
         plugins[ii] = 0;
         Nplugins++;
         zdialog_cb_app(zd,"menuname",menuname);
         pp = zdialog_cb_get(zd,"menuname",ii);
      }

      if (plugins[ii]) zfree(plugins[ii]);
      cc = strlen(menuname) + strlen(command) + 4;
      plugins[ii] = (char *) zmalloc(cc);
      *plugins[ii] = 0;
      strncatv(plugins[ii],cc,menuname," = ",command,0);
      return 1;                                                                  //  19.0
   }

   if (zstat == 2)                                                               //  remove current plugin
   {
      zd->zstat = 0;                                                             //  keep dialog active

      zdialog_fetch(zd,"menuname",menuname,100);

      for (ii = 0; ii < Nplugins; ii++)                                          //  find existing plugin record
      {
         pp = strstr(plugins[ii]," = ");
         if (! pp) continue;
         *pp = 0;
         jj = strmatch(menuname,plugins[ii]);
         *pp = ' ';
         if (jj) break;
      }

      if (ii == Nplugins) return 1;                                              //  not found

      zfree(plugins[ii]);                                                        //  remove plugin record
      Nplugins--;
      for (jj = ii; jj < Nplugins; jj++)
         plugins[jj] = plugins[jj+1];
      zdialog_cb_delete(zd,"menuname",menuname);                                 //  delete entry from combo box
      zdialog_stuff(zd,"menuname","");
      zdialog_stuff(zd,"command","");
      return 1;                                                                  //  19.0
   }

   if (zstat == 3)                                                               //  done
   {
      snprintf(buff,199,"%s/plugins",get_zhomedir());                            //  open file for plugins
      fid = fopen(buff,"w");
      if (! fid) {
         zmessageACK(Mwin,strerror(errno));
         return 1;
      }

      for (int ii = 0; ii < Nplugins; ii++)                                      //  save plugins
         if (plugins[ii])
            fprintf(fid,"%s \n",plugins[ii]);

      fclose(fid);

      zdialog_free(zd);
      popup_plugmenu = 0;                                                        //  rebuild popup menu
      return 1;                                                                  //  19.0
   }

   return 1;
}


//  process plugin menu selection
//  execute correspinding command using current image file

void m_run_plugin(GtkWidget *, cchar *menu)
{
   using namespace plugins_names;

   int         ii, jj, err;
   char        *pp = 0, plugincommand[200], pluginfile[100];
   PXM         *pxmtemp;
   zdialog     *zd = 0;

   F1_help_topic = "plugins";

   for (ii = 0; ii < Nplugins; ii++)                                             //  search plugins for menu name
   {
      pp = strchr(plugins[ii],'=');                                              //  match menu name to plugin command
      if (! pp) continue;                                                        //  menu name = ...
      *pp = 0;
      jj = strmatch(plugins[ii],menu);
      *pp = '=';
      if (jj) break;
   }

   if (ii == Nplugins) {
      zmessageACK(Mwin,"plugin menu not found %s",menu);
      return;
   }

   strncpy0(plugincommand,pp+1,200);                                             //  corresp. command
   strTrim2(plugincommand);

   pp = strstr(plugincommand,"%s");                                              //  no file placeholder in command
   if (! pp) {
      err = shell_ack(plugincommand);                                            //  execute non-edit plugin command
      goto RETURN;
   }
   
   EFplugin.menufunc = m_run_plugin;
   EFplugin.funcname = menu;
   if (! edit_setup(EFplugin)) return;                                           //  start edit function

   snprintf(pluginfile,100,"%s/plugfile.tif",temp_folder);                       //  .../tempfiles-nnnnnn/plugfile.tif

   err = PXM_save(E1pxm,pluginfile,8,100,1);                                     //  E1 >> plugin_file
   if (err) goto FAIL;

   zd = zmessage_post(Mwin,"20/20",0,E2X("Plugin working ..."));

   repl_1str(plugincommand,command,"%s",pluginfile);                             //  command filename

   err = shell_ack(command);                                                     //  execute plugin command
   if (err) goto FAIL;
   
   pxmtemp = TIFF_PXM_load(pluginfile);                                          //  read command output file
   if (! pxmtemp) {
      zmessageACK(Mwin,E2X("plugin failed"));
      goto FAIL;
   }

   PXM_free(E3pxm);                                                              //  plugin_file >> E3
   E3pxm = pxmtemp;

   CEF->Fmods++;                                                                 //  assume image was modified
   CEF->Fsaved = 0;
   edit_done(0);
   goto RETURN;

FAIL:
   edit_cancel(0);

RETURN:
   if (zd) zdialog_free(zd);
   return;
}


