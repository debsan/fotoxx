/********************************************************************************

   Fotoxx      edit photos and manage collections

   Copyright 2007-2019 Michael Cornelison
   source code URL: https://kornelix.net
   contact: kornelix@posteo.de

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version. See https://www.gnu.org/licenses

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
   See the GNU General Public License for more details.

*********************************************************************************/

#include <wait.h>
#include <tiffio.h>
#include <png.h>
#include "jpeglib.h"
#include "libraw/libraw.h"
#include <lcms2.h>
#include <champlain/champlain.h>
#include <champlain-gtk/champlain-gtk.h>
#include <clutter-gtk/clutter-gtk.h>

#include "zfuncs.h"

//  Fotoxx definitions

#define Frelease  "fotoxx-19.16"                                                 //  Fotoxx release version
#define Flicense  "Free software - GNU General Public License v.3"
#define Fhomepage "https://kornelix.net"
#define Fcontact  "kornelix@posteo.de"
#define Ftranslators "Translators: \n"       \
         "    Doriano Blengino, André Campos Rodovalho, \n"   \
         "    J. A. Miralles Puignau, Xavier Ribes, Erich Küster "
#define MEGA (1024 * 1024)                                                       //  1 million as 2**20
#define FMEGA (1.0 * MEGA) 
#define PI 3.141592654
#define RAD (180.0/PI)

#define PXMpix(PXM,px,py) (PXM->pixels+(py)*(PXM->rs)+(px)*(PXM->nc))            //  PXM and PXB pixel (RGB/RGBA nc = 3/4)
#define PXBpix(PXB,px,py) (PXB->pixels+(py)*(PXB->rs)+(px)*(PXB->nc))            //    e.g. red = PXMpix(...)[0] 
#define pixbright(pix) (0.25*(pix)[0]+0.65*(pix)[1]+0.10*(pix)[2])               //  overall brightness 0-255

//  color match function for integer/float RGB colors 0-255
//  returns 0.0 to 1.0 = perfect match
#define RGBMATCH(r1,g1,b1,r2,g2,b2)                      \
      0.0000000596 * (256.0 - fabsf(float(r1-r2)))       \
                   * (256.0 - fabsf(float(g1-g2)))       \
                   * (256.0 - fabsf(float(b1-b2)))
#define PIXMATCH(pix1,pix2)            \
      (RGBMATCH(pix1[0],pix1[1],pix1[2],pix2[0],pix2[1],pix2[2]))

//  compile time limits that could be increased

#define thumbfilesize 512                                                        //  thumbnail file pixel size 
#define wwhh_limit1 30000                                                        //  max. image width or height            18.01
#define wwhh_limit2 (512*MEGA)                                                   //  max. image width x height (8 GB)      19.0
#define max_threads 8                                                            //  max. threads (hyperthreads useless)   18.01
#define maxtopfolders 200                                                        //  max. no. of top image folders
#define indexrecl 2000                                                           //  max. image index rec. (>XFCC >tagFcc)
#define maximages 1000000                                                        //  max. image files supported
#define maxgallery 25000                                                         //  max. gallery files supported          18.07
#define maxtagcats 200                                                           //  max tag categories
#define tagcc  50                                                                //  max cc for one tag or category ID
#define tagFcc 1000                                                              //  max tag cc for one image file
#define maxtags 100000                                                           //  max tags and tags/category            18.01
#define tagGcc 200000                                                            //  max tag cc for one category
#define tagMcc 1000                                                              //  max tag cc for batch add tags
#define tagScc 500                                                               //  max tag cc for search tags
#define tagRcc 300                                                               //  max tag cc for recent tags
#define GSmax 100000                                                             //  max files for gallery_select()
#define Mxmeta 20                                                                //  max indexed metadata keys             18.01

//  EXIF/IPTC keys for embedded image metadata

#define iptc_keywords_key "Keywords"                                             //  keywords (tags)
#define iptc_rating_key "Rating"                                                 //  star rating
#define exif_ww_key "ImageWidth"                                                 //  image width and height                19.15
#define exif_hh_key "ImageHeight"                                                //  (replace exif_wwhh_key)
#define exif_date_key "DateTimeOriginal"                                         //  photo date/time
#define exif_orientation_key "Orientation"                                       //  orientation
#define exif_rollangle_key "RollAngle"                                           //  roll angle - minor level error
#define exif_editlog_key "ImageHistory"                                          //  edit history log
#define exif_comment_key "Comment"                                               //  image comment
#define exif_usercomment_key "UserComment"                                       //  image comment
#define iptc_caption_key "Caption-Abstract"                                      //  image caption
#define exif_copyright_key "Copyright"                                           //  image copyright
#define exif_focal_length_35_key "FocalLengthIn35mmFormat"                       //  focal length, 35mm equivalent         19.16
#define exif_focal_length_key "FocalLength"                                      //  focal length, real                    19.16
#define exif_city_key "City"                                                     //  city/location name (geotags)
#define exif_country_key "Country"                                               //  country name
#define exif_lati_key "GPSLatitude"                                              //  latitude in degrees (-180 to +180)
#define exif_longi_key "GPSLongitude"                                            //  longitude in degrees (-180 to +180)
#define exif_colorprof1_key "ICCProfileName"                                     //  ICC color profile name, e.g. "sRGB"
#define exif_colorprof2_key "ICC_Profile"                                        //  embedded color profile data
#define exif_maxcc 2000                                                          //  max. cc for exif/iptc text

//  GTK/GDK shortcuts

#define TEXTWIN GTK_TEXT_WINDOW_TEXT
#define NODITHER GDK_RGB_DITHER_NONE
#define ALWAYS GTK_POLICY_ALWAYS
#define NEVER GTK_POLICY_NEVER
#define AUTO GTK_POLICY_AUTOMATIC
#define GDKRGB GDK_COLORSPACE_RGB
#define LINEATTRIBUTES GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER
#define BILINEAR GDK_INTERP_BILINEAR
#define NEAREST GDK_INTERP_NEAREST
#define SCROLLWIN GTK_SCROLLED_WINDOW
#define VERTICAL GTK_ORIENTATION_VERTICAL
#define HORIZONTAL GTK_ORIENTATION_HORIZONTAL
#define MWIN GTK_WINDOW(Mwin)

//  externals from zfuncs module

namespace zfuncs {
   extern GdkDisplay        *display;                                            //  X11 workstation (KB, mouse, screen)
   extern GdkDeviceManager  *manager;                                            //  knows screen / mouse associations
   extern GdkScreen         *screen;                                             //  monitor (screen)
   extern GdkDevice         *mouse;                                              //  pointer device
   extern GtkSettings       *settings;                                           //  screen settings
   extern cchar      *build_date_time;                                           //  build date and time                   19.0
   extern char       *appimagexe;                                                //  appimage executable path name         19.0
   extern char       *progexe;                                                   //  executable path name                  19.0
   extern timeb      startime;                                                   //  fotoxx startup time 
   extern int        monitor_ww, monitor_hh;                                     //  monitor pixel dimensions
   extern int        appfontsize;                                                //  app font size
   extern char       zappname[20];                                               //  app name/version
   extern char       zlocale[8];                                                 //  current language lc_RC
   extern char       zicondir[200];                                              //  where application icons live
   extern char       zimagedir[200];                                             //  where application image files live    18.01
   extern int        Nmalloc, Nstrdup, Nfree;                                    //  malloc() strdup() free() counters
   extern int        vmenuclickposn;                                             //  vert. menu icon click position, 0-100
   extern int        vmenuclickbutton;                                           //   "" button: 1/3 = left/right mouse
   extern int        zdialog_count;                                              //  zdialog count (new - free)
   extern int        zdialog_busy;                                               //  open zdialogs (run - destroy)
   extern zdialog    *zdialog_list[zdialog_max];                                 //  active zdialog list                   18.01
   extern pthread_t  tid_main;                                                   //  main() thread ID
}

enum FTYPE { FNF, FDIR, IMAGE, RAW, VIDEO, THUMB, OTHER };                       //  file types, FNF = file not found
enum GTYPE { TNONE, GDIR, SEARCH, META, RECENT, NEWEST, ALBUM };                 //  gallery types
enum GSORT { SNONE, FNAME, FDATE, PDATE, FSIZE, PSIZE };                         //  gallery sort types                    19.0
enum GSEQ  { QNONE, ASCEND, DESCEND };                                           //  gallery sort sequence

struct GFlist_t  {                                                                 //  current gallery file list in memory
   char     *file;                                                               //  /folder.../filename
   char     fdate[16];                                                           //  file date: yyyymmddhhmmss
   char     pdate[16];                                                           //  photo date: yyyymmddhhmmss
   int      fsize;                                                               //  file size, bytes                      19.0
   int      psize;                                                               //  image size, pixels                    19.0
   int      mdindex;                                                             //  index to metadata list Gmdlist[*]
};

//  externals from f.gallery

namespace navi {                                                                 //  gallery() data
   extern GFlist_t *GFlist;                                                      //  gallery file data table
   extern char     **Gmdlist;                                                    //  corresp. metadata list
   extern int      Gmdrows;                                                      //  text rows in metadata list
   extern int      Nfiles;                                                       //  gallery file count (images + subdirs)
   extern int      Nfolders;                                                     //  gallery subfolder count
   extern int      Nimages;                                                      //  gallery image file count
   extern char     *galleryname;                                                 //  folder path or gallery type name
   extern GTYPE    gallerytype;                                                  //  gallery type: folder, recent, etc.
   extern GSORT    gallerysort;                                                  //  filename/file-date/photo-date
   extern GSEQ     galleryseq;                                                   //  ascending/descending
   extern int      gallerypainted;                                               //  gallery paint is complete
   extern int      galleryposn;                                                  //  gallery target scroll position
   extern int      scrollposn;                                                   //  current scroll position
   extern int      xwinW, xwinH;                                                 //  image gallery window size
   extern int      thumbsize;                                                    //  curr. thumbnail display size
   extern int      genthumbs;                                                    //  counts thumbnails generated
   extern double   thumbcache_MB;                                                //  thumbnail cache size, MB

   int    gallery_paint(GtkWidget *, cairo_t *);                                 //  gallery window paint function
   void   menufuncx(GtkWidget *, cchar *menu);                                   //  gallery menu buttons function
   void   changefolder(GtkWidget *widget, GdkEventButton *event);                //  gallery folder change function
   void   newtop(GtkWidget *widget, GdkEventButton *event);                      //  gallery change top folder function
   void   newalbum(GtkWidget *widget, GdkEventButton *event);                    //  gallery change album function
   int    mouse_event(GtkWidget *, GdkEvent *, void *);                          //  gallery window mouse event function
   void   gallery_sort();                                                        //  gallery choose sort order and sort
   char * gallery_dragfile();                                                    //  gallery window file drag function
   void   gallery_dropfile(int mx, int my, char *file);                          //  gallery window file drop function
   int    KBaction(cchar *action);                                               //  gallery window KB key function
}

EX int      GScount;                                                             //  gallery_select(), file count
EX char     *GSfiles[GSmax];                                                     //  gallery_select(), selected files

//  GTK etc. parameters

EX GtkWidget      *Mwin, *MWhbox, *MWvbox, *MWmenu;                              //  main window containers
EX GtkWidget      *Fhbox, *Fvbox, *Fpanel, *Fpanlab, *Fdrawin;                   //  F window widgets, file view
EX GtkWidget      *Ghbox, *Gvbox, *Gsep, *Gpanel, *Gtop, *Galbum;                //  G window widgets, gallery view
EX GtkWidget      *Gscroll, *Gdrawin;
EX GtkAdjustment  *Gadjust;
EX GtkWidget      *Mhbox, *Mvbox;                                                //  M window widgets, net map view
EX GtkWidget      *Whbox, *Wvbox, *Wdrawin;                                      //  W window widgets, world map view
EX GtkWidget      *Cdrawin;                                                      //  curr. drawing window, Fdrawin/Wdrawin
EX GdkWindow      *gdkwin;                                                       //  corresp. GDK window
EX PIXBUF         *BGpixbuf;                                                     //  window background pixbuf

EX GdkCursor      *arrowcursor;                                                  //  main window cursors
EX GdkCursor      *dragcursor;
EX GdkCursor      *drawcursor;
EX GdkCursor      *blankcursor;

EX int      BLACK[3], WHITE[3], RED[3], GREEN[3], BLUE[3];                       //  RGB values 0-255
EX int      LINE_COLOR[3];                                                       //  line drawing color, one of the above

EX draw_context_t  draw_context;                                                 //  GDK window drawing context

//  user settings

EX char     *locale;                                                             //  language and region setting           18.07
EX char     *menu_style;                                                         //  menu style: icons/text/both
EX int      FBrgb[3];                                                            //  F view background color, RGB 0-255
EX int      GBrgb[3];                                                            //  G view background color, RGB 0-255
EX int      MFrgb[3];                                                            //  menu font color, RGB 0-255            18.01
EX int      MBrgb[3];                                                            //  menu background color, RGB 0-255      18.01
EX int      iconsize;                                                            //  menu icon size
EX char     *dialog_font;                                                        //  dialog font e.g. "sans 10"
EX char     *startalbum;                                                         //  start album: file name
EX char     *startdisplay;                                                       //  start display: recent/prev/blank/file/folder
EX char     *startfolder;                                                        //  start folder (startdisplay=folder)
EX char     *startfile;                                                          //  start image file (startdisplay=file)
EX int      Fdragopt;                                                            //  1/2 = 1x drag / magnified scroll
EX int      Fshowhidden;                                                         //  show hidden folders in gallery view
EX int      Flastversion;                                                        //  prev/next button gets last versions only
EX int      Fshiftright;                                                         //  shift image to right margin
EX int      zoomcount;                                                           //  zoom count to reach 2x (1-8)
EX float    zoomratio;                                                           //  corresp. zoom ratio: 2**(1/zoomcount)
EX int      map_dotsize;                                                         //  map dot size / mouse capture size
EX int      jpeg_def_quality;                                                    //  default jpeg save quality
EX char     *RAWfiletypes;                                                       //  recognized RAW files: .raw .rw2 ...
EX char     *myRAWtypes;                                                         //  RAW types encountered
EX char     *VIDEOfiletypes;                                                     //  recognized video files: .mov .mp4 ...
EX char     *myVIDEOtypes;                                                       //  VIDEO types encountered

#define CCC (XFCC*2+200)                                                         //  max. command line size
EX char        command[CCC];                                                     //  (command, parameters, 2 filespecs)

//  menu name and function table

typedef void menufunc_t(GtkWidget *,cchar *);                                    //  menu function type

struct menutab_t {
   GtkWidget    *topmenu;                                                        //  parent menu
   cchar        *menu;                                                           //  menu text
   cchar        *icon;                                                           //  menu icon
   cchar        *desc;                                                           //  menu short description (tooltip)
   menufunc_t   *func;                                                           //  menu function (GtkWidget *, cchar *)
   cchar        *arg;                                                            //  argument
};

#define maxmenus 250
EX menutab_t   menutab[maxmenus];
EX int         Nmenus;

//  KB shortcut tables                                                           //  18.07

struct kbsftab_t {                                                               //  eligible menus for KB shortcut
   cchar       *menu;                                                            //  menu name
   menufunc_t  *func;                                                            //  function
   cchar       *arg;                                                             //  argument
};

#define maxkbsf 100
EX kbsftab_t   kbsftab[maxkbsf];
EX int         Nkbsf;

struct kbsutab_t {                                                               //  user-defined shortcuts
   char        *key;                                                             //  key name, e.g. "Shift+U"
   char        *menu;                                                            //  menu name, e.g. "Redo"
};

#define maxkbsu 50
EX kbsutab_t   kbsutab[maxkbsu];
EX int         Nkbsu;

//  general parameters

EX cchar       *command_params;                                                  //  command line parameters               19.0
EX char        desktopname[100];                                                 //  locale specific desktop folder        18.07
EX char        *Prelease;                                                        //  prior fotoxx version
EX int         Ffirsttime;                                                       //  first time startup
EX int         mwgeom[4];                                                        //  main window position and size
EX pid_t       fotoxxPID;                                                        //  my process PID                        19.0
EX int         NWT;                                                              //  working threads to use
EX int         Nval[100];                                                        //  static integer values 0-99
EX char        FGWM;                                                             //  curr. view mode: 'F' 'G' 'W' 'M'
EX char        PFGWM;                                                            //  prior view mode 
EX int         Frawtherapee;                                                     //  flag, Raw Therapee available
EX int         Fgrowisofs;                                                       //  flag, growisofs available
EX int         PTtools;                                                          //  flag, pano tools available
EX int         Fvideo;                                                           //  flag, ffmpeg available
EX int         Faddr2line;                                                       //  flag, addr2line available
EX int         Fshutdown;                                                        //  app shutdown underway
EX int         Fdebug;                                                           //  debug flag
EX int         Findexlev;                                                        //  0/1/2 = none/old/old+new image files
EX int         FMindexlev;                                                       //  Findexlev if start via file manager
EX int         Pindexlev;                                                        //  Findexlev if command parameter
EX char        *xmeta_keys[Mxmeta];                                              //  indexed metadata names (compressed)   18.01
EX int         Fkillfunc;                                                        //  flag, running function should quit
EX int         Fpaintlock;                                                       //  flag, window updates locked           19.0
EX int         Fpaintrequest;                                                    //  window paint request pending          19.0
EX int         Fmetamod;                                                         //  image metadata unsaved changes
EX int         Fblock;                                                           //  edit functions mutex flag
EX int         Ffuncbusy;                                                        //  function is busy/working              19.0
EX int         Fthreadbusy;                                                      //  thread is busy/working                19.0
EX int         Ffullscreen;                                                      //  flag, window is fullscreen
EX int         Fpanelshow;                                                       //  show or hide F view top panel
EX int         Fblowup;                                                          //  zoom small images to window size
EX int         Fmashup;                                                          //  flag, mashup function is active
EX int         Fslideshow;                                                       //  flag, slide show is active
EX int         Fview360;                                                         //  flag, view360 function is active      18.01
EX int         Fescape;                                                          //  KB escape was pressed                 19.0
EX char        *ss_KBkeys;                                                       //  slide show KB keys, packed            18.01
EX int         Frecent;                                                          //  start with recent files gallery
EX int         Fnew;                                                             //  start with newly added files gallery
EX int         Fprev;                                                            //  start with previous file
EX int         Fblank;                                                           //  start with blank window
EX char        *topmenu;                                                         //  latest top-menu selection
EX char        *commandmenu;                                                     //  command line, startup menu function
EX char        *commandalbum;                                                    //  command line, startup album gallery
EX cchar       *F1_help_topic;                                                   //  current function help topic
EX int         Fcaptions;                                                        //  show image captions/comments
EX char        *netmap_source;                                                   //  net map source
EX char        *mapbox_access_key;                                               //  mapbox map source access key
EX char        *topfolders[maxtopfolders];                                       //  user top-level image folders
EX int         Ntopfolders;                                                      //  topfolder[] count
EX char        *thumbfolder;                                                     //  thumbnails folder
EX char        *initial_file;                                                    //  initial file on command line
EX char        *curr_file, *curr_folder;                                         //  current image file and folder
EX char        curr_file_type[8];                                                //  jpg / tif / png / other
EX int         curr_file_bpc;                                                    //  image file bits/color, 8/16
EX int         curr_file_size;                                                   //  image file size on disk
EX int         curr_file_posn;                                                   //  position in current gallery, 0-last
EX int         last_file_posn;                                                   //  remember when curr. file deleted
EX int         curr_file_count;                                                  //  count of images in current set
EX char        *curr_album;                                                      //  current or last used album            18.07
EX char        *clicked_file;                                                    //  image file / thumbnail clicked
EX int         clicked_posn;                                                     //  clicked gallery position (Nth)
EX int         clicked_width;                                                    //  clicked thumbnail position
EX int         clicked_height;                                                   //    (normalized 0-100)
EX char        *imagefiletypes;                                                  //  supported image file types, .jpg .png etc.
EX char        *last_curr_file;                                                  //  curr_file in last session
EX char        *last_galleryname;                                                //  galleryname in last session
EX GTYPE       last_gallerytype;                                                 //  gallerytype in last session
EX char        *colormapfile;                                                    //  curr. printer color map file
EX char        *copymove_loc;                                                    //  copy/move folder last used
EX char        *color_chooser_file;                                              //  user's color chooser image file

EX int         Fscriptbuild;                                                     //  edit script build is in-progress      18.07
EX char        scriptfile[200];                                                  //  current script file name
EX FILE        *script_fid;                                                      //  current script file FID

EX char        f_load_type[8];                                                   //  data set by PXB/PXM_load()            19.0
EX int         f_load_bpc;
EX int         f_load_size;
EX char        *f_save_file;                                                     //  data set by PXB/PXM_save()
EX char        f_save_type[8];
EX int         f_save_bpc;
EX uint        f_save_size;

EX VOL double  Fbusy_goal, Fbusy_done;                                           //  BUSY message progress tracking
EX char        paneltext[200];                                                   //  top panel application text

//  files and folders in /home/<user>/.fotoxx/

EX char        index_folder[200];                                                //  image index folder
EX char        index_file[200];                                                  //  image index file                      19.0
EX char        temp_folder[200];                                                 //  temp. files folder                    18.07
EX char        tags_defined_file[200];                                           //  tags defined file
EX char        recentfiles_file[200];                                            //  file of recent image files
EX char        albums_folder[200];                                               //  folder for saved image albums
EX char        gallerymem_file[200];                                             //  file for recent galleries memory 
EX char        saved_areas_folder[200];                                          //  folder for saved select area files
EX char        saved_curves_folder[200];                                         //  folder for saved curve data
EX char        retouch_folder[200];                                              //  folder for saved retouch settings     18.07
EX char        custom_kernel_folder[200];                                        //  folder for saved custom kernel data
EX char        addtext_folder[200];                                              //  folder for m_add_text files
EX char        addline_folder[200];                                              //  folder for m_add_lines files
EX char        favorites_folder[200];                                            //  folder for favorites menu
EX char        mashup_folder[200];                                               //  folder for mashup projects
EX char        KBshortcuts[200];                                                 //  keyboard shortcuts file
EX char        slideshow_folder[200];                                            //  folder for slide show files
EX char        slideshow_trans_folder[200];                                      //  folder for slide show transition files
EX char        pattern_folder[200];                                              //  folder for pattern files
EX char        printer_color_folder[200];                                        //  folder for printer calibration
EX char        scripts_folder[200];                                              //  folder for script files
EX char        searchresults_file[200];                                          //  file for search results
EX char        maps_folder[200];                                                 //  folder for fotoxx_maps files
EX char        user_maps_folder[200];                                            //  folder for user-added map files
EX char        montage_maps_folder[200];                                         //  folder for montage map files
EX char        palettes_folder[200];                                             //  folder for color palett files         19.0

//  fotoxx PXM and PXB pixmaps

struct PXM {                                                                     //  PXM pixmap, 3 x float per pixel
   char        wmi[8];                                                           //  self-identifier
   int         ww, hh, nc, rs;                                                   //  width, height, channels, row stride
   float       *pixels;                                                          //  ww * hh * nc * sizeof(float)
};

struct PXB {                                                                     //  PXB pixmap, 3 x uint8/pixel
   char        wmi[8];                                                           //  self-identifier
   int         ww, hh, nc, rs;                                                   //  width, height, channels, row stride
   uint8       *pixels;                                                          //  hh * rs bytes (in pixbuf)
   PIXBUF      *pixbuf;                                                          //  parallel GDK pixbuf sharing pixels
};

//  parameters for F/W image window painting (functions Fpaint() etc.)

struct FWstate {
   PXB         *fpxb;                                                            //  image PXB, size = 1x
   PXB         *mpxb;                                                            //  image PXB, size = Mscale
   int         morgx, morgy;                                                     //  Dpxb origin in Mpxb image
   int         dorgx, dorgy;                                                     //  Dpxb origin in drawing window
   float       fzoom;                                                            //  image zoom scale (0 = fit window)
   float       mscale;                                                           //  scale factor, 1x image to window image
   float       pscale;                                                           //  prior scale factor
};

EX FWstate    Fstate, Wstate;                                                    //  parameters for F and W windows
EX FWstate    *Cstate;                                                           //  current parameters, F or W

#define Fpxb   Fstate.fpxb                                                       //  F window parameter equivalents
#define Mpxb   Fstate.mpxb
#define Morgx  Fstate.morgx
#define Morgy  Fstate.morgy
#define Dorgx  Fstate.dorgx
#define Dorgy  Fstate.dorgy
#define Fzoom  Fstate.fzoom
#define Mscale Fstate.mscale
#define Pscale Fstate.pscale

EX PXM         *E0pxm;                                                           //  edit pixmap, original image
EX PXM         *E1pxm;                                                           //  edit pixmap, base image for editing
EX PXM         *E3pxm;                                                           //  edit pixmap, edited image
EX PXM         *ERpxm;                                                           //  edit pixmap, undo/redo image
EX PXM         *E8pxm;                                                           //  scratch image for some functions
EX PXM         *E9pxm;                                                           //  scratch image for some functions

EX int         Dww, Dhh;                                                         //  main/drawing window size
EX int         dww, dhh;                                                         //  Dpxb size in drawing window, <= Dww, Dhh
EX int         zoomx, zoomy;                                                     //  req. zoom center of window

EX int         Mbutton;                                                          //  mouse button, 1/3 = left/right
EX int         Mwxposn, Mwyposn;                                                 //  mouse position, window space
EX int         Mxposn, Myposn;                                                   //  mouse position, image space
EX int         LMclick, RMclick;                                                 //  mouse left, right click
EX int         Mxclick, Myclick;                                                 //  mouse click position, image space
EX int         Mdrag;                                                            //  mouse drag underway
EX int         mouse_dragtime;                                                   //  current drag duration, milliseconds
EX double      wacom_pressure;                                                   //  Wacom tablet, stylus pressure 0.0 to 1.0
EX int         Mwdragx, Mwdragy;                                                 //  drag increment, window space
EX int         Mxdown, Mydown, Mxdrag, Mydrag;                                   //  mouse drag vector, image space
EX int         Fmousemain;                                                       //  mouse acts on main window not dialog
EX int         Mcapture;                                                         //  mouse captured by edit function

EX int         KBcapture;                                                        //  KB key captured by edit function
EX int         KBkey;                                                            //  active keyboard key
EX int         KBcontrolkey;                                                     //  keyboard key states, 1 = down
EX int         KBshiftkey;
EX int         KBaltkey;

//  lines, text and circles drawn over window image whenever repainted

struct topline_t {
   int      x1, y1, x2, y2;                                                      //  endpoint coordinates in image space
   int      type;                                                                //  1/2 = solid/dotted
};

#define maxtoplines 8                                                            //  max. top lines
EX topline_t   toplines[8], ptoplines[8];                                        //  top lines, prior top lines
EX int         Ntoplines, Nptoplines;                                            //  current counts

struct toptext_t {
   int      ID, px, py;
   cchar    *text;
   cchar    *font;
};

#define maxtoptext 100                                                           //  max. text strings
EX toptext_t   toptext[100];                                                     //  text strings
EX int         Ntoptext;                                                         //  current count

struct topcircle_t {
   int      px, py;
   int      radius;
};

#define maxtopcircles 100                                                        //  max. circles
EX topcircle_t    topcircles[100];                                               //  circles
EX int            Ntopcircles;                                                   //  current count

//  spline curve data

typedef void spcfunc_t(int spc);                                                 //  callback function, spline curve edit

EX float    splcurve_minx;                                                       //  min. anchor point dist, % scale

struct spldat {                                                                  //  spline curve data
   GtkWidget   *drawarea;                                                        //  drawing area for spline curves
   spcfunc_t   *spcfunc;                                                         //  callback function when curve changed
   int         Nspc;                                                             //  number of curves, 1-10
   int         fact[10];                                                         //  curve is active 
   int         vert[10];                                                         //  curve is vert. (1) or horz. (0)
   int         nap[10];                                                          //  anchor points per curve
   float       apx[10][50], apy[10][50];                                         //  up to 50 anchor points per curve
   float       yval[10][1000];                                                   //  y-values for x = 0 to 1 by 0.001
   int         Nscale;                                                           //  no. of fixed scale lines, 0-10
   float       xscale[2][10];                                                    //  2 x-values for end points
   float       yscale[2][10];                                                    //  2 y-values for end points
};

//  select area data

#define sa_initseq 10                  //  initial sequence number (0/1/2 reserved)
#define sa_maxseq 9999                 //  ultimate limit 64K
#define mode_rect       1              //  select rectangle by drag/click
#define mode_ellipse    2              //  select ellipse by drag
#define mode_draw       3              //  freehand draw by drag/click
#define mode_follow     4              //  follow edge indicated by clicks
#define mode_replace    5              //  adjust edge by dragging mouse
#define mode_mouse      6              //  select area within mouse (radius)
#define mode_onecolor   7              //  select one matching color within mouse
#define mode_allcolors  8              //  select all matching colors within mouse
#define mode_image      9              //  select whole image

EX VOL int     sa_stat;                                                          //  0/1/2/3/4 = none/edit/xx/fini/disab
EX int         sa_mode;                                                          //  1-7 = curr. select area edit method
EX uint16      sa_endpx[10000], sa_endpy[10000];                                 //  last pixel drawn per seqence no.
EX int         sa_thresh;                                                        //  mouse pixel distance threshold
EX int         sa_mouseradius;                                                   //  mouse selection radius
EX int         sa_searchrange;                                                   //  search range (* mouse radius)
EX int         sa_mousex, sa_mousey;                                             //  mouse position in image
EX int         sa_lastx, sa_lasty;                                               //  last selected mouse position          18.07
EX float       sa_colormatch;                                                    //  color range to match (0.001 to 1.0)
EX int         sa_calced;                                                        //  edge calculation done
EX int         sa_blendwidth;                                                    //  edge blend width
EX int         sa_minx, sa_maxx;                                                 //  enclosing rectangle for area
EX int         sa_miny, sa_maxy;
EX char        *sa_stackdirec;                                                   //  pixel search stack
EX int         *sa_stackii;
EX int         sa_maxstack;
EX int         sa_Nstack;
EX int         sa_currseq;                                                       //  current select sequence no.
EX int         sa_Ncurrseq;                                                      //  current sequence pixel count
EX char        *sa_pixselc;                                                      //  maps pixels selected in current cycle
EX uint16      *sa_pixmap;                                                       //  0/1/2+ = outside/edge/inside edge dist
EX uint8       *sa_pixmap2;                                                      //  inside pixel via sa_finish_auto()     19.0
EX uint        sa_Npixel;                                                        //  total select area pixel count
EX uint8       *sa_hairy_alpha;                                                  //  m_select_hairy() transparency poop
EX int         sa_fww, sa_fhh;                                                   //  valid image dimensions for select area
EX int         Fshowarea;                                                        //  show select area outline
EX int         areanumber;                                                       //  increasing sequential number

//  grid lines parameters, for each grid lines set [G]

#define GON    0                                                                 //  gridsettings[G][0]  grid lines on/off
#define GX     1                                                                 //              [G][1]  x-lines on/off
#define GY     2                                                                 //              [G][2]  y-lines on/off
#define GXS    3                                                                 //              [G][3]  x-lines spacing
#define GYS    4                                                                 //              [G][4]  y-lines spacing
#define GXC    5                                                                 //              [G][5]  x-lines count
#define GYC    6                                                                 //              [G][6]  y-lines count
#define GXF    7                                                                 //              [G][7]  x-lines offset
#define GYF    8                                                                 //              [G][8]  y-lines offset
#define G9     9                                                                 //              [G][9]  unused
EX int         currgrid;                                                         //  current grid params set, 0-5
EX int         gridsettings[6][10];                                              //  settings for 6 sets of grid lines

//  Image index record.
//  pdate, wwhh, tags, capt, comms, gtags may have "null" (char) as a missing value.

struct xxrec_t {
   char        *file;                                                            //  image filespec
   char        fdate[16];                                                        //  file date, yyyymmddhhmmss
   char        pdate[16];                                                        //  image (photo) date, yyyymmddhhmmss
   char        rating[4];                                                        //  IPTC rating, "0" to "5" stars
   int         ww, hh;                                                           //  image width x height, pixels          19.0
   int         fsize;                                                            //  image file size, bytes                19.0
   char        *tags;                                                            //  IPTC tags
   char        *capt;                                                            //  IPTC caption
   char        *comms;                                                           //  EXIF comments
   char        *location;                                                        //  city, park, monument ...
   char        *country;                                                         //  country
   float       flati, flongi;                                                    //  earth coordinates
   char        *xmeta;                                                           //  indexed metadata                      18.01
};

EX xxrec_t     **xxrec_tab;                                                      //  image index table, file sequence
EX int         Nxxrec;                                                           //  count, < maximages
EX int         Findexvalid;                                                      //  0/1/2 = no / yes:old / yes:old+new

//  image edit function data

struct editfunc {                                                                //  edit function data
   cchar       *menuname;                                                        //  menu name in menutab[*]
   cchar       *funcname;                                                        //  function name, e.g. flatten (< 32 chars)
   int         FprevReq;                                                         //  request to use smaller image if poss.
   int         Farea;                                                            //  area: 0/1/2 = delete/ignore/useable
   int         Fmods;                                                            //  flag, image modifications by this func
   int         Fpreview;                                                         //  flag, using smaller image for edits
   int         Frestart;                                                         //  flag, OK to restart with new file
   int         FusePL;                                                           //  flag, OK to use with paint/lever edits
   int         Fscript;                                                          //  flag, function can be scripted
   int         Fsaved;                                                           //  current mods are saved to disk
   zdialog     *zd;                                                              //  edit dialog
   spldat      *curves;                                                          //  edit curve data
   void        (*menufunc)(GtkWidget *, cchar *);                                //  edit menu function in menutab[*]
   void *      (*threadfunc)(void *);                                            //  edit thread function
   void        (*mousefunc)();                                                   //  edit mouse function
   int         thread_command, thread_status;
   int         thread_pend, thread_done, thread_hiwater;
};

EX editfunc    *CEF;                                                             //  current active edit function

//  undo/redo stack data

#define maxedits 100                                                             //  undo/redo stack size
EX char        URS_filename[100];                                                //  stack image filename template
EX int         URS_pos;                                                          //  stack position, 0-99
EX int         URS_max;                                                          //  stack max. position, 0-99
EX char        URS_funcs[100][32];                                               //  corresp. edit func. names
EX int         URS_saved[100];                                                   //  corresp. edit saved to disk or not
EX int         URS_reopen_pos;                                                   //  reopen last saved file, stack position

struct textattr_t {                                                              //  attributes for gentext() function
   char     text[1000];                                                          //  text to generate image from
   char     font[80];                                                            //  font name
   int      size;                                                                //  font size
   int      tww, thh;                                                            //  generated image size, unrotated
   float    angle;                                                               //  text angle, degrees
   float    sinT, cosT;                                                          //  trig funcs for text angle
   char     color[4][20];                                                        //  text, backing, outline, shadow "R|G|B"
   int      transp[4];                                                           //  corresponding transparencies 0-255
   int      towidth;                                                             //  outline width, pixels
   int      shwidth;                                                             //  shadow width
   int      shangle;                                                             //  shadow angle -180...+180
   PXB      *pxb_text;                                                           //  image with text/outline/shadow
};

struct lineattr_t {                                                              //  attributes for genline() function
   int      length, width;                                                       //  line length and width, pixels
   int      larrow, rarrow;                                                      //  left/right arrow head size (0 = no arrow)
   int      lww, lhh;                                                            //  generated image size, unrotated
   float    angle;                                                               //  line angle, degrees
   float    sinT, cosT;                                                          //  trig funcs for line angle
   char     color[4][20];                                                        //  line, background, outline, shadow "R|G|B"
   int      transp[4];                                                           //  corresponding transparencies 0-255
   int      towidth;                                                             //  outline width, pixels
   int      shwidth;                                                             //  shadow width
   int      shangle;                                                             //  shadow angle -180...+180
   PXB      *pxb_line;                                                           //  image with line/outline/shadow 
};

//  dialogs with global visibility

EX zdialog     *zd_brightgraph;                                                  //  brightness distribution zdialog
EX zdialog     *zd_darkbrite;                                                    //  highlight dark/bright pixels zdialog
EX zdialog     *zd_editmeta;                                                     //  edit metadata zdialog
EX zdialog     *zd_editanymeta;                                                  //  edit any metadata zdialog
EX zdialog     *zd_deletemeta;                                                   //  delete metadata zdialog
EX zdialog     *zd_batchtags;                                                    //  batch tags zdialog
EX zdialog     *zd_metaview;                                                     //  view metadata zdialog
EX zdialog     *zd_filesave;                                                     //  file save zdialog
EX zdialog     *zd_rename;                                                       //  rename file zdialog
EX zdialog     *zd_copymove;                                                     //  copy/move file zdialog
EX zdialog     *zd_deltrash;                                                     //  delete/trash zdialog
EX zdialog     *zd_upright;                                                      //  upright file zdialog
EX zdialog     *zd_sela;                                                         //  select area zdialog
EX zdialog     *zd_magnify;                                                      //  magnify image zdialog
EX zdialog     *zd_album_replacefile;                                            //  album_replacefile zdialog             19.0
EX zdialog     *zd_gallery_select1;                                              //  gallery_select1 zdialog
EX zdialog     *zd_gallery_select;                                               //  gallery_select zdialog
EX zdialog     *zd_edit_bookmarks;                                               //  bookmarks edit zdialog
EX zdialog     *zd_ss_imageprefs;                                                //  slide show image prefs zdialog

//  method for running thread to send an event to an active zdialog

EX zdialog     *zd_thread;                                                       //  active zdialog
EX cchar       *zd_thread_event;                                                 //  event to send

//  edit function parameters

EX int         trimx1, trimy1, trimx2, trimy2;                                   //  trim rectangle NW and SE corners
EX int         trimww, trimhh;                                                   //  trim rectangle width and height
EX char        *trimbuttons[5];                                                  //  trim dialog button labels
EX char        *trimratios[5];                                                   //  corresponding aspect ratios
EX int         editresize[2];                                                    //  edit resize width, height
EX float       whitebalance[4];                                                  //  white balance factors                 19.0
EX float       lens_mm;                                                          //  pano lens mm setting

//  GTK functions (fotoxx main)

int   main(int argc, char * argv[]);                                             //  main program
int   initzfunc(void *);                                                         //  initializations
int   delete_event();                                                            //  window delete event function
int   destroy_event();                                                           //  window destroy event function
int   state_event(GtkWidget *, GdkEvent *event);                                 //  window state event function
void  drop_event(int mousex, int mousey, char *file);                            //  file drag-drop event function
int   gtimefunc(void *arg);                                                      //  periodic function
void  update_Fpanel();                                                           //  update F window information panel
void  paintlock(int lock);                                                       //  block or unblock window updates
int   Fpaint(GtkWidget *, cairo_t *);                                            //  F and W drawing area paint function
void  Fpaintnow();                                                               //  window repaint synchronously
void  Fpaint2();                                                                 //  window repaint (thread callable)
void  Fpaint3(int px, int py, int ww, int hh, cairo_t *cr);                      //  update Mpxb area from updated E3 area
void  Fpaint0(int px, int py, int ww, int hh, cairo_t *cr);                      //  update Mpxb area from updated E0 area
void  Fpaint4(int px, int py, int ww, int hh, cairo_t *cr);                      //  update Dpxb area from updated Mpxb
void  Fpaint3_thread(int px, int py, int ww, int hh);                            //  Fpaint3 for threads (define update area)
void  Fpaint3_main();                                                            //  update Fpaint3 area in main thread
void  mouse_event(GtkWidget *, GdkEventButton *, void *);                        //  mouse event function
void  m_zoom(GtkWidget *, cchar *);                                              //  zoom image +/-
void  KBevent(GdkEventKey *event);                                               //  pass dialog KB events to main app
int   KBpress(GtkWidget *, GdkEventKey *, void *);                               //  KB key press event function
int   KBrelease(GtkWidget *, GdkEventKey *, void *);                             //  KB key release event function
void  win_fullscreen(int hidemenu);                                              //  full screen without menu/panel
void  win_unfullscreen();                                                        //  restore to prior size with menu etc.
void  set_mwin_title();                                                          //  main window title = curr_file path

//  cairo drawing functions (fotoxx main)

void  draw_pixel(int px, int py, cairo_t *cr, int fat = 0);                      //  draw pixel
void  erase_pixel(int px, int py, cairo_t *cr);                                  //  erase pixel
void  draw_line(int x1, int y1, int x2, int y2, int type, cairo_t *cr);          //  draw line or dotted line (type 1/2)
void  erase_line(int x1, int y1, int x2, int y2, cairo_t *cr);                   //  erase line
void  draw_toplines(int arg, cairo_t *cr);                                       //  draw all pre-set overlay lines
void  draw_gridlines(cairo_t *cr);                                               //  draw grid lines on image
void  add_toptext(int ID, int px, int py, cchar *text, cchar *font);             //  add text string with ID on window
void  draw_toptext(cairo_t *cr);                                                 //  draw text strings when window repainted
void  erase_toptext(int ID);                                                     //  remove all text strings with ID
void  draw_text(int px, int py, cchar *text, cchar *font, cairo_t *cr);          //  draw text on window
void  add_topcircle(int px, int py, int radius);                                 //  draw circle on window
void  draw_topcircles(cairo_t *cr);                                              //  draw circles when window repainted
void  erase_topcircles();                                                        //  remove all circles
void  draw_mousecircle(int cx, int cy, int rad, int Ferase, cairo_t *cr);        //  draw circle around mouse pointer
void  draw_mousecircle2(int cx, int cy, int rad, int Ferase, cairo_t *cr);       //  2nd circle for m_copypixels1()
void  draw_mousearc(int cx, int cy, int ww, int hh, int Ferase, cairo_t *cr);    //  draw ellipse around pointer

//  spline curve edit functions (fotoxx main)

spldat * splcurve_init(GtkWidget *frame, void func(int spc));                    //  initialize spline curves
int      splcurve_adjust(void *, GdkEventButton *event, spldat *);               //  curve editing function
int      splcurve_addnode(spldat *, int spc, float px, float py);                //  add a new node to a curve
int      splcurve_resize(GtkWidget *);                                           //  adjust drawing area height
int      splcurve_draw(GtkWidget *, cairo_t *, spldat *);                        //  spline curve draw signal function
int      splcurve_generate(spldat *, int spc);                                   //  generate data from anchor points
float    splcurve_yval(spldat *, int spc, float xval);                           //  get curve y-value
int      splcurve_load(spldat *sd, FILE *fid = 0);                               //  load curve from a file
int      splcurve_save(spldat *sd, FILE *fid = 0);                               //  save curve to a file

//  edit support functions (fotoxx main)

int   edit_setup(editfunc &EF);                                                  //  start new edit transaction
void  edit_cancel(int keep);                                                     //  cancel edit (keep zdialog etc.)
void  edit_done(int keep);                                                       //  commit edit, add undo stack
void  edit_undo();                                                               //  undo edit, back to org. image
void  edit_redo();                                                               //  redo the edit after undo
void  edit_reset();                                                              //  reset to initial status
void  edit_fullsize();                                                           //  convert to full-size pixmaps
int   edit_load_widgets(editfunc *EF, FILE *fid = 0);                            //  load zdialog widgets from a file
int   edit_save_widgets(editfunc *EF, FILE *fid = 0);                            //  save zdialog widgets to a file
int   edit_save_last_widgets(editfunc *EF);                                      //  save last-used zdialog widgets
int   edit_load_prev_widgets(editfunc *EF);                                      //  load last-used zdialog widgets
void  m_undo_redo(GtkWidget *, cchar *);                                         //  undo / redo if left / right mouse click
void  m_undo(GtkWidget *, cchar *);                                              //  undo one edit
void  m_redo(GtkWidget *, cchar *);                                              //  redo one edit
void  undo_all();                                                                //  undo all edits of current image
void  redo_all();                                                                //  redo all edits of current image
void  save_undo();                                                               //  undo/redo save function
void  load_undo();                                                               //  undo/redo load function
int   checkpend(cchar *list);                                                    //  check for lock, busy, pending changes

typedef void mcbFunc();                                                          //  callback function type
EX mcbFunc   *mouseCBfunc;                                                       //  current edit mouse function
void  takeMouse(mcbFunc func, GdkCursor *);                                      //  capture mouse for edit dialog
void  freeMouse();                                                               //  free mouse for main window

//  thread support functions (fotoxx main)

typedef void * threadfunc(void *);                                               //  edit thread function
void  start_thread(threadfunc func, void *arg);                                  //  start edit thread
void  signal_thread();                                                           //  signal thread, work is pending
void  wait_thread_idle();                                                        //  wait for work complete
void  wrapup_thread(int command);                                                //  wait for exit or command exit
void  thread_idle_loop();                                                        //  thread: wait for work or exit command
void  thread_exit();                                                             //  thread: exit unconditionally
void  do_wthreads(threadfunc func, int Nt);                                      //  start worker threads and wait         18.07

//  other support functions (fotoxx main)

void  save_params();                                                             //  save parameters for next session
void  load_params();                                                             //  load parameters from prior session
void  free_resources(int fkeepundo = 0);                                         //  free all allocated resources
int   sigdiff(float d1, float d2, float signf);                                  //  test for significant difference

//  window and menu builder (f.widgets.cc)

void  build_widgets();                                                           //  build widgets for F/G/W/M view modes
void  popup_menufunc(GtkWidget *, cchar *menu);                                  //  image/thumb right-click menu func
void  image_Rclick_popup();                                                      //  popup menu for image right-click
void  gallery_Lclick_func(int Nth);                                              //  function for thumbnail left-click
void  gallery_Rclick_popup(int Nth);                                             //  popup menu for thumbnail right-click
void  m_viewmode(GtkWidget *, cchar *fgw);                                       //  set current F/G/W/M view mode
void  m_favorites(GtkWidget *, cchar *);                                         //  graphic popup menu, user favorites

//  file menu functions (f.file.cc)

void  m_new_session(GtkWidget *, cchar *);                                       //  start parallel fotoxx session
void  new_session(cchar *args);                                                  //  callable function                     18.07
void  m_recentfiles(GtkWidget *, cchar *);                                       //  open recently accessed file
void  add_recent_file(cchar *file);                                              //  add file to recent file list
void  m_newfiles(GtkWidget *, cchar *);                                          //  open newly added file
void  m_open_drag(int x, int y, char *file);                                     //  open drag-drop file
void  m_cycle2files(GtkWidget *, cchar *);                                       //  open/cycle 2 previous files
void  m_cycle3files(GtkWidget *, cchar *);                                       //  open/cycle 3 previous files
void  m_rawtherapee(GtkWidget *, cchar *rawfile);                                //  open RAW file with Raw Therapee       19.0
void  m_view360(GtkWidget *, cchar *);                                           //  view a 360 degree panorama            18.01
int   f_open(cchar *file, int n = 0, int kp = 0, int ak = 1, int z = 0);         //  open new current image file
int   f_open_saved();                                                            //  open saved file, retain undo/redo stack
void  f_preload(int next);                                                       //  start preload of next file
void  m_prev(GtkWidget *, cchar *);                                              //  open previous file in gallery
void  m_next(GtkWidget *, cchar *);                                              //  open next file in gallery
void  m_prev_next(GtkWidget *, cchar *);                                         //  open prev/next if left/right icon click
void  m_rename(GtkWidget *, cchar *);                                            //  rename an image file (same location)
void  m_blank_image(GtkWidget *, cchar *);                                       //  create new blank image file
int   create_blank_file(cchar *file, int ww, int hh, int RGB[3]);                //  create new blank image file, callable
void  m_blank_window(GtkWidget *, cchar *);                                      //  blank / unblank window                18.01
void  m_copy_move(GtkWidget *, cchar *);                                         //  copy or move image file to new location
void  m_copyto_desktop(GtkWidget *, cchar *);                                    //  copy image file to desktop
void  m_copyto_clip(GtkWidget *, cchar *file);                                   //  copy an image file to the clipboard
void  m_wallpaper(GtkWidget *, cchar *file);                                     //  current file >> desktop wallpaper     19.0
void  m_delete_trash(GtkWidget *, cchar *);                                      //  delete or trash an image file
void  m_print(GtkWidget *, cchar *);                                             //  print an image file
void  m_print_calibrated(GtkWidget *, cchar *);                                  //  print an image file with adjusted colors
void  m_quit(GtkWidget *, cchar *);                                              //  exit application with checks
void  quitxx();                                                                  //  exit unconditionally
void  m_help(GtkWidget *, cchar *menu);                                          //  various help menu functions

void  m_file_save(GtkWidget *, cchar *);                                         //  save modified image file to disk
void  m_file_save_replace(GtkWidget *, cchar *menu);                             //  for KB shortcut Save File (replace) 
void  m_file_save_version(GtkWidget *, cchar *menu);                             //  for KB shortcut Save File Version

char * file_rootname(cchar *file);                                               //  get root name without .vNN.ext        18.07
char * file_basename(cchar *file);                                               //  get base name /.../filename.ext       19.0
char ** file_all_versions(cchar *file, int &nf);                                 //  get all versions for given file       19.0
char * file_new_version(cchar *file);                                            //  get next avail. file version name     19.0
char * file_newest_version(cchar *file);                                         //  get newest version or original file   19.0
char * file_prior_version(cchar *file);                                          //  get prior version or original file    19.0

int   f_save(char *outfile, cchar *type, int bpc, int qual, int ack);            //  save current image file to disk       19.0
int   f_save_as();                                                               //  save_as dialog (choose file and bpc)

int    linedit_open(cchar *filespec);                                            //  open text file for line editing
char * linedit_get();                                                            //  read next existing record
int    linedit_put(cchar *record);                                               //  put next output record
int    linedit_close();                                                          //  finish, output file replaces input

int    find_imagefiles(cchar *dr, int fgs, char **&fils, int &nf, int zf = 1);   //  find all image files within a folder
char * raw_to_tiff(cchar *rawfile);                                              //  RAW filespec >> corresp. tiff filespec

//  thumbnail gallery and navigation functions (f.gallery.cc)

char   * gallery(cchar *filez, cchar *action, int Nth);                          //  display image gallery window, navigate
void     gallery_memory(cchar *action);                                          //  save/recall gallery sort and posn.    18.01
void     set_gwin_title();                                                       //  main window title = gallery name
char   * prev_next_file(int index, int lastver);                                 //  get prev/next file with last version option
char   * prev_next_gallery(int index);                                           //  get prev/next gallery (physical folder)
int      file_position(cchar *file, int Nth);                                    //  get rel. position of file in gallery

FTYPE    image_file_type(cchar *file);                                           //  file type: FNF folder IMAGE RAW THUMB OTHER
char   * thumb2imagefile(cchar *thumbfile);                                      //  get image file for thumbnail file
char   * image2thumbfile(cchar *imagefile);                                      //  get thumbnail file for image file
int      thumbfile_OK(cchar *imagefile);                                         //  check thumbnail file exists and not stale
int      update_thumbfile(cchar *imagefile);                                     //  create or refresh thumbnail file
void     delete_thumbfile(cchar *imagefile);                                     //  delete thumbnail for file
PIXBUF * get_cache_thumb(cchar *imagefile, PXB *thumbpxb);                       //  get thumbnail from cache, add if needed
void     preload_thumbs(int targetfile);                                         //  preload thumbnails into cache

void     gallery_popimage();                                                     //  popup big image of clicked thumbnail
char   * gallery_select1(cchar *gfolder);                                        //  select one file from gallery window
void     gallery_select1_Lclick_func(int Nth);                                   //  gallery_select1, thumbnail left-click
void     gallery_select_clear();                                                 //  clear gallery_select() file list
int      gallery_select();                                                       //  select files from gallery window
void     gallery_select_Lclick_func(int Nth);                                    //  gallery_select, thumbnail left-click
void     gallery_select_Rclick_func(int Nth);                                    //  gallery_select, thumbnail right-click

void     m_source_folder(GtkWidget *, cchar *);                                  //  set gallery from current file folder
void     m_alldirs(GtkWidget *, cchar *);                                        //  list folders, click for gallery 
void     m_bookmarks(GtkWidget *, cchar *);                                      //  select bookmarked image, goto gallery posn
void     m_edit_bookmarks(GtkWidget *, cchar *);                                 //  edit bookmarks (gallery/file position)
void     edit_bookmarks_Lclick_func(int Nth);                                    //  thumbnail click response function
void     m_show_hidden(GtkWidget *, cchar *);                                    //  for KB shortcut "show hidden files"   18.01

//  albums and slide show menu (f.albums.cc)

void  m_manage_albums(GtkWidget *, cchar *);                                     //  create and edit image albums
int   album_from_gallery(cchar *albumname);                                      //  create album from current gallery 
void  album_show(char *file = 0);                                                //  show current album in gallery
void  album_purge_replace();                                                     //  purge/replace deleted files           19.0
void  album_replacefile_Lclick_func(int Nth);                                    //  album_replacefile() thumbnail click   19.0
void  album_pastefile(char *file, int posn);                                     //  insert file at position
void  album_movefile(int pos1, int pos2);                                        //  move file position in album
void  album_batch_convert(char **, char **, int);                                //  convert after files renamed/moved
void  m_current_album(GtkWidget *, cchar *);                                     //  show current or last used album       18.07
void  m_album_copy2cache(GtkWidget *, cchar *);                                  //  copy an image file to the file cache
void  m_album_cut2cache(GtkWidget *, cchar *);                                   //  remove image from album, add to cache
void  m_album_pastecache(GtkWidget *, cchar *);                                  //  paste image cache to album position
void  m_album_removefile(GtkWidget *, cchar *);                                  //  remove image from album

void  m_slideshow(GtkWidget *, cchar *);                                         //  enter or leave slideshow mode
void  ss_imageprefs_Lclick_func(int Nth);                                        //  slideshow image prefs thumbnail click func
void  ss_KBfunc(int KBkey);                                                      //  slideshow keyboard input              18.01

//  metadata menu functions (f.meta.cc)

int   select_meta_keys(char *itemlist[], int Fexclude);                          //  dialog - select metadata items        18.01
void  meta_view(int type);                                                       //  popup metadata report
void  m_meta_view_short(GtkWidget *, cchar *);                                   //  view selected EXIF/IPTC data
void  m_meta_view_long(GtkWidget *, cchar *);                                    //  view all EXIF/IPTC data
void  m_meta_edit_main(GtkWidget *, cchar *);                                    //  edit date/rating/tags dialog
void  m_meta_edit_any(GtkWidget *, cchar *);                                     //  add or change any EXIF/IPTC data
void  m_meta_delete(GtkWidget *, cchar *);                                       //  delete EXIF/IPTC data
void  m_meta_captions(GtkWidget *, cchar *);                                     //  show caption/comments at the top

void  m_batch_tags(GtkWidget *, cchar *);                                        //  add and remove tags
void  m_batch_rename_tags(GtkWidget *, cchar *);                                 //  rename tags for all image files 
void  m_batch_photo_date_time(GtkWidget *, cchar *menu);                         //  change or shift photo dates/times     18.01
void  m_batch_change_metadata(GtkWidget *, cchar *);                             //  add/change/delete metadata
void  m_batch_report_metadata(GtkWidget *, cchar *);                             //  metadata report
void  m_batch_geotags(GtkWidget *, cchar *menu);                                 //  add or change geotags

void  m_meta_places_dates(GtkWidget *, cchar *);                                 //  report images by location and date range
void  m_meta_timeline(GtkWidget *, cchar *);                                     //  report images by month
void  m_search_images(GtkWidget *, cchar *);                                     //  search image metadata

void  metadate_pdate(cchar *metadate, char *pdate, char *ptime);                 //  "yyyymmddhhmm" > "yyyy-mm-dd" + "hh:mm:ss"
void  load_filemeta(cchar *file);                                                //  load metadata for image file
void  save_filemeta(cchar *file);                                                //  save metadata in EXIF and image index
int   add_tag_fotoxx(cchar *file);                                               //  add "fotoxx" tag to an edited file
void  set_meta_wwhh(int ww, int hh);                                             //  set ww/hh outside m_meta_edit()
void  update_image_index(cchar *file);                                           //  update image index for image file
void  delete_image_index(cchar *file);                                           //  delete image index for image file

void  m_set_map_markers(GtkWidget *, cchar *);                                   //  set markers for all images or gallery only

void  m_load_filemap(GtkWidget *, cchar *);                                      //  load a file map chosen by user (W view)
void  filemap_mousefunc();                                                       //  mouse function for file map view mode
void  filemap_paint_dots();                                                      //  paint red dots on map image locations
void  free_filemap();                                                            //  free memory used by file map image

void  m_netmap_source(GtkWidget *, cchar *);                                     //  choose net map source (M view)
void  m_load_netmap(GtkWidget *, cchar *);                                       //  load the initial net map
void  netmap_paint_dots();                                                       //  paint red dots on map image locations
void  m_netmap_zoomin(GtkWidget *, cchar *);                                     //  zoom net map to given location
void  m_netmap_locs(GtkWidget *, cchar *);                                       //  save amd recall net map locations

int   exif_get(cchar *file, cchar **keys, char **kdat, int nk);                  //  get EXIF/IPTC data for given keys
int   exif_put(cchar *file, cchar **keys, cchar **kdat, int nk);                 //  put EXIF/IPTC data for given keys
int   exif_copy(cchar *f1, cchar *f2, cchar **keys, cchar **kdat, int nk);       //  copy EXIF/IPTC data from file to file
int   exif_server(int Nrecs, char **inputs, char **outputs);                     //  run exiftool as a server process
void  exif_tagdate(cchar *exifdate, char *tagdate);                              //  yyyy:mm:dd:hh:mm:ss to yyyymmddhhmmss
void  tag_exifdate(cchar *tagdate, char *exifdate);                              //  yyyymmddhhmmss to yyyy:mm:dd:hh:mm:ss

xxrec_t * get_xxrec(cchar *file);                                                //  get image index record from memory table
int put_xxrec(xxrec_t *xxrec, cchar *file);                                      //  add/update image index table and file
xxrec_t * read_xxrec_seq(int &ftf);                                              //  read image index records 1-last
int write_xxrec_seq(xxrec_t *xxrec, int &ftf);                                   //  write image index records 1-last

//  select area menu functions (f.area.cc)

void  m_select(GtkWidget *, cchar *);                                            //  select area within image
void  m_select_hairy(GtkWidget *, cchar *);                                      //  select hairy or irregular edge
void  m_select_find_gap(GtkWidget *, cchar *);                                   //  find gap in area outline
void  m_select_show(GtkWidget *, cchar *);                                       //  enable area for subsequent edits
void  m_select_hide(GtkWidget *, cchar *);                                       //  show area outline
void  m_select_enable(GtkWidget *, cchar *);                                     //  hide area outline
void  m_select_disable(GtkWidget *, cchar *);                                    //  disable area
void  m_select_invert(GtkWidget *, cchar *);                                     //  invert area
void  m_select_clear(GtkWidget *, cchar *);                                      //  clear area

void  m_select_copy(GtkWidget *, cchar *);                                       //  copy area to default file
void  m_select_save(GtkWidget *, cchar *);                                       //  copy area to designamted file
void  m_select_load(GtkWidget *, cchar *);                                       //  load file and paste as area in image
void  m_select_paste(GtkWidget *, cchar *);                                      //  paste last copied area into image

void  sa_geom_mousefunc();                                                       //  select rectangle or ellipse
void  sa_draw_mousefunc();                                                       //  edge line drawing function
int   sa_nearpix(int mx, int my, int rad, int &px, int &py, int fx);             //  find nearest pixel
void  sa_draw_line(int px1, int py1, int px2, int py2, cairo_t *cr);             //  draw a connected line
void  sa_draw1pix(int px, int py, cairo_t *cr);                                  //  add one pixel to select area
void  sa_mouse_select();                                                         //  select by mouse (opt. color match)
void  sa_nextseq();                                                              //  start next sequence number
void  sa_unselect_pixels(cairo_t *cr);                                           //  remove current selection
void  sa_pixmap_create();                                                        //  create area pixel maps
void  sa_finish();                                                               //  finish - map interior pixels
void  sa_finish_auto();                                                          //  finish - interior pixels already known
void  sa_unfinish();                                                             //  set finished area back to edit mode
void  sa_map_pixels();                                                           //  map edge and interior pixels
void  sa_show(int flag, cairo_t *cr);                                            //  show or hide area outline
void  sa_show_rect(int px1, int py1, int ww, int hh, cairo_t *cr);               //  show area outline inside a rectangle
int   sa_validate();                                                             //  validate area for curr. image
void  sa_enable();                                                               //  enable area
void  sa_disable();                                                              //  disable area
void  sa_invert();                                                               //  invert area
void  sa_clear();                                                                //  clear area
void  sa_edgecalc();                                                             //  calculate edge distances
void  sa_edgecreep(int);                                                         //  adjust area edge +/-
float sa_blendfunc(int edgedist);                                                //  calculate edge blend factor

//  image edit functions - Edit menu (f.edit1.cc)

void  m_trim_rotate(GtkWidget *, cchar *);                                       //  combined trim/rotate function
void  m_resize(GtkWidget *, cchar *);                                            //  resize image
void  m_retouch(GtkWidget *, cchar *);                                           //  brightness, contrast, color
void  m_colorbal(GtkWidget *, cchar *);                                          //  white point, black point, temperature
void  m_sharpen(GtkWidget *, cchar *);                                           //  sharpen image
void  sharp_GR_callable(int amount, int thresh);                                 //  callable gradient sharpen             18.07
void  m_blur(GtkWidget *, cchar *);                                              //  blur image
void  m_blur_background(GtkWidget *, cchar *);                                   //  blur image outside of selected areas
void  m_denoise(GtkWidget *, cchar *);                                           //  image noise reduction
void  m_redeyes(GtkWidget *, cchar *);                                           //  red-eye removal
void  m_color_mode(GtkWidget *, cchar *);                                        //  B+W/color, negative/positive, sepia
void  m_color_sat(GtkWidget *, cchar *);                                         //  adjust color saturation
void  m_adjust_RGB(GtkWidget *, cchar *);                                        //  color adjust using RGB or CMY colors
void  m_adjust_HSL(GtkWidget *, cchar *);                                        //  color adjust with HSL
void  HSLtoRGB(float H, float S, float L, float &R, float &G, float &B);         //  convert HSL color space to RGB
void  RGBtoHSL(float R, float G, float B, float &H, float &S, float &L);         //  convert RGB color space to HSL
void  m_add_text(GtkWidget *, cchar *);                                          //  add text to an image
void  load_text(zdialog *zd);                                                    //  load text and attributes from a file
void  save_text(zdialog *zd);                                                    //  save text and attributes to a file
int   gentext(textattr_t *attr);                                                 //  generate text image from attributes
void  m_add_lines(GtkWidget *, cchar *);                                         //  add line/arrow to an image
void  load_line(zdialog *zd);                                                    //  load line attributes from a file
void  save_line(zdialog *zd);                                                    //  save line attributes to a file
int   genline(lineattr_t *attr);                                                 //  generate line/arrow image from attributes
void  m_upright(GtkWidget *, cchar *);                                           //  upright rotated image
void  m_mirror(GtkWidget *, cchar *);                                            //  mirror image horz. or vert.
void  m_paint_edits(GtkWidget *, cchar *);                                       //  select and edit in parallel
void  m_lever_edits(GtkWidget *, cchar *);                                       //  leverage edits by pixel bright/color

//  image edit functions - Repair menu (f.edit2.cc)

void  m_voodoo1(GtkWidget *, cchar *);                                           //  automatic image retouch
void  m_voodoo2(GtkWidget *, cchar *);                                           //  automatic image retouch
void  m_edit_brightness(GtkWidget *, cchar *);                                   //  edit brightness distribution
void  m_gradients(GtkWidget *, cchar *);                                         //  magnify brightness gradients
void  m_flatten(GtkWidget *, cchar *);                                           //  flatten brightness distribution
void  m_gretinex(GtkWidget *, cchar *);                                          //  rescale RGB levels - global           18.07
void  m_zretinex(GtkWidget *, cchar *);                                          //  rescale RGB levels - zonal            18.07
void  m_zonal_colors(GtkWidget *, cchar *);                                      //  revise brightness/color in local areas
void  m_match_colors(GtkWidget *, cchar *);                                      //  set image2 colors to match image1
void  m_colordepth(GtkWidget *, cchar *);                                        //  set color depth 1-16 bits/color
void  m_smart_erase(GtkWidget *, const char *);                                  //  smart erase object
void  m_bright_ramp(GtkWidget *, cchar *);                                       //  add brightness/color ramp across image
void  m_paint_image(GtkWidget *, cchar *);                                       //  paint pixels with the mouse
int   color_chooser(zdialog *zd, cchar *butt, uint8 rgb[3]);                     //  dialog, get color from color file     19.0
int   HSL_chooser(zdialog *zd, cchar *butt, uint8 RGB[3]);                       //  dialog, get color from HSL dialog     19.0
void  m_copypixels1(GtkWidget *, cchar *);                                       //  copy pixels within one image
void  m_copypixels2(GtkWidget *, cchar *);                                       //  copy pixels from imageA to imageB     18.07
void  m_copypixels3(GtkWidget *, cchar *);                                       //  source image process for the above    18.07
void  m_paint_transp(GtkWidget *, cchar *);                                      //  paint transparnecy with the mouse
void  m_color_fringes(GtkWidget *, const char *);                                //  reduce chromatic abberation
void  m_anti_alias(GtkWidget *, const char *);                                   //  anti-alias
void  m_plugins(GtkWidget *, cchar *);                                           //  edit plugins menu or run a plugin function

//  image edit functions - Warp menu (f.warp.cc)

void  m_unbend(GtkWidget *, cchar *);                                            //  unbend panoramas
void  m_perspective(GtkWidget *, cchar *);                                       //  warp tetragon into rectangle
void  m_warp_area(GtkWidget *, cchar *);                                         //  warp image within an area
void  m_unwarp_closeup(GtkWidget *, cchar *);                                    //  warp image within closeup face area
void  m_warp_curved(GtkWidget *, cchar *);                                       //  warp image, curved transform
void  m_warp_linear(GtkWidget *, cchar *);                                       //  warp image, linear transform
void  m_warp_affine(GtkWidget *, cchar *);                                       //  warp image, affine transform
void  m_flatbook(GtkWidget *, cchar *);                                          //  flatten a photographed book page
void  m_area_rescale(GtkWidget *, cchar *);                                      //  rescale image, selected areas unchanged
void  m_waves(GtkWidget *, cchar *);                                             //  warp image using a wave pattern
void  m_twist(GtkWidget *, cchar *);                                             //  twist image centered at mouse
void  m_sphere(GtkWidget *, cchar *);                                            //  image spherical projection
void  m_stretch(GtkWidget *, cchar *);                                           //  image stretch projection              19.0
void  m_inside_out(GtkWidget *, cchar *);                                        //  turn an image inside-out              19.0
void  m_tiny_planet(GtkWidget *, cchar *);                                       //  convert image to tiny planet          19.0

//  image edit functions - Effects menu (f.effects.cc)

void  m_sketch(GtkWidget *, cchar *);                                            //  simulated sketch
void  m_cartoon(GtkWidget *, cchar *);                                           //  cartoon drawing
void  m_line_drawing(GtkWidget *, cchar *);                                      //  outline drawing
void  m_color_drawing(GtkWidget *, cchar *);                                     //  solid color drawing
void  m_emboss(GtkWidget *, cchar *);                                            //  simulated embossing
void  m_tiles(GtkWidget *, cchar *);                                             //  tile array (pixelate)
void  m_dither(GtkWidget *, cchar *);                                            //  dithered dots                         19.0
void  m_painting(GtkWidget *, cchar *);                                          //  simulated painting
void  m_vignette(GtkWidget *, cchar *);                                          //  vignette tool
void  m_texture(GtkWidget *, cchar *);                                           //  add texture to image
void  m_pattern(GtkWidget *, cchar *);                                           //  tile image with a pattern
void  m_mosaic(GtkWidget *, cchar *);                                            //  make mosaic with tiles from images
void  m_shift_colors(GtkWidget *, cchar *);                                      //  shift colors into other colors
void  m_alien_colors(GtkWidget *, cchar *);                                      //  revise hues using an algorithm
void  m_anykernel(GtkWidget *, cchar *);                                         //  apply custom convolution kernel

//  image edit functions - Combine menu (f.combine.cc)

void  m_HDR(GtkWidget *, cchar *);                                               //  make HDR composite image
void  m_HDF(GtkWidget *, cchar *);                                               //  make HDF composite image
void  m_stack_paint(GtkWidget *, cchar *);                                       //  stack / paint image
void  m_stack_noise(GtkWidget *, cchar *);                                       //  stack / noise reduction
void  m_stack_layer(GtkWidget *, cchar *);                                       //  stack / layer paint image             19.0
void  m_pano_horz(GtkWidget *, cchar *);                                         //  make panorama composite image
void  m_pano_vert(GtkWidget *, cchar *);                                         //  make vertical panorama
void  m_pano_PT(GtkWidget *, cchar *);                                           //  make panorama via pano tools
void  m_image_diffs(GtkWidget *, cchar *);                                       //  show differences between 2 images

//  image edit functions - mashup and montage (f.mashup.cc) 

void  m_mashup(GtkWidget *, cchar *);                                            //  arrange images and text in custom layout
void  m_montage(GtkWidget *, cchar *);                                           //  combine images into an image montage
void  montage_Lclick_func(int mx, int my);                                       //  montage image click function

//  process menu functions (f.process.cc)

void  m_batch_convert(GtkWidget *, cchar *);                                     //  rename/convert/resize/export image files
int   batch_sharp_func(PXM *pxm, int amount, int thresh);                        //  callable sharpen func used by batch funcs
void  m_batch_upright(GtkWidget *, cchar *);                                     //  upright rotated image files
void  m_batch_deltrash(GtkWidget *, cchar *);                                    //  delete or trash selected files
void  m_batch_RAW(GtkWidget *, cchar *);                                         //  convert RAW files, libraw or Raw Therapee
void  m_burn_DVD(GtkWidget *, cchar *);                                          //  burn selected images to CD/DVD
void  m_export_filelist(GtkWidget *, cchar *);                                   //  create file of selected image files
void  m_export_files(GtkWidget *, cchar *);                                      //  export image files to a folder
void  m_edit_script(GtkWidget *, cchar *);                                       //  build script file with N edit funcs   18.07
void  edit_script_addfunc(editfunc *);                                           //  edit_done() hook to add script func   18.07
void  m_run_script(GtkWidget *, cchar *);                                        //  run script on current image file      18.07
void  m_batch_script(GtkWidget *, cchar *);                                      //  run script on batch of image files    18.07

//  tools menu functions (f.tools.cc)

void  m_index(GtkWidget *, cchar *);                                             //  rebuild image index and thumbnails
void  index_rebuild(int level, int menu);                                        //  index rebuild function
void  m_move_fotoxx_home(GtkWidget *, cchar *);                                  //  move fotoxx home folder               18.07
void  m_settings(GtkWidget *, cchar *);                                          //  user settings
void  m_KBshortcuts(GtkWidget *, cchar *);                                       //  edit KB shortcuts, update file
int   KBshortcuts_load();                                                        //  load KB shortcuts at startup time
void  m_brightgraph(GtkWidget *, cchar *);                                       //  show brightness distribution graph
void  brightgraph_graph(GtkWidget *drawin, cairo_t *cr, int *);                  //  draw brightness distribution graph
void  brightgraph_scale(GtkWidget *drawarea, cairo_t *cr, int *);                //  draw brightness scale, black to white band
void  m_magnify(GtkWidget *, cchar *);                                           //  magnify image within a radius of the mouse
void  m_duplicates(GtkWidget *, cchar *);                                        //  find duplicate image files
void  m_show_RGB(GtkWidget *, cchar *);                                          //  show RGB values at mouse click
void  m_color_profile(GtkWidget *, cchar *);                                     //  convert to another color profile
void  m_calibrate_printer(GtkWidget *, cchar *);                                 //  calibrate printer colors
void  print_calibrated();                                                        //  print image with adjusted colors
void  m_gridlines(GtkWidget *, cchar *);                                         //  grid lines setup dialog
void  toggle_grid(int action);                                                   //  set grid off/on or toggle (0/1/2)
void  m_line_color(GtkWidget *, cchar *);                                        //  foreground line color (area/mouse/trim...)
void  m_darkbrite(GtkWidget *, cchar *);                                         //  highlight the darkest and brightest pixels
void  darkbrite_paint();                                                         //  paint function called from Fpaint()
void  m_remove_dust(GtkWidget *, const char *);                                  //  remove dust
void  m_stuck_pixels(GtkWidget *, const char *);                                 //  fix stuck pixels
void  m_monitor_color(GtkWidget *, cchar *);                                     //  check monitor brightness and color
void  m_monitor_gamma(GtkWidget *, cchar *);                                     //  adjust monitor gamma
void  m_change_lang(GtkWidget *, cchar *);                                       //  change language
void  m_untranslated(GtkWidget *, cchar *);                                      //  report missing translations
void  m_resources(GtkWidget *, cchar *);                                         //  report CPU and memory usage
void  m_zappcrash_test(GtkWidget *, cchar *);                                    //  zappcrash test function

//  pixmap memory image and file I/O functions (f.pixmap.cc)

PXM * PXM_make(int ww, int hh, int nc);                                          //  create a PXM pixmap
void  PXM_free(PXM *&pxm);                                                       //  free PXM pixmap
void  PXM_audit(PXM *pxm);                                                       //  audit contents of a PXM pixmap
void  PXM_clear(PXM *pxm, int BW);                                               //  clear PXM pixmap to value BW (0/255)
void  PXM_addalpha(PXM *pxm);                                                    //  add alpha channel to PXM pixmap 
PXM * PXM_copy(PXM *pxm);                                                        //  copy PXM pixmap
PXM * PXM_copy_area(PXM *pxm, int orgx, int orgy, int ww, int hh);               //  copy section of PXM pixmap
PXM * PXM_rescale(PXM *pxm, int ww, int hh);                                     //  rescale PXM pixmap (ww/hh)
PXM * PXM_rotate(PXM *pxm, float angle, int fast = 0);                           //  rotate PXM pixmap

PXB * PXB_make(int ww, int hh, int nc);                                          //  create a PXB pixmap                   19.0
void  PXB_free(PXB *&pxb);                                                       //  free PXB pixmap
void  PXB_addalpha(PXB *pxb);                                                    //  add alpha channel to PXB pixmap
void  PXB_subalpha(PXB *pxb);                                                    //  remove alpha channel from PXB pixmap
PXB * PXB_copy(PXB *pxb);                                                        //  copy (duplicate) PXB pixmap
void  PXB_copy_area(PXB *, int, int, int, int, PXB *, int, int);                 //  copy area from one PXB to another
PXB * PXB_subpxb(PXB *pxb1, int px1, int py1, int ww, int hh);                   //  create new PXB from PXB section
PXB * PXB_half(PXB *pxb1);                                                       //  rescale PXB pixmap to 1/2 size
PXB * PXB_rescale(PXB *pxb, int ww, int hh);                                     //  rescale PXB pixmap (ww/hh)
PXB * PXB_resize(PXB *pxb1, int size);                                           //  resize PXB pixmap to max. ww/hh
PXB * PXB_rescale_fast(PXB *pxb1, int ww, int hh);                               //  rescale  " " faster, less quality
PXB * PXB_resize_fast(PXB *pxb1, int size);                                      //  resize PXB pixmap to max. ww/hh
PXB * PXB_rotate(PXB *pxb, float angle);                                         //  rotate PXB pixmap

int   vpixel(PXB *pxb, float px, float py, uint8 *vpix);                         //  get PXB virtual pixel at (px,py)
int   vpixel(PXM *pxm, float px, float py, float *vpix);                         //  get PXM virtual pixel at (px,py)

PXB * PXM_PXB_copy(PXM *pxm);                                                    //  PXM to pixbuf, same scale
void  PXM_PXB_update(PXM *, PXB *, int px3, int py3, int ww3, int hh3);          //  update PXB area from PXM, same scale
void  PXB_PXB_update(PXB *, PXB *, int px3, int py3, int ww3, int hh3);          //  update PXB area from PXB, any scale

PXB * PXB_load(cchar *filespec, int fack);                                       //  load file to PXB pixmap, 8 bpc
PXM * PXM_load(cchar *filespec, int fack);                                       //  load file to PXM pixmap, 8/16 bpc
int   PXB_save(PXB *pxb, cchar *filespec, int bpc, int quality, int fack);       //  save PXB pixmap to file
int   PXM_save(PXM *pxm, cchar *filespec, int bpc, int quality, int fack);       //  save PXM pixmap to file

PXB * JPG_PXB_load(cchar *file, int size = 0);                                   //  JPG file to PXB, 8 bpc 
PXM * JPG_PXM_load(cchar *file);                                                 //  JPG file to PXM, 8 bpc
int   PXB_JPG_save(PXB *pxb, cchar *file, int quality);                          //  PXB to JPG file, 8 bpc
int   PXM_JPG_save(PXM *pxm, cchar *file, int quality);                          //  PXM to JPG file, 8 bpc

PXB * TIFF_PXB_load(cchar *filespec);                                            //  TIFF file to PXB, 8 bpc
PXM * TIFF_PXM_load(cchar *filespec);                                            //  TIFF file to PXM, 8/16 bpc
int   PXB_TIFF_save(PXB *pxb, cchar *filespec, int bpc);                         //  PXB to TIFF file, 8/16 bpc
int   PXM_TIFF_save(PXM *pxm, cchar *filespec, int bpc);                         //  PXM to TIFF file, 8/16 bpc

PXB * PNG_PXB_load(cchar *filespec);                                             //  PNG file to PXB, 8 bpc, keep alpha
PXM * PNG_PXM_load(cchar *filespec);                                             //  PNG file to PXM, 8/16 bpc
int   PXB_PNG_save(PXB *pxb, cchar *filespec, int bpc);                          //  PXB to PNG file, 8/16 bpc
int   PXM_PNG_save(PXM *pxm, cchar *filespec, int bpc);                          //  PXM to PNG file, 8/16 bpc

PXB * ANY_PXB_load(cchar *filespec);                                             //  ANY file to PXB, 8 bpc
PXM * ANY_PXM_load(cchar *filespec);                                             //  ANY file to PXM, 8 bpc

PXB * RAW_PXB_load(cchar *filespec);                                             //  RAW file to PXB, 8 bpc
PXM * RAW_PXM_load(cchar *filespec);                                             //  RAW file to PXM, 16 bpc

void pixelvert(uint8  *buff1, uint8  *buff2, int ww, int hh, int nc1, int nc2);  //  convert pixel buffer format
void pixelvert(uint8  *buff1, uint16 *buff2, int ww, int hh, int nc1, int nc2);  //    and/or channel count
void pixelvert(uint8  *buff1, float  *buff2, int ww, int hh, int nc1, int nc2);
void pixelvert(uint16 *buff1, uint8  *buff2, int ww, int hh, int nc1, int nc2);
void pixelvert(uint16 *buff1, uint16 *buff2, int ww, int hh, int nc1, int nc2);
void pixelvert(uint16 *buff1, float  *buff2, int ww, int hh, int nc1, int nc2);
void pixelvert(float  *buff1, uint8  *buff2, int ww, int hh, int nc1, int nc2);
void pixelvert(float  *buff1, uint16 *buff2, int ww, int hh, int nc1, int nc2);
void pixelvert(float  *buff1, float  *buff2, int ww, int hh, int nc1, int nc2);


/********************************************************************************/

//  translatable strings used in multiple dialogs

#define Badd E2X("Add")
#define Baddall E2X("Add All")
#define Ball E2X("All")
#define Bamount E2X("Amount")
#define Bangle E2X("Angle")
#define Bapply E2X("Apply")
#define Bauto E2X("Auto")
#define Bblack E2X("Black")
#define Bblendwidth E2X("Blend Width")
#define Bblue E2X("Blue")
#define Bbottom E2X("Bottom")
#define Bbrightness E2X("Brightness")
#define Bbrowse E2X("Browse")
#define Bcalculate E2X("Calculate") 
#define Bcancel E2X("Cancel")
#define Bcenter E2X("Center")
#define Bchange E2X("Change") 
#define Bchoose E2X("Choose")
#define Bclear E2X("Clear")
#define Bclose E2X("Close")
#define Bcolor E2X("Color")
#define Bcompleted E2X("COMPLETED")
#define Bcontinue E2X("continue")
#define Bcontrast E2X("Contrast")
#define Bcopy E2X("Copy")
#define Bcreate E2X("Create")
#define Bcurvefile E2X("Curve File:")
#define Bcut E2X("Cut")
#define Bdeband E2X("Deband")
#define Bdelete E2X("Delete")
#define Bdisable E2X("Disable")
#define Bdisplay E2X("Display")
#define Bdone E2X("Done")
#define Bedge E2X("edge")
#define Bedit E2X("Edit")
#define Benable E2X("Enable")
#define Berase E2X("Erase")
#define Bfetch E2X("Fetch")
#define Bfileexists E2X("output file already exists")
#define Bfilenotfound E2X("file not found")
#define Bfilenotfound2 E2X("file not found: %s")
#define Bfileselected E2X("%d image files selected")
#define Bfind E2X("Find")
#define Bfinish E2X("Finish")
#define Bflatten E2X("Flatten")
#define Bfont E2X("Font")
#define Bgallerytruncated E2X("gallery truncated to %d images")
#define Bgreen E2X("Green")
#define Bgrid E2X("Grid")
#define Bheight E2X("Height")
#define Bhelp E2X("Help")
#define Bhide E2X("Hide")
#define Bimage E2X("Image")
#define Bimages E2X("Images")
#define Binsert E2X("Insert")
#define Binvert E2X("Invert")
#define Bkeep E2X("Keep")
#define Bleft E2X("Left")
#define Blength E2X("Length") 
#define Blimit E2X("limit")
#define Bload E2X("Load")
#define Bmagnify E2X("Magnify")
#define Bmake E2X("Make")
#define Bmanagetags E2X("Manage Tags")
#define Bmap E2X("Map")
#define Bmatchlevel E2X("Match Level:")
#define Bmax E2X("Max")
#define Bmouseradius E2X("mouse radius") 
#define Bnegative E2X("Negative")
#define Bnew E2X("New")
#define Bnext E2X("Next")
#define Bno E2X("No")
#define Bnowriteperm E2X("no write permission") 
#define Bnoimages E2X("no images")
#define Bnoindex E2X("image index disabled \n Enable?")
#define Bnofileselected E2X("no image files selected")
#define Bnone E2X("None")
#define Bnoselection E2X("no selection")
#define Boldindex E2X("image index not updated")
#define Bopacitycenter E2X("opacity center")
#define Bopacityedge E2X("opacity edge")
#define Bopen E2X("Open")
#define Bpaintradius E2X("Paint Radius")
#define Bpaste E2X("Paste")
#define Bpause E2X("Pause")
#define Bpercent E2X("Percent")
#define Bpower E2X("Power")
#define Bpresets E2X("Presets")
#define Bprev E2X("Prev")
#define Bproceed E2X("Proceed")
#define Bradius E2X("Radius")
#define Brange E2X("range")
#define Bred E2X("Red")
#define Bredo E2X("Redo")
#define Breduce E2X("Reduce")
#define Bremove E2X("Remove")
#define Brename E2X("Rename")
#define Breplace E2X("Replace")
#define Breserved E2X("Reserved")
#define Breset E2X("Reset")
#define Bright E2X("Right")
#define Brotate E2X("Rotate")
#define Brun E2X("Run")
#define Bsave E2X("Save")
#define Bsearch E2X("Search")
#define Bseconds E2X("Seconds")
#define Bselect E2X("Select")
#define Bselectfiles E2X("Select Files")
#define Bshow E2X("Show")
#define Bsize E2X("Size")
#define Bstart E2X("Start")
#define Bstop E2X("Stop")
#define Bstrength E2X("Strength")
#define Bareanotfinished E2X("the area is not finished")
#define Bthresh E2X("Threshold")
#define Btop E2X("Top")
#define Btransparency E2X("Transparency")
#define Btrash E2X("Trash")
#define Btrim E2X("Trim")
#define Bundoall E2X("Undo All")
#define Bundolast E2X("Undo Last")
#define Bundo E2X("Undo")
#define Bunfinish E2X("Unfinish")
#define Bupdate E2X("Update")
#define Bview E2X("View")
#define Bweb E2X("Web")
#define Bwhite E2X("White")
#define Bwidth E2X("Width")
#define Bxoffset E2X("x-offset")
#define Byoffset E2X("y-offset")
#define Byes E2X("Yes")


