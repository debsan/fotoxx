.TH FOTOXX 1 2019-01-01 "Linux" "Fotoxx man page"

.SH NAME
 Fotoxx - photo/image editor and collection manager

.SH SYNOPSIS
 \fBFotoxx\fR [ \fBoptions\fR ] [ \fIfile\fR | \fIfolder\fR ]

.SH DESCRIPTION
 Organize and manage a large image collection. Edit and optimize 
 photos and other images, search images, and perform batch operations. 
 Fotoxx is a GTK application which operates in its own window. 
 The included user manual explains its operation in great detail.
 The following is a summary of capabilities.

.SH OVERVIEW 

  • Organize and manage a large image collection: find what you are looking for, 
    create albums (subcollections), process RAW images, repair and enhance photos, 
    combine images (HDR, panorama, mashup, montage), edit and report metadata ...
  • Thumbnail browser/navigator with variable size thumbnails and list view.
  • Camera RAW file conversion, single or batch, with 16 bit color depth.
  • Internal processing in 24 bits per color (float), output in 8 or 16 bits.
  • A comprehensive set of image edit functions is available: brightness, color,
    contrast, trim/crop, rotate, upright, resize, sharpen, denoise, paint, text, 
    lines, red eye, fix distortions, HDR, panorama, stack, montage, mashup ...
  • Advanced edit functions to bring out details and add flair to an image.
  • A large set of creative special effects and arty transforms is available
    (sketch, cartoon, emboss, dither, painting, mosaic, sphere, tiny planet ...)
  • Select image objects or areas and edit them separately from the background
    (draw outline, follow edges, spread out from mouse to matching colors ...)
  • Special selection tool for complex edges (e.g. hair, foliage)
  • Copy areas within and across images by mouse painting and blending.
  • Rapid visual feedback using the full image or a selected zoom-in area.
  • Most edit functions can be 'painted' locally and gradually using the mouse.
  • View a 360 degree panorama image (Google Street View format).
  • Metadata edit and report (tags, dates, captions, geotags ... any metadata).
  • Edited files are saved with a version number, originals are kept by default.
  • Transparent image areas can be retained. Variable transparency can be added.
  • Custom scripts: record a series of image edits, use as a new edit function.
  • Batch tools are available to rename, resize, convert, process RAW files, 
    export, add, revise and report metadata, and execute custom edit scripts.
  • Search images using any metadata and folder/file names or partial names
    (dates/times, locations, ratings, captions/comments, exposure data ...).
  • Show a table of image locations and date groups, click for gallery of images.
  • Show an image calendar, click on year or month for a gallery of images.
  • Find images by clicking markers on a scalable world map (internet map).
  • Also use locally stored maps (world, continents, nations, cities ... custom).
  • Create albums with selected images. Arrange sequence by drag and drop.
  • Slide show: use many animated image transitions and slow pan/zoom in or out.
  • Mashup: arrange images and text in an arbitrary layout using the mouse.
  • Print an image at any scale. A printer color calibration tool is available.
  • Show video files (first frame) like regular images, optionally play videos.
  • Custom favorites menu - user can select and arrange icon and text menus
  • Custom keyboard shortcuts can be assigned to all functions
  • Comprehensive user guide and context help popups via F1 key.

.SH OPTIONS
Command line options
 \fB-v\fR                 print version and exit
 \fB-lang\fR code         specify language (de, fr ...)
 \fB-prev\fR              open last file from the previous session
 \fB-recent\fR            show a gallery of most recent files 
 \fIfile\fR               initial image file to view or edit
 \fIfolder\fR             initial folder for image gallery

.SH SEE ALSO
 The home page for Fotoxx is at https://kornelix.net

.SH AUTHORS
 Written by Mike Cornelison <kornelix@posteo.de>
 https://kornelix.net

